#include <iostream>
#include <string>
#include "depgraph.h"

using namespace std;

auto console = spdlog::stdout_logger_mt("console");

DepGraph::DepGraph() {
    _logger = console;
}

DepGraph::~DepGraph() {
    for (auto const& it: allNodes) {
        _logger->info("DepGraph::~DepGraph -> Deleting \"{}\"", it->getIdentifier());
        delete it;
    }
}

int DepGraph::addNode(string identifier, string content) {
    if (identifier.empty()) {
        _logger->warn("DepGraph::addNode -> Empty identifier.");
        return -1;
    }
    if (content.empty()) {
        _logger->warn("DepGraph::addNode -> Empty content.");
        return -2;
    }
    for (auto const& it: allNodes) {
        if (identifier.compare(it->getIdentifier()) == 0) {
            _logger->warn("DepGraph::addNode -> Existing identifier.");
            return -3;
        }
    }
    DepGraphNode* newNode = new DepGraphNode();
    newNode->setIdentifier(identifier);
    newNode->setContent(content);
    allNodes.push_back(newNode);
    return 0;
}

int DepGraph::addDependency(string identifier, string dependency) {
    if (identifier.empty()) {
        _logger->warn("DepGraph::addDependency -> Empty identifier.");
        return -1;
    }
    if (dependency.empty()) {
        _logger->warn("DepGraph::addDependency -> Empty dependency.");
        return -2;
    }
    DepGraphNode *idNode = NULL;
    DepGraphNode *depNode = NULL;
    for (auto const& it: allNodes) {
        if (idNode && depNode) {
            break;
        }
        if (identifier.compare(it->getIdentifier()) == 0) {
            idNode = it;
            continue;
        }
        if (dependency.compare(it->getIdentifier()) == 0) {
            depNode = it;
        }
    }
    if (!idNode) {
        _logger->warn("DepGraph::addDependency -> Identifier not found.");
        return -3;
    }
    if (!depNode) {
        _logger->warn("DepGraph::addDependency -> Dependency not found.");
        return -4;
    }
    if (idNode->addDescendant(depNode) != 0) {
        _logger->warn("DepGraph::addDependency -> Descendant adding failed.");
        return -5;
    }
    depNode->addAncestor(idNode);
    // if (depNode->addAncestor(idNode) != 0) {
    //     _logger->warn("DepGraph::addDependency -> Ancestor adding failed.");
    //     // NOTE: This case is difficult to test from this class because
    //     // addDescendant() will catch the same case but earlier. Something
    //     // has to be wrong in DepGraphNode for this branch to run
    //     return -6;
    // }
    return 0;
}

int DepGraph::removeNode(string identifier) {
    if (identifier.empty()) {
        _logger->warn("DepGraph::removeNode -> Empty identifier.");
        return -1;
    }
    DepGraphNode *idNode = NULL;
    vector<DepGraphNode*>::iterator it = allNodes.begin();
    vector<DepGraphNode*>::iterator foundIt = allNodes.end();
    while (it != allNodes.end()) {
        if (identifier.compare((*it)->getIdentifier()) == 0) {
            foundIt = it;
            idNode = *it;
            break;
        }
        it++;
    }
    if (!idNode) {
        _logger->warn("DepGraph::removeNode -> Identifier not found.");
        return -2;
    }
    idNode->removeDescendantAncestor();
    idNode->removeAncestorDescendant();
    allNodes.erase(foundIt);
    return 0;
}

int DepGraph::getNumberOfNodes() {
    return allNodes.size();
}

int DepGraph::getNumberOfAncestors(string identifier) {
    for (auto const& it: allNodes) {
        if (identifier.compare(it->getIdentifier()) == 0) {
            return it->getNumberOfAncestors();
        }
    }
    return -1;
}

int DepGraph::getNumberOfDescendants(string identifier) {
    for (auto const& it: allNodes) {
        if (identifier.compare(it->getIdentifier()) == 0) {
            return it->getNumberOfDescendants();
        }
    }
    return -1;
}

bool DepGraph::hasAncestor(string identifier, string dependency) {
    for (auto const& it: allNodes) {
        if (identifier.compare(it->getIdentifier()) == 0) {
            return it->hasAncestor(dependency);
        }
    }
    return false;
}

bool DepGraph::hasDescendant(string identifier, string dependency) {
    for (auto const& it: allNodes) {
        if (identifier.compare(it->getIdentifier()) == 0) {
            return it->hasDescendant(dependency);
        }
    }
    return false;
}

void DepGraph::printGraph() {
    _logger->info("DepGraph::printGraph -> begin");
    for (auto const& it: allNodes) {
        it->printNode();
    }
    _logger->info("DepGraph::printGraph -> end");
}

