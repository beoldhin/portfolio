#include <vector>
#include <iostream>
#include "depgraphnode.h"

using namespace std;

DepGraphNode::DepGraphNode() {
    _logger = spdlog::get("console");
}

DepGraphNode::~DepGraphNode() {
    ancestors.clear();
    descendants.clear();
}

string DepGraphNode::getIdentifier() {
    return identifier;
}

void DepGraphNode::setIdentifier(string identifier) {
    this->identifier = identifier;
}

int DepGraphNode::addAncestor(DepGraphNode *ancestor) {
    if (!ancestor) {
        return -1;
    }
    for (auto const& it: ancestors) {
        if (it == ancestor) {
            return -2;
        }
    }
    ancestors.push_back(ancestor);
    return 0;
}

int DepGraphNode::addDescendant(DepGraphNode *descendant) {
    if (!descendant) {
        return -1;
    }
    for (auto const& it: descendants) {
        if (it == descendant) {
            return -2;
        }
    }
    descendants.push_back(descendant);
    return 0;
}

int DepGraphNode::removeAncestorDescendant() {
    for (auto const& it: ancestors) {
        vector<DepGraphNode*>::iterator other = it->descendants.begin();
        while (other != it->descendants.end()) {
            if (getIdentifier().compare((*other)->getIdentifier()) == 0) {
                it->descendants.erase(other);
                break;
            }
            other++;
        }
    }
    return 0;
}

int DepGraphNode::removeDescendantAncestor() {
    for (auto const& it: descendants) {
        vector<DepGraphNode*>::iterator other = it->ancestors.begin();
        while (other != it->ancestors.end()) {
            if (getIdentifier().compare((*other)->getIdentifier()) == 0) {
                it->ancestors.erase(other);
                break;
            }
            other++;
        }
    }
    return 0;
}

void DepGraphNode::setContent(string content) {
    this->content = content;
}

int DepGraphNode::getNumberOfAncestors() {
    return ancestors.size();
}

int DepGraphNode::getNumberOfDescendants() {
    return descendants.size();
}

bool DepGraphNode::hasAncestor(string dependency) {
    for (auto const& it: ancestors) {
        if (dependency.compare(it->getIdentifier()) == 0) {
            return true;
        }
    }
    return false;
}

bool DepGraphNode::hasDescendant(string dependency) {
    for (auto const& it: descendants) {
        if (dependency.compare(it->getIdentifier()) == 0) {
            return true;
        }
    }
    return false;
}

void DepGraphNode::printNode() {
    _logger->info("  Node Identifier: {}", identifier);
    for (auto const& it: ancestors) {
        _logger->info("    Ancestor Identifier: {}", it->getIdentifier());
    }
    for (auto const& it: descendants) {
        _logger->info("    Descendant Identifier: {}", it->getIdentifier());
    }
}

