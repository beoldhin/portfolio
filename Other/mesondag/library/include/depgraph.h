#pragma once
#include <string>
#include <vector>
#include "spdlog/spdlog.h"
#include "depgraphnode.h"

class DepGraph {
public:
    DepGraph();
    ~DepGraph();
    int addNode(std::string identifier, std::string content);
    int addDependency(std::string identifier, std::string dependency);
    int removeNode(std::string identifier);
public:
    int getNumberOfNodes();
    int getNumberOfAncestors(std::string identifier);
    int getNumberOfDescendants(std::string identifier);
    bool hasAncestor(std::string identifier, std::string dependency);
    bool hasDescendant(std::string identifier, std::string dependency);
    void printGraph();
private:
    std::vector<DepGraphNode*> allNodes;
private:
    std::shared_ptr<spdlog::logger> _logger;
};

