#pragma once
#include <string>
#include <vector>
#include "spdlog/spdlog.h"

class DepGraphNode {
public:
    DepGraphNode();
    ~DepGraphNode();
    std::string getIdentifier();
    void setIdentifier(std::string identifier);
    int addAncestor(DepGraphNode *ancestor);
    int addDescendant(DepGraphNode *descendant);
    int removeAncestorDescendant();
    int removeDescendantAncestor();
    void setContent(std::string content);
public:
    int getNumberOfAncestors();
    int getNumberOfDescendants();
    bool hasAncestor(std::string dependency);
    bool hasDescendant(std::string dependency);
    void printNode();
private:
    std::string identifier;
    std::vector<DepGraphNode*> ancestors;
    std::vector<DepGraphNode*> descendants;
    std::string content;
private:
    std::shared_ptr<spdlog::logger> _logger;
};

