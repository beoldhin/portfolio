#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "depgraph.h"

TEST_CASE("Adding a node works correctly") {
    SECTION("Calling with empty identifier") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("","SomeContent") == -1);
        REQUIRE(depGraph->getNumberOfNodes() == 0);
        delete depGraph;
    }
    SECTION("Calling with empty content") {
        DepGraph* depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode","") == -2);
        REQUIRE(depGraph->getNumberOfNodes() == 0);
        delete depGraph;
    }
    SECTION("Calling with non-existing identifier") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode","SomeContent") == 0);
        REQUIRE(depGraph->getNumberOfNodes() == 1);
        REQUIRE(depGraph->getNumberOfDescendants("SomeNode") == 0);
        REQUIRE(depGraph->getNumberOfAncestors("SomeNode") == 0);
        delete depGraph;
    }
    SECTION("Calling with existing identifier") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode","SomeContent") == 0);
        REQUIRE(depGraph->addNode("SomeNode","SomeContent") == -3);
        REQUIRE(depGraph->getNumberOfNodes() == 1);
        REQUIRE(depGraph->getNumberOfDescendants("SomeNode") == 0);
        REQUIRE(depGraph->getNumberOfAncestors("SomeNode") == 0);
        delete depGraph;
    }
}

TEST_CASE("Adding a dependency works correctly") {
    SECTION("Calling with empty identifier") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addDependency("","SomeContent") == -1);
        delete depGraph;
    }
    SECTION("Calling with empty dependency") {
        DepGraph* depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode","SomeContent") == 0);
        REQUIRE(depGraph->addDependency("SomeNode","") == -2);
        REQUIRE(depGraph->getNumberOfNodes() == 1);
        delete depGraph;
    }
    SECTION("Calling with unknown identifier") {
        DepGraph* depGraph = new DepGraph();
        REQUIRE(depGraph->addDependency("Unknown","Unknown") == -3);
        delete depGraph;
    }
    SECTION("Calling with unknown dependency") {
        DepGraph* depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode", "SomeContent") == 0);
        REQUIRE(depGraph->addDependency("SomeNode","Unknown") == -4);
        REQUIRE(depGraph->getNumberOfNodes() == 1);
        REQUIRE(depGraph->getNumberOfDescendants("SomeNode") == 0);
        REQUIRE(depGraph->getNumberOfAncestors("SomeNode") == 0);
        REQUIRE(depGraph->hasDescendant("SomeNode","Unknown") == false);
        REQUIRE(depGraph->hasAncestor("SomeNode","Unknown") == false);
        delete depGraph;
    }
    SECTION("Adding a new dependency") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode","SomeContent") == 0);
        REQUIRE(depGraph->addNode("OtherNode","SomeContent") == 0);
        REQUIRE(depGraph->addDependency("SomeNode", "OtherNode") == 0);
        REQUIRE(depGraph->getNumberOfNodes() == 2);
        REQUIRE(depGraph->getNumberOfDescendants("SomeNode") == 1);
        REQUIRE(depGraph->getNumberOfAncestors("OtherNode") == 1);
        REQUIRE(depGraph->hasDescendant("SomeNode","OtherNode") == true);
        REQUIRE(depGraph->hasAncestor("OtherNode","SomeNode") == true);
        delete depGraph;
    }
    SECTION("Adding an already existing dependency") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode","SomeContent") == 0);
        REQUIRE(depGraph->addNode("OtherNode","SomeContent") == 0);
        REQUIRE(depGraph->addDependency("SomeNode", "OtherNode") == 0);
        REQUIRE(depGraph->addDependency("SomeNode", "OtherNode") == -5);
        REQUIRE(depGraph->getNumberOfNodes() == 2);
        REQUIRE(depGraph->getNumberOfDescendants("SomeNode") == 1);
        REQUIRE(depGraph->getNumberOfAncestors("OtherNode") == 1);
        REQUIRE(depGraph->hasDescendant("SomeNode","OtherNode") == true);
        REQUIRE(depGraph->hasAncestor("OtherNode","SomeNode") == true);
        delete depGraph;
    }
}

TEST_CASE("Removing a node works correctly") {
    SECTION("Calling with empty identifier") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->removeNode("") == -1);
        delete depGraph;
    }
    SECTION("Calling with unknown identifier") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->removeNode("Unknown") == -2);
        delete depGraph;
    }
    SECTION("Removing without dependencies") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode","SomeContent") == 0);
        REQUIRE(depGraph->removeNode("SomeNode") == 0);
        delete depGraph;
    }
    SECTION("Removing a node with one descendant") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("SomeNode","SomeContent") == 0);
        REQUIRE(depGraph->addNode("OtherNode","SomeContent") == 0);
        REQUIRE(depGraph->addDependency("SomeNode","OtherNode") == 0);
        REQUIRE(depGraph->removeNode("SomeNode") == 0);
        REQUIRE(depGraph->getNumberOfNodes() == 1);
        REQUIRE(depGraph->getNumberOfAncestors("SomeNode") == -1);
        REQUIRE(depGraph->getNumberOfDescendants("SomeNode") == -1);
        REQUIRE(depGraph->getNumberOfAncestors("OtherNode") == 0);
        REQUIRE(depGraph->getNumberOfDescendants("OtherNode") == 0);
        REQUIRE(depGraph->hasDescendant("SomeNode","OtherNode") == false);
        REQUIRE(depGraph->hasAncestor("OtherNode","SomeNode") == false);
        delete depGraph;
    }
}

TEST_CASE("Complex graph management works correctly") {
    SECTION("Creating a complex network works correctly") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("Mojo","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Data-Dumper","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Encode","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Digest-MD5","SomeContent") == 0);
        REQUIRE(depGraph->addNode("IO","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Time-HiRes","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Digest","SomeContent") == 0);
        REQUIRE(depGraph->addNode("XSLoader","SomeContent") == 0);
        REQUIRE(depGraph->addNode("File-Temp","SomeContent") == 0);
        REQUIRE(depGraph->addNode("MIME-Base64","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Test-Simple","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Test-Harness","SomeContent") == 0);
        REQUIRE(depGraph->addNode("PathTools","SomeContent") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Data-Dumper") == 0);
        REQUIRE(depGraph->addDependency("Mojo","MIME-Base64") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Encode") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Digest-MD5") == 0);
        REQUIRE(depGraph->addDependency("Mojo","IO") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("Mojo","File-Temp") == 0);
        REQUIRE(depGraph->addDependency("Mojo","PathTools") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Time-HiRes") == 0);
        REQUIRE(depGraph->addDependency("Digest-MD5","Digest") == 0);
        REQUIRE(depGraph->addDependency("Digest-MD5","XSLoader") == 0);
        REQUIRE(depGraph->addDependency("Digest","MIME-Base64") == 0);
        REQUIRE(depGraph->addDependency("XSLoader","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("File-Temp","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("File-Temp","PathTools") == 0);
        REQUIRE(depGraph->addDependency("Test-Simple","Test-Harness") == 0);
        REQUIRE(depGraph->addDependency("Test-Harness","PathTools") == 0);
        REQUIRE(depGraph->getNumberOfNodes() == 13);
        REQUIRE(depGraph->getNumberOfAncestors("PathTools") == 3);
        REQUIRE(depGraph->getNumberOfDescendants("PathTools") == 0);
        REQUIRE(depGraph->hasAncestor("PathTools","Test-Harness") == true);
        REQUIRE(depGraph->hasAncestor("PathTools","File-Temp") == true);
        REQUIRE(depGraph->hasAncestor("PathTools","Mojo") == true);
        REQUIRE(depGraph->getNumberOfAncestors("Digest") == 1);
        REQUIRE(depGraph->getNumberOfDescendants("Digest") == 1);
        REQUIRE(depGraph->hasAncestor("Digest","Digest-MD5") == true);
        REQUIRE(depGraph->hasDescendant("Digest","MIME-Base64") == true);
        delete depGraph;
    }
    SECTION("Removing from a complex network works correctly") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("Mojo","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Data-Dumper","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Encode","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Digest-MD5","SomeContent") == 0);
        REQUIRE(depGraph->addNode("IO","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Time-HiRes","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Digest","SomeContent") == 0);
        REQUIRE(depGraph->addNode("XSLoader","SomeContent") == 0);
        REQUIRE(depGraph->addNode("File-Temp","SomeContent") == 0);
        REQUIRE(depGraph->addNode("MIME-Base64","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Test-Simple","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Test-Harness","SomeContent") == 0);
        REQUIRE(depGraph->addNode("PathTools","SomeContent") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Data-Dumper") == 0);
        REQUIRE(depGraph->addDependency("Mojo","MIME-Base64") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Encode") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Digest-MD5") == 0);
        REQUIRE(depGraph->addDependency("Mojo","IO") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("Mojo","File-Temp") == 0);
        REQUIRE(depGraph->addDependency("Mojo","PathTools") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Time-HiRes") == 0);
        REQUIRE(depGraph->addDependency("Digest-MD5","Digest") == 0);
        REQUIRE(depGraph->addDependency("Digest-MD5","XSLoader") == 0);
        REQUIRE(depGraph->addDependency("Digest","MIME-Base64") == 0);
        REQUIRE(depGraph->addDependency("XSLoader","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("File-Temp","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("File-Temp","PathTools") == 0);
        REQUIRE(depGraph->addDependency("Test-Simple","Test-Harness") == 0);
        REQUIRE(depGraph->addDependency("Test-Harness","PathTools") == 0);
        REQUIRE(depGraph->getNumberOfDescendants("Mojo") == 9);
        REQUIRE(depGraph->getNumberOfDescendants("Test-Harness") == 1);
        REQUIRE(depGraph->getNumberOfDescendants("File-Temp") == 2);
        REQUIRE(depGraph->removeNode("PathTools") == 0);
        REQUIRE(depGraph->getNumberOfNodes() == 12);
        REQUIRE(depGraph->getNumberOfDescendants("Mojo") == 8);
        REQUIRE(depGraph->getNumberOfDescendants("Test-Harness") == 0);
        REQUIRE(depGraph->getNumberOfDescendants("File-Temp") == 1);
        REQUIRE(depGraph->removeNode("File-Temp") == 0);
        REQUIRE(depGraph->getNumberOfAncestors("Mojo") == 0);
        REQUIRE(depGraph->getNumberOfDescendants("Mojo") == 7);
        REQUIRE(depGraph->getNumberOfAncestors("Test-Simple") == 2);
        REQUIRE(depGraph->getNumberOfDescendants("Test-Simple") == 1);
        delete depGraph;
    }
    SECTION("Adding to a complex network works correctly") {
        DepGraph *depGraph = new DepGraph();
        REQUIRE(depGraph->addNode("Mojo","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Data-Dumper","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Encode","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Digest-MD5","SomeContent") == 0);
        REQUIRE(depGraph->addNode("IO","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Time-HiRes","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Digest","SomeContent") == 0);
        REQUIRE(depGraph->addNode("XSLoader","SomeContent") == 0);
        REQUIRE(depGraph->addNode("File-Temp","SomeContent") == 0);
        REQUIRE(depGraph->addNode("MIME-Base64","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Test-Simple","SomeContent") == 0);
        REQUIRE(depGraph->addNode("Test-Harness","SomeContent") == 0);
        REQUIRE(depGraph->addNode("PathTools","SomeContent") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Data-Dumper") == 0);
        REQUIRE(depGraph->addDependency("Mojo","MIME-Base64") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Encode") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Digest-MD5") == 0);
        REQUIRE(depGraph->addDependency("Mojo","IO") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("Mojo","File-Temp") == 0);
        REQUIRE(depGraph->addDependency("Mojo","PathTools") == 0);
        REQUIRE(depGraph->addDependency("Mojo","Time-HiRes") == 0);
        REQUIRE(depGraph->addDependency("Digest-MD5","Digest") == 0);
        REQUIRE(depGraph->addDependency("Digest-MD5","XSLoader") == 0);
        REQUIRE(depGraph->addDependency("Digest","MIME-Base64") == 0);
        REQUIRE(depGraph->addDependency("XSLoader","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("File-Temp","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("File-Temp","PathTools") == 0);
        REQUIRE(depGraph->addDependency("Test-Simple","Test-Harness") == 0);
        REQUIRE(depGraph->addDependency("Test-Harness","PathTools") == 0);
        REQUIRE(depGraph->addNode("NewNode","SomeContent") == 0);
        REQUIRE(depGraph->getNumberOfNodes() == 14);
        REQUIRE(depGraph->getNumberOfDescendants("IO") == 0);
        REQUIRE(depGraph->getNumberOfAncestors("Digest-MD5") == 1);
        REQUIRE(depGraph->getNumberOfAncestors("XSLoader") == 1);
        REQUIRE(depGraph->getNumberOfAncestors("Test-Simple") == 3);
        REQUIRE(depGraph->getNumberOfAncestors("File-Temp") == 1);
        REQUIRE(depGraph->addDependency("IO","NewNode") == 0);
        REQUIRE(depGraph->addDependency("NewNode","Digest-MD5") == 0);
        REQUIRE(depGraph->addDependency("NewNode","XSLoader") == 0);
        REQUIRE(depGraph->addDependency("NewNode","Test-Simple") == 0);
        REQUIRE(depGraph->addDependency("NewNode","File-Temp") == 0);
        REQUIRE(depGraph->getNumberOfAncestors("IO") == 1);
        REQUIRE(depGraph->getNumberOfDescendants("IO") == 1);
        REQUIRE(depGraph->getNumberOfAncestors("Digest-MD5") == 2);
        REQUIRE(depGraph->getNumberOfDescendants("Digest-MD5") == 2);
        REQUIRE(depGraph->getNumberOfAncestors("XSLoader") == 2);
        REQUIRE(depGraph->getNumberOfDescendants("XSLoader") == 1);
        REQUIRE(depGraph->getNumberOfAncestors("Test-Simple") == 4);
        REQUIRE(depGraph->getNumberOfDescendants("Test-Simple") == 1);
        REQUIRE(depGraph->getNumberOfAncestors("File-Temp") == 2);
        REQUIRE(depGraph->getNumberOfDescendants("File-Temp") == 2);
        delete depGraph;
    }
}

TEST_CASE("Other tests to increase coverity") {
    DepGraph *depGraph = new DepGraph();
    REQUIRE(depGraph->addNode("SomeNode","SomeContent") == 0);
    REQUIRE(depGraph->addNode("OtherNode","SomeContent") == 0);
    REQUIRE(depGraph->hasAncestor("Unknown","Unknown") == false);
    REQUIRE(depGraph->addDependency("SomeNode","OtherNode") == 0);
    depGraph->printGraph();
    delete depGraph;
}

