#!/usr/bin/env python

import networkx as nx
import matplotlib.pyplot as plt

import os
import sys
import yaml

from dagutils import *
from dbdiff import *


class DepGraph:
    G = 0   # the graph
    dbdiff = 0  # Database of differences
    sched = []  # scheduled order result
    rsched = []  # scheduled order result in reverse
    dagutils = 0
    assetdir = 0
    dependword = 'Depends'
    dbfilename = 'cache/dbdiff.db'
    gmlfilename = 'cache/daggml.gml'
    schedfilename = 'cache/sched.topo'
    rschedfilename = 'cache/rsched.topo'
    def __init__(self):
        self.G = nx.DiGraph()
        self.dbdiff = DBDiff()
        self.dagutils = DagUtils()
    def find_node_by_name(self, nodename):
        for node in self.G:
            if node == nodename:
                return True
        return False
    def find_node_by_list(self, nodenames):
        for nodename in nodenames:
            if find_node_by_name(nodename) == True:
                return True
        return False
    def check_dependent_node(self, nodenames):
        for nodename in nodenames:
            if self.find_node_by_name(nodename) == False:
                print("Dependent node \"" + nodename + "\" not found!")
                sys.exit(1)
    def check_self_dependency(self, nodenames, basenodename):
        for nodename in nodenames:
            if nodename == basenodename:
                print("Dependency to self \"" + nodename + "\"!")
                sys.exit(1)
    def check_simple_circular(self, nodenames, basenodename):
        for nodename in nodenames:
            for templeft,tempright in self.G.node[nodename].items():
                if templeft == self.dependword:
                    for dependname in tempright:
                        if dependname == basenodename:
                            print("Cross dependency between \"" + basenodename + "\" and \"" + nodename + "\"!")
                            sys.exit(1)
    def save_states_to_file(self):
        nx.write_gml(self.G, self.gmlfilename)
        self.dagutils.save_list_to_file(self.schedfilename, self.sched)
        self.dagutils.save_list_to_file(self.rschedfilename, self.rsched)
    def save_states_to_database(self, filenames):
        gmlfile = self.dagutils.read_from_file(self.gmlfilename)
        sched = self.dagutils.read_from_file(self.schedfilename)
        rsched = self.dagutils.read_from_file(self.rschedfilename)
        self.dbdiff.add_entry(filenames, gmlfile, sched, rsched)
    def save_states(self, filenames):
        self.save_states_to_file()
        self.save_states_to_database(filenames)
    def restore_states_from_database(self, filenames):
        gmlfile, sched, rsched = self.dbdiff.get_entry(filenames)
        if gmlfile == "" or sched == "" or rsched == "":
            return False
        self.dagutils.write_to_file(self.gmlfilename, gmlfile)
        self.dagutils.write_to_file(self.schedfilename, sched)
        self.dagutils.write_to_file(self.rschedfilename, rsched)
        return True
    def restore_states_from_file(self):
        self.G = nx.read_gml(self.gmlfilename, relabel=True)
        self.sched = self.dagutils.read_list_from_file(self.schedfilename)
        self.rsched = self.dagutils.read_list_from_file(self.rschedfilename)
    def restore_states(self, filenames):
        if self.restore_states_from_database(filenames) == False:
            return False
        self.restore_states_from_file()
        return True
    def read_config_file(self, conffilename):
        # Read file
        with open(conffilename, 'r') as f:
            documents = yaml.load_all(f)
            for doc in documents:
                for nodename,props in doc.items():
                    if self.find_node_by_name(nodename) == True:
                        print("Node \"" + nodename + "\" already exists!")
                        sys.exit(1)
                    self.G.add_node(nodename)
                    for propleft,propright in props.items():
                        self.G.node[nodename][propleft] = propright
        # Check file validity
        for nodename,props in self.G.nodes(data=True):
            for propleft,propright in props.items():
                if propleft == self.dependword:
                    self.check_dependent_node(propright)
                    self.check_self_dependency(propright, nodename)
                    self.check_simple_circular(propright, nodename)
        # Now create the edges
        for nodename,props in self.G.nodes(data=True):
            for propleft,propright in props.items():
                if propleft == self.dependword:
                    for depend in propright:
                        self.G.add_edge(nodename, depend)
                    del self.G.node[nodename][propleft]
        # Create scheduling order
        self.create_scheduling_order()
    def get_scheduling_order(self):
        return self.sched
    def get_reverse_scheduling_order(self):
        return self.rsched
    def create_scheduling_order(self):
        self.sched = []
        removelist = []
        GCopy = self.G.copy()
        while GCopy.number_of_nodes() > 0:
            for node in GCopy.nodes_iter():
                descendants = nx.descendants(GCopy, node)
                if not descendants:
                    removelist.append(node)
            for remove in removelist:
                GCopy.remove_node(remove)
            if len(removelist) >= 2:
                for i in range(len(removelist)):
                    if i == 0:
                        self.sched.append("S:" + removelist[i])
                    elif i == len(removelist)-1:
                        self.sched.append("E:" + removelist[i])
                    else:
                        self.sched.append("C:" + removelist[i])
            elif len(removelist) == 1:
                self.sched.extend(removelist)
            removelist[:] = []
        self.create_reverse_scheduling_order()
    def create_reverse_scheduling_order(self):
        self.rsched = []
        removelist = []
        GCopy = self.G.copy()
        while GCopy.number_of_nodes() > 0:
            for node in GCopy.nodes_iter():
                ancestors = nx.ancestors(GCopy, node)
                if not ancestors:
                    removelist.append(node)
            for remove in removelist:
                GCopy.remove_node(remove)
            if len(removelist) >= 2:
                for i in range(len(removelist)):
                    if i == 0:
                        self.rsched.append("S:" + removelist[i])
                    elif i == len(removelist)-1:
                        self.rsched.append("E:" + removelist[i])
                    else:
                        self.rsched.append("C:" + removelist[i])
            elif len(removelist) == 1:
                self.rsched.extend(removelist)
            removelist[:] = []
    def read_configurations(self, conffilenames):
        print("Creating states...")
        for conffilename in conffilenames:
            self.read_config_file(conffilename)
        print("Saving states...")
        self.save_states(conffilenames)
    def print_result(self):  # PUBLIC
        nx.draw(self.G, with_labels=True)
        plt.show()
        # print(self.G.nodes(data=True))
    def create_traverse_list(self):  # PUBLIC
        traverselist = []
        for node in self.G.nodes_iter():
            descendants = nx.descendants(self.G, node)
            if not descendants:
                traverselist.append(node)
        return traverselist
    def create_traverse_list_reverse(self):  # PUBLIC
        traverselist = []
        for node in self.G.nodes_iter():
            ancestors = nx.ancestors(self.G, node)
            if not ancestors:
                traverselist.append(node)
        return traverselist
    def complete_traverse_node(self, node, traverselist, isDebug=False):  # PUBLIC
        if nx.descendants(self.G, node):  # For input legality checking
            print("ERROR: Input node Has descendants")
            return []
        edges = self.G.in_edges(node, data=False)
        self.G.remove_node(node)
        traverselist.remove(node)
        reallist = []
        if isDebug:
            for node in self.G.nodes_iter():
                ancestors = nx.ancestors(self.G, node)
                if not ancestors:
                    reallist.append(node)
            reallist = sorted(reallist)
        for edge in edges:
            ancestor = edge[0]
            descendants = nx.descendants(self.G, ancestor)
            if not descendants:
                traverselist.append(ancestor)
        if isDebug:
            traverselist = sorted(traverselist)
            if traverselist != reallist:
                print("ERROR: Internal error")
        return traverselist
    def complete_traverse_node_reverse(self, node, traverselist, isDebug=False):  # PUBLIC
        if nx.ancestors(self.G, node) and nx.descendants(self.G, node):  # For input legality checking
            print("ERROR: Input node Has ancestors and descendants")
            return []
        edges = self.G.out_edges(node, data=False)
        self.G.remove_node(node)
        traverselist.remove(node)
        reallist = []
        if isDebug:
            for node in self.G.nodes_iter():
                ancestors = nx.ancestors(self.G, node)
                if not ancestors:
                    reallist.append(node)
            reallist = sorted(reallist)
        for edge in edges:
            descendant = edge[1]
            ancestors = nx.ancestors(self.G, descendant)
            if not ancestors:
                traverselist.append(descendant)
        if isDebug:
            traverselist = sorted(traverselist)
            if traverselist != reallist:
                print("ERROR: Internal error")
        # Debug on -- end
        return traverselist
    def read_config_files(self, conffilenames):  # PUBLIC
        # Sorting the file name list is very important here
        # as it creates the uniqueness and avoids unnecessary
        # cache skipping
        conffilenames = sorted(conffilenames)
        updated = self.dbdiff.update_state(conffilenames)
        if updated == True:
            self.read_configurations(conffilenames)
        else:
            print("Restoring states...")
            if self.restore_states(conffilenames) == False:
                # This is the case with new config file arrangement
                # but the config files are not changed
                self.read_configurations(conffilenames)
    def initialize(self, assetdir):  # PUBLIC
        self.assetdir = assetdir
        self.dbdiff.initialize_db(self.dbfilename)
    def uninitialize(self):  # PUBLIC
        self.dbdiff.uninitialize_db()

