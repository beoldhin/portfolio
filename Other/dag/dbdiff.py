#!/usr/bin/env python

import os
import time
import sqlite3

class DBDiff:
    c = 0
    conn = 0
    dbfilename = 0
    # sqlite create
    diffs_CREATE     = '''CREATE TABLE IF NOT EXISTS diffs
                          (ID integer PRIMARY KEY, filename text, filemodtime text, filesize text)'''
    cacheptrs_CREATE = '''CREATE TABLE IF NOT EXISTS cacheptrs
                          (ID integer PRIMARY KEY, filename text, config text)'''
    cache_CREATE     = '''CREATE TABLE IF NOT EXISTS cache
                          (ID integer PRIMARY KEY, config text, gmlfile blob, sched blob, rsched blob)'''
    # sqlite find
    diffs_FIND       = '''SELECT * FROM diffs
                          WHERE filename=?'''
    cacheptrs_FIND   = '''SELECT * FROM cacheptrs
                          WHERE filename=?'''
    cache_FIND       = '''SELECT * FROM cache
                          WHERE config=?'''
    # sqlite insert
    diffs_INSERT     = '''INSERT INTO diffs(filename, filemodtime, filesize)
                          VALUES (?, ?, ?)'''
    cacheptrs_INSERT = '''INSERT INTO cacheptrs(filename, config)
                          VALUES (?, ?)'''
    cache_INSERT     = '''INSERT into cache(config, gmlfile, sched, rsched)
                          VALUES (?, ?, ?, ?)'''
    # sqlite delete
    cacheptrs_DELETE = '''DELETE FROM cacheptrs
                          WHERE ID=?'''
    cache_DELETE     = '''DELETE FROM cache
                          WHERE ID=?'''
    # sqlite update
    diffs_UPDATE     = '''UPDATE diffs
                          SET filemodtime=?, filesize=?
                          WHERE filename=?'''
    def create_config_str(self, filenames):
        config = ":".join(filenames)
        return config
    def create_config_list(self, filenames):
        config = filenames.split(':')
        return config
    def get_diff_info(self, filename):
        filemodtime = time.ctime(os.path.getmtime(filename))
        filesize = str(os.path.getsize(filename))
        return filemodtime, filesize
    def update_cache(self, filename):
        while True:
            self.c.execute(self.cacheptrs_FIND, (filename,))
            fetched = self.c.fetchone()
            if not fetched:
                break
            dbID = fetched[0]
            config = fetched[2]
            self.c.execute(self.cacheptrs_DELETE, (dbID,))
            while True:
                self.c.execute(self.cache_FIND, (config,))
                fetched = self.c.fetchone()
                if not fetched:
                    break
                dbID = fetched[0]
                self.c.execute(self.cache_DELETE, (dbID,))
    def update_diff(self, filename):
        filemodtime, filesize = self.get_diff_info(filename)
        self.c.execute(self.diffs_FIND, (filename,))
        fetched = self.c.fetchone()
        if not fetched:
            self.c.execute(self.diffs_INSERT, (filename, filemodtime, filesize))
            return False
        else:
            if fetched[1] == filename and fetched[2] == filemodtime and fetched[3] == filesize:
                return False
            self.c.execute(self.diffs_UPDATE, (filemodtime, filesize, filename))
        return True
    def initialize_db(self, dbfilename):  # PUBLIC
        self.dbfilename = dbfilename
        self.conn = sqlite3.connect(self.dbfilename)
        self.c = self.conn.cursor()
        self.c.execute(self.diffs_CREATE)
        self.c.execute(self.cacheptrs_CREATE)
        self.c.execute(self.cache_CREATE)
    def update_state(self, filenames):  # PUBLIC
        updated = False
        for filename in filenames:
            if self.update_diff(filename) == True:
                self.update_cache(filename)
                updated = True
        return updated
    def add_entry(self, filenames, gmlfile, sched, rsched):  # PUBLIC
        config = self.create_config_str(filenames)
        self.c.execute(self.cache_FIND, (config,))
        fetched = self.c.fetchone()
        if fetched:
            return False
        self.c.execute(self.cache_INSERT, (config, gmlfile, sched, rsched))
        for filename in filenames:
            self.c.execute(self.cacheptrs_INSERT, (filename, config))
        return True
    def get_entry(self, filenames):  # PUBLIC
        config = self.create_config_str(filenames)
        self.c.execute(self.cache_FIND, (config,))
        fetched = self.c.fetchone()
        if not fetched:
            return "", "", ""
        return fetched[2], fetched[3], fetched[4]
    def uninitialize_db(self):  # PUBLIC
        self.conn.commit()
        self.conn.close()

