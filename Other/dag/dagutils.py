#!/usr/bin/env python

import os

class DagUtils:
    def save_list_to_file(self, filename, savelist):
        with open(filename, 'w') as f:
            for save in savelist:
                f.write(save + '\n')
    def read_list_from_file(self, filename):
        readlist = []
        with open(filename, 'r') as f:
            while True:
                line = f.readline()
                if not line:
                    break
                readlist.append(line.rstrip('\n'))
        return readlist
    def read_from_file(self, filename):
        with open(filename, 'rb') as f:
            content = f.read()
            return content
    def write_to_file(self, filename, content):
        with open(filename, 'wb') as f:
            f.write(content)
    def get_argv_filenames(self, argv, directory):  # PUBLIC
        filenames = argv
        filenames.pop(0)
        for i,s in enumerate(filenames):
            filenames[i] = directory + '/' + s
        return filenames
    def get_filenames(self, extension, directory):  # PUBLIC
        yamlfilenames = []
        for root,dirs,files in os.walk(directory):
            for file in files:
                if file.endswith(extension):
                    yamlfilenames.append(os.path.join(root, file))
        return yamlfilenames

