#!/usr/bin/env python

from random import randrange
from dagutils import *
from depgraph import *


assetdir = 'assets'
filenames = []
dagutils = DagUtils()
depgraph = DepGraph()
depgraph.initialize(assetdir)
filenames = dagutils.get_argv_filenames(sys.argv, assetdir)
if not filenames:
    filenames = dagutils.get_filenames('.yaml', assetdir)
depgraph.read_config_files(filenames)


isDebug = False

traverselist = []
def complete_init():
    global traverselist
    traverselist = depgraph.create_traverse_list()
    print(traverselist)
    print("")
def complete_init_reverse():
    global traverselist
    traverselist = depgraph.create_traverse_list_reverse()
    print(traverselist)
    print("")
def complete_node():
    global traverselist
    randnum = randrange(len(traverselist))
    node = traverselist[randnum]
    print("Remove " + node)
    traverselist = depgraph.complete_traverse_node(node, traverselist, isDebug)
    print(traverselist)
    print("")
def complete_node_reverse():
    global traverselist
    randnum = randrange(len(traverselist))
    node = traverselist[randnum]
    print("Remove " + node)
    traverselist = depgraph.complete_traverse_node_reverse(node, traverselist, isDebug)
    print(traverselist)
    print("")

complete_init_reverse()
while len(traverselist) > 0:
    complete_node_reverse()

depgraph.uninitialize()

