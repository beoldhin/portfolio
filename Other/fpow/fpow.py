#!/usr/bin/env python
# This script is for the application "4 pics 1 word" where the user
# has to "guess" a word explaining the 4 pictures".
# The "4 pics 1 one" application randomizes the 12 characters to select
# letters from each time the application is launched. This creates the
# possibility of comparing the occurrences of characters so that if one
# character does *not* anymore occur in the list, it is certainly not the
# character used in the solution.
# Example (word was "SUIT"):
# ./fpow.py XCJLXZGUSTIX TWSOUJCRIFEE 
# Occurrences:
# [0, 0, 2, 0, 1, 1, 1, 0, 2, 2, 0, 1, 0, 0, 1, 0, 0, 1, 2, 2, 2, 0, 1, 1, 0, 1]
# Result characters:
# CIJSTU
# ./fpow.py XCJLXZGUSTIX TWSOUJCRIFEE OCQICTUSDQWB
# Occurrences:
# [0, 1, 3, 1, 1, 1, 1, 0, 3, 2, 0, 1, 0, 0, 2, 0, 1, 1, 3, 3, 3, 0, 2, 1, 0, 1]
# Result characters:
# CISTU
# ./fpow.py XCJLXZGUSTIX TWSOUJCRIFEE OCQICTUSDQWB TUHPQVSNBINB
# Occurrences:
# [0, 2, 3, 1, 1, 1, 1, 1, 4, 2, 0, 1, 0, 1, 2, 1, 2, 1, 4, 4, 4, 1, 2, 1, 0, 1]
# Result characters:
# ISTU
import sys

chrMatrix = []
matrixLen = ord('Z') - ord('A') + 1
for i in range(matrixLen):
    chrMatrix.append(0)

def add_string_to_list(inputStr):
    upperStr = inputStr.upper()
    upperStr = sorted(upperStr)
    currentChr = chr(0)
    for i in range(len(upperStr)):
        if upperStr[i] == currentChr:
            continue
        pos = ord(upperStr[i]) - ord('A')
        chrMatrix[pos] += 1
        currentChr = upperStr[i]

for i in range(1, len(sys.argv)):
    if len(sys.argv[i]) != 12:
        print("ERROR: Invalid number of characters")
        sys.exit()
    add_string_to_list(sys.argv[i])

resultStr = ""
inputLimit = len(sys.argv) - 1
for i in range(len(chrMatrix)):
    if chrMatrix[i] == inputLimit:
        resultStr += chr(i + ord('A'))

print("Occurrences:")
print(chrMatrix)
print("Result characters:")
print(resultStr)

