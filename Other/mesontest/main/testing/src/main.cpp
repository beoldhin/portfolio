#define CATCH_CONFIG_RUNNER
#include "../../../tools/Catch/include/catch.hpp"
#include "../../../library/include/myobject.h"

MyObject *myobject;

int main( int argc, char** argv )
{
  myobject = new MyObject();

  int result = Catch::Session().run(argc, argv);

  delete myobject;

  return (result < 0xff ? result : 0xff);
}

TEST_CASE( "Factorials are computed", "[factorial]" ) {
  REQUIRE( myobject->someTest(1) == 5 );
  REQUIRE( myobject->someTest(13) == 27 );
  REQUIRE( myobject->someTest(100) == -1 );
}

