#include <iostream>
#include "../../../library/include/myobject.h"

using namespace std;

int main(int argc, char **argv) {

    MyObject* myobject = new MyObject();
    
    myobject->someTest(10);
    cout << "Testing!" << endl;
    
    delete myobject;
    return 0;
}

