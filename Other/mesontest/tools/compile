#!/usr/bin/env bash

cd ..
base_root=`pwd`

library_development_dirs=("$base_root/library/build/development" "$base_root/library/src")
library_production_dirs=("$base_root/library/build/production" "$base_root/library/src")
library_testing_dirs=("$base_root/library/build/testing" "$base_root/library/src")

executable_development_dirs=("$base_root/main/development/build" "$base_root/main/development/src")
executable_production_dirs=("$base_root/main/production/build" "$base_root/main/production/src")
executable_testing_dirs=("$base_root/main/testing/build" "$base_root/main/testing/src")

# Configuration for development (debugging, no optimizations etc.)
development_options="--buildtype debug --warnlevel 1"

# Configuration for production (no debug, optimization etc.)
production_options="--buildtype release"

# Configuration for testing (unit tests, coverage, etc.)
testing_options="--buildtype debug -Db_coverage=true"

meson_init() {
  builddir=$1
  srcdir=$2
  options=$3
  rm -rf $builddir && mkdir $builddir
  cd $srcdir
  meson $options $builddir
  cd $builddir
  ninja
}

if [ $# -eq 0 ]; then
  meson_init ${library_development_dirs[@]} "$development_options"
  meson_init ${executable_development_dirs[@]} "$development_options"
elif [ $# -eq 1 ]; then
  if [ $1 == "prod" ]; then
    meson_init ${library_production_dirs[@]} "$production_options"
    meson_init ${executable_production_dirs[@]} "$production_options"
  elif [ $1 == "test" ]; then
    meson_init ${library_testing_dirs[@]} "$testing_options"
    meson_init ${executable_testing_dirs[@]} "$testing_options"
  else
    echo "Error: Unknown parameter name."
  fi
else
  echo "Error: Unknown number of parameters."
fi

