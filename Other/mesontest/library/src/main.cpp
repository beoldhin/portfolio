#include <iostream>
#include "../include/myobject.h"

int main(int argc, char **argv) {

    MyObject* myobject = new MyObject();
    
    myobject->someTest(10);
    
    delete myobject;
    return 0;
}

