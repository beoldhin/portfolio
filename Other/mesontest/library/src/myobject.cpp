#include "../include/myobject.h"

MyObject::MyObject() {
}

MyObject::~MyObject() {
}

int MyObject::someTest(int someArg) {
    if (someArg == 1) {
        return 5;
    }
    if (someArg == 13) {
        return 27;
    }
    return -1;
}

