#define CATCH_CONFIG_RUNNER
#include "../../tools/Catch/include/catch.hpp"
#include "../../include/myobject.h"

int main( int argc, char* argv[] )
{
  // global setup...

  int result = Catch::Session().run(argc, argv);

  // global clean-up...

  return (result < 0xff ? result : 0xff);
}

