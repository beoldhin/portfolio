#!/usr/bin/env python

from namegetter import *

import os
import yaml

class SshKeyChecker:
    namegetter = None
    sshkeyfilepairs = []
    fromtodef = '{}@{} -> {}@{}'
    def __init__(self):
        self.namegetter = NameGetter()
    def collect_yaml_filenames(self, wantednametype):
        yamlfilenames = []
        if wantednametype == 'privatekeys':
            sshkeydir = '/etc/puppetlabs/code/environments/production/hieradata/sshkeys/privatekeys'
            extension = '.eyaml'
        else:
            sshkeydir = '/etc/puppetlabs/code/environments/production/hieradata/sshkeys/publickeys'
            extension = '.yaml'
        for root,dirs,files in os.walk(sshkeydir):
            for file in files:
                if file.endswith(extension):
                    yamlfilenames.append(os.path.join(root, file))
        return yamlfilenames
    def collect_ssh_keynames(self, wantednametype):
        listitems = []
        yamlfilenames = self.collect_yaml_filenames(wantednametype)
        for yamlfilename in yamlfilenames:
            with open(yamlfilename, 'r') as f:
                documents = yaml.load_all(f)
                for document in documents:
                    for outerleftdef,outerrightdef in document.items():
                        outernames = outerleftdef.split('-')
                        nametype = outernames[0]
                        direction = outernames[1]
                        fromuser = ""
                        fromname = ""
                        touser = ""
                        toname = ""
                        if direction == 'from':
                            fromname = outernames[2]
                            fromuser = outernames[3]
                        else:
                            toname = outernames[2]
                            touser = outernames[3]
                        for innerleftdef,innerrightdef in outerrightdef.items():
                            innernames = innerleftdef.split('-')
                            direction = innernames[0]
                            if direction == 'from':
                                fromname = innernames[1]
                                if len(innernames) == 3:
                                    fromuser = innernames[2]
                                else:
                                    fromuser = outernames[3]
                            else:
                                toname = innernames[1]
                                if len(innernames) == 3:
                                    touser = innernames[2]
                                else:
                                    touser = outernames[3]
                            appenditems = [fromuser, fromname, touser, toname]
                            for item in listitems:
                                if item == appenditems:
                                    fromto = self.fromtodef.format(fromuser, fromname, touser, toname)
                                    print("ERROR: Duplicate keys in ssh keyfiles! ({})".format(fromto))
                            listitems.append(appenditems)
        return listitems
    def collect_namegetter_names(self):
        listitems = []
        yitems = len(self.namegetter.fromto_names)
        for j in range(yitems):
            fromname = self.namegetter.fromto_names[j][0]
            fromname, fromuser = self.namegetter.get_userandhost(fromname)
            if not self.namegetter.is_external_name(fromname):
                xitems = len(self.namegetter.fromto_names[j])
                for i in range(1, xitems):
                    toname = self.namegetter.fromto_names[j][i]
                    toname, touser = self.namegetter.get_userandhost(toname)
                    if not touser:
                        touser = fromuser
                    fromname = fromname.split('-')[0]
                    fromname = self.namegetter.get_machinename(fromname)
                    toname = toname.split('-')[0]
                    toname = self.namegetter.get_machinename(toname)
                    appenditems = [fromuser, fromname, touser, toname]
                    for item in listitems:
                        if item == appenditems:
                            fromto = self.fromtodef.format(fromuser, fromname, touser, toname)
                            print("ERROR: Duplicate names in namegetter! ({})".format(fromto))
                    listitems.append(appenditems)
        return listitems
    def compare_lists(self, firstlist, secondlist):
        yitems = len(firstlist)
        for j in range(yitems):
            firstlistitems = firstlist[j]
            fromuser, fromname, touser, toname = firstlistitems
            fromto = self.fromtodef.format(fromuser, fromname, touser, toname)
            if firstlistitems in secondlist:
                print("  Match: {}".format(fromto))
            else:
                print("  ERROR: {}".format(fromto))

sshkeychecker = SshKeyChecker()
privatekeys = sshkeychecker.collect_ssh_keynames('privatekeys')
publickeys = sshkeychecker.collect_ssh_keynames('publickeys')
namegetternames = sshkeychecker.collect_namegetter_names())
print("Private key list length is {}".format(len(privatekeys)))
print("Public key list length is {}".format(len(publickeys)))
print("Namegetter list length is {}".format(len(namegetternames)))
print("Privatekeys in publickeys:")
sshkeychecker.compare_lists(privatekeys, publickeys)
print("Publickeys in privatekeys:")
sshkeychecker.compare_lists(publickeys, privatekeys)
print("Namegetternames in privatekeys:")
sshkeychecker.compare_lists(namegetternames, privatekeys)
print("Namegetternames in publickeys:")
sshkeychecker.compare_lists(namegetternames, publickeys)

