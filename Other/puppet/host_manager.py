#!/usr/bin/env python

import re
import os
import sys
from plumbum.path.utils import delete

hostpath = '/etc/puppetlabs/code/modules'

def print_usage_string():
    prgname = sys.argv[0]
    print('Usage: {} <add|remove> <name>'.format(prgname))
    print('Usage: {} <rename> <oldname> <newname>'.format(prgname))
    sys.exit(1)

class HostManager:
    filepath = ''
    filepathlen = 0
    foundlist = []
    def __init__(self, filepath):
        self.filepath = filepath
        self.filepathlen = len(filepath.split(os.sep))
    def is_correctpath(self, filepath):
        filepath = filepath.split(os.sep)
        filepathlen = len(filepath)
        if filepathlen >= self.filepathlen + 1:
            pathtocheck = filepath[self.filepathlen]
            if pathtocheck == 'vcsrepo':
                return False
        return True
    def collect_filenames(self):
        for root, subdirs, filenames in os.walk(self.filepath, False):
            for filename in filenames:
                filename = os.path.join(root, filename)
                if self.is_correctpath(filename):
                    self.foundlist.append([filename, 'file'])
            if self.is_correctpath(root):
                self.foundlist.append([root, 'directory'])
    def host_exists(self, entry, hostname):
        filepath = entry[0]
        entrytype = entry[1]
        filepathsplit = filepath.split(os.sep)
        if entrytype == 'file':
            filename = os.path.basename(filepath)
            filename = filename.replace('.', '-')  # definition can end to '.' so split it also
            filename = filename.split('-')
            for definition in filename:
                if definition == hostname:
                    return True
            del filepathsplit[-1]  # delete filename from path and continue
        # If directory or (file and no hostname in filename):
        for definition in filepathsplit:
            if definition == hostname:
                return True
        return False
    def add_host(self, hostname):  # PUBLIC
        pass
    def remove_host(self, hostname):  # PUBLIC
        self.collect_filenames()
        for entry in self.foundlist:
            if self.host_exists(entry, hostname):
                filepath = entry[0]
                entrytype = entry[1]
                if entrytype == 'file':
                    print('Delete file: {}'.format(filepath))
                else:  # directory
                    print('Delete directory: {}'.format(filepath))
                delete(filepath)
    def rename_host(self, oldname, newname):  # PUBLIC
        self.collect_filenames()
        for entry in self.foundlist:
            if self.host_exists(entry, oldname):
                # TODO: Remove entries with type "file" and no "oldname" found from the
                # filename part. This will leave only directories with "oldname" AND
                # filenames with the filename part matching the "oldname".
                fromfilepath = entry[0]
                tofilepath = re.sub(r"\b%s\b" % oldname, newname, fromfilepath)
                print(tofilepath)

hostmanager = HostManager(hostpath)

arguments = sys.argv[1:]
if len(arguments) < 2:
    print_usage_string()
if len(arguments) == 2:
    hostname = arguments[1]
    if arguments[0] == 'add':
        hostmanager.add_host(hostname)
    elif arguments[0] == 'remove':
        hostmanager.remove_host(hostname)
    else:
        print_usage_string()
elif len(arguments) == 3:
    oldname = arguments[1]
    newname = arguments[2]
    if arguments[0] == 'rename':
        hostmanager.rename_host(oldname, newname)
    else:
        print_usage_string()
else:
    print_usage_string()

