#!/usr/bin/env python

from createsshconfigs import CreateSshConfigs

sshpath = '/etc/puppet/files/tests/sshconfigs'
sshconfigs = CreateSshConfigs(sshpath)
sshconfigs.initialize()
sshconfigs.append_definitions()

