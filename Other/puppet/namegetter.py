#!/usr/bin/env python

class NameGetter:
    localhost_name = '127.0.1.1'
    domain_name = 'gondor.org'
    machine_names = [
            ['elrond',  'beril', 'azog'],
            ['gandalf', 'aldor'],
            ['shelob',  'fror'],
            ['gimli']]
    fqdn_names = [
            ['github.com', 'github'],
            ['feonar.dy.fi', 'feonar']]
    ext_names = ['feonar', 'nexus7', 'github']
    forward_names = [
            ['20000 127.0.0.1:80', 'feonar'],
            ['9001 127.0.0.1:9001', 'fror']]
    ext_to_int = [
            [ 'fror', 'beoldhin.ddns.net' ],
            [ 'beril', 'beoldhin.ddns.net' ]]
    fromto_names = [
            ['root@azog',      'quetzal@azog',
                               'git@azog' ],
            ['quetzal@azog',   'git@azog',
                               'git@fror',
                               'aldor',
                               'gimli',
                               'fror',
                               'feonar',
                               'github' ],
            ['root@gimli',     'quetzal@gimli',
                               'git@gimli' ],
            ['quetzal@gimli',  'git@gimli',
                               'git@fror',
                               'azog',
                               'fror',
                               'github' ],
            ['root@aldor',     'quetzal@aldor',
                               'quetzal@azog' ],
            ['quetzal@aldor',  'azog-NK',
                               'github' ],
            ['root@fror',      'quetzal@fror',
                               'git@fror' ],
            ['quetzal@fror',   'git@fror',
                               'github' ],
            ['quetzal@nexus7', 'azog',
                               'aldor',
                               'fror' ]]
    dest_users = [  # TODO: REMOVE THIS!
            ['quetzal@feonar', 'olterman'],
            ['quetzal@github', 'git']]
    bashrc_names = [
            ['leela@aldor', '256color',
                            'powerline',
                            'vimgpg',
                            'vimedit',
                            'python',
                            'xoff'],
            ['quetzal@aldor', '256color',
                              'powerline',
                              'vimgpg',
                              'vimedit',
                              'python',
                              'xoff'],
            ['root@azog',     ''],  ## use only base file
            ['quetzal@azog',  '256color',
                              'powerline-arch',
                              'browser',
                              'vimgpg',
                              'vimedit',
                              'python',
                              'xoff',
                              'android'],
            ['quetzal@gimli', '256color',
                              'powerline-arch',
                              'browser',
                              'vimgpg',
                              'vimedit',
                              'python',
                              'xoff'],
            ['quetzal@fror',  '256color',
                              'vimedit',
                              'python']]
    profile_names = [
            ['root@azog',     'gem-arch'],
            ['quetzal@azog',  'gem-arch'],
            ['quetzal@gimli', 'gem-arch']]
    bashalias_names = [
            ['leela@aldor', 'common',
                            'common-iftop-wlan0',
                            'shutreboot',
                            'gnome',
                            'ssd',
                            'temp',
                            'trans',
                            'updates',
                            'weather'],
            ['quetzal@aldor', 'common',
                              'common-iftop-wlan0',
                              'shutreboot',
                              'gnome',
                              'puppet',
                              'puppetclient',
                              'ssd',
                              'temp-aldor',
                              'tmux',
                              'trans',
                              'updates',
                              'weather'],
            ['quetzal@gimli', 'bt',
                              'common',
                              'common-iftop-wlan0',
                              'shutreboot-arch',
                              'gnome',
                              'gpg',
                              'latex',
                              'math-arch',
                              'puppet',
                              'puppetclient-arch',
                              'ssd',
                              'temp-gimli',
                              'tmux',
                              'trans',
                              'updates-arch',
                              'webdev',
                              'weather-arch'],
            ['quetzal@azog',  'angband',
                              'backup-arch',
                              'bt',
                              'common',
                              'common-iftop-eth0',
                              'shutreboot-arch',
                              'gnome',
                              'gpg',
                              'latex',
                              'math-arch',
                              'postfix',
                              'puppet',
                              'puppetclient-arch',
                              'puppetserver-arch',
                              'ssd',
                              'temp-azog',
                              'tmux',
                              'trans',
                              'updates-arch',
                              'webdev',
                              'weather-arch',
                              'java-arch'],
            ['quetzal@fror',  'common',
                              'common-iftop-eth0',
                              'shutreboot',
                              'puppet',
                              'puppetclient',
                              'ssd',
                              'temp-fror',
                              'tmux',
                              'updates-nograph']]
    to_names_set = set()
    to_names_list = []
    to_names_noext_set = set()
    to_names_noext_list = []
    def __init__(self):
        self.create_tonames()
    def is_external_name(self, name):
        if name in self.ext_names:
            return True
        return False
    def get_destuser(self, name):
        yitems = len(self.dest_users)
        for j in range(yitems):
            if self.dest_users[j][0] == name:
                return self.dest_users[j][1]
        return name.split('@')[0]
    def get_userandhost(self, definition):
        splitstr = definition.split('@')
        if len(splitstr) == 2:
            return splitstr[1], splitstr[0]
        return definition, None
    def get_toname(self, definition):
        splitstr = definition.split('-')
        if len(splitstr) == 2:
            if splitstr[1] == 'NK':
                return splitstr[0], False
        return definition, True
    def create_tonames(self):
        yitems = len(self.fromto_names)
        for j in range(yitems):
            xitems = len(self.fromto_names[j])
            if xitems < 2:
                raise ValueError('Too few from-to names')
            fromname = self.fromto_names[j][0]
            fromname, user = self.get_userandhost(fromname)
            for i in range(1, xitems):
                toname = self.fromto_names[j][i]
                nametoadd = '{}@{}'.format(user, toname)
                if nametoadd not in self.to_names_set:
                    self.to_names_list.append(nametoadd)
                    self.to_names_set.add(nametoadd)
                if not self.is_external_name(toname):
                    if nametoadd not in self.to_names_noext_set:
                        self.to_names_noext_list.append(nametoadd)
                        self.to_names_noext_set.add(nametoadd)
    def get_fromnames(self, toname, touser):
        fromnames = []
        yitems = len(self.fromto_names)
        for j in range(yitems):
            xitems = len(self.fromto_names[j])
            if xitems < 2:
                raise ValueError('Too few from-to names')
            fromname = self.fromto_names[j][0]
            fromhost, fromuser = self.get_userandhost(fromname)
            if fromuser == touser:
                for i in range(1, xitems):
                    if self.fromto_names[j][i] == toname:
                        fromname = fromname.split('@')[1]
                        fromnames.append(fromname)
        return fromnames
    def get_latest_machinename(self, name):
        yitems = len(self.machine_names)
        for j in range(yitems):
            if self.machine_names[j][0] == name:
                return self.machine_names[j][-1]
        return name
    def get_machinename(self, name):
        yitems = len(self.machine_names)
        for j in range(yitems):
            xitems = len(self.machine_names[j])
            for i in range(xitems):
                if name == self.machine_names[j][i]:
                    return self.machine_names[j][0]
        if not self.is_external_name(name):
            errortext = 'Invalid machine name "{}"'.format(name)
            raise ValueError(errortext)
        return name
    def get_fqdn(self, name):
        yitems = len(self.fqdn_names)
        for j in range(yitems):
            xitems = len(self.fqdn_names[j])
            for i in range(xitems):
                if name == self.fqdn_names[j][i]:
                    return self.fqdn_names[j][0]
        return name
    def get_forward(self, name):
        yitems = len(self.forward_names)
        for j in range(yitems):
            xitems = len(self.forward_names[j])
            for i in range(xitems):
                if name == self.forward_names[j][i]:
                    return self.forward_names[j][0]
        return None
    def get_extname(self, name):
        yitems = len(self.ext_to_int)
        for j in range(yitems):
            if name == self.ext_to_int[j][0]:
                return self.ext_to_int[j][1]
        return None

