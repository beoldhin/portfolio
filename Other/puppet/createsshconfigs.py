#!/usr/bin/env python

from plumbum import local
from plumbum.cmd import echo, touch

from namegetter import *

sshpath = '/etc/puppetlabs/code/modules/opensshclient/files/machines'

class CreateSshConfigs:
    user = ''
    namegetter = None
    echofilename = ''
    destfilepath = ''
    firstdef = (
        "# Generated file, DO NOT EDIT\n"
        "Host *\n"
        "  IdentitiesOnly yes\n"
        "  SendEnv LANG LC_* TERM\n")
    hostdef = (
        "Host {}\n"
        "  HostName {}\n"
        "  User {}\n"
        "  IdentityFile {}")
    hostdef_newuser = (
        "Host {}-{}\n"
        "  Hostname {}\n"
        "  User {}\n"
        "  IdentityFile {}")
    githubdef = (
        "  UserKnownHostsFile=/dev/null\n"
        "  StrictHostKeyChecking no\n"
        "  LogLevel ERROR")
    forwarddef = "  LocalForward {}"
    def __init__(self, destfilepath):
        print('Initializing CreateSshConfigs...')
        self.namegetter = NameGetter()
        self.destfilepath = destfilepath
    def create_filename(self, fromname, fromuser):
        return '{}/{}/users/{}/config'.format(self.destfilepath, fromname, fromuser)
    def create_identityfilename(self, fromname, toname, fromuser, touser):
        if fromuser != 'root':
            if fromuser == touser:
                return '/home/{}/.ssh/id_rsa_{}_to_{}'.format(fromuser, fromname, toname)
            return '/home/{}/.ssh/id_rsa_{}_to_{}_{}'.format(fromuser, fromname, toname, touser)
        if fromuser == touser:
            return '/root/.ssh/id_rsa_{}_to_{}'.format(fromname, toname)
        return '/root/.ssh/id_rsa_{}_to_{}_{}'.format(fromname, toname, touser)
    def echo_line(self, line):
        (echo[line] >> self.echofilename)()
    def reset_file(self, destfilename):
        print('Creating file {}...'.format(destfilename))
        p = local.path(destfilename)
        if p.exists():
            p.delete()
        touch(destfilename)
        p.chown('root', 'puppet')
        p.chmod(0o640)
    def set_first_content(self, destfilename):
        self.reset_file(destfilename)
        self.echofilename = destfilename
        self.echo_line(self.firstdef)
    def append_one_external(self, params, fromname, toname, newuser):
        if fromname != toname:
            extname = self.namegetter.get_extname(params[0])
            if extname:
                params[0] = '{}-ext'.format(params[0])
                if newuser:
                    params[2] = extname
                    self.echo_line(self.hostdef_newuser.format(*params))
                    self.append_definition_end(fromname, toname, True)
                else:
                    params[1] = extname
                    self.echo_line(self.hostdef.format(*params))
                    self.append_definition_end(fromname, toname, False)
    def append_definition_end(self, fromname, toname, newuser):
        if toname == 'github':
            self.echo_line(self.githubdef)
        if toname != fromname and not newuser:
            forward_name = self.namegetter.get_forward(toname)
            if forward_name:
                self.echo_line(self.forwarddef.format(forward_name))
        self.echo_line('')
    def append_one_definition(self, fromname, fromuser, toname, touser):
        # print(">> {}@{} -> {}@{}".format(fromuser, fromname, touser, toname))
        toname, haskey = self.namegetter.get_toname(toname)
        destfilename = self.create_filename(fromname, fromuser)
        # print(">> {}".format(destfilename))
        origfromname = self.namegetter.get_machinename(fromname)
        origtoname = self.namegetter.get_machinename(toname)
        generated_touser = False
        if not touser:  # Get touser with fromuser if touser not defined
            identityfilename = self.create_identityfilename(origfromname, origtoname, fromuser, fromuser)
            definition = '{}@{}'.format(fromuser, toname)
            touser = self.namegetter.get_destuser(definition)
            generated_touser = True
        else:
            identityfilename = self.create_identityfilename(origfromname, origtoname, fromuser, touser)
        fqdnname = self.namegetter.get_fqdn(toname)
        self.echofilename = destfilename
        if fromuser == touser:
            params = [toname, fqdnname, touser, identityfilename]
            self.echo_line(self.hostdef.format(*params))
            self.append_definition_end(fromname, toname, False)
            self.append_one_external(params, fromname, toname, False)
        else:
            if not generated_touser:
                params = [toname, touser, fqdnname, touser, identityfilename, touser]
                self.echo_line(self.hostdef_newuser.format(*params))
                self.append_definition_end(fromname, toname, True)
                self.append_one_external(params, fromname, toname, True)
            else:  # touser generated so it is the only user for that host: do not show "-<user>"
                params = [toname, fqdnname, touser, identityfilename]
                self.echo_line(self.hostdef.format(*params))
                self.append_definition_end(fromname, toname, False)
                self.append_one_external(params, fromname, toname, False)
    def initialize(self):  # PUBLIC
        yitems = len(self.namegetter.fromto_names)
        for j in range(yitems):
            fromname = self.namegetter.fromto_names[j][0]
            fromname, fromuser = self.namegetter.get_userandhost(fromname)
            if not self.namegetter.is_external_name(fromname):
                filename = self.create_filename(fromname, fromuser)
                self.set_first_content(filename)
    def append_definitions(self):  # PUBLIC
        yitems = len(self.namegetter.fromto_names)
        for j in range(yitems):
            fromname = self.namegetter.fromto_names[j][0]
            fromname, fromuser = self.namegetter.get_userandhost(fromname)
            if not self.namegetter.is_external_name(fromname):
                xitems = len(self.namegetter.fromto_names[j])
                for i in range(1, xitems):
                    toname = self.namegetter.fromto_names[j][i]
                    toname, touser = self.namegetter.get_userandhost(toname)
                    self.append_one_definition(fromname, fromuser, toname, touser)

if __name__ == "__main__":
    sshconfigs = CreateSshConfigs(sshpath)
    sshconfigs.initialize()
    sshconfigs.append_definitions()

