#!/usr/bin/env python

from createbasepuppetconfigs import CreateBasePuppetConfigs

basepuppetpath = '/etc/puppet/files/tests/basepuppetconfigs'
basepuppetconfigs = CreateBasePuppetConfigs(basepuppetpath)
basepuppetconfigs.initialize()
basepuppetconfigs.append_definitions()

