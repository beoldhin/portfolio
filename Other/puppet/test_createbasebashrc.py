#!/usr/bin/env python

from createbasebashrc import CreateBaseBashRc

basepath = '/etc/puppet/files/tests/bashrc'
basebashrc = CreateBaseBashRc(basepath)
basebashrc.initialize()
basebashrc.append_definitions()

