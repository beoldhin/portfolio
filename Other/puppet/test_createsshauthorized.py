#!/usr/bin/env python

from createsshauthorized import CreateSshAuthorized

sshpath = '/etc/puppet/files/tests/authorizedkeys'
createsshauthorized = CreateSshAuthorized(sshpath)
createsshauthorized.process_publickeys()

