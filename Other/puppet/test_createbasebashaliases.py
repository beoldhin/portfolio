#!/usr/bin/env python

from createbasebashaliases import CreateBaseBashAliases

basepath = '/etc/puppet/files/tests/bashaliases'
basebashaliases = CreateBaseBashAliases(basepath)
basebashaliases.initialize()
basebashaliases.append_definitions()

