#!/usr/bin/env python

from plumbum import local
from plumbum.cmd import sudo, touch, chown, chmod, cat

from namegetter import *

configpath = 'configs'
basepath = '/etc/puppetlabs/code/modules/base/files'

class CreateBaseBashAliases:
    namegetter = None
    configpath = ''
    destfilename = ''
    destfilepath = ''
    def __init__(self, configpath, destfilepath):
        print('Initializing CreateBaseBashAliases...')
        self.namegetter = NameGetter()
        self.configpath = configpath
        self.destfilepath = destfilepath
    def create_sourcefilename(self, definition):
        return '{}/bash_aliases-{}'.format(self.configpath, definition)
    def create_destfilename(self, machinename, user):
        return '{}/machines/{}/users/{}/bash_aliases'.format(self.destfilepath, machinename, user)
    def reset_file(self, destfilename):
        print('Creating file {}...'.format(destfilename))
        p = local.path(destfilename)
        if p.exists():
            p.delete()
        touch(destfilename)
        p.chown('root', 'root')
        p.chmod(0o644)
    def append_one_definition(self, definition, machinename, user):
        sourcefilename = self.create_sourcefilename(definition)
        destfilename = self.create_destfilename(machinename, user)
        (cat[sourcefilename] >> destfilename)()
    def initialize(self):  # PUBLIC
        yitems = len(self.namegetter.bashalias_names)
        for j in range(yitems):
            machinename = self.namegetter.bashalias_names[j][0]
            machinename, user = self.namegetter.get_userandhost(machinename)
            destfilename = self.create_destfilename(machinename, user)
            self.reset_file(destfilename)
    def append_definitions(self):  # PUBLIC
        yitems = len(self.namegetter.bashalias_names)
        for j in range(yitems):
            xitems = len(self.namegetter.bashalias_names[j])
            machinename = self.namegetter.bashalias_names[j][0]
            machinename, user = self.namegetter.get_userandhost(machinename)
            for i in range(1, xitems):
                definition = self.namegetter.bashalias_names[j][i]
                self.append_one_definition(definition, machinename, user)

if __name__ == "__main__":
    basebashaliases = CreateBaseBashAliases(configpath, basepath)
    basebashaliases.initialize()
    basebashaliases.append_definitions()

