class mono::machines::gimli::main ( $params = undef ) {
  mono::os::arch::main{ 'mono::gimli':
    params => $params
  }
}

