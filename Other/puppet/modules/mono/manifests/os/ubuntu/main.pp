define mono::os::ubuntu::main ( $params = undef ) {
  package { 'mono-complete':
    ensure => installed
  }
}

