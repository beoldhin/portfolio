define basessd::os::arch::user ( $user = undef, $params = under ) {
  $machinename = $params['machinename']
  $distribution = $params['distribution']
  $unixpath = "puppet:///modules/basessd/os/${distribution}"
  $machinepath = "puppet:///modules/basessd/machines/${machinename}"
  file { "asdconf-${machinename}":
    ensure  => present,
    require => User[$user],
    path    => "/etc/asd.conf",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${machinepath}/asd.conf"
  }
  # NOTE: The following file should go to
  # /etc/systemd/system/asd-resync.timer.d/frequency.conf
  file { "frequencyconf-${machinename}":
    ensure  => present,
    require => User[$user],
    path    => "/home/${user}/.frequency.conf",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/frequency.conf",
  }
}

