define basessd::os::arch::main ( $params = undef, $sizeopt = undef ) {
  $machinename = $params['machinename']
  $distribution = $params['distribution']
  $unixpath = "puppet:///modules/basessd/os/${distribution}"
  $machinepath = "puppet:///modules/basessd/machines/${machinename}"
  # Smartmontools for 'SSD_Life_Left'
  package{ 'smartmontools':
    ensure => installed
  }
  # Note: We are using "require" here to create order: bindfs requires the
  # this file's last entry as it needs to be the last entry
  # Modules file for "overlay" module
  file { "modules-${machinename}":
    ensure  => present,
    path    => '/etc/modules',
    replace => yes,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => "${unixpath}/modules",
  }
  # Sudoers file for "psd-overlay-helper" definition
  file { "sudoers-${machinename}":
    ensure  => present,
    path    => '/etc/sudoers',
    replace => yes,
    mode    => '0440',
    owner   => 'root',
    group   => 'root',
    source  => "${machinepath}/sudoers",
  }
  file { "journald-${machinename}":
    ensure  => present,
    path    => '/etc/systemd/journald.conf',
    replace => yes,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => "${unixpath}/journald.conf",
  }
  # NOTE!: The handling of tmpfs is currently managed automatically by systemd
  # DO NOT CREATE ANY SETTINGS IN FSTAB MANUALLY FOR TMPFS!
  # DO NOT MAKE ASD USE ANY OF THESE DIRECTORIES!
}

