define basessd::os::ubuntu::lowmem( $params = undef ) {
  # Use this for 4GB and lower mem
  augeas { 'sysctl.conf':
    context => '/files/etc/sysctl.conf',
    lens    => 'sysctl.lns',
    incl    => '/etc/sysctl.conf',
    changes => [
      'set vm.swappiness 1',
      'set vm.vfs_cache_pressure 50'
    ],
  }
}

