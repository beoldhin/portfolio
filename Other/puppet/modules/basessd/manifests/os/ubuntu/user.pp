define basessd::os::ubuntu::user ( $user = undef, $params = under ) {
  $machinename = $params['machinename']
  $machinepath = "puppet:///modules/basessd/machines/${machinename}"
  file { "asdconf-${machinename}":
    ensure  => present,
    require => User[$user],
    path    => "/home/${user}/.asd.conf",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${machinepath}/asd.conf"
  }
  file { "frequencyconf-${machinename}":
    ensure  => present,
    require => User[$user],
    path    => "/home/${user}/.frequency.conf",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${machinepath}/frequency.conf",
  }
}

