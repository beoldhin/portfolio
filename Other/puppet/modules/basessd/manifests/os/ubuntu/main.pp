define basessd::os::ubuntu::main ( $params = undef, $sizeopt = undef ) {
  $machinename = $params['machinename']
  $machinepath = "puppet:///modules/basessd/machines/${machinename}"
  # Smartmontools for 'SSD_Life_Left'
  package{ 'smartmontools':
    ensure => installed
  }
  # Note: We are using "require" here to create order: bindfs requires the
  # this file's last entry as it needs to be the last entry
  # Modules file for "overlay" module
  file { "modules-${machinename}":
    ensure  => present,
    path    => '/etc/modules',
    replace => yes,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => "${machinepath}/modules",
  }
  # Sudoers file for "psd-overlay-helper" definition
  file { "sudoers-${machinename}":
    ensure  => present,
    path    => '/etc/sudoers',
    replace => yes,
    mode    => '0440',
    owner   => 'root',
    group   => 'root',
    source  => "${machinepath}/sudoers",
  }
  # NOTE! Google stores things to /tmp and expects the settings to survive over reboots!
  # NOTE! Directory /var/log in /tmp causes many programs to stop working as they can't
  # create the default logging directories. However, at least systemd fails if moving
  # /var/log to anything-sync-daemon (asd), i.e. it can't handle that change. Conclusion:
  # disabling all tmpfs and low-evel asd services for now.

  # Old comments below:

  # # NOTE: Don't move the below definitions to asd.conf!
  # # NOTE: Some directories are needed before ASD loads.
  # # creating "too many symbolic links error".
  # # NOTE: /var/tmp is problematic as it should be in ASD
  # # but it is also "low level" service. Disabling for now.
  # mount { '/tmp':
  #   ensure  => present,
  #   device  => 'tmpfs',
  #   fstype  => 'tmpfs',
  #   options => "defaults,noatime,mode=1777${sizeopt}",
  #   atboot  => true,
  # }
  # mount { '/var/log':
  #   ensure  => present,
  #   require => Mount['/tmp'],
  #   device  => 'tmpfs',
  #   fstype  => 'tmpfs',
  #   options => "defaults,noatime,mode=0755${sizeopt}",
  #   atboot  => true,
  # }
  # # IMPORTANT NOTE:
  # # /var/run: There is already support by default in Ubuntu for tmpfs
  # # /var/tmp: This directory should preserve data after boot, switched to asd
  # # /var/spool: For cron and mails, should preserve after boot, too big for asd
  # # Also read: http://askubuntu.com/questions/57297/why-has-var-run-been-migrated-to-run
  # # Also read: https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard
  # # NOTE: Type "mount" to see the distributions tempfs mounts (/run).
}

