class basessd::machines::gimli::quetzal ( $user = undef, $params = under ) {
  basessd::os::arch::user{ 'basessd::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

