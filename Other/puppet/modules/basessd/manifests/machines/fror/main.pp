class basessd::machines::fror::main ( $params = undef ) {
  $sizeopt = ',size=100m'
  basessd::os::ubuntu::main{ 'basessd::fror':
    params  => $params,
    sizeopt => $sizeopt
  }
  basessd::os::ubuntu::lowmem{ 'basessd::lowmem':
    params => $params
  }
}

