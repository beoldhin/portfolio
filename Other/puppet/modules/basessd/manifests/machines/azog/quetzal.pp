class basessd::machines::azog::quetzal ( $user = undef, $params = under ) {
  basessd::os::arch::user{ 'basessd::azog::quetzal':
    user   => $user,
    params => $params
  }
}

