class git::machines::azog::quetzal ( $user = undef, $params = undef ) {
  git::os::arch::user{ 'git::azog::quetzal':
    user   => $user,
    params => $params
  }
}

