class git::machines::azog::main ( $params = undef ) {
  git::os::arch::main{ 'git::azog':
    params => $params
  }
}

