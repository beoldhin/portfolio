class git::machines::haldar::quetzal ( $user = undef, $params = undef ) {
  git::os::ubuntu::user{ 'git::haldar::quetzal':
    user   => $user,
    params => $params
  }
}

