class git::machines::fror::quetzal ( $user = undef, $params = undef ) {
  git::os::ubuntu::user{ 'git::fror::quetzal':
    user   => $user,
    params => $params
  }
}

