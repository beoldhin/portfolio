class git::machines::gimli::main ( $params = undef ) {
  git::os::arch::main{ 'git::gimli':
    params => $params
  }
}

