class git::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  git::os::arch::user{ 'git::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

