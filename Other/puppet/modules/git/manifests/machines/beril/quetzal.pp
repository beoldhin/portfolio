class git::machines::beril::quetzal ( $user = undef, $params = undef ) {
  git::os::ubuntu::user{ 'git::beril::quetzal':
    user   => $user,
    params => $params
  }
}

