class git::machines::aldor::quetzal ( $user = undef, $params = undef ) {
  git::os::ubuntu::user{ 'git::aldor::quetzal':
    user   => $user,
    params => $params
  }
}

