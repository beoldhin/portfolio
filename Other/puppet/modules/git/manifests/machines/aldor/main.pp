class git::machines::aldor::main ( $params = undef ) {
  git::os::ubuntu::main{ 'git::aldor':
    params => $params
  }
}

