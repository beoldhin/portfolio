define git::os::ubuntu::main ( $params = undef ) {
  $origmachine = $params['origmachine']
  package { 'git':
    ensure => installed
  }
  package { 'git-gui':
    ensure  => installed,
    require => Package['git'],
  }
  package { 'tig':
    ensure  => installed,
    require => Package['git'],
  }
  package { 'gitg':
    ensure  => installed,
    require => Package['git'],
  }
  package { 'git-cola':
    ensure  => installed,
    require => Package['git'],
  }
}

