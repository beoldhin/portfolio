define git::os::arch::user ( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $userpath = "puppet:///modules/git/users/${user}"
  $settings = hiera("git-${user}")
  file { "git-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['git'],
    ],
    path    => "${homepath}/.gitconfig",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    content => epp('git/gitconfig.epp', {
      'name'  => $settings['name'],
      'email' => $settings['email'] }
    )
  }
  file { "tig-config-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      File["config-dir-${user}"],
    ],
    path    => "${homepath}/.config/tig",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "tigrc-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['tig'],
      File["tig-config-dir-${user}"],
    ],
    path    => "${homepath}/.config/tig/config",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/config",
  }
}

