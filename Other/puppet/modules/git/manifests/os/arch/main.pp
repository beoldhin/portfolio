define git::os::arch::main ( $params = undef ) {
  $origmachine = $params['origmachine']
  package { 'git':
    ensure => installed
  }
  package { 'tig':
    ensure  => installed,
    require => Package['git'],
  }
  package { 'gitg':
    ensure  => installed,
    require => Package['git'],
  }
}

