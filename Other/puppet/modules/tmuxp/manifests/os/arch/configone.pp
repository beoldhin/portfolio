define tmuxp::os::arch::configone(
$user = undef,
$project = undef,
$projectpath = undef,
$windowlayout = undef ) {
  $homepath = "/home/${user}"
  file { "${project}.yml-${user}":
    ensure  => present,
    require => [
      User[$user],
#      Package['tmuxp'],
      File["tmuxp-dir-${user}"]
    ],
    path    => "${homepath}/.tmuxp/${project}.yml",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    content => epp("tmuxp/${project}.yml.epp", {
      'projectpath'  => $projectpath,
      'windowlayout' => $windowlayout }
    )
  }
}

