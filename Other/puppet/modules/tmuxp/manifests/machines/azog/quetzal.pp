class tmuxp::machines::azog::quetzal ( $user = undef, $params = undef ) {
  tmuxp::os::arch::user{ 'tmuxp::azog::quetzal':
    user   => $user,
    params => $params
  }
}

