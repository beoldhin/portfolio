class tmuxp::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  tmuxp::os::arch::user{ 'tmuxp::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

