class tmuxinator::machines::beril::quetzal ( $user = undef, $params = undef ) {
  tmuxinator::os::ubuntu::user{ 'tmuxinator::beril::quetzal':
    user   => $user,
    params => $params,
  }
}

