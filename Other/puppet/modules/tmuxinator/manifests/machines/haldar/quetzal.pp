class tmuxinator::machines::haldar::quetzal ( $user = undef, $params = undef ) {
  tmuxinator::os::ubuntu::user{ 'tmuxinator::haldar::quetzal':
    user   => $user,
    params => $params,
  }
}

