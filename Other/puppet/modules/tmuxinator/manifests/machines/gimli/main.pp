class tmuxinator::machines::gimli::main ( $params = undef ) {
  tmuxinator::os::arch::main{ 'tmuxinator::gimli':
    params => $params
  }
}

