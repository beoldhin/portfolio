class tmuxinator::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  tmuxinator::os::arch::user{ 'tmuxinator::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

