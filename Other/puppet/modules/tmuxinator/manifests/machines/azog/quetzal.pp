class tmuxinator::machines::azog::quetzal ( $user = undef, $params = undef ) {
  tmuxinator::os::arch::user{ 'tmuxinator::azog::quetzal':
    user   => $user,
    params => $params
  }
}

