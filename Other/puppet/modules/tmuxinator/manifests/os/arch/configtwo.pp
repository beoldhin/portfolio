define tmuxinator::os::arch::configtwo(
$user = undef,
$project = undef,
$windowpath1 = undef,
$windowpath2 = undef,
$windowlayout1 = undef,
$windowlayout2 = undef ) {
  $homepath = "/home/${user}"
  file { "${project}.yml-${user}":
    ensure  => present,
    require => [
      User[$user],
#      Package['tmuxinator'],
      File["tmuxinator-dir-${user}"]
    ],
    path    => "${homepath}/.tmuxinator/${project}.yml",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    content => epp("tmuxinator/${project}.yml.epp", {
      'windowpath1'   => $windowpath1,
      'windowpath2'   => $windowpath2,
      'windowlayout1' => $windowlayout1,
      'windowlayout2' => $windowlayout2 }
    )
  }
}

