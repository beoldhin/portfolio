define tmuxinator::os::arch::configone(
$user = undef,
$project = undef,
$projectpath = undef,
$windowlayout = undef ) {
  $homepath = "/home/${user}"
  file { "${project}.yml-${user}":
    ensure  => present,
    require => [
      User[$user],
#      Package['tmuxinator'],
      File["tmuxinator-dir-${user}"]
    ],
    path    => "${homepath}/.tmuxinator/${project}.yml",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    content => epp("tmuxinator/${project}.yml.epp", {
      'projectpath'  => $projectpath,
      'windowlayout' => $windowlayout }
    )
  }
}

