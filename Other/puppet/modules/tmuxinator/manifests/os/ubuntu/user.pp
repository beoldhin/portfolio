define tmuxinator::os::ubuntu::user ( $user = undef, $params = undef ) {
  $resolution = $params['resolution']
  $settings = hiera("tmuxinator-${user}")
  $windowlayout = $settings["config-${resolution}-3windows"]
  $windowlayout1 = $windowlayout
  $windowlayout2 = $windowlayout
  $homepath = "/home/${user}"
  file { "tmuxinator-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['tmuxinator']
    ],
    path    => "${homepath}/.tmuxinator",
    mode    => '0755',
    owner   => $user,
    group   => $user,
    purge   => true
  }
  $oneparams = $settings['paths-one']
  each($oneparams) |$value| {
    $project = $value[0]
    $projectpath = $value[1]
    tmuxinator::os::ubuntu::configone{ "tmuxinator::configone::${project}":
      user         => $user,
      project      => $project,
      projectpath  => $projectpath,
      windowlayout => $windowlayout
    }
  }
  $twoparams = $settings['paths-two']
  each($twoparams) |$value| {
    $project = $value[0]
    $windowpath1 = $value[1][0]
    $windowpath2 = $value[1][1]
    tmuxinator::os::ubuntu::configtwo{ "tmuxinator::configtwo::${project}":
      user          => $user,
      project       => $project,
      windowpath1   => $windowpath1,
      windowpath2   => $windowpath2,
      windowlayout1 => $windowlayout1,
      windowlayout2 => $windowlayout2
    }
  }
}

