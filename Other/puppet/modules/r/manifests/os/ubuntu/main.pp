define r::os::ubuntu::main ( $params = undef ) {
  package { 'r-base':
    ensure => installed
  }
  package { 'r-base-dev':
    ensure  => installed,
    require => Package['r-base'],
  }
  package { 'libcurl4-gnutls-dev':  # for installing knitr and others
    ensure  => installed,
    require => Package['r-base-dev'],
  }
}

