class tmux::machines::aldor::main ( $params = undef ) {
  tmux::os::ubuntu::main{ 'tmux::aldor':
    params => $params
  }
}

