class tmux::machines::aldor::quetzal ( $user = undef, $params = undef ) {
  tmux::os::ubuntu::user{ 'tmux::aldor::quetzal':
    user   => $user,
    params => $params
  }
}

