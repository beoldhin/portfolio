class tmux::machines::aldor::leela ( $user = undef, $params = undef ) {
  tmux::os::ubuntu::user{ 'tmux::aldor::leela':
    user   => $user,
    params => $params
  }
}

