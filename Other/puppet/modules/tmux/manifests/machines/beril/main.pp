class tmux::machines::beril::main ( $params = undef ) {
  tmux::os::ubuntu::main{ 'tmux::beril':
    params => $params
  }
}

