class tmux::machines::beril::quetzal ( $user = undef, $params = undef ) {
  tmux::os::ubuntu::user{ 'tmux::beril::quetzal':
    user   => $user,
    params => $params
  }
}

