class tmux::machines::azog::quetzal ( $user = undef, $params = undef ) {
  tmux::os::arch::user{ 'tmux::azog::quetzal':
    user   => $user,
    params => $params
  }
}

