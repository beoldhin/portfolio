class tmux::machines::azog::main ( $params = undef ) {
  tmux::os::arch::main{ 'tmux::azog':
    params => $params
  }
}

