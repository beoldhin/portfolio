class tmux::machines::gimli::main ( $params = undef ) {
  tmux::os::arch::main{ 'tmux::gimli':
    params => $params
  }
}

