class tmux::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  tmux::os::arch::user{ 'tmux::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

