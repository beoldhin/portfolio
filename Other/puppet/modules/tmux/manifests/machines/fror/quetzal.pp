class tmux::machines::fror::quetzal ( $user = undef, $params = undef ) {
  tmux::os::ubuntu::user{ 'tmux::fror::quetzal':
    user   => $user,
    params => $params
  }
}

