class tmux::machines::fror::main ( $params = undef ) {
  tmux::os::ubuntu::main{ 'tmux::fror':
    params => $params
  }
}

