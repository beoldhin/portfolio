class tmux::machines::haldar::quetzal ( $user = undef, $params = undef ) {
  tmux::os::ubuntu::user{ 'tmux::haldar::quetzal':
    user   => $user,
    params => $params
  }
}

