class tmux::machines::haldar::main ( $params = undef ) {
  tmux::os::ubuntu::main{ 'tmux::haldar':
    params => $params
  }
}

