class tmux::os::ubuntu::addrepo ( $codename = undef ) {
  # Unfortunately the apt::ppa package has bugs in Mint
  Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
  exec { 'addrepo-tmux':
    command => 'add-apt-repository -y ppa:pi-rho/dev && apt-get update',
    creates => "/etc/apt/sources.list.d/pi-rho-dev${codename}.list",
  }
  package { 'tmux':
    ensure  => installed,
    require => Exec['addrepo-tmux'],
  }
}

