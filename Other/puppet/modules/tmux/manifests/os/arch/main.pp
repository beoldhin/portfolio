define tmux::os::arch::main ( $params = undef ) {
  package { 'xsel':  # for copypaste from/to tmux to work
    ensure => installed
  }
  package { 'tmux':
    ensure => installed
  }
  # Copy all own scripts under /usr/local/bin -- begin
  file { 'pdf_launch':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/pdf_launch',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/pdf_launch',
  }
  file { 'tmux_angband':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_angband',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_angband',
  }
  file { 'tmux_app_params':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_app_params',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_app_params',
  }
  file { 'tmux_newsbeuter':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_newsbeuter',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_newsbeuter',
  }
  file { 'tmux_octave-cli':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_octave-cli',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_octave-cli',
  }
  file { 'tmux_pedit':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_pedit',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_pedit',
  }
  file { 'tmux_sage':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_sage',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_sage',
  }
  file { 'tmux_stop_session':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_stop_session',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_stop_session',
  }
  file { 'tmux_tig':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_tig',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_tig',
  }
  file { 'tmux_vim':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_vim',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_vim',
  }
  file { 'tmux_weechat':
    ensure  => present,
    require => Package['tmux'],
    path    => '/usr/local/bin/tmux_weechat',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/tmux/tmux_weechat',
  }
  # Copy all own scripts under /usr/local/bin -- end
}

