define tmux::os::arch::user ( $user = undef, $params = undef ) {
  $distribution = $params['distribution']
  $homepath = "/home/${user}"
  $userpath = "puppet:///modules/tmux/os/${distribution}/users/${user}"
  file { "tmux.conf-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['tmux'],
    ],
    path    => "${homepath}/.tmux.conf",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/tmux.conf",
  }
}

