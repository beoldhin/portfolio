class weatherutil::machines::azog::quetzal ( $user = undef, $params = undef ) {
  weatherutil::os::arch::user{ 'weatherutil::azog::quetzal':
    user   => $user,
    params => $params
  }
}

