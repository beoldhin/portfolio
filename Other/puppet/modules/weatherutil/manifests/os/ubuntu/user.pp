define weatherutil::os::ubuntu::user ( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $userpath = "puppet:///modules/weatherutil/users/${user}"
  file { "weatherutil-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['weather-util'],
    ],
    path    => "${homepath}/.weather",
    mode    => '0775',
    owner   => $user,
    group   => $user,
  }
  file { "weatherrc-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["weatherutil-dir-${user}"],
      Package['weather-util'],
    ],
    path    => "${homepath}/.weather/weatherrc",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/weatherrc",
  }
}

