class handbrake::machines::azog::main ( $params = undef ) {
  handbrake::os::arch::main{ 'handbrake::azog':
    params => $params
  }
}

