define handbrake::os::ubuntu::main ( $params = undef ) {
  package { 'handbrake':
    ensure => installed
  }
}

