define remmina::os::ubuntu::main ( $params = undef ) {
  package { 'remmina':
    ensure => installed
  }
  # Also install all possible plugins -- begin
  package { 'remmina-plugin-gnome':
    ensure => installed
  }
  package { 'remmina-plugin-rdp':
    ensure => installed
  }
  package { 'remmina-plugin-vnc':
    ensure => installed
  }
  package { 'remmina-plugin-nx':
    ensure => installed
  }
  package { 'remmina-plugin-xdmcp':
    ensure => installed
  }
  package { 'remmina-plugin-telepathy':
    ensure => installed
  }
  # Also install all possible plugins -- end
}

