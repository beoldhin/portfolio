class remmina::machines::gimli::main ( $params = undef ) {
  remmina::os::arch::main{ 'remmina::gimli':
    params => $params
  }
}

