class remmina::machines::azog::main ( $params = undef ) {
  remmina::os::arch::main{ 'remmina::azog':
    params => $params
  }
}

