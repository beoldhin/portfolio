define ctags::os::ubuntu::user ( $user = undef, $params = undef ) {
  # LaTeX specific definitions -- start
  $userpath = "puppet:///modules/ctags/users/${user}"
  file { "ctags-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['exuberant-ctags'],
    ],
    path    => "/home/${user}/.ctags",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/ctags"
  }
  # LaTeX specific definitions -- end
}

