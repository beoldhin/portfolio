define ctags::os::ubuntu::main ( $params = undef ) {
  package { 'exuberant-ctags':
    ensure => installed
  }
}

