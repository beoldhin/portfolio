define ctags::os::arch::main ( $params = undef ) {
  package { 'ctags':
    ensure => installed
  }
}

