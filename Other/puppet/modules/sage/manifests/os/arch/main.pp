define sage::os::arch::main ( $params = undef ) {
  package { 'sagemath':
    ensure => installed,
  }
  package { 'sage-notebook':
    ensure  => installed,
    require => Package['sagemath'],
  }
  package { 'sagemath-doc':
    ensure => installed,
    require => Package['sagemath'],
  }
}

