class sage::os::ubuntu::addrepo ( $params = undef ) {
  $distribution = $params['distribution']
  $codename = $params['codename']
  # Unfortunately the apt::ppa package has bugs in Mint
  Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
  exec { 'addrepo-sage':
    command => 'add-apt-repository -y ppa:aims/sagemath && apt-get update',
    creates => "/etc/apt/sources.list.d/aims-${distribution}-sagemath-${codename}.list"
  }
  package { 'sagemath-upstream-binary':
    ensure  => installed,
    require => Exec['addrepo-sage'],
  }
}

