class sage::machines::gimli::main ( $params = undef ) {
  sage::os::arch::main{ 'sage::gimli':
    params => $params
  }
}

