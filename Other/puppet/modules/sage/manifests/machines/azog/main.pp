class sage::machines::azog::main ( $params = undef ) {
  sage::os::arch::main{ 'sage::azog':
    params => $params
  }
}

