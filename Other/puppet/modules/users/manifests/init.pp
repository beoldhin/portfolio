class users ( $params = undef ) {
  $users = $params['users']
  $machinename = $params['machinename']
  each($users) |$value| {
    $privclassdef = "${name}::machines::${machinename}::${value}"
    if defined( $privclassdef ) {
      class { $privclassdef:
        user   => $value,
        params => $params
      }
    }
    else {
      notify { "Skipped user \"${value}\"": }
    }
  }
}

