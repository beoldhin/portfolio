class users::machines::azog::quetzal ( $user = undef, $params = undef ) {
  users::os::arch::user{ 'users::azog::quetzal':
    user   => $user,
    params => $params,
  }
}

