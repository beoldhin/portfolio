class users::machines::aldor::jay ( $user = undef, $params = undef ) {
    # Note: Use "mkpasswd -m sha-512" to generate type 6 password
    # (package whois)
    # Note: Use "id <user>" to find user's uid/gid/groups
    user { $user:
      ensure     => present,
      comment    => 'Jay,,,',
      password   => '',  # No password
      shell      => '/bin/bash',
      managehome => true
    }
    users::os::ubuntu::dirs{ 'users::dir::aldor::jay':
      user   => $user,
      params => $params
    }
}

