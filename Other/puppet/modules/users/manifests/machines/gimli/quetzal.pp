class users::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  users::os::arch::user{ 'users::gimli::quetzal':
    user   => $user,
    params => $params,
  }
}

