define users::os::ubuntu::user ( $user = undef, $params = undef ) {
  # Note: Use "mkpasswd -m sha-512" to generate type 6 password hash (package whois)
  # Note: Use "python -c "import crypt, getpass, pwd; print crypt.crypt(raw_input(), '\$6\$' + raw_input() + '\$')""
  #   to generate hash /etc/shadow entry from password + salt
  # Note: Use "id <user>" to find user's uid/gid/groups
  $machinename = $params['machinename']
  $settings = hiera("user-${user}")
  user { $user:
    ensure     => present,
    comment    => $settings[$machinename]['comment'],
    password   => $settings[$machinename]['hash'],
    shell      => '/bin/bash',
    groups     => [ 'sudo', ],
    managehome => true
  }
  users::os::ubuntu::dirs{ "users::dirs::${machinename}::${user}":
    user   => $user,
    params => $params
  }
}

