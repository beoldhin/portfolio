define users::os::ubuntu::dirs( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  file { "config-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
    ],
    path    => "${homepath}/.config",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { "local-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
    ],
    path    => "${homepath}/.local",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { "local-share-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      File["local-dir-${user}"],
    ],
    path    => "${homepath}/.local/share",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { "local-share-applications-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      File["local-share-dir-${user}"],
    ],
    path    => "${homepath}/.local/share/applications",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { "desktop-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
    ],
    path    => "${homepath}/Desktop",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
}

