define gimp::os::ubuntu::main ( $params = undef ) {
  package { 'gimp':
    ensure => installed,
  }
  package { 'gimp-plugin-registry':
    ensure  => installed,
    require => Package['gimp'],
  }
}

