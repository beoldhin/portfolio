define gimp::os::arch::main ( $params = undef ) {
  package { 'gimp':
    ensure => installed,
  }
  package { 'gimp-plugin-fblur':
    ensure  => installed,
    require => Package['gimp'],
  }
  package { 'gimp-plugin-gmic':
    ensure  => installed,
    require => Package['gimp'],
  }
  package { 'gimp-plugin-lqr':
    ensure  => installed,
    require => Package['gimp'],
  }
  package { 'gimp-plugin-mathmap':
    ensure  => installed,
    require => Package['gimp'],
  }
  package { 'gimp-plugin-wavelet-decompose':
    ensure  => installed,
    require => Package['gimp'],
  }
  package { 'gimp-plugin-wavelet-denoise':
    ensure  => installed,
    require => Package['gimp'],
  }
}

