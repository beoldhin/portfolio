class gimp::machines::gimli::main ( $params = undef ) {
  gimp::os::arch::main{ 'gimp::gimli':
    params => $params
  }
}

