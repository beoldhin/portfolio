define newsbeuter::os::arch::main ( $params = undef ) {
  package { 'newsbeuter':
    ensure => installed
  }
  file { 'newsbeuter_browser':
    ensure  => present,
    require => Package['newsbeuter'],
    path    => '/usr/local/bin/browser',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/newsbeuter/os/unix/browser',
  }
}

