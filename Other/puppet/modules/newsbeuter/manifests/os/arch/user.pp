define newsbeuter::os::arch::user ( $user = undef, $params = undef ) {
  $userpath = "puppet:///modules/newsbeuter/users/${user}"
  file { "newsbeuter-newsbeuter-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['newsbeuter'],
      File["config-dir-${user}"],
    ],
    path    => "/home/${user}/.config/newsbeuter",
    mode    => '0775',
    owner   => $user,
    group   => $user,
  }
  file { "newsbeuter-xdg-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['newsbeuter'],
      File["local-share-dir-${user}"],
    ],
    path    => "/home/${user}/.local/share/newsbeuter",
    replace => no,
    mode    => '0775',
    owner   => $user,
    group   => $user,
  }
  file { 'newsbeuter-urls':
    ensure  => present,
    require => [
      User[$user],
      Package['newsbeuter'],
      File["newsbeuter-newsbeuter-dir-${user}"],
    ],
    path    => "/home/${user}/.config/newsbeuter/urls",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/urls",
  }
  file { 'newsbeuter-config':
    ensure  => present,
    require => [
      User[$user],
      Package['newsbeuter'],
      File["newsbeuter-newsbeuter-dir-${user}"],
    ],
    path    => "/home/${user}/.config/newsbeuter/config",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/config",
  }
}

