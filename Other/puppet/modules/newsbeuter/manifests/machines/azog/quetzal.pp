class newsbeuter::machines::azog::quetzal ( $user = undef, $params = undef ) {
  newsbeuter::os::arch::user{ 'newsbeuter::azog::quetzal':
    user   => $user,
    params => $params
  }
}

