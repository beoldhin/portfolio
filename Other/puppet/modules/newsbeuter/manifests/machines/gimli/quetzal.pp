class newsbeuter::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  newsbeuter::os::arch::user{ 'newsbeuter::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

