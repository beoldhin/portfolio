define opensshserver::os::ubuntu::master ( $params = undef ) {
  package { 'openssh-server':
    ensure => installed
  }
  # Next is the OTPW setup
  augeas { 'sshd_config':
    require => Package['openssh-server'],
    context => '/files/etc/ssh/sshd_config',
    lens    => 'sshd.lns',
    incl    => '/etc/ssh/sshd_config',
    changes => [
      'set PermitRootLogin no',
      'set ChallengeResponseAuthentication yes',
      'set PasswordAuthentication no',
      'set UsePAM yes',
    ],
  }
}

