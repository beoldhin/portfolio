define opensshserver::os::ubuntu::main ( $params = undef ) {
  package { 'openssh-server':
    ensure => installed
  }
  augeas { 'sshd_config':
    require => Package['openssh-server'],
    context => '/files/etc/ssh/sshd_config',
    lens    => 'sshd.lns',
    incl    => '/etc/ssh/sshd_config',
    changes => [
      'set PermitRootLogin no',
      'set PasswordAuthentication no',
      'set UsePAM no',
    ],
  }
}

