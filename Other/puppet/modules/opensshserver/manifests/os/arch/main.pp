define opensshserver::os::arch::main ( $params = undef ) {
  augeas { 'sshd_config':
    require => Package['openssh'],
    context => '/files/etc/ssh/sshd_config',
    lens    => 'sshd.lns',
    incl    => '/etc/ssh/sshd_config',
    changes => [
      'set PermitRootLogin no',
      'set PasswordAuthentication no',
      'set UsePAM no',
    ],
  }
}

