define opensshserver::os::arch::master ( $params = undef ) {
  package { 'openssh':
    ensure => installed
  }
  # Next is the OTPW setup
  augeas { 'sshd_config':
    require => Package['openssh'],
    context => '/files/etc/ssh/sshd_config',
    lens    => 'sshd.lns',
    incl    => '/etc/ssh/sshd_config',
    changes => [
      'set PermitRootLogin no',
      'set ChallengeResponseAuthentication yes',
      'set PasswordAuthentication no',
      'set UsePAM yes',
    ],
  }
}

