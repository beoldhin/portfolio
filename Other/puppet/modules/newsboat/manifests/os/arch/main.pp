define newsboat::os::arch::main ( $params = undef ) {
  package { 'newsboat':
    ensure => installed
  }
  file { 'newsboat_browser':
    ensure  => present,
    require => Package['newsboat'],
    path    => '/usr/local/bin/browser',
    replace => yes,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    source  => 'puppet:///modules/newsboat/os/unix/browser',
  }
}

