define newsboat::os::arch::user ( $user = undef, $params = undef ) {
  $userpath = "puppet:///modules/newsboat/users/${user}"
  file { "newsboat-newsboat-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['newsboat'],
      File["config-dir-${user}"],
    ],
    path    => "/home/${user}/.config/newsboat",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { "newsboat-xdg-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['newsboat'],
      File["local-share-dir-${user}"],
    ],
    path    => "/home/${user}/.local/share/newsboat",
    replace => no,
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { 'newsboat-urls':
    ensure  => present,
    require => [
      User[$user],
      Package['newsboat'],
      File["newsboat-newsboat-dir-${user}"],
    ],
    path    => "/home/${user}/.config/newsboat/urls",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/urls",
  }
  file { 'newsboat-config':
    ensure  => present,
    require => [
      User[$user],
      Package['newsboat'],
      File["newsboat-newsboat-dir-${user}"],
    ],
    path    => "/home/${user}/.config/newsboat/config",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/config",
  }
}

