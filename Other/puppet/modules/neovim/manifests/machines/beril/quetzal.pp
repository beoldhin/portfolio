class neovim::machines::beril::quetzal ( $user = undef, $params = undef ) {
  neovim::os::ubuntu::user{ 'neovim::beril::quetzal':
    user   => $user,
    params => $params
  }
}

