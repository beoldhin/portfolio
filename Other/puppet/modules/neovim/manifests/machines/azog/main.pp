class neovim::machines::azog::main ( $params = undef ) {
  neovim::os::arch::main{ 'neovim::azog':
    params => $params
  }
}

