class neovim::machines::azog::quetzal ( $user = undef, $params = undef ) {
  neovim::os::arch::user{ 'neovim::azog::quetzal':
    user   => $user,
    params => $params
  }
}

