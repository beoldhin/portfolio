define neovim::os::arch::main ( $params = undef ) {
  package { 'neovim':
    ensure  => installed,
    require => Package['ctags'],
  }
  package { 'python-neovim':
    ensure  => installed,
    require => Package['neovim'],
  }
}

