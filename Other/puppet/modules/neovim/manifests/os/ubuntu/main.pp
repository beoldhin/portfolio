define neovim::os::ubuntu::main ( $params = undef ) {
  class { 'neovim::os::ubuntu::addrepo':
    params => $params
  }
}

