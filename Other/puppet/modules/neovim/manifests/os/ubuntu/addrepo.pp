class neovim::os::ubuntu::addrepo ( $params = undef ) {
  $distribution = $params['distribution']
  $codename = $params['codename']
  # Unfortunately the apt::ppa package has bugs in Mint
  Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
  exec { 'addrepo-neovim':
    command => 'add-apt-repository -y ppa:neovim-ppa/unstable && apt-get update',
    creates =>
    "/etc/apt/sources.list.d/neovim-ppa-${distribution}-unstable-${codename}.list",
  }
  package { 'neovim':
    ensure  => installed,
    require => Exec['addrepo-neovim'],
  }
}

