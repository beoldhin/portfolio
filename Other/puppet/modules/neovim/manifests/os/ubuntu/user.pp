define neovim::os::ubuntu::user ( $user = undef, $params = undef ) {
  $userpath = "puppet:///modules/neovim/users/${user}"
  file { "neovim-neovim-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['neovim'],
    ],
    path    => "/home/${user}/.config/nvim",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "neovim-tmp-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['neovim'],
      File["neovim-neovim-dir-${user}"],
    ],
    path    => "/home/${user}/.config/nvim/tmp",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "neovim-backup-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['neovim'],
      File["neovim-neovim-dir-${user}"],
    ],
    path    => "/home/${user}/.config/nvim/backup",
    replace => no,
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "neovim-ftplugin-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['neovim'],
      File["neovim-neovim-dir-${user}"],
    ],
    path    => "/home/${user}/.config/nvim/ftplugin",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "neovim-ftdetect-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['neovim'],
      File["neovim-neovim-dir-${user}"],
    ],
    path    => "/home/${user}/.config/nvim/ftdetect",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "neovim-undo-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['neovim'],
      File["neovim-neovim-dir-${user}"],
    ],
    path    => "/home/${user}/.config/nvim/undo",
    replace => no,
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "neovim-init.vim-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['neovim'],
      Package['curl'],
      Package['git'],
    ],
    path    => "/home/${user}/.config/nvim/init.vim",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/init.vim",
  }
  file { "neovim-tex.vim-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['neovim'],
      File["neovim-ftplugin-dir-${user}"],
    ],
    path    => "/home/${user}/.config/nvim/ftplugin/tex.vim",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/tex.vim"
  }
  file { "neovim-Rnw.vim-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['neovim'],
      File["neovim-ftdetect-dir-${user}"],
    ],
    path    => "/home/${user}/.config/nvim/ftdetect/Rnw.vim",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/Rnw.vim",
  }
}

