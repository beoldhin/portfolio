define youtubedl::os::arch::main ( $params = undef ) {
  package { 'youtube-dl':
    ensure => installed
  }
}

