define youtubedl::os::ubuntu::main ( $params = undef ) {
  package { 'youtube-dl':
    ensure => installed
  }
}

