define abcde::os::ubuntu::main ( $params = undef ) {
  $machinename = $params['machinename']
  $codename = $params['codename']
  package { 'abcde':
    ensure => installed
  }
  $settings = hiera("abcde-${machinename}")
  file { "abcde.conf":
    ensure  => present,
    require => Package['abcde'],
    path    => '/etc/abcde.conf',
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => epp("abcde/os/ubuntu/${codename}/abcde.conf.epp", {
      'lame'     => $settings['lame'],
      'lameopts' => $settings['lameopts'],
      'flac'     => $settings['flac'],
      'id3v2'    => $settings['id3v2'] }
    )
  }
}

