define vlc::os::ubuntu::user ( $user = undef, $params = undef ) {
  $machinename = $params['machinename']
  $settings = hiera("services-${user}")
  $homepath = "/home/${user}"
  $configpath = "puppet:///modules/vlc/machines/${machinename}"
  file { "vlc-vlc-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vlc'],
      File["local-share-dir-${user}"],
    ],
    path    => "${homepath}/.local/share/vlc",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { "vlc-lua-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vlc'],
      File["vlc-vlc-dir-${user}"],
    ],
    path    => "${homepath}/.local/share/vlc/lua",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { "vlc-sd-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vlc'],
      File["vlc-lua-dir-${user}"],
    ],
    path    => "${homepath}/.local/share/vlc/lua/sd",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  vcsrepo { "${homepath}/.local/share/vlc/lua/sd":
    ensure   => present,
    require  => [
      User[$user],
      Package['git'],
      Package['vlc'],
      File["vlc-sd-dir-${user}"],
    ],
    provider => git,
    source   => 'https://github.com/diegofn/TuneIn-Radio-VLC.git',
    revision => 'master',
    user     => $user,
  }
  file { "radiotime.lua-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vlc'],
      Vcsrepo["${homepath}/.local/share/vlc/lua/sd"],
    ],
    path    => '/usr/lib/vlc/lua/playlist/radiotime.lua',
    replace => yes,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => "${configpath}/radiotime.lua",
  }
  file { "streamtheworld.lua-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vlc'],
      Vcsrepo["${homepath}/.local/share/vlc/lua/sd"],
    ],
    path    => '/usr/lib/vlc/lua/playlist/streamtheworld.lua',
    replace => yes,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    source  => "${configpath}/streamtheworld.lua",
  }
  file { "tunein.lua-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vlc'],
      Vcsrepo["${homepath}/.local/share/vlc/lua/sd"],
    ],
    path    => "${homepath}/.local/share/vlc/lua/sd/tunein.lua",
    replace => yes,
    mode    => '0600',
    owner   => $user,
    group   => $user,
    content => epp("vlc/tunein.lua-${machinename}.epp", {
      'user' => $settings['tunein']['user'],
      'pass' => $settings['tunein']['pass'] }
    )
  }
}

