define vlc::os::ubuntu::main ( $params = undef ) {
  package { 'vlc':
    ensure => installed
  }
  package { 'browser-plugin-vlc':
    ensure  => installed,
    require => Package['vlc'],
  }
}

