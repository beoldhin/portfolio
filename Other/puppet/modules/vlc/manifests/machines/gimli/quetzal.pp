class vlc::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  vlc::os::arch::user{ 'vlc::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

