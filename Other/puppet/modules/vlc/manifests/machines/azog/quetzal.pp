class vlc::machines::azog::quetzal ( $user = undef, $params = undef ) {
  vlc::os::arch::user{ 'vlc::azog::quetzal':
    user   => $user,
    params => $params
  }
}

