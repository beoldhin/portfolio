# Note: Use "mypuppettest" first to see how /etc/fstab will be changed.
# Then make the UUID modification also to puppet's file.

class nodes::gimli::main {

  $params = { users             => [ 'quetzal' ],
              machinename       => 'gimli',
              origmachine       => 'gimli',
              backup_dest_host  => 'azog',
              backup_dest_user  => 'quetzal',
              backup_data_owner => 'quetzal',
              distribution      => 'arch',
              dosboxver         => '0.74',
              slightver         => '5.1',
              resolution        => '1080p' }

  class { 'templates::base::main':      params => $params }
  class { 'templates::base::ssd':       params => $params }
  class { 'templates::system::main':    params => $params }
  class { 'templates::system::graph':   params => $params }
  class {   'virtualbox':               params => $params }
  class {   'powerline':                params => $params }
  class { 'templates::ssh::main':       params => $params }
  class { 'templates::math::main':      params => $params }
  class {   'sage':                     params => $params }
  class { 'templates::microsoft::main': params => $params }
  class { 'templates::media::main':     params => $params }
  class {   'dosbox':                   params => $params }
  class { 'templates::editing::main':   params => $params }
  class {   'newsboat':                 params => $params }
}

