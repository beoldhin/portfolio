# Note: Use "mypuppettest" first to see how /etc/fstab will be changed.
# Then make the UUID modification also to puppet's file.

class nodes::beril::main {
  $params = { users         => [ 'quetzal', ],
              mastermachine => 'beril',
              machinename   => 'beril',
              origmachine   => 'elrond',
              distribution  => 'ubuntu',   # Distribution's name
              codename      => 'yakkety',  # Distribution's codename
              dosboxver     => '0.74',     # Dosbox version
              slightver     => '5.1',      # Silverlight version
              resolution    => '1080p' }   # Screen resolution

  class { 'templates::base::main'      : params => $params }
  class {   'radeon'                   : params => $params }
  class {   'puppet'                   : params => $params }
  class {   'gitserver'                : params => $params }
  class {   'postfix'                  : params => $params }
  class { 'templates::base::ssd'       : params => $params }
  class { 'templates::system::main'    : params => $params }
  class { 'templates::system::graph'   : params => $params }
  class {   'virtualbox'               : params => $params }
  class {   'powerline'                : params => $params }
  class { 'templates::ssh::main'       : params => $params }
  class {   'otpw'                     : params => $params }
  class { 'templates::math::main'      : params => $params }
#  class {   'sage'                     : params => $params }
  class { 'templates::microsoft::main' : params => $params }
  class { 'templates::media::main'     : params => $params }
  class { 'templates::media::dvd'      : params => $params }
  class { 'templates::media::cd'       : params => $params }
  class {   'dosbox'                   : params => $params }
  class { 'templates::editing::main'   : params => $params }
  class {   'freeplane'                : params => $params }
#  class {   'jabref'                   : params => $params }
  class {   'gimp'                     : params => $params }

  # class {   'neovim'                   : params => $params }
  # class {   'tiled'                    : params => $params }
}

