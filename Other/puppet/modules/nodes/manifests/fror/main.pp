# Note: Use "mypuppettest" first to see how /etc/fstab will be changed.
# Then make the UUID modification also to puppet's file.

class nodes::fror::main {
  $params = { users         => [ 'quetzal' ],
              mastermachine => 'azog',
              machinename   => 'fror',
              origmachine   => 'shelob',
              distribution  => 'ubuntu',  # Distribution's name
              codename      => 'xenial',  # Distribution's codename
              dosboxver     => '0.74',    # Dosbox version
              slightver     => '5.1' }    # Silverlight version

  class { 'templates::base::main'    : params => $params }
  class { 'templates::server::main'  : params => $params }
  class {   'basessd'                : params => $params }
  class { 'templates::system::main'  : params => $params }
  class { 'templates::ssh::main'     : params => $params }
  class {   'octave'                 : params => $params }
  class {   'newsbeuter'             : params => $params }
  class {   'dict'                   : params => $params }
  class { 'templates::editing::main' : params => $params }
  class { 'templates::chat::main'    : params => $params }
}

