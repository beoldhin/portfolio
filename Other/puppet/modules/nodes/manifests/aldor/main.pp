# Note: Use "mypuppettest" first to see how /etc/fstab will be changed.
# Then make the UUID modification also to puppet's file.

class nodes::aldor::main {
    $params = { users             => [ 'leela', 'jay', 'quetzal', ],
                machinename       => 'aldor',
                origmachine       => 'gandalf',
                backup_dest_host  => 'azog',
                backup_dest_user  => 'quetzal',
                backup_data_owner => 'leela',    # For "Mirrored" directory
                distribution      => 'ubuntu',   # Distribution's name
                codename          => 'zesty',    # Distribution's codename
                dosboxver         => '0.74',     # Dosbox version
                slightver         => '5.1',      # Silverlight version
                resolution        => '1080p' }   # Screen resolution

    class { 'templates::base::main'      : params => $params }
    # class {   'bindfs'                   : params => $params }
    class { 'templates::base::ssd'       : params => $params }
    class { 'templates::system::main'    : params => $params }
    class { 'templates::system::graph'   : params => $params }
    class {   'powerline'                : params => $params }
    class { 'templates::ssh::main'       : params => $params }
    class { 'templates::math::main'      : params => $params }
    class { 'templates::microsoft::main' : params => $params }
    class { 'templates::media::main'     : params => $params }
    class { 'templates::media::dvd'      : params => $params }
    class { 'templates::media::cd'       : params => $params }
    class { 'templates::editing::main'   : params => $params }
    # class {   'freeplane'                : params => $params }
}

