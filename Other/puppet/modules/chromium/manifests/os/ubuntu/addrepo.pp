class chromium::os::ubuntu::addrepo ( $params = undef ) {
  $distribution = $params['distribution']
  $codename = $params['codename']
  # Unfortunately the apt::ppa package has bugs in Mint
  Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
  exec { 'addrepo-pepperflashplugin':
    command => 'add-apt-repository -y ppa:skunk/pepper-flash && apt-get update',
    creates => "/etc/apt/sources.list.d/skunk-${distribution}-pepper-flash-${codename}.list"
  }
  package { 'pepflashplugin-installer':
    ensure  => installed,
    require => Exec['addrepo-pepperflashplugin'],
  }
  # Finally make sure "pepperflashplugin-nonfree" is NOT installed!
  package { 'pepperflashplugin-nonfree':
    ensure  => purged,
    require => Package['pepflashplugin-installer']
  }
}

