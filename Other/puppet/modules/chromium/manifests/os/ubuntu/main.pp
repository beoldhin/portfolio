define chromium::os::ubuntu::main ( $params = undef ) {
  # $machinename = $params['machinename']
  # $configpath = "puppet:///modules/chromium/machines/${machinename}"
  package { 'chromium-browser':
    ensure => installed
  }
  # # Using the PPA for this reason:
  # # https://bugs.launchpad.net/ubuntu/+source/pepperflashplugin-nonfree/+bug/1312219
  # # The PPA version updates automatically, the one from Ubuntu doesn't.
  # class { 'chromium::os::ubuntu::addrepo':
  #   params  => $params,
  #   require => Package['chromium-browser']
  # }
  # # package { 'pepperflashplugin-nonfree':
  # #   ensure  => installed,
  # #   require => Package['chromium-browser'],
  # # }
  # file { 'chromium-default':
  #   ensure  => present,
  #   require => Package['chromium-browser'],
  #   path    => '/etc/chromium-browser/default',
  #   replace => yes,
  #   mode    => '0644',
  #   owner   => 'root',
  #   group   => 'root',
  #   source  => "${configpath}/default",
  # }
}

