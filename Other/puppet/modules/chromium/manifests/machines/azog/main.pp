class chromium::machines::azog::main ( $params = undef ) {
  chromium::os::arch::main{ 'chromium::azog':
    params => $params
  }
}

