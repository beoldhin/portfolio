class chromium::machines::gimli::main ( $params = undef ) {
  chromium::os::arch::main{ 'chromium::gimli':
    params => $params
  }
}

