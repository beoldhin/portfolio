define octave::os::ubuntu::user ( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $userpath = "puppet:///modules/octave/users/${user}"
  file { "octaverc-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['octave'],
    ],
    path    => "${homepath}/.octaverc",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/octaverc",
  }
}

