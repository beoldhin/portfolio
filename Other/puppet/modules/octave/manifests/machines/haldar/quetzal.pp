class octave::machines::haldar::quetzal ( $user = undef, $params = undef ) {
  octave::os::ubuntu::user{ 'octave::haldar::quetzal':
    user   => $user,
    params => $params
  }
}

