class octave::machines::gimli::main ( $params = undef ) {
  octave::os::arch::main{ 'octave::gimli':
    params => $params
  }
}

