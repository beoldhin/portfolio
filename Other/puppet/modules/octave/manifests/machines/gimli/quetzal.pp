class octave::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  octave::os::arch::user{ 'octave::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

