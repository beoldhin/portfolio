class octave::machines::azog::quetzal ( $user = undef, $params = undef ) {
  octave::os::arch::user{ 'octave::azog::quetzal':
    user   => $user,
    params => $params
  }
}

