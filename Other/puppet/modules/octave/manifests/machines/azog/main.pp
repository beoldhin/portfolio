class octave::machines::azog::main ( $params = undef ) {
  octave::os::arch::main{ 'octave::azog':
    params => $params
  }
}

