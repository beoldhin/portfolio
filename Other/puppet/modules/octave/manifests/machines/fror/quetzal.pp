class octave::machines::fror::quetzal ( $user = undef, $params = undef ) {
  octave::os::ubuntu::user{ 'octave::fror::quetzal':
    user   => $user,
    params => $params
  }
}

