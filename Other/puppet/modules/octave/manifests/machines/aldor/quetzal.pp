class octave::machines::aldor::quetzal ( $user = undef, $params = undef ) {
  octave::os::ubuntu::user{ 'octave::aldor::quetzal':
    user   => $user,
    params => $params
  }
}

