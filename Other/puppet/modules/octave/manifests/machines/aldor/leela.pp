class octave::machines::aldor::leela ( $user = undef, $params = undef ) {
  octave::os::ubuntu::user{ 'octave::aldor::leela':
    user   => $user,
    params => $params
  }
}

