class octave::machines::beril::quetzal ( $user = undef, $params = undef ) {
  octave::os::ubuntu::user{ 'octave::beril::quetzal':
    user   => $user,
    params => $params
  }
}

