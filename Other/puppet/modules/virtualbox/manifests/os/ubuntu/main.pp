define virtualbox::os::ubuntu::main ( $params = undef ) {
  package { 'virtualbox':
    ensure => installed
  }
  package { 'virtualbox-guest-additions-iso':
    ensure => installed
  }
}

