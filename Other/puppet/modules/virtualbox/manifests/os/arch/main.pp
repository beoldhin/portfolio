define virtualbox::os::arch::main ( $params = undef ) {
  package { 'virtualbox-host-modules-arch':
    ensure => installed
  }
  package { 'virtualbox':
    ensure  => installed,
    require => Package['virtualbox-host-modules-arch']
  }
}

