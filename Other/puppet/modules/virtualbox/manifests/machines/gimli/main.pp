class virtualbox::machines::gimli::main ( $params = undef ) {
  virtualbox::os::arch::main{ 'virtualbox::gimli':
    params => $params
  }
}

