class virtualbox::machines::azog::main ( $params = undef ) {
  virtualbox::os::arch::main{ 'virtualbox::azog':
    params => $params
  }
}

