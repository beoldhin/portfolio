define powertop::os::ubuntu::main ( $params = undef ) {
  package { 'powertop':
    ensure => installed
  }
}

