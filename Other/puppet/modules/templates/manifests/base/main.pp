class templates::base::main( $params = undef ) {
  class { 'users' : params => $params }
  class { 'base'  : params => $params }
  class { 'git'   : params => $params }
}

