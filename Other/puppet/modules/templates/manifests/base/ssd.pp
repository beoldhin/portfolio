class templates::base::ssd( $params = undef ) {
  class { 'basessd' : params => $params }
  class { 'psd'     : params => $params }
}

