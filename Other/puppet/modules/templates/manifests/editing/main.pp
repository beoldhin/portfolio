class templates::editing::main( $params = undef ) {
  class { 'ctags'  : params => $params }
  class { 'vim'    : params => $params }
  # class { 'neovim' : params => $params }
}
