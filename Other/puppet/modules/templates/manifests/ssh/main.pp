class templates::ssh::main( $params = undef ) {
  class { 'opensshclient' : params => $params }
  class { 'opensshserver' : params => $params }
  class { 'mosh'          : params => $params }
  class { 'sshfs'         : params => $params }
}

