class templates::system::main( $params = undef ) {
  class { 'lmsensors'   : params => $params }
# #  class { 'gdebi'       : params => $params }
  class { 'gddrescue'   : params => $params }
  class { 'tmux'        : params => $params }
  # class { 'tmuxinator'  : params => $params }
  class { 'tmuxp'       : params => $params }
  class { 'curl'        : params => $params }
  class { 'tree'        : params => $params }
  class { 'pdfgrep'     : params => $params }
  class { 'pngcrush'    : params => $params }
  class { 'nmap'        : params => $params }
  # class { 'trickle'     : params => $params }
  class { 'weatherutil' : params => $params }
  class { 'youtubedl'   : params => $params }
#  class { 'faad'        : params => $params }
  class { 'htop'        : params => $params }
  class { 'iftop'       : params => $params }
  class { 'iotop'       : params => $params }
  class { 'powertop'    : params => $params }
  class { 'fdupes'      : params => $params }
}

