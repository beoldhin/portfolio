class psd::os::ubuntu::addrepo ( $params = undef ) {
  $distribution = $params['distribution']
  $codename = $params['codename']
  # Unfortunately the apt::ppa package has bugs in Mint
  Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
  exec { 'addrepo-psd':
    command => 'add-apt-repository -y ppa:graysky/utils && apt-get update',
    creates =>
      "/etc/apt/sources.list.d/graysky-${distribution}-utils-${codename}.list",
  }
  package { 'profile-sync-daemon':
    ensure  => installed,
    require => Exec['addrepo-psd'],
  }
#  package { 'profile-cleaner':
#    ensure  => installed,
#    require => Exec['addrepo-psd'],
#  }
}

