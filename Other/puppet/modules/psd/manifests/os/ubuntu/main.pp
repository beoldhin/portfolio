define psd::os::ubuntu::main ( $params = undef ) {

  # NOTE: psd has been moved to regular package starting from Ubuntu 16.10

  package { 'profile-sync-daemon':
    ensure => installed
  }

  # class { 'psd::os::ubuntu::addrepo':
  #   params => $params
  # }
}

