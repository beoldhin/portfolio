define psd::os::arch::user ( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $distribution = $params['distribution']
  $configpath = "puppet:///modules/psd/os/${distribution}"
  file { "psd-config-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
#      Package['profile-sync-daemon'],
      File["config-dir-${user}"],
    ],
    path    => "${homepath}/.config/psd",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "psd-config-file-${user}":
    ensure  => present,
    require => [
      User[$user],
#      Package['profile-sync-daemon'],
      File["psd-config-dir-${user}"],
    ],
    path    => "${homepath}/.config/psd/psd.conf",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${configpath}/psd.conf",
  }
  # file { "config-chromium-dir-${user}":
  #   ensure => directory,
  #   path   => "/home/${user}/.config/chromium",
  #   mode   => '0700',
  #   owner  => $user,
  #   group  => $user,
  # }
  # file { "config-mozilla-dir-${user}":
  #   ensure => directory,
  #   path   => "/home/${user}/.mozilla",
  #   mode   => '0700',
  #   owner  => $user,
  #   group  => $user,
  # }
}

