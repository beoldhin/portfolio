class psd::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  psd::os::arch::user{ 'psd::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

