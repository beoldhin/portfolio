class psd::machines::azog::quetzal ( $user = undef, $params = undef ) {
  psd::os::arch::user{ 'psd::azog::quetzal':
    user   => $user,
    params => $params
  }
}

