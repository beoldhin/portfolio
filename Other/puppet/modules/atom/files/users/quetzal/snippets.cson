# Your snippets
#
# Atom snippets allow you to enter a simple prefix in the editor and hit tab to
# expand the prefix into a larger code block with templated values.
#
# You can create a new snippet in this file by typing "snip" and then hitting
# tab.
#
# An example CoffeeScript snippet to expand log to console.log:
#
# '.source.coffee':
#   'Console log':
#     'prefix': 'log'
#     'body': 'console.log $1'
#
# Each scope (e.g. '.source.coffee' above) can only be declared once.
#
# This file uses CoffeeScript Object Notation (CSON).
# If you are unfamiliar with CSON, you can read more about it in the
# Atom Flight Manual:
# http://flight-manual.atom.io/using-atom/sections/basic-customization/#_cson

'.text.bibtex':
  'Biblatex entry for online content':
    'prefix': 'xbibo'
    'body': """
      @Online{online:$1,
        Title        = {$2},
        Author       = {$3},
        Date         = {$4},
        Url          = {$5},
        Organization = {$6},
        Urldate      = {$7}
      }
      $8
    """
  'Biblatex entry for book content':
    'prefix': 'xbibb'
    'body': """
      @Book{book:$1,
        Title     = {$2},
        Author    = {$3},
        Year      = {$4},
        Edition   = {$5},
        Editor    = {$6},
        ISBN      = {$7},
        Pagetotal = {$8},
        Publisher = {$9},
        Subtitle  = {$10}
      }
      $11
    """

'.text.latex':
  'Epigraph (package epigraph)':
    'prefix': 'xepi'
    'body': """
      \\\\epigraph{$1}{$2}
      $0
    """
  'Table (package tabu)':
    'prefix': 'xtab'
    'body': """
      \\\\begin{tabu}[$1]
        \\\\toprule
        $2 \\\\\\\\
        \\\\midrule
        \\\\bottomrule
      \\\\end{tabu}
      $3
    """
  'Graphics (package graphicx)':
    'prefix': 'xgra'
    'body': """
      \\\\begin{figure}[H]
        \\\\centering
        \\\\includegraphics[width=$1\\\\linewidth]{$2}
        \\\\caption{$3}
      \\\\label{$4:$5}
      \\\\end{figure}
      $6
    """
  'Unnumbered alignment, used for vertically aligned equations in homeworks':
    'prefix': 'xali'
    'body': """
      \\\\begin{align*}
        $1
      \\\\end{align*}
      $2
    """
  'Unnumbered equation, used for referencing textbooks etc.':
    'prefix': 'xequ'
    'body': """
      \\\\begin{equation*}
        $1
      \\\\end{equation*}
      $2
    """
  'Fractional numbers':
    'prefix': 'xfra'
    'body': '\\\\frac{$1}{$2}$3'
  'Automatically resized absolute value':
    'prefix': 'xabs'
    'body': '\\\\xabs*{$1}$2'
  'Automatically resized parentheses':
    'prefix': 'xpar'
    'body': '\\\\xparen*{$1}$2'
  'Automatically resized braces':
    'prefix': 'xbrace'
    'body': '\\\\xbrace*{$1}$2'
  'Automatically resized brackets':
    'prefix': 'xbrack'
    'body': '\\\\xbrack*{$1}$2'
  'Cases for grouped equations':
    'prefix': 'xcas'
    'body': """
      \\\\begin{cases}
        $1
      \\\\end{cases}
      $2
    """
  'Overline, used for recurring decimals':
    'prefix': 'xove'
    'body': '\\\\overline{$1}$2'
  'Number format, used inside text blocks and SI':
    'prefix': 'xnum'
    'body': '\\\\num{$1}$2'
  'SI format, for value and unit':
    'prefix': 'xsia'
    'body': '\\\\SI{$1}{$2}$3'
  'SI format, for unit only':
    'prefix': 'xsib'
    'body': '\\\\si{$1}$2'
  'Square root of value':
    'prefix': 'xsra'
    'body': '\\\\sqrt{$1}$2'
  'Nth root of value':
    'prefix': 'xsrb'
    'body': '\\\\sqrt{$1}{$2}$3'
  'Predefined multiplication symbol (dot or cross)':
    'prefix': 'xmul'
    'body': '\\\\multx $1'
  'School: Intermediary state for multiplication':
    'prefix': 'xxmu'
    'body': '\\\\quad \\\\left| \\\\multx $1\\\\right. \\\\\\\\$2'
  'School: Intermediary state for division':
    'prefix': 'xxdi'
    'body': '\\\\quad \\\\left| \\\\div $1\\\\right. \\\\\\\\$2'
  'School: Expansion with ending rpar, expansion on left':
    'prefix': 'xexp'
    'body': '\\\\leftidx{^{\\\\left.$1\\\\right)}}{$2}{}$3'
  'School: Reduction with starting lpar, reduction on right':
    'prefix': 'xred'
    'body': '\\\\leftidx{}{$1}{^{\\\\left($2\\\\right.}}$3'
  'School: Intermediary note for GCD':
    'prefix': 'xgcd'
    'body': '\\\\quad \\\\left| \\\\text{gcd}\\\\left($1,$2\\\\right) = $3\\\\right. \\\\\\\\$4'
  'School: Intermediary note for LCM':
    'prefix': 'xlcm'
    'body': '\\\\quad \\\\left| \\\\text{lcm}\\\\left($1,$2\\\\right) = $3\\\\right. \\\\\\\\$4'
  'Sum (sigma)':
    'prefix': 'xsum'
    'body': '\\\\sum\\\\limits_{$1}^{$2} $3'
  'Todo comment (package todonotes)':
    'prefix': 'xtod'
    'body': '\\\\todo[color=$1!$2,inline]{$3}$4'
  'Autocite':
    'prefix': 'xaca'
    'body': '\\\\autocite{$1:$2}$3'
  'Autocite with page number reference':
    'prefix': 'xacb'
    'body': '\\\\autocite[$1]{$2:$3}$4'
  'Closed interval':
    'prefix': 'xina'
    'body': '\\\\interval{$1}{$2}$3'
  'Defined interval':
    'prefix': 'xinb'
    'body': '\\\\interval[$1]{$2}{$3}$4'
  'Italic text (for references without .bib)':
    'prefix': 'xtit'
    'body': '\\\\textit{$1}$2'
  'Emphasized text (for making bold etc.)':
    'prefix': 'xemp'
    'body': '\\\\emph{$1}$2'
  'Phantom text (for creating space but not showing)':
    'prefix': 'xpha'
    'body': '\\\\phantom{$1}$2'
  'Exponentiation':
    'prefix': 'xpow'
    'body': '^{$1}$2'
  'Footnote placeholder':
    'prefix': 'xfnm'
    'body': '\\\\onifnm{}$1'
  'Footnote content':
    'prefix': 'xfnt'
    'body': '\\\\onifnm{$1}$2'
  'Footnote add to counter':
    'prefix': 'xfnc'
    'body': '\\\\onifnc{$1}$2'
  'Basic quotation (package csquotes)':
    'prefix': 'xenq'
    'body': '\\\\enquote{$1}$2'
  'Block quote (package csquotes)':
    'prefix': 'xbqa'
    'body': '\\\\blockquote{$1}$2'
  'Block quote with author (package csquotes)':
    'prefix': 'xbqb'
    'body': '\\\\blockcquote[$1]{$2}{$3}$4'
  'Block highlight of text (package soul)':
    'prefix': 'xhil'
    'body': '\\\\hl{$1}$2'
  'Inline code block listing (package listings)':
    'prefix': 'xlsa'
    'body': """
      \\\\begin{lstlisting}[language=$1]
        $2
      \\\\end{lstlisting}
      $3
    """
  'File code block listing (package listings)':
    'prefix': 'xlsb'
    'body': '\\\\lstinputlisting[language=$1]{$2}$3'
  'Enumerated list':
    'prefix': 'xenu'
    'body': """
      \\\\begin{enumerate}
        \\\\item $1
      \\\\end{enumerate}
      $2
    """
  'Itemized list':
    'prefix': 'xite'
    'body': """
      \\\\begin{itemize}
        \\\\item $1
      \\\\end{itemize}
      $2
    """
  'Matrix (parentheses)':
    'prefix': 'xmap'
    'body': """
      \\\\begin{pmatrix}
        $1
      \\\\end{pmatrix}
      $2
    """
  'Matrix (bracketed)':
    'prefix': 'xmab'
    'body': """
      \\\\begin{bmatrix}
        $1
      \\\\end{bmatrix}
      $2
    """
  'Degree symbol in math mode':
    'prefix': 'xdeg'
    'body': '^{\\\\circ}$1'
  'Limits (lim) construct':
    'prefix': 'xlim'
    'body': '\\\\lim_{$1 \\\\to $2}$3'
  'Centi meter (siunitx)':
    'prefix': 'ycm'
    'body': '\\\\centi\\\\meter$1'
  'Gram per centimeter cubed (siunitx)':
    'prefix': 'ygpcmc'
    'body': '\\\\gram\\\\per\\\\centi\\\\meter\\\\cubed$1'
  'Kilocalorie (siunitx)':
    'prefix': 'ykc'
    'body': '\\\\kilo\\\\calorie$1'
  'Kilocalorie per hour (siunitx)':
    'prefix': 'ykcph'
    'body': '\\\\kilo\\\\calorie\\\\per\\\\hour$1'
  'Kilogram per meter cubed (siunitx)':
    'prefix': 'ykgpmc'
    'body': '\\\\kilo\\\\gram\\\\per\\\\meter\\\\cubed$1'
  'Kilometers per hour (siunitx)':
    'prefix': 'ykmph'
    'body': '\\\\kilo\\\\meter\\\\per\\\\hour$1'
  'Litre per second (siunitx)':
    'prefix': 'ylps'
    'body': '\\\\litre\\\\per\\\\second$1'
  'Meters cubed (siunitx)':
    'prefix': 'ymc'
    'body': '\\\\meter\\\\cubed$1'
  'Meters per second (siunitx)':
    'prefix': 'ymps'
    'body': '\\\\meter\\\\per\\\\second$1'
  'Meters per second squared (siunitx)':
    'prefix': 'ympss'
    'body': '\\\\meter\\\\per\\\\second\\\\squared$1'
  'Meters per second cubed (siunitx)':
    'prefix': 'ympsc'
    'body': '\\\\meter\\\\per\\\\second\\\\cubed$1'
  'Meters squared (siunitx)':
    'prefix': 'yms'
    'body': '\\\\meter\\\\squared$1'
  'Watt per meter squared (siunitx)':
    'prefix': 'ywpms'
    'body': '\\\\watt\\\\per\\\\meter\\\\squared$1'

