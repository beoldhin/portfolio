class atom::machines::azog::quetzal ( $user = undef, $params = undef ) {
  atom::os::arch::user{ 'atom::azog::quetzal':
    user   => $user,
    params => $params
  }
}

