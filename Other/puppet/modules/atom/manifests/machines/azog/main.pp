class atom::machines::azog::main( $params = undef ) {
  atom::os::arch::main{ 'atom::azog':
    params => $params
  }
}

