class atom::machines::gimli::main( $params = undef ) {
  atom::os::arch::main{ 'atom::gimli':
    params => $params
  }
}

