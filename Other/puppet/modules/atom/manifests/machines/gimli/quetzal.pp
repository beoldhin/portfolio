class atom::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  atom::os::arch::user{ 'atom::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

