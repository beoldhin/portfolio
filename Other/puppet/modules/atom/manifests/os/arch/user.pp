define atom::os::arch::user ( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $userpath = "puppet:///modules/atom/users/${user}"
  file { "atom-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['atom'],
    ],
    path    => "${homepath}/.atom",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "snippets.cson-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['atom'],
      File["atom-dir-${user}"],
    ],
    path    => "${homepath}/.atom/snippets.cson",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/snippets.cson"
  }
}

