class atom::os::ubuntu::addrepo ( $params = undef ) {
  $distribution = $params['distribution']
  $codename = $params['codename']
  # Unfortunately the apt::ppa package has bugs in Mint
  Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
  exec { 'addrepo-atom':
    command => 'add-apt-repository -y ppa:webupd8team/atom && apt-get update',
    creates => "/etc/apt/sources.list.d/webupd8team-${distribution}-atom-${codename}.list"
  }
  package { 'atom':
    ensure  => installed,
    require => Exec['addrepo-atom'],
  }
}

