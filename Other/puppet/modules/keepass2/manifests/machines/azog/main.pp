class keepass2::machines::azog::main ( $params = undef ) {
  keepass2::os::arch::main{ 'keepass2::azog':
    params => $params
  }
}

