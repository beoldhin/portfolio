define keepass2::os::ubuntu::main ( $params = undef ) {
  package { 'keepass2':
    ensure => installed
  }
}

