#!/usr/bin/env python

import sys
import subprocess
import datetime

import gnome
import gnome.ui
import gtk
import shlex

class Namespace: pass
ns = Namespace()
ns.dialog = None


def main():
    prog = gnome.init("gnome_save_yourself", "1.0", gnome.libgnome_module_info_get(), sys.argv, [])
    client = gnome.ui.master_client()
    #set up call back for when 'logout'/'Shutdown' button pressed
    client.connect("save-yourself", session_save_yourself)

def session_save_yourself(*args):
    subprocess.call(shlex.split('/usr/local/bin/killgroup chrome'))
    return True

main()
gtk.main()

