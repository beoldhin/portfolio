define base::os::ubuntu::bcm20702a0( $params = undef ) {
  $hcdfile = 'BCM20702A1-0b05-17cb.hcd'
  $assetpath = 'puppet:///modules/base/assets'
  file { 'bcm20702a1_hcd':
      ensure  => present,
      path    => "/lib/firmware/brcm/${hcdfile}",
      replace => yes,
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      source  => "${assetpath}/${hcdfile}"
  }
}

