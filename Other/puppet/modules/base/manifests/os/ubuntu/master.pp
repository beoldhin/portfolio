define base::os::ubuntu::master( $user = undef, $params = undef ) {
  $machinename = $params['machinename']
  $homepath = "/home/${user}"
  $unixpath = 'puppet:///modules/base/os/unix'
  $userpath = "puppet:///modules/base/machines/${machinename}/users/${user}"
  file { "dropbox_backup-${user}":
    ensure  => present,
    require => User[$user],
    path    => "${homepath}/dropbox_backup",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/dropbox_backup",
  }
  file { "initws_puppet-${user}":
    ensure  => present,
    require => User[$user],
    path    => "${homepath}/initws_puppet",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/initws_puppet"
  }
}

