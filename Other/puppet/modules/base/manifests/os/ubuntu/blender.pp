define base::os::ubuntu::blender( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $baseospath = 'base/os/ubuntu'
  $assetpath = 'puppet:///modules/base/assets'
  file { "blender.desktop-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["local-share-applications-dir-${user}"],
    ],
    path    => "${homepath}/.local/share/applications/blender.desktop",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    content => epp("${baseospath}/blender.desktop.epp", {
     'user' => $user }
    )
  }
  file { "blenderlogo-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/blender.png",
    replace => yes,
    mode    => '0640',
    owner   => $user,
    group   => $user,
    source  => "${assetpath}/blender.png",
  }
}

