define base::os::ubuntu::xonotic( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $baseospath = 'base/os/ubuntu'
  $assetpath = 'puppet:///modules/base/assets'
  file { "xonotic.desktop-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["local-share-applications-dir-${user}"],
    ],
    path    => "${homepath}/.local/share/applications/xonotic.desktop",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    content => epp("${baseospath}/xonotic.desktop.epp", {
     'user' => $user }
    )
  }
  file { "xonoticlogo-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/xonotic.png",
    replace => yes,
    mode    => '0640',
    owner   => $user,
    group   => $user,
    source  => "${assetpath}/xonotic.png",
  }
}

