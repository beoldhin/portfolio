define base::os::ubuntu::rsync(
$user = undef,
$params = undef,
$dataowner = undef ) {
  $machinename = $params['machinename']
  $distribution = $params['distribution']
  $backup_dest_host = $params['backup_dest_host']
  $backup_dest_user = $params['backup_dest_user']
  $backup_data_owner = $params['backup_data_owner']
  $homepath = "/home/${user}"
  $unixpath = "puppet:///modules/base/os/${distribution}"
  $userpath = "puppet:///modules/base/machines/${machinename}/users/${user}"
  file { 'rsync_backup':
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/rsync_backup",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    content => epp("base/os/${distribution}/rsync_backup.epp", {
      'backup_dest_host' => $backup_dest_host,
      'backup_dest_user' => $backup_dest_user }
    )
  }
  file { 'rsync_mirror':
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/rsync_mirror",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    content => epp("base/os/${distribution}/rsync_mirror.epp", {
      'backup_dest_user' => $backup_dest_user }
    )
  }
  file { 'rsync_restore':
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/rsync_restore",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    content => epp("base/os/${distribution}/rsync_restore.epp", {
      'backup_dest_host' => $backup_dest_host,
      'backup_dest_user' => $backup_dest_user }
    )
  }
  file { 'rsync_crestore':
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"],
    ],
    path    => "${homepath}/Binaries/rsync_crestore",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    content => epp("base/os/${distribution}/rsync_crestore", {
      'backup_dest_host'  => $backup_dest_host,
      'backup_dest_user'  => $backup_dest_user,
      'backup_data_owner' => $backup_data_owner }
    )
  }
  file { "rsync_includelist-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"],
    ],
    path    => "${homepath}/Binaries/rsync_includelist",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/rsync_includelist",
  }
  file { "rsync_cincludelist-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"],
    ],
    path    => "${homepath}/Binaries/rsync_cincludelist",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/rsync_cincludelist",
  }
  file { 'rsync_setperms':
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"],
    ],
    path    => "${homepath}/Binaries/rsync_setperms",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/rsync_setperms",
  }
}

