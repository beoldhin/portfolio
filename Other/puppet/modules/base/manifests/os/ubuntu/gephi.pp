define base::os::ubuntu::gephi( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $baseospath = 'base/os/ubuntu'
  $assetpath = 'puppet:///modules/base/assets'
  file { "gephi.desktop-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["local-share-applications-dir-${user}"],
    ],
    path    => "${homepath}/.local/share/applications/gephi.desktop",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    content => epp("${baseospath}/gephi.desktop.epp", {
     'user' => $user }
    )
  }
  file { "gephilogo-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/gephi.png",
    replace => yes,
    mode    => '0640',
    owner   => $user,
    group   => $user,
    source  => "${assetpath}/gephi.png",
  }
}

