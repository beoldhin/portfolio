define base::os::ubuntu::graph( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $unixpath = 'puppet:///modules/base/os/unix'
  file { "clean_chrome-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"],
    ],
    path    => "${homepath}/Binaries/clean_chrome",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/clean_chrome",
  }
  base::os::ubuntu::blender{ "base::blender::${user}":
    user   => $user,
    params => $params
  }
  base::os::ubuntu::cmap{ "base::cmap::${user}":
    user   => $user,
    params => $params
  }
  base::os::ubuntu::gephi{ "base::gephi::${user}":
    user   => $user,
    params => $params
  }
  base::os::ubuntu::xonotic{ "base::xonotic::${user}":
    user   => $user,
    params => $params
  }
}

