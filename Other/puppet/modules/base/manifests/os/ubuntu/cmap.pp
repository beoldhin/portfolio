define base::os::ubuntu::cmap( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $baseospath = 'base/os/ubuntu'
  $assetpath = 'puppet:///modules/base/assets'
  file { "cmaptools.desktop-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["local-share-applications-dir-${user}"],
    ],
    path    => "${homepath}/.local/share/applications/cmaptools.desktop",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    content => epp("${baseospath}/cmaptools.desktop.epp", {
     'user' =>  $user }
    )
  }
  file { "cmaplogo-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"],
    ],
    path    => "${homepath}/Binaries/cmap.png",
    replace => yes,
    mode    => '0640',
    owner   => $user,
    group   => $user,
    source  => "${assetpath}/cmap.png",
  }
}

