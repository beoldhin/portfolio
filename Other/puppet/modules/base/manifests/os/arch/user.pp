define base::os::arch::user( $user = undef, $params = undef ) {
  $machinename = $params['machinename']
  $distribution = $params['distribution']
  $homepath = "/home/${user}"
  $unixpath = 'puppet:///modules/base/os/unix'
  $distpath = "puppet:///modules/base/os/${distribution}"
  $userpath = "puppet:///modules/base/machines/${machinename}/users/${user}"
  file { "bashrc-${user}":
    ensure  => present,
    require => User[$user],
    path    => "${homepath}/.bashrc",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/bashrc",
  }
  file { "bash_aliases-${user}":
    ensure  => present,
    require => User[$user],
    path    => "${homepath}/.bash_aliases",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/bash_aliases",
  }
  file { "bash_logout-${user}":
    ensure  => present,
    require => User[$user],
    path    => "${homepath}/.bash_logout",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${distpath}/bash_logout",
  }
  file { "profile-${user}":
    ensure  => present,
    require => User[$user],
    path    => "${homepath}/.profile",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/profile"
  }
  file { "latexmk-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      File["config-dir-${user}"],
    ],
    path    => "${homepath}/.config/latexmk",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "latexmkrc-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["latexmk-dir-${user}"],
    ],
    path    => "${homepath}/.config/latexmk/latexmkrc",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/users/${user}/latexmkrc",
  }
  file { "binaries-dir-${user}":
    ensure  => directory,
    require => User[$user],
    path    => "${homepath}/Binaries",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "icons-dir-${user}":
    ensure   => directory,
    require  => [
      User[$user],
      File["binaries-dir-${user}"],
    ],
    path     => "${homepath}/Binaries/Icons",
    mode     => '0755',
    owner    => $user,
    group    => $user,
  }
  file { "projects-dir-${user}":
    ensure  => directory,
    require => User[$user],
    path    => "${homepath}/Projects",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "projects-git-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      File["projects-dir-${user}"],
    ],
    path    => "${homepath}/Projects/git",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  vcsrepo { "${homepath}/Projects/git/anything-sync-daemon":
    ensure   => present,
    require  => [
      User[$user],
      Package['git'],
      File["projects-git-dir-${user}"],
    ],
    provider => git,
    source   => 'https://github.com/graysky2/anything-sync-daemon.git',
    revision => 'master',
    user     => $user,
  }
  file { "mirrored-dir-${user}":
    ensure  => directory,
    require => User[$user],
    path    => "${homepath}/Mirrored",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  # Note: The public directory is needed by bindfs,
  # otherwise mount will fail
  file { "public-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      File["mirrored-dir-${user}"],
    ],
    path    => "${homepath}/Mirrored/PUBLIC",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "update_git-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/update_git",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/update_git",
  }
  file { "ps_mem.py-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/ps_mem.py",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/ps_mem.py",
  }
  file { "stopwatch-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/stopwatch",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/stopwatch",
  }
  file { "clean_old-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/clean_old",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${distpath}/clean_old",
  }
  file { "launcher-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/launcher",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/launcher",
  }
  file { "fontgen-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/fontgen",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/fontgen",
  }
  file { "apmtester-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/apmtester",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/apmtester",
  }
  file { "setradeon-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/set_radeon",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${distpath}/set_radeon",
  }
  file { "create_CookSupply-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/create_CookSupply",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/create_CookSupply",
  }
}

