define base::os::arch::root( $user = undef, $params = undef ) {
  $machinename = $params['machinename']
  $userpath = "puppet:///modules/base/machines/${machinename}/users/root"
  file { "bashrc-root":
    ensure  => present,
    path    => '/root/.bashrc',
    replace => yes,
    mode    => '0644',
    owner   => root,
    group   => root,
    source  => "${userpath}/bashrc",
  }
  file { "profile-root":
    ensure  => present,
    path    => '/root/.profile',
    replace => yes,
    mode    => '0644',
    owner   => root,
    group   => root,
    source  => "${userpath}/profile",
  }
}

