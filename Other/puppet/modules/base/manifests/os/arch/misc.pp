define base::os::arch::misc( $user = undef, $params = undef ) {
  $unixpath = 'puppet:///modules/base/os/unix'
  file { "encedit":
    ensure  => present,
    path    => "/usr/local/bin/encedit",
    replace => yes,
    mode    => '0755',
    owner   => root,
    group   => root,
    source  => "${unixpath}/encedit"
  }
  file { "killgroup":
    ensure  => present,
    path    => "/usr/local/bin/killgroup",
    replace => yes,
    mode    => '0755',
    owner   => root,
    group   => root,
    source  => "${unixpath}/killgroup"
  }
  file { "shopcheck":
    ensure  => present,
    path    => "/usr/local/bin/shopcheck",
    replace => yes,
    mode    => '0755',
    owner   => root,
    group   => root,
    source  => "${unixpath}/shopcheck"
  }
  file { "cardinal":
    ensure  => present,
    path    => "/usr/local/bin/cardinal",
    replace => yes,
    mode    => '0755',
    owner   => root,
    group   => root,
    source  => "${unixpath}/cardinal"
  }
  file { "chksysupd":
    ensure  => present,
    path    => "/usr/local/bin/chksysupd",
    replace => yes,
    mode    => '0755',
    owner   => root,
    group   => root,
    source  => "${unixpath}/chksysupd"
  }
  file { "mklatexstub":
    ensure  => present,
    path    => "/usr/local/bin/mklatexstub",
    replace => yes,
    mode    => '0755',
    owner   => root,
    group   => root,
    source  => "${unixpath}/mklatexstub"
  }
}

