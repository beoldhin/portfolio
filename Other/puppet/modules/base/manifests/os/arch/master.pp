define base::os::arch::master( $user = undef, $params = undef ) {
  $machinename = $params['machinename']
  $homepath = "/home/${user}"
  $unixpath = 'puppet:///modules/base/os/unix'
  $userpath = "puppet:///modules/base/machines/${machinename}/users/${user}"
  file { "backup_admin-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/backup_admin",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/backup_admin",
  }
  file { "initws_puppet-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["binaries-dir-${user}"]
    ],
    path    => "${homepath}/Binaries/initws_puppet",
    replace => yes,
    mode    => '0744',
    owner   => $user,
    group   => $user,
    source  => "${unixpath}/initws_puppet"
  }
}

