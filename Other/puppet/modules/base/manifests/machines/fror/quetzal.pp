class base::machines::fror::quetzal ( $user = undef, $params = undef ) {
  $identifier = '::fror::quetzal'
  base::os::ubuntu::user{ "base::user${identifier}":
    user   => $user,
    params => $params
  }
}

