# Generated file, DO NOT EDIT
define base::machines::fror::hosts( $params = undef ) {
  $machinename = $params['machinename']
  file { 'hostname':
    ensure  => present,
    path    => '/etc/hostname',
    replace => yes,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => "${machinename}\n",
  }
  host { "hosts-${machinename}":
    ensure       => present,
    name         => "${machinename}.gondor.org",
    host_aliases => $machinename,
    ip           => '127.0.1.1',
  }
}
