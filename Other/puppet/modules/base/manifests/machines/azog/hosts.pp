# Generated file, DO NOT EDIT
define base::machines::azog::hosts( $params = undef ) {
  $machinename = $params['machinename']
  file { 'hostname':
    ensure  => present,
    path    => '/etc/hostname',
    replace => yes,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => "${machinename}\n",
  }
  host { "hosts-${machinename}":
    ensure       => present,
    name         => "${machinename}.gondor.org",
    host_aliases => $machinename,
    ip           => '127.0.1.1',
  }
  host { 'hosts-fror':
    ensure       => present,
    require      => Host["hosts-${machinename}"],
    name         => 'fror.gondor.org',
    host_aliases => 'fror',
    ip           => $::ip_fror,
  }
  host { 'hosts-aldor':
    ensure       => present,
    require      => Host['hosts-fror'],
    name         => 'aldor.gondor.org',
    host_aliases => 'aldor',
    ip           => $::ip_aldor,
  }
  host { 'hosts-gimli':
    ensure       => present,
    require      => Host['hosts-aldor'],
    name         => 'gimli.gondor.org',
    host_aliases => 'gimli',
    ip           => $::ip_gimli,
  }
}
