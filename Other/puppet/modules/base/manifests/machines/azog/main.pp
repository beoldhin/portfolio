class base::machines::azog::main( $params = undef ) {
  base::os::arch::root{ "base::root":
    user   => $user,
    params => $params
  }
  base::machines::azog::hosts{ 'base::azog::hosts':
    params => $params
  }
  # When runnig Arch, use AUR package "bcm20702a1-firmware" instead!
  # base::os::ubuntu::bcm20702a0{ 'base::bcm20702a0':
  #   params => $params
  # }
}

