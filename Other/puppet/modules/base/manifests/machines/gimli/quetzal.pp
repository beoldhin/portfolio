class base::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  $identifier = '::gimli::quetzal'
  $dataowner = 'quetzal'
  base::os::arch::user{ "base::user${identifier}":
    user   => $user,
    params => $params
  }
  base::os::arch::rsync{ "base::rsync${identifier}":
    user      => $user,
    params    => $params,
    dataowner => $dataowner
  }
  base::os::arch::graph{ "base::graph${identifier}":
    user   => $user,
    params => $params
  }
  base::os::arch::misc{ "base::misc${identifier}":
    user   => $user,
    params => $params
  }
}

