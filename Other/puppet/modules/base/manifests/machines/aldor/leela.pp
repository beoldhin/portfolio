class base::machines::aldor::leela ( $user = undef, $params = undef ) {
  $identifier = '::aldor::leela'
  base::os::ubuntu::user{ "base::user${identifier}":
    user   => $user,
    params => $params
  }
  base::os::ubuntu::graph{ "base::user${identifier}":
    user   => $user,
    params => $params
  }
}

