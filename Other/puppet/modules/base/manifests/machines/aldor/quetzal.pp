class base::machines::aldor::quetzal ( $user = undef, $params = undef ) {
  $identifier = '::aldor::quetzal'
  $dataowner = 'leela'
  base::os::ubuntu::user{ "base::user${identifier}":
    user   => $user,
    params => $params
  }
  base::os::ubuntu::rsync{ "base::rsync${identifier}":
    user      => $user,
    params    => $params,
    dataowner => $dataowner
  }
  base::os::ubuntu::graph{ "base::graph${identifier}":
    user   => $user,
    params => $params
  }
}

