class nmap ( $params = undef ) {
  $machinename = $params['machinename']
  $mainclassdef = "${name}::machines::${machinename}::main"
  if defined( $mainclassdef ) {
    class { $mainclassdef: params => $params }
  }
  else {
    notify { "Skipped machine \"${machinename}\"": }
  }
}

