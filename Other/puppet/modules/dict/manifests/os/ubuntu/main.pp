define dict::os::ubuntu::main ( $params = undef ) {
  package { 'translate-shell':
    ensure => installed
  }
  package { 'dict':
    ensure => installed
  }
  package { 'dictd':
    ensure => installed
  }
  package { 'dict-freedict-eng-rus':
    ensure => installed
  }
  package { 'dict-freedict-eng-swe':
    ensure => installed
  }
  package { 'libtranslate-bin':
    ensure => installed
  }
  package { 'dict-gcide':
    ensure => installed
  }
  package { 'dict-wn':
    ensure => installed
  }
  package { 'dict-devil':
    ensure => installed
  }
  package { 'dict-moby-thesaurus':
    ensure => installed
  }
}

