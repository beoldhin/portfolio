define opensshclient::os::ubuntu::main ( $params = undef ) {
  $machinename = $params['machinename']
  $origmachine = $params['origmachine']
  $configpath = "puppet:///modules/opensshclient/machines/${machinename}/users/root"
  package { 'openssh-client':
    ensure => installed
  }
  file { 'sshclient-ssh-dir':
    ensure  => directory,
    path    => '/root/.ssh',
    mode    => '0700',
    owner   => 'root',
    group   => 'root',
    recurse => true,
    purge   => true,
  }
  # Let's manage known_hosts, otherwise it will be deleted
  file { 'known_hosts':
    ensure  => present,
    path    => '/root/.ssh/known_hosts',
    replace => no,
    mode    => '0600',
    owner   => 'root',
    group   => 'root',
  }
  file { 'sshclient-config-root':
    ensure  => present,
    require => File['sshclient-ssh-dir'],
    path    => '/root/.ssh/config',
    replace => yes,
    mode    => '0600',
    owner   => 'root',
    group   => 'root',
    source  => "${configpath}/config",
  }
  $privatekeys = hiera("privatekeys-from-${origmachine}-root")
  each($privatekeys) |$value| {
    $tonames = split($value[0], '-')
    $todef = $tonames[0]
    if $todef == 'to' {  # skip if temporarily disabled with "toNo"
      $toname = $tonames[1]
      $touser = $tonames[2]
      $privatedata = $value[1]
      $filename = "/root/.ssh/id_rsa_${origmachine}_to_${toname}_${touser}"
      file { $filename:
        require => File['sshclient-ssh-dir'],
        path    => $filename,
        replace => yes,
        mode    => '0600',
        owner   => 'root',
        group   => 'root',
        content => $privatedata
      }
    }
  }
  # NOTE: No publickeys for root by purpose
}

