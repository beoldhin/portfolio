define opensshclient::os::ubuntu::user ( $user = undef, $params = undef ) {
  $machinename = $params['machinename']
  $origmachine = $params['origmachine']
  $homepath = "/home/${user}"
  $configpath = "puppet:///modules/opensshclient/machines/${machinename}/users/${user}"
  $authpath = "puppet:///modules/opensshclient/machines/${origmachine}/users/${user}"
  file { "sshclient-ssh-dir-${user}":
    ensure  => directory,
    require => User[$user],
    path    => "${homepath}/.ssh",
    mode    => '0600',
    owner   => $user,
    group   => $user,
    recurse => true,
    purge   => true,
  }
  # Let's manage known_hosts, otherwise it will be deleted (purge => true)
  file { "known_hosts-${user}":
    ensure  => present,
    require => User[$user],
    path    => "${homepath}/.ssh/known_hosts",
    replace => no,
    mode    => '0600',
    owner   => $user,
    group   => $user,
  }
  file { "authorized_keys-${origmachine}-${user}":
    ensure  => present,
    require => User[$user],
    path    => "${homepath}/.ssh/authorized_keys",
    replace => yes,
    mode    => '0600',
    owner   => $user,
    group   => $user,
    source  => "${authpath}/authorized_keys",
  }
  file { "sshclient-config-${user}":
    ensure  => present,
    require => [
      User[$user],
      File["sshclient-ssh-dir-${user}"],
    ],
    path    => "${homepath}/.ssh/config",
    replace => yes,
    mode    => '0600',
    owner   => $user,
    group   => $user,
    source  => "${configpath}/config",
  }
  $privatekeys = hiera("privatekeys-from-${origmachine}-${user}")
  each($privatekeys) |$value| {
    $tonames = split($value[0], '-')
    $todef = $tonames[0]
    if $todef == 'to' {  # skip if temporarily disabled with "toNo" (otpw)
      $toname = $tonames[1]
      if size($tonames) == 3 {
        $touser = $tonames[2]
      } else {
        $touser = ''
      }
      $privatedata = $value[1]
      if $user == $touser {
        $filename = "id_rsa_${origmachine}_to_${toname}"
      } else {
        if $touser != '' {
          $filename = "id_rsa_${origmachine}_to_${toname}_${touser}"
        } else {
          $filename = "id_rsa_${origmachine}_to_${toname}"
        }
      }
      file { $filename:
        require => [
          User[$user],
          File["sshclient-ssh-dir-${user}"],
        ],
        path    => "${homepath}/.ssh/${filename}",
        replace => yes,
        mode    => '0600',
        owner   => $user,
        group   => $user,
        content => $privatedata
      }
    }
  }
  $publickeys = hiera("publickeys-to-${origmachine}-${user}")
  each ($publickeys) |$value| {
    $fromnames = split($value[0], '-')
    $fromname = $fromnames[1]
    if size($fromnames) == 3 {
      $fromuser = $fromnames[2]
    } else {
      $fromuser = ''
    }
    $publicdata = $value[1]
    if $user == $fromuser {
      $filename = "id_rsa_${fromname}_to_${origmachine}.pub"
    } else {
      if $fromuser != '' {
        $filename = "id_rsa_${fromname}_to_${origmachine}_${fromuser}.pub"
      } else {
        $filename = "id_rsa_${fromname}_to_${origmachine}.pub"
      }
    }
    file { $filename:
      require => [
        User[$user],
        File["sshclient-ssh-dir-${user}"],
      ],
      path    => "${homepath}/.ssh/${filename}",
      replace => yes,
      mode    => '0644',
      owner   => $user,
      group   => $user,
      content => $publicdata
    }
  }
}

