class opensshclient::machines::haldar::quetzal(
$user = undef,
$params = undef ) {
  opensshclient::os::ubuntu::user{ 'opensshclient::haldar::quetzal':
    user   => $user,
    params => $params
  }
}

