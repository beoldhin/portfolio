class opensshclient::machines::aldor::quetzal(
$user = undef,
$params = undef ) {
  opensshclient::os::ubuntu::user{ 'opensshclient::aldor::quetzal':
    user   => $user,
    params => $params
  }
}

