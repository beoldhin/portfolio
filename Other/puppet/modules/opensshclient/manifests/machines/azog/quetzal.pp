class opensshclient::machines::azog::quetzal(
$user = undef,
$params = undef ) {
  opensshclient::os::arch::user{ 'opensshclient::azog::quetzal':
    user   => $user,
    params => $params
  }
}

