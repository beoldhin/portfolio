class opensshclient::machines::gimli::quetzal(
$user = undef,
$params = undef ) {
  opensshclient::os::arch::user{ 'opensshclient::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

