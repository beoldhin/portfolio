class opensshclient::machines::beril::quetzal(
$user = undef,
$params = undef ) {
  opensshclient::os::ubuntu::user{ 'opensshclient::beril::quetzal':
    user   => $user,
    params => $params
  }
}

