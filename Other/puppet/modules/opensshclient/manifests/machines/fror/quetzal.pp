class opensshclient::machines::fror::quetzal(
$user = undef,
$params = undef ) {
  opensshclient::os::ubuntu::user{ 'opensshclient::fror::quetzal':
    user   => $user,
    params => $params
  }
}

