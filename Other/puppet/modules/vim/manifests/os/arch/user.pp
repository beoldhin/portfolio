define vim::os::arch::user ( $user = undef, $params = undef ) {
  $homepath = "/home/${user}"
  $userpath = "puppet:///modules/vim/users/${user}"
  file { "vim-vim-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vim'],
    ],
    path    => "${homepath}/.vim",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "vim-tmp-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vim'],
      File["vim-vim-dir-${user}"],
    ],
    path    => "${homepath}/.vim/tmp",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "vim-backup-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vim'],
      File["vim-vim-dir-${user}"],
    ],
    path    => "${homepath}/.vim/backup",
    replace => no,
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "vim-ftdetect-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vim'],
      File["vim-vim-dir-${user}"],
    ],
    path    => "${homepath}/.vim/ftdetect",
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "vim-undo-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vim'],
      File["vim-vim-dir-${user}"],
    ],
    path    => "${homepath}/.vim/undo",
    replace => no,
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "vim-ultisnips-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['vim'],
      File["vim-vim-dir-${user}"],
    ],
    path    => "${homepath}/.vim/UltiSnips",
    replace => no,
    mode    => '0755',
    owner   => $user,
    group   => $user,
  }
  file { "vimrc-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vim'],
      Package['curl'],
      Package['git'],
    ],
    path    => "${homepath}/.vimrc",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/vimrc",
  }
  file { "tex.vim-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vim'],
      File["vim-ftdetect-dir-${user}"],
    ],
    path    => "${homepath}/.vim/ftdetect/tex.vim",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/tex.vim",
  }
  file { "Rnw.vim-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vim'],
      File["vim-ftdetect-dir-${user}"],
    ],
    path    => "${homepath}/.vim/ftdetect/Rnw.vim",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/Rnw.vim",
  }
  file { "tex.snippets-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vim'],
      File["vim-ultisnips-dir-${user}"],
    ],
    path    => "${homepath}/.vim/UltiSnips/tex.snippets",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/tex.snippets",
  }
  file { "text.snippets-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vim'],
      File["vim-ultisnips-dir-${user}"],
    ],
    path    => "${homepath}/.vim/UltiSnips/text.snippets",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/text.snippets",
  }
  file { "bib.snippets-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['vim'],
      File["vim-ultisnips-dir-${user}"],
    ],
    path    => "${homepath}/.vim/UltiSnips/bib.snippets",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/bib.snippets",
  }
}

