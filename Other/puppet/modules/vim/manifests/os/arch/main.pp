define vim::os::arch::main ( $params = undef ) {
  package { 'vim':
    ensure  => installed,
    require => Package['ctags'],
  }
  package { 'vimpager':
    ensure  => installed,
    require => Package['vim'],
  }
}

