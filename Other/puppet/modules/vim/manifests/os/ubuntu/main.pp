define vim::os::ubuntu::main ( $params = undef ) {
  package { 'vim-nox':
    ensure  => installed,
    require => Package['exuberant-ctags'],
  }
}

