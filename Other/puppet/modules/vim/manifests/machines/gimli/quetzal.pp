class vim::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  vim::os::arch::user{ 'vim::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

