class vim::machines::gimli::main ( $params = undef ) {
  vim::os::arch::main{ 'vim::gimli':
    params => $params
  }
}

