class vim::machines::fror::main ( $params = undef ) {
  vim::os::ubuntu::main{ 'vim::fror':
    params => $params
  }
}

