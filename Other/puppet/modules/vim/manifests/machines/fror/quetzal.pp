class vim::machines::fror::quetzal ( $user = undef, $params = undef ) {
  vim::os::ubuntu::user{ 'vim::fror::quetzal':
    user   => $user,
    params => $params
  }
}

