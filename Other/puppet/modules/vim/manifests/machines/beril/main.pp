class vim::machines::beril::main ( $params = undef ) {
  vim::os::ubuntu::main{ 'vim::beril':
    params => $params
  }
}

