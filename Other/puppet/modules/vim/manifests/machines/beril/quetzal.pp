class vim::machines::beril::quetzal ( $user = undef, $params = undef ) {
  vim::os::ubuntu::user{ 'vim::beril::quetzal':
    user   => $user,
    params => $params
  }
}

