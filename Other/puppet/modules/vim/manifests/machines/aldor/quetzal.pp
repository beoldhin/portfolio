class vim::machines::aldor::quetzal ( $user = undef, $params = undef ) {
  vim::os::ubuntu::user{ 'vim::aldor::quetzal':
    user   => $user,
    params => $params
  }
}

