class vim::machines::aldor::main ( $params = undef ) {
  vim::os::ubuntu::main{ 'vim::aldor':
    params => $params
  }
}

