class vim::machines::haldar::quetzal ( $user = undef, $params = undef ) {
  vim::os::ubuntu::user{ 'vim::haldar::quetzal':
    user   => $user,
    params => $params
  }
}

