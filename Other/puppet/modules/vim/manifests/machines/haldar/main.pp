class vim::machines::haldar::main ( $params = undef ) {
  vim::os::ubuntu::main{ 'vim::haldar':
    params => $params
  }
}

