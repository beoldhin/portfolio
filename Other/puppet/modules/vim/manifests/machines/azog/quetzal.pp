class vim::machines::azog::quetzal ( $user = undef, $params = undef ) {
  vim::os::arch::user{ 'vim::azog::quetzal':
    user   => $user,
    params => $params
  }
}

