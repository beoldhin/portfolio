class vim::machines::azog::main ( $params = undef ) {
  vim::os::arch::main{ 'vim::azog':
    params => $params
  }
}

