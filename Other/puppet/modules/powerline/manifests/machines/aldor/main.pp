class powerline::machines::aldor::main ( $params = undef ) {
  powerline::os::ubuntu::main{ 'powerline::aldor':
    params => $params
  }
}

