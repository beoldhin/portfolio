class powerline::machines::aldor::quetzal( $user = undef, $params = undef ) {
  powerline::os::ubuntu::user{ 'powerline::aldor::quetzal':
    user   => $user,
    params => $params
  }
}

