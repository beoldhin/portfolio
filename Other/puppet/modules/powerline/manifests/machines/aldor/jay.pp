class powerline::machines::aldor::gilbert( $user = undef, $params = undef ) {
  powerline::os::ubuntu::user{ 'powerline::aldor::gilbert':
    user   => $user,
    params => $params
  }
}

