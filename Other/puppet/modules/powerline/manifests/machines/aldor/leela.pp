class powerline::machines::aldor::leela( $user = undef, $params = undef ) {
  powerline::os::ubuntu::user{ 'powerline::aldor::leela':
    user   => $user,
    params => $params
  }
}

