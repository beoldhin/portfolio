class powerline::machines::haldar::main ( $params = undef ) {
  powerline::os::ubuntu::main{ 'powerline::haldar':
    params => $params
  }
}

