class powerline::machines::haldar::quetzal( $user = undef, $params = undef ) {
  powerline::os::ubuntu::user{ 'powerline::haldar::quetzal':
    user   => $user,
    params => $params
  }
}

