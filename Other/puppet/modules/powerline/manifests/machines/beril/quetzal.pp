class powerline::machines::beril::quetzal( $user = undef, $params = undef ) {
  powerline::os::ubuntu::user{ 'powerline::beril::quetzal':
    user   => $user,
    params => $params
  }
}

