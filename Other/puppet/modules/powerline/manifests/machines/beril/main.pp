class powerline::machines::beril::main ( $params = undef ) {
  powerline::os::ubuntu::main{ 'powerline::beril':
    params => $params
  }
}

