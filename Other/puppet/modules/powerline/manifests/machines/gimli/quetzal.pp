class powerline::machines::gimli::quetzal( $user = undef, $params = undef ) {
  powerline::os::arch::user{ 'powerline::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

