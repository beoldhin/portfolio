class powerline::machines::gimli::main ( $params = undef ) {
  powerline::os::arch::main{ 'powerline::gimli':
    params => $params
  }
}

