class powerline::machines::azog::main ( $params = undef ) {
  powerline::os::arch::main{ 'powerline::azog':
    params => $params
  }
}

