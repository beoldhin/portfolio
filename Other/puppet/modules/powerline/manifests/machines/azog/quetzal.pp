class powerline::machines::azog::quetzal( $user = undef, $params = undef ) {
  powerline::os::arch::user{ 'powerline::azog::quetzal':
    user   => $user,
    params => $params
  }
}

