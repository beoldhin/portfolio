define powerline::os::ubuntu::user( $user = undef, $params = undef ) {
  $codename = $params['codename']
  $homepath = "/home/${user}"
  $configpath = "puppet:///modules/powerline/os/ubuntu/${codename}"
  file { "powerline-powerline-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['powerline'],
      File["config-dir-${user}"],
    ],
    path    => "${homepath}/.config/powerline",
    mode    => '0775',
    owner   => $user,
    group   => $user,
  }
  file { "config-json-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['powerline'],
      File["powerline-powerline-dir-${user}"],
    ],
    path    => "${homepath}/.config/powerline/config.json",
    replace => yes,
    mode    => '0664',
    owner   => $user,
    group   => $user,
    source  => "${configpath}/config.json",
  }
}

