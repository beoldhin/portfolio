define powerline::os::ubuntu::main ( $params = undef ) {
  package { 'python3-powerline':
    ensure => installed
  }
  package { 'powerline':
    ensure  => installed,
    require => Package['python3-powerline'],
  }
  package { 'fonts-powerline':
    ensure  => installed,
    require => Package['powerline'],
  }
}

