define powerline::os::arch::main ( $params = undef ) {
  package { 'powerline':
    ensure => installed
  }
  package { 'powerline-common':
    ensure => installed
  }
  package { 'powerline-fonts':
    ensure => installed
  }
  package { 'powerline-vim':
    ensure => installed
  }
  package { 'python-powerline':
    ensure =>  installed
  }
}

