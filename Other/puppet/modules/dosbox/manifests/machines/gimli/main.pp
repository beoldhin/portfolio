class dosbox::machines::gimli::main ( $params = undef ) {
  dosbox::os::arch::main{ 'dosbox::gimli':
    params => $params
  }
}

