class dosbox::machines::gimli::quetzal ( $user = undef, $params = undef ) {
  dosbox::os::arch::user{ 'dosbox::gimli::quetzal':
    user   => $user,
    params => $params
  }
}

