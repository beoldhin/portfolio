class dosbox::machines::azog::main ( $params = undef ) {
  dosbox::os::arch::main{ 'dosbox::azog':
    params => $params
  }
}

