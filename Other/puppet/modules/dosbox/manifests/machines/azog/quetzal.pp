class dosbox::machines::azog::quetzal ( $user = undef, $params = undef ) {
  dosbox::os::arch::user{ 'dosbox::azog::quetzal':
    user   => $user,
    params => $params
  }
}

