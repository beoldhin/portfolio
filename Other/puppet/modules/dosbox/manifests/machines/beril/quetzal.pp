class dosbox::machines::beril::quetzal ( $user = undef, $params = undef ) {
  dosbox::os::ubuntu::user{ 'dosbox::beril::quetzal':
    user   => $user,
    params => $params
  }
}

