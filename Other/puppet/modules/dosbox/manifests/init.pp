class dosbox ( $params = undef ) {
  $users = $params['users']
  $machinename = $params['machinename']
  $dosboxver = $params['dosboxver']
  $mainclassdef = "${name}::machines::${machinename}::main"
  if defined( $mainclassdef ) {
    class { $mainclassdef: params => $params }
  }
  else {
    notify { "Skipped machine \"${machinename}\"": }
  }
  each($users) |$value| {
    $privclassdef = "${name}::machines::${machinename}::${value}"
    if defined( $privclassdef ) {
      class { $privclassdef:
        user   => $value,
        params => $params
      }
    }
    else {
      notify { "Skipped user \"${value}\"": }
    }
  }
}

