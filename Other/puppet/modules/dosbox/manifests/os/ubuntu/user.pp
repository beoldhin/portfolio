define dosbox::os::ubuntu::user ( $user = undef, $params = undef ) {
  $machinename = $params['machinename']
  $dosboxver = $params['dosboxver']
  $userpath = "puppet:///modules/dosbox/machines/${machinename}/users/${user}"
  file { "dosbox-dosbox-dir-${user}":
    ensure  => directory,
    require => [
      User[$user],
      Package['dosbox'],
    ],
    path    => "/home/${user}/.dosbox",
    mode    => '0700',
    owner   => $user,
    group   => $user,
  }
  file { "dosbox.conf-${user}":
    ensure  => present,
    require => [
      User[$user],
      Package['dosbox'],
      File["dosbox-dosbox-dir-${user}"],
    ],
    path    => "/home/${user}/.dosbox/dosbox-${dosboxver}.conf",
    replace => yes,
    mode    => '0644',
    owner   => $user,
    group   => $user,
    source  => "${userpath}/dosbox-${dosboxver}.conf",
  }
}

