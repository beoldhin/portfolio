#!/usr/bin/env python

from plumbum import local
from plumbum.cmd import  cat

from namegetter import *

configpath = 'configs'
basepath = '/etc/puppetlabs/code/modules/base/files'

class CreateBaseProfile:
    namegetter = None
    configpath = ''
    destfilepath = ''
    def __init__(self, configpath, destfilepath):
        print('Initializing CreateBaseProfile...')
        self.namegetter = NameGetter()
        self.configpath = configpath
        self.destfilepath = destfilepath
    def create_sourcebasefilename(self, machinename, user):
        if user == "root":
            return '{}/profile-{}-{}-base'.format(self.configpath, machinename, user)
        return '{}/profile-{}-base'.format(self.configpath, machinename)
    def create_sourcedeffilename(self, definition):
        return '{}/profile-{}'.format(self.configpath, definition)
    def create_destfilename(self, machinename, user):
        return '{}/machines/{}/users/{}/profile'.format(self.destfilepath, machinename, user)
    def reset_file(self, machinename, user):
        sourcefilename = self.create_sourcebasefilename(machinename, user)
        destfilename = self.create_destfilename(machinename, user)
        print('Creating file {}...'.format(destfilename))
        p = local.path(sourcefilename)
        p.copy(destfilename, override=True)
    def append_one_definition(self, definition, machinename, user):
        sourcefilename = self.create_sourcedeffilename(definition)
        destfilename = self.create_destfilename(machinename, user)
        (cat[sourcefilename] >> destfilename)()
    def initialize(self):  # PUBLIC
        yitems = len(self.namegetter.profile_names)
        for j in range(yitems):
            machinename = self.namegetter.profile_names[j][0]
            machinename, user = self.namegetter.get_userandhost(machinename)
            self.reset_file(machinename, user)
    def append_definitions(self):  # PUBLIC
        yitems = len(self.namegetter.profile_names)
        for j in range(yitems):
            xitems = len(self.namegetter.profile_names[j])
            machinename = self.namegetter.profile_names[j][0]
            machinename, user = self.namegetter.get_userandhost(machinename)
            for i in range(1, xitems):
                definition = self.namegetter.profile_names[j][i]
                self.append_one_definition(definition, machinename, user)

if __name__ == "__main__":
    baseprofile = CreateBaseProfile(configpath, basepath)
    baseprofile.initialize()
    baseprofile.append_definitions()

