#!/usr/bin/env python

from plumbum import local
from plumbum.cmd import echo, touch

from namegetter import *

basepuppetpath = '/etc/puppetlabs/code/modules/base/manifests/machines'

class CreateBasePuppetConfigs:
    namegetter = None
    foundpairs = []
    destfilepath = ''
    prevrequire = ''
    secondone = False
    firstdef = (
        "# Generated file, DO NOT EDIT\n"
        "define base::machines::{}::hosts( $params = undef ) {{\n"
        "  $machinename = $params['machinename']")
    hostnamedef = (
        "  file { 'hostname':\n"
        "    ensure  => present,\n"
        "    path    => '/etc/hostname',\n"
        "    replace => yes,\n"
        "    mode    => '0644',\n"
        "    owner   => 'root',\n"
        "    group   => 'root',\n"
        "    content => \"${machinename}\\n\",\n"
        "  }")
    ownhostdef = (
        "  host {{ \"hosts-${{machinename}}\":\n"
        "    ensure       => present,\n"
        "    name         => \"${{machinename}}.{}\",\n"
        "    host_aliases => $machinename,\n"
        "    ip           => '{}',\n"
        "  }}")
    otherhostdef = (
        "  host {{ 'hosts-{}':\n"
        "    ensure       => present,\n"
        "    require      => Host[{}],\n"
        "    name         => '{}.{}',\n"
        "    host_aliases => '{}',\n"
        "    ip           => $::ip_{},\n"
        "  }}")
    def __init__(self, destfilepath):
        print('Initializing CreateBasePuppetConfigs...')
        self.namegetter = NameGetter()
        self.destfilepath = destfilepath
    def create_destfilename(self, machinename):
        return '{}/{}/hosts.pp'.format(self.destfilepath, machinename)
    def echo_content(self, content):
        (echo[content] >> self.echofilename)()
    def reset_file(self, destfilename):
        print('Creating file {}...'.format(destfilename))
        p = local.path(destfilename)
        if p.exists():
            p.delete()
        touch(destfilename)
        p.chown('root', 'root')
        p.chmod(0o644)
    def append_first_content(self, destfilename, machinename):
        params = [machinename]
        self.echofilename = destfilename
        self.echo_content(self.firstdef.format(*params))
        self.echo_content(self.hostnamedef)
        params = [self.namegetter.domain_name, self.namegetter.localhost_name]
        self.echo_content(self.ownhostdef.format(*params))
    def append_one_definition(self, destfilename, sourcemachinename, destmachinename):
        params = [destmachinename,
                  self.prevrequire,
                  destmachinename, self.namegetter.domain_name,
                  destmachinename,
                  destmachinename]
        self.echofilename = destfilename
        self.echo_content(self.otherhostdef.format(*params))
        self.prevrequire = '\'hosts-{}\''.format(destmachinename)
    def append_last_content(self, destfilename):
        self.echofilename = destfilename
        self.echo_content('}')
    def find_pairs(self, fromname, toname):
        listitems = [fromname, toname]
        nitems = len(self.foundpairs)
        for i in range(nitems):
            if self.foundpairs[i] == listitems:
                return True
        self.foundpairs.append(listitems)
        return False
    def initialize(self):  # PUBLIC
        yitems = len(self.namegetter.fromto_names)
        for j in range(yitems):
            fromname = self.namegetter.fromto_names[j][0]
            fromname, fromuser = self.namegetter.get_userandhost(fromname)
            if not self.namegetter.is_external_name(fromname) and fromuser != 'root':
                destfilename = self.create_destfilename(fromname)
                self.reset_file(destfilename)
                self.append_first_content(destfilename, fromname)
    def append_definitions(self):  # PUBLIC
        yitems = len(self.namegetter.fromto_names)
        for j in range(yitems):
            self.prevrequire = '\"hosts-${machinename}\"'
            fromname = self.namegetter.fromto_names[j][0]
            fromname, fromuser = self.namegetter.get_userandhost(fromname)
            if not self.namegetter.is_external_name(fromname) and fromuser != 'root':
                xitems = len(self.namegetter.fromto_names[j])
                destfilename = self.create_destfilename(fromname)
                for i in range(1, xitems):
                    toname = self.namegetter.fromto_names[j][i]
                    toname, touser = self.namegetter.get_userandhost(toname)
                    toname, haskey = self.namegetter.get_toname(toname)
                    if not self.namegetter.is_external_name(toname) and fromname != toname:
                        if self.find_pairs(fromname, toname) == False:
                            self.append_one_definition(destfilename, fromname, toname)
                self.append_last_content(destfilename)

if __name__ == "__main__":
    basepuppetconfigs = CreateBasePuppetConfigs(basepuppetpath)
    basepuppetconfigs.initialize()
    basepuppetconfigs.append_definitions()

