#!/usr/bin/env python

from plumbum import local

from namegetter import *

import sys

def print_usage_string():
    prgname = sys.argv[0]
    print('Usage: {} <user@sourcehost> <destinationhost>'.format(prgname))
    sys.exit(1)

class SshKeyCreator:
    nbits = '4096'
    namegetter = None
    destfilepath = 'temp/'
    destfilepath = '/etc/puppetlabs/code/modules/opensshclient/files/'
    def __init__(self):
        self.namegetter = NameGetter()
    def create_destfilename(self, sourcemachinename, destmachinename):
        return '{}id_rsa_{}_to_{}'.format(self.destfilepath, sourcemachinename, destmachinename)
    def create_command(self, comment, destfilename):
        return 'ssh-keygen -t rsa -b {} -C \"{}\" -f {}'.format(self.nbits, comment, destfilename)
    def create_comment(self, fromuser, frommachinename, touser, tomachinename):
        # Valid cases:
        # - a@b -> d   (destination name & no nestination name)
        # - a@b -> c@d (destination name overridden)
        # Invalid cases:
        # - b -> d
        # - b -> c@d
        fromcomment = '{}@{}'.format(fromuser, frommachinename)
        if touser:
            tocomment = '{}@{}'.format(touser, tomachinename)
        else:
            todefinition = '{}@{}'.format(fromuser, tomachinename)
            touser = self.namegetter.get_destuser(todefinition)
            if fromuser == touser:
                tocomment = '{}'.format(tomachinename)
            else:
                tocomment = '{}@{}'.format(touser, tomachinename)
        return '{}->{}'.format(fromcomment, tocomment)
    def create(self, fromdefinition, todefinition):
        frommachinename, fromuser = self.namegetter.get_userandhost(fromdefinition)
        if fromuser:
            frommachinename = self.namegetter.get_latest_machinename(frommachinename)
            tomachinename, touser = self.namegetter.get_userandhost(todefinition)
            tomachinename = self.namegetter.get_latest_machinename(tomachinename)
            sourcemachinename = self.namegetter.get_machinename(frommachinename)
            destmachinename = self.namegetter.get_machinename(tomachinename)
            destfilename = self.create_destfilename(sourcemachinename, destmachinename)
            print('Run command [term 2]: \'ssh {}\''.format(destmachinename))
            print('Run command [term 1]: \'sudo rm {}*\''.format(destfilename))
            comment = self.create_comment(fromuser, frommachinename, touser, tomachinename)
            command = self.create_command(comment, destfilename)
            print('Run command [term 1]: \'sudo {}\''.format(command))
            print('Run command [term 1]: \'sudo chown root:puppet {}\''.format(destfilename))
            print('Run command [term 1]: \'sudo chmod 0640 {}\''.format(destfilename))
            print('Run command [term 1]: \'sudo ./configcreator.py\'')
            print('Run command [term 2]: \'mypuppettest\'')
            print('Run command [term 2]: \'mypuppetsync\'')
        else:
            raise ValueError('No source user!')

creator = SshKeyCreator()

arguments = sys.argv[1:]
if len(arguments) != 2:
    print_usage_string()

fromdefinition = arguments[0]
todefinition = arguments[1]
creator.create(fromdefinition, todefinition)

# creator.create('quetzal@elrond', 'elrond')
# creator.create('quetzal@elrond', 'gandalf')
# creator.create('quetzal@elrond', 'sam')
# creator.create('quetzal@elrond', 'shelob')
# creator.create('quetzal@elrond', 'olterman@feonar')
# creator.create('quetzal@elrond', 'beoldhin@github')
#
# creator.create('quetzal@gandalf', 'gandalf')
# creator.create('quetzal@gandalf', 'elrond')
# creator.create('quetzal@gandalf', 'beoldhin@github')
#
# creator.create('quetzal@sam', 'sam')
# creator.create('quetzal@sam', 'elrond')
# creator.create('quetzal@sam', 'gandalf')
# creator.create('quetzal@sam', 'shelob')
# creator.create('quetzal@sam', 'olterman@feonar')
# creator.create('quetzal@sam', 'beoldhin@github')
#
# creator.create('quetzal@shelob', 'shelob')
# creator.create('quetzal@shelob', 'beoldhin@github')
#
