#!/usr/bin/env python

from plumbum import local
from plumbum.cmd import echo, touch

import os
import yaml

sshpath = '/etc/puppetlabs/code/modules/opensshclient/files/machines'

class CreateSshAuthorized:
    echofilename = ''
    destfilepath = ''
    created_filenames = []
    sshkeydir = '/etc/puppet/hieradata/sshkeys/publickeys'
    def __init__(self, destfilepath):
        print('Initializing CreateSshAuthorized...')
        self.destfilepath = destfilepath
    def create_destfilename(self, toname, touser):
        return '{}/{}/users/{}/authorized_keys'.format(self.destfilepath, toname, touser)
    def reset_file(self, destfilename):
        print('Creating file {}...'.format(destfilename))
        p = local.path(destfilename)
        if p.exists():
            p.delete()
        touch(destfilename)
        p.chown('root', 'root')
        p.chmod(0o644)
    def echo_content(self, touser, toname, content):
        destfilename = self.create_destfilename(toname, touser)
        if not destfilename in self.created_filenames:
            self.created_filenames.append(destfilename)
            self.reset_file(destfilename)
        self.echofilename = destfilename
        (echo[content] >> self.echofilename)()
    def collect_yaml_filenames(self):
        yamlfilenames = []
        for root,dirs,files in os.walk(self.sshkeydir):
            for file in files:
                if file.endswith('.yaml'):
                    yamlfilenames.append(os.path.join(root, file))
        return yamlfilenames
    def process_publickeys(self):  # PUBLIC
        yamlfilenames = self.collect_yaml_filenames()
        for yamlfilename in yamlfilenames:
            with open(yamlfilename, 'r') as f:
                documents = yaml.load_all(f)
                for document in documents:
                    for outerleftdef,outerrightdef in document.items():
                        outernames = outerleftdef.split('-')
                        nametype = outernames[0]
                        direction = outernames[1]
                        if nametype != 'publickeys':
                            print("ERROR: Unknown to-nametype ({})".format(nametype))
                            sys.exit(1)
                        if direction != 'to':
                            print("ERROR: Unknown to-direction ({})".format(direction))
                            sys.exit(1)
                        toname = outernames[2]
                        touser = outernames[3]
                        for innerleftdef,innerrightdef in outerrightdef.items():
                            innernames = innerleftdef.split('-')
                            direction = innernames[0]
                            if direction != 'from':
                                print("ERROR: Unknown from-direction ({})".format(direction))
                                sys.exit(1)
                            fromname = innernames[1]
                            if len(innernames) == 3:
                                fromuser = innernames[2]
                            else:
                                fromuser = outernames[3]
                            content = innerrightdef
                            self.echo_content(touser, toname, content)

if __name__ == "__main__":
    createsshauthorized = CreateSshAuthorized(sshpath)
    createsshauthorized.process_publickeys()

