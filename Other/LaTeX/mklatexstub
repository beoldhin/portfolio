#!/usr/bin/env bash

# WARNING: Line below will be changed by code!

declare -a project_file=(
    "\documentclass{templates/template}"
    "\input{extras/packages}"
    "REPLACE_BY_CODE_A"
    "\osisetcited{}"
    ""
    "\title{This is my title}"
    ""
    "\author{This is my author}"
    ""
    "\begin{document}"
    "\input{extras/settings}"
    "\osisettoc{}"
    ""
    "% \osisetstyle{companion}{bringhurst}{verville}"
    "% \osisetstyle{companion}{bringhurst}{dash}"
    "% \osisetstyle{companion}{bringhurst}{southall}"
    "% \osisetstyle{companion}{bringhurst}{crosshead}"
    "\osisetstyle{companion}{bringhurst}{tandh}"
    "% \osisetstyle{companion}{default}{southall}"
    ""
    "\input{content/index}"
    ""
    "\osiprintbib{}"
    "\end{document}"
)

# WARNING: Line below will be changed by code!

declare -a tool_clean_file=(
    "#!/usr/bin/env bash"
    "# Add unknown extensions to ~/.latexmkrc (\$clean_ext)"
    "REPLACE_BY_CODE_A"
    ""
    "# NOTE: Do NOT remove the output pdf file as it will confuse evince"
    "find ../build -type f -not -name "\${project}.pdf" -delete"
    "find ../build -type d -not -name "\${project}.pdf" -delete 2> /dev/null"
)

# WARNING: Line below will be changed by code!

declare -a tool_compile_file=(
    "#!/usr/bin/env bash"
    "REPLACE_BY_CODE_A"
    ""
    "cd .."
    ""
    "diradd() {"
    "    mkdir -p \$1"
    "}"
    ""
    "# cd build"
    "# diradd parts/health"
    "# cd .."
    ""
    "cd src"
    "latexmk -outdir=../build \${project}.tex"
    "cd .."
)

# WARNING: Line below will be changed by code!

declare -a tool_packer_file=(
    "#!/usr/bin/env bash"
    "REPLACE_BY_CODE_A"
    "echo \"Converting pdf to ps...\""
    "pdf2ps build/\${project}.pdf"
    "echo \"Converting ps to pdf...\""
    "ps2pdf \${project}.ps"
    "echo \"Removing converted ps...\""
    "rm \${project}.ps"
    "echo \"Showing size difference...\""
    "ls -l build/\${project}.pdf \${project}.pdf"
)

# WARNING: Line below will be changed by code!

declare -a tool_tomd_file=(
    "#!/usr/bin/env bash"
    "REPLACE_BY_CODE_A"
    "citstyle=numeric-comp"
    ""
    "cd ../src"
    "pandoc -s -S \${project}.tex -o \${project}.md -f latex -t markdown --bibliography=\${project}.bib --biblatex --csl=\${citstyle}.csl --standalone"
)

declare -a packages_file=(
    "\setdefaultlanguage{english}"
    "\setotherlanguage{greek}"
    "\setotherlanguage{latin}"
    ""
    "\newfontfamily\greekfont[Script=Greek]{Linux Libertine O}"
    "\newfontfamily\latinfont[Script=Latin]{Linux Libertine O}"
    ""
    "% Chemical formulas"
    "\usepackage[version=4]{mhchem}"
    ""
    "% For \"definecolor\" command:"
    "\usepackage[svgnames]{xcolor}"
    ""
    "% \usepackage[backgroundcolor=Linen,textsize=scriptsize]{todonotes}"
    "\usepackage[backgroundcolor=Linen]{todonotes}"
    ""
    "% Text highlighting"
    "\usepackage{soul}"
    "\sethlcolor{LemonChiffon}"
)

declare -a settings_file=(
    "% Set very small footnote size:"
    "\renewcommand{\footnotesize}{\scriptsize\selectfont}"
    ""
    "% Set space between paragraphs"
    "% \setlength{\parskip}{\baselineskip}%"
    "% \setlength{\parindent}{0pt}%"
    ""
    "% All blockquotes are indented"
    "% \SetBlockThreshold{0}"
)

declare -a template_file=(
    "% Template version: 2.0 (for \"memoir\" package)"
    "\NeedsTeXFormat{LaTeX2e}"
    ""
    "% Declare commenting options:"
    "\newcommand{\OsiTodo}{}"
    "\newcommand{\OsiComment}{}"
    "\newcommand{\OsiHighlight}{}"
    ""
    "% Declare page formatting:"
    "\newcommand{\OsiSides}{oneside}"
    "\newcommand{\OsiColumns}{twocolumn}"
    "\newcommand{\OsiFinality}{final}"
    "\newcommand{\OsiDecimal}{.}"
    ""
    "% Use \"multx\" for preferred multiplication symbol:"
    "\let\multx\cdot"
    ""
    "% Load needed class"
    "\LoadClass[article,a4paper,9pt,\OsiSides,\OsiColumns,openright,\OsiFinality]{memoir}"
    ""
    "% Set better margins"
    "% \medievalpage[9]"
    ""
    "% Try to eliminate \"overfull box\" warnings (memoir specific):"
    "\midsloppy%"
    ""
    "% Try to eliminate widows (memoir specific):"
    "% NOTE: There seems to be a bug with sloppybottom and footnotes"
    "% (marks and footnote marks don't match)"
    "\sloppybottom%"
    ""
    "% Add support for translated texts:"
    "\RequirePackage{polyglossia}"
    ""
    "% Add support for doing correct quoting of sources:"
    "\RequirePackage[autostyle=true]{csquotes}"
    ""
    "% The newpx/newtx font package (2017) and textcomp"
    "\RequirePackage{newtxtext}"
    "\RequirePackage{newtxmath}"
    "\RequirePackage{textcomp}"
    ""
    "% Add better type of bibliography, biblatex+biber instead of bibtex:"
    "\RequirePackage[style=numeric-comp,defernumbers=true,backref=true,backend=biber,urldate=edtf,date=edtf,seconds=true]{biblatex}"
    ""
    "% This works better than \sloppy, \sloppypar and \setlength{\emergencystretch}{30pt}"
    "\RequirePackage[activate={true,nocompatibility},\OsiFinality]{microtype}"
    ""
    "% Load common and important classes"
    "\RequirePackage{tabu}"
    ""
    "% The following sets decimal symbol for \num, and \SI:"
    "\RequirePackage[detect-weight=true,detect-family=true,output-decimal-marker={\OsiDecimal}]{siunitx}"
    ""
    "% Add lorem ipsum package for testing:"
    "\RequirePackage{lipsum}"
    ""
    "% Translate some strings of the bibliography"
    "\DefineBibliographyStrings{english}{%"
    "    bibliography = {References},"
    "}"
    "%\DefineBibliographyStrings{finnish}{%"
    "%    bibliography = {Lähteet},"
    "%}"
    ""
    "% Command: Add bib resource"
    "% Argument 1: Bib file name"
    "\newcommand{\osiaddbibres}[1]{%"
    "    % Set low penalties for breaks at numbers, uppercase letters and lowercase letters"
    "    % This makes URL to break at any point in the references"
    "    \setcounter{biburlnumpenalty}{100}"
    "    \setcounter{biburlucpenalty}{100}"
    "    \setcounter{biburllcpenalty}{100}"
    "    \addbibresource{#1}"
    "}"
    ""
    "% Command: Set cited marks"
    "\newcommand{\osisetcited}{%"
    "    \DeclareBibliographyCategory{cited}"
    "    \AtEveryCitekey{\addtocategory{cited}{\thefield{entrykey}}}"
    "    \nocite{*}"
    "}"
    ""
    "% Command: Set Title"
    "\newcommand{\osisettitle}{%"
    "    \frontmatter"
    "    \begin{titlingpage}"
    "    \maketitle"
    "    \end{titlingpage}"
    "}"
    ""
    "% Command: Set Table of Contents"
    "\newcommand{\osisettoc}{%"
    "    \frontmatter"
    "    \begin{titlingpage}"
    "    \maketitle"
    "    \end{titlingpage}"
    "    \microtypesetup{protrusion=false} % Add also for LoT and LoF"
    "    \tableofcontents"
    "    \microtypesetup{protrusion=true} % Add also for LoT and LoF"
    "}"
    ""
    "% Command: Set styles (mandatory)"
    "\newcommand{\osisetstyle}[3]{%"
    "    \mainmatter{}"
    "    % Available page styles: empty, plain, headings, myheadings, simple, ruled,"
    "    % Ruled, companion"
    "    \pagestyle{#1}"
    "    % Available head styles: bringhurst, crosshead, default, dowding, komalike,"
    "    % memman, ntglike, tandh, wilsondob"
    "    \headstyles{#2}"
    "    % Available chapter styles: default, section, article, reparticle, hangnum,"
    "    % companion, demo, bianchi, bringhurst, brotherton, chappell, culver, dash,"
    "    % demo2, demo3, ell, ger, lyhne, madsen, pedersen, southall, thatcher,"
    "    % veelo, verville, crosshead, dowding, komalike, ntglike, tandh, wilsondob"
    "    \chapterstyle{#3}"
    "}"
    ""
    "% Command: Set spaced and unindented paragraphs"
    "\newcommand{\osisetspacedpars}{%"
    "    \setlength{\parindent}{0pt}"
    "    \nonzeroparskip{}"
    "}"
    ""
    "% Command: Print Bibliography"
    "\newcommand{\osiprintbib}{%"
    "    \clearpage{}"
    "    \pagestyle{plain}"
    "    \backmatter{}"
    "    %\printbibliography[heading=bibintoc,resetnumbers=true]"
    "    \printbibliography[heading=bibintoc,category=cited,resetnumbers=true]"
    "    %\printbibliography[heading=bibintoc,title={Lisälukemisto},notcategory=cited]"
    "    % \printbibliography[heading=bibintoc,title={Further Reading},notcategory=cited]"
    "}"
    ""
    "% Command: Set title for solution"
    "%\renewcommand{\solutiontitle}{\noindent\textbf{Ratkaisu:}\par\noindent}"
    ""
    "% Command: Set epigraph length"
    "\newcommand{\osisetepilen}[1]{\setlength{\epigraphwidth}{#1\textwidth}}"
    ""
    "% Command: Add note"
    "\newcommand{\ositodo}[1]{\ifdefined\OsiTodo\todo[inline]{#1}\fi}"
    ""
    "% Command: Add footnote mark"
    "\newcommand{\osifnm}{\footnotemark{}}"
    ""
    "% Command: Add to footnote counter"
    "\newcommand{\osifnc}[1]{\addtocounter{footnote}{-#1}}"
    ""
    "% Command: Add footnote content"
    "\newcommand{\osifnt}[1]{\stepcounter{footnote}\footnotetext{#1}}"
    ""
    "% Command: Add footnote content as quote"
    "\newcommand{\osifntq}[1]{\stepcounter{footnote}\footnotetext{\enquote{#1}}}"
    ""
    "% Command: Reset footnote counter"
    "\newcommand{\osifnr}[1]{\setcounter{footnote}{0}}"
    ""
    "% Command: Add plain break"
    "\newcommand{\osiplainbreak}{\plainbreak{1}}"
    ""
    "% Command: Add fancy break"
    "\newcommand{\osifancybreak}[1]{\fancybreak{\rule{\columnwidth}{0.5pt}\\#1\\\rule[5pt]{\columnwidth}{0.5pt}}}"
    ""
    "% Command: Quote italic text"
    "\newcommand{\osiqit}[1]{\enquote{\textit{#1}}}"
    ""
    "% Command: Highlight text"
    "\newcommand{\osihl}[1]{\ifdefined\OsiHighlight\hl{#1}\else#1\fi}"
    ""
    "% Command: Add comment box"
    "\newcommand{\osicomment}[1]{\ifdefined\OsiComment\begin{tcolorbox}#1\end{tcolorbox}\fi}"
)

# -------------------------------------------------------------------------------

show_usage() {
    echo "Usage: $0 <directory>"
    exit 1
}

create_dir() {
    dirname=$1
    if [ -d $dirname ]; then
        echo "ERROR: Directory ${dirname} already exists."
        exit 1
    fi
    mkdir $dirname
}

replace_line() {
    searchdef="REPLACE_BY_CODE_${1}"
    replacedef=$2
    local -n reparr=$3
    arrcounter=0
    entityfound=false
    for i in "${reparr[@]}"; do
        if [ "$i" == "$searchdef" ]; then
            entityfound=true
            break
        fi
        ((arrcounter++))
    done
    if [ "$entityfound" = false ]; then
        echo "ERROR: Replaceable not found!"
        exit 1
    fi
    declarecmd="$3[${arrcounter}]=$replacedef"
    declare -g $declarecmd
}

array_to_file() {
    outfile=$1
    local -n outarr=$2
    for i in "${outarr[@]}"; do
        echo "$i" >> $outfile
    done
}

if [ "$#" -ne 1 ]; then
    echo "ERROR: Unknown number of parameters."
    show_usage
fi
project=$1

create_dir $project
cd $project
project=${project,,}

replace_line "A" "\osiaddbibres{${project}.bib}" project_file
replace_line "A" "project=$project" tool_clean_file
replace_line "A" "project=$project" tool_compile_file
replace_line "A" "project=$project" tool_packer_file
replace_line "A" "project=$project" tool_tomd_file

create_dir "build"
create_dir "docs"
create_dir "src"
create_dir "tools"
cd "src"
create_dir "content"
create_dir "extras"
create_dir "templates"

touch "${project}.bib"
array_to_file "${project}.tex" project_file

cd "extras"
array_to_file "packages.tex" packages_file
array_to_file "settings.tex" settings_file

cd ..
cd "content"
touch "index.tex"

cd ..
cd "templates"
array_to_file "template.cls" template_file

cd ..
cd ..
cd "tools"
array_to_file "clean" tool_clean_file
array_to_file "compile" tool_compile_file
array_to_file "packer" tool_packer_file
array_to_file "tomd" tool_tomd_file
chmod u+x clean
chmod u+x compile
chmod u+x packer
chmod u+x tomd

