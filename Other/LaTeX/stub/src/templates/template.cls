% Template version: 2.0 (for "memoir" package)
\NeedsTeXFormat{LaTeX2e}

% Declare commenting options:
\newcommand{\OsiTodo}{}
\newcommand{\OsiComment}{}
\newcommand{\OsiHighlight}{}

% Declare page formatting:
\newcommand{\OsiSides}{oneside}
\newcommand{\OsiColumns}{twocolumn}
\newcommand{\OsiFinality}{final}
\newcommand{\OsiDecimal}{.}

% Use "multx" for preferred multiplication symbol:
\let\multx\cdot

% Load needed class
\LoadClass[article,a4paper,9pt,\OsiSides,\OsiColumns,openright,\OsiFinality]{memoir}

% Set better margins
% \medievalpage[9]

% Try to eliminate "overfull box" warnings (memoir specific):
\midsloppy%

% Try to eliminate widows (memoir specific):
% NOTE: There seems to be a bug with sloppybottom and footnotes
% (marks and footnote marks don't match)
\sloppybottom%

% Add support for translated texts:
\RequirePackage{polyglossia}

% Add support for doing correct quoting of sources:
\RequirePackage[autostyle=true]{csquotes}

% The newpx/newtx font package (2017) and textcomp
\RequirePackage{newtxtext}
\RequirePackage{newtxmath}
\RequirePackage{textcomp}

% Add better type of bibliography, biblatex+biber instead of bibtex:
\RequirePackage[style=numeric-comp,defernumbers=true,backref=true,backend=biber,urldate=edtf,date=edtf,seconds=true]{biblatex}

% This works better than \sloppy, \sloppypar and \setlength{\emergencystretch}{30pt}
\RequirePackage[activate={true,nocompatibility},\OsiFinality]{microtype}

% Load common and important classes
\RequirePackage{tabu}

% The following sets decimal symbol for \num, and \SI:
\RequirePackage[detect-weight=true,detect-family=true,output-decimal-marker={\OsiDecimal}]{siunitx}

% Add lorem ipsum package for testing:
\RequirePackage{lipsum}

% Translate some strings of the bibliography
\DefineBibliographyStrings{english}{%
    bibliography = {References},
}
%\DefineBibliographyStrings{finnish}{%
%    bibliography = {Lähteet},
%}

% Command: Add bib resource
% Argument 1: Bib file name
\newcommand{\osiaddbibres}[1]{%
    % Set low penalties for breaks at numbers, uppercase letters and lowercase letters
    % This makes URL to break at any point in the references
    \setcounter{biburlnumpenalty}{100}
    \setcounter{biburlucpenalty}{100}
    \setcounter{biburllcpenalty}{100}
    \addbibresource{#1}
}

% Command: Set cited marks
\newcommand{\osisetcited}{%
    \DeclareBibliographyCategory{cited}
    \AtEveryCitekey{\addtocategory{cited}{\thefield{entrykey}}}
    \nocite{*}
}

% Command: Set Title
\newcommand{\osisettitle}{%
    \frontmatter
    \begin{titlingpage}
    \maketitle
    \end{titlingpage}
}

% Command: Set Table of Contents
\newcommand{\osisettoc}{%
    \frontmatter
    \begin{titlingpage}
    \maketitle
    \end{titlingpage}
    \microtypesetup{protrusion=false} % Add also for LoT and LoF
    \tableofcontents
    \microtypesetup{protrusion=true} % Add also for LoT and LoF
}

% Command: Set styles (mandatory)
\newcommand{\osisetstyle}[3]{%
    \mainmatter{}
    % Available page styles: empty, plain, headings, myheadings, simple, ruled,
    % Ruled, companion
    \pagestyle{#1}
    % Available head styles: bringhurst, crosshead, default, dowding, komalike,
    % memman, ntglike, tandh, wilsondob
    \headstyles{#2}
    % Available chapter styles: default, section, article, reparticle, hangnum,
    % companion, demo, bianchi, bringhurst, brotherton, chappell, culver, dash,
    % demo2, demo3, ell, ger, lyhne, madsen, pedersen, southall, thatcher,
    % veelo, verville, crosshead, dowding, komalike, ntglike, tandh, wilsondob
    \chapterstyle{#3}
}

% Command: Set spaced and unindented paragraphs
\newcommand{\osisetspacedpars}{%
    \setlength{\parindent}{0pt}
    \nonzeroparskip{}
}

% Command: Print Bibliography
\newcommand{\osiprintbib}{%
    \clearpage{}
    \pagestyle{plain}
    \backmatter{}
    %\printbibliography[heading=bibintoc,resetnumbers=true]
    \printbibliography[heading=bibintoc,category=cited,resetnumbers=true]
    %\printbibliography[heading=bibintoc,title={Lisälukemisto},notcategory=cited]
    % \printbibliography[heading=bibintoc,title={Further Reading},notcategory=cited]
}

% Command: Set title for solution
%\renewcommand{\solutiontitle}{\noindent\textbf{Ratkaisu:}\par\noindent}

% Command: Set epigraph length
\newcommand{\osisetepilen}[1]{\setlength{\epigraphwidth}{#1\textwidth}}

% Command: Add note
\newcommand{\ositodo}[1]{\ifdefined\OsiTodo\todo[inline]{#1}\fi}

% Command: Add footnote mark
\newcommand{\osifnm}{\footnotemark{}}

% Command: Add to footnote counter
\newcommand{\osifnc}[1]{\addtocounter{footnote}{-#1}}

% Command: Add footnote content
\newcommand{\osifnt}[1]{\stepcounter{footnote}\footnotetext{#1}}

% Command: Add footnote content as quote
\newcommand{\osifntq}[1]{\stepcounter{footnote}\footnotetext{\enquote{#1}}}

% Command: Reset footnote counter
\newcommand{\osifnr}[1]{\setcounter{footnote}{0}}

% Command: Add plain break
\newcommand{\osiplainbreak}{\plainbreak{1}}

% Command: Add fancy break
\newcommand{\osifancybreak}[1]{\fancybreak{\rule{\columnwidth}{0.5pt}\#1\\rule[5pt]{\columnwidth}{0.5pt}}}

% Command: Quote italic text
\newcommand{\osiqit}[1]{\enquote{\textit{#1}}}

% Command: Highlight text
\newcommand{\osihl}[1]{\ifdefined\OsiHighlight\hl{#1}\else#1\fi}

% Command: Add comment box
\newcommand{\osicomment}[1]{\ifdefined\OsiComment\begin{tcolorbox}#1\end{tcolorbox}\fi}
