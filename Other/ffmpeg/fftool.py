#!/usr/bin/env python

import os
import sys
import subprocess
from pathlib import Path

# import traceback
# Use this to print the stack trace, if needed:
# for line in traceback.format_stack():
#     print(line.strip())

class FFTools:
    inputDir = 'input'
    outputDir = 'output'
    imagesDir = 'images'
    origimgsDir = 'origimgs'
    tempDir = None
    def __init__(self):
        self.requireProgram('ffmpeg')
        self.requireProgram('ffprobe')
        self.requireProgram('convert')
        self.requireProgram('mogrify')
        self.requireProgram('ffmpeg-normalize')
        self.requireDir(self.inputDir)
        self.requireDir(self.outputDir)
        self.requireDir(self.imagesDir)
        self.requireDir(self.origimgsDir)
        self.tempDir = '/home/{}/Binaries/Temp'.format(self.runCommand('whoami').strip('\n'))
    def unpackDict(self, p):
        for k, v in p.items():
            setattr(self, k, v)
    def runCommand(self, command):
        retText = subprocess.check_output(command, shell=True).decode('UTF-8')
        return retText
    def requireProgram(self, program):
        command = ""
        try:
            command = "command -v {}".format(program)
            retText = self.runCommand(command)
        except Exception:
            print("ERROR: Required program missing: {}".format(program))
            sys.exit(1)
    def getOverlayPos(self, definition, xPadding, yPadding):
        overlay = ''
        if definition == '0':
            overlay = '{}:{}'.format(xPadding, yPadding)
        elif definition == '1':
            overlay = 'main_w-overlay_w-{}:{}'.format(xPadding, yPadding)
        elif definition == '2':
            overlay = '(main_w/2)-(overlay_w/2):(main_h/2)-(overlay_h/2)'
        elif definition == '3':
            overlay = '{}:main_h-overlay_h-{}'.format(xPadding, yPadding)
        elif definition == '4':
            overlay = 'main_w-overlay_w-{}:main_h-overlay_h-{}'.format(xPadding, yPadding)
        else:
            print('ERROR: Unknown overlay definition ({}).'.format(definition))
            sys.exit(1)
        return overlay
    def requireFile(self, fileName):
        fileObj = Path(fileName)
        if not fileObj.is_file():
            print('ERROR: File does not exist ({}).'.format(fileName))
            sys.exit(1)
        return fileObj
    def requireDir(self, dirName):
        fileObj = Path(dirName)
        if not fileObj.is_dir():
            print('ERROR: Directory does not exist ({}).'.format(dirName))
            sys.exit(1)
        return fileObj
    def getBaseName(self, fileName):
        fileObj = Path(fileName)
        return fileObj.name;
    def getStemName(self, fileName):
        fileObj = Path(fileName)
        return fileObj.stem;
    def slideshowResize(self, p):
        self.unpackDict(p)
        # Paths used:
        # Image output: ./images
        # Image input: ./origimgs
        totalSize = '{}x{}'.format(self.xSize, self.ySize)
        imageSpec = '{}/*.png'.format(self.origimgsDir)
        cmdResizeImageFiles = 'mogrify -resize {} -background black -gravity center -extent {} -path {} {}'.format(totalSize, totalSize, self.imagesDir, imageSpec)
        self.runCommand(cmdResizeImageFiles)
    def slideshow(self, p):
        self.unpackDict(p)
        # https://askubuntu.com/questions/486297/how-to-select-video-quality-from-youtube-dl
        # Paths used:
        # Video output: ./output
        # Image slides: ./images
        imagesExt = 'png'
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.*'))
        for inputFile in inputFiles:
            numOfImages = int(self.runCommand('find {}/ -mindepth 1 -type f -name image*.{} -printf x | wc -c'.format(self.imagesDir, imagesExt)).rstrip('\n'))
            if numOfImages == 0:
                print('ERROR: No image files found.')
                sys.exit(1)
            frameRate = self.runCommand('ffprobe -v 0 -of csv=p=0 -select_streams 0 -show_entries stream=r_frame_rate "{}" | bc -l'.format(inputFile)).rstrip('\n')
            seconds = self.runCommand("ffprobe -i \"{}\" -show_format -v quiet | sed -n 's/duration=//p'".format(inputFile)).rstrip('\n')
            imageRate = self.runCommand('echo {}/{} | bc -l'.format(numOfImages, seconds)).rstrip('\n')
            print('Found {} image files...'.format(numOfImages))
            print('Creating {} seconds long slideshow...'.format(seconds))
            print('Slideshow image rate will be {} FPS...'.format(imageRate))
            print('Framerate of video will be {} FPS...'.format(frameRate))
            print('Overlay image will be {} pixels wide...'.format(self.overlayWidth))
            print('Overlay image will have {}x{} padding...'.format(self.xPadding, self.yPadding))
            print('Overlay image will have {} transparency...'.format(self.transparency))
            outputFile = '{}/{}'.format(self.outputDir, '{}.{}'.format(self.getStemName(inputFile), 'mp4'))
            imagesFile = '{}/{}-images.{}'.format(self.outputDir, self.getStemName(inputFile), 'mp4')
            overlayPos = self.getOverlayPos(self.overlayLocation, self.xPadding, self.yPadding)
            # ----------
            filterString = "[1]scale={}:-1,lut=a=val*{}[top];[0][top]overlay={}:shortest=1[out]".format(self.overlayWidth, self.transparency, overlayPos)
            imagesdef = '{}/image-%03d.{}'.format(self.imagesDir, imagesExt)
            cmdCreateImageShowFile = 'ffmpeg -loop 1 -framerate {} -i "{}" -c:v libx264 -r {} -pix_fmt yuv420p -t {} -y "{}"'.format(imageRate, imagesdef, frameRate, seconds, imagesFile)
            cmdCreateFinalShowFile = 'ffmpeg -i "{}" -i "{}" -filter_complex "{}" -map "[out]" -map 1:a -c:a copy -c:v libx264 -y "{}"'.format(imagesFile, inputFile, filterString, outputFile)
            cmdRemoveImageShowFile = 'rm "{}"'.format(imagesFile)
            self.runCommand(cmdCreateImageShowFile)
            self.runCommand(cmdCreateFinalShowFile)
            self.runCommand(cmdRemoveImageShowFile)
    def overlay(self, p):
        self.unpackDict(p)
        # Paths used:
        # Video input:  ./input
        # Video output: ./output
        if self.audioSource != 'firstaudio' and self.audioSource != 'secondaudio':
            print("ERROR: Unknown audio source.")
            sys.exit(1)
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.*'))
        for inputFile in inputFiles:
            outputFile = '{}/{}'.format(self.outputDir, '{}.{}'.format(self.getStemName(inputFile), 'mp4'))
            audioSource = '0'
            if self.audioSource == 'secondaudio':
                audioSource = '1'
            overlayPos = self.getOverlayPos(self.overlayLocation, self.xPadding, self.yPadding)
            filterString = "[1]scale={}:-1,lut=a=val*{}[top];[0][top]overlay={}:shortest=1[out]".format(self.overlayWidth, self.transparency, overlayPos)
            cmdCreateFinalFile = 'ffmpeg -i "{}" -i "{}" -filter_complex "{}" -map "[out]" -map {}:a -c:a copy -c:v libx264 -y -shortest "{}"'.format(inputFile, self.overlayFile, filterString, audioSource, outputFile)
            self.runCommand(cmdCreateFinalFile)
    def watermark(self, p):
        self.unpackDict(p)
        # Paths used:
        # Video input:      ./input
        # Video output:     ./output
        # Watermark resize: ./output
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.*'))
        for inputFile in inputFiles:
            watermarkOutput = '{}/{}'.format(self.outputDir, self.getBaseName(self.watermarkFile))
            outputFile = '{}/{}'.format(self.outputDir, self.getBaseName(inputFile))
            overlayPos = self.getOverlayPos(self.watermarkLocation, self.xPadding, self.yPadding)
            filterString = '[1]lut=a=val*{}[a];[0][a]overlay={}'.format(self.transparency, overlayPos)
            cmdResizeWatermarkFile = 'convert -resize "{}x" "{}" "{}"'.format(self.watermarkWidth, self.watermarkFile, watermarkOutput)
            cmdCreateFinalFile = 'ffmpeg -i "{}" -i "{}" -loop 1 -filter_complex "{}" -codec:a copy "{}"'.format(inputFile, watermarkOutput, filterString, outputFile)
            cmdRemoveWatermarkFile = 'rm "{}"'.format(watermarkOutput)
            self.runCommand(cmdResizeWatermarkFile)
            self.runCommand(cmdCreateFinalFile)
            self.runCommand(cmdRemoveWatermarkFile)
    def stabilize(self, p):
        self.unpackDict(p)
        # Paths used:
        # Vide input: ./input
        # Video output: ./output
        if self.zooming != 'zoom' and self.zooming != 'nozoom':
            print("ERROR: Unknown zooming definition.")
            sys.exit(1)
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.*'))
        for inputFile in inputFiles:
            outputFile = '{}/{}'.format(self.outputDir, self.getBaseName(inputFile))
            stepsizeSetting = ':stepsize={}'.format(self.stepSize)
            smoothSetting = 'smoothing={}'.format(self.smoothing)
            zoomSetting = ''
            if self.zooming == 'nozoom':
                zoomSetting = ':crop=black:optzoom=0'
            cmdCreateTransformsFile = 'ffmpeg -i "{}" -vf vidstabdetect=shakiness=10{} -f null -'.format(inputFile, stepsizeSetting)
            cmdTransformsToVideoFile = 'ffmpeg -i "{}" -vf vidstabtransform={}{},unsharp=5:5:0.8:3:3:0.4 "{}"'.format(inputFile, smoothSetting, zoomSetting, outputFile)
            cmdRemoveTransformsFile = 'rm "transforms.trf"'
            self.runCommand(cmdCreateTransformsFile)
            self.runCommand(cmdTransformsToVideoFile)
            self.runCommand(cmdRemoveTransformsFile)
    def containerToMp3VolumeFixed(self, p):
        self.unpackDict(p)
        # Paths used:
        # Audio input: ./input
        # Temp output: ./normalized
        # Mp3 output: ./output
        tempExt = 'mkv'
        targetExt = 'mp3'
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.*'))
        for inputFile in inputFiles:
            cmdContainerToTemp = 'export TMP={} && ffmpeg-normalize \"{}\" --verbose --target-level {}'.format(self.tempDir, inputFile, self.audioLevel)
            self.runCommand(cmdContainerToTemp)
            tempoTimesTen = int((float(self.tempo))*10)
            tempFileTarget = 'normalized/{}.{}'.format(inputFile.stem, tempExt)
            targetFile = '{}/{}-lvl{}-tmp{}.{}'.format(self.outputDir, inputFile.stem, self.audioLevel, tempoTimesTen, targetExt)
            filterString = ''
            if self.tempo != '0':
                filterString = '-filter:a "atempo={}"'.format(self.tempo)
            cmdTempToFinal = 'ffmpeg -i "{}" {} -acodec {} "{}"'.format(tempFileTarget, filterString, targetExt, targetFile)
            self.runCommand(cmdTempToFinal)
            cmdDeleteTempFile = 'rm "{}"'.format(tempFileTarget)
            self.runCommand(cmdDeleteTempFile)
    def flip(self, p):
        self.unpackDict(p)
        # Paths used:
        # Video input: ./input
        # Video output: ./output
        if self.flipType != 'h' and self.flipType != 'v':
            print("ERROR: Unknown flip type.")
            sys.exit(1)
        flipDef = 'hflip'
        if self.flipType == 'v':
            flipDef = 'vflip'
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.*'))
        for inputFile in inputFiles:
            outputFile = '{}/{}'.format(self.outputDir, inputFile.name)
            cmdDoConvert = 'ffmpeg -i "{}" -vf {} -c:a copy "{}"'.format(inputFile, flipDef, outputFile)
            self.runCommand(cmdDoConvert)
    def rotate(self, p):
        self.unpackDict(p)
        # Paths used:
        # Video input: ./input
        # Video output: ./output
        if self.rotateType != '0' and self.rotateType != '1' and self.rotateType != '2' and self.rotateType != '3' and self.rotateType != '4':
            print("ERROR: Unknown rotate type.")
            sys.exit(1)
        rotateDef = ''
        if self.rotateType == '0':
            rotateDef = 'transpose=2'
        elif self.rotateType == '1':
            rotateDef = 'transpose=0'
        elif self.rotatetype == '2':
            rotateDef = 'transpose=1'
        elif self.rotateType == '3':
            rotateDef = 'transpose=3'
        else:
            rotateDef = 'transpose=1,transpose=1'
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.*'))
        for inputFile in inputFiles:
            outputFile = '{}/{}'.format(self.outputDir, inputFile.name)
            cmdDoConvert = 'ffmpeg -i "{}" -vf {} -c:a copy "{}"'.format(inputFile, rotateDef, outputFile)
            self.runCommand(cmdDoConvert)
    def m4aToAac(self, p):
        self.unpackDict(p)
        # Paths used:
        # m4a input: ./input
        # mp3 output: ./output
        # Input example: "youtube-dl -f 140 <videofile>"
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.m4a'))
        for inputFile in inputFiles:
            outputFile = '{}/{}.aac'.format(self.outputDir, inputFile.stem)
            cmdDoConvert = 'ffmpeg -i "{}" -acodec copy "{}"'.format(inputFile, outputFile)
            self.runCommand(cmdDoConvert)
    def flvToMp3(self, p):
        self.unpackDict(p)
        # Paths used:
        # m4a input: ./input
        # mp3 output: ./output
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.flv'))
        for inputFile in inputFiles:
            inputStem = '{}'.format(inputFile.stem)
            inputStem = inputStem.replace('"', '\\"')
            inputFile = 'input/{}.flv'.format(inputStem)
            outputFile = '{}/{}.mp3'.format(self.outputDir, inputStem)
            cmdDoConvert = 'ffmpeg -i "{}" -acodec copy "{}"'.format(inputFile, outputFile)
            self.runCommand(cmdDoConvert)
    def removeinput(self, p):
        self.unpackDict(p)
        inputs = Path(self.inputDir)
        inputFiles = list(inputs.glob('*.*'))
        for inputFile in inputFiles:
            print(inputFile)
            selection = input('Remove this? [y/n]: ')
            if "$selection" == "y":
                cmdRemoveIt = 'rm "{}"'.format(inputFile)
                self.runCommand(cmdRemoveIt)
    def removeoutput(self, p):
        self.unpackDict(p)
        outputs = Path(self.outputDir)
        outputFiles = list(outputs.glob('*.*'))
        for outputFile in outputFiles:
            print(outputFile)
            selection = input('Remove this? [y/n]: ')
            if "$selection" == "y":
                cmdRemoveIt = 'rm "{}"'.format(outputFile)
                self.runCommand(cmdRemoveIt)

def printUsageSlideshowResize(command):
    print("{} slideshowresize <xsize> <ysize>".format(command))
    print("Resizes images for <slideshow> command to match a given size.")
    print("    Xsize is the positive horizontal integer value in pixels: video or screen width.")
    print("    Ysize is the positive vertical integer value in pixels: vide or screen height.")

def printUsageSlideshow(command):
    print("{} slideshow <overlaywidth> <overlaylocation> <xpadding> <ypadding> <transparency>".format(command))
    print("Creates slideshows where the images are on the background and the video on top as overlay.")
    print("    Overlaywidth is a positive integer value in pixels.")
    print("    Overlaylocation has the following possible values:")
    print("        0 = top left")
    print("        1 = top right")
    print("        2 = center (<xpadding>, <ypadding> has no effect)")
    print("        3 = bottom left")
    print("        4 = bottom right")
    print("    Xpadding is a positive integer value in pixels (x padding for overlay).")
    print("    Ypadding is a positive integer value in pixels (y padding for overlay).")
    print("    Transparency is a positive decimal number between 0..1 (1 = full opaque).")
    print("    NOTE: Use the <slideshowresize> option once before this command as it makes all the images the same size!")

def printUsageOverlay(command):
    print("{} overlay <overlayfile> <firstaudio|secondaudio> <overlaywidth> <overlaylocation> <xpadding> <ypadding> <transparency>".format(command))
    print("Creates new videos of two different videos where the second is overlaid on top of the other in the background.")
    print("    Firstaudio means to use the audio of the first file (<mediafile>).")
    print("    Secondaudio means to use the audio of the second file (<overlayfile>).")
    print("    Overlaywidth is a positive integer value in pixels.")
    print("    Overlaylocation has the following possible values:")
    print("        0 = top left")
    print("        1 = top right")
    print("        2 = center (<xpadding>, <ypadding> has no effect)")
    print("        3 = bottom left")
    print("        4 = bottom right")
    print("    Xpadding is a positive integer value in pixels (x padding for overlay).")
    print("    Ypadding is a positive integer value in pixels (y padding for overlay).")
    print("    Transparency is a positive decimal number between 0..1 (1 = full opaque).")

def printUsageWatermark(command):
    print("{} watermark <watermarkfile> <watermarkwidth> <watermarklocation> <xpadding> <ypadding> <transparency>".format(command))
    print("Adds a watermark (picture) as overlay on top of all existing videos.")
    print("    Watermarkwidth is a positive integer value in pixels.")
    print("    Watermarklocation has the following possible values:")
    print("        0 = top left")
    print("        1 = top right")
    print("        2 = center (<xpadding>, <ypadding> has no effect)")
    print("        3 = bottom left")
    print("        4 = bottom right")
    print("    Xpadding is a positive integer value in pixels (x padding for overlay).")
    print("    Ypadding is a positive integer value in pixels (y padding for overlay).")
    print("    Transparency is a positive decimal number between 0..1 (1 = full opaque).")

def printUsageStabilize(command):
    print("{} stabilize <stepsize> <smoothing> <zoom|nozoom>".format(command))
    print("Stabilizes all \"bouncy\" videos.")
    print("    Stepsize is a positive integer value (default 6).")
    print("    Smoothing is a positive integer value (default 15).")
    print("        \"A larger value leads to a smoother video, but limits the acceleration of the camera")
    print("         (pan/tilt movements). 0 is a special case where a static camera is simulated.\"")
    print("        Note: High values will create strange results.")
    print("    Zoom means there won't be visible black borders but quality is lost (<1:1 zoom).")
    print("    Nozoom means black borders will be visible but movement correction is shown (1:1 i.e. zoom).")

def printUsageContainerToMp3VolumeFixed(command):
    print("{} conttomp3fixed <audiolevel> <tempo>".format(command))
    print("Converts audio in container (video/audio) to normalized mp3 output.")
    print("    Audiolevel is the negative integer dB level for the audio (default is -23 dB).")
    print("    Use <suffix|nosuffix> to add the volume correction \"-lvl\" filename suffix.")
    print("    Tempo is a positive decimal number between 0.5 and 2.0 (0 for no effect).")

def printUsageFlip(command):
    print("{} flip <h|v>".format(command))
    print("Flips all videos either horizontally (h) or vertically (v).")

def printUsageRotate(command):
    print("{} rotate <0|1|2>".format(command))
    print("Rotates all videos by specified amount:")
    print("    0 = 90 degrees counterclockwise")
    print("    1 = 90 degrees counterclockwise and vertical flip")
    print("    2 = 90 degrees clockwise")
    print("    3 = 90 degrees clockwise and vertical flip")
    print("    4 = 180 degrees clockwise")

def printUsageM4aToAac(command):
    print("{} m4atoaac".format(command))
    print("Extracts all audio data to aac files from all m4a containers (no conversion done).")

def printUsageFlvToMp3(command):
    print("{} flvtomp3".format(command))
    print("Extracts all audio data to mp3 files from all flv containers (no conversion done).")

def printUsageRemoveInput(command):
    print("{} removeinput".format(command))
    print("Remove specified files from input folder, one by one.")

def printUsageRemoveOutput(command):
    print("{} removeoutput".format(command))
    print("Remove specified files from output folder, one by one.")

def printUsage():
    command = sys.argv[0]
    printUsageSlideshowResize(command)
    print("")
    printUsageSlideshow(command)
    print("")
    printUsageOverlay(command)
    print("")
    printUsageWatermark(command)
    print("")
    printUsageStabilize(command)
    print("")
    printUsageContainerToMp3VolumeFixed(command)
    print("")
    printUsageFlip(command)
    print("")
    printUsageRotate(command)
    print("")
    printUsageM4aToAac(command)
    print("")
    printUsageFlvToMp3(command)
    print("")
    printUsageRemoveInput(command)
    print("")
    printUsageRemoveOutput(command)
    print("")
    sys.exit()

fftools = FFTools()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        printUsage()
    nargs = len(sys.argv)
    command = sys.argv[1]
    args = sys.argv[2:]
    if nargs == 4 and command == 'slideshowresize':
        params = {
            'xSize': args[0],
            'ySize': args[1]
        }
        fftools.slideshowResize(params)
    elif nargs == 7 and command == 'slideshow':
        params = {
            'overlayWidth':    args[0],
            'overlayLocation': args[1],
            'xPadding':        args[2],
            'yPadding':        args[3],
            'transparency':    args[4]
        }
        fftools.slideshow(params)
    elif nargs == 8 and command == 'overlay':
        params = {
            'audioSource':     args[0],
            'overlayWidth':    args[1],
            'overlayLocation': args[2],
            'xPadding':        args[3],
            'yPadding':        args[4],
            'transparency':    args[5]
        }
        fftools.overlay(params)
    elif nargs == 8 and command == 'watermark':
        params = {
            'watermarkFile':     args[0],
            'watermarkWidth':    args[1],
            'watermarkLocation': args[2],
            'xPadding':          args[3],
            'yPadding':          args[4],
            'transparency':      args[5]
        }
        fftools.watermark(params)
    elif nargs == 5 and command == 'stabilize':
        params = {
            'stepSize':  args[0],
            'smoothing': args[1],
            'zooming':   args[2]
        }
        fftools.stabilize(params)
    elif nargs == 4 and command == 'conttomp3fixed':
        params = {
            'audioLevel': args[0],
            'tempo':      args[1]
        }
        fftools.containerToMp3VolumeFixed(params)
    elif nargs == 3 and command == 'flip':
        params = {
            'flipType': args[0]
        }
        fftools.flip(params)
    elif nargs == 3 and command == 'rotate':
        params = {
            'rotateType': args[0]
        }
        fftools.rotate(params)
    elif nargs == 2 and command == 'm4atoaac':
        params = {}
        fftools.m4aToAac(params)
    elif nargs == 2 and command == 'flvtomp3':
        params = {}
        fftools.flvToMp3(params)
    elif nargs == 2 and command == 'removeinput':
        params = {}
        fftools.removeinput(params)
    elif nargs == 2 and command == 'removeoutput':
        params = {}
        fftools.removeoutput(params)
    else:
        printUsage()

