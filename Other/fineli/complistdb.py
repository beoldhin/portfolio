#!/usr/bin/env python

from __future__ import print_function

import os
import errno
import sqlite3

class CompListDB:
    cur        = None
    conn       = None
    finelidb   = None
    complistdb = None
    cachepath  = 'cache/'
    dbname     = 'complist_{}_{}.db'
    complist_CREATE = '''CREATE TABLE IF NOT EXISTS complistdb
                         (ID integer PRIMARY KEY, foodid integer,
                                                  foodname text,
                                                  amount real)'''
    complist_INSERT = '''INSERT INTO complistdb(foodid,
                                                foodname,
                                                amount)
                         VALUES(?, ?, ?)'''
    complists_FIND = '''SELECT * FROM complistdb'''
    complist_FIND = '''SELECT * FROM complistdb
                       WHERE foodid=?'''
    def __init__(self, finelidb):
        self.finelidb = finelidb
    def create_databases(self):  # PUBLIC
        components = self.finelidb.find_components()
        for language in self.finelidb.languages:
            for component in components:
                compname = component[1]
                self.delete_database(compname, language)
                self.create_database(compname, language)
    def get_list(self, compname, language):  # PUBLIC
        self.open_db_session(compname, language)
        self.cur.execute(self.complists_FIND)
        fetches = self.cur.fetchall()
        self.close_db_session()
        return fetches
    def create_database(self, compname, language):
        print("Creating list database: {:>8}, {}...".format(compname, language))
        self.open_db_session(compname, language)
        self.cur.execute(self.complist_CREATE)
        fetches = self.finelidb.find_foods_component_descending(compname)
        for i in range(len(fetches)):
            foodid   = fetches[i][1]
            foodname = self.finelidb.find_foodname(foodid,language).lower().capitalize()
            amount   = fetches[i][3]
            self.cur.execute(self.complist_INSERT, (foodid, foodname, amount,))
        self.conn.commit()
        self.close_db_session()
    def get_list_positions(self, foodids, compname, language): # PUBLIC
        returnlist = []
        self.open_db_session(compname, language)
        for foodid in foodids:
            self.cur.execute(self.complist_FIND, (foodid,))
            fetched = self.cur.fetchone()
            if fetched:
                returnlist.append(fetched[0])
            else:
                returnlist.append(-1)
        self.close_db_session()
        return returnlist
    def get_dbname(self, compname, language):
        dbname = self.dbname.format(compname.lower(), language.lower())
        dbname = self.cachepath + dbname
        return dbname
    def open_db_session(self, compname, language):
        dbname = self.get_dbname(compname, language)
        self.conn = sqlite3.connect(dbname)
        self.cur  = self.conn.cursor()
    def close_db_session(self):
        self.conn.close()
    def delete_database(self, compname, language):
        print("Deleting list database: {:>8}, {}...".format(compname, language))
        dbname = self.get_dbname(compname, language)
        try:
            os.remove(dbname)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise

