#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

from operator import itemgetter
from enum import Enum

import sqlite3
import errno
import copy
import sys
import os

class CalcDBItems(Enum):
    dbid     = 0
    foodid   = 1
    foodname = 2
    eufdname = 3
    amount   = 4

class Calculator:
    conn               = None
    cur                = None
    finelidb           = None
    calc_totals        = {}
    calc_totals_perday = {}
    calc_comps         = {}
    calc_prodcomps     = {}
    linelength         = 77
    line               = '-' * linelength
    cachepath          = 'cache/'
    dbname             = 'component_value_{}.db'
    component_value_CREATE = '''CREATE TABLE IF NOT EXISTS compvalopt
                                (ID integer PRIMARY KEY, foodid integer,
                                                         foodname text,
                                                         eufdname text,
                                                         amount real)'''
    component_value_INSERT = '''INSERT INTO compvalopt(foodid,
                                                       foodname,
                                                       eufdname,
                                                       amount)
                                VALUES (?, ?, ?, ?)'''
    component_value_all_FIND = '''SELECT * FROM compvalopt'''
    component_value_FIND = '''SELECT * FROM compvalopt
                              WHERE foodid=?'''
    def __init__(self, finelidb):
        self.finelidb = finelidb
    def create_databases(self):  # PUBLIC
        for language in self.finelidb.languages:
            self.create_database(language)
    def create_database(self, language):  # PUBLIC
        self.delete_database(language)
        print("Creating component value database for {}...".format(language))
        self.open_db_session(language)
        self.cur.execute(self.component_value_CREATE)
        compvalues = self.finelidb.find_component_values_asc_all()
        previd = sys.maxint
        for compvalue in compvalues:
            foodid, eufdname, amount = compvalue[1:4]
            compunit = self.finelidb.find_component(eufdname)[2]
            if compunit != 'KJ':
                amount = self.get_grams_for_massunit(amount, compunit)
            foodname = ''  # To save DB space (6 MB vs. 14..17 MB)
            if foodid != previd:  # This check needs the DB foodid to be ASC/DESC order
                foodname = self.finelidb.find_foodname(foodid, language)
                previd = foodid
            self.cur.execute(self.component_value_INSERT, (foodid, foodname, eufdname, amount,))
        self.conn.commit()
        self.close_db_session()
    def get_nearestlist(self, language, complist):  # PUBLIC
        self.open_db_session(language)
        self.cur.execute(self.component_value_all_FIND)
        fetches = self.cur.fetchall()
        previd = sys.maxint
        foodname = ''
        pctdiff = float(0)
        foundnearest = 0
        nearestlist = []
        skippedlist = []
        for fetched in fetches:
            foodid   = fetched[CalcDBItems.foodid]
            eufdname = fetched[CalcDBItems.eufdname]
            amount   = fetched[CalcDBItems.amount]
            if foodid != previd:
                foodname = fetched[CalcDBItems.foodname]
                if previd != sys.maxint:
                    if len(complist) == foundnearest:
                        nearestlist.append((foodname, float(pctdiff*100)))
                    else:
                        skippedlist.append((foodname, len(complist) - foundnearest))
                    pctdiff = float(0)
                    foundnearest = 0
                previd = foodid
            for compitem in complist:
                compname, compamount = compitem
                percentage = float(0)
                if compname == eufdname:
                    percentage = abs((amount - compamount) / compamount)
                    pctdiff += percentage
                    foundnearest += 1
        if len(complist) == foundnearest:
            nearestlist.append((foodname, pctdiff*100))
        nearestlist.sort(key=itemgetter(1))
        skippedlist.sort(key=itemgetter(1))
        print("{:>5} {:<60} {:>10}".format('#', 'Food name', '%'))
        print(self.line)
        for i in range(len(nearestlist)):
            sequence   = "{}.".format(i + 1)
            foodname = nearestlist[i][0].lower().capitalize()
            foodname = self.limit_string(foodname, 60)
            pctdiff  = str(round(nearestlist[i][1],1)) + '%'
            print("{:>5} {:<60} {:>10}".format(sequence, foodname, pctdiff))
        print('')
        print("{:>5} {:<65} {:>5}".format('#', 'Skipped food name', 'Diff'))
        print(self.line)
        for i in range(len(skippedlist)):
            sequence   = "{}.".format(i + 1)
            foodname   = skippedlist[i][0].lower().capitalize()
            foodname   = self.limit_string(foodname, 65)
            difference = skippedlist[i][1]
            print("{:>5} {:<65} {:>5}".format(sequence, foodname, difference))
        self.close_db_session()
    def get_massunit_mass_for_grams(self, amount, unit):  # PUBLIC
        unit = unit.upper()
        if unit == 'G':
            return amount
        elif unit == 'MG':
            return amount * 1000
        elif unit == 'UG':
            return amount * 1000000
        elif unit == 'NG':
            return amount * 1000000000
        print("ERROR: Unknown unit in (calculator)")
        return float(0)
    def get_grams_for_massunit(self, amount, unit):  # PUBLIC
        unit = unit.upper()
        if unit == 'G':
            return amount
        if unit == 'KG':
            return amount * 1000
        elif unit == 'MG':
            return amount / 1000
        elif unit == 'UG':
            return amount / 1000000
        elif unit == 'NG':
            return amount / 1000000000
        print("ERROR: Unknown unit for (calculator)")
        return float(0)
    def get_in_above_grams(self, grams):  # PUBLIC
        kilo = float(1000)
        mega = float(1000000)
        if grams >= mega:
            return round(grams/mega, 1), 't'
        if grams >= kilo:
            return round(grams/kilo, 1), 'kg'
        return round(grams, 1), 'g'
    def get_in_joules(self, kjoules):  # PUBLIC
        kilo = float(1000)
        mega = float(1000000)
        giga = float(1000000000)
        tera = float(1000000000000)
        megadiv = kjoules / (mega / kilo)
        gigadiv = kjoules / (giga / kilo)
        teradiv = kjoules / (tera / kilo)
        if round(teradiv) > float(0):
            return round(teradiv, 1), "TJ"
        elif round(gigadiv) > float(0):
            return round(gigadiv, 1), "GJ"
        elif round(megadiv) > float(0):
            return round(megadiv, 1), "MJ"
        return round(kjoules, 1), "kJ"
    def get_in_cals(self, kcals):  # PUBLIC
        if kcals < float(10000):
            return int(round(kcals)), "kcal"
        kilo = float(1000)
        mega = float(1000000)
        giga = float(1000000000)
        tera = float(1000000000000)
        megadiv = kcals / (mega / kilo)
        gigadiv = kcals / (giga / kilo)
        teradiv = kcals / (tera / kilo)
        if round(teradiv) > float(0):
            return round(teradiv, 1), "Mcal"
        elif round(gigadiv) > float(0):
            return round(gigadiv, 1), "Gcal"
        elif round(megadiv) > float(0):
            return round(megadiv, 1), "Mcal"
        return round(kcals, 1), "kcal"
    def convert_kJ_to_kcal(self, kJ):  # PUBLIC
        return kJ * 0.239005736
    def get_grams_for_volume_or_mass(self, foodid, amount):  # PUBLIC
        result = 0
        amountsplit = amount.split(' ')
        if len(amountsplit) >= 2:
            unit = amountsplit[-1].upper()
            amount = float(amountsplit[len(amountsplit)-2])
            decilitres = self.get_decilitres_for_volume(amount, unit, False)
            if decilitres > float(0):
                massperdl = self.get_foodaddunit(foodid)
                if massperdl == float(0):
                    print("ERROR: Unhandled foodaddunit (calculator)")
                    sys.exit(1)
                result = massperdl * decilitres
            else:
                result = self.get_grams_for_mass(amount, unit)
                if result == float(0):
                    print("ERROR: Unhandled units (calculator)")
                    sys.exit(1)
        return result
    def initialize(self):  # PUBLIC
        self.calc_totals.clear()
        self.calc_totals_perday.clear()
    def calculate_product(self, productid, eufdname, amount):  # PUBLIC
        if not eufdname in self.calc_totals:
            self.calc_totals[eufdname] = amount
        else:
            self.calc_totals[eufdname] += amount
        if not productid in self.calc_prodcomps:
            self.calc_prodcomps[productid] = {}
        if not eufdname in self.calc_prodcomps[productid]:
            self.calc_prodcomps[productid][eufdname] = amount
        else:
            self.calc_prodcomps[produdctid][eufdname] += amount
    def calculate_components(self, foodid, multiplier):  # PUBLIC
        self.cur.execute(self.component_value_FIND, (foodid,))
        fetches = self.cur.fetchall()
        for fetched in fetches:
            eufdname = fetched[CalcDBItems.eufdname]
            amount   = fetched[CalcDBItems.amount] * multiplier
            if not eufdname in self.calc_totals:
                self.calc_totals[eufdname] = amount
            else:
                self.calc_totals[eufdname] += amount
            if not foodid in self.calc_comps:
                self.calc_comps[foodid] = {}
            if not eufdname in self.calc_comps[foodid]:
                self.calc_comps[foodid][eufdname] = amount
            else:
                self.calc_comps[foodid][eufdname] += amount
    def create_perday(self, numofdays):  # PUBLIC
        self.calc_totals_perday = copy.deepcopy(self.calc_totals)
        for key, value in self.calc_totals_perday.items():
            self.calc_totals_perday[key] = value / numofdays
    def get_value_total(self, eufdname):  # PUBLIC
        value = float(0)
        try:
            value = self.calc_totals[eufdname]
        except:
            pass
        return value
    def get_value_perday(self, eufdname):  # PUBLIC
        value = float(0)
        try:
            value = self.calc_totals_perday[eufdname]
        except:
            pass
        return value
    def get_value_comp(self, foodid, eufdname):  # PUBLIC
        value = float(0)
        try:
            value = self.calc_comps[foodid][eufdname]
        except:
            pass
        return value
    def get_value_prodcomp(self, productid, eufdname):  # PUBLIC
        value = float(0)
        try:
            value = self.calc_prodcomps[productid][eufdname]
        except:
            pass
        return value
    def get_comp_foodids(self):  # PUBLIC
        foodids = []
        for key, value in self.calc_comps.items():
            foodids.append(key)
        return foodids
    def get_prodcomp_productids(self):  # PUBLIC
        productids = []
        for key, value in self.calc_prodcomps.items():
            productids.append(key)
        return productids
    def limit_string(self, strtolimit, limit):
        cutlength = limit - len('...')
        if len(strtolimit) > cutlength:
            strtolimit = strtolimit[:cutlength] + '...'
        return strtolimit
    def get_decilitres_for_volume(self, amount, unit, checkerror=True):
        if unit == 'DL':
            return amount
        elif unit == 'L':
            return amount * float(10)
        elif unit == 'RKL':
            return amount * 0.15
        elif unit == 'TL':
            return amount * 0.05
        elif unit == 'MM':
            return amount * 0.01
        elif unit == 'UKCUP':
            return amount * 2.8
        elif unit == 'USCUP':
            return amount * 2.4
        elif unit == 'UKPINT':
            return amount * 5.7
        elif unit == 'USPINT':
            return amount * 4.8
        elif unit == 'GALLON':
            return amount * 3.75
        if checkerror:
            print("ERROR: Unknown unit '{}' for decilitres (calculator)".format(unit))
        return float(0)
    def get_grams_for_mass(self, amount, unit):
        if unit == 'G':
            return amount
        elif unit == 'KG':
            return amount * float(1000)
        elif unit == 'LB':
            return amount * float(454)
        elif unit == 'STICK':
            return amount * float(113)
        elif unit == 'OZ':
            return amount * 28.5
        print("ERROR: Unknown unit '{}' for grams (calculator)".format(unit))
        return float(0)
    def get_foodaddunit(self, foodid):
        addunits = self.finelidb.find_foodaddunits(foodid)
        for addunit in addunits:
            foodunit, mass = addunit[2:4]
            if foodunit == 'DL':
                return mass
            elif foodunit == 'L':
                return mass * 0.1
            elif foodunit == 'RKL':
                return mass * (1/0.15)
            elif foodunit == 'TL':
                return mass * (1/0.05)
            elif foodunit == 'MM':
                return mass * (1/0.01)
        print(">> {} {}".format(foodid, addunits))
        return float(0)
    def get_dbname(self, language):
        dbname = self.cachepath + self.dbname.format(language)
        return dbname
    def open_db_session(self, language):
        dbname    = self.get_dbname(language)
        self.conn = sqlite3.connect(dbname)
        self.cur  = self.conn.cursor()
    def close_db_session(self):
        if self.conn:
            self.conn.close()
    def delete_database(self, language):
        print("Deleting component value database for {}...".format(language))
        dbname = self.get_dbname(language)
        try:
            os.remove(dbname)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise

