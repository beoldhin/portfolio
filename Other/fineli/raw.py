#!/usr/bin/env python

from __future__ import unicode_literals

class Raw:
    foodids = []
    calculator = 0
    def __init__(self, calculator):
        self.calculator = calculator
    def initialize(self):  # PUBLIC
        del self.calculator[:]
    def calculate_raw(self, foodid, amount):  # PUBLIC
        multiplier = self.calculator.get_grams_for_volume_or_mass(foodid, amount) / 100
        self.calculator.calculate_components(foodid, multiplier)
        self.foodids.append(int(foodid))
    def get_foodids(self):  # PUBLIC
        return self.foodids
    def uninitialize(self):  # PUBLIC
        pass

