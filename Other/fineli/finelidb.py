#!/usr/bin/env python

from enum import Enum

import os
import sys
import csv
import errno
import locale
import sqlite3

class Totalenergies(Enum):
    fat    = 0
    prot   = 1
    choavl = 2
    alc    = 3
    fibc   = 4
    oa     = 5
    sugoh  = 6

class Fats(Enum):
    fasat  = 0
    fatrn  = 1
    famcis = 2
    fapu   = 3

class Bases(Enum):
    enerc  = 0
    choavl = 1
    fat    = 2
    prot   = 3
    alc    = 4

class FineliDB:
    cur                        = 0
    conn                       = 0
    languages                  = [ 'EN', 'FI', 'SV', ]
    csv_delimiter              = ';'
    cachepath                  = 'cache/'
    file_encoding              = 'ISO-8859-1'
    data_locale                = 'fi_FI.UTF-8'
    fineliname                 = 'fineli/Fineli_Rel16_Fine68_open/'  # Peruspaketti 2
    dbname                     = 'fineli.db'
    dbname_csv_component       = 'component.csv'
    dbname_csv_component_value = 'component_value.csv'
    dbname_csv_contribfood     = 'contribfood.csv'
    dbname_csv_food            = 'food.csv'
    dbname_csv_foodaddunit     = 'foodaddunit.csv'
    dbname_csv_publication     = 'publication.csv'
    dbname_csv_acqtype         = 'acqtype_{}.csv'
    dbname_csv_cmpclass        = 'cmpclass_{}.csv'
    dbname_csv_compunit        = 'compunit_{}.csv'
    dbname_csv_eufdname        = 'eufdname_{}.csv'
    dbname_csv_foodname        = 'foodname_{}.csv'
    dbname_csv_foodtype        = 'foodtype_{}.csv'
    dbname_csv_foodunit        = 'foodunit_{}.csv'
    dbname_csv_fuclass         = 'fuclass_{}.csv'
    dbname_csv_igclass         = 'igclass_{}.csv'
    dbname_csv_methtype        = 'methtype_{}.csv'
    dbname_csv_process         = 'process_{}.csv'
    multiplier_totalenergies   = [ 37.0, 17.0, 17.0, 29.0, 8.0, 13.0, 10.0 ]
    summary_totalenergies      = [ 'FAT', 'PROT', 'CHOAVL', 'ALC', 'FIBC', 'OA', 'SUGOH' ]
    summary_fats               = [ 'FASAT', 'FATRN', 'FAMCIS', 'FAPU' ]
    summary_carbs              = [ 'SUGAR', 'STARCH' ]
    detailed_bases             = [ 'ENERC', 'CHOAVL', 'FAT', 'PROT', 'ALC' ]
    detailed_fibers            = [ 'FIBC', 'OA', 'SUGOH', 'STARCH', 'SUGAR', 'FRUS', 'GALS', \
                                   'GLUS', 'LACS', 'MALS', 'SUCS', 'PSACNCS', 'FIBINS' ]
    detailed_fats              = [ 'FAFRE', 'FAPU', 'FAMCIS', 'FASAT', 'FATRN', 'FAPUN3', \
                                   'FAPUN6', 'F18D2CN6', 'F18D3N3', 'F20D5N3', 'F22D6N3', \
                                   'CHOLE', 'STERT' ]
    detailed_minerals          = [ 'CA', 'FE', 'ID', 'K', 'MG', 'NA', 'NACL', 'P', 'SE', 'ZN' ]
    detailed_nitrogens         = [ 'TRP' ]
    detailed_vitamins          = [ 'FOL', 'NIAEQ', 'NIA', 'VITPYRID', 'RIBF', 'THIA', 'VITB12', \
                                   'VITC', 'VITA', 'CAROTENS', 'VITD', 'VITE', 'VITK' ]
    # Stored searches for caching
    foodid_component_values    = 0
    component_values           = None
    # COMPONENT
    component_CREATE = '''CREATE TABLE IF NOT EXISTS component
                          (ID integer PRIMARY KEY, eufdname  text,
                                                   compunit  text,
                                                   cmpclass  text,
                                                   cmpclassp text)'''
    component_INSERT = '''INSERT INTO component(eufdname,
                                                compunit,
                                                cmpclass,
                                                cmpclassp)
                          VALUES (?, ?, ?, ?)'''
    components_FIND = '''SELECT * FROM component'''
    component_FIND = '''SELECT * FROM component
                        WHERE eufdname=?'''
    # COMPONENT_VALUE
    component_value_CREATE = '''CREATE TABLE IF NOT EXISTS component_value
                                (ID integer PRIMARY KEY, foodid integer,
                                                         eufdname text,
                                                         bestloc  real,
                                                         acqtype  text,
                                                         methtype text)'''
    component_value_INSERT = '''INSERT INTO component_value(foodid,
                                                            eufdname,
                                                            bestloc,
                                                            acqtype,
                                                            methtype)
                                VALUES (?, ?, ?, ?, ?)'''
    component_value_FIND = '''SELECT * FROM component_value
                              WHERE foodid=? AND eufdname=?'''
    component_values_FIND = '''SELECT * FROM component_value
                               WHERE foodid=?'''
    component_values_all_FIND = '''SELECT * FROM component_value'''
    component_values_all_asc_FIND = '''SELECT * FROM component_value
                                       ORDER by foodid ASC'''
    component_value_asc_FIND = '''SELECT * FROM component_value
                                  WHERE eufdname=?
                                  ORDER BY bestloc ASC'''
    component_value_desc_FIND = '''SELECT * FROM component_value
                                   WHERE eufdname=?
                                   ORDER BY bestloc DESC'''
    # CONTRIBFOOD
    contribfood_CREATE = '''CREATE TABLE IF NOT EXISTS contribfood
                            (ID integer PRIMARY KEY, foodid   integer,
                                                     confdid  integer,
                                                     amount   real,
                                                     foodunit text,
                                                     mass     real,
                                                     evremain text,
                                                     recyear  integer)'''
    contribfood_INSERT = '''INSERT INTO contribfood(foodid,
                                                    confdid,
                                                    amount,
                                                    foodunit,
                                                    mass,
                                                    evremain,
                                                    recyear)
                            VALUES (?, ?, ?, ?, ?, ?, ?)'''
    # FOOD
    food_CREATE = '''CREATE TABLE IF NOT EXISTS food
                     (ID integer PRIMARY KEY, foodid integer,
                                              foodname text,
                                              foodtype text,
                                              process  text,
                                              edport   real,
                                              igclass  text,
                                              igclassp text,
                                              fuclass  text,
                                              fuclassp text)'''
    food_INSERT = '''INSERT INTO food(foodid,
                                      foodname,
                                      foodtype,
                                      process,
                                      edport,
                                      igclass,
                                      igclassp,
                                      fuclass,
                                      fuclassp)
                     VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)'''
    food_FIND = '''SELECT * FROM food
                   WHERE foodid=?'''
    # FOODADDUNIT
    foodaddunit_CREATE = '''CREATE TABLE IF NOT EXISTS foodaddunit
                            (ID integer PRIMARY KEY, foodid   integer,
                                                     foodunit text,
                                                     mass     real)'''
    foodaddunit_INSERT = '''INSERT INTO foodaddunit(foodid,
                                                    foodunit,
                                                    mass)
                            VALUES(?, ?, ?)'''
    foodaddunit_FIND = '''SELECT * FROM foodaddunit
                          WHERE foodid=? AND foodunit=?'''
    foodaddunits_FIND = '''SELECT * FROM foodaddunit
                           WHERE foodid=?'''
    # PUBLICATION
    publication_CREATE = '''CREATE TABLE IF NOT EXISTS publication
                            (ID integer PRIMARY KEY, publid   integer,
                                                     citation text,
                                                     reftype  text,
                                                     acqtype  text,
                                                     doi      text)'''
    publication_INSERT = '''INSERT INTO publication(publid,
                                                    citation,
                                                    reftype,
                                                    acqtype,
                                                    doi)
                            VALUES(?, ?, ?, ?, ?)'''
    # ACQTYPE
    acqtype_CREATE = '''CREATE TABLE IF NOT EXISTS acqtype
                        (ID integer PRIMARY KEY, thscode  text,
                                                 descript text,
                                                 lang     text)'''
    acqtype_INSERT = '''INSERT INTO acqtype(thscode,
                                            descript,
                                            lang)
                        VALUES (?, ?, ?)'''
    acqtype_FIND = '''SELECT * FROM acqtype
                      WHERE thscode=? AND lang=?'''
    # CMPCLASS
    cmpclass_CREATE = '''CREATE TABLE IF NOT EXISTS cmpclass
                         (ID integer PRIMARY KEY, thscode  text,
                                                  descript text,
                                                  lang     text)'''
    cmpclass_INSERT = '''INSERT INTO cmpclass(thscode,
                                              descript,
                                              lang)
                         VALUES (?, ?, ?)'''
    cmpclass_FIND = '''SELECT * FROM cmpclass
                       WHERE thscode=? AND lang=?'''
    # COMPUNIT
    compunit_CREATE = '''CREATE TABLE IF NOT EXISTS compunit
                         (ID integer PRIMARY KEY, thscode  text,
                                                  descript text,
                                                  lang     text)'''
    compunit_INSERT = '''INSERT INTO compunit(thscode,
                                              descript,
                                              lang)
                         VALUES (?, ?, ?)'''
    compunit_FIND = '''SELECT * FROM compunit
                       WHERE thscode=? AND lang=?'''
    # EUFDNAME
    eufdname_CREATE = '''CREATE TABLE IF NOT EXISTS eufdname
                         (ID integer, thscode  text,
                                      descript text,
                                      lang     text)'''
    eufdname_INSERT = '''INSERT INTO eufdname(thscode,
                                              descript,
                                              lang)
                        VALUES (?, ?, ?)'''
    eufdname_FIND = '''SELECT * FROM eufdname
                       WHERE thscode=? AND lang=?'''
    # FOODNAME
    foodname_CREATE = '''CREATE TABLE IF NOT EXISTS foodname
                         (ID integer, foodid   integer,
                                      foodname text,
                                      lang     text)'''
    foodname_INSERT = '''INSERT INTO foodname(foodid,
                                              foodname,
                                              lang)
                        VALUES (?, ?, ?)'''
    foodname_FIND = '''SELECT * FROM foodname
                       WHERE foodid=? AND lang=?'''
    # FOODTYPE
    foodtype_CREATE = '''CREATE TABLE IF NOT EXISTS foodtype
                         (ID integer, thscode  text,
                                      descript text,
                                      lang     text)'''
    foodtype_INSERT = '''INSERT INTO foodtype(thscode,
                                              descript,
                                              lang)
                        VALUES (?, ?, ?)'''
    foodtype_FIND = '''SELECT * FROM foodtype
                       WHERE thscode=? AND lang=?'''
    # FOODUNIT
    foodunit_CREATE = '''CREATE TABLE IF NOT EXISTS foodunit
                         (ID integer, thscode  text,
                                      descript text,
                                      lang     text)'''
    foodunit_INSERT = '''INSERT INTO foodunit(thscode,
                                              descript,
                                              lang)
                        VALUES (?, ?, ?)'''
    foodunit_FIND = '''SELECT * FROM foodunit
                       WHERE thscode=? AND lang=?'''
    # FUCLASS
    fuclass_CREATE = '''CREATE TABLE IF NOT EXISTS fuclass
                        (ID integer, thscode  text,
                                     descript text,
                                     lang     text)'''
    fuclass_INSERT = '''INSERT INTO fuclass(thscode,
                                            descript,
                                            lang)
                        VALUES (?, ?, ?)'''
    fuclass_FIND = '''SELECT * FROM fuclass
                      WHERE thscode=? AND lang=?'''
    # IGCLASS
    igclass_CREATE = '''CREATE TABLE IF NOT EXISTS igclass
                        (ID integer, thscode  text,
                                     descript text,
                                     lang     text)'''
    igclass_INSERT = '''INSERT INTO igclass(thscode,
                                            descript,
                                            lang)
                        VALUES (?, ?, ?)'''
    igclass_FIND = '''SELECT * FROM igclass
                      WHERE thscode=? AND lang=?'''
    # METHTYPE
    methtype_CREATE = '''CREATE TABLE IF NOT EXISTS methtype
                        (ID integer, thscode  text,
                                     descript text,
                                     lang     text)'''
    methtype_INSERT = '''INSERT INTO methtype(thscode,
                                              descript,
                                              lang)
                        VALUES (?, ?, ?)'''
    methtype_FIND = '''SELECT * FROM methtype
                       WHERE thscode=? AND lang=?'''
    # PROCESS
    process_CREATE = '''CREATE TABLE IF NOT EXISTS process
                        (ID integer, thscode  text,
                                     descript text,
                                     lang     text)'''
    process_INSERT = '''INSERT INTO process(thscode,
                                            descript,
                                            lang)
                        VALUES (?, ?, ?)'''
    process_FIND = '''SELECT * FROM process
                      WHERE thscode=? AND lang=?'''
    def __init__(self):
        self.dbname                     = self.cachepath  + self.dbname
        self.dbname_csv_component       = self.fineliname + self.dbname_csv_component
        self.dbname_csv_component_value = self.fineliname + self.dbname_csv_component_value
        self.dbname_csv_contribfood     = self.fineliname + self.dbname_csv_contribfood
        self.dbname_csv_food            = self.fineliname + self.dbname_csv_food
        self.dbname_csv_foodaddunit     = self.fineliname + self.dbname_csv_foodaddunit
        self.dbname_csv_publication     = self.fineliname + self.dbname_csv_publication
        self.dbname_csv_acqtype         = self.fineliname + self.dbname_csv_acqtype
        self.dbname_csv_cmpclass        = self.fineliname + self.dbname_csv_cmpclass
        self.dbname_csv_compunit        = self.fineliname + self.dbname_csv_compunit
        self.dbname_csv_eufdname        = self.fineliname + self.dbname_csv_eufdname
        self.dbname_csv_foodname        = self.fineliname + self.dbname_csv_foodname
        self.dbname_csv_foodtype        = self.fineliname + self.dbname_csv_foodtype
        self.dbname_csv_foodunit        = self.fineliname + self.dbname_csv_foodunit
        self.dbname_csv_fuclass         = self.fineliname + self.dbname_csv_fuclass
        self.dbname_csv_igclass         = self.fineliname + self.dbname_csv_igclass
        self.dbname_csv_methtype        = self.fineliname + self.dbname_csv_methtype
        self.dbname_csv_process         = self.fineliname + self.dbname_csv_process
        locale.setlocale(locale.LC_NUMERIC, self.data_locale)
    # def convert_mass(self, value, unit):
    #     if unit == 'KG':
    #         return value * 1000
    #     elif unit == 'MG':
    #         return value / 1000
    #     elif unit == 'NG':
    #         return value / 1000000000
    #     elif unit == 'UG':
    #         return value / 1000000
    #     return value
    def create_database_component(self):
        self.cur.execute(self.component_CREATE)
        dbname = self.dbname_csv_component
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 4:
                    print("ERROR: Invalid number of columns (component)")
                    sys.exit(1)
                if not firstrow:
                    eufdname  = unicode(row[0], self.file_encoding)
                    compunit  = unicode(row[1], self.file_encoding)
                    cmpclass  = unicode(row[2], self.file_encoding)
                    cmpclassp = unicode(row[3], self.file_encoding)
                    self.cur.execute(self.component_INSERT, (eufdname,
                                                             compunit,
                                                             cmpclass,
                                                             cmpclassp,) )
                else:
                    firstrow = False
                    if row[0] != 'EUFDNAME' or \
                       row[1] != 'COMPUNIT' or \
                       row[2] != 'CMPCLASS' or \
                       row[3] != 'CMPCLASSP':
                          print("ERROR: Invalid header line (component)")
                          sys.exit(1)
    def create_database_component_value(self):
        self.cur.execute(self.component_value_CREATE)
        dbname = self.dbname_csv_component_value
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 5:
                    print("ERROR: Invalid number of columns (component_value)")
                    sys.exit(1)
                if not firstrow:
                    foodid   = locale.atoi(row[0])
                    eufdname = unicode(row[1], self.file_encoding)
                    bestloc  = locale.atof(row[2])
                    acqtype  = unicode(row[3], self.file_encoding)
                    methtype = unicode(row[4], self.file_encoding)
                    self.cur.execute(self.component_value_INSERT, (foodid,
                                                                   eufdname,
                                                                   bestloc,
                                                                   acqtype,
                                                                   methtype,) )
                else:
                    firstrow = False
                    if row[0] != 'FOODID'   or \
                       row[1] != 'EUFDNAME' or \
                       row[2] != 'BESTLOC'  or \
                       row[3] != 'ACQTYPE'  or \
                       row[4] != 'METHTYPE':
                          print("ERROR: Invalid header line (component_value)")
                          sys.exit(1)
    def create_database_contribfood(self):
        self.cur.execute(self.contribfood_CREATE)
        dbname = self.dbname_csv_contribfood
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 7:
                    print("ERROR: Invalid number of columns (contribfood)")
                    sys.exit(1)
                if not firstrow:
                    foodid   = locale.atoi(row[0])
                    confdid  = unicode(row[1], self.file_encoding)
                    if row[2] == '':
                        amount = float(0)
                    else:
                        amount   = locale.atof(row[2])
                    foodunit = unicode(row[3], self.file_encoding)
                    if row[4] == '':
                        mass = float(0)
                    else:
                        mass     = locale.atof(row[4])
                    evremain = unicode(row[5], self.file_encoding)
                    if row[6] == '':
                        recyear = 0
                    else:
                        recyear = locale.atoi(row[6])
                    self.cur.execute(self.contribfood_INSERT, (foodid,
                                                               confdid,
                                                               amount,
                                                               foodunit,
                                                               mass,
                                                               evremain,
                                                               recyear,) )
                else:
                    firstrow = False
                    if row[0] != 'FOODID'   or \
                       row[1] != 'CONFDID'  or \
                       row[2] != 'AMOUNT'   or \
                       row[3] != 'FOODUNIT' or \
                       row[4] != 'MASS'     or \
                       row[5] != 'EVREMAIN' or \
                       row[6] != 'RECYEAR':
                           print("ERROR: Invalid header line (contribfood)")
                           sys.exit(1)
    def create_database_food(self):
        self.cur.execute(self.food_CREATE)
        dbname = self.dbname_csv_food
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 9:
                    print("ERROR: Invalid number of columns (food)")
                    sys.exit(1)
                if not firstrow:
                    foodid   = locale.atoi(row[0])
                    foodname = unicode(row[1], self.file_encoding)
                    foodtype = unicode(row[2], self.file_encoding)
                    process  = unicode(row[3], self.file_encoding)
                    edport   = locale.atof(row[4])
                    igclass  = unicode(row[5], self.file_encoding)
                    igclassp = unicode(row[6], self.file_encoding)
                    fuclass  = unicode(row[7], self.file_encoding)
                    fuclassp = unicode(row[8], self.file_encoding)
                    self.cur.execute(self.food_INSERT, (foodid,
                                                        foodname,
                                                        foodtype,
                                                        process,
                                                        edport,
                                                        igclass,
                                                        igclassp,
                                                        fuclass,
                                                        fuclassp,) )
                else:
                    firstrow = False
                    if row[0] != 'FOODID'   or \
                       row[1] != 'FOODNAME' or \
                       row[2] != 'FOODTYPE' or \
                       row[3] != 'PROCESS'  or \
                       row[4] != 'EDPORT'   or \
                       row[5] != 'IGCLASS'  or \
                       row[6] != 'IGCLASSP' or \
                       row[7] != 'FUCLASS'  or \
                       row[8] != 'FUCLASSP':
                           print("ERROR: Invalid header line (food)")
                           sys.exit(1)
    def create_database_foodaddunit(self):
        self.cur.execute(self.foodaddunit_CREATE)
        dbname = self.dbname_csv_foodaddunit
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (foodaddunit)")
                    sys.exit(1)
                if not firstrow:
                    foodid   = locale.atoi(row[0])
                    foodunit = unicode(row[1], self.file_encoding)
                    mass     = locale.atof(row[2])
                    self.cur.execute(self.foodaddunit_INSERT, (foodid,
                                                               foodunit,
                                                               mass,) )
                else:
                    firstrow = False
                    if row[0] != 'FOODID'   or \
                       row[1] != 'FOODUNIT' or \
                       row[2] != 'MASS':
                        print("ERROR: Invalid header line (foodaddunit)")
                        sys.exit(1)
    def create_database_publication(self):
        self.cur.execute(self.publication_CREATE)
        dbname = self.dbname_csv_publication
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 5:
                    print("ERROR: Invalid number of columns (publication)")
                    sys.exit(1)
                if not firstrow:
                    publid   = unicode(row[0], self.file_encoding)
                    citation = unicode(row[1], self.file_encoding)
                    reftype  = unicode(row[2], self.file_encoding)
                    acqtype  = unicode(row[3], self.file_encoding)
                    doi      = unicode(row[4], self.file_encoding)
                    self.cur.execute(self.publication_INSERT, (publid,
                                                               citation,
                                                               reftype,
                                                               acqtype,
                                                               doi,) )
                else:
                    firstrow = False
                    if row[0] != 'PUBLID'   or \
                       row[1] != 'CITATION' or \
                       row[2] != 'REFTYPE'  or \
                       row[3] != 'ACQTYPE'  or \
                       row[4] != 'DOI':
                        print("ERROR: Invalid header line (publication)")
                        sys.exit(1)
    def create_database_acqtype(self, language):
        self.cur.execute(self.acqtype_CREATE)
        dbname = self.dbname_csv_acqtype.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (acqtype)")
                    sys.exit(1)
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.acqtype_INSERT, (thscode,
                                                           descript,
                                                           lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (acqtype)")
                        sys.exit(1)
    def create_database_cmpclass(self, language):
        self.cur.execute(self.cmpclass_CREATE)
        dbname = self.dbname_csv_cmpclass.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (cmpclass)")
                    sys.exit(1)
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.cmpclass_INSERT, (thscode,
                                                            descript,
                                                            lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (cmpclass)")
                        sys.exit(1)
    def create_database_compunit(self, language):
        self.cur.execute(self.compunit_CREATE)
        dbname = self.dbname_csv_compunit.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (compunit)")
                    sys.exit()
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.compunit_INSERT, (thscode,
                                                            descript,
                                                            lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (compunit)")
                        sys.exit(1)
    def create_database_eufdname(self, language):
        self.cur.execute(self.eufdname_CREATE)
        dbname = self.dbname_csv_eufdname.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (eufdname)")
                    sys.exit()
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.eufdname_INSERT, (thscode,
                                                            descript,
                                                            lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (eufdname)")
                        sys.exit(1)
    def create_database_foodname(self, language):
        self.cur.execute(self.foodname_CREATE)
        dbname = self.dbname_csv_foodname.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (foodname)")
                    sys.exit()
                if not firstrow:
                    foodid   = locale.atoi(row[0])
                    foodname = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.foodname_INSERT, (foodid,
                                                            foodname,
                                                            lang,) )
                else:
                    firstrow = False
                    if row[0] != 'FOODID'   or \
                       row[1] != 'FOODNAME' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (foodname)")
                        sys.exit(1)
    def create_database_foodtype(self, language):
        self.cur.execute(self.foodtype_CREATE)
        dbname = self.dbname_csv_foodtype.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (foodtype)")
                    sys.exit()
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.foodtype_INSERT, (thscode,
                                                            descript,
                                                            lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (foodtype)")
                        sys.exit(1)
    def create_database_foodunit(self, language):
        self.cur.execute(self.foodunit_CREATE)
        dbname = self.dbname_csv_foodunit.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (foodunit)")
                    sys.exit()
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.foodunit_INSERT, (thscode,
                                                            descript,
                                                            lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (foodunit)")
                        sys.exit(1)
    def create_database_fuclass(self, language):
        self.cur.execute(self.fuclass_CREATE)
        dbname = self.dbname_csv_fuclass.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (fuclass)")
                    sys.exit()
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.fuclass_INSERT, (thscode,
                                                           descript,
                                                           lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (fuclass)")
                        sys.exit(1)
    def create_database_igclass(self, language):
        self.cur.execute(self.igclass_CREATE)
        dbname = self.dbname_csv_igclass.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (igclass)")
                    sys.exit()
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.igclass_INSERT, (thscode,
                                                           descript,
                                                           lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (igclass)")
                        sys.exit(1)
    def create_database_methtype(self, language):
        self.cur.execute(self.methtype_CREATE)
        dbname = self.dbname_csv_methtype.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (methtype)")
                    sys.exit()
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.methtype_INSERT, (thscode,
                                                            descript,
                                                            lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (methtype)")
                        sys.exit(1)
    def create_database_process(self, language):
        self.cur.execute(self.process_CREATE)
        dbname = self.dbname_csv_process.format(language)
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=self.csv_delimiter)
            for row in csvreader:
                if len(row) != 3:
                    print("ERROR: Invalid number of columns (process)")
                    sys.exit()
                if not firstrow:
                    thscode  = unicode(row[0], self.file_encoding)
                    descript = unicode(row[1], self.file_encoding)
                    lang     = unicode(row[2], self.file_encoding)
                    self.cur.execute(self.process_INSERT, (thscode,
                                                           descript,
                                                           lang,) )
                else:
                    firstrow = False
                    if row[0] != 'THSCODE'  or \
                       row[1] != 'DESCRIPT' or \
                       row[2] != 'LANG':
                        print("ERROR: Invalid header line (process)")
                        sys.exit(1)
    def open_db_session(self):  # PUBLIC
        self.conn = sqlite3.connect(self.dbname)
        self.cur = self.conn.cursor()
    def delete_database(self):  # PUBLIC
        print("Deleting main database...")
        try:
            os.remove(self.dbname)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise
    def close_db_session(self):  # PUBLIC
        self.conn.close()
    def create_database(self):  # PUBLIC
        print("Creating main database...")
        self.open_db_session()
        self.create_database_component()
        self.create_database_component_value()
        self.create_database_contribfood()
        self.create_database_food()
        self.create_database_foodaddunit()
        # self.create_database_publication()
        for language in self.languages:
            self.create_database_acqtype(language)
            self.create_database_cmpclass(language)
            self.create_database_compunit(language)
            self.create_database_eufdname(language)
            self.create_database_foodname(language)
            self.create_database_foodtype(language)
            self.create_database_foodunit(language)
            self.create_database_fuclass(language)
            self.create_database_igclass(language)
            self.create_database_methtype(language)
            self.create_database_process(language)
        self.conn.commit()
        self.close_db_session()
    def find_acqtype(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.acqtype_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_cmpclass(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.cmpclass_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_compunit(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.compunit_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_eufdname(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.eufdname_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_foodtype(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.foodtype_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_foodunit(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.foodunit_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_fuclass(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.fuclass_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_igclass(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.igclass_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_methtype(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.methtype_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_process(self, thscode, lang):  # PUBLIC
        self.cur.execute(self.process_FIND, (thscode, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_foodname(self, foodid, lang):  # PUBLIC
        self.cur.execute(self.foodname_FIND, (foodid, lang,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def find_foodaddunits(self, foodid):  # PUBLIC
        self.cur.execute(self.foodaddunits_FIND, (foodid,))
        fetches = self.cur.fetchall()
        return fetches
    def find_foodaddunit(self, foodid, foodunit):  # PUBLIC
        self.cur.execute(self.foodaddunit_FIND, (foodid, foodunit,))
        fetched = self.cur.fetchone()
        if not fetched:
            return None
        return fetched[3]
    # def find_component_values_fast(self, foodid):  # PUBLIC
    #     self.cur.execute(self.component_values_FIND, (foodid,))
    #     fetches = self.cur.fetchall()
    #     return fetches
    def find_component_values_all(self):  # PUBLIC
        self.cur.execute(self.component_values_all_FIND)
        fetches = self.cur.fetchall()
        return fetches
    def find_component_values_asc_all(self):  # PUBLIC
        self.cur.execute(self.component_values_all_asc_FIND)
        fetches = self.cur.fetchall()
        return fetches
    def find_component_values(self, foodid):  # PUBLIC
        self.cur.execute(self.component_values_FIND, (foodid,))
        fetches = self.cur.fetchall()
        for i in range(len(fetches)):
            fetches[i] = list(fetches[i])
            fetched = fetches[i]
            eufdname = fetched[2]
            compunit = self.find_component(eufdname)[0]
            fetches[i][3] = self.convert_mass(fetched[3], compunit)
        return fetches
    def find_component_value(self, foodid, eufdname):  # PUBLIC
        self.cur.execute(self.component_value_FIND, (foodid, eufdname,))
        fetched = self.cur.fetchone()
        if not fetched:
            return [None, None, None, float(0), None, None]
        fetched = list(fetched)
        compunit = self.find_component(eufdname)[0]
        fetched[3] = self.convert_mass(fetched[3], compunit)
        return fetched
    def find_components(self):  # PUBLIC
        self.cur.execute(self.components_FIND)
        fetches = self.cur.fetchall()
        return fetches
    def find_component(self, eufdname, lang=None):  # PUBLIC
        self.cur.execute(self.component_FIND, (eufdname,))
        fetched   = self.cur.fetchone()
        # compunit  = fetched[2]
        # cmpclass  = fetched[3]
        # cmpclassp = fetched[4]
        # if lang:
        #     compunit  = self.find_compunit(compunit,  lang)
        #     cmpclass  = self.find_cmpclass(cmpclass,  lang)
        #     cmpclassp = self.find_cmpclass(cmpclassp, lang)
        # return compunit, cmpclass, cmpclassp
        return fetched
    def find_food(self, foodid, lang=None):  # PUBLIC
        self.cur.execute(self.food_FIND, (foodid,))
        fetched  = self.cur.fetchone()
        foodname = fetched[2]
        foodtype = fetched[3]
        process  = fetched[4]
        edport   = fetched[5]
        igclass  = fetched[6]
        igclassp = fetched[7]
        fuclass  = fetched[8]
        fuclassp = fetched[9]
        if lang:
            foodtype = self.find_foodtype(foodtype, lang)
            process  = self.find_process(process,   lang)
            igclass  = self.find_igclass(igclass,   lang)
            igclassp = self.find_igclass(igclassp,  lang)
            fuclass  = self.find_fuclass(fuclass,   lang)
            fuclassp = self.find_fuclass(fuclassp,  lang)
        return foodname, foodtype, process, edport, igclass, igclassp, fuclass, fuclassp
    def find_foods_component_ascending(self, eufdname,):  # PUBLIC
        self.cur.execute(self.component_value_asc_FIND, (eufdname,))
        fetches = self.cur.fetchall()
        return fetches
    def find_foods_component_descending(self, eufdname):  # PUBLIC
        self.cur.execute(self.component_value_desc_FIND, (eufdname,))
        fetches = self.cur.fetchall()
        return fetches
    # ---
    def get_component_value_from_multiple(self, values, eufdname):
        for value in values:
            if value[2] == eufdname:
                return value[3]
        return float(0)
    def find_cached_component_values(self, foodid):
        if not self.component_values or foodid != self.foodid_component_values:
            self.component_values = self.find_component_values(foodid)
            self.foodid_component_values = foodid
    def get_component_value(self, foodid, multiplier, findlist):  # PUBLIC
        returnlist = []
        self.find_cached_component_values(foodid)
        for eufdname in findlist:
            value = self.get_component_value_from_multiple(self.component_values, eufdname)
            returnlist.append(value * multiplier)
        return returnlist
    # ---
    def get_component_value_summary_totalenergy(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.summary_totalenergies)
    def get_component_value_summary_fats(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.summary_fats)
    def get_component_value_summary_carbs(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.summary_carbs)
    # ---
    def get_component_value_base(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.detailed_bases)
    def get_component_value_fibers(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.detailed_fibers)
    def get_component_value_fats(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.detailed_fats)
    def get_component_value_minerals(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.detailed_minerals)
    def get_component_value_nitrogens(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.detailed_nitrogens)
    def get_component_value_vitamins(self, foodid, multiplier):  # PUBLIC
        return self.get_component_value(foodid, multiplier, self.detailed_vitamins)

# TODO: contribfood

