#!/usr/bin/env python

from __future__ import unicode_literals

from datetime import date
from datetime import datetime
from datetime import timedelta

class DateUtils:
    isodate = '%Y-%m-%d'
    def is_supported(self, datedef):  # PUBLIC
        if datedef == 'today':
            return True
        if datedef == 'yesterday':
            return True
        if datedef == 'lastweek':
            return True
        return False
    def get_date_ranges(self, datedef):  # PUBLIC
        if datedef == 'today':
            return self.get_today_range()
        if datedef == 'yesterday':
            return self.get_yesterday_range()
        if datedef == 'lastweek':
            return self.get_lastweek_range()
        return Null, Null
    def get_today_date(self):  # PUBLIC
        now = datetime.today()
        now = now.strftime(self.isodate)
        return now
    def get_date_difference_in_months(self, startdate, enddate):   # PUBLIC
        startdate  = datetime.strptime(str(startdate), self.isodate)
        enddate    = datetime.strptime(str(enddate), self.isodate)
        difference = (enddate - startdate).days / 30
        return difference
    def get_today_range(self):
        now = self.get_today_date()
        return now, now
    def get_yesterday_range(self):
        now       = datetime.today()
        oneday    = timedelta(days=1)
        yesterday = now - oneday
        yesterday = yesterday.strftime(self.isodate)
        return yesterday, yesterday
    def get_lastweek_range(self):
        now           = datetime.today()
        dayofweek     = now.weekday()
        toendofweek   = timedelta(days=dayofweek+1)
        tostartofweek = timedelta(days=dayofweek+7)
        endofweek     = now - toendofweek
        startofweek   = now - tostartofweek
        startdate     = startofweek.strftime(self.isodate)
        enddate       = endofweek.strftime(self.isodate)
        return startdate, enddate

