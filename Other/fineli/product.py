#!/usr/bin/env python

from __future__ import unicode_literals

from enum import Enum

import os
import sys
import csv
import errno
import codecs
import sqlite3

class ProdItems(Enum):
    productid    = 1
    foodname     = 2
    manufacturer = 3
    eufdname     = 4
    amount       = 5
    unit         = 6

class Product:
    productids         = []
    conn               = None
    cur                = None
    calculator         = None
    csv_delimiter      = ';'
    cachepath          = 'cache/'
    csvpath            = 'owndata/'
    dbname             = 'products.db'
    dbname_csv_product = 'product.csv'
    dbname_csv_comps   = 'product_comps.csv'
    product_CREATE = '''CREATE TABLE IF NOT EXISTS product
                        (ID integer PRIMARY KEY, productid integer,
                                                 foodname text,
                                                 manufacturer text,
                                                 eufdname text,
                                                 amount real,
                                                 unit text)'''
    product_INSERT = '''INSERT INTO product(productid,
                                            foodname,
                                            manufacturer,
                                            eufdname,
                                            amount,
                                            unit)
                        VALUES (?, ?, ?, ?, ?, ?)'''
    product_FIND = '''SELECT * FROM product
                      WHERE productid=?'''
    def __init__(self, calculator):
        self.dbname             = "{}{}".format(self.cachepath, self.dbname)
        self.dbname_csv_product = "{}{}".format(self.csvpath,   self.dbname_csv_product)
        self.dbname_csv_comps   = "{}{}".format(self.csvpath,   self.dbname_csv_comps)
        self.calculator         = calculator
    def create_database(self):  # PUBLIC
        self.delete_database()
        print("Creating product database...")
        self.open_db_session()
        self.cur.execute(self.product_CREATE)
        products = self.read_products()
        dbname = self.dbname_csv_comps
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=str(self.csv_delimiter))
            for row in csvreader:
                productid, eufdname, amount, unit = row
                if not firstrow:
                    foodname, manufacturer = products[int(productid)]
                    foodname     = unicode(foodname)
                    manufacturer = unicode(manufacturer)
                    eufdname     = unicode(eufdname)
                    unit         = unicode(unit)
                    self.cur.execute(self.product_INSERT, (productid,
                                                           foodname,
                                                           manufacturer,
                                                           eufdname,
                                                           amount,
                                                           unit,))
                else:
                    firstrow = False
                    if productid != 'PRODUCTID' or \
                       eufdname  != 'EUFDNAME'  or \
                       amount    != 'AMOUNT'    or \
                       unit      != 'UNIT':
                           print("ERROR: Invalid header line (product comp)")
                           sys.exit(1)
        self.conn.commit()
        self.close_db_session()
    def initialize(self):  # PUBLIC
        self.open_db_session()
        del self.productids[:]
    def get_productids(self):  # PUBLIC
        return self.productids
    def find_productname(self, productid):  # PUBLIC
        self.cur.execute(self.product_FIND, (productid,))
        fetched = self.cur.fetchone()
        return fetched[2]
    def calculate_product(self, productid, amount):  # PUBLIC
        amountsplit = amount.split(' ')
        amount = float(amountsplit[0])
        unit = amountsplit[1].upper()
        multiplier = self.calculator.get_grams_for_mass(amount, unit) / 100
        self.cur.execute(self.product_FIND, (productid,))
        fetches = self.cur.fetchall()
        for fetched in fetches:
            productid = fetched[ProdItems.productid]
            eufdname  = fetched[ProdItems.eufdname]
            amount    = fetched[ProdItems.amount] * multiplier
            self.calculator.calculate_product(productid, eufdname, amount)
            self.productids.append(productid)
    def uninitialize(self):  # PUBLIC
        self.close_db_session()
    def read_products(self):
        dbname = self.dbname_csv_product
        products = {}
        with open(dbname, 'rb') as csvfile:
            firstrow = True
            csvreader = csv.reader(csvfile, delimiter=str(self.csv_delimiter))
            for row in csvreader:
                productid, foodname, manufacturer = row
                if not firstrow:
                    foodname     = foodname.lower().capitalize()
                    manufacturer = manufacturer.lower().capitalize()
                    products[int(productid)] = (foodname, manufacturer)
                else:
                    firstrow = False
                    if productid     != 'PRODUCTID' or \
                        foodname     != 'FOODNAME'  or \
                        manufacturer != 'MANUFACTURER':
                        print("ERROR: Invalid header line (product)")
                        sys.exit(1)
        return products
    def open_db_session(self):
        self.conn = sqlite3.connect(self.dbname)
        self.cur  = self.conn.cursor()
    def close_db_session(self):
        if self.conn:
            self.conn.close()
    def delete_database(self):
        print("Deleting product database...")
        try:
            os.remove(self.dbname)
        except OSError as e:
            if e.errno != errno.ENOENT:
                raise

