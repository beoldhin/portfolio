#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

from operator import itemgetter
from enum import Enum

from datetime import datetime
import sys
import yaml

class UserItems(Enum):
    agemonths = 0
    language  = 1
    name      = 2
    sex       = 3
    userid    = 4
    weight    = 5

class Users:
    users      = 0
    ymlfile    = 0
    yamlpath   = 'owndata/'
    dbname_yml = 'users.yml'
    # ---
    man        = 'M'
    female     = 'F'
    def __init__(self):
        self.dbname_yml = self.yamlpath + self.dbname_yml
    def initialize(self):  # PUBLIC
        # print "      * Users.initialize() enter"
        # print "      * Users.initialize() YML file open"
        self.ymlfile = open(self.dbname_yml, 'r')
        # print "      * Users.initialize() exit"
    def create_users(self):  # PUBLIC
        self.users = yaml.load_all(self.ymlfile)
    def get_user(self, finduserid):  # PUBLIC
        founditems = None
        for user in self.users:
            for data, items in user.items():
                if data == 'User':
                    for userdata, useritem in items.items():
                        if userdata == 'UserID' and useritem == int(finduserid):
                            founditems = items
                            break
        userlist = []
        if founditems:
            for userdata,useritem in founditems.items():
                if userdata == 'Name':
                    userlist.append((userdata, useritem))
                elif userdata == 'Birthday':
                    try:
                        birthday   = datetime.strptime(str(useritem), '%Y-%m-%d')
                        current    = datetime.today()
                        difference = (current - birthday).days / 30
                        userlist.append(('AgeMonths', int(difference)))
                    except:
                        print("ERROR: Invalid date format (user)")
                        sys.exit(1)
                elif userdata == 'Language':
                    userlist.append((userdata, useritem))
                elif userdata == 'Sex':
                    sex = useritem.split('-')[0]
                    if sex != self.man and sex != self.female:
                        print("ERROR: Invalid sex (user)")
                        sys.exit(1)
                    userlist.append((userdata, useritem))
                elif userdata == 'UserID':
                    userlist.append((userdata, useritem))
                elif userdata == 'Weight':
                    userlist.append((userdata, float(useritem)))
            userlist.sort(key=itemgetter(0))
        return userlist
    def uninitialize(self):  # PUBLIC
        # print "      * Users.uninitialize() YML file close"
        self.ymlfile.close()

