#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

from datetime import datetime
from manager import *

import yaml
import sys

# TODO: Find duplicate days and create error

class Diary:
    diary      = 0
    manager    = 0
    ymlfile    = 0
    startdate  = 0
    enddate    = 0
    outputfile = None
    dbname_yml = 'diary_{}.yml'
    yamlpath   = 'owndata/diaries/'
    def __init__(self, manager, outputfile):
        self.manager    = manager
        self.outputfile = outputfile
    def initialize(self, user, startdate, enddate):  # PUBLIC
        self.dbname_yml = self.create_ymlfilename(user)
        self.ymlfile = open(self.dbname_yml, 'r')
        self.startdate = self.convert_time(startdate)
        self.enddate = self.convert_time(enddate)
        if self.startdate > self.enddate:
            print('ERROR: Start date is later than end date')
            sys.exit(1)
        self.manager.initialize(user, self.outputfile)
        self.manager.calculate_initialize()
    def create_diary(self):  # PUBLIC
        self.diary = yaml.load_all(self.ymlfile)
    def calculate_diary(self):  # PUBLIC
        numofdays = 0
        for item in self.diary:
            for data, items in item.items():
                if data == 'Diary':
                    foundentry = False
                    for userdata, useritem in items.items():
                        if userdata == 'Date':
                            dateinfo = datetime.combine(useritem, datetime.min.time())
                            if dateinfo >= self.startdate and dateinfo <= self.enddate:
                                numofdays += 1
                                foundentry = True
                        if userdata == 'Date':
                            print("Found diary date {}...".format(useritem), file=self.outputfile)
                        if userdata == 'Foods' and foundentry == True:
                            self.calculate_foods(useritem)
                            print('', file=self.outputfile)
        print("Found total {} days.".format(numofdays), file=self.outputfile)
        self.manager.calculate_result(numofdays)
    def uninitialize(self):  # PUBLIC
        self.ymlfile.close()
        self.manager.uninitialize()
    def convert_time(self, datestr):
        date = None
        try:
            date = datetime.strptime(datestr, '%Y-%m-%d')
        except:
            print('ERROR: Invalid date format (diary)')
            sys.exit(1)
        return date
    def create_ymlfilename(self, user):
        user = user.lower().replace(' ', '_')
        dbname_yml = self.dbname_yml.format(user)
        dbname_yml = self.yamlpath + dbname_yml
        return dbname_yml
    def calculate_foods(self, foods):
        for food in foods:
            splitfood = food.split(':')
            if splitfood[0] == 'Recipe' and len(splitfood) != 3:
                print('ERROR: Invalid number of recipe food entries (diary)')
                sys.exit(1)
            if splitfood[0] == 'Product' and len(splitfood) != 3:
                print('ERROR: Invalid number of product food entries (diary)')
                sys.exit(1)
            if splitfood[0] == 'Raw' and len(splitfood) != 3:
                print('ERROR: Invalid number of raw food entries (diary)')
                sys.exit(1)
            foodsource = splitfood[0]
            fooditem,amount = splitfood[1:3]
            if foodsource == 'Recipe':
                self.manager.calculate_recipe(fooditem, amount)
            elif foodsource == 'Product':
                self.manager.calculate_product(fooditem, amount)
            elif foodsource == 'Raw':
                self.manager.calculate_raw(fooditem, amount)
            else:
                print('ERROR: Unknown food type (diary)')
                sys.exit(1)

