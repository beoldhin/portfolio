#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

import yaml
import sys

class Recipe:
    foodids    = []
    finelidb   = None
    recipe     = None
    ymlfile    = None
    calculator = None
    outputfile = None
    yamlpath   = 'owndata/recipes/'
    def __init__(self, finelidb, calculator):
        self.finelidb   = finelidb
        self.calculator = calculator
    def initialize(self, recipename):  # PUBLIC
        recipename   = recipename.replace(' ', '_')
        dbname_yml   = self.yamlpath + recipename + '.yml'
        self.ymlfile = open(dbname_yml, 'r')
        del self.foodids[:]
    def set_outputfile(self, outputfile):  # PUBLIC
        self.outputfile = outputfile
    def create_recipe(self):  # PUBLIC
        self.recipe = yaml.load_all(self.ymlfile)
    def calculate_recipe(self, percentage, language):  # PUBLIC
        percentage = float(percentage) / 100
        for item in self.recipe:
            for data, items in item.items():
                if data == 'ingredients':
                    self.calculate_foods(items, percentage, language)
    def get_foodids(self):  # PUBLIC
        return self.foodids
    def uninitialize(self):  # PUBLIC
        if self.ymlfile:
            self.ymlfile.close()
    def calculate_foods(self, items, percentage, language):
        totalenergies = []
        for item in items:
            if item[0] != '(' or item[-1] != ')':
                print("    Skipped item: {}".format(item), file=self.outputfile)
                continue
            index = item.find(')')
            amount = item[1:index]
            index = item.rfind('(')
            foodid = item[index+1:-1]
            foodname = self.finelidb.find_foodname(foodid, language).lower().capitalize()
            print("    Calculating item: {:>5} / {}, {}...".format(foodid, foodname, amount), file=self.outputfile)
            multiplier = (self.calculator.get_grams_for_volume_or_mass(foodid, amount) / 100) * percentage
            self.calculator.calculate_components(foodid, multiplier)
            self.foodids.append(int(foodid))

