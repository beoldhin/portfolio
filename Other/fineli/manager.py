#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

from operator import itemgetter

from finelidb import *
from complistdb import *
from calculator import *
from recipe import *
from product import *
from raw import *
from limits import *

class Manager:
    finelidb   = None
    complistdb = None
    calculated = None
    calculator = None
    recipe     = None
    product    = None
    raw        = None
    limits     = None
    userlimits = None
    outputfile = None
    # ---
    linelength  = 99
    recommended = 'Recommended'
    thickline   = '=' * 99
    line        = '-' * 99
    def __init__(self):
        self.finelidb   = FineliDB()
        self.complistdb = CompListDB(self.finelidb)
        self.calculator = Calculator(self.finelidb)
        self.recipe     = Recipe(self.finelidb, self.calculator)
        self.product    = Product(self.calculator)
        self.raw        = Raw(self.calculator)
        self.limits     = Limits()
    def initialize(self, userid, outputfile):  # PUBLIC
        self.finelidb.open_db_session()
        self.limits.initialize()
        self.limits.create_limits(userid)
        self.userlimits = self.limits.get_limits()
        self.outputfile = outputfile
        self.recipe.set_outputfile(outputfile)
        self.product.initialize()
    def create_database_maindb(self):  # PUBLIC
        self.finelidb.delete_database()
        self.finelidb.create_database()
    def create_database_productdb(self):  # PUBLIC
        self.product.create_database()
    def create_database_listdb(self):  # PUBLIC
        self.finelidb.open_db_session()
        self.complistdb.create_databases()
        self.finelidb.close_db_session()
    def create_database_compvaldb(self):  # PUBLIC
        self.finelidb.open_db_session()
        self.calculator.create_databases()
        self.finelidb.close_db_session()
    def get_complist(self, compname, language):  # PUBLIC
        compname = compname.upper()
        language = language.upper()
        self.finelidb.open_db_session()
        eufdname = self.finelidb.find_eufdname(compname,language).capitalize()
        compunit = self.finelidb.find_component(compname)[2].lower()
        self.print_complistitem('#', eufdname, compunit)
        print(self.line, file=self.outputfile)
        fetches = self.complistdb.get_list(compname, language)
        for i in range(len(fetches)):
            sequence = "{}.".format(i + 1)
            foodname = fetches[i][2]
            foodname = self.limit_string(foodname, 60)
            amount   = round(fetches[i][3], 1)
            self.print_complistitem(sequence, foodname, amount)
        self.finelidb.close_db_session()
    def get_complist2(self, compname, language):  # PUBLIC
        compname = compname.upper()
        language = language.upper()
        eufdname = self.finelidb.find_eufdname(compname,language).capitalize()
        print("{:>5} {:<57} {:>10} {:>10}".format('#', eufdname, 'Target', 'Highest'))
        print('-' * 85)
        target, highest = self.userlimits[compname][1:]
        fetches = self.complistdb.get_list(compname, language)
        results = []
        compunit = self.finelidb.find_component(compname)[2]
        for fetched in fetches:
            idx, foodname, amount = fetched[1:4]
            gramvalue = self.calculator.get_grams_for_massunit(amount, compunit) / 100
            if gramvalue > float(0):
                result1 = target  / gramvalue
                result2 = highest / gramvalue
                results.append((foodname, result1, result2))
        for i in range(len(results)):
            sequence = "{}.".format(i + 1)
            foodname, target, highest = results[i]
            foodname = self.limit_string(foodname, 57)
            target, unit  = self.calculator.get_in_above_grams(target)
            target = "{} {}".format(target, unit)
            if highest > float(0):
                highest, unit = self.calculator.get_in_above_grams(highest)
                highest = "{} {}".format(highest, unit)
            else:
                highest = '-'
            print("{:>5} {:<57} {:>10} {:>10}".format(sequence, foodname, target, highest))
    def get_nearestlist(self, language, compdefs):  # PUBLIC
        language = language.upper()
        complist = []
        for compdef in compdefs:
            compname, amount = compdef.split('=')
            compname = compname.upper()
            amount   = float(amount)
            complist.append((compname, amount))
        self.calculator.get_nearestlist(language, complist)
    def calculate_initialize(self):  # PUBLIC
        self.calculator.initialize()
        self.calculator.open_db_session('EN')
    def calculate_recipe(self, fooditem, amount):  # PUBLIC
        recipename = fooditem.lower()
        percentage = int(amount)
        language   = self.limits.get_language()
        print('', file=self.outputfile)
        print("Calculating Recipe: {}, {}%...".format(recipename.capitalize(), percentage), file=self.outputfile)
        self.recipe.initialize(recipename)
        self.recipe.create_recipe()
        self.recipe.calculate_recipe(percentage, language)
        self.recipe.uninitialize()
    def calculate_product(self, fooditem, amount):  # PUBLIC
        productid = fooditem
        productname = self.product.find_productname(productid).lower().capitalize()
        print("Calculating Product: {:>6} / {}, {}...".format(productid, productname, amount), file=self.outputfile)
        self.product.calculate_product(productid, amount)
    def calculate_raw(self, fooditem, amount):  # PUBLIC
        foodid = fooditem
        language = self.limits.get_language()
        foodname = self.finelidb.find_foodname(foodid,language).lower().capitalize()
        print("Calculating Raw: {:>10} / {}, {}...".format(foodid, foodname, amount), file=self.outputfile)
        self.raw.calculate_raw(foodid, amount)
    def calculate_result(self, numofdays):  # PUBLIC
        if numofdays == 0:
            return
        print('', file=self.outputfile)
        print('', file=self.outputfile)
        decimals = 2
        language = self.limits.get_language()
        print("Summary of daily average:", file=self.outputfile)
        print(self.thickline, file=self.outputfile)
        print('', file=self.outputfile)
        self.calculator.create_perday(numofdays)
        self.print_total_energies(language, decimals, numofdays)
        self.print_total_fats(    language, decimals, numofdays)
        self.print_total_carbs(   language, decimals, numofdays)
        self.print_per_day(       language, decimals, numofdays)
        self.print_total_comps(   language, decimals, numofdays)
    def uninitialize(self):  # PUBLIC
        self.raw.uninitialize()
        self.product.uninitialize()
        self.recipe.uninitialize()
        self.limits.uninitialize()
        self.calculator.close_db_session()
        self.finelidb.close_db_session()
    def limit_string(self, strtolimit, limit):
        cutlength = limit - len('...')
        if len(strtolimit) > cutlength:
            strtolimit = strtolimit[:cutlength] + '...'
        return strtolimit
    def print_complistitem(self, sequence, foodname, amount):
        print("{:>5} {:<60} {:>8}".format(sequence, foodname, amount), file=self.outputfile)
    def print_complist(self, eufdname, language):
        foodids = self.recipe.get_foodids()
        foodids.extend(self.raw.get_foodids())
        if len(foodids) > 0:
            complist = self.complistdb.get_list_positions(foodids, eufdname, language)
            complist.sort()
            return complist
    def print_total_energies(self, language, decimals, numofdays):
        eufdname    = self.finelidb.detailed_bases[Bases.enerc]
        energytotal = self.calculator.get_value_perday(self.finelidb.detailed_bases[Bases.enerc])
        if energytotal > float(0):
            self.print_energy(energytotal, eufdname, energytotal, language, decimals, self.recommended)
            print(self.line, file=self.outputfile)
            for i in range(len(self.finelidb.summary_totalenergies)):
                eufdname   = self.finelidb.summary_totalenergies[i]
                multiplier = self.finelidb.multiplier_totalenergies[i]
                value      = self.calculator.get_value_perday(self.finelidb.summary_totalenergies[i]) * multiplier
                if i == Totalenergies.fat:
                    recommended = '25-40%'
                elif i == Totalenergies.prot:
                    recommended = '10-20%'
                elif i == Totalenergies.choavl:
                    recommended = '45-60%'
                elif i == Totalenergies.fibc:
                    recommended = '200-280 kJ'
                else:
                    recommended = '-'
                self.print_energy(value, eufdname, energytotal, language, decimals, recommended)
            print('', file=self.outputfile)
    def print_total_fats(self, language, decimals, numofdays):
        eufdname    = self.finelidb.summary_totalenergies[Totalenergies.fat]
        multiplier  = self.finelidb.multiplier_totalenergies[Totalenergies.fat]
        energytotal = self.calculator.get_value_perday(self.finelidb.detailed_bases[Bases.enerc])
        fattotal    = self.calculator.get_value_perday(self.finelidb.summary_totalenergies[Totalenergies.fat]) * multiplier
        if fattotal > float(0):
            self.print_energy(fattotal, eufdname, fattotal, language, decimals, self.recommended)
            print(self.line, file=self.outputfile)
            for i in range(len(self.finelidb.summary_fats)):
                eufdname = self.finelidb.summary_fats[i]
                value    = self.calculator.get_value_perday(self.finelidb.summary_fats[i]) * multiplier
                if i == Fats.fasat:
                    recommended = '< 10%'
                elif i == Fats.fatrn:
                    recommended = '0%'
                elif i == Fats.famcis:
                    recommended = '10-20%'
                elif i == Fats.fapu:
                    recommended = '5-10%'
                else:
                    recommended = '-'
                self.print_energy(value, eufdname, energytotal, language, decimals, recommended)
            print('', file=self.outputfile)
    def print_total_carbs(self, language, decimals, numofdays):
        eufdname    =    self.finelidb.summary_totalenergies[Totalenergies.choavl]
        multiplier  = self.finelidb.multiplier_totalenergies[Totalenergies.choavl]
        energytotal = self.calculator.get_value_perday(self.finelidb.detailed_bases[Bases.enerc])
        carbstotal  = self.calculator.get_value_perday(self.finelidb.summary_totalenergies[Totalenergies.choavl]) * multiplier
        if carbstotal > float(0):
            self.print_energy(carbstotal, eufdname, carbstotal, language, decimals, self.recommended)
            print(self.line, file=self.outputfile)
            for i in range(len(self.finelidb.summary_carbs)):
                eufdname = self.finelidb.summary_carbs[i]
                value    = self.calculator.get_value_perday(self.finelidb.summary_carbs[i]) * multiplier
                self.print_energy(value, eufdname, energytotal, language, decimals, '-')
            print('', file=self.outputfile)
            print('', file=self.outputfile)
    def print_per_day(self, language, decimals, numofdays):
        if numofdays == 0:
            return
        print("Limits per day and needed changes:", file=self.outputfile)
        print(self.thickline, file=self.outputfile)
        print('', file=self.outputfile)
        sequence    = '#'
        eufdname    = 'Component'
        highest     = 'Highest'
        recommended = 'Target'
        completed   = 'Done'
        percentage  = '%'
        print("{:>4} {:<48} {:>10} {:>12} {:>10} {:>10}".format(sequence, eufdname, highest, recommended, completed, percentage), file=self.outputfile)
        print(self.line, file=self.outputfile)
        language  = self.limits.get_language()
        sortlist  = []
        totalpct  = float(0)
        totalppct = float(0)
        totalmpct = float(0)
        i = 0
        for key, value in self.userlimits.items():
            eufdshortname = key
            unit, recommended, highest = value
            completed  = self.calculator.get_value_perday(eufdshortname)
            eufdname   = self.finelidb.find_eufdname(eufdshortname, language).capitalize()
            donegrams  = self.calculator.get_massunit_mass_for_grams(completed, unit)
            unit       = unit.lower()
            sequence   = "{}.".format(i + 1)
            higheststr = ''
            if highest == float(0):
                higheststr = '-'
            else:
                highestmass = self.calculator.get_massunit_mass_for_grams(highest, unit)
                higheststr  = "{} {}".format(int(round(highestmass)), unit)
            targetmass     = self.calculator.get_massunit_mass_for_grams(recommended, unit)
            recommendedstr = "{} {}".format(int(round(targetmass)), unit)
            completedstr   = "{} {}".format(int(round(donegrams)), unit)
            if completed > float(0):
                percentage = float(0)
                changesign = '-'
                if completed < recommended:
                    percentage = recommended / completed
                    changesign = '+'
                    totalppct += percentage
                elif completed > recommended:
                    percentage = completed / recommended
                    changesign = '-'
                    totalmpct += percentage
                percentage *= 100
                totalpct   += percentage
                sortlist.append((eufdname, higheststr, recommendedstr, completedstr, percentage, changesign))
            else:
                percentage = sys.float_info.max
                sortlist.append((eufdname, higheststr, recommendedstr, completedstr, percentage, '?'))
        sortlist.sort(key=itemgetter(4), reverse=True)
        for i in range(len(sortlist)):
            sequence = str(i + 1) + '.'
            eufdname, highest, recommended, completed, percentage, changesign = sortlist[i]
            eufdname = self.limit_string(eufdname, 48)
            if percentage == sys.float_info.max:
                percentage = '-'
            else:
                percentage = "{}{}%".format(changesign, int(round(percentage)))
            print("{:>4} {:<48} {:>10} {:>12} {:>10} {:>10}".format(sequence, eufdname, highest, recommended, completed, percentage), file=self.outputfile)
        print(self.line, file=self.outputfile)
        totalppct = "+{}%".format(int(round(totalppct*100)))
        totalmpct = "-{}%".format(int(round(totalmpct*100)))
        totalpct  =  "{}%".format(int(round(totalpct)))
        print("{:>4} {:<48} {:>10} {:>12} {:>10} {:>10}".format('', '', '', totalppct, totalmpct, totalpct), file=self.outputfile)
        print('', file=self.outputfile)
        print('', file=self.outputfile)
    def print_total_comps(self, language, decimals, numofdays):
        print("Total consumption in weight by foods, categorized by consumption limited components:", file=self.outputfile)
        print(self.thickline, file=self.outputfile)
        print('', file=self.outputfile)
        amountlist = []
        foodids    = self.calculator.get_comp_foodids()
        productids = self.calculator.get_prodcomp_productids()
        for key, value in self.userlimits.items():
            sortedfoodids = []
            eufdshortname = key
            eufdname = self.finelidb.find_eufdname(eufdshortname,language).capitalize()
            print("{:>5} {:<69} {:>8} {:>14}".format('#', eufdname, '%', 'Pos'), file=self.outputfile)
            print(self.line, file=self.outputfile)
            total = self.calculator.get_value_total(eufdshortname)
            if total > float(0):
                for foodid in foodids:
                    value = self.calculator.get_value_comp(foodid, eufdshortname)
                    amountlist.append((int(foodid), value, True))
                for productid in productids:
                    value = self.calculator.get_value_prodcomp(productid, eufdshortname)
                    amountlist.append((int(productid), value, False))
                amountlist.sort(key=itemgetter(1), reverse=True)
                for item in amountlist:
                    foodid = item[0]
                    sortedfoodids.append(foodid)
                complistpos = self.complistdb.get_list_positions(sortedfoodids, eufdshortname, language)
                del sortedfoodids[:]
                for i in range(len(amountlist)):
                    thisid, amount, foodidcase = amountlist[i]
                    sequence      = "{}.".format(i + 1)
                    percentage    = (amount / total) * 100
                    percentagestr = "{}%".format(int(round((percentage))))
                    listpos  = '-'
                    foodname = ''
                    if foodidcase == True:
                        foodname = self.finelidb.find_foodname(thisid,language).lower().capitalize()
                        foodname = self.limit_string(foodname, 69)
                        listpos = str(complistpos[i])
                        if listpos == '-1':
                            listpos = '-'
                        else:
                            listpos += '.'
                    else:
                        foodname  = self.product.find_productname(thisid).lower().capitalize()
                        foodname  = "{} {}".format('(*)', foodname)
                        foodname  = self.limit_string(foodname, 69)
                        listpos = '-'
                        if percentage == float(0):
                            percentagestr = '-'
                    print("{:>5} {:<69} {:>8} {:>14}".format(sequence, foodname, percentagestr, listpos), file=self.outputfile)
                print('', file=self.outputfile)
                del amountlist[:]
            else:
                print("Zero consumption.", file=self.outputfile)
                print('', file=self.outputfile)
    def print_energy(self, value, eufdname, total, language, decimals, recommended):
        Jvalue,Jvaluestr     = self.calculator.get_in_joules(value)
        kcalvalue            = self.calculator.convert_kJ_to_kcal(value)
        calvalue,calvaluestr = self.calculator.get_in_cals(kcalvalue)
        txtelement           = self.finelidb.find_eufdname(eufdname, language).capitalize() + ":"
        energyelement1       = str(Jvalue) + " " + Jvaluestr
        energyelement2       = str(calvalue) + " " + calvaluestr
        totalelement         = str(int(round((value / total) * 100))) + "%"
        print("{:<46} {:>10} {:>15} {:>9} {:>15}".format(txtelement, energyelement1, energyelement2, totalelement, recommended), file=self.outputfile)

