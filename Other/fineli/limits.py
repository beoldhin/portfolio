#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

from operator import itemgetter

from users import *

import yaml

# TODO: In exception case try to find the lowest number also from the non-exception (age) list

class Limits:
    user       = 0
    limits     = 0
    ymlfile    = 0
    yamlpath   = 'owndata/'
    dbname_yml = 'limits.yml'
    # ---
    man        = 'M'
    female     = 'F'
    def __init__(self):
        self.users      = Users()
        self.dbname_yml = self.yamlpath + self.dbname_yml
    def generate_limit(self, eufdname, items, unit):  # PUBLIC
        age = self.user[UserItems.agemonths][1]
        sex = self.user[UserItems.sex][1].split('-')
        founditems = None
        if items:
            childlimit = 12 * 10  # 10 years
            for limitdata, limititem in items.items():
                if age < childlimit:
                    if limitdata == 'Children':
                        founditems = limititem
                        break
                else:
                    if sex[0] == self.man and limitdata == 'Men':
                        founditems = limititem
                        break
                    elif sex[0] == self.female and limitdata == 'Women':
                        founditems = limititem
                        break
        limit = 0
        highest = 0
        limitlist = []
        if founditems:
            limit = 0
            limit, highest = self.find_exception(founditems, sex)
            if limit == 0:
                founditems = self.remove_exceptions(founditems)
                founditems = self.sort_limits(founditems)
                for i in range(len(founditems)):
                    limit = founditems[i][0]
                    if limit >= age:
                        limit   = founditems[i-1][1]
                        highest = founditems[i-1][2]
                        break
            limitlist.append(eufdname)
            limitlist.append(unit)
            gramvalue = self.convert_mass(float(limit), unit)
            limitlist.append(gramvalue)
            gramvalue = self.convert_mass(float(highest), unit)
            limitlist.append(gramvalue)
        return limitlist
    def initialize(self):  # PUBLIC
        self.ymlfile = open(self.dbname_yml, 'r')
    def create_limits(self, userid):  # PUBLIC
        self.set_user(userid)
        self.limits = yaml.load_all(self.ymlfile)
    def get_language(self):  # PUBLIC
        return self.user[UserItems.language][1]
    def get_limit(self, eufdname):  # PUBLIC
        for limit in self.limits:
            for data, items in limit.items():
                data = data.split('-')
                if data[0] == 'Limit' and data[1] == eufdname:
                    unit = data[2]
                    result = self.generate_limit(eufdname, items, unit)
                    return result
    def get_limits(self):  # PUBLIC
        limitlist = {}
        for limit in self.limits:
            for data, items in limit.items():
                data = data.split('-')
                if data[0] == 'Limit':
                    eufdname = data[1]
                    unit = data[2]
                    result = self.generate_limit(eufdname, items, unit)
                    limitlist[result[0]] = tuple(result[1:])
        return limitlist
    def uninitialize(self):  # PUBLIC
        self.ymlfile.close()
    def set_user(self, userid):
        self.users.initialize()
        self.users.create_users()
        self.user = self.users.get_user(userid)
        self.users.uninitialize()
    def convert_mass(self, value, unit):
        if unit == 'KG':
            return value * 1000
        elif unit == 'MG':
            return value / 1000
        elif unit == 'NG':
            return value / 1000000000
        elif unit == 'UG':
            return value / 1000000
        else:
            print("ERROR: Unknown unit (limit)")
            sys.exit(1)
        return value
    def find_exception(self, items, usersex):
        limit     = 0
        highest   = 0
        if len(usersex) > 1:
            userexcepts = usersex[1:]
            exceptions = []
            for item in items:
                item = item.split(':')
                splititem = item[0].split('-')
                if splititem[0] == 'Except':
                    excepttype = splititem[1]
                    limit      = splititem[2]
                    highest    = item[1].split('-')[1]
                    exceptions.append((excepttype, limit, highest))
            lowestlimit = 0
            lowestlimithighest = 0
            for userexcept in userexcepts:
                for exception in exceptions:
                    excepttype, limit, highest = exception
                    if userexcept == excepttype:
                        if lowestlimit == 0:
                            lowestlimit        = limit
                            lowestlimithighest = highest
                        elif limit < lowestlimit:
                            lowestlimit        = limit
                            lowestlimithighest = highest
            if lowestlimit > 0:
                limit   = lowestlimit
                highest = lowestlimithighest
        return limit, highest
    def remove_exceptions(self, items):
        while True:
            found = False
            for item in items:
                splititemx = item.split(':')
                splititem = splititemx[0].split('-')
                if splititem[0] == 'Except':
                    items.remove(item)
                    found = True
                    break
            if not found:
                break
        return items
    def sort_limits(self, items):
        sortlist = []
        for item in items:
            item = item.split(':')
            splititem = item[0].split('-')
            if splititem[0] != 'Below':
                print("ERROR: Unknown limit definition (limit)")
                sys.exit(1)
            highest = int(item[1].split('-')[1])
            limit   = int(splititem[2])
            time    = int(splititem[1][:-1])
            timedef = splititem[1][-1:]
            if timedef == 'm':
               sortlist.append((time, limit, highest))
            elif timedef == 'y':
                sortlist.append((time*12, limit, highest))
            else:
                print("ERROR: Unknown time definition (limit)")
                sys.exit(1)
        sortlist.sort(key=itemgetter(0))
        return sortlist

