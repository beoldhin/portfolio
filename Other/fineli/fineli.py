#!/usr/bin/env python

from __future__ import unicode_literals
from __future__ import print_function

from manager import *
from dateutils import *
from diary import *

import codecs
import sys

outputfile = None

def print_usage_string():
    prgname = sys.argv[0]
    print("Usage: {} <userid> <startdate> <enddate>".format(prgname))
    print("Usage: {} <userid> today|yesterday|lastweek|lastmonth".format(prgname))
    print("Usage: {} getcomplist <componentname> <language>".format(prgname))
    print("Usage: {} getcomplist2 <userid> <componentname> <language>".format(prgname))
    print("Usage: {} getnearestlist <language> (<component=amount>)".format(prgname))
    print("Usage: {} createmaindb".format(prgname))
    print("Usage: {} createproductdb".format(prgname))
    print("Usage: {} createcompvaldb".format(prgname))
    print("Usage: {} createlistdb".format(prgname))
    sys.exit(1)

def create_outputfile(userid, startdate, enddate, todaydate):
    global outputfile
    filename = "output/fineli_{0}_{2}_{3}_{1}.txt".format(userid, todaydate, startdate, enddate)
    outputfile = codecs.open(filename, 'w', encoding='utf8')

def close_outputfile():
    global outputfile
    outputfile.close()

def calculate_diary(userid, startdate, enddate, todaydate):
    global outputfile
    create_outputfile(userid, startdate, enddate, todaydate)
    print("User ID    = {}".format(userid), file=outputfile)
    print("Start date = {}".format(startdate), file=outputfile)
    print("End date   = {}".format(enddate), file=outputfile)
    print("Today date = {}".format(todaydate), file=outputfile)
    print('', file=outputfile)
    diary = Diary(manager, outputfile)
    diary.initialize(userid, startdate, enddate)
    diary.create_diary()
    diary.calculate_diary()
    diary.uninitialize()
    close_outputfile()


manager   = Manager()
dateutils = DateUtils()


firstargument  = None
secondargument = None
arguments = sys.argv[1:]
if len(arguments) >= 1:
    firstargument = arguments[0]
if len(arguments) >= 2:
    secondargument = arguments[1]
if len(arguments) == 1 and firstargument == 'createmaindb':
    manager.create_database_maindb()
elif len(arguments) == 1 and firstargument == 'createproductdb':
    manager.create_database_productdb()
elif len(arguments) == 1 and firstargument == 'createcompvaldb':
    manager.create_database_compvaldb()
elif len(arguments) == 1 and firstargument == 'createlistdb':
    manager.create_database_listdb()
elif len(arguments) == 3 and firstargument == 'getcomplist':
    compname, language = arguments[1:3]
    manager.get_complist(compname, language)
elif len(arguments) == 4 and firstargument == 'getcomplist2':
    userid, compname, language = arguments[1:4]
    manager.initialize(userid, outputfile)
    manager.get_complist2(compname, language)
    manager.uninitialize()
elif len(arguments) >= 3 and firstargument =='getnearestlist':
    language = secondargument
    compdefs = arguments[2:]
    manager.get_nearestlist(language, compdefs)
elif len(arguments) == 2 and dateutils.is_supported(secondargument) == True:
    userid, datedef = arguments
    startdate, enddate = dateutils.get_date_ranges(datedef)
    todaydate = dateutils.get_today_date()
    calculate_diary(userid, startdate, enddate, todaydate)
elif len(arguments) == 3:
    userid, startdate, enddate = arguments
    todaydate = dateutils.get_today_date()
    calculate_diary(userid, startdate, enddate, todaydate)
else:
    print_usage_string()

