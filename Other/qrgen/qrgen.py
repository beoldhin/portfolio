#!/usr/bin/env python

# Version 1.3

# UN Website URL
# UN YouTube Video
# S? Image File
# S? PDF File
# U? Google Maps Location
# U Twitter
# U Facebook
# ? LinkedIn
# U Instagram
# ? FourSquare
# U App Store Download
# ? iTunes Link
# U Dropbox
# P Plain Text
# P+ Telephone Number
# U Skype Call
# + SMS Message
# + Email Address
# + Email Message
#   Contact Details
# ? Digital Business Card
# S Attendance Tracking
#   Event (VCALENDAR)
# + Wifi Login
# U Paypal Buy Now Link
# + Bitcoin

# Mnemonics:
# P = Plaintext
# S = Subscription required
# U = URL (plaintext)
# D = Direct link, no special processing
# N = No need to implement
# ? = Don't know how to implement
# + = Implemented

import sys
import shlex
import urllib.parse
import subprocess

UseOptimizedEscaper = True
ErrorCorrection = 'L'

class QrGenerator():
    def __init__(self):
        self.requireProgram('qrencode')
        self.requireProgram('zbarimg')
    def createUrl(self, baseUrl, args):
        url = baseUrl
        params = urllib.parse.urlencode(args)
        if params:
            url += '?{}'.format(params)
        return url
    def createCode(self, outFile, qrStr):
        qrStr = shlex.quote(qrStr)
        cmd = 'qrencode --type=PNG --level={} --output={} {}'.format(ErrorCorrection, outFile, qrStr)
        retText = self.runCommand(cmd, False)
        if retText:  # Expect empty string if OK
            return False
        print('QR code created. Verifying code...')
        cmd = 'zbarimg --quiet {}'.format(outFile)
        retText = self.runCommand(cmd, True)
        # Next remove leading 'QR-Code:' string from zbarimg
        # output and add quotes around the text for comparison
        retText = retText.split(':',1)[1].rstrip()
        retText = shlex.quote(retText)
        if retText == qrStr:
            print('QR code created successfully.')
        else:
            print('QR code verification failed.')
    def runCommand(self, command, silent):
        if silent == False:
            print('Running command: {}'.format(command))
        retText = subprocess.check_output(command, shell=True).decode('UTF-8')
        return retText
    def printErrorText(self, errorText):
        print(errorText)
        sys.exit(1)
    def requireProgram(self, program):
        command = ''
        try:
            command = 'command -v {}'.format(program)
            retText = self.runCommand(command, True)
        except Exception:
            self.printErrorText('ERROR: Required program missing: {}'.format(program))
    def escapeStr(self, origStr, separators):
        # The idea is to escape only those needed to recognize the next separator in
        # practice for each specific decoder case. This will minimize the amount of
        # required output bits of the product. If, however, this does not work (see
        # ZXing to always excape all four characters), use the less specific encoding.
        # See also:
        # https://github.com/zxing/zxing/wiki/Barcode-Contents#wifi-network-config-android
        # https://en.wikipedia.org/wiki/MeCard_(QR_code)
        newStr = origStr
        if UseOptimizedEscaper == True:
            separators.append('\\')  # Minimum set needed by apps in practice (Android "QR Code Reader" app etc.)
            for ch in separators:
                newStr = newStr.replace(ch, '\\' + ch)
            return newStr
        # Use less optimized version:
        for ch in [ '\\', ';', ',', ':' ]:
            newStr = newStr.replace(ch, '\\' + ch)
        return newStr

class QrBitcoin:
    qrGen = None
    def __init__(self):
        self.qrGen = QrGenerator()
    def generate(self, address, amount, label, message):
        outFile = 'bitcoin.png'
        urlStr = 'bitcoin:{}'
        args = {}
        if amount:
            args['amount'] = amount
        if label:
            args['label'] = label
        if message:
            args['message'] = message
        qrStr = self.qrGen.createUrl(urlStr.format(address), args)
        self.qrGen.createCode(outFile, qrStr)

class QrEmailAddress:
    qrGen = None
    def __init__(self):
        self.qrGen = QrGenerator()
    def generate(self, emailAddress):
        outFile = 'emailaddr.png'
        formatStr = 'MAILTO:{}'
        mailtoEndsWith = []
        emailAddress = self.qrGen.escapeStr(emailAddress, mailtoEndsWith)
        qrStr = formatStr.format(emailAddress)
        self.qrGen.createCode(outFile, qrStr)

class QrEmailMessage:
    qrGen = None
    def __init__(self):
        self.qrGen = QrGenerator()
    def generate(self, emailAddress, subject, body):
        outFile = 'emailmessage.png'
        formatStr = 'MATMSG:TO:{};SUB:{};BODY:{};;'
        toEndsWith = [ ';' ]
        subEndsWith = [ ';' ]
        bodyEndsWith = [ ';' ]
        emailAddress = self.qrGen.escapeStr(emailAddress, toEndsWith)
        subject = self.qrGen.escapeStr(subject, subEndsWith)
        body = self.qrGen.escapeStr(body, bodyEndsWith)
        qrStr = formatStr.format(emailAddress, subject, body)
        self.qrGen.createCode(outFile, qrStr)

class QrPaypal:
    qrGen = None
    def __init__(self):
        self.qrGen = QrGenerator()
    def generate(self, emailAddress, itemDesc, prodCode, itemValue, currency):
        outFile = 'paypal.png'
        urlStr = 'https://www.paypal.com/cgi-bin/webscr'
        args = {}
        args['cmd'] = '_xclick'
        args['business'] = emailAddress
        args['item_name'] = itemDesc
        if prodCode:
            args['item_number'] = prodCode
        args['amount'] = itemValue
        args['currency_code'] = currency
        qrStr = self.qrGen.createUrl(urlStr, args)
        self.qrGen.createCode(outFile, qrStr)

class QrPlainText:
    qrGen = None
    def __init__(self):
        self.qrGen = QrGenerator()
    def generate(self, plainText):
        outFile = 'plaintext.png'
        formatStr = '{}'
        ptEndsWith = []
        plainText = self.qrGen.escapeStr(plainText, ptEndsWith)
        qrStr = formatStr.format(plainText)
        self.qrGen.createCode(outFile, qrStr)

class QrSms:
    qrGen = None
    def __init__(self):
        self.qrGen = QrGenerator()
    def generate(self, phoneNumber, message):
        outFile = 'sms.png'
        formatStr = 'SMSTO:{}:{}'
        phoneNumberEndsWith = [ ':' ]
        messageEndsWith = []
        phoneNumber = self.qrGen.escapeStr(phoneNumber, phoneNumberEndsWith)
        message = self.qrGen.escapeStr(message, messageEndsWith)
        qrStr = formatStr.format(phoneNumber, message)
        self.qrGen.createCode(outFile, qrStr)

class QrTel:
    qrGen = None
    def __init__(self):
        self.qrGen = QrGenerator()
    def generate(self, phoneNumber):
        outFile = 'tel.png'
        formatStr = 'TEL:{}'
        telEndsWith = []
        phoneNumber = self.qrGen.escapeStr(phoneNumber, telEndsWith)
        qrStr = formatStr.format(phoneNumber)
        self.qrGen.createCode(outFile, qrStr)

class QrWiFi:
    qrGen = None
    def __init__(self):
        self.qrGen = QrGenerator()
    def requireVisibility(self, visibility):
        if visibility == 'hidden':
            return True
        if visibility == 'visible':
            return True
        if visibility == '':
            return True
        self.qrGen.printErrorText('ERROR: Unknown visibility setting.')
    def getVisibilityString(self, visibility):
        if visibility == 'hidden':
            return 'H:true'
        if visibility == 'visible':
            return 'H:false'
        if visibility == '':
            return ''
        self.qrGen.printErrorText('ERROR: Unknown visibility setting (check before).')
    def generate(self, ssid, authType, password, visibility):
        outFile = '{}.png'.format(ssid)
        formatStr = 'WIFI:S:{};T:{};P:{};{};'
        ssidEndsWith = [ ';' ]
        authTypeEndsWith = [ ';' ]
        passwordEndsWith = [ ';' ]
        visibilityStr = self.getVisibilityString(visibility)
        ssid = self.qrGen.escapeStr(ssid, ssidEndsWith)
        authTypeEndsWith = self.qrGen.escapeStr(authType, authTypeEndsWith)
        password = self.qrGen.escapeStr(password, passwordEndsWith)
        qrStr = formatStr.format(ssid, authType, password, visibilityStr)
        self.qrGen.createCode(outFile, qrStr)

class QrGenPrompt():
    def readLine(self, prompt):
        print(prompt, end='', flush=True)
        line = sys.stdin.readline().rstrip()
        return line
    def generateBitcoin(self):
        qrBitcoin = QrBitcoin()
        address = self.readLine('Enter your address: ')
        amount = self.readLine('Enter your amount (O): ')
        label = self.readLine('Enter your label (O): ')
        message = self.readLine('Enter your message (O): ')
        qrBitcoin.generate(address, amount, label, message)
    def generateEmailAddress(self):
        qrEmailAddress = QrEmailAddress()
        emailAddress = self.readLine('Enter your email address: ')
        qrEmailAddress.generate(emailAddress)
    def generateEmailMessage(self):
        qrEmailMessage = QrEmailMessage()
        emailAddress = self.readLine('Enter your email address: ')
        subject = self.readLine('Enter your subject: ')
        body = self.readLine('Enter your body text: ')
        qrEmailMessage.generate(emailAddress, subject, body)
    def generatePaypal(self):
        qrPaypal = QrPaypal()
        emailAddress = self.readLine('Enter your email address: ')
        itemDesc = self.readLine('Enter your item description: ')
        prodCode = self.readLine('Enter your product code (O): ')
        itemValue = self.readLine('Enter your item value: ')
        currency = self.readLine('Enter your currency: ')
        qrPaypal.generate(emailAddress, itemDesc, prodCode, itemValue, currency)
    def generatePlainText(self):
        qrPlainText = QrPlainText()
        plainText = self.readLine('Enter your text: ')
        qrPlainText.generate(plainText)
    def generateSms(self):
        qrSms = QrSms()
        sms = self.readLine('Enter your phone number: ')
        message = self.readLine('Enter your message: ')
        qrSms.generate(sms, message)
    def generateTel(self):
        qrTel = QrTel()
        tel = self.readLine('Enter your phone number: ')
        qrTel.generate(tel)
    def generateWiFi(self):
        qrWiFi = QrWiFi()
        visibility = self.readLine('Enter your visibility (O): ')
        qrWiFi.requireVisibility(visibility)
        ssid = self.readLine('Enter your SSID: ')
        password = self.readLine('Enter your password: ')
        authType = self.readLine('Enter your auth type: ')
        qrWiFi.generate(ssid, authType, password, visibility)

class QrGenCommandLine:
    command = None
    arguments = None
    prompt = None
    def __init__(self, args):
        self.command = args[0]
        self.arguments = args[1:]
        self.prompt = QrGenPrompt()
    def manageMainUsage(self):
        if len(self.arguments) <= 0:
            self.printUsageMainOptions()
        firstArg = self.arguments[0]
        if len(self.arguments) == 1:
            if firstArg == 'printtypes':
                self.printCommandTypes()
            else:
                self.printUsageMainOptions()
        elif len(self.arguments) == 2:
            secondArg = self.arguments[1]
            if firstArg == 'prompt':
                if secondArg == 'bitcoin':
                    self.prompt.generateBitcoin()
                elif secondArg == 'emailaddress':
                    self.prompt.generateEmailAddress()
                elif secondArg == 'emailmessage':
                    self.prompt.generateEmailMessage()
                elif secondArg == 'paypal':
                    self.prompt.generatePaypal()
                elif secondArg == 'plaintext':
                    self.prompt.generatePlainText()
                elif secondArg == 'sms':
                    self.prompt.generateSms()
                elif secondArg == 'tel':
                    self.prompt.generateTel()
                elif secondArg == 'wifi':
                    self.prompt.generateWiFi()
                else:
                    self.printUsageMainOptions()
            elif firstArg == 'args':
                print('ERROR: Not implemented.')
            else:
                self.printUsageMainOptions()
        else:
            self.printUsageMainOptions()
    def printUsageMainOptions(self):
        print('{} args <cmdtype> <arguments>'.format(self.command))
        print('{} prompt <cmdtype>'.format(self.command))
        print('{} printtypes'.format(self.command))
        print('')
        print('Note: Use "args" to pass cmdtype arguments directly or "prompt" for interactivity.')
        print('Note: Use "printtypes" to see all supported command types and their arguments.')
        sys.exit(1)
    def printCommandTypeBitcoin(self):
        print('----------------------------------------------------------------------------------------')
        print('Usage: bitcoin <address> <amount> <label> <message>')
        print('Generates QR code from bitcoin definition.')
        print('----------------------------------------------------------------------------------------')
        print('address = Bitcoin address.')
        print(' amount = Amount in mBTC (optional).')
        print('  label = Label (e.g. name of recipient, optional).')
        print('message = Message (e.g. reason of transaction, optional).')
        print('')
        print('')
    def printCommandTypeEmailAddress(self):
        print('----------------------------------------------------------------------------------------')
        print('Usage: emailaddress <string>')
        print('Generates QR code from email address.')
        print('----------------------------------------------------------------------------------------')
        print('string = The email address.')
        print('')
        print('')
    def printCommandTypeEmailMessage(self):
        print('----------------------------------------------------------------------------------------')
        print('Usage: emailmessage <Email address> <Subject> <Body>')
        print('Generates QR code from email message.')
        print('----------------------------------------------------------------------------------------')
        print('Email address = The email address.')
        print('      Subject = Subject of the message.')
        print('         Body = Body text of the message.')
        print('')
        print('')
    def printCommandTypePaypal(self):
        print('----------------------------------------------------------------------------------------')
        print('Usage: paypal <Email address> <Item description> <Item product code> <Item value> <Currency>')
        print('Generates QR code from paypal information.')
        print('----------------------------------------------------------------------------------------')
        print('    Email address: Email address of the Paypal account.')
        print(' Item description: Description of the item.')
        print('Item product code: Product code of the item.')
        print('       Item value: Value of the item.')
        print('         Currency: Used currency (three letters).')
        print('')
        print('')
    def printCommandTypePlainText(self):
        print('----------------------------------------------------------------------------------------')
        print('Usage: plaintext <string>')
        print('Generates QR code from plain text. Plain text can include URL(s) and/or URI(s).')
        print('----------------------------------------------------------------------------------------')
        print('string = The plain text. Examples are "https://www.google.com" and "tel:+1-816-555-1212".')
        print('')
        print('')
    def printCommandTypeSms(self):
        print('----------------------------------------------------------------------------------------')
        print('Usage: sms <Phone number> <Message>')
        print('Generates QR code from SMS message to a phone number.')
        print('----------------------------------------------------------------------------------------')
        print('Phone number = The phone number.')
        print('     Message = The SMS message.')
        print('')
        print('')
    def printCommandTypeTel(self):
        print('----------------------------------------------------------------------------------------')
        print('Usage: tel <Phone number>')
        print('Generates QR code from phone number.')
        print('----------------------------------------------------------------------------------------')
        print('Phone number = The phone number.')
        print('')
        print('')
    def printCommandTypeWiFi(self):
        print('----------------------------------------------------------------------------------------')
        print('Usage: wifi hidden|visible <SSID> <Password> <Authentication type>')
        print('Generates QR code from WiFi network settings.')
        print('----------------------------------------------------------------------------------------')
        print('             hidden = Explicitly set "visible" for the network visibility (may be needed sometimes).')
        print('            visible = Explicitly set "hidden" for the network visibility (usually not needed).')
        print('               SSID = Network SSID (network name).')
        print('           Password = Password of the network. Ignored if <Authentication type> is "nopass".')
        print('Authentication type = Examples include WEP, WPA, WPA2 or "nopass" if no encryption.')
        print('Note: Setting "hidden|visible" is optional. Leave empty if not needed (recommended by default).')
        print('')
        print('')
    def printCommandTypes(self):
        self.printCommandTypeBitcoin()
        self.printCommandTypeEmailAddress()
        self.printCommandTypeEmailMessage()
        self.printCommandTypePaypal()
        self.printCommandTypePlainText()
        self.printCommandTypeSms()
        self.printCommandTypeTel()
        self.printCommandTypeWiFi()

if __name__ == '__main__':
    cmdLine = QrGenCommandLine(sys.argv)
    cmdLine.manageMainUsage()

