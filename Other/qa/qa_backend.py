#!/usr/bin/env python
# Arguments for this script:
# 1: Course definition [courseDef]
# The first argument defines "all" or
# "courses:<course1>[,<courseN>]":
# all courses are asked with "all",
# only comma separated courses are asked with comma separated list
# 2: Category definition [categoryDef]
# The second argument defines "all" or
# "categories:<category1>[,<categoryN>]":
# all categories are asked with "all",
# only comma separated categories are asked with comma separated list
# 3: Configuration ID
# The third argument defines the configuration ID
# This script tries to find the "ConfigId:<ID>" from the scripts
# This script uses the "config/" directory under the working directory
# 4: Path and file name to the questions/answers file

# Configuration file format:
# [askAmount]
# The first line defines the number of questions asked: integer
# (number of questions from set) or percentage (percentage of questions from
# set) without decimal
# [limitDef]
# The second line defines "timelimit:no" or "timelimit:<timelimit>":
# no timelimit for questions is defined with "no",
# timelimit as seconds is defined with "<timelimit>"
# [sourceDef]
# The third line defines "firstonly", "secondonly" or "bothways":
# only first entry is the question source with "firstonly",
# only second entry is the question source with "secondonly",
# both entries (randomly) are the question sources with "bothways"
# [askType]
# The fourth line defines "ask" or "generate":
# questions are asked with "ask"
# [showOrigin]
# The fifth line defines "showorigin", "hideorigin", "hidetotal" or
# "hidecurrent":
# origin is shown with "showorigin",
# origin is hidden with "hideorigin",
# origin and total number of questions are hidden with "hidetotal",
# origin, total number of questions and current number of question are
# hidden with "hidecurrent"
# [showCorrect]
# The sixth line defines "showcorrectanswer", "showcorrectandwrong" or
# "hidecorrectandwrong":
# correct answer with right and wrong texts are shown with "showcorrectanswer",
# right and wrong texts are shown with "showcorrectandwrong",
# right and wrong texts are hidden with "hidecorrectandwrong"
# [caseSensitivity, spaceSensitivity]
# The next two lines define "casesensitive" or "caseinsensitive" and
# "spacesensitive" or "spaceinsensitive":
# cases are normalized with "caseinsensitive" and spaces are removed with
# "spaceinsensitive"
import os
import sys
import time
import signal
import random
from multiprocessing.connection import Listener

from qa_ipc import *
from qa_backend_config import *

class QABackend:
    # IPC specific begin
    qaIpc               = 0
    listener            = 0
    connection          = 0
    frontendEndString   = ""
    frontendLostString  = ""
    backendLostString   = ""
    # IPC specific end
    # Settings begin
    configArgs          = 0
    qaBackendConfig     = 0
    currentSettings     = 0
    # Settings end
    # Module specific (ask/generate) begin
    categoryLookup      = []
    randList            = []
    pairedStart         = 0
    numberOfPairs       = 0
    numberOfCategorized = 0
    numberOfQuestions   = 0
    numOfCorrect        = 0
    numOfWrong          = 0
    oldTime             = 0  # for calculating elapsed time
    question            = ""
    answer              = ""
    specifier           = ""
    # Module specific (ask/generate) end
    qaFile              = 0
    # Helper variables for frontend/backend strings
    def __init__(self):
        self.qaIpc              = QAIpc()
        self.qaBackendConfig    = QABackendConfig()
        self.frontendEndString  = self.qaIpc.get_frontend_end_string()
        self.frontendLostString = self.qaIpc.get_frontend_lost_string()
        self.backendLostString  = self.qaIpc.get_backend_lost_string()
    def __del__(self):
        self.uninitialize_instance()
    # Helper for uninitialization just before exit
    def uninitialize_instance(self):
        if self.connection != 0:
            self.connection.close()
            self.connection = 0
        if self.listener != 0:
            self.listener.close()
            self.listener = 0
        if self.qaFile != 0:
            self.qaFile.close()
            self.qaFile = 0
    # Helper for error messages
    def send_error_string(self, error):
        # If needed in the future: Write error to socket (if alive)
        sys.stdout.write(error)
    # Helper for immediate program exit
    def do_program_exit(self, stopped, frontendLost, outOfTime):
        if not frontendLost:
            self.end_do_questions(stopped, outOfTime)
            self.ipc_send_string(self.backendLostString)
        sys.exit()
    # Helper function for IPC receive
    def ipc_receive_string(self):
        recvString = ""
        try:
            recvString += self.qaIpc.receive_string(self.connection)
        except Exception as exception:
            # Here we may get EOFError but let's catch all
            stringToSend = "BACKEND ERROR: (Receive) Frontend unexpectedly hung up at " + \
                str(self.listener.last_accepted) + "\n"
            self.send_error_string(stringToSend)
            self.do_program_exit(True, True, False)
        return recvString
    # Helper function for IPC send
    def ipc_send_string(self, stringToSend):
        try:
            self.qaIpc.send_string(self.connection, stringToSend)
        except Exception as exception:
            # Here we may get broken pipe (32) error but let's catch all
            stringToSend = "BACKEND ERROR: (Send) Frontend unexpectedly hung up at " + \
                str(self.listener.last_accepted) + "\n"
            self.send_error_string(stringToSend)
            self.do_program_exit(True, True, False)
    # Read the number of question/answer pairs
    def read_number_of_qa_pairs(self):
        qaFileName = self.configArgs.qaFileName
        self.qaFile = open(qaFileName, 'r')
        self.pairedStart = self.qaFile.tell()
        self.numberOfPairs = 0
        self.categoryLookup = []
        numberOfLine = 1
        while 1:
            qaLine = self.qaFile.readline().rstrip('\n')
            if not qaLine:
                break
            qa = qaLine.split('\t')
            if len(qa) != 6:
                stringToSend = "BACKEND ERROR: Unknown number of line entries.\n"
                self.send_error_string(stringToSend)
                self.do_program_exit(False, False, False)
            if self.qaBackendConfig.is_matching_course(qa[0]):
                if self.qaBackendConfig.is_matching_category(qa[1]):
                    self.categoryLookup.append(numberOfLine)
                    self.numberOfPairs += 1
            numberOfLine += 1
        if self.numberOfPairs == 0:
            stringToSend = "BACKEND ERROR: No pairs of question/answers.\n"
            self.send_error_string(stringToSend)
            self.do_program_exit(False, False, False)
        self.qaFile.close()
    # Now ask the questions by preparing the random list first
    def set_next_empty_slot(self, randNum, valueToSet):
        i = 0
        while 1:
            if self.randList[i] == 0:
                randNum -= 1
                if randNum == 0:
                    self.randList[i] = valueToSet
                    break
            i += 1
    def prepare_random_list(self):
        self.randList = []
        for i in range(self.numberOfPairs):
            self.randList.append(0)
        for i in range(len(self.categoryLookup)):
            randNum = random.randrange(1, self.numberOfPairs-i+1)
            self.set_next_empty_slot(randNum, self.categoryLookup[i])
        self.categoryLookup = []
        self.numberOfPairs = len(self.randList)
        if self.numberOfQuestions < self.numberOfPairs:
            del self.randList[self.numberOfQuestions:]
    def get_time_string_from_seconds(self, seconds, rawConversion):
        if seconds == 0 and not rawConversion:
            return "Unlimited"
        m, s = divmod(seconds, 60)
        h, m = divmod(m, 60)
        timeString = "%02d:%02d:%02d" % (h, m, s)
        return timeString
    def is_randomly_true(self):
        return bool(random.choice([True, False]))
    def get_right_question(self, question, answer, specifier):
        doShuffle = False
        if self.currentSettings.sourceDef == "secondonly":
            doShuffle = True
        elif self.currentSettings.sourceDef == "bothways":
            if self.is_randomly_true():
                doShuffle = True
        if doShuffle:
            questionTmp = question
            question = answer
            answer = questionTmp
            if len(specifier) > 0:
                if len(specifier) == 1 and specifier[0] == '-':
                    specifier = ""
                else:
                    specifier = " (" + specifier + ")"
        else:
            specifier = ""
        return (question, answer, specifier)
    def is_matching_answer(self, correctAnswers, givenAnswer):
        correctAnswersList = [correctAnswers]
        splittedLine = correctAnswers.split(':')
        if len(splittedLine) == 2:
            splittedLine[0] = splittedLine[0].lower()
            if splittedLine[0] == "oneof":
                correctAnswersList = splittedLine[1].split(',')
        for correctAnswer in correctAnswersList:
            correctAnswer = correctAnswer.strip()
            if correctAnswer == givenAnswer:
                return True
        return False
    def ask_question(self, tip):
        stringToSend = "A: " + self.qaIpc.get_prompt_string()
        self.ipc_send_string(stringToSend)
        stringToSend = ""
        inputAnswer = self.ipc_receive_string()
        if inputAnswer == self.frontendEndString:
            self.do_program_exit(True, False, False)
        if inputAnswer == self.frontendLostString:
            self.do_program_exit(True, True, False)
        inputAnswer = self.qaBackendConfig.manipulate_string_with_config( \
            inputAnswer)
        if self.is_matching_answer(self.answer, inputAnswer):
            self.numOfCorrect += 1
            if self.currentSettings.showCorrect != "hidecorrectandwrong":
                stringToSend += "CORRECT (" + str(self.numOfCorrect) + "/" + \
                      str(self.numberOfQuestions) + ")" + "\n"
            stringToSend += "\n"
            self.ipc_send_string(stringToSend)
            return
        # Next is for wrong answer cases
        self.numOfWrong += 1
        if self.currentSettings.showCorrect != "hidecorrectandwrong":
            stringToSend += "WRONG (" + str(self.numOfWrong) + "/" + \
                str(self.numberOfQuestions) + ")" + "\n"
        if self.currentSettings.showCorrect == "showcorrectanswer":
            stringToSend += "CORRECT: \"" + self.answer + "\"" + "\n"
        if tip != "":
            stringToSend += "TIP: " + tip + "\n"
        if self.currentSettings.limitDef > 0:
            timeLeft = int(signal.getitimer(signal.ITIMER_REAL)[0])
            warningLevel = int(float(self.currentSettings.limitDef) / 3.0)
            if timeLeft <= warningLevel:
                stringToSend += "\n" + "TIME: " + str(timeLeft) + \
                    " seconds left" + "\n"
        stringToSend += "\n"
        self.ipc_send_string(stringToSend)
    def generate_answer(self, tip):
        stringToSend = "A: " + self.answer + "\n"
        if tip != "":
           stringToSend += "TIP: " + tip + "\n"
        stringToSend += "\n"
        self.ipc_send_string(stringToSend)
    def get_visible_question(self):
        foundQuestion = self.question
        splittedLine = foundQuestion.split(':')
        if len(splittedLine) == 2:
            splittedLine[0] = splittedLine[0].lower()
            if splittedLine[0] == "oneof":
                questionList = splittedLine[1].split(',')
                foundQuestion = questionList[0].lower()
        return foundQuestion
    def signal_handler_interrupt(self, signal, frame):  # CALLBACK
        self.do_program_exit(True, False, False)
    def signal_handler_timer(self, signal, frame):  # CALLBACK
        self.do_program_exit(True, False, True)
    def set_arguments(self, arguments):  # PUBLIC
        hasArguments = self.qaBackendConfig.set_arguments(arguments)
        if not hasArguments:
            self.do_program_exit(False, False, False)
        self.configArgs = self.qaBackendConfig.configArgs
    def initialize_settings_file_based(self):  # PUBLIC
        self.qaBackendConfig.get_configurations("config/")
        selectedSettings = self.qaBackendConfig.select_settings()
        if selectedSettings < 0:
            stringToSend = "BACKEND ERROR: Configuration not found!\n"
            self.send_error_string(stringToSend)
            self.do_program_exit(False, False, False)
        self.currentSettings = self.qaBackendConfig.get_selected_settings()
    def initialize_qa_file_based(self):  # PUBLIC
        self.read_number_of_qa_pairs()
        self.numberOfQuestions = \
            self.qaBackendConfig.decode_configurations(self.numberOfPairs)
        if self.numberOfQuestions == 0:
            stringToSend = "BACKEND ERROR: No questions to ask!\n"
            self.send_error_string(stringToSend)
            self.do_program_exit(False, False, False)
    def initialize_question_based(self):  # PUBLIC
        self.prepare_random_list()
    def initialize_signal_handlers(self):  # PUBLIC
        if self.configArgs.hasIntSig:
            # "Hidden" option for optional int signal handler (on by default):
            # The interrupt signal must not be processed if launched from
            # bash script as CTRL+C will be received by both frontend and
            # backend, thus causing the frontend to never receive the result
            signal.signal(signal.SIGINT, self.signal_handler_interrupt)
        signal.signal(signal.SIGALRM, self.signal_handler_timer)
        if self.currentSettings.limitDef > 0:
            signal.setitimer(signal.ITIMER_REAL, self.currentSettings.limitDef)
    def start_backend(self, bindAddress, bindPort):  # PUBLIC
        # Start listening for the first connection
        address = (bindAddress, bindPort)
        self.listener = Listener(address, authkey="password")
        print "BACKEND INFO: Listening at " + str(address)
        self.connection = self.listener.accept()
        print "BACKEND INFO: Connection accepted from " + str(self.listener.last_accepted)
        # Show time to do questions
        if self.currentSettings.askType == "ask":
            limitDef = self.currentSettings.limitDef
            stringToSend = "\nTime available: "
            stringToSend += self.get_time_string_from_seconds(limitDef, False)
            stringToSend += "\n\n"
            self.ipc_send_string(stringToSend)
        # Start asking questions
        qaFileName = self.configArgs.qaFileName
        self.qaFile = open(qaFileName, 'r')
        self.numOfCorrect = 0
        self.numOfWrong = 0
        self.oldTime = time.time()
        numOfCurrentQuestion = 1
        for pos in self.randList:
            self.qaFile.seek(self.pairedStart)
            self.question = ""
            self.answer = ""
            self.specifier = ""
            for i in range(pos):
                qaLine = self.qaFile.readline().rstrip('\n')
                qa = qaLine.split('\t')
                self.question = qa[2]
                self.answer = qa[4]
                self.specifier = qa[5]
                self.question, self.answer, self.specifier = \
                    self.get_right_question( \
                    self.question, self.answer, self.specifier)
            self.question = self.question.strip()
            self.answer = self.qaBackendConfig.manipulate_string_with_config( \
                self.answer)
            # Now we have the question and answer
            stringToSend = "Q"
            if self.currentSettings.showOrigin != "hidecurrent":
                stringToSend += " " + str(numOfCurrentQuestion)
            if self.currentSettings.showOrigin != "hidetotal" and \
               self.currentSettings.showOrigin != "hidecurrent":
                stringToSend += "/" + str(self.numberOfQuestions)
            if self.currentSettings.showOrigin == "showorigin":
                stringToSend += " (" + str(pos) + ")"
            question = self.get_visible_question()
            stringToSend += ": " + question + self.specifier
            stringToSend += "\n"
            self.ipc_send_string(stringToSend)
            tip = ""
            if qa[3] != "-":
                tip = qa[3]
            if self.currentSettings.askType == "ask":
                self.ask_question(tip)
            else:
                self.generate_answer(tip)
            numOfCurrentQuestion += 1
        self.end_do_questions(False, False)
        self.ipc_send_string(self.backendLostString)
    def end_do_questions(self, stopped, outOfTime):  # PUBLIC
        if self.currentSettings.askType != "ask" or self.numberOfQuestions == 0:
            return
        # Next is for "ask" case
        elapsedTime = int(time.time() - self.oldTime)
        timeString = self.get_time_string_from_seconds(elapsedTime, True)
        stringToSend = ""
        if stopped:
            stringToSend += "\n"
            if outOfTime:
                stringToSend += "OUT OF TIME!" + "\n"
                stringToSend += "\n"
        stringToSend += "Results"                                      + "\n"
        stringToSend += "==============================="              + "\n"
        stringToSend += "Elapsed time: " + timeString                  + "\n"
        stringToSend += "Correct:      " + str(self.numOfCorrect)      + "\n"
        stringToSend += "Wrong:        " + str(self.numOfWrong)        + "\n"
        stringToSend += "Total:        " + str(self.numberOfQuestions) + "\n"
        correctRatio = float(self.numOfCorrect) / \
            float(self.numberOfQuestions) * 100.0
        stringToSend += "Summary:      " + str(int(correctRatio)) + \
            " % was correct" + "\n"
        stringToSend += "\n"
        self.ipc_send_string(stringToSend)

qaBackend = QABackend()
qaBackend.set_arguments(sys.argv[1:])
qaBackend.initialize_settings_file_based()
qaBackend.initialize_qa_file_based()
qaBackend.initialize_question_based()
qaBackend.initialize_signal_handlers()

qaBackend.start_backend("localhost", 6000)

