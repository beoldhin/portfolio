#!/usr/bin/env python

import sys

class QAIpc:
    # IPC specific begin
    eotString          = "<EOT>"
    promptString       = "<PROMPT>"
    frontendEndString  = "<FRONTEND_END>"
    frontendLostString = "<FRONTEND_LOST>"
    backendLostString  = "<BACKEND_LOST>"
    def extract_substring(self, mainString, stringToExtract):
        stringToExtractLen = len(stringToExtract)
        if len(mainString) < stringToExtractLen:
            return (False, "")
        extractedString = mainString[-stringToExtractLen:]
        if extractedString != stringToExtract:
            return (False, "")
        mainStringWithoutExtract = mainString[:-stringToExtractLen]
        return (True, mainStringWithoutExtract)
    def get_eot_string(self):  # PUBLIC
        return self.eotString
    def get_prompt_string(self):  # PUBLIC
        return self.promptString
    def get_frontend_end_string(self):  # PUBLIC
        return self.frontendEndString
    def get_frontend_lost_string(self):  # PUBLIC
        return self.frontendLostString
    def get_backend_lost_string(self):  # PUBLIC
        return self.backendLostString
    def receive_string(self, connection):  # PUBLIC
        recvString = ""
        eotStringLen = len(self.eotString)
        while 1:
            recvBytes = connection.recv_bytes()
            recvString += recvBytes
            stringHasEot, stringWithoutEot = \
                self.extract_substring(recvString, self.eotString)
            if not stringHasEot:
                continue
            return stringWithoutEot
    def send_string(self, connection, stringToSend):  # PUBLIC
        if connection != 0:
            stringToSend += self.get_eot_string()
            connection.send_bytes(stringToSend)
        else:
            # Filter out command string if accidentally coming here
            if len(stringToSend) > 0 and stringToSend[0] == '<':
                return
            sys.stdout.write(stringToSend)
    def get_input_prompt(self, recvString):  # PUBLIC
        stringHasPrompt, stringWithoutPrompt = \
            self.extract_substring(recvString, self.promptString)
        return (stringHasPrompt, stringWithoutPrompt)

