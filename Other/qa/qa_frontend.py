#!/usr/bin/env python
import sys
import signal
from multiprocessing.connection import Client

from qa_ipc import *

class QAFrontend:
    address            = 0
    # IPC specific begin
    qaIpc              = 0
    connection         = 0
    frontendEndString  = ""
    frontendLostString = ""
    backendLostString  = ""
    # Helper variables for frontend/backend strings
    def __init__(self):
        self.qaIpc              = QAIpc()
        self.frontendEndString  = self.qaIpc.get_frontend_end_string()
        self.frontendLostString = self.qaIpc.get_frontend_lost_string()
        self.backendLostString  = self.qaIpc.get_backend_lost_string()
    def __del__(self):
        self.uninitialize_instance()
    # Helper for uninitialization just before exit
    def uninitialize_instance(self):
        if self.connection != 0:
            self.connection.close()
            self.connection = 0
    # Helper for error messages
    def send_error_string(self, error):
        # If needed in the future: Write error to socket (if alive)
        sys.stdout.write(error)
    # Helper for immediate program exit
    def do_program_exit(self, stopped, backendLost):
        if not backendLost:
            self.ipc_send_string(self.frontendLostString)
        self.uninitialize_instance()
        sys.exit()
    # Helper function for IPC receive
    def ipc_receive_string(self):
        recvString = ""
        try:
            recvString += self.qaIpc.receive_string(self.connection)
        except Exception as exception:
            # Here we may get EOFError but let's catch all
            stringToSend = "FRONTEND ERROR: (Receive) Backend unexpectedly hung up at " + \
                str(self.address) + "\n"
            self.send_error_string(stringToSend)
            self.do_program_exit(True, True)
        return recvString
    # Helper function for IPC send
    def ipc_send_string(self, stringToSend):
        # TODO: Why no broken pipe from here after answer and no backend?
        try:
            self.qaIpc.send_string(self.connection, stringToSend)
        except Exception as exception:
            # Here we may get broken pipe (32) error but let's catch all
            stringToSend = "FRONTEND ERROR: (Send) Backend unexpectedly hung up at " + \
                str(self.address) + "\n"
            self.send_error_string(stringToSend)
            self.do_program_exit(True, True)
    def signal_handler_interrupt(self, signal, frame):  # CALLBACK
        # Here we don't exit but only make backend to give the result
        self.ipc_send_string(self.frontendEndString)
    def initialize_signal_handlers(self):  # PUBLIC
        signal.signal(signal.SIGINT, self.signal_handler_interrupt)
    def start_frontend(self, connectAddress, connectPort):  # PUBLIC
        self.address = (connectAddress, connectPort)
        print "FRONTEND INFO: Connecting to " + str(self.address)
        try:
            self.connection = Client(self.address, authkey="password")
        except Exception as exception:
            print "FRONTEND ERROR: Could not connect to " + str(self.address)
            self.do_program_exit(True, True)
        print "FRONTEND INFO: Connection OK to " + str(self.address)
        while 1:
            recvString = self.ipc_receive_string()
            if recvString == self.backendLostString:
                break
            stringHasPrompt, stringWithoutPrompt = \
                self.qaIpc.get_input_prompt(recvString)
            if not stringHasPrompt:
                sys.stdout.write(recvString)
                continue
            # Next is for the input case
            inputAnswer = raw_input(stringWithoutPrompt)
            self.ipc_send_string(inputAnswer)
        print "FRONTEND INFO: Connection lost to " + str(self.address)
        self.connection.close()
        self.connection = 0

qaFrontend = QAFrontend()
qaFrontend.initialize_signal_handlers()

qaFrontend.start_frontend("localhost", 6000)

