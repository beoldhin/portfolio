#!/usr/bin/env python

# NOTE: Remove sys.exit() from this class and return False/True instead!

import os
import sys
import copy

class QAConfigArgs:
    courseDef   = []
    categoryDef = []
    configId    = ""
    qaFileName  = ""
    hasIntSig   = True

class QAConfigEntry:
    configId         = ""
    askType          = ""
    limitDef         = 0
    askAmount        = ""
    sourceDef        = ""
    showOrigin       = ""
    showCorrect      = ""
    caseSensitivity  = ""
    spaceSensitivity = ""
    def reset(self):
        self.configId         = ""
        self.askType          = ""
        self.limitDef         = 0
        self.askAmount        = ""
        self.sourceDef        = ""
        self.showOrigin       = ""
        self.showCorrect      = ""
        self.caseSensitivity  = ""
        self.spaceSensitivity = ""

class QABackendConfig:
    configArgs       = 0
    # Settings begin
    currentSettings  = 0
    selectedSettings = -1
    allSettings      = []
    settingsFileName = ""
    settingsFile     = 0
    # Settings end
    def __init__(self):
        self.configArgs      = QAConfigArgs()
        self.currentSettings = QAConfigEntry()
    def __del__(self):
        self.uninitialize_instance()
    def uninitialize_instance(self):
        if self.settingsFile != 0:
            self.settingsFile.close()
            self.settingsFile = 0
    # Helper for error messages
    def send_error_string(self, error):
        # If needed in the future: Write error to socket (if alive)
        sys.stdout.write(error)
    # Read one line of configuration
    def read_configuration_line(self):
        line = self.settingsFile.readline().rstrip('\n')
        if not line:
            return ""
        line = line.strip()
        line = line.lower()
        splittedLine = line.split(':')
        if line == "ask" or line == "generate":
            self.currentSettings.askType = line
        elif len(splittedLine) == 2:
            if splittedLine[0] == "configid":
                self.currentSettings.configId = splittedLine[1]
            elif splittedLine[0] == "timelimit":
                if splittedLine[1] == "no":
                    self.currentSettings.limitDef = 0
                else:
                    self.currentSettings.limitDef = int(splittedLine[1])
        elif line[-1] == '%' or line.isdigit():
            self.currentSettings.askAmount = line
        elif line == "firstonly" or line == "secondonly" or line == "bothways":
            self.currentSettings.sourceDef = line
        elif line == "showorigin" or line == "hideorigin" or \
             line == "hidetotal" or line == "hidecurrent":
            self.currentSettings.showOrigin = line
        elif line == "showorigin" or line == "hideorigin":
            self.currentSettings.showOrigin = line
        elif line == "showcorrectanswer" or line == "showcorrectandwrong" or \
             line == "hidecorrectandwrong":
            self.currentSettings.showCorrect = line
        elif line == "casesensitive" or line == "caseinsensitive":
            self.currentSettings.caseSensitivity = line
        elif line == "spacesensitive" or line == "spaceinsensitive":
            self.currentSettings.spaceSensitivity = line
        return line
    def read_configuration_file(self):
        self.settingsFile = open(self.settingsFileName, 'r')
        self.currentSettings.categoryDef = []
        line = "do-while simulation so some text here"
        while line != "":
            line = self.read_configuration_line()
        if self.currentSettings.askType == "generate":
            self.currentSettings.limitDef         = 0
            self.currentSettings.showCorrect      = "N/A"
            self.currentSettings.caseSensitivity  = "N/A"
            self.currentSettings.spaceSensitivity = "N/A"
        if self.currentSettings.configId         == "" or \
           self.currentSettings.askType          == "" or \
           self.currentSettings.limitDef           < 0 or \
           self.currentSettings.askAmount        == "" or \
           self.currentSettings.sourceDef        == "" or \
           self.currentSettings.showOrigin       == "" or \
           self.currentSettings.showCorrect      == "" or \
           self.currentSettings.caseSensitivity  == "" or \
           self.currentSettings.spaceSensitivity == "":
            stringToSend =  "BACKEND Error: Missing configuration\n"
            stringToSend += "configId = "             +  \
                self.currentSettings.configId         + "\n"
            stringToSend += "askType = "              +  \
                self.currentSettings.askType          + "\n"
            stringToSend += "limitDef = "             +  \
                str(self.currentSettings.limitDef)    + "\n"
            stringToSend += "askAmount = "            +  \
                self.currentSettings.askAmount        + "\n"
            stringToSend += "sourceDef = "            +  \
                self.currentSettings.sourceDef        + "\n"
            stringToSend += "showOrigin = "           +  \
                self.currentSettings.showOrigin       + "\n"
            stringToSend += "showCorrect = "          +  \
                self.currentSettings.showCorrect      + "\n"
            stringToSend += "caseSensitivity = "      +  \
                self.currentSettings.caseSensitivity  + "\n"
            stringToSend += "spaceSensitivity = "     +  \
                self.currentSettings.spaceSensitivity + "\n"
            self.send_error_string(stringToSend)
            self.settingsFile.close()
            self.settingsFile = 0
            return False
        for i in range(len(self.allSettings)):
            if self.currentSettings.configId == self.allSettings[i].configId:
                stringToSend = "BACKEND ERROR: Duplicate config ID(s)\n"
                self.send_error_string(stringToSend)
                self.settingsFile.close()
                self.settingsFile = 0
                return False
        self.settingsFile.close()
        self.settingsFile = 0
        return True
    def set_arguments(self, arguments):  # PUBLIC
        argsLen = len(arguments)
        if argsLen < 4 or argsLen > 5:
            stringToSend = "BACKEND ERROR: Invalid number of arguments.\n"
            self.send_error_string(stringToSend)
            return False
        for i in range(len(arguments)):
            if i != 3:  # Paths/files names must be intact
                arguments[i] = arguments[i].lower()
        if arguments[0] == "all":
            self.configArgs.courseDef.append("all")
        else:
            self.configArgs.courseDef = arguments[0].split(',')
        if arguments[1] == "all":
            self.configArgs.categoryDef.append("all")
        else:
            self.configArgs.categoryDef = arguments[1].split(',')
        self.configArgs.configId = arguments[2]
        self.configArgs.qaFileName = arguments[3]
        if argsLen == 5:
            if arguments[4] == "nointsig":
                self.configArgs.hasIntSig = False
            else:
                stringToSend = "BACKEND ERROR: Invalid nointsig definition.\n"
                self.send_error_string(stringToSend)
                return False
        return True
    def select_settings(self):  # PUBLIC
        self.selectedSettings = -1
        configId = self.configArgs.configId
        for i in range(len(self.allSettings)):
            if self.allSettings[i].configId == configId:
                self.selectedSettings = i;
                break
        return self.selectedSettings
    def get_selected_settings(self):  # PUBLIC
        selectedSettings = self.allSettings[self.selectedSettings]
        return selectedSettings
    def is_matching_course(self, course):  # PUBLIC
        selectedSettings = self.get_selected_settings()
        course = course.lower()
        if self.configArgs.courseDef[0] == "all":
            return True
        for courseDef in self.configArgs.courseDef:
            courseDef = courseDef.strip()
            if courseDef == course:
                return True
        return False
    def is_matching_category(self, category):  # PUBLIC
        selectedSettings = self.get_selected_settings()
        category = category.lower()
        if self.configArgs.categoryDef[0] == "all":
            return True
        for categoryDef in self.configArgs.categoryDef:
            categoryDef = categoryDef.strip()
            if categoryDef == category:
                return True
        return False
    def get_configurations(self, startDir):  # PUBLIC
        for dirpath, dirnames, filenames in os.walk(startDir):
            for filename in filenames:
                self.settingsFileName = dirpath + "/" + filename
                succeeded = self.read_configuration_file()
                if not succeeded:
                    self.allSettings = []
                    return
                self.allSettings.append(copy.deepcopy(self.currentSettings))
                self.currentSettings.reset()
        print "BACKEND INFO: Found " + str(len(self.allSettings)) + " settings."
    # Check we have the number of question/answer pairs
    # Check askAmount or modify percentage to number
    def decode_configurations(self, numberOfPairs):  # PUBLIC
        selectedSettings = self.get_selected_settings()
        isPercentage = False
        askAmount = selectedSettings.askAmount
        if askAmount[-1] == '%':
            askAmount = selectedSettings.askAmount.replace("%", "")
            isPercentage = True
        if not askAmount.isdigit():
            stringToSend = "BACKEND ERROR: No digit detected for question amount (\"" +\
                selectedSettings.askAmount + "\")" + "\n"
            self.send_error_string(stringToSend)
            return 0
        numberOfQuestions = int(askAmount)
        if isPercentage:
            numberOfQuestions = int(float(askAmount)/100.0 * numberOfPairs)
        if numberOfQuestions == 0:
            stringToSend = "BACKEND ERROR: No questions to ask (0 < " + \
                str(numberOfPairs) + ")" + "\n"
            self.send_error_string(stringToSend)
            return 0
        if numberOfQuestions > numberOfPairs:
            stringToSend = "BACKEND WARNING: Not enough questions to ask (" + \
                  str(numberOfQuestions) + " > " + \
                  str(numberOfPairs) + ")" + "\n"
            self.send_error_string(stringToSend)
            numberOfQuestions = numberOfPairs
        return numberOfQuestions
    def manipulate_string_with_config(self, stringToManipulate):  # PUBLIC
        selectedSettings = self.get_selected_settings()
        returnString = stringToManipulate.strip()
        if selectedSettings.askType == "generate":
            return returnString
        if selectedSettings.caseSensitivity == "caseinsensitive":
            returnString = returnString.lower()
        if selectedSettings.spaceSensitivity == "spaceinsensitive":
            returnString = returnString.replace(" ", "")
        return returnString

