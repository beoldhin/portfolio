require(plotrix)
require(ggplot2)
require(reshape2)

density_geometry <- function() {
    return(c('point', 'smooth'))
}

theme_topleft <- function() {
    themedef <- theme(legend.justification=c(0,1),
                      legend.position=c(0,1))
    return(themedef)
}

theme_topleft_horizontal <- function() {
    themedef <- theme(legend.justification=c(0,1),
                      legend.position=c(0,1),
                      legend.box="horizontal",
                      legend.box.just="top")
    return(themedef)
}

theme_topright <- function() {
    themedef <- theme(legend.justification=c(1,1),
                      legend.position=c(1,1))
    return(themedef)
}

theme_bottomleft <- function() {
    themedef <- theme(legend.justification=c(0,0),
                      legend.position=c(0,0))
    return(themedef)
}

theme_bottomright <- function() {
    themedef <- theme(legend.justification=c(1,0),
                      legend.position=c(1,0))
    return(themedef)
}

tekstilaatikko <- function(teksti) {
    plot.new()
    textbox(c(0,1.0), 1, margin=0.02, teksti)
}

tekstilaatikko_khiihyväksy <- function(pvalue, muut1, muut2, teksti) {
    khii_hyväksy <- "Khiin neliö -menetelmällä havaittiin, että p-arvo on %s. Mikäli p-arvon rajana pidetään arvoa 0,05 tai jopa 0,01, tarkoittaisi tämä, että alle näiden arvojen menevä tulos johtaisi null-hypoteesin hylkäämiseen. Tässä tapauksessa tuli siis ilmi, että emme kyenneet hylkäämään null-hypoteesia, joka tarkoitti ettei %s välillä ole eroja. Khiin neliön expected-taulusta näemme arvot, jotka olisivat totta mikäli %s ei olisi vaikutusta. %s"
    teksti <- sprintf(khii_hyväksy, pvalue, muut1, muut2, teksti)
    tekstilaatikko(teksti)
}

tekstilaatikko_khiihylkää <- function(pvalue, muut1, muut2, teksti) {
    khii_hylkää <- "Khiin neliö -menetelmällä havaittiin, että p-arvo on %s. Mikäli p-arvon rajana pidetään arvoa 0,05 tai jopa 0,01, tarkoittaisi tämä, että alle näiden arvojen menevä tulos johtaisi null-hypoteesin hylkäämiseen. Tässä tapauksessa tuli siis ilmi, että jouduimme hylkäämään null-hypoteesin, joka tarkoitti ettei %s välillä ole eroja. Khiin neliön expected-taulusta näemme arvot, jotka olisivat totta mikäli %s ei olisi vaikutusta. %s"
    teksti <- sprintf(khii_hylkää, pvalue, muut1, muut2, teksti)
    tekstilaatikko(teksti)
}

tekstilaatikko_spearmanhyväksy <- function(pvalue, muut, teksti) {
    spearman_hyväksy <- "Spearmanin korrelaatiokertoimella havaittiin, että p-arvo on %s. Mikäli p-arvon rajana pidetään arvoa 0,05 tai jopa 0,01, tarkoittaisi tämä, että alle näiden arvojen menevä tulos johtaisi null-hypoteesin hylkäämiseen. Tässä tapauksessa tuli siis ilmi, että emme kyenneet hylkäämään null-hypoteesia, joka tarkoitti ettei %s välillä ole merkittävää korrelaatiota. %s"
    teksti <- sprintf(spearman_hyväksy, pvalue, muut, teksti)
    tekstilaatikko(teksti)
}

tekstilaatikko_spearmanhylkää <- function(pvalue, muut, teksti) {
    spearman_hylkää <- "Spearmanin korrelaatiokertoimella havaittiin, että p-arvo on %s. Mikäli p-arvon rajana pidetään arvoa 0,05 tai jopa 0,01, tarkoittaisi tämä, että alle näiden arvojen menevä tulos johtaisi null-hypoteesin hylkäämiseen. Tässä tapauksessa tuli siis ilmi, että jouduimme hylkäämään null-hypoteesin, joka tarkoitti ettei %s välillä ole merkittävää korrelaatiota. %s"
    teksti <- sprintf(spearman_hylkää, pvalue, muut, teksti)
    tekstilaatikko(teksti)
}

tekstilaatikko_kolmogorov <- function(pvalue, samplet, teksti) {
    kolmogorov <- "Kolmogorov-Smirnov-testillä saimme p-arvoksi %s. Mitä pienempi p-arvo tästä testistä saadaan, sitä suuremmalla todennäkösyydellä kahden eri populaation mittaustulokset ovat eri jakaumista. Tasaisen jakauman luomiseen käytimme omien datojemme keskiarvoa sekä keskihajontaa kuten käytämme alla olevassa kuvaajassa, johon olemme luoneet Tuotettu-funktiolle satunnaisia arvoja samalla menetelmällä. Arvoja luomme %i kappaletta, joka ei välttämättä vielä tuota täydellistä kuvaajaa, mutta riittää silmämääräiseen arviointiin. %s"
    teksti <- sprintf(kolmogorov, pvalue, samplet, teksti)
    tekstilaatikko(teksti)
}

tekstilaatikko_kruskal <- function(pvalue, teksti) {
    kruskal <- "Kruskal-Wallis-testillä saimme p-arvoksi %s. Mitä suurempi p-arvo tästä testistä saadaan, sitä pienemmällä todennäköisyydellä voidaan sanoa, että tutkitut jakaumat ovat erilaisia. %s"
    teksti <- sprintf(kruskal, pvalue, teksti)
    tekstilaatikko(teksti)
}

harjoitus_alustus <- function() {
    dataset <- read.csv(file="Asuntomyynti.csv", sep=',', dec='.')
    dataset$myyvuosi <- as.numeric(gsub(',', '', dataset$myyvuosi))
    dataset$neliöt <- as.numeric(gsub(',', '', dataset$neliöt))
    dataset$rakvuosi <- as.numeric(gsub(',', '', dataset$rakvuosi))
    dataset$hinta <- as.numeric(gsub(',', '', dataset$hinta))
    dataset$neliöhinta <- as.numeric(gsub(',', '', dataset$neliöhinta))
    dataset$ikä <- as.numeric(gsub(',', '', dataset$ikä))
    # message("Datatyypit")
    # message("----------")
    # print(sapply(dataset, class))
    return(dataset)
}

harjoitus_1 <- function() {
    # smoothing method (function) to use, eg. lm, glm, gam, loess, rlm. For datasets with n < 1000 default is loess.
    # For datasets with 1000 or more observations defaults to gam, see gam for more details.
    message("")
    message("Tehtävä 1")
    message("---------")
    message("Asunnon hinta vs. asunnon ikä:")
    plottext <- "Seuraavassa yhdeksässä kuvaajassa käytämme LOESS-menetelmää (myöhemmin binäärisssä riippumattomissa muuttujissa gam-menetelmää). Menetelmä soveltuu erityisesti tilanteisiin, joissa muuttujien välisen yhteyden matemaattista muotoa ei tiedetä (exploratory-tutkimus). Menetelmä kehitettiin 1979 eikä ole siis nk klassisten tilastomenetelmien joukossa (vaatii tietokoneen). Saatuja korrelaatioita vertaamme silmämääräisesti LOESS-kuvaajiin."
    tekstilaatikko(plottext)
    print(qplot(data=dataset, x=ikä, y=hinta, size=neliöt, colour=neliöt, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=ikä, y=hinta, size=neliöhinta, colour=neliöhinta, geom=density_geometry(), method='loess') + theme_topright())
    # Siivotaan rivit, joissa esiintyy "NA". Siivotaan myös oudot vuodet (-1).
    # Huom: emme siivoa niitä kun luemme tiedoston, sillä tällöin menettäisimme dataa (emme halua aina poistaa rivejä).
    ikä_vs_hinta <- data.frame(Ikä=dataset$ikä, Hinta=dataset$hinta)
    ikä_vs_hinta$Ikä[ikä_vs_hinta$Ikä == -1] <- NA
    ikä_vs_hinta <- na.omit(ikä_vs_hinta)
    korrelaatio <- cor(ikä_vs_hinta$Ikä, ikä_vs_hinta$Hinta)
    message("  Korrelaatio: ", korrelaatio)
    plottext <- "Tässä tapauksessa saimme korrelaatiokertoimeksi -0,2199198. Tämä tarkoittaa, että yhden muuttujan arvon kasvaessa toinen vähenee. Väheneminen ei ole kuitenkaan täysin suoraa (-1), sillä arvojen välillä on hajontaa. Kuvaajasta näemme, että asunnon iän lisääntyessä pyyntihinta jonkin verran laskee tarkastelujaksolla. Hajonta ja ilmeisesti myös tutkimusdatan luotettavuus kuitenkin lisääntyy iän kasvaessa, eli vanhimmissa asunnoissa on todennäköisesti hyvin hoidettuja arvoasuntoja (kivitalot, lähes museokohteet ydinkeskustoissa) ja huonosti hoidetut talot on jo purettu."
    tekstilaatikko(plottext)
    message("Asunnon hinta vs. asunnon pinta-ala:")
    print(qplot(data=dataset, x=neliöt, y=hinta, size=ikä, colour=ikä, geom=density_geometry(), method='loess') + theme_topleft())
    print(qplot(data=dataset, x=neliöt, y=hinta, size=neliöhinta, colour=neliöhinta, geom=density_geometry(), method='loess') + theme_topleft())
    neliöt_vs_hinta <- data.frame(Neliöt=dataset$neliöt, Hinta=dataset$hinta)
    korrelaatio <- cor(neliöt_vs_hinta$Neliöt, neliöt_vs_hinta$Hinta)
    message("  Korrelaatio: ", korrelaatio)
    plottext <- "Tässä tapauksessa saimme korrelaatiokertoimeksi 0,8112238. Tämä tarkoittaa, että yhden muuttujan arvon kasvaessa myös toinen muuttuja kasvaa. Muuttujien välinen sidos oli lähes täydellistä (+1) eikä arvojen välillä ollut juurikaan hajontaa. Myös asuntojen ikä ja neliöhintä näyttävät sijoittuvan tasaisesti käyrän ylä- ja alapuolelle."
    tekstilaatikko(plottext)
    message("Asunnon neliöhinta vs. asunnon ikä:")
    print(qplot(data=dataset, x=ikä, y=neliöhinta, size=hinta, colour=hinta, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=ikä, y=neliöhinta, size=neliöt, colour=neliöt, geom=density_geometry(), method='loess') + theme_topright())
    ikä_vs_neliöhinta <- data.frame(Ikä=dataset$ikä, Neliöhinta=dataset$neliöhinta)
    ikä_vs_neliöhinta$Ikä[ikä_vs_neliöhinta$Ikä == -1] <- NA
    ikä_vs_neliöhinta <- na.omit(ikä_vs_neliöhinta)
    korrelaatio <- cor(ikä_vs_neliöhinta$Ikä, ikä_vs_neliöhinta$Neliöhinta)
    message("  Korrelaatio: ", korrelaatio)
    plottext <- "Tässä tapauksessa saimme korrelaatiokertoimeksi -0,5422257. Tämä tarkoittaa, että yhden muuttujan arvon kasvaessa toinen vähenee. Väheneminen ei ole kuitenkaan täysin suoraa (-1), sillä arvojen välillä on hajontaa etenkin asunnon iän kasvaessa. Neliöhinta lähtee nousuun vasta kun asunnon ikä on n. 50 vuotta. Tulosten tulkintaa rasittaa kuitenkin ääripäiden poikkeavat lukumäärät ja outlierit."
    tekstilaatikko(plottext)
    message("Asunnon neliöhinta vs. asunnon pinta-ala:")
    print(qplot(data=dataset, x=neliöt, y=neliöhinta, size=hinta, colour=hinta, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=neliöt, y=neliöhinta, size=ikä, colour=ikä, geom=density_geometry(), method='loess') + theme_topright())
    neliöt_vs_neliöhinta <- data.frame(Neliöt=dataset$neliöt, Neliöhinta=dataset$neliöhinta)
    korrelaatio <- cor(neliöt_vs_neliöhinta$Neliöt, neliöt_vs_neliöhinta$Neliöhinta)
    message("  Korrelaatio: ", korrelaatio)
    plottext <- "Tässä tapauksessa saimme korrelaatiokertoimeksi -0,09708185. Tämä tarkoittaa, että yhden muuttujan arvon kasvaessa toinen vähenee. Väheneminen on kuitenkin lähes olematonta (0), mutta paremman kuvauksen tilanteesta ehkä antaa kuvaaja, sillä väheneminen ei ollut lineaarista ja hajonnassa oli suuria vaihteluita."
    tekstilaatikko(plottext)
    message("Asunnon neliöhinta vs. myyntivuosi:")
    print(qplot(data=dataset, x=myyvuosi, y=neliöhinta, size=hinta, colour=hinta, geom=density_geometry(), method='loess') + theme_topleft_horizontal())
    myyntivuosi_vs_neliöhinta <- data.frame(Myyntivuosi=dataset$myyvuosi, Neliöhinta=dataset$neliöhinta)
    korrelaatio <- cor(myyntivuosi_vs_neliöhinta$Myyntivuosi, myyntivuosi_vs_neliöhinta$Neliöhinta)
    message("  Korrelaatio: ", korrelaatio)
    plottext <- "Tässä tapauksessa saimme korrelaatiokertoimeksi 0,4531018. Tämä tarkoittaa, että yhden muuttujan arvon kasvaessa myös toinen muuttuja kasvaa. Kasvu oli kuitenkin jyrkkyydeltään puoliväliä (max. +1) ja etenkin uusimmissa asuinnoissa oli enemmän kalleimpia outliereita. Kaaviosta näemme myös, että ajan kehittyessä myös asuntojen pyyntihinta on kasvanut."
    tekstilaatikko(plottext)
}

harjoitus_2 <- function() {
    message("")
    message("Tehtävä 2")
    message("---------")
    message("Saunojen osuus:")
    saunat <- sum(dataset$sauna == 1, na.rm=TRUE)
    eisaunaa <- sum(dataset$sauna == 2, na.rm=TRUE)
    kaikki <- saunat + eisaunaa
    saunat_pct <- saunat / kaikki * 100.0
    eisaunaa_pct <- eisaunaa / kaikki * 100.0
    message("  Saunojen osuus: ", round(saunat_pct), "%")
    message("  Osuus ilman saunaa: ", round(eisaunaa_pct), "%")
    message("Saunojen osuus asuntotyypeittäin:")
    saunat_freqs_on <- c(sum(dataset$sauna == 1 & dataset$tyyppi == 1, na.rm=TRUE),
                         sum(dataset$sauna == 1 & dataset$tyyppi == 2, na.rm=TRUE),
                         sum(dataset$sauna == 1 & dataset$tyyppi == 3, na.rm=TRUE))
    saunat_freqs_ei <- c(sum(dataset$sauna == 2 & dataset$tyyppi == 1, na.rm=TRUE),
                         sum(dataset$sauna == 2 & dataset$tyyppi == 2, na.rm=TRUE),
                         sum(dataset$sauna == 2 & dataset$tyyppi == 3, na.rm=TRUE))
    saunat_labs = c("Kerrostalo",
                    "Rivitalo/paritalo",
                    "Omakotitalo")
    saunat <- data.frame(Asuntotyyppi=saunat_labs, Kyllä=saunat_freqs_on, Ei=saunat_freqs_ei)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    saunat_exp <- melt(saunat, id=("Asuntotyyppi"))
    names(saunat_exp)[2] <- "Sauna"
    names(saunat_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    saunat_tabs <- xtabs(data=saunat_exp, Lukumäärä ~ Asuntotyyppi + Sauna)
    saunat_summary <- summary(saunat_tabs)
    # Lasketaan khiin neliö
    sarake_nimet <- c("Asuntotyyppi", "Sauna", "Lukumäärä")
    saunat_chisq <- chisq.test(saunat_tabs)
    message("  Khiin neliön p-arvo: ", saunat_chisq$p.value)
    saunat_observed_long <- data.frame(saunat_chisq$observed)
    saunat_expected_long <- data.frame(as.table(saunat_chisq$expected))
    saunat_residuals_long <- data.frame(saunat_chisq$residuals)
    names(saunat_observed_long) <- sarake_nimet
    names(saunat_expected_long) <- sarake_nimet
    names(saunat_residuals_long) <- sarake_nimet
    saunat_observed <- dcast(saunat_observed_long, Asuntotyyppi ~ Sauna, value.var="Lukumäärä")
    saunat_expected <- dcast(saunat_expected_long, Asuntotyyppi ~ Sauna, value.var="Lukumäärä")
    saunat_residuals <- dcast(saunat_residuals_long, Asuntotyyppi ~ Sauna, value.var="Lukumäärä")
    print(saunat_observed)
    print(saunat_expected)
    print(saunat_residuals)
    message("Havainnollistaminen:")
    print(qplot(main="Saunojen esiintyminen asuntotyypeittäin\n(todellisuudessa toteutunut)", data=saunat_exp, stat="identity", position="dodge", geom="bar", x=Asuntotyyppi, y=Lukumäärä, fill=Sauna) + theme_topright())
    print(qplot(main="Saunojen esiintyminen asuntotyypeittäin\n(Khiin neliöllä odotettu)", data=saunat_expected_long, stat="identity", position="dodge", geom="bar", x=Asuntotyyppi, y=Lukumäärä, fill=Sauna) + theme_topright())
    message("Asuntotyyppien välisten erojen tilastollinen merkitsevyys:")
    plottext <- "Tässä esimerkissä saunojen olemassaololla oli erittäin suuri merkitys kaikilla tasoilla (residuaalit jopa +13 ja -7)."
    tekstilaatikko_khiihylkää("2,2 x 10^(-16)", "saunan olemassaolon", "saunojen olemassaololla", plottext)
}

harjoitus_3 <- function() {
    message("")
    message("Tehtävä 3")
    message("---------")
    tyypit_freqs <- c(sum(dataset$tyyppi == 1, na.rm=TRUE),
                      sum(dataset$tyyppi == 2, na.rm=TRUE),
                      sum(dataset$tyyppi == 3, na.rm=TRUE))
    tyypit_labs = c("Kerrostalo",
                    "Rivitalo/paritalo",
                    "Omakotitalo")
    tyypit <- data.frame(Asuntotyyppi=tyypit_labs, Lukumäärä=tyypit_freqs)
    print(qplot(main="Asuntotyyppien osuudet", data=tyypit, stat="identity", geom="bar", x=reorder(Asuntotyyppi,-Lukumäärä), y=Lukumäärä, xlab="Asuntotyyppi"))
    message("Vertaile keskimääräisiä neliöhintoja:")
    asuntotyypit1 <- dataset[dataset$tyyppi == 1,]
    asuntotyypit2 <- dataset[dataset$tyyppi == 2,]
    asuntotyypit3 <- dataset[dataset$tyyppi == 3,]
    means_neliöhinta <- c(mean(asuntotyypit1$neliöhinta, na.rm=TRUE),
                          mean(asuntotyypit2$neliöhinta, na.rm=TRUE),
                          mean(asuntotyypit3$neliöhinta, na.rm=TRUE))
    medians_neliöhinta <- c(median(asuntotyypit1$neliöhinta, na.rm=TRUE),
                            median(asuntotyypit2$neliöhinta, na.rm=TRUE),
                            median(asuntotyypit3$neliöhinta, na.rm=TRUE))
    tyypit_neliöhinta <- data.frame(Asuntotyyppi=tyypit_labs, Keskiarvo=means_neliöhinta, Mediaani=medians_neliöhinta)
    tyypit_neliöhinta_exp <- melt(tyypit_neliöhinta, id=("Asuntotyyppi"))
    names(tyypit_neliöhinta_exp)[2] <- "Menetelmä"
    names(tyypit_neliöhinta_exp)[3] <- "Lopputulos"
    print(qplot(main="Neliöhinta asuntotyypeittäin\n(keskiarvo vs. mediaani)", data=tyypit_neliöhinta_exp, stat="identity", position="dodge", geom="bar", x=Asuntotyyppi, y=Lopputulos, fill=Menetelmä) + theme(legend.position="top"))
    message("Vertaile keskimääräisiä hintoja:")
    means_hinta <- c(mean(asuntotyypit1$hinta, na.rm=TRUE),
                     mean(asuntotyypit2$hinta, na.rm=TRUE),
                     mean(asuntotyypit3$hinta, na.rm=TRUE))
    medians_hinta <- c(median(asuntotyypit1$hinta, na.rm=TRUE),
                       median(asuntotyypit2$hinta, na.rm=TRUE),
                       median(asuntotyypit3$hinta, na.rm=TRUE))
    tyypit_hinta <- data.frame(Asuntotyyppi=tyypit_labs, Keskiarvo=means_hinta, Mediaani=medians_hinta)
    tyypit_hinta_exp <- melt(tyypit_hinta, id=("Asuntotyyppi"))
    names(tyypit_hinta_exp)[2] <- "Menetelmä"
    names(tyypit_hinta_exp)[3] <- "Lopputulos"
    print(qplot(main="Hinta asuntotyypeittäin\n(keskiarvo vs. mediaani)", data=tyypit_hinta_exp, stat="identity", position="dodge", geom="bar", x=Asuntotyyppi, y=Lopputulos, fill=Menetelmä) + theme_topright())
    message("Vertaile keskimääräisiä pinta-aloja:")
    means_neliöt <- c(mean(asuntotyypit1$neliöt, na.rm=TRUE),
                      mean(asuntotyypit2$neliöt, na.rm=TRUE),
                      mean(asuntotyypit3$neliöt, na.rm=TRUE))
    medians_neliöt <- c(median(asuntotyypit1$neliöt, na.rm=TRUE),
                        median(asuntotyypit2$neliöt, na.rm=TRUE),
                        median(asuntotyypit3$neliöt, na.rm=TRUE))
    tyypit_neliöt <- data.frame(Asuntotyyppi=tyypit_labs, Keskiarvo=means_neliöt, Mediaani=medians_neliöt)
    tyypit_neliöt_exp <- melt(tyypit_neliöt, id=("Asuntotyyppi"))
    names(tyypit_neliöt_exp)[2] <- "Menetelmä"
    names(tyypit_neliöt_exp)[3] <- "Lopputulos"
    print(qplot(main="Pinta-ala asuntotyypeittäin\n(keskiarvo vs. mediaani)", data=tyypit_neliöt_exp, stat="identity", position="dodge", geom="bar", x=Asuntotyyppi, y=Lopputulos, fill=Menetelmä) + theme_topright())
    message("Vertaile keskimääräisiä ikiä:")
    asuntotyypit1_no_na <- asuntotyypit1
    asuntotyypit2_no_na <- asuntotyypit2
    asuntotyypit3_no_na <- asuntotyypit3
    asuntotyypit1_no_na[asuntotyypit1_no_na == -1] <- NA
    asuntotyypit2_no_na[asuntotyypit2_no_na == -1] <- NA
    asuntotyypit3_no_na[asuntotyypit3_no_na == -1] <- NA
    means_ikä <- c(mean(asuntotyypit1_no_na$ikä, na.rm=TRUE),
                   mean(asuntotyypit2_no_na$ikä, na.rm=TRUE),
                   mean(asuntotyypit3_no_na$ikä, na.rm=TRUE))
    medians_ikä <- c(median(asuntotyypit1_no_na$ikä, na.rm=TRUE),
                     median(asuntotyypit2_no_na$ikä, na.rm=TRUE),
                     median(asuntotyypit3_no_na$ikä, na.rm=TRUE))
    tyypit_ikä <- data.frame(Asuntotyyppi=tyypit_labs, Keskiarvo=means_ikä, Mediaani=medians_ikä)
    tyypit_ikä_exp <- melt(tyypit_ikä, id=("Asuntotyyppi"))
    names(tyypit_ikä_exp)[2] <- "Menetelmä"
    names(tyypit_ikä_exp)[3] <- "Lopputulos"
    print(qplot(main="Iät asuntotyypeittäin\n(keskiarvo vs. mediaani)", data=tyypit_ikä_exp, stat="identity", position="dodge", geom="bar", x=Asuntotyyppi, y=Lopputulos, fill=Menetelmä) + theme_topright())
    plottext <- "Studentin t-testi on tarkoitettu tilanteeseen, jossa vertaillaan kahden ryhmän välistä eroa: esim. tutkittava lääke testattavilla vs. kontrollilla. T-testillä saisimme tietenkin merkitsevyyden asuntoryhmittäin, mutta tällöin meidän pitäisi vertailla kaikkia tapauksia keskenään. Paremmin tällaiseen vertailuun soveltuu esim. korrelaatiokerroin tai ristiintaulukointi ja khiin neliö. Lisäksi tilannetta hankaloittaa aineiston vinous (keskiarvo eri kuin mediaani). Tyydymme siis arvioimaan tilastollista merkitsevyyttä silmämääräisesti edellisistä kuvaajista, joissa etenkin omakotitalot osoittautuivat kalleimmiksi, tilavimmiksi ja rivitalot/paritalot nuorimmiksi. "
    tekstilaatikko(plottext)
    print(qplot(data=dataset, x=myyvuosi, y=neliöhinta, geom=density_geometry(), method='loess'))
    print(qplot(data=dataset, x=neliöt, y=neliöhinta, geom=density_geometry(), method='loess'))
    print(qplot(data=dataset, x=sauna, y=neliöhinta, geom=density_geometry(), method='gam'))
    print(qplot(data=dataset, x=alue, y=neliöhinta, geom=density_geometry(), method='loess'))
    print(qplot(data=dataset, x=rakvuosi, y=neliöhinta, geom=density_geometry(), method='loess'))
    print(qplot(data=dataset, x=hinta, y=neliöhinta, geom=density_geometry(), method='loess'))
    print(qplot(data=dataset, x=ikä, y=neliöhinta, geom=density_geometry(), method='loess'))
    plottext <- "Hintaeroihin löytyy selittäviksi tekijöiksi myös asunnon rakennusvuosi (uudempi kalliimpi), neliöt (suurempi halvempi), saunallisuus (saunalla kalliimpi), alueiden väliset erot (vaihteluita), rakennusvuosi (uudempi kalliimpi), asunnon hinta (kalliimmalla kalliimpi neliöhinta) ja asunnon ikä (laskeva 50v asti, nouseva 50v+)."
    tekstilaatikko(plottext)
}

harjoitus_4 <- function() {
    message("")
    message("Tehtävä 4")
    message("---------")
    neliöhinta_vs_sauna_on <- data.frame(Neliöhinta=dataset$neliöhinta, Sauna=dataset$sauna)
    neliöhinta_vs_sauna_ei <- data.frame(Neliöhinta=dataset$neliöhinta, Sauna=dataset$sauna)
    neliöhinta_vs_sauna_on$Sauna[neliöhinta_vs_sauna_on$Sauna != 1] <- NA
    neliöhinta_vs_sauna_ei$Sauna[neliöhinta_vs_sauna_ei$Sauna != 2] <- NA
    neliöhinta_vs_sauna_on <- na.omit(neliöhinta_vs_sauna_on)
    neliöhinta_vs_sauna_ei <- na.omit(neliöhinta_vs_sauna_ei)
    # Testataan F-testillä ensin varianssit:
    # "Before proceeding with the t-test, it is necessary to evaluate the sample variances of the
    # two groups, using a Fisher’s F-test to verify the homoskedasticity (homogeneity of variances)."
    # https://en.wikipedia.org/wiki/Homoscedasticity
    ftest <- var.test(neliöhinta_vs_sauna_on$Neliöhinta, neliöhinta_vs_sauna_ei$Neliöhinta)$p.value
    message("  F-testin p-arvo: ", ftest)
    # p-arvoksi tuli 9.442e-12:
    # "To solve this problem we must use to a Student’s t-test with two samples, assuming that the
    # two samples are taken from populations that follow a Gaussian distribution (if we cannot
    # assume that, we must solve this problem using the non-parametric test called Wilcoxon-Mann-
    # Whitney test; we will see this test in a future post)."
    wmhtest <- wilcox.test(neliöhinta_vs_sauna_on$Neliöhinta, neliöhinta_vs_sauna_ei$Neliöhinta)$p.value
    message("  Wilcoxonin p-arvo: ", wmhtest)
    ttest <- t.test(neliöhinta_vs_sauna_on$Neliöhinta, neliöhinta_vs_sauna_ei$Neliöhinta)$p.value
    message("  Studentin t-testin p-arvo: ", ttest)
    message("Havainnollistaminen:")
    tiheys <- rbind(data.frame(Tyyppi="Saunallinen", Neliöhinta=neliöhinta_vs_sauna_on$Neliöhinta),
                    data.frame(Tyyppi="Saunaton", Neliöhinta=neliöhinta_vs_sauna_ei$Neliöhinta))
    print(qplot(main="Tiheysfunktio", data=tiheys, Neliöhinta, ylab="Tiheys", geom="density", color=Tyyppi, fill=Tyyppi, alpha=I(0.2)) + theme_topright())
    plottext <- "Studentin t-testillä saimme p-arvoksi 2.364 x 10^(-45). Tämä tulos (< 0,05) tarkoittaa, että saunallisten ja ei-saunallisten asuntojen neliöhinnoista löytyi tilastollisesti merkitsevä ero."
    tekstilaatikko(plottext)
}

harjoitus_5 <- function() {
    message("")
    message("Tehtävä 5")
    message("---------")
    neliöhinta_vs_koko_iso <- data.frame(Neliöhinta=dataset$neliöhinta, Koko=dataset$neliöt)
    neliöhinta_vs_koko_pieni <- data.frame(Neliöhinta=dataset$neliöhinta, Koko=dataset$neliöt)
    neliöhinta_vs_koko_iso$Koko[neliöhinta_vs_koko_iso$Koko < 75.0] <- NA
    neliöhinta_vs_koko_pieni$Koko[neliöhinta_vs_koko_pieni$Koko >= 75.0] <- NA
    neliöhinta_vs_koko_iso <- na.omit(neliöhinta_vs_koko_iso)
    neliöhinta_vs_koko_pieni <- na.omit(neliöhinta_vs_koko_pieni)
    ttest <- t.test(neliöhinta_vs_koko_iso$Neliöhinta, neliöhinta_vs_koko_pieni$Neliöhinta)$p.value
    message("Studentin t-testin p-arvo: ", ttest)
    message("Havainnollistaminen:")
    tiheys <- rbind(data.frame(Tyyppi="Iso", Neliöhinta=neliöhinta_vs_koko_iso$Neliöhinta),
                    data.frame(Tyyppi="Pieni", Neliöhinta=neliöhinta_vs_koko_pieni$Neliöhinta))
    print(qplot(main="Tiheysfunktio", data=tiheys, Neliöhinta, ylab="Tiheys", geom="density", color=Tyyppi, fill=Tyyppi, alpha=I(0.2)) + theme_topright())
    plottext <- "Studentin t-testillä saimme p-arvoksi 0,00007825. Tämä tulos (< 0,05) tarkoittaa, että isojen ja pienten asuntojen neliöhinnoista löytyi tilastollisesti merkitsevä ero."
    tekstilaatikko(plottext)
}

harjoitus_6 <- function() {
    message("")
    message("Tehtävä 6")
    message("---------")
    myyntivuosi_uudet <- dataset[dataset$myyvuosi >= 2005 & dataset$myyvuosi <= 2007,]
    myyntivuosi_vanhat <- dataset[dataset$myyvuosi >= 1999 & dataset$myyvuosi <= 2000,]
    message("Keskimääräiset neliöhinnat:")
    neliöhinnat_uudet <- myyntivuosi_uudet$neliöhinta
    neliöhinnat_vanhat <- myyntivuosi_vanhat$neliöhinta
    neliöhinnat_uudet <- na.omit(neliöhinnat_uudet)
    neliöhinnat_vanhat <- na.omit(neliöhinnat_vanhat)
    ttest <- t.test(neliöhinnat_uudet, neliöhinnat_vanhat)$p.value
    message("  Studentin t-testin p-arvo: ", ttest)
    tiheys <- rbind(data.frame(Tyyppi="Uudet", Neliöhinta=neliöhinnat_uudet),
                    data.frame(Tyyppi="Vanhat", Neliöhinta=neliöhinnat_vanhat))
    print(qplot(main="Neliöhintojen tiheysfunktio", data=tiheys, Neliöhinta, ylab="Tiheys", geom="density", color=Tyyppi, fill=Tyyppi, alpha=I(0.2)) + theme_topright())
    message("Keskimääräiset pyyntihinnat:")
    hinnat_uudet <- myyntivuosi_uudet$hinta
    hinnat_vanhat <- myyntivuosi_vanhat$hinta
    hinnat_uudet <- na.omit(hinnat_uudet)
    hinnat_vanhat <- na.omit(hinnat_vanhat)
    ttest <- t.test(hinnat_uudet, hinnat_vanhat)$p.value
    message("  Studentin t-testin p-arvo: ", ttest)
    tiheys <- rbind(data.frame(Tyyppi="Uudet", Pyyntihinta=hinnat_uudet),
                    data.frame(Tyyppi="Vanhat", Pyyntihinta=hinnat_vanhat))
    print(qplot(main="Pyyntihintojen tiheysfunktio", data=tiheys, Pyyntihinta, ylab="Tiheys", geom="density", color=Tyyppi, fill=Tyyppi, alpha=I(0.2)) + theme_topright())
    message("Keskimääräiset pinta-alat:")
    neliöt_uudet <- myyntivuosi_uudet$neliöt
    neliöt_vanhat <- myyntivuosi_vanhat$neliöt
    neliöt_uudet <- na.omit(neliöt_uudet)
    neliöt_vanhat <- na.omit(neliöt_vanhat)
    ttest <- t.test(neliöt_uudet, neliöt_vanhat)$p.value
    message("  Studentin t-testin p-arvo: ", ttest)
    tiheys <- rbind(data.frame(Tyyppi="Uudet", Neliöt=neliöt_uudet),
                    data.frame(Tyyppi="Vanhat", Neliöt=neliöt_vanhat))
    print(qplot(main="Pinta-alojen tiheysfunktio", data=tiheys, Neliöt, ylab="Tiheys", geom="density", color=Tyyppi, fill=Tyyppi, alpha=I(0.2)) + theme_topright())
    message("Keskimääräiset asuntojen iät:")
    iät_uudet <- myyntivuosi_uudet$ikä
    iät_vanhat <- myyntivuosi_vanhat$ikä
    iät_uudet[iät_uudet == -1] <- NA
    iät_vanhat[iät_vanhat == -1] <- NA
    iät_uudet <- na.omit(iät_uudet)
    iät_vanhat <- na.omit(iät_vanhat)
    ttest <- t.test(iät_uudet, iät_vanhat)$p.value
    message("  Studentin t-testin p-arvo: ", ttest)
    tiheys <- rbind(data.frame(Tyyppi="Uudet", Iät=iät_uudet),
                    data.frame(Tyyppi="Vanhat", Iät=iät_vanhat))
    print(qplot(main="Ikien tiheysfunktio", data=tiheys, Iät, ylab="Tiheys", geom="density", color=Tyyppi, fill=Tyyppi, alpha=I(0.2)) + theme_topright())
    plottext <- "Studentin t-testillä saimme kaikissa tilastollisesti merkitsevän (< 0,05) eron seuraavissa tilanteissa: neliöhinnat, pyyntihinnat ja pinta-alat. Kuitenkin asuntojen ikien kohdalla p-arvoksi muodostui 0,746 tarkoittaen, että asuntojen iällä ei ollut tilastollisesti merkitsevää eroa näiden kahden vertailuajankohdan välillä."
    tekstilaatikko(plottext)
    message("Neliöhintojen prosentuaalinen nousu:")
    neliöhinnat_uudet_mean <- mean(neliöhinnat_uudet)
    neliöhinnat_vanhat_mean <- mean(neliöhinnat_vanhat)
    nousu_pct <- (neliöhinnat_uudet_mean - neliöhinnat_vanhat_mean) / neliöhinnat_vanhat_mean * 100.0
    message("  Vastaus: ", round(nousu_pct), "%")
}

harjoitus_7 <- function() {
    message("")
    message("Tehtävä 7")
    message("---------")
    message("Kolmogorov-Smirnov ja asuntojen pinta-alat")
    samplet <- 1000000
    neliöt <- dataset$neliöt
    neliöt <- na.omit(neliöt)
    neliöt <- unique(neliöt)  # Tärkeä, sillä ks.test vaatii unique():n, josta taas luomme myös mean()/sd():n
    kstest <- ks.test(neliöt, pnorm, mean(neliöt), sd(neliöt))$p.value
    message("  Kolmogorov-Smirnov-testin p-arvo: ", kstest)
    neliöt_tuot <- rnorm(samplet, mean(neliöt), sd(neliöt))
    neliöt_jaot <- rbind(data.frame(Funktio="Toteutunut", Neliöt=neliöt),
                         data.frame(Funktio="Tuotettu", Neliöt=neliöt_tuot))
    leveys <- (max(neliöt) - min(neliöt)) / 60
    print(qplot(main="Pinta-alojen histogrammi", neliöt, xlab="Neliöt", ylab="Lukumäärä", geom="histogram", binwidth=leveys, color=..count.., fill=..count..) + theme_topright())
    plottext <- "Tässä esimerkissä pinta-alojen kuvaaja poikkesi vähiten tasaisesti jaetusta kuvaajasta yhteensä kolmesta mitatusta muuttujasta. Tässäkään tapauksessa ei kuitenkaan ylitetty p-arvon alpha 0,05:ttä eli tämäkin kuvaaja havaittiin hajonnaltaan erilaiseksi."
    tekstilaatikko_kolmogorov("0,03791", samplet, plottext)
    print(qplot(main="Pinta-alojen tiheysfunktio", data=neliöt_jaot, Neliöt, ylab="Tiheys", geom="density", color=Funktio, fill=Funktio, alpha=I(0.2)) + theme_topright())
    message("Kolmogorov-Smirnov ja asuntojen hinnat")
    hinnat <- dataset$hinta
    hinnat <- na.omit(hinnat)
    hinnat <- unique(hinnat)  # Tärkeä, sillä ks.test vaatii unique():n, josta taas luomme myös mean()/sd():n
    kstest <- ks.test(unique(hinnat), "pnorm", mean(hinnat), sd(hinnat))$p.value
    message("  Kolmogorov-Smirnov-testin p-arvo: ", kstest)
    hinnat_tuot <- rnorm(samplet, mean(hinnat), sd(hinnat))
    hinnat_jaot <- rbind(data.frame(Funktio="Toteutunut", Hinnat=hinnat),
                         data.frame(Funktio="Tuotettu", Hinnat=hinnat_tuot))
    leveys <- (max(hinnat) - min(hinnat)) / 60
    print(qplot(main="Hintojen histogrammi", hinnat, xlab="Hinnat", ylab="Lukumäärä", geom="histogram", binwidth=leveys, color=..count.., fill=..count..) + theme_topright())
    plottext <- "Tässä esimerkissä hintojen kuvaaja poikkesi eniten tasaisesti jaetusta kuvaajasta yhteensä kolmesta mitatusta muuttujasta."
    tekstilaatikko_kolmogorov("5,032 x 10^(-7)", samplet, plottext)
    print(qplot(main="Hintojen tiheysfunktio", data=hinnat_jaot, Hinnat, ylab="Tiheys", geom="density", color=Funktio, fill=Funktio, alpha=I(0.2)) + theme_topright())
    message("Kolmogorov-Smirnov ja asuntojen neliöhinnat")
    neliöhinnat <- dataset$neliöhinta
    neliöhinnat <- na.omit(neliöhinnat)
    neliöhinnat <- unique(neliöhinnat)  # Tärkeä, sillä ks.test vaatii unique():n, josta taas luomme myös mean()/sd():n
    kstest <- ks.test(unique(neliöhinnat), "pnorm", mean(neliöhinnat), sd(neliöhinnat))$p.value
    message("  Kolmogorov-Smirnov-testin p-arvo: ", kstest)
    neliöhinnat_tuot <- rnorm(samplet, mean(neliöhinnat), sd(neliöhinnat))
    neliöhinnat_jaot <- rbind(data.frame(Funktio="Toteutunut", Neliöhinnat=neliöhinnat),
                              data.frame(Funktio="Tuotettu", Neliöhinnat=neliöhinnat_tuot))
    leveys <- (max(neliöhinnat) - min(neliöhinnat)) / 60
    print(qplot(main="Neliöhintojen histogrammi", neliöhinnat, xlab="Neliöhinnat", ylab="Lukumäärä", geom="histogram", binwidth=leveys, color=..count.., fill=..count..) + theme_topright())
    plottext <- "Tässä esimerkissä hintojen kuvaaja poikkesi toiseksi eniten tasaisesti jaetusta kuvaajasta yhteensä kolmesta mitatusta muuttujasta."
    tekstilaatikko_kolmogorov("0,00211", samplet, plottext)
    print(qplot(main="Neliöhintojen tiheysfunktio", data=neliöhinnat_jaot, Neliöhinnat, ylab="Tiheys", geom="density", color=Funktio, fill=Funktio, alpha=I(0.2)) + theme_topright())
}

harjoitus_8 <- function() {
    message("")
    message("Tehtävä 8")
    message("---------")
    message("Mann-Whitney U ja saunalliset/saunattomat:")
    neliöhinnat_sauna_on <- data.frame(Neliöhinta=dataset$neliöhinta, Sauna=dataset$sauna)
    neliöhinnat_sauna_ei <- data.frame(Neliöhinta=dataset$neliöhinta, Sauna=dataset$sauna)
    neliöhinnat_sauna_on$Sauna[neliöhinnat_sauna_on$Sauna == 2] <- NA
    neliöhinnat_sauna_ei$Sauna[neliöhinnat_sauna_ei$Sauna == 1] <- NA
    neliöhinnat_sauna_on <- na.omit(neliöhinnat_sauna_on)
    neliöhinnat_sauna_ei <- na.omit(neliöhinnat_sauna_ei)
    neliöhinnat <- rbind(data.frame(Sauna="Saunallinen", Neliöhinta=neliöhinnat_sauna_on$Neliöhinta),
                         data.frame(Sauna="Saunaton", Neliöhinta=neliöhinnat_sauna_ei$Neliöhinta))
    print(qplot(main="Saunallisuuden tiheysfunktio", data=neliöhinnat, Neliöhinta, ylab="Tiheys", geom="density", color=Sauna, fill=Sauna, alpha=I(0.2)) + theme_topright())
    # Tässä testissä joudumme poistamaan duplikaatit: sidos on tietenkin molemminpuoleinen,
    # siksi käytämme vain kahden arvon data framea
    mwutest_data <- data.frame(Neliöhinta=dataset$neliöhinta, Sauna=dataset$sauna)
    mwutest_data <- na.omit(mwutest_data)
    mwutest_data <- unique(mwutest_data)
    mwutest <- wilcox.test(data=dataset, neliöhinta ~ sauna, na.rm=TRUE)$p.value
    message("  Mann-Whitney U -testin p-arvo: ", mwutest)
    plottext <- "Mann-Whitney U -testillä saimme p-arvoksi 3,414 x 10^(-36). Tämän testin p-arvon tulkinta on kuten t-testillä eli näiden kahden ryhmän väliltä löytyi tilastollisesti merkitsevä ero. Tämä testi on kuitenkin suunniteltu tilanteeseen, jossa toinen tutkittavista muuttujista on ranked ordinal (meidän tapauksessamme binäärinen saunan olemassaolo)."
    tekstilaatikko(plottext)
}

harjoitus_9 <- function() {
    # Kolme+ riippumattoman muuttujan ei-parametrisiin testeishin suositellaan Kruskal-Wallisia
    # http://sphweb.bumc.bu.edu/otlt/MPH-Modules/BS/BS704_Nonparametric/BS704_Nonparametric7.html
    message("")
    message("Tehtävä 9")
    message("---------")
    message("Asuntotyypit vs. hinnat:")
    hinnat_tyyppi1 <- dataset$hinta[dataset$tyyppi == 1]
    hinnat_tyyppi2 <- dataset$hinta[dataset$tyyppi == 2]
    hinnat_tyyppi3 <- dataset$hinta[dataset$tyyppi == 3]
    hinnat_tyyppi1 <- na.omit(hinnat_tyyppi1)
    hinnat_tyyppi2 <- na.omit(hinnat_tyyppi2)
    hinnat_tyyppi3 <- na.omit(hinnat_tyyppi3)
    hinnat <- rbind(data.frame(Asuntotyyppi="Kerrostalo", Hinta=hinnat_tyyppi1),
                    data.frame(Asuntotyyppi="Rivitalo/paritalo", Hinta=hinnat_tyyppi2),
                    data.frame(Asuntotyyppi="Omakotitalo", Hinta=hinnat_tyyppi3))
    print(qplot(main="Asuntotyyppi vs. hinta tiheysfunktio", data=hinnat, Hinta, ylab="Tiheys", geom="density", color=Asuntotyyppi, fill=Asuntotyyppi, alpha=I(0.2)) + theme_topright())
    kwtest <- kruskal.test(dataset$hinta, dataset$tyyppi, na.rm=TRUE)$p.value
    message("  Kruskal-Wallis-testin p-arvo: ", kwtest)
    plottext <- "Tässä esimerkissä tutkitut kolme jakaumaa poikkesivat toisistaan toiseksi eniten."
    tekstilaatikko_kruskal("4,84 x 10^(-102)", plottext)
    message("Asuntotyypit vs. neliöhinnat:")
    neliöhinnat_tyyppi1 <- dataset$neliöhinta[dataset$tyyppi == 1]
    neliöhinnat_tyyppi2 <- dataset$neliöhinta[dataset$tyyppi == 2]
    neliöhinnat_tyyppi3 <- dataset$neliöhinta[dataset$tyyppi == 3]
    neliöhinnat_tyyppi1 <- na.omit(neliöhinnat_tyyppi1)
    neliöhinnat_tyyppi2 <- na.omit(neliöhinnat_tyyppi2)
    neliöhinnat_tyyppi3 <- na.omit(neliöhinnat_tyyppi3)
    neliöhinnat <- rbind(data.frame(Asuntotyyppi="Kerrostalo", Neliöhinta=neliöhinnat_tyyppi1),
                         data.frame(Asuntotyyppi="Rivitalo/paritalo", Neliöhinta=neliöhinnat_tyyppi2),
                         data.frame(Asuntotyyppi="Omakotitalo", Neliöhinta=neliöhinnat_tyyppi3))
    print(qplot(main="Asuntotyyppi vs. neliöhinta tiheysfunktio", data=neliöhinnat, Neliöhinta, ylab="Tiheys", geom="density", color=Asuntotyyppi, fill=Asuntotyyppi, alpha=I(0.2)) + theme_topright())
    kwtest <- kruskal.test(dataset$neliöhinta, dataset$tyyppi, na.rm=TRUE)$p.value
    message("  Kruskal-Wallis-testin p-arvo: ", kwtest)
    plottext <- "Tässä esimerkissä tutkitut kolme jakaumaa poikkesivat toisistaan vähiten."
    tekstilaatikko_kruskal("9,68 x 10^(-8)", plottext)
    message("Asuntotyypit vs. pinta-alat:")
    neliöt_tyyppi1 <- dataset$neliöt[dataset$tyyppi == 1]
    neliöt_tyyppi2 <- dataset$neliöt[dataset$tyyppi == 2]
    neliöt_tyyppi3 <- dataset$neliöt[dataset$tyyppi == 3]
    neliöt_tyyppi1 <- na.omit(neliöt_tyyppi1)
    neliöt_tyyppi2 <- na.omit(neliöt_tyyppi2)
    neliöt_tyyppi3 <- na.omit(neliöt_tyyppi3)
    neliöt <- rbind(data.frame(Asuntotyyppi="Kerrostalo", Neliöt=neliöt_tyyppi1),
                    data.frame(Asuntotyyppi="Rivitalo/paritalo", Neliöt=neliöt_tyyppi2),
                    data.frame(Asuntotyyppi="Omakotitalo", Neliöt=neliöt_tyyppi3))
    print(qplot(main="Asuntotyyppi vs. pinta-ala tiheysfunktio", data=neliöt, Neliöt, ylab="Tiheys", geom="density", color=Asuntotyyppi, fill=Asuntotyyppi, alpha=I(0.2)) + theme_topright())
    kwtest <- kruskal.test(dataset$neliöt, dataset$tyyppi, na.rm=TRUE)$p.value
    message("  Kruskal-Wallis-testin p-arvo: ", kwtest)
    plottext <- "Tässä esimerkissä tutkitut kolme jakaumaa poikkesivat toisistaan eniten."
    tekstilaatikko_kruskal("1.27 x 10^(-129)", plottext)
}

pdf("harjoituksia3.pdf", paper="default")
dataset <- harjoitus_alustus()
harjoitus_1()
harjoitus_2()
harjoitus_3()
harjoitus_4()
harjoitus_5()
harjoitus_6()
harjoitus_7()
harjoitus_8()
harjoitus_9()
dev.off()
