males <- c(314, 419, 454)
females <- c(458, 567, 479)
categories <- c("Liberal", "Moderate", "Conservative")
polviews <- data.frame(Polviews=categories, Male=males, Female=females)
message("Original table")
message("--------------")
polviews

message("")
message("Expanded table")
message("--------------")
require(reshape2)
polviews_exp <- melt(polviews, id=c("Polviews"))
names(polviews_exp)[2] <- "Gender"
names(polviews_exp)[3] <- "Amount"
polviews_exp

message("")
message("Cross-tabulation")
message("----------------")
polviews_tabs <- xtabs(polviews_exp$Amount ~ polviews_exp$Polviews + polviews_exp$Gender)

message("")
message("Cross-tabulation summary")
message("------------------------")
polviews_summary <- summary(polviews_tabs)
polviews_summary

polviews_chisq <- chisq.test(polviews_tabs)
polviews_observed <- polviews_chisq$observed
polviews_expected <- polviews_chisq$expected
polviews_residuals <- polviews_chisq$residuals
message("")
message("Cross-tabulation (observed)")
message("---------------------------")
polviews_observed
message("")
message("Cross-tabulation (expected)")
message("---------------------------")
polviews_expected
message("")
message("Cross-tabulation (Residuals)")
message("----------------------------")
polviews_residuals

