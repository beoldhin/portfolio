require(moments)
require(reshape2)
require(plotrix)

getmode <- function(v) {
    uniqv <- unique(v)
    uniqv[which.max(tabulate(match(v, uniqv)))]
}

tekstilaatikko <- function(teksti) {
    plot.new()
    textbox(c(0,1.0), 1, margin=0.02, teksti)
}

tekstilaatikko_khiihyväksy <- function(pvalue, muut1, muut2, teksti) {
    khii_hyväksy <- "Khiin neliö -menetelmällä havaittiin, että p-arvo on %s. Mikäli p-arvon rajana pidetään arvoa 0,05 tai jopa 0,01, tarkoittaisi tämä, että alle näiden arvojen menevä tulos johtaisi null-hypoteesin hylkäämiseen. Tässä tapauksessa tuli siis ilmi, että emme kyenneet hylkäämään null-hypoteesia, joka tarkoitti ettei %s välillä ole eroja. Khiin neliön expected-taulusta näemme arvot, jotka olisivat totta mikäli %s ei olisi vaikutusta. %s"
    teksti <- sprintf(khii_hyväksy, pvalue, muut1, muut2, teksti)
    tekstilaatikko(teksti)
}

tekstilaatikko_khiihylkää <- function(pvalue, muut1, muut2, teksti) {
    khii_hylkää <- "Khiin neliö -menetelmällä havaittiin, että p-arvo on %s. Mikäli p-arvon rajana pidetään arvoa 0,05 tai jopa 0,01, tarkoittaisi tämä, että alle näiden arvojen menevä tulos johtaisi null-hypoteesin hylkäämiseen. Tässä tapauksessa tuli siis ilmi, että jouduimme hylkäämään null-hypoteesin, joka tarkoitti ettei %s välillä ole eroja. Khiin neliön expected-taulusta näemme arvot, jotka olisivat totta mikäli %s ei olisi vaikutusta. %s"
    teksti <- sprintf(khii_hylkää, pvalue, muut1, muut2, teksti)
    tekstilaatikko(teksti)
}

tekstilaatikko_spearmanhyväksy <- function(pvalue, muut, teksti) {
    spearman_hyväksy <- "Spearmanin korrelaatiokertoimella havaittiin, että p-arvo on %s. Mikäli p-arvon rajana pidetään arvoa 0,05 tai jopa 0,01, tarkoittaisi tämä, että alle näiden arvojen menevä tulos johtaisi null-hypoteesin hylkäämiseen. Tässä tapauksessa tuli siis ilmi, että emme kyenneet hylkäämään null-hypoteesia, joka tarkoitti ettei %s välillä ole merkittävää korrelaatiota. %s"
    teksti <- sprintf(spearman_hyväksy, pvalue, muut, teksti)
    tekstilaatikko(teksti)
}

tekstilaatikko_spearmanhylkää <- function(pvalue, muut, teksti) {
    spearman_hylkää <- "Spearmanin korrelaatiokertoimella havaittiin, että p-arvo on %s. Mikäli p-arvon rajana pidetään arvoa 0,05 tai jopa 0,01, tarkoittaisi tämä, että alle näiden arvojen menevä tulos johtaisi null-hypoteesin hylkäämiseen. Tässä tapauksessa tuli siis ilmi, että jouduimme hylkäämään null-hypoteesin, joka tarkoitti ettei %s välillä ole merkittävää korrelaatiota. %s"
    teksti <- sprintf(spearman_hylkää, pvalue, muut, teksti)
    tekstilaatikko(teksti)
}

tunnusluvut <- function(data) {
    message("Mediaani, keskiarvo, kvartiilit:")
    yhteenveto <- summary(data)
    print(yhteenveto)
    message("Moodi (mode):")
    moodi <- getmode(data)
    print(moodi)
    message("Vaihteluvälit (range):")
    vaihteluvälit <- range(data, na.rm=TRUE)
    print(vaihteluvälit)
    message("Keskihajonta (standard deviation):")
    keskihajonta <- sd(data, na.rm=TRUE)
    print(keskihajonta)
    message("Varianssi (keskihajonnan neliö, variance):")
    varianssi <- var(data, na.rm=TRUE)
    print(varianssi)
    message("Vinousmitta (skewness):")
    vinous <- skewness(data, na.rm=TRUE)
    print(vinous)
    message("Huipukkuus (kurtosis):")
    huipukkuus <- kurtosis(data, na.rm=TRUE)
    print(huipukkuus)
}

harjoitus_alustus <- function() {
    dataset <- read.csv(file="Pytinki2003.csv", sep=',', dec='.')
    # message("Datatyypit")
    # message("----------")
    # sapply(dataset, class)
    return(dataset)
}

harjoitus_2_1 <- function() {
    message("")
    message("Tehtävä 2, alatehtävä 1")
    message("-----------------------")
    naiset <- sum(dataset$k01sukup == 1, na.rm=TRUE)
    miehet <- sum(dataset$k01sukup == 2, na.rm=TRUE)
    kaikki <- naiset + miehet
    naiset_pct <- naiset / kaikki * 100.0
    miehet_pct <- miehet / kaikki * 100.0
    message("Naisten osuus: ", round(naiset_pct), "%")
    message("Miesten osuus: ", round(miehet_pct), "%")
}

harjoitus_2_2 <- function() {
    message("")
    message("Tehtävä 2, alatehtävä 2")
    message("-----------------------")
    ikä_freqs_n <- c(sum(dataset$k02ikä == 1 & dataset$k01sukup == 1, na.rm=TRUE),
                     sum(dataset$k02ikä == 2 & dataset$k01sukup == 1, na.rm=TRUE),
                     sum(dataset$k02ikä == 3 & dataset$k01sukup == 1, na.rm=TRUE),
                     sum(dataset$k02ikä == 4 & dataset$k01sukup == 1, na.rm=TRUE))
    ikä_freqs_m <- c(sum(dataset$k02ikä == 1 & dataset$k01sukup == 2, na.rm=TRUE),
                     sum(dataset$k02ikä == 2 & dataset$k01sukup == 2, na.rm=TRUE),
                     sum(dataset$k02ikä == 3 & dataset$k01sukup == 2, na.rm=TRUE),
                     sum(dataset$k02ikä == 4 & dataset$k01sukup == 2, na.rm=TRUE))
    ikä_pct_n <- ikä_freqs_n
    ikä_pct_m <- ikä_freqs_m
    kaikki <- sum(ikä_freqs_n) + sum(ikä_freqs_m)
    for (i in 1:4) {
        ikä_pct_n[i] <- ikä_pct_n[i] / kaikki * 100.0
        ikä_pct_m[i] <- ikä_pct_m[i] / kaikki * 100.0
    }
    ikä_labs <- c("0-20",
                  "20-35",
                  "36-49",
                  "50+")
    mcol<-color.gradient(c(0,0,0.5,1),c(0,0,0.5,1),c(1,1,0.5,1),4)
    fcol<-color.gradient(c(1,1,0.5,1),c(0.5,0.5,0.5,1),c(0.5,0.5,0.5,1),4)
    par(mar=pyramid.plot(ikä_pct_m, ikä_pct_n, labels=ikä_labs,
                         main="Ikäjakaumat", top.labels=c("Miehet","Ikä","Naiset"),
                         lxcol=mcol, rxcol=fcol,
                         gap=3, show.values=TRUE))
}

harjoitus_2_3 <- function() {
    message("")
    message("Tehtävä 2, alatehtävä 3")
    message("-----------------------")
    asuinp_freqs <- c(sum(dataset$k03asump == 1, na.rm=TRUE),
                      sum(dataset$k03asump == 2, na.rm=TRUE),
                      sum(dataset$k03asump == 3, na.rm=TRUE),
                      sum(dataset$k03asump == 4, na.rm=TRUE))
    asuinp_labs <- c("Seinäjoki",
                     "Seinänaapurikunta",
                     "Muu kunta Etelä-pohjanmaalla",
                     "Muu Suomi")
    pie(main="Asuinalueet", asuinp_freqs, labels=asuinp_labs)
}

harjoitus_2_4 <- function() {
    message("")
    message("Tehtävä 2, alatehtävä 4")
    message("-----------------------")
    asemat_freqs <- c(sum(dataset$k04asema == 1, na.rm=TRUE),
                      sum(dataset$k04asema == 2, na.rm=TRUE),
                      sum(dataset$k04asema == 3, na.rm=TRUE),
                      sum(dataset$k04asema == 4, na.rm=TRUE),
                      sum(dataset$k04asema == 5, na.rm=TRUE),
                      sum(dataset$k04asema == 6, na.rm=TRUE))
    asemat_labs <- c("Opiskelija",
                     "Työntekijä",
                     "Toimihenkilö",
                     "Johtavassa\nasemassa",
                     "Yrittäjä",
                     "Ei\ntyöelämässä")
    asemat_värit <- c("grey", "red", "red", "red", "red", "grey")
    asemat <- data.frame(Asema=asemat_labs, Lukumäärä=asemat_freqs, Värit=asemat_värit)
    asemat <- asemat[order(-asemat$Lukumäärä),]
    barplot(main="Asema työelämässä", asemat$Lukumäärä, names.arg=asemat$Asema, col=asemat$Värit, horiz=FALSE, cex.names=.8)
}

harjoitus_2_5 <- function() {
    message("")
    message("Tehtävä 2, alatehtävä 5")
    message("-----------------------")
    yksityis <- sum(dataset$k05miksi == 1, na.rm=TRUE)
    edustaja <- sum(dataset$k05miksi == 2, na.rm=TRUE)
    kaikki <- yksityis + edustaja
    yksityis_pct <- yksityis / kaikki * 100.0
    edustaja_pct <- edustaja / kaikki * 100.0
    message("Yksityishenkilönä: ", round(yksityis_pct,0), "%")
    message("Yrityksen edustajana: ", round(edustaja_pct,0), "%")
}

harjoitus_2_6 <- function() {
    message("")
    message("Tehtävä 2, alatehtävä 6")
    message("-----------------------")
    yksityis <- sum(dataset$k05miksi == 1, na.rm=TRUE)
    edustaja <- sum(dataset$k05miksi == 2, na.rm=TRUE)
    etapa_freqs <- c(yksityis, edustaja)
    etapa_labs <- c("Yksityishenkilönä", "Yrityksen\nedustajana")
    etapa_värit <- c("red", "grey")
    pie(main="Edustustapa", etapa_freqs, labels=etapa_labs, col=etapa_värit)
}

harjoitus_3_k08aihep <- function() {
    message("Kiinnostavimmat aihepiirit:")
    kiiaihep_freqs <- c(sum(dataset$k08aihep == 1, na.rm=TRUE),
                        sum(dataset$k08aihep == 2, na.rm=TRUE),
                        sum(dataset$k08aihep == 3, na.rm=TRUE),
                        sum(dataset$k08aihep == 4, na.rm=TRUE),
                        sum(dataset$k08aihep == 5, na.rm=TRUE),
                        sum(dataset$k08aihep == 6, na.rm=TRUE))
    kiiaihep_labs <- c("Vanhan\nkorjaaminen",
                       "Uuden\nrakentaminen",
                       "Sisustaminen",
                       "Piharakentaminen",
                       "Vapaa-ajan\nrakentaminen",
                       "LVIS-ratkaisut")
    kiiaihep <- data.frame(Kiinnostavimmat=kiiaihep_labs, Lukumäärä=kiiaihep_freqs)
    kiiaihep <- kiiaihep[order(-kiiaihep$Lukumäärä),]
    kiiaihep_värit <- c("red", "red", "red", "grey", "grey", "grey")
    barplot(main="Kiinnostavimmat aihepiirit", kiiaihep$Lukumäärä, names.arg=kiiaihep$Kiinnostavimmat, col=kiiaihep_värit, horiz=FALSE, cex.names=.7)
    plottext <- "Kiinnostavimmat aihepiirit olivat ylivoimaisesti vanhan korjaaminen, sisustus ja uuden rakentaminen. Tilanne selviää myös seuraavasta piirakkakaaviosta."
    tekstilaatikko(plottext)
    message("Kiinnostavimmat aihepiirit (jatk.):")
    kiiaihep_ryhmät_freqs <- c(sum(dataset$k08aihep == 1, na.rm=TRUE) +
                               sum(dataset$k08aihep == 3, na.rm=TRUE) +
                               sum(dataset$k08aihep == 2, na.rm=TRUE),
                               sum(dataset$k08aihep == 5, na.rm=TRUE) +
                               sum(dataset$k08aihep == 4, na.rm=TRUE) +
                               sum(dataset$k08aihep == 6, na.rm=TRUE))
    kiiaihep_ryhmät_labs <- c("Ryhmä A", "Ryhmä B")
    kiiaihep_ryhmät_värit <- c("red", "grey")
    pie(main="Kiinnostavimmat aihepiirit (jatk.)", kiiaihep_ryhmät_freqs, labels=kiiaihep_ryhmät_labs, col=kiiaihep_ryhmät_värit)
    plottext <- sprintf("Tässä kaaviossa Ryhmä A tarkoittaa vanhan korjaamista, sisustusta ja uuden rakentamista (N=%i) Ryhmä B tarkoittaa vapaa-ajan rakentamista, piharakentamista ja LVIS-ratkaisuja (N=%i).", kiiaihep_ryhmät_freqs[1], kiiaihep_ryhmät_freqs[2])
    tekstilaatikko(plottext)
}

harjoitus_3_k091_k095 <- function() {
    message("Kävijöiden suunnitelmat:")
    suunnit_freqs <- c(sum(dataset$k091 == 1, na.rm=TRUE),
                       sum(dataset$k092 == 2, na.rm=TRUE),
                       sum(dataset$k093 == 3, na.rm=TRUE),
                       sum(dataset$k094 == 4, na.rm=TRUE),
                       sum(dataset$k095 == 5, na.rm=TRUE))
    suunnit_labs <- c("Rakentaminen",
                      "Remontointi",
                      "Sisustaminen",
                      "Piha-alueen\nmuokkaaminen",
                      "Ei suunnitelmia")
    suunnit <- data.frame(Suunnitelmat=suunnit_labs, Lukumäärä=suunnit_freqs)
    suunnit <- suunnit[order(-suunnit$Lukumäärä),]
    barplot(main="Suunnitelmat", suunnit$Lukumäärä, names.arg=suunnit$Suunnitelmat, horiz=FALSE, cex.names=.8)
    plottext <- sprintf("Eniten suunnitelmia messuilla kävijöillä liittyi remontointiin (N=%i). Toisaalta huomattavan suuri oli myös ilman suunnitelmia olleiden määrä (N=%i). Rakentaminen, sisustaminen ja piha-alueen muokkaaminen sijoittuivat yleisyydessä näiden kahden ääripään väliin (N=%i, %i, %i).", suunnit$Lukumäärä[1], suunnit$Lukumäärä[5], suunnit$Lukumäärä[2], suunnit$Lukumäärä[3], suunnit$Lukumäärä[4])
    tekstilaatikko(plottext)
}

harjoitus_3_k10kerta <- function() {
    message("Monesko kerta messuilla:")
    mkerta_freqs <- c(sum(dataset$k10kerta == 1, na.rm=TRUE),
                      sum(dataset$k10kerta == 2, na.rm=TRUE),
                      sum(dataset$k10kerta == 3, na.rm=TRUE),
                      sum(dataset$k10kerta == 4, na.rm=TRUE))
    mkerta_labs <- c("1.",
                     "2.",
                     "3-5.",
                     "6.+")
    mkerta <- data.frame(Käyntikerta=mkerta_labs, Lukumäärä=mkerta_freqs)
    mkerta <- mkerta[order(-mkerta$Lukumäärä),]
    barplot(main="Monesko kerta messuilla", mkerta$Lukumäärä, names.arg=mkerta$Käyntikerta, horiz=FALSE, cex.names=.6)
    plottext <- sprintf("Eniten tapauksia löytyi niiltä, joilla käyntikerta oli 3-5 (N=%i). Lähes yhtä paljon käyntikertoja löytyi kuitenkin ensimmäistä kertaa messuille tulevilta (N=%i). Toista kertaa messuille tulevia oli kuitenkin jo huomattavan paljon vähemmän (N=%i) ja kuusi tai useamman kertaa käyviä löytyi kaikkein vähiten (N=%i).", mkerta$Lukumäärä[1], mkerta$Lukumäärä[2], mkerta$Lukumäärä[3], mkerta$Lukumäärä[4])
    tekstilaatikko(plottext)
}

harjoitus_3_k11tieto <- function() {
    message("Messuista saatu tieto:")
    tietol_freqs <- c(sum(dataset$k11tieto == 1, na.rm=TRUE),
                      sum(dataset$k11tieto == 2, na.rm=TRUE),
                      sum(dataset$k11tieto == 3, na.rm=TRUE),
                      sum(dataset$k11tieto == 4, na.rm=TRUE),
                      sum(dataset$k11tieto == 5, na.rm=TRUE),
                      sum(dataset$k11tieto == 6, na.rm=TRUE),
                      sum(dataset$k11tieto == 7, na.rm=TRUE))
    tietol_labs <- c("Sanomalehti-\nmainos",
                     "TV",
                     "Radio",
                     "Kuulin\ntuttavalta",
                     "Asiakaskut-\nsukortti",
                     "Pytinki-\nmessulehti",
                     "Jokin muu")
    tietol <- data.frame(Tietolähde=tietol_labs, Lukumäärä=tietol_freqs)
    tietol <- tietol[order(-tietol$Lukumäärä),]
    barplot(main="Messuista saatu tieto", tietol$Lukumäärä, names.arg=tietol$Tietolähde, horiz=FALSE, cex.names=.7)
    plottext <- "Ylivoimaisesti eniten messuista saatiin tietoa sanomalehdestä. Sanomalehti tietolähteenä vastasi osuudeltaan n. puolta kaikkien muiden tietolähteiden yhteenlasketusta osuudesta. Tilanne selviää myös seuraavasta piirakkakaaviosta."
    tekstilaatikko(plottext)
    message("Messuista saatu tieto (jatk.):")
    tietol_ryhmät_freqs <- c(sum(dataset$k11tieto == 1, na.rm=TRUE),
                             sum(dataset$k11tieto == 5, na.rm=TRUE) +
                             sum(dataset$k11tieto == 6, na.rm=TRUE) +
                             sum(dataset$k11tieto == 4, na.rm=TRUE) +
                             sum(dataset$k11tieto == 2, na.rm=TRUE) +
                             sum(dataset$k11tieto == 7, na.rm=TRUE) +
                             sum(dataset$k11tieto == 3, na.rm=TRUE))
    tietol_ryhmät_labs <- c("Sanomalehtimainos", "Muut")
    tietol_ryhmät_värit <- c("red", "grey")
    pie(main="Messuista saatu tieto (jatk.)", tietol_ryhmät_freqs, labels=tietol_ryhmät_labs, col=tietol_ryhmät_värit)
}

harjoitus_3_k12lehti <- function() {
    message("Tutustuitko messulehteen:")
    messul_freqs <- c(sum(dataset$k12lehti == 1, na.rm=TRUE),
                      sum(dataset$k12lehti == 2, na.rm=TRUE))
    messul_labs <- c("Kyllä", "Ei")
    pie(main="Tutustuitko messulehteen?", messul_freqs, labels=messul_labs)
}

harjoitus_3_k13konta <- function() {
    message("Saitko uusia kontakteja:")
    kontakt_freqs <- c(sum(dataset$k13konta == 1, na.rm=TRUE),
                       sum(dataset$k13konta == 2, na.rm=TRUE),
                       sum(dataset$k13konta == 3, na.rm=TRUE),
                       sum(dataset$k13konta == 4, na.rm=TRUE))
    kontakt_labs <- c("Paljon",
                      "Jonkin verran",
                      "Vähän",
                      "En lainkaan")
    pie(main="Saitko messuilta uusia kontakteja?", kontakt_freqs, labels=kontakt_labs)
    kontakteja <- sum(kontakt_freqs[1:3])
    plottext <- sprintf("Huomionarvoista on messuilta kontakteja saaneiden suuri osuus. Jonkin verran, vähän ja paljon kontakteja saaneiden osuus oli huomattava (N=%i) verrattuna niihin, joka ilmoittivat etteivät saaneet lainkaan kontakteja (N=%i).", kontakteja, kontakt_freqs[4])
    tekstilaatikko(plottext)
}

harjoitus_3_k14hank <- function() {
    message("Vaikuttaako messukäynti omiin hankintoihisi:")
    hankint_freqs <- c(sum(dataset$k14hank == 1, na.rm=TRUE),
                       sum(dataset$k14hank == 2, na.rm=TRUE),
                       sum(dataset$k14hank == 3, na.rm=TRUE),
                       sum(dataset$k14hank == 4, na.rm=TRUE))
    hankint_labs <- c("Ratkaisevasti",
                      "Jonkin verran",
                      "En osaa sanoa",
                      "En lainkaan")
    pie(main="Vaikuttaako messukäynti omiin hankintoihisi?", hankint_freqs, labels=hankint_labs)
    plottext <- sprintf("Messuilla on myös vaikuttava voima. Vastaajista enemmistö arvioi messukäynnin vaikuttavan omiin hankintoihin jonkin verran (N=%i) ja ratkaisevasti (N=%i). Ainoastaan huomattavan pieni osuus käyneistä totesi ettei messukäynnillä ole minkäänlaista vaikutusta omiin hankintoihin (N=%i). Tulosten arvioinnissa kannattaa ottaa huomioon myös niiden henkilöiden suuri määrä, jotka eivät osanneet arvioida messukäynnin vaikutusta omiin hankintoihin (N=%i).", hankint_freqs[2], hankint_freqs[1], hankint_freqs[4], hankint_freqs[3])
    tekstilaatikko(plottext)
}

harjoitus_3_k15isku <- function() {
    message("Kävitkö/ aiotko käydä kuuntelemassa tietoiskuja:")
    kävitkö_freqs <- c(sum(dataset$k15isku == 1, na.rm=TRUE),
                       sum(dataset$k15isku == 2, na.rm=TRUE))
    kävitkö_labs <- c("Kyllä",
                      "Ei")
    pie(main="Kävitkö/ aiotko käydä kuuntelemassa tietoiskuja?", kävitkö_freqs, labels=kävitkö_labs)
    plottext <- sprintf("Huomionarvoista on, että enemmistö ei ole kiinnostunut tietoiskuista (N=%i/%i) vaikka enemmistö ilmoitti messukäyntien vaikuttavan omiin hankintoihin.", kävitkö_freqs[2], kävitkö_freqs[1])
    tekstilaatikko(plottext)
}

harjoitus_3_k16näytö <- function() {
    message("Kävitkö/ aiotko käydä katsomassa työnäytöksiä:")
    kävitkö2_freqs <- c(sum(dataset$k16näytö == 1, na.rm=TRUE),
                        sum(dataset$k16näytö == 2, na.rm=TRUE))
    kävitkö2_labs <- c("Kyllä",
                       "Ei")
    pie(main="Kävitkö/ aiotko käydä katsomassa työnäytöksiä?", kävitkö2_freqs, labels=kävitkö2_labs)
}

harjoitus_3_k17tyyt <- function() {
    message("Oletko tyytyväinen messujen tarjontaan?")
    tyyt_freqs <- c(sum(dataset$k17tyyt == 1, na.rm=TRUE),
                    sum(dataset$k17tyyt == 2, na.rm=TRUE),
                    sum(dataset$k17tyyt == 3, na.rm=TRUE),
                    sum(dataset$k17tyyt == 4, na.rm=TRUE))
    tyyt_labs <- c("Erittäin\ntyytyväinen",
                   "Melko\ntyytyväinen",
                   "Melko\ntyytymätön",
                   "Erittäin\ntyytymätön")
    pie(main="Oletko tyytyväinen messujen tarjontaan?", tyyt_freqs, labels=tyyt_labs)
    tyytyväiset <- sum(tyyt_freqs[1:2])
    tyytymättömät <- sum(tyyt_freqs[3:4])
    plottext <- sprintf("Osallistujista suurin osa (N=%i) ilmoitti olleensa messujen tarjontaan joko erittäin tai melko tyytyväisiä. Ainoastaan vähemmistö ilmoitti olevansa joko melko tai erittäin tyytymättömiä (N=%i)", tyytyväiset, tyytymättömät)
    tekstilaatikko(plottext)
    tunnusluvut(dataset$k17tyyt)
}

harjoitus_4_1 <- function() {
    message("")
    message("Tehtävä 4, alatehtävä 1")
    message("-----------------------")
    hist(main="Arvioi tyytyväisyyttäsi messujen tarjontaan kouluarvosanalla 4-10", dataset$k18arvio, xlab="Arvosana", ylab="Lukumäärä", breaks=seq(from=4,to=10,by=0.25), col=c("grey"))
    plottext <- "Arvosanoja annettiin lukumääräisesti eniten seuraavasti (laskeva järjestys): 8, 9, 7. Vähiten vastauksia saivat ääripäät kuten arvosanat, joissa esiintyi osapisteitä (0,25, 0,5)"
    tekstilaatikko(plottext)
    tunnusluvut(dataset$k18arvio)
}

harjoitus_4_23 <- function() {
    message("")
    message("Tehtävä 4, alatehtävä 2-3")
    message("-------------------------")
    boxplot(main="Arvioi tyytyväisyyttäsi messujen tarjontaan kouluarvosanalla 4-10\n(jatk.)", dataset$k18arvio, col=c("gold","darkgreen"), notch=FALSE)
    plottext <- "Boxplotista havaitaan, että arvosanojen keskiarvo oli 8, joka oli myös alaneljännes. Yläneljännes oli arvosana 9. Ääriarvot muodostivat 10 ja 6,5. Yksittäisiä arvosanoja löytyi myös arvosanoista 6 ja 5."
    tekstilaatikko(plottext)
    arvosana_summary <- summary(dataset$k18arvio)
    arvosana_summary
}

harjoitus_5_1 <- function() {
    message("")
    message("Tehtävä 5, alatehtävä 1")
    message("-----------------------")
    tulosyy_labs <- c("Alan hankintoja\nsuunnitteilla",
                      "Kiinnostunut\nammattirakentajana",
                      "Asiakaskutsu",
                      "Viihtyisän päivän\nvietto",
                      "Muu syy")
    counts <- table(dataset$k01sukup, dataset$k07syy)
    barplot(main="Messuille tulon syy sukupuolen mukaan", counts, names.arg=tulosyy_labs, legend=c("Naiset","Miehet"), col=c("red", "blue"), cex.names=0.7, beside=TRUE)
    plottext <- "Messuille tulon syyksi naiset kertoivat miehiä enemmän aikomuksestaan suunnitella alan hankintoja sekä viettää viihtyisää päivää. Miehet puolestaan raportoivat enemmän kiinnostuksestaan ammattirakentajana."
    tekstilaatikko(plottext)
}

harjoitus_5_1_vaikutus <- function() {
    message("")
    message("Tehtävä 5, alatehtävä 1 (vaikutus)")
    message("----------------------------------")
    tulosyyt_freqs_n <- c(sum(dataset$k01sukup == 1 & dataset$k07syy == 1, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k07syy == 2, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k07syy == 3, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k07syy == 4, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k07syy == 5, na.rm=TRUE))
    tulosyyt_freqs_m <- c(sum(dataset$k01sukup == 2 & dataset$k07syy == 1, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k07syy == 2, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k07syy == 3, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k07syy == 4, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k07syy == 5, na.rm=TRUE))
    tulosyy_labs <- c("Alan hankintoja suunnitteilla",
                      "Kiinnostunut ammattirakentajana",
                      "Asiakaskutsu",
                      "Viihtyisän päivän vietto",
                      "Muu syy")
    tulosyyt <- data.frame(Tulosyy=tulosyy_labs, Naiset=tulosyyt_freqs_n, Miehet=tulosyyt_freqs_m)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    tulosyyt_exp <- melt(tulosyyt, id=("Tulosyy"))
    names(tulosyyt_exp)[2] <- "Sukupuoli"
    names(tulosyyt_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    tulosyyt_tabs <- xtabs(tulosyyt_exp$Lukumäärä ~ tulosyyt_exp$Tulosyy + tulosyyt_exp$Sukupuoli)
    tulosyyt_summary <- summary(tulosyyt_tabs)
    # Lasketaan kiin neliö
    sarake_nimet <- c("Tulosyy", "Sukupuoli", "Lukumäärä")
    sarake_nimet_s <- c("Naiset", "Miehet")
    tulosyyt_chisq <- chisq.test(tulosyyt_tabs)
    print(tulosyyt_chisq)
    tulosyyt_observed <- data.frame(tulosyyt_chisq$observed)
    tulosyyt_expected <- data.frame(as.table(tulosyyt_chisq$expected))
    tulosyyt_residuals <- data.frame(tulosyyt_chisq$residuals)
    names(tulosyyt_observed) <- sarake_nimet
    names(tulosyyt_expected) <- sarake_nimet
    names(tulosyyt_residuals) <- sarake_nimet
    tulosyyt_observed <- dcast(tulosyyt_observed, Tulosyy ~ Sukupuoli, value.var="Lukumäärä")
    tulosyyt_expected <- dcast(tulosyyt_expected, Tulosyy ~ Sukupuoli, value.var="Lukumäärä")
    tulosyyt_residuals <- dcast(tulosyyt_residuals, Tulosyy ~ Sukupuoli, value.var="Lukumäärä")
    print(tulosyyt_observed)
    print(tulosyyt_expected)
    print(tulosyyt_residuals)
    plottext <- "Tässä esimerkissä naisten kiinnostus etenkin ammattirakentajana oli huomattavan pientä kun taas miehillä huomattavan suurta."
    tekstilaatikko_khiihylkää("0,004937", "sukupuolten", "sukupuolella", plottext)
}

harjoitus_5_2 <- function() {
    message("")
    message("Tehtävä 5, alatehtävä 2")
    message("-----------------------")
    kiiaihep_labs <- c("Vanhan\nkorjaaminen",
                       "Uuden\nrakentaminen",
                       "Sisustaminen",
                       "Piharakentaminen",
                       "Vapaa-ajan\nrakentaminen",
                       "LVIS-ratkaisut")
    counts <- table(dataset$k01sukup, dataset$k08aihep)
    barplot(main="Messujen kiinnostavin aihepiiri sukupuolen mukaan", counts, names.arg=kiiaihep_labs, legend=c("Naiset","Miehet"), col=c("red", "blue"), cex.names=0.7, beside=TRUE)
    plottext <- "Messujen kiinnostavimmaksi aihepiiriksi naiset ilmoittivat ylivoimaisesti eniten sisustuksen. Miehet puolestaan ilmoittivat vapaa-ajan rakentamisen sekä LVIS-ratkaisut."
    tekstilaatikko(plottext)
}

harjoitus_5_2_vaikutus <- function() {
    message("")
    message("Tehtävä 5, alatehtävä 2 (vaikutus)")
    message("----------------------------------")
    kiiaihep_freqs_n <- c(sum(dataset$k01sukup == 1 & dataset$k08aihep == 1, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k08aihep == 2, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k08aihep == 3, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k08aihep == 4, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k08aihep == 5, na.rm=TRUE),
                          sum(dataset$k01sukup == 1 & dataset$k08aihep == 6, na.rm=TRUE))
    kiiaihep_freqs_m <- c(sum(dataset$k01sukup == 2 & dataset$k08aihep == 1, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k08aihep == 2, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k08aihep == 3, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k08aihep == 4, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k08aihep == 5, na.rm=TRUE),
                          sum(dataset$k01sukup == 2 & dataset$k08aihep == 6, na.rm=TRUE))
    kiiaihep_labs <- c("Vanhan korjaaminen",
                       "Uuden rakentaminen",
                       "Sisustaminen",
                       "Piharakentaminen",
                       "Vapaa-ajan rakentaminen",
                       "LVIS-ratkaisut")
    kiiaihep <- data.frame(Aihepiiri=kiiaihep_labs, Naiset=kiiaihep_freqs_n, Miehet=kiiaihep_freqs_m)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    kiiaihep_exp <- melt(kiiaihep, id=("Aihepiiri"))
    names(kiiaihep_exp)[2] <- "Sukupuoli"
    names(kiiaihep_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    kiiaihep_tabs <- xtabs(kiiaihep_exp$Lukumäärä ~ kiiaihep_exp$Aihepiiri + kiiaihep_exp$Sukupuoli)
    kiiaihep_summary <- summary(kiiaihep_tabs)
    # Lasketaan kiin neliö
    sarake_nimet <- c("Aihepiiri", "Sukupuoli", "Lukumäärä")
    sarake_nimet_s <- c("Naiset", "Miehet")
    kiiaihep_chisq <- chisq.test(kiiaihep_tabs)
    print(kiiaihep_chisq)
    kiiaihep_observed <- data.frame(kiiaihep_chisq$observed)
    kiiaihep_expected <- data.frame(as.table(kiiaihep_chisq$expected))
    kiiaihep_residuals <- data.frame(kiiaihep_chisq$residuals)
    names(kiiaihep_observed) <- sarake_nimet
    names(kiiaihep_expected) <- sarake_nimet
    names(kiiaihep_residuals) <- sarake_nimet
    kiiaihep_observed <- dcast(kiiaihep_observed, Aihepiiri ~ Sukupuoli, value.var="Lukumäärä")
    kiiaihep_expected <- dcast(kiiaihep_expected, Aihepiiri ~ Sukupuoli, value.var="Lukumäärä")
    kiiaihep_residuals <- dcast(kiiaihep_residuals, Aihepiiri ~ Sukupuoli, value.var="Lukumäärä")
    print(kiiaihep_observed)
    print(kiiaihep_expected)
    print(kiiaihep_residuals)
    plottext <- "Tässä esimerkissä naiset olivat poikkeuksellisen paljon kiinnostuneita sisustamisesta ja poikkeuksellisen vähän kiinnostuneita LVIS-ratkaisuista kun taas miehillä tilanne oli päinvastainen."
    tekstilaatikko_khiihylkää("0,000000165", "sukupuolten", "sukupuolella", plottext)
}

harjoitus_5_3 <- function() {
    message("")
    message("Tehtävä 5, alatehtävä 3")
    message("-----------------------")
    tyyt_labs <- c("Erittäin\ntyytyväinen",
                   "Melko\ntyytyväinen",
                   "Melko\ntyytymätön",
                   "Erittäin\ntyytymätön")
    counts <- table(dataset$k01sukup, dataset$k17tyyt)
    barplot(main="Tyytyväisyys messujen tarjontaan sukupuolen mukaan", counts, names.arg=tyyt_labs, legend=c("Naiset","Miehet"), col=c("red", "blue"), cex.names=0.7, beside=TRUE)
    plottext <- "Naiset antoivat miehiä enemmän ääniä ilmaistessaan tyytyväisyyttää mutta myös ilmaistessa erittäin tyytymättömän tilaansa."
    tekstilaatikko(plottext)
}

harjoitus_5_3_vaikutus <- function() {
    message("")
    message("Tehtävä 5, alatehtävä 3 (vaikutus)")
    message("----------------------------------")
    tyyt_freqs_n <- c(sum(dataset$k01sukup == 1 & dataset$k17tyyt == 1, na.rm=TRUE),
                      sum(dataset$k01sukup == 1 & dataset$k17tyyt == 2, na.rm=TRUE),
                      sum(dataset$k01sukup == 1 & dataset$k17tyyt == 3, na.rm=TRUE),
                      sum(dataset$k01sukup == 1 & dataset$k17tyyt == 4, na.rm=TRUE))
    tyyt_freqs_m <- c(sum(dataset$k01sukup == 2 & dataset$k17tyyt == 1, na.rm=TRUE),
                      sum(dataset$k01sukup == 2 & dataset$k17tyyt == 2, na.rm=TRUE),
                      sum(dataset$k01sukup == 2 & dataset$k17tyyt == 3, na.rm=TRUE),
                      sum(dataset$k01sukup == 2 & dataset$k17tyyt == 4, na.rm=TRUE))
    tyyt_labs <- c("Erittäin tyytyväinen",
                   "Melko tyytyväinen",
                   "Melko tyytymätön",
                   "Erittäin tyytymätön")
    tyyt <- data.frame(Tyytyväisyys=tyyt_labs, Naiset=tyyt_freqs_n, Miehet=tyyt_freqs_m)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    tyyt_exp <- melt(tyyt, id=("Tyytyväisyys"))
    names(tyyt_exp)[2] <- "Sukupuoli"
    names(tyyt_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    tyyt_tabs <- xtabs(tyyt_exp$Lukumäärä ~ tyyt_exp$Tyytyväisyys + tyyt_exp$Sukupuoli)
    tyyt_summary <- summary(tyyt_tabs)
    # Lasketaan kiin neliö
    sarake_nimet <- c("Tyytyväisyys", "Sukupuoli", "Lukumäärä")
    sarake_nimet_s <- c("Naiset", "Miehet")
    tyyt_chisq <- chisq.test(tyyt_tabs)
    print(tyyt_chisq)
    tyyt_observed <- data.frame(tyyt_chisq$observed)
    tyyt_expected <- data.frame(as.table(tyyt_chisq$expected))
    tyyt_residuals <- data.frame(tyyt_chisq$residuals)
    names(tyyt_observed) <- sarake_nimet
    names(tyyt_expected) <- sarake_nimet
    names(tyyt_residuals) <- sarake_nimet
    tyyt_observed <- dcast(tyyt_observed, Tyytyväisyys ~ Sukupuoli, value.var="Lukumäärä")
    tyyt_expected <- dcast(tyyt_expected, Tyytyväisyys ~ Sukupuoli, value.var="Lukumäärä")
    tyyt_residuals <- dcast(tyyt_residuals, Tyytyväisyys ~ Sukupuoli, value.var="Lukumäärä")
    print(tyyt_observed)
    print(tyyt_expected)
    print(tyyt_residuals)
    plottext <- "Tässä esimerkissä sekä naiset että miehet antoivat suhteellisen samankaltaisia vastauksia."
    tekstilaatikko_khiihyväksy("0,7706", "sukupuolten", "sukupuolella", plottext)
}

harjoitus_5_4 <- function() {
    message("")
    message("Tehtävä 5, alatehtävä 4")
    message("-----------------------")
    arvio_sukup_n <- dataset$k18arvio[dataset$k01sukup == 1]
    arvio_sukup_n <- arvio_sukup_n[!is.na(arvio_sukup_n)]
    arvio_sukup_m <- dataset$k18arvio[dataset$k01sukup == 2]
    arvio_sukup_m <- arvio_sukup_m[!is.na(arvio_sukup_m)]
    hist(main="Messujen saama kokonaisarvosana sukupuolen mukaan\n(naiset)", arvio_sukup_n, xlab="Arvosana", ylab="Lukumäärä", breaks=seq(from=4,to=10,by=0.25), col=c("grey"))
    hist(main="Messujen saama kokonaisarvosana sukupuolen mukaan\n(miehet)", arvio_sukup_m, xlab="Arvosana", ylab="Lukumäärä", breaks=seq(from=4,to=10,by=0.25), col=c("grey"))
    boxplot(main="Messujen saama kokonaisarvosana sukupuolen mukaan\n(naiset)", arvio_sukup_n, col=c("gold","darkgreen"))
    boxplot(main="Messujen saama kokonaisarvosana sukupuolen mukaan\n(miehet)", arvio_sukup_m, col=c("gold","darkgreen"))
    plottext <- "Miehet olivat taipuvaisia antamaan keskimäärin puoli pistettä huonompia arvosanoja sekä enemmän ääniä myös arvosanojen ääripäissa 10 ja 5. Toisaalta miesten antama äänimäärä etenkin arvosanalle 8 oli lisäksi huomattavasti naisten äänimäärää pienempi. Miesten tyytymättömyys tulee kuitenkin esille vain arvosanoissa, sillä tyytyväisyydessä messujen tarjontaan olivat naiset antaneet enemmän ääniä ilmaistessaan erittäin tyytymätöntä tilaansa."
    tekstilaatikko(plottext)
}

harjoitus_5_4_vaikutus <- function() {
    message("")
    message("Tehtävä 5, alatehtävä 4 (vaikutus)")
    message("----------------------------------")
    # Tässä arvosanat pitää todennäköisesti pyöristää kokonaislukuihin tai jakaa
    # kategorioihin, sillä chi squared -testi vaatii suhteellisen suuren määrän
    # sampleja. Vastaajien määrän vähyys ja arvaosanojen laaja skaala ei välttämättä
    # suoraan riitä. Käytetään vuoden 1952 arvosanojen luokitusta:
    # https://fi.wikipedia.org/wiki/Arvosana
    # arvio_freqs_n <- sum(dataset$k01sukup == 1 & (round(dataset$k18arvio)>=9.0 & round(dataset$k18arvio) <= 10.0))
    # arvio_freqs_n <- sum(dataset$round(dataset$k18arvio == 9.00, na.rm=TRUE)
    arvio_freqs_n <- c(sum(dataset$k01sukup == 1 & round(dataset$k18arvio) >= 9.0 & round(dataset$k18arvio) <= 10.0, na.rm=TRUE),
                       sum(dataset$k01sukup == 1 & round(dataset$k18arvio) >= 7.0 & round(dataset$k18arvio) <= 8.0, na.rm=TRUE),
                       sum(dataset$k01sukup == 1 & round(dataset$k18arvio) >= 4.0 & round(dataset$k18arvio) <= 6.0, na.rm=TRUE))
    arvio_freqs_m <- c(sum(dataset$k01sukup == 2 & round(dataset$k18arvio) >= 9.0 & round(dataset$k18arvio) <= 10.0, na.rm=TRUE),
                       sum(dataset$k01sukup == 2 & round(dataset$k18arvio) >= 7.0 & round(dataset$k18arvio) <= 8.0, na.rm=TRUE),
                       sum(dataset$k01sukup == 2 & round(dataset$k18arvio) >= 4.0 & round(dataset$k18arvio) <= 6.0, na.rm=TRUE))
    arvio_labs <- c("Kiitettävä",  # 9 & 10
                    "Tyydyttävä",  # 7 & 8
                    "Välttävä")    # 5 & 6 (ja 4)
    arvio <- data.frame(Arvio=arvio_labs, Naiset=arvio_freqs_n, Miehet=arvio_freqs_m)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    arvio_exp <- melt(arvio, id=("Arvio"))
    names(arvio_exp)[2] <- "Sukupuoli"
    names(arvio_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    arvio_tabs <- xtabs(arvio_exp$Lukumäärä ~ arvio_exp$Arvio + arvio_exp$Sukupuoli)
    arvio_summary <- summary(arvio_tabs)
    # Lasketaan kiin neliö
    sarake_nimet <- c("Arvio", "Sukupuoli", "Lukumäärä")
    sarake_nimet_s <- c("Naiset", "Miehet")
    arvio_chisq <- chisq.test(arvio_tabs)
    print(arvio_chisq)
    arvio_observed <- data.frame(arvio_chisq$observed)
    arvio_expected <- data.frame(as.table(arvio_chisq$expected))
    arvio_residuals <- data.frame(arvio_chisq$residuals)
    names(arvio_observed) <- sarake_nimet
    names(arvio_expected) <- sarake_nimet
    names(arvio_residuals) <- sarake_nimet
    arvio_observed <- dcast(arvio_observed, Arvio ~ Sukupuoli, value.var="Lukumäärä")
    arvio_expected <- dcast(arvio_expected, Arvio ~ Sukupuoli, value.var="Lukumäärä")
    arvio_residuals <- dcast(arvio_residuals, Arvio ~ Sukupuoli, value.var="Lukumäärä")
    print(arvio_observed)
    print(arvio_expected)
    print(arvio_residuals)
    plottext <- "Tässä esimerkissä sekä naiset että miehet antoivat suhteellisen samankaltaisia vastauksia."
    tekstilaatikko_khiihyväksy("0,07705", "sukupuolten", "sukupuolella", plottext)
}

harjoitus_6_1 <- function() {
    message("")
    message("Tehtävä 6, alatehtävä 1")
    message("-----------------------")
    tulosyy_labs <- c("Alan hankintoja\nsuunnitteilla",
                      "Kiinnostunut\nammattirakentajana",
                      "Asiakaskutsu",
                      "Viihtyisän päivän\nvietto",
                      "Muu syy")
    ikä_labs <- c("0-20",
                  "20-35",
                  "36-49",
                  "50+")
    counts <- table(dataset$k02ikä, dataset$k07syy)
    barplot(main="Messuille tulon syy iän mukaan", counts, names.arg=tulosyy_labs, legend=ikä_labs, col=c("violet","orange","yellow","green"), cex.names=0.7, beside=TRUE)
}

harjoitus_6_1_vaikutus <- function() {
    message("")
    message("Tehtävä 6, alatehtävä 1 (vaikutus)")
    message("----------------------------------")
    tulosyy_freqs_ikä1 <- c(sum(dataset$k02ikä == 1 & dataset$k07syy == 1, na.rm=TRUE),
                            sum(dataset$k02ikä == 1 & dataset$k07syy == 2, na.rm=TRUE),
                            sum(dataset$k02ikä == 1 & dataset$k07syy == 3, na.rm=TRUE),
                            sum(dataset$k02ikä == 1 & dataset$k07syy == 4, na.rm=TRUE),
                            sum(dataset$k02ikä == 1 & dataset$k07syy == 5, na.rm=TRUE))
    tulosyy_freqs_ikä2 <- c(sum(dataset$k02ikä == 2 & dataset$k07syy == 1, na.rm=TRUE),
                            sum(dataset$k02ikä == 2 & dataset$k07syy == 2, na.rm=TRUE),
                            sum(dataset$k02ikä == 2 & dataset$k07syy == 3, na.rm=TRUE),
                            sum(dataset$k02ikä == 2 & dataset$k07syy == 4, na.rm=TRUE),
                            sum(dataset$k02ikä == 2 & dataset$k07syy == 5, na.rm=TRUE))
    tulosyy_freqs_ikä3 <- c(sum(dataset$k02ikä == 3 & dataset$k07syy == 1, na.rm=TRUE),
                            sum(dataset$k02ikä == 3 & dataset$k07syy == 2, na.rm=TRUE),
                            sum(dataset$k02ikä == 3 & dataset$k07syy == 3, na.rm=TRUE),
                            sum(dataset$k02ikä == 3 & dataset$k07syy == 4, na.rm=TRUE),
                            sum(dataset$k02ikä == 3 & dataset$k07syy == 5, na.rm=TRUE))
    tulosyy_freqs_ikä4 <- c(sum(dataset$k02ikä == 4 & dataset$k07syy == 1, na.rm=TRUE),
                            sum(dataset$k02ikä == 4 & dataset$k07syy == 2, na.rm=TRUE),
                            sum(dataset$k02ikä == 4 & dataset$k07syy == 3, na.rm=TRUE),
                            sum(dataset$k02ikä == 4 & dataset$k07syy == 4, na.rm=TRUE),
                            sum(dataset$k02ikä == 4 & dataset$k07syy == 5, na.rm=TRUE))
    tulosyy_labs <- c("Alan hankintoja suunnitteilla",
                      "Kiinnostunut ammattirakentajana",
                      "Asiakaskutsu",
                      "Viihtyisän päivän vietto",
                      "Muu syy")
    tulosyy <- data.frame(Tulosyy=tulosyy_labs, Ikä1=tulosyy_freqs_ikä1, Ikä2=tulosyy_freqs_ikä2, Ikä3=tulosyy_freqs_ikä3, Ikä4=tulosyy_freqs_ikä4)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    tulosyy_exp <- melt(tulosyy, id=("Tulosyy"))
    names(tulosyy_exp)[2] <- "Ikä"
    names(tulosyy_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    tulosyy_tabs <- xtabs(tulosyy_exp$Lukumäärä ~ tulosyy_exp$Tulosyy + tulosyy_exp$Ikä)
    tulosyy_summary <- summary(tulosyy_tabs)
    # Lasketaan kiin neliö
    sarake_nimet <- c("Tulosyy", "Ikä", "Lukumäärä")
    sarake_nimet_s <- c("Naiset", "Miehet")
    tulosyy_chisq <- chisq.test(tulosyy_tabs)
    print(tulosyy_chisq)
    tulosyy_observed <- data.frame(tulosyy_chisq$observed)
    tulosyy_expected <- data.frame(as.table(tulosyy_chisq$expected))
    tulosyy_residuals <- data.frame(tulosyy_chisq$residuals)
    names(tulosyy_observed) <- sarake_nimet
    names(tulosyy_expected) <- sarake_nimet
    names(tulosyy_residuals) <- sarake_nimet
    tulosyy_observed <- dcast(tulosyy_observed, Tulosyy ~ Ikä, value.var="Lukumäärä")
    tulosyy_expected <- dcast(tulosyy_expected, Tulosyy ~ Ikä, value.var="Lukumäärä")
    tulosyy_residuals <- dcast(tulosyy_residuals, Tulosyy ~ Ikä, value.var="Lukumäärä")
    print(tulosyy_observed)
    print(tulosyy_expected)
    print(tulosyy_residuals)
    plottext <- "Tässä esimerkissä nuorimmilla oli poikkeuksellisen vähän suunnitteilla alan hankintoja ja poikkeuksellisen paljon kiinnostusta ammattirakentajana sekä muita syitä."
    tekstilaatikko_khiihylkää("0,0002061", "ikäluokkien", "iällä", plottext)
}

harjoitus_6_2 <- function() {
    message("")
    message("Tehtävä 6, alatehtävä 2")
    message("-----------------------")
    kiiaihep_labs <- c("Vanhan\nkorjaaminen",
                       "Uuden\nrakentaminen",
                       "Sisustaminen",
                       "Piharakentaminen",
                       "Vapaa-ajan\nrakentaminen",
                       "LVIS-ratkaisut")
    ikä_labs <- c("0-20",
                  "20-35",
                  "36-49",
                  "50+")
    counts <- table(dataset$k02ikä, dataset$k08aihep)
    barplot(main="Kiinnostavin aihepiiri iän mukaan", counts, names.arg=kiiaihep_labs, legend=ikä_labs, col=c("violet","orange","yellow","green"), cex.names=0.7, beside=TRUE)
}

harjoitus_6_2_vaikutus <- function() {
    message("")
    message("Tehtävä 6, alatehtävä 2 (vaikutus)")
    message("----------------------------------")
    kiiaihep_freqs_ikä1 <- c(sum(dataset$k02ikä == 1 & dataset$k08aihep == 1, na.rm=TRUE),
                             sum(dataset$k02ikä == 1 & dataset$k08aihep == 2, na.rm=TRUE),
                             sum(dataset$k02ikä == 1 & dataset$k08aihep == 3, na.rm=TRUE),
                             sum(dataset$k02ikä == 1 & dataset$k08aihep == 4, na.rm=TRUE),
                             sum(dataset$k02ikä == 1 & dataset$k08aihep == 5, na.rm=TRUE),
                             sum(dataset$k02ikä == 1 & dataset$k08aihep == 6, na.rm=TRUE))
    kiiaihep_freqs_ikä2 <- c(sum(dataset$k02ikä == 2 & dataset$k08aihep == 1, na.rm=TRUE),
                             sum(dataset$k02ikä == 2 & dataset$k08aihep == 2, na.rm=TRUE),
                             sum(dataset$k02ikä == 2 & dataset$k08aihep == 3, na.rm=TRUE),
                             sum(dataset$k02ikä == 2 & dataset$k08aihep == 4, na.rm=TRUE),
                             sum(dataset$k02ikä == 2 & dataset$k08aihep == 5, na.rm=TRUE),
                             sum(dataset$k02ikä == 2 & dataset$k08aihep == 6, na.rm=TRUE))
    kiiaihep_freqs_ikä3 <- c(sum(dataset$k02ikä == 3 & dataset$k08aihep == 1, na.rm=TRUE),
                             sum(dataset$k02ikä == 3 & dataset$k08aihep == 2, na.rm=TRUE),
                             sum(dataset$k02ikä == 3 & dataset$k08aihep == 3, na.rm=TRUE),
                             sum(dataset$k02ikä == 3 & dataset$k08aihep == 4, na.rm=TRUE),
                             sum(dataset$k02ikä == 3 & dataset$k08aihep == 5, na.rm=TRUE),
                             sum(dataset$k02ikä == 3 & dataset$k08aihep == 6, na.rm=TRUE))
    kiiaihep_freqs_ikä4 <- c(sum(dataset$k02ikä == 4 & dataset$k08aihep == 1, na.rm=TRUE),
                             sum(dataset$k02ikä == 4 & dataset$k08aihep == 2, na.rm=TRUE),
                             sum(dataset$k02ikä == 4 & dataset$k08aihep == 3, na.rm=TRUE),
                             sum(dataset$k02ikä == 4 & dataset$k08aihep == 4, na.rm=TRUE),
                             sum(dataset$k02ikä == 4 & dataset$k08aihep == 5, na.rm=TRUE),
                             sum(dataset$k02ikä == 4 & dataset$k08aihep == 6, na.rm=TRUE))
    kiiaihep_labs <- c("Vanhan korjaaminen",
                       "Uuden rakentaminen",
                       "Sisustaminen",
                       "Piharakentaminen",
                       "Vapaa-ajan rakentaminen",
                       "LVIS-ratkaisut")
    kiiaihep <- data.frame(Kiinnostavin=kiiaihep_labs, Ikä1=kiiaihep_freqs_ikä1, Ikä2=kiiaihep_freqs_ikä2, Ikä3=kiiaihep_freqs_ikä3, Ikä4=kiiaihep_freqs_ikä4)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    kiiaihep_exp <- melt(kiiaihep, id=("Kiinnostavin"))
    names(kiiaihep_exp)[2] <- "Ikä"
    names(kiiaihep_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    kiiaihep_tabs <- xtabs(kiiaihep_exp$Lukumäärä ~ kiiaihep_exp$Kiinnostavin + kiiaihep_exp$Ikä)
    kiiaihep_summary <- summary(kiiaihep_tabs)
    # Lasketaan kiin neliö
    sarake_nimet <- c("Kiinnostavin", "Ikä", "Lukumäärä")
    sarake_nimet_s <- c("Naiset", "Miehet")
    kiiaihep_chisq <- chisq.test(kiiaihep_tabs)
    print(kiiaihep_chisq)
    kiiaihep_observed <- data.frame(kiiaihep_chisq$observed)
    kiiaihep_expected <- data.frame(as.table(kiiaihep_chisq$expected))
    kiiaihep_residuals <- data.frame(kiiaihep_chisq$residuals)
    names(kiiaihep_observed) <- sarake_nimet
    names(kiiaihep_expected) <- sarake_nimet
    names(kiiaihep_residuals) <- sarake_nimet
    kiiaihep_observed <- dcast(kiiaihep_observed, Kiinnostavin ~ Ikä, value.var="Lukumäärä")
    kiiaihep_expected <- dcast(kiiaihep_expected, Kiinnostavin ~ Ikä, value.var="Lukumäärä")
    kiiaihep_residuals <- dcast(kiiaihep_residuals, Kiinnostavin ~ Ikä, value.var="Lukumäärä")
    print(kiiaihep_observed)
    print(kiiaihep_expected)
    print(kiiaihep_residuals)
    plottext <- "Tässä esimerkissä löytyi poikkeuksellisen paljon eroja ikäluokkien välillä. Nuorimmilla oli poikkeuksellisen paljon kiinnostusta sisustamista kohtaan, toisessa ikäluokassa eniten kiinnostusta uuden rakentamista kohtaan ja vanhimmilla vähiten kiinnostusta uuden rakentamista kohtaan."
    tekstilaatikko_khiihylkää("0,000000000002282", "ikäluokkien", "iällä", plottext)
}

harjoitus_6_3 <- function() {
    message("")
    message("Tehtävä 6, alatehtävä 3")
    message("-----------------------")
    tyyt_labs <- c("Erittäin\ntyytyväinen",
                   "Melko\ntyytyväinen",
                   "Melko\ntyytymätön",
                   "Erittäin\ntyytymätön")
    ikä_labs <- c("0-20",
                  "20-35",
                  "36-49",
                  "50+")
    counts <- table(dataset$k02ikä, dataset$k17tyyt)
    barplot(main="Tyytyväisyys messujen tarjontaan iän mukaan", counts, names.arg=tyyt_labs, legend=ikä_labs, col=c("violet","orange","yellow","green"), cex.names=0.7, beside=TRUE)
}

harjoitus_6_3_vaikutus <- function() {
    message("")
    message("Tehtävä 6, alatehtävä 3 (vaikutus)")
    message("----------------------------------")
    tyyt_freqs_ikä1 <- c(sum(dataset$k02ikä == 1 & dataset$k17tyyt == 1, na.rm=TRUE),
                         sum(dataset$k02ikä == 1 & dataset$k17tyyt == 2, na.rm=TRUE),
                         sum(dataset$k02ikä == 1 & dataset$k17tyyt == 3, na.rm=TRUE),
                         sum(dataset$k02ikä == 1 & dataset$k17tyyt == 4, na.rm=TRUE))
    tyyt_freqs_ikä2 <- c(sum(dataset$k02ikä == 2 & dataset$k17tyyt == 1, na.rm=TRUE),
                         sum(dataset$k02ikä == 2 & dataset$k17tyyt == 2, na.rm=TRUE),
                         sum(dataset$k02ikä == 2 & dataset$k17tyyt == 3, na.rm=TRUE),
                         sum(dataset$k02ikä == 2 & dataset$k17tyyt == 4, na.rm=TRUE))
    tyyt_freqs_ikä3 <- c(sum(dataset$k02ikä == 3 & dataset$k17tyyt == 1, na.rm=TRUE),
                         sum(dataset$k02ikä == 3 & dataset$k17tyyt == 2, na.rm=TRUE),
                         sum(dataset$k02ikä == 3 & dataset$k17tyyt == 3, na.rm=TRUE),
                         sum(dataset$k02ikä == 3 & dataset$k17tyyt == 4, na.rm=TRUE))
    tyyt_freqs_ikä4 <- c(sum(dataset$k02ikä == 4 & dataset$k17tyyt == 1, na.rm=TRUE),
                         sum(dataset$k02ikä == 4 & dataset$k17tyyt == 2, na.rm=TRUE),
                         sum(dataset$k02ikä == 4 & dataset$k17tyyt == 3, na.rm=TRUE),
                         sum(dataset$k02ikä == 4 & dataset$k17tyyt == 4, na.rm=TRUE))
    tyyt_labs <- c("Erittäin tyytyväinen",
                   "Melko tyytyväinen",
                   "Melko tyytymätön",
                   "Erittäin tyytymätön")
    tyyt <- data.frame(Tyytyväisyys=tyyt_labs, Ikä1=tyyt_freqs_ikä1, Ikä2=tyyt_freqs_ikä2, Ikä3=tyyt_freqs_ikä3, Ikä4=tyyt_freqs_ikä4)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    tyyt_exp <- melt(tyyt, id=("Tyytyväisyys"))
    names(tyyt_exp)[2] <- "Ikä"
    names(tyyt_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    tyyt_tabs <- xtabs(tyyt_exp$Lukumäärä ~ tyyt_exp$Tyytyväisyys + tyyt_exp$Ikä)
    tyyt_summary <- summary(tyyt_tabs)
    # Lasketaan kiin neliö
    sarake_nimet <- c("Tyytyväisyys", "Ikä", "Lukumäärä")
    sarake_nimet_s <- c("Naiset", "Miehet")
    tyyt_chisq <- chisq.test(tyyt_tabs)
    print(tyyt_chisq)
    tyyt_observed <- data.frame(tyyt_chisq$observed)
    tyyt_expected <- data.frame(as.table(tyyt_chisq$expected))
    tyyt_residuals <- data.frame(tyyt_chisq$residuals)
    names(tyyt_observed) <- sarake_nimet
    names(tyyt_expected) <- sarake_nimet
    names(tyyt_residuals) <- sarake_nimet
    tyyt_observed <- dcast(tyyt_observed, Tyytyväisyys ~ Ikä, value.var="Lukumäärä")
    tyyt_expected <- dcast(tyyt_expected, Tyytyväisyys ~ Ikä, value.var="Lukumäärä")
    tyyt_residuals <- dcast(tyyt_residuals, Tyytyväisyys ~ Ikä, value.var="Lukumäärä")
    print(tyyt_observed)
    print(tyyt_expected)
    print(tyyt_residuals)
    plottext <- "Tässä esimerkissä kaikki ikäluokat antoivat suhteellisen samankaltaisia vastauksia."
    tekstilaatikko_khiihyväksy("0,1916", "ikäluokkien", "iällä", plottext)
}

harjoitus_6_4 <- function() {
    message("")
    message("Tehtävä 6, alatehtävä 4")
    message("-----------------------")
    arvio_ikä_0_20 <- dataset$k18arvio[dataset$k02ikä == 1]
    arvio_ikä_20_35 <- dataset$k18arvio[dataset$k02ikä == 2]
    arvio_ikä_36_49 <- dataset$k18arvio[dataset$k02ikä == 3]
    arvio_ikä_50p <- dataset$k18arvio[dataset$k02ikä == 4]
    arvio_ikä_0_20 <- arvio_ikä_0_20[!is.na(arvio_ikä_0_20)]
    arvio_ikä_20_35 <- arvio_ikä_20_35[!is.na(arvio_ikä_20_35)]
    arvio_ikä_36_49 <- arvio_ikä_36_49[!is.na(arvio_ikä_36_49)]
    arvio_ikä_50p <- arvio_ikä_50p[!is.na(arvio_ikä_50p)]
    hist(main="Messujen saama kokonaisarvosana iän mukaan\n(0-20v)", arvio_ikä_0_20, xlab="Arvosana", ylab="Lukumäärä", breaks=seq(from=4,to=10,by=0.25), col=c("grey"))
    hist(main="Messujen saama kokonaisarvosana iän mukaan\n(20-35v)", arvio_ikä_20_35, xlab="Arvosana", ylab="Lukumäärä", breaks=seq(from=4,to=10,by=0.25), col=c("grey"))
    hist(main="Messujen saama kokonaisarvosana iän mukaan\n(36-49v)", arvio_ikä_36_49, xlab="Arvosana", ylab="Lukumäärä", breaks=seq(from=4,to=10,by=0.25), col=c("grey"))
    hist(main="Messujen saama kokonaisarvosana iän mukaan\n(50+v)", arvio_ikä_50p, xlab="Arvosana", ylab="Lukumäärä", breaks=seq(from=4,to=10,by=0.25), col=c("grey"))
    boxplot(main="Messujen saama kokonaisarvosana iän mukaan\n(0-20v)", arvio_ikä_0_20, col=c("gold","darkgreen"))
    boxplot(main="Messujen saama kokonaisarvosana iän mukaan\n(20-35v)", arvio_ikä_20_35, col=c("gold","darkgreen"))
    boxplot(main="Messujen saama kokonaisarvosana iän mukaan\n(36-49v)", arvio_ikä_36_49, col=c("gold","darkgreen"))
    boxplot(main="Messujen saama kokonaisarvosana iän mukaan\n(50+v)", arvio_ikä_50p, col=c("gold","darkgreen"))
    ikä_freqs <- c(sum(dataset$k02ikä == 1, na.rm=TRUE),
                   sum(dataset$k02ikä == 2, na.rm=TRUE),
                   sum(dataset$k02ikä == 3, na.rm=TRUE),
                   sum(dataset$k02ikä == 4, na.rm=TRUE))
    ikä_labs <- c("0-20",
                  "20-35",
                  "36-49",
                  "50+")
    ikä <- data.frame(Ikä=ikä_labs, Lukumäärä=ikä_freqs)
    ikä <- ikä[order(-ikä$Lukumäärä),]
    barplot(main="Kokonaisvastausmäärät iän mukaan\n(vääristävät tekijät)", ikä$Lukumäärä, names.arg=ikä$Ikä, col="grey", cex.names=0.7, beside=FALSE)
}

harjoitus_6_4_vaikutus <- function() {
    message("")
    message("Tehtävä 6, alatehtävä 4 (vaikutus)")
    message("----------------------------------")
    arvio_freqs_ikä1 <- c(sum(dataset$k02ikä == 1 & round(dataset$k18arvio) >= 9.0 & round(dataset$k18arvio) <= 10.0, na.rm=TRUE),
                          sum(dataset$k02ikä == 1 & round(dataset$k18arvio) >= 7.0 & round(dataset$k18arvio) <=  8.0, na.rm=TRUE),
                          sum(dataset$k02ikä == 1 & round(dataset$k18arvio) >= 4.0 & round(dataset$k18arvio) <=  6.0, na.rm=TRUE))
    arvio_freqs_ikä2 <- c(sum(dataset$k02ikä == 2 & round(dataset$k18arvio) >= 9.0 & round(dataset$k18arvio) <= 10.0, na.rm=TRUE),
                          sum(dataset$k02ikä == 2 & round(dataset$k18arvio) >= 7.0 & round(dataset$k18arvio) <=  8.0, na.rm=TRUE),
                          sum(dataset$k02ikä == 2 & round(dataset$k18arvio) >= 4.0 & round(dataset$k18arvio) <=  6.0, na.rm=TRUE))
    arvio_freqs_ikä3 <- c(sum(dataset$k02ikä == 3 & round(dataset$k18arvio) >= 9.0 & round(dataset$k18arvio) <= 10.0, na.rm=TRUE),
                          sum(dataset$k02ikä == 3 & round(dataset$k18arvio) >= 7.0 & round(dataset$k18arvio) <=  8.0, na.rm=TRUE),
                          sum(dataset$k02ikä == 3 & round(dataset$k18arvio) >= 4.0 & round(dataset$k18arvio) <=  6.0, na.rm=TRUE))
    arvio_freqs_ikä4 <- c(sum(dataset$k02ikä == 4 & round(dataset$k18arvio) >= 9.0 & round(dataset$k18arvio) <= 10.0, na.rm=TRUE),
                          sum(dataset$k02ikä == 4 & round(dataset$k18arvio) >= 7.0 & round(dataset$k18arvio) <=  8.0, na.rm=TRUE),
                          sum(dataset$k02ikä == 4 & round(dataset$k18arvio) >= 4.0 & round(dataset$k18arvio) <=  6.0, na.rm=TRUE))
    arvio_labs <- c("Kiitettävä",  # 9 & 10
                    "Tyydyttävä",  # 7 & 8
                    "Välttävä")    # 5 & 6 (ja 4)
    arvio <- data.frame(Arvio=arvio_labs, Ikä1=arvio_freqs_ikä1, Ikä2=arvio_freqs_ikä2, Ikä3=arvio_freqs_ikä3, Ikä4=arvio_freqs_ikä4)
    # Tehdään datasta "pitkä" helpompaan analysointiin
    arvio_exp <- melt(arvio, id=("Arvio"))
    names(arvio_exp)[2] <- "Ikä"
    names(arvio_exp)[3] <- "Lukumäärä"
    # Tehdään ristiintaulukointi
    arvio_tabs <- xtabs(arvio_exp$Lukumäärä ~ arvio_exp$Arvio + arvio_exp$Ikä)
    arvio_summary <- summary(arvio_tabs)
    # Lasketaan kiin neliö
    sarake_nimet <- c("Arvio", "Ikä", "Lukumäärä")
    sarake_nimet_s <- c("Naiset", "Miehet")
    arvio_chisq <- chisq.test(arvio_tabs)
    print(arvio_chisq)
    arvio_observed <- data.frame(arvio_chisq$observed)
    arvio_expected <- data.frame(as.table(arvio_chisq$expected))
    arvio_residuals <- data.frame(arvio_chisq$residuals)
    names(arvio_observed) <- sarake_nimet
    names(arvio_expected) <- sarake_nimet
    names(arvio_residuals) <- sarake_nimet
    arvio_observed <- dcast(arvio_observed, Arvio ~ Ikä, value.var="Lukumäärä")
    arvio_expected <- dcast(arvio_expected, Arvio ~ Ikä, value.var="Lukumäärä")
    arvio_residuals <- dcast(arvio_residuals, Arvio ~ Ikä, value.var="Lukumäärä")
    print(arvio_observed)
    print(arvio_expected)
    print(arvio_residuals)
    plottext <- "Tässä esimerkissä kaikki ikäluokat antoivat suhteellisen samankaltaisia vastauksia."
    tekstilaatikko_khiihyväksy("0,07437", "ikäluokkien", "iällä", plottext)
}

harjoitus_6_keskiarvot <- function() {
    message("")
    message("Tehtävä 6, keskiarvot")
    message("---------------------")
    message("Ikä vs. tyytyväisyys:")
    arvio_ikä_0_20 <- dataset$k17tyyt[dataset$k02ikä == 1]
    arvio_ikä_20_35 <- dataset$k17tyyt[dataset$k02ikä == 2]
    arvio_ikä_36_49 <- dataset$k17tyyt[dataset$k02ikä == 3]
    arvio_ikä_50p <- dataset$k17tyyt[dataset$k02ikä == 4]
    arvio_ikä_0_20 <- arvio_ikä_0_20[!is.na(arvio_ikä_0_20)]
    arvio_ikä_20_35 <- arvio_ikä_20_35[!is.na(arvio_ikä_20_35)]
    arvio_ikä_36_49 <- arvio_ikä_36_49[!is.na(arvio_ikä_36_49)]
    arvio_ikä_50p <- arvio_ikä_50p[!is.na(arvio_ikä_50p)]
    keskiarvo1 <- mean(arvio_ikä_0_20)
    print(keskiarvo1)
    keskiarvo2 <- mean(arvio_ikä_20_35)
    print(keskiarvo2)
    keskiarvo3 <- mean(arvio_ikä_36_49)
    print(keskiarvo3)
    keskiarvo4 <- mean(arvio_ikä_50p)
    print(keskiarvo4)
    message("Ikä vs. kokonaisarvosana:")
    arvio_ikä_0_20 <- dataset$k18arvio[dataset$k02ikä == 1]
    arvio_ikä_20_35 <- dataset$k18arvio[dataset$k02ikä == 2]
    arvio_ikä_36_49 <- dataset$k18arvio[dataset$k02ikä == 3]
    arvio_ikä_50p <- dataset$k18arvio[dataset$k02ikä == 4]
    arvio_ikä_0_20 <- arvio_ikä_0_20[!is.na(arvio_ikä_0_20)]
    arvio_ikä_20_35 <- arvio_ikä_20_35[!is.na(arvio_ikä_20_35)]
    arvio_ikä_36_49 <- arvio_ikä_36_49[!is.na(arvio_ikä_36_49)]
    arvio_ikä_50p <- arvio_ikä_50p[!is.na(arvio_ikä_50p)]
    keskiarvo1 <- mean(arvio_ikä_0_20)
    print(keskiarvo1)
    keskiarvo2 <- mean(arvio_ikä_20_35)
    print(keskiarvo2)
    keskiarvo3 <- mean(arvio_ikä_36_49)
    print(keskiarvo3)
    keskiarvo4 <- mean(arvio_ikä_50p)
    print(keskiarvo4)
}

harjoitus_6_korrelaatio <- function() {
    message("")
    message("Tehtävä 6, korrelaatiot")
    message("-----------------------")
    message("Ikä vs. tyytyväisyys:")
    plot(main="Hajontakuvio tyytyväisyys vs. ikä", xlab="Tyytyväisyys", ylab="Ikä", dataset$k17tyyt, dataset$k02ikä)
    korrelaatio <- cor.test(dataset$k17tyyt, dataset$k02ikä, method="spearman")
    print(korrelaatio)
    plottext <- ""
    tekstilaatikko_spearmanhyväksy("0,539", "iän ja tyytyväisyyden", plottext)
    message("Ikä vs. kokonaisarvosana:")
    plot(main="Hajontakuvio arvosana vs. ikä", xlab="Arvosana", ylab="Ikä", dataset$k18arvio, dataset$k02ikä)
    korrelaatio <- cor.test(dataset$k18arvio, dataset$k02ikä, method="spearman")
    print(korrelaatio)
    plottext <- ""
    tekstilaatikko_spearmanhyväksy("0,7875", "iän ja arvosanojen", plottext)
}

harjoitus_2 <- function() {
    harjoitus_2_1()
    harjoitus_2_2()
    harjoitus_2_3()
    harjoitus_2_4()
    harjoitus_2_5()
    harjoitus_2_6()
}

harjoitus_3 <- function() {
    message("")
    message("Tehtävä 3")
    message("---------")
    harjoitus_3_k08aihep()
    harjoitus_3_k091_k095()
    harjoitus_3_k10kerta()
    harjoitus_3_k11tieto()
    harjoitus_3_k12lehti()
    harjoitus_3_k13konta()
    harjoitus_3_k14hank()
    harjoitus_3_k15isku()
    harjoitus_3_k16näytö()
    harjoitus_3_k17tyyt()
}

harjoitus_4 <- function() {
    harjoitus_4_1()
    harjoitus_4_23()
}

harjoitus_5 <- function() {
    harjoitus_5_1()
    harjoitus_5_1_vaikutus()
    harjoitus_5_2()
    harjoitus_5_2_vaikutus()
    harjoitus_5_3()
    harjoitus_5_3_vaikutus()
    harjoitus_5_4()
    harjoitus_5_4_vaikutus()
}

harjoitus_6 <- function() {
    harjoitus_6_1()
    harjoitus_6_1_vaikutus()
    harjoitus_6_2()
    harjoitus_6_2_vaikutus()
    harjoitus_6_3()
    harjoitus_6_3_vaikutus()
    harjoitus_6_4()
    harjoitus_6_4_vaikutus()
}

harjoitus_6x <- function() {
   harjoitus_6_keskiarvot()
   harjoitus_6_korrelaatio()
}

dataset <- harjoitus_alustus()
harjoitus_2()
harjoitus_3()
harjoitus_4()
harjoitus_5()
harjoitus_6()
harjoitus_6x()

# plottext <- "Vastausten kokonaismäärästä huomataan, että lukumääräisesti vähiten vastanneita oli 
# textplot(plottext, valign="center", cex=0.8, halign="left", mar=c(0,0,0,0), col=2)

