dataset <- read.csv(file="Asuntomyynti.csv", sep=',', dec='.')
dataset$myyvuosi <- as.integer(gsub(',', '', dataset$myyvuosi))
dataset$rakvuosi <- as.integer(gsub(',', '', dataset$rakvuosi))
dataset$hinta <- as.numeric(gsub(',', '', dataset$hinta))
dataset$neliöhinta <- as.numeric(gsub(',', '', dataset$neliöhinta))
# Laita -1 iät määrittelemättömiksi
dataset$ikä[dataset$ikä == -1] <- NA
# message("Datatyypit")
# message("----------")
# sapply(dataset, class)
# Tee intervallit
message("Neliöhintojen frekvenssit")
message("-------------------------")
nhinta_int <- c(0, 800, 1000, 1200, 1400, 1600, 1800, 2000, Inf)
nhinta_lab <- c("- 800",
                "800 - 1000",
                "1000 - 1200",
                "1200 - 1400",
                "1400 - 1600",
                "1600 - 1800",
                "1800 - 2000",
                "2000 +")
nhinta_frek <- as.data.frame(table(cut(dataset$neliöhinta, breaks=nhinta_int, labels=nhinta_lab, right=TRUE)))
colnames(nhinta_frek)[1] <- "Neliöhinta"
colnames(nhinta_frek)[2] <- "Lukumäärä"
nhinta_frek$Prosentti <- round(nhinta_frek$Lukumäärä / sum(nhinta_frek$Lukumäärä) * 100.0, 2)
nhinta_frek
message("")
message("Pinta-alojen frekvenssit")
message("------------------------")
alat_int <- c(0, 60, 80, 110, Inf)
alat_lab <- c("- 60",
              "60 - 80",
              "80 - 110",
              "110 +")
alat_frek <- as.data.frame(table(cut(dataset$neliöt, breaks=alat_int, labels=alat_lab, right=TRUE)))
colnames(alat_frek)[1] <- "Pinta-ala"
colnames(alat_frek)[2] <- "Lukumäärä"
alat_frek
message("")
message("Myyntiajankohdat")
message("----------------")
ajank1_int <- c(1999, 2000)
ajank2_int <- c(2005, 2007)
ajank1_lab <- c("1999 - 2000")
ajank2_lab <- c("2005 - 2007")
ajank1_frek <- as.data.frame(table(cut(dataset$myyvuosi, breaks=ajank1_int, labels=ajank1_lab, right=TRUE)))
ajank2_frek <- as.data.frame(table(cut(dataset$myyvuosi, breaks=ajank2_int, labels=ajank2_lab, right=TRUE)))
ajank_frek <- rbind(ajank1_frek, ajank2_frek)
colnames(ajank_frek)[1] <- "Myyntivuosi"
colnames(ajank_frek)[2] <- "Lukumäärä"
ajank_frek

