require(plotrix)
require(ggplot2)
require(reshape2)

density_geometry <- function() {
    return(c('point', 'smooth'))
}

theme_disabled <- function() {
    themedef <- theme(legend.position="none")
    return(themedef);
}

theme_disabled_x <- function() {
    themedef <- theme(axis.text.x=element_blank(), axis.ticks.x=element_blank(), legend.position="none");
    return(themedef);
}

theme_topleft <- function() {
    themedef <- theme(legend.justification=c(0,1),
                      legend.position=c(0,1))
    return(themedef)
}

theme_topleft_horizontal <- function() {
    themedef <- theme(legend.justification=c(0,1),
                      legend.position=c(0,1),
                      legend.box="horizontal",
                      legend.box.just="top")
    return(themedef)
}

theme_topright <- function() {
    themedef <- theme(legend.justification=c(1,1),
                      legend.position=c(1,1))
    return(themedef)
}

theme_bottomleft <- function() {
    themedef <- theme(legend.justification=c(0,0),
                      legend.position=c(0,0))
    return(themedef)
}

theme_bottomright <- function() {
    themedef <- theme(legend.justification=c(1,0),
                      legend.position=c(1,0))
    return(themedef)
}

convert_american <- function(column) {
    return(as.numeric(gsub(',', '', column)))
}

tekstilaatikko <- function(teksti) {
    plot.new()
    textbox(c(0,1.0), 1, margin=0.02, teksti)
}

harjoitus_alustus <- function() {
    dataset <- read.csv(file="Kunnat2007.csv", sep=',', dec='.')
    dataset$Asukasluku <- convert_american(dataset$Asukasluku)
    dataset$Pinta.ala <- convert_american(dataset$Pinta.ala)
    dataset$Veroaste <- convert_american(dataset$Veroaste)
    dataset$Tulotaso <- convert_american(dataset$Tulotaso)
    dataset$Naiset <- convert_american(dataset$Naiset)
    dataset$Lapset <- convert_american(dataset$Lapset)
    dataset$Seniorit <- convert_american(dataset$Seniorit)
    dataset$Koulutus <- convert_american(dataset$Koulutus)
    # message("Datatyypit")
    # message("----------")
    # print(sapply(dataset, class))
    # print(dataset)
    return(dataset)
}

harjoitus_1 <- function() {
    message("Tehtävä 1")
    message("---------")
    message("Kuinka monta kuntaa sijaitsee Länsi-Suomen läänissä?:")
    kunnat <- sum(dataset$Lääni == 1, na.rm=TRUE)
    vastaus <- sprintf("  Länsi-Suomen läänissä sijaitsee %i kuntaa.", kunnat)
    message(vastaus)
    message("Montako prosenttia Länsi-Suomen lääni on kaikista Suomen kunnista?:")
    osuus <- (kunnat / nrow(dataset)) * 100.0
    vastaus <- sprintf("  Länsi-Suomen lääni on %.2f%% kaikista Suomen kunnista.", osuus)
    message(vastaus)
}

harjoitus_2 <- function() {
    message("Tehtävä 2")
    message("---------")
    taulu_int <- c(0, 18.00, 19.00, 20.00, Inf)
    taulu_lab <- c("Alle 18,00", "18,00-18,99", "19,00-19,99", "20,00-")
    taulu_freq <- data.frame(table(cut(dataset$Veroaste, breaks=taulu_int, labels=taulu_lab, right=FALSE)))
    names(taulu_freq)[1] <- "Tuloveroprosentti"
    names(taulu_freq)[2] <- "Lukumäärä"
    taulu_freq$Prosentti <- round((taulu_freq$Lukumäärä / nrow(dataset)) * 100.0, 2)
    print(taulu_freq)
    # TODO: KIRJOITA TAULUKKO TIEDOSTOON!
}

harjoitus_3 <- function() {
    message("Tehtävä 3")
    message("---------")
    message("Mikä on kuntien pienin asukasluku?:")
    asukasluku_min <- min(dataset$Asukasluku)
    vastaus <- sprintf("  Pienin asukasluku on %i asukasta.", asukasluku_min)
    message(vastaus)
    message("Mikä on kuntien suurin asukasluku?:")
    asukasluku_max <- max(dataset$Asukasluku)
    vastaus <- sprintf("  Suurin asukasluku on %i asukasta.", asukasluku_max)
    message(vastaus)
    message("Kuinka monessa prosentissa kunnista asukasluku on alle 50 000?:")
    alle50k <- sum(dataset$Asukasluku < 50000, na.rm=TRUE)
    prosentti <- (alle50k / nrow(dataset)) * 100.0
    vastaus <- sprintf("  Asukasluku on alle 50 000 n. %.2f%% tapauksista.", prosentti)
    message(vastaus)
    message("Mikä on kuntien keskimääräinen asukasluku?:")
    asukasluku_kesk <- round(mean(dataset$Asukasluku, na.rm=TRUE))
    vastaus <- sprintf("  Kuntien keskimääräinen asukasluku on n. %i henkilöä.", asukasluku_kesk)
    message(vastaus)
    message("Mikä on kuntien asukaslukujen mediaani?:")
    asukasluku_median <- round(median(dataset$Asukasluku, na.rm=TRUE))
    vastaus <- sprintf("  Kuntien asukaslukujen mediaani on n. %i henkilöä.", asukasluku_median)
    message(vastaus)
    message("Mikä on kuntien asukaslukujen keskihajonnan suuruus?:")
    asukasluku_sd <- round(sd(dataset$Asukasluku, na.rm=TRUE))
    vastaus <- sprintf("  Kuntien asukaslukujen keskihajonnan suuruus on n. %i henkilöä.", asukasluku_sd)
    message(vastaus)
    message("Millaisena pidät kuntien asukaslukujen keskihajonnan suuruutta?:")
    vastaus <- "  Asukaslukujen keskihajonta on suurta ja tukee havaintoa keskimääräisestä/mediaanista asukasluvusta."
    message(vastaus)
    message("Onko kuntien asukaslukujakauma symmetrinen, oikealle vino vai vasemmalle vino?:")
    message("  Kuntien asukaslukujakauma on oikealle vino.")
    message("Mistä päättelit kuntien asulukujakauman vinouden?:")
    vinous <- skewness(dataset$Asukasluku, na.rm=TRUE)
    vastaus <- sprintf("  Vinouden voimme päätellä vinousmitasta joka oli %.2f sekä keskiarvon ja mediaanin suhteesta, eli keskimääräinen asukasluku oli paljon mediaania suurempi.", vinous)
    message(vastaus)
    message("Havainnollistus:")
    print(qplot(main="Asukaslukujen tiheysfunktio", data=dataset, x=Asukasluku, geom="density", ylab="Tiheys", color=0, fill=0, alpha=I(0.5)) + theme(legend.position="none"))
}

harjoitus_4 <- function() {
    message("Tehtävä 4")
    message("---------")
    message("Luokittele kuntien asukasluvut sopivalla tavalla:")
    # minimi <- min(dataset$Asukasluku)
    # maksimi <- max(dataset$Asukasluku)
    # print(minimi)
    # print(maksimi)
    # test <- data.frame(table(cut(dataset$Asukasluku, breaks=seq(0, 580000, by=10000))))
    # print(test)
    # test <- data.frame(table(cut(dataset$Asukasluku, breaks=c(0, 5000, 10000, Inf))))
    # print(test)
    vastaus <- data.frame(table(cut(dataset$Asukasluku, breaks=c(0, 2500, 5000, 10000, Inf))))
    print(vastaus)
}

harjoitus_5 <- function() {
    message("Tehtävä 5")
    message("---------")
    kaupungit <- dataset[dataset$Kuntamuoto == 1,]
    muutkunnat <- dataset[dataset$Kuntamuoto == 2,]
    rivit_labs <- c("Kaupunki", "Muu kunta", "Kaikki")
    lkm <- c(sum(kaupungit$Asukasluku, na.rm=TRUE),
             sum(muutkunnat$Asukasluku, na.rm=TRUE),
             sum(dataset$Asukasluku, na.rm=TRUE))
    asukas_mean <- c(mean(kaupungit$Asukasluku, na.rm=TRUE),
                     mean(muutkunnat$Asukasluku, na.rm=TRUE),
                     mean(dataset$Asukasluku, na.rm=TRUE))
    asukas_median <- c(median(kaupungit$Asukasluku, na.rm=TRUE),
                       median(muutkunnat$Asukasluku, na.rm=TRUE),
                       median(dataset$Asukasluku, na.rm=TRUE))
    asukas_min <- c(min(kaupungit$Asukasluku, na.rm=TRUE),
                    min(muutkunnat$Asukasluku, na.rm=TRUE),
                    min(dataset$Asukasluku, na.rm=TRUE))
    asukas_max <- c(max(kaupungit$Asukasluku, na.rm=TRUE),
                    max(muutkunnat$Asukasluku, na.rm=TRUE),
                    max(dataset$Asukasluku, na.rm=TRUE))
    asukas_sd <- c(sd(kaupungit$Asukasluku, na.rm=TRUE),
                   sd(muutkunnat$Asukasluku, na.rm=TRUE),
                   sd(dataset$Asukasluku, na.rm=TRUE))
    taulukko <- data.frame(Kuntamuoto=rivit_labs, Lkm=lkm, Keskim.asukas=asukas_mean, Mediaani.asukas=asukas_median, Minimi=asukas_min, Maksimi=asukas_max, Hajonta=asukas_sd)
    print(taulukko)
    vastaus <- "  Kaupungissa asuvien yhteismäärä on n. kaksinkertainen verrattuna muualla asuviin. Kaupungeissa myös asuu keskimäärin 7 kertaa enemmän asukkaita kuin muissa kunnissa. Suurimman kaupungin asukasmäärä on n. 600 000 kun taas muualla n. 40 000. Keskihajotan on kaupungeissa n. 12 kertaa suurempaa kuin muualla."
    message(vastaus)
}

harjoitus_6 <- function() {
    # https://en.wikipedia.org/wiki/Coefficient_of_determination
    message("Tehtävä 6")
    message("---------")
    tulotaso_vs_koulutus <- data.frame(Tulotaso=dataset$Tulotaso, Koulutus=dataset$Koulutus)
    tulotaso_vs_koulutus <- na.omit(tulotaso_vs_koulutus)
    korrelaatio <- cor(tulotaso_vs_koulutus$Tulotaso, tulotaso_vs_koulutus$Koulutus)
    vastaus <- sprintf("  Korrelaatiokerroin on %f.", korrelaatio)
    message(vastaus)
    print(qplot(data=dataset, x=Koulutus, y=Tulotaso, size=Asukasluku, colour=Asukasluku, geom=density_geometry(), method='loess') + theme_topleft_horizontal())
    message("Selitä, mitä saamastasi korrelaatiokertoimesta voit päätellä:")
    plottext <- "Tässä tapauksessa saimme korrelaatiokertoimeksi 0,8518685. Tämä tarkoittaa, että yhden muuttujan arvon kasvaesssa myös toinen muuttuja kasvaa. Muuttujien välinen sidos oli lähes täydellistä (+1) eikä arvojen välillä ollut juurikaan hajontaa."
    tekstilaatikko(plottext)
    message("Mikä on selitysaste ja mitä se tarkoittaa?:")
    lmalli <- lm(data=dataset, formula=Koulutus~Tulotaso)
    lmalli_summary <- summary.lm(lmalli)
    print(lmalli_summary)
    vastaus <- sprintf("  Selitysasteella eli R:n neliöllä tarkoitetaan miten hyvin lineaarinen malli selittää saatuja arvoja. Arvo 1 tarkoittaa, että lineaarinen malli selittää saatuja arvoja täydellisesti. Tässä tapauksessa saimme arvon %f.", lmalli_summary$r.squared)
    message(vastaus)
    message("Mitä Sig.-arvo ja N:n arvo tässä tarkoittavat?:")
    message("  N:n arvo tarkoittaa SPSS:ä vastinparien määrää. Sig. tarkoittaa merkitsevyysastetta, joka R:llä saamme F-testilllä. Saimme p-arvoksi alle 2 x 10^(-16), mikä tarkoittaa tilastollisesti erittäin merkitsevää, sillä null-hypoteesi on, että testi noudattaa F-jakaumaa.")
    message("Tutki muidenkin muuttujien välisiä korrelaatioita. Käytä vain mielekkäitä muuttujia:")
    lmalli <- lm(data=dataset, formula=Koulutus~Asukasluku)
    lmalli_summary <- summary.lm(lmalli)
    print(lmalli_summary)
    lmalli <- lm(data=dataset, formula=Koulutus~Pinta.ala)
    lmalli_summary <- summary.lm(lmalli)
    print(lmalli_summary)
    lmalli <- lm(data=dataset, formula=Lapset~Seniorit)
    lmalli_summary <- summary.lm(lmalli)
    print(lmalli_summary)
    lmalli <- lm(data=dataset, formula=Naiset~Lapset)
    lmalli_summary <- summary.lm(lmalli)
    print(lmalli_summary)
    print(qplot(data=dataset, x=Koulutus, y=Tulotaso, size=Asukasluku, colour=Asukasluku, geom=density_geometry(), method='loess') + theme_topleft_horizontal())
    print(qplot(data=dataset, x=Koulutus, y=Tulotaso, size=Pinta.ala, colour=Pinta.ala, geom=density_geometry(), method='loess') + theme_topleft_horizontal())
    print(qplot(data=dataset, x=Koulutus, y=Tulotaso, size=Veroaste, colour=Veroaste, geom=density_geometry(), method='loess') + theme_topleft_horizontal())
    print(qplot(data=dataset, x=Koulutus, y=Asukasluku, size=Pinta.ala, colour=Pinta.ala, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Koulutus, y=Asukasluku, size=Veroaste, colour=Veroaste, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Koulutus, y=Asukasluku, size=Tulotaso, colour=Tulotaso, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Koulutus, y=Pinta.ala, size=Asukasluku, colour=Asukasluku, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Koulutus, y=Pinta.ala, size=Tulotaso, colour=Tulotaso, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Koulutus, y=Pinta.ala, size=Veroaste, colour=Veroaste, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Lapset, y=Seniorit, size=Asukasluku, colour=Asukasluku, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Lapset, y=Seniorit, size=Pinta.ala, colour=Pinta.ala, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Lapset, y=Seniorit, size=Veroaste, colour=Veroaste, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Lapset, y=Seniorit, size=Koulutus, colour=Koulutus, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Naiset, y=Lapset, size=Asukasluku, colour=Asukasluku, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Naiset, y=Lapset, size=Pinta.ala, colour=Pinta.ala, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Naiset, y=Lapset, size=Veroaste, colour=Veroaste, geom=density_geometry(), method='loess') + theme_topright())
    print(qplot(data=dataset, x=Naiset, y=Lapset, size=Koulutus, colour=Koulutus, geom=density_geometry(), method='loess') + theme_topright())
    message("Analysoi tuloksia:")
    vastaus <- "Tutkituista muuttujista parhaiten keskenään korreloivat koulutus ja tulotaso, koulutus ja asukasluku sekä lapset ja seniorit. Näistä suurimman R-arvon järjestyksessä seuraavat: koulutus ja tulotaso, lapset ja seniorit sekä koulutus ja asukasluku."
    tekstilaatikko(vastaus)
}

harjoitus_7 <- function() {
    message("Tehtävä 7")
    message("---------")
    message("Selvitä ristiintaulukoimalla, kuinka suuri osa eri lääneissä on kaupunkeja ja muita kuntia:")
    läänit_lab <- c("Etelä-Suomen lääni",
                    "Länsi-Suomen lääni",
                    "Itä-Suomen lääni",
                    "Oulun lääni",
                    "Lapin lääni",
                    "Ahvenanmaa")
    osuudet_freqs_tbl <- table(dataset$Lääni, dataset$Kuntamuoto)
    osuudet_props_tbl <- prop.table(osuudet_freqs_tbl, 2)
    osuudet_freqs <- data.frame(Lääni=läänit_lab, Kaupunki=osuudet_freqs_tbl[,"1"], Muukunta=osuudet_freqs_tbl[,"2"])
    osuudet_props <- data.frame(Lääni=läänit_lab, Kaupunki=osuudet_props_tbl[,"1"], Muukunta=osuudet_props_tbl[,"2"])
    osuudet_props$Kaupunki = osuudet_props$Kaupunki * 100.0
    osuudet_props$Muukunta = osuudet_props$Muukunta * 100.0
    print(osuudet_freqs)
    print(osuudet_props)
}

harjoitus_8 <- function() {
    message("Tehtävä 8")
    message("---------")
    message("Esitä sopivalla tavalla keskimääräisten verotettavien tulojen jakauma (€/asukas) Suomen kunnissa:")
    print(qplot(main="Tulojen tiheysfunktio", data=dataset, Tulotaso, ylab="Tiheys", geom="density", color=0, fill=0, alpha=.5) + theme_disabled())
    print(qplot(main="Tulojen violinplot", data=dataset, y=Tulotaso, x=1, xlab="", geom="violin", color=0, fill=0, alpha=.5) + theme_disabled_x())
    print(qplot(main="Tulojen boxplot", data=dataset, y=Tulotaso, x=1, xlab="", geom="boxplot", color=0, fill=0, alpha=.5) + theme_disabled_x())
}

harjoitus_9 <- function() {
    message("Tehtävä 9")
    message("---------")
    message("Vertaile muuttujien saamia arvoja lääneittäin sopivia menetelmiä käyttäen:")

    

}

pdf("harjoituksia4.pdf", paper="default")
dataset <- harjoitus_alustus()
# harjoitus_1()
# harjoitus_2()
# harjoitus_3()
# harjoitus_4()
# harjoitus_5()
# harjoitus_6()
# harjoitus_7()
harjoitus_8()
# harjoitus_9()
invisible(dev.off())
