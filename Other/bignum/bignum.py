#!/usr/bin/python

# Implementing grade school algorithms

class Bignum:
    numa = ""
    numb = ""
    lena = 0
    lenb = 0
    lens = 0
    def do_initialize( self, numa, numb ):
        self.numa = numa
        self.numb = numb
        self.lena = len( numa )
        self.lenb = len( numb )
    def do_fill_zeros( self ):
        self.lens = self.lena
        if self.lenb > self.lena:
            self.numa = (self.lenb-self.lena)*"0" + self.numa
            self.lena = self.lenb
            self.lens = self.lenb
        if self.lena > self.lenb:
            self.numb = (self.lena-self.lenb)*"0" + self.numb
            self.lenb = self.lena
            self.lens = self.lena
    def do_add( self, numa, numb, do_init, do_fill ):
        print "Add: " + numa + " + " + numb
        res = ""
        carry = 0
        if do_init:
            self.do_initialize( numa, numb )
        if do_fill:
            self.do_fill_zeros()
        numa = self.numa
        numb = self.numb
        lens = self.lens
        for i in reversed( range( 0, lens ) ):
            numaidx = numa[i]
            numbidx = numb[i]
            numaidx_i = int(numaidx)
            numbidx_i = int(numbidx)
            res_tmp = numaidx_i + numbidx_i + carry
            carry_old = carry
            carry = res_tmp / 10
            res_new = res_tmp - ( carry * 10 )
            res = str(res_new) + res
            print "[" + str(i) + "]: " + numaidx + " + " + numbidx + " + (" + str(carry_old) + ") = " + str(res_new)
        if carry > 0:
            res = str(carry) + res
            print "[-]:" + " (" + str(carry) + ")" + " = " + str(carry)
        print "Add result: " + res
        return res
    def do_subtract( self, numa, numb, do_init, do_fill ):
        # Preconditions:
        # numa >= numb
        print "Subtract: " + numa + " - " + numb
        res = ""
        carry = 0
        if do_init:
            self.do_initialize( numa, numb )
        if do_fill:
            self.do_fill_zeros()
        numa = self.numa
        numb = self.numb
        lens = self.lens
        for i in reversed( range( 0, lens ) ):
            numaidx = numa[i]
            numbidx = numb[i]
            numaidx_i = int(numaidx)
            numbidx_i = int(numbidx)
            res_tmp = ""
            carry_old = carry
            if numaidx_i < numbidx_i + carry:
                res_new = 10 + numaidx_i - numbidx_i - carry
                carry = 1
            else:
                res_new = numaidx_i - numbidx_i - carry
                carry = 0
            res = str(res_new) + res
            print "[" + str(i) + "]: " + numaidx + " - " + numbidx + " - (" + str(carry_old) + ") = " + str(res_new)
        print "Subtract result: " + res
        return res
    def do_multiply( self, numa, numb, do_init ):
        print "Multiply: " + numa + " * " + numb
        if do_init:
            self.do_initialize( numa, numb )
        mult = 0
        cumul_add = ""
        numa = self.numa
        numb = self.numb
        lena = self.lena
        lenb = self.lenb
        for j in reversed( range( 0, lenb ) ):
            res = ""
            carry = 0
            for i in reversed( range( 0, lena ) ):
                numaidx = numa[i]
                numbidx = numb[j]
                numaidx_i = int(numaidx)
                numbidx_i = int(numbidx)
                res_tmp = numaidx_i * numbidx_i + carry
                carry_old = carry
                carry = res_tmp / 10
                res_new = res_tmp - ( carry * 10 )
                res = str(res_new) + res
                print "[" + str(i) + "]: " + numaidx + " * [" + str(j) + "]:" + numbidx + " + (" + str(carry_old) + ") = " + str(res_new)
            if carry > 0:
                res = str(carry) + res
                print "[-]:" + " (" + str(carry) + ")" + " = " + str(carry)
            res = res + mult * "0"
            if cumul_add != "":
                cumul_add = self.do_add( cumul_add, res, True, True )
            else:
                cumul_add = res
            mult = mult + 1
        print "Multiply result: " + cumul_add
        return cumul_add
    def add( self, numa, numb ):
        res = self.do_add( numa, numb, True, True )
        return res
    def subtract( self, numa, numb ):
        res = self.do_subtract( numa, numb, True, True )
        return res
    def multiply( self, numa, numb ):
        res = self.do_multiply( numa, numb, True )
        return res

numa = "987654"
numb = "121"
bignum = Bignum()
bignum.subtract( numa, numb )

