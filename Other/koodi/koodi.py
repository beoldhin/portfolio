#!/usr/bin/python
import fileinput
import time
import sys

NUMBER_OF_FILE_ENTRIES    = 5
IBAN_LENGTH_FINLAND       = 16
IBAN_COUNTRY_FINLAND      = "FI"
EUROS_CHARACTER_LIMIT     = 6
CENTS_CHARACTER_LIMIT     = 2
REFERENCE_CHARACTER_LIMIT = 20
CURRENT_TIME_TAG          = "<current>"

fileData = []

def handle_iban_number( iban ):
    ibanCountry = iban[:2]
    ibanNumber = iban[2:]
    ibanNumberLen = len( ibanNumber )
    if ibanNumberLen!=IBAN_LENGTH_FINLAND or ibanCountry!=IBAN_COUNTRY_FINLAND:
        return "0"
    return ibanNumber

def handle_payable_amount( payable ):
    payableSplit = payable.partition( "." )
    euros = payableSplit[0]
    cents = payableSplit[2]
    eurosLen = len(euros)
    centsLen = len(cents)
    if eurosLen==0 or centsLen==0:
        return "0"
    if eurosLen>EUROS_CHARACTER_LIMIT or centsLen!=CENTS_CHARACTER_LIMIT:
        return "0"
    zeroPadding = ( EUROS_CHARACTER_LIMIT - eurosLen ) * '0'
    return zeroPadding + euros + cents

def handle_reference_number( reference ):
    referenceLen = len( reference )
    if referenceLen > REFERENCE_CHARACTER_LIMIT:
        return "0"
    zeroPadding = ( REFERENCE_CHARACTER_LIMIT - referenceLen ) * '0'
    return zeroPadding + reference

def handle_payable_date( payable ):
    if payable == CURRENT_TIME_TAG:
        timeStruct = time.localtime();
    else:
        timeStruct = time.strptime( payable, "%Y-%m-%d" )
    year = timeStruct.tm_year % 100;
    month = timeStruct.tm_mon;
    day = timeStruct.tm_mday;
    return "%02d%02d%02d" % ( year, month, day )

for line in fileinput.input():
    trimmedLine = line.strip()
    trimmedLineLen = len( trimmedLine )
    if trimmedLineLen == 0:
        continue
    fileData.append(trimmedLine)
fileDataLen = len(fileData)

if fileDataLen != NUMBER_OF_FILE_ENTRIES:
    print "Invalid amount of entries (" + str(fileDataLen) + ")"
    sys.exit(-1)

input_version_number   = fileData[0]
input_iban_number      = fileData[1]
input_payable_amount   = fileData[2]
input_reference_number = fileData[3]
input_payable_date     = fileData[4]

if input_version_number != "4":
    print "Invalid symbol version number"
    sys.exit(-1)
version_number = input_version_number;

iban_number = handle_iban_number( input_iban_number )
if iban_number == "0":
    print "Invalid IBAN number"
    sys.exit(-1)

payable_amount = handle_payable_amount( input_payable_amount )
if payable_amount == "0":
    print "Invalid payable amount"
    sys.exit(-1)

reference_number = handle_reference_number( input_reference_number )
if reference_number == "0":
    print "Invalid reference number"
    sys.exit(-1)

payable_date = handle_payable_date( input_payable_date )
if payable_date == "0":
    print "Invalid payable date"
    sys.exit(-1)

reserved = "000"
version4_result = version_number + iban_number + payable_amount + reserved + reference_number + payable_date
print version4_result

