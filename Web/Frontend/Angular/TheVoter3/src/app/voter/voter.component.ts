import { Component, OnInit } from '@angular/core';

import { VoterService } from '../voter.service';
import { Candidate } from '../candidate';
import { Vote } from '../vote';

import 'rxjs/add/observable/throw';

enum VoterState {
  Uninitialized = 0,
  Running = 1,
  Succeeded = 2,
  Failed = 3
}

@Component({
  selector: 'app-voter',
  templateUrl: 'voter.component.html',
  styleUrls: ['voter.component.css']
})
export class VoterComponent implements OnInit {

  getCandidatesResp: Candidate[];
  getCandidatesState: VoterState = VoterState.Uninitialized;
  getCandidatesErrMsg: string = "";

  makeVoteResp: Vote[];
  makeVoteState: VoterState = VoterState.Uninitialized;
  makeVoteErrMsg: string = "";

  getVotedResp: Vote[];
  getVotedState: VoterState = VoterState.Uninitialized;
  getVotedErrMsg: string = "";

  constructor(private voterService: VoterService) {
  }

  ngOnInit() {
    // var vote = {
    //   votedid: 43287432
    // }
    // this.makeVote(vote);
    // console.log(this.makeVoteResp);
    //
    // this.getVoted();
    this.getVoted();
    this.getCandidates();
    // this.getVoted();
    // console.log(this.voted);
    // var vote = {
    //   votedid: 43287432
    // }
    // this.makeVote(vote);
  }

  clicked(identifier) {
    var vote = {
      votedid: identifier
    }
    this.makeVote(vote);
  }

  resetCandidates() {
    this.getCandidatesResp = [];
    this.getCandidatesState = VoterState.Uninitialized;
    this.getCandidatesErrMsg = "";
  }

  resetMakeVote() {
    this.makeVoteResp = [];
    this.makeVoteState = VoterState.Uninitialized;
    this.makeVoteErrMsg = "";
  }

  resetGetVoted() {
    this.getVotedResp = [];
    this.getVotedState = VoterState.Uninitialized;
    this.getVotedErrMsg = "";
  }

  resetAll() {
    this.resetCandidates();
    this.resetMakeVote();
    this.resetGetVoted();
  }

  getCandidates() {
    this.resetCandidates();
    this.resetMakeVote();
    this.getCandidatesState = VoterState.Running;
    this.voterService.getCandidates()
        .subscribe(
         candidates => {
           this.getCandidatesResp = candidates;
           this.getCandidatesState = VoterState.Succeeded;
           this.getCandidatesErrMsg = "";
         },
         error => {
           this.getCandidatesResp = [];
           this.getCandidatesState = VoterState.Failed;
           this.getCandidatesErrMsg = <any>error;
         });
  }

  makeVote(vote) {
    this.resetMakeVote();
    this.makeVoteState = VoterState.Running;
    this.voterService.makeVote(vote)
        .subscribe(
          voted => {
            this.makeVoteResp = voted;
            this.makeVoteState = VoterState.Succeeded;
            this.makeVoteErrMsg = "";
            // Update directly without having to call this.getVoted() here
            this.getVotedResp = voted;
            this.getVotedState = VoterState.Succeeded;
            this.getVotedErrMsg = "";
          },
          error => {
            this.makeVoteResp = [];
            this.makeVoteState = VoterState.Failed;
            this.makeVoteErrMsg = <any>error;
          });
  }

  getVoted() {
    this.resetGetVoted();
    this.getVotedState = VoterState.Running;
    this.voterService.getVoted()
        .subscribe(
          voted => {
            this.getVotedResp = voted;
            this.getVotedState = VoterState.Succeeded;
            this.getVotedErrMsg = "";
          },
          error => {
            this.getVotedResp = [];
            this.getVotedState = VoterState.Failed;
            this.getVotedErrMsg = <any>error;
          });
  }

}

