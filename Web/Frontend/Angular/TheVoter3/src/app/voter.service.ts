import { Injectable } from '@angular/core';

import { Auth } from './auth.service';
import { AuthHttp } from 'angular2-jwt';

import { Response } from '@angular/http';
import { Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { Candidate } from './candidate';
import { Vote } from './vote';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class VoterService {

  private candidatesUrl = 'http://localhost:3000/secured/candidates';
  private makeVoteUrl = 'http://localhost:3000/secured/makevote';
  private getVotedUrl = 'http://localhost:3000/secured/getvoted';

  constructor(private auth: Auth, private authHttp: AuthHttp) {
  }

  getCandidates(): Observable<Candidate[]> {
    return this.authHttp.get(this.candidatesUrl)
                        .map(this.extractData)
                        .catch(this.handleError);
  }

  makeVote(vote) {
    var headers = this.getPutHeaders();
    var content = JSON.stringify(vote);
    return this.authHttp.put(this.makeVoteUrl, content, {headers: headers})
               .map(this.extractData)
               .catch(this.handleError);
  }

  getVoted(): Observable<Vote[]> {
    return this.authHttp.get(this.getVotedUrl)
                        .map(this.extractData)
                        .catch(this.handleError);
  }

  private getPutHeaders() {
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return headers;
  }

  private extractData(response: Response) {
    var body = response.json();
    return body;
  }

  private handleError(error: any) {
    var errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

}

