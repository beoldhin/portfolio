export class Candidate {
  identifier: number;
  party: string;
  firstname: string;
  lastname: string;
  age: number;
  occupation: string;
  hometown: string;
  motto: string;
  image_url: string;
}

