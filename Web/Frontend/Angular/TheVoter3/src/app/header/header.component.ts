import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Auth } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private auth: Auth, private router: Router) { }

  ngOnInit() {
  }

  login() {
    this.auth.login();
    this.router.navigate(['voter']);
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['brand']);
  }
}
