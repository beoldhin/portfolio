How to make auth0 working (demo project in https://auth0.com/docs/quickstart/spa/angular2/01-login):

1. Create new project: "ng new AuthTest"
2. Go to the new project: "cd AuthTests"
3. Replace all ".rc4" to ".rc5" in packages.json in order to avoid the "unmet dependecy" message
4. "npm install" (because just "npm update" may not be sufficient)
5. Install the login UI: "npm install --save auth0-lock"
6. Install the web token package: "npm install --save angular2-jwt"
7. Create new service "AuthService": "ng generate service Auth"
8. Copy the contents from demo project's auth.service.ts and rename "Auth" -> "AuthService"
9. Create new component "AuthComponent: "ng generate component Auth"
10. Copy the contents from demo project's app.template.html to auth.component.html (only the "nav" part)
11. Add "import { AuthComponent} from './auth';" to app.component.ts
12. Add "<app-auth></app-auth>" to app.component.html
13. Add "providers: [ AuthService ]," to app.component.ts
14. Add "import { AuthService } to app.component.ts
15. Add "constructor(private authService: AuthService) {}" to app.component.ts
16. 
