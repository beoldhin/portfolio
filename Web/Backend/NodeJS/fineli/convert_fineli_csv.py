#!/usr/bin/env python

import csv
import sys

class Converter:
    from_name="database/Fineli_Rel17_open/"
    to_name="database/converted/"
    def __init__(self, filename):
        self.from_name = self.from_name + filename
        self.to_name = self.to_name + filename
    def convert(self):
        print("Converting to: {}".format(self.to_name));
        with open(self.from_name, 'rb') as readfile:
            with open(self.to_name, 'wb') as writefile:
                reader = csv.reader(readfile, delimiter=';')
                for row in reader:
                    for i in range(len(row)):
                        column = row[i].decode('iso-8859-1').encode('utf8');
                        if column.find(',') != -1:
                            writefile.write('\"');
                            writefile.write(column);
                            writefile.write('\"');
                        else:
                            writefile.write(column);
                        if i < len(row) - 1:
                            writefile.write(',');
                    writefile.write("\n");

if len(sys.argv) != 2:
    print("Invalid number of arguments.");
    sys.exit(1);
filename=sys.argv[1]

converter = Converter(filename);
converter.convert();

