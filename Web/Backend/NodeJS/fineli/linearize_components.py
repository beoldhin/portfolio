#!/usr/bin/env python

import csv

class LinearizeComponents:
    languages = ['EN', 'FI', 'SV']
    language = None
    from_path = "database/converted"
    to_path = "database/linearized"
    def write_column(self, writefile, column):
        if column.find(',') != -1:
            writefile.write('\"')
            writefile.write(column)
            writefile.write('\"')
        else:
            writefile.write(column)
    def get_EUFDNAME(self, column):
        from_name = "{}/eufdname_{}.csv".format(self.from_path, self.language)
        with open(from_name, 'rb') as readfile:
            reader = csv.reader(readfile, delimiter=',')
            for row in reader:
                found = False;
                for i in range(len(row)):
                    if i==0 and row[i]==column:  # THSCODE
                        found = True;
                    if i==1 and found==True:  # DESCRIPT
                        return row[i];
        print("ERROR: {}".format(column))
        return None
    def get_CMPCLASS(self, column):
        from_name = "{}/cmpclass_{}.csv".format(self.from_path, self.language)
        with open(from_name, 'rb') as readfile:
            reader = csv.reader(readfile, delimiter=',')
            for row in reader:
                found = False
                for i in range(len(row)):
                    if i==0 and row[i]==column:  # THSCODE
                        found = True;
                    if i==1 and found==True:  # DESCRIPT
                        return row[i];
        print("ERROR: {}".format(column))
        return None;
    def linearize_column(self, column, index):
        if index == 0:
            return self.get_EUFDNAME(column)
        elif index == 2 or index == 3:
            return self.get_CMPCLASS(column)
        return column;
    def linearize_one(self):
        from_name = "{}/component.csv".format(self.from_path)
        to_name = "{}/component_{}.csv".format(self.to_path, self.language)
        with open(from_name, 'rb') as readfile:
            with open(to_name, 'wb') as writefile:
                reader = csv.reader(readfile, delimiter=',')
                firstrow = True
                for row in reader:
                    for i in range(len(row)):
                        if firstrow == False:
                            column = self.linearize_column(row[i], i);
                        else:
                            column = row[i].lower();
                        self.write_column(writefile, column);
                        if i < len(row) - 1:
                            writefile.write(',');
                    writefile.write("\n");
                    firstrow = False;
    def linearize(self):
        for language in self.languages:
            print("Linearizing components for language {}...".format(language))
            self.language = language
            self.linearize_one()

linearizecomponents = LinearizeComponents();
linearizecomponents.linearize();

