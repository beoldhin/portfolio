#!/usr/bin/env python

import csv

class LinearizeFoods:
    languages = ['EN', 'FI', 'SV']
    language = None
    foodid = None
    from_path = "database/converted"
    to_path = "database/linearized"
    def write_column(self, writefile, column):
        if column.find(',') != -1:
            writefile.write('\"')
            writefile.write(column)
            writefile.write('\"')
        else:
            writefile.write(column)
    def get_FOODNAME(self, column):
        from_name = "{}/foodname_{}.csv".format(self.from_path, self.language)
        with open(from_name, 'rb') as readfile:
            reader = csv.reader(readfile, delimiter=',')
            for row in reader:
                found = False
                for i in range(len(row)):
                    if i==0 and row[i]==column:  # THSCODE
                        found = True
                    if i==1 and found==True:  # DESCRIPT
                        return row[i]
        print("ERROR: {}".format(column))
        return None
    def get_FOODTYPE(self, column):
        from_name = "{}/foodtype_{}.csv".format(self.from_path, self.language)
        with open(from_name, 'rb') as readfile:
            reader = csv.reader(readfile, delimiter=',')
            for row in reader:
                found = False
                for i in range(len(row)):
                    if i==0 and row[i]==column:  # THSCODE
                        found = True
                    if i==1 and found==True:  # DESCRIPT
                        return row[i]
        print("ERROR: {}".format(column))
        return None
    def get_PROCESS(self, column):
        from_name = "{}/process_{}.csv".format(self.from_path, self.language)
        with open(from_name, 'rb') as readfile:
            reader = csv.reader(readfile, delimiter=',')
            for row in reader:
                found = False
                for i in range(len(row)):
                    if i==0 and row[i]==column:  # THSCODE
                        found = True
                    if i==1 and found==True:  # DESCRIPT
                        return row[i]
        print("ERROR: {}".format(column))
        return None
    def get_IGCLASS(self, column):
        from_name = "{}/igclass_{}.csv".format(self.from_path, self.language)
        with open(from_name, 'rb') as readfile:
            reader = csv.reader(readfile, delimiter=',')
            for row in reader:
                found = False
                for i in range(len(row)):
                    if i==0 and row[i]==column:  # THSCODE
                        found = True
                    if i==1 and found==True:  # DESCRIPT
                        return row[i]
        print("ERROR: {}".format(column))
        return None
    def get_FUCLASS(self, column):
        from_name = "{}/fuclass_{}.csv".format(self.from_path, self.language)
        with open(from_name, 'rb') as readfile:
            reader = csv.reader(readfile, delimiter=',')
            for row in reader:
                found = False
                for i in range(len(row)):
                    if i==0 and row[i]==column:  # THSCODE
                        found = True
                    if i==1 and found==True:  # DESCRIPT
                        return row[i]
        print("ERROR: {}".format(column))
        return None
    def linearize_column(self, column, index):
        if index == 0:
            self.foodid = column
        if index == 1:
            return self.get_FOODNAME(self.foodid)
        elif index == 2:
            return self.get_FOODTYPE(column)
        elif index == 3:
            return self.get_PROCESS(column)
        elif index == 5 or index == 6:
            return self.get_IGCLASS(column)
        elif index == 7 or index == 8:
            return self.get_FUCLASS(column)
        return column
    def linearize_one(self):
        from_name = "{}/food.csv".format(self.from_path)
        to_name = "{}/food_{}.csv".format(self.to_path, self.language)
        with open(from_name, 'rb') as readfile:
            with open(to_name, 'wb') as writefile:
                reader = csv.reader(readfile, delimiter=',')
                firstrow = True
                for row in reader:
                    for i in range(len(row)):
                        if firstrow == False:
                            column = self.linearize_column(row[i], i)
                        else:
                            column = row[i].lower()
                        self.write_column(writefile, column)
                        if i < len(row) - 1:
                            writefile.write(',')
                    writefile.write("\n")
                    firstrow = False
    def linearize(self):
        for language in self.languages:
            print("Linearizing foods for language {}...".format(language))
            self.language = language
            self.linearize_one()

linearizefoods = LinearizeFoods()
linearizefoods.linearize()

