const logger = require('winston');
const cors = require('cors');
const bodyParser = require('body-parser');
// const validator = require('express-validator');

module.exports = function(app) {
  logger.info('Initializing Base...');
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  // app.use(validator());
  const argv = require('minimist')(process.argv.slice(2));
  app.set('port', argv['p']);
}

