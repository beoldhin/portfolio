-- MySQL dump 10.16  Distrib 10.1.30-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: cooksupply
-- ------------------------------------------------------
-- Server version	10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `cooksupply`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `cooksupply` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `cooksupply`;

--
-- Table structure for table `Addresses`
--

DROP TABLE IF EXISTS `Addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Addresses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CountriesHaveCitiesID` int(11) NOT NULL,
  `Address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Address_UNIQUE` (`Address`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Addresses`
--

LOCK TABLES `Addresses` WRITE;
/*!40000 ALTER TABLE `Addresses` DISABLE KEYS */;
INSERT INTO `Addresses` VALUES (1,1,'Humppakatu 2C 80','2018-01-06 13:34:26','2018-01-06 13:34:28'),(3,2,'Jeppekuja 3D 12','2018-01-06 13:34:28','2018-01-06 13:34:30');
/*!40000 ALTER TABLE `Addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Cities`
--

DROP TABLE IF EXISTS `Cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cities` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `City` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `City_UNIQUE` (`City`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cities`
--

LOCK TABLES `Cities` WRITE;
/*!40000 ALTER TABLE `Cities` DISABLE KEYS */;
INSERT INTO `Cities` VALUES (1,'Helsinki','2018-01-06 13:34:15','2018-01-06 13:34:17'),(3,'Stockholm','2018-01-06 13:34:17','2018-01-06 13:34:19');
/*!40000 ALTER TABLE `Cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Countries`
--

DROP TABLE IF EXISTS `Countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Countries` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Country_UNIQUE` (`Country`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Countries`
--

LOCK TABLES `Countries` WRITE;
/*!40000 ALTER TABLE `Countries` DISABLE KEYS */;
INSERT INTO `Countries` VALUES (1,'FIN','2018-01-06 13:34:11','2018-01-06 13:34:13'),(3,'SWE','2018-01-06 13:34:13','2018-01-06 13:34:15');
/*!40000 ALTER TABLE `Countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CountriesHaveCities`
--

DROP TABLE IF EXISTS `CountriesHaveCities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CountriesHaveCities` (
  `CountryID` int(11) NOT NULL,
  `CityID` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CountryID`,`CityID`),
  KEY `FK_CountriesHaveCities_Cities_idx` (`CityID`),
  KEY `FK_CountriesHaveCities_Countries_idx` (`CountryID`),
  CONSTRAINT `FK_CountriesHaveCities_Cities` FOREIGN KEY (`CityID`) REFERENCES `Cities` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CountriesHaveCities_Countries` FOREIGN KEY (`CountryID`) REFERENCES `Countries` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CountriesHaveCities`
--

LOCK TABLES `CountriesHaveCities` WRITE;
/*!40000 ALTER TABLE `CountriesHaveCities` DISABLE KEYS */;
INSERT INTO `CountriesHaveCities` VALUES (3,3,'2018-01-06 13:34:19','2018-01-06 13:34:22');
/*!40000 ALTER TABLE `CountriesHaveCities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ingredients`
--

DROP TABLE IF EXISTS `Ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ingredients` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Ingredient` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ingredient of a processed product (only subset of FineliID information). Otherwise Fineli ID (numeric).',
  `Weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Ingredient_UNIQUE` (`Ingredient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ingredients`
--

LOCK TABLES `Ingredients` WRITE;
/*!40000 ALTER TABLE `Ingredients` DISABLE KEYS */;
/*!40000 ALTER TABLE `Ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orders`
--

DROP TABLE IF EXISTS `Orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Orders` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orders`
--

LOCK TABLES `Orders` WRITE;
/*!40000 ALTER TABLE `Orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `Orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrdersHaveProducts`
--

DROP TABLE IF EXISTS `OrdersHaveProducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrdersHaveProducts` (
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`OrderID`,`ProductID`),
  KEY `FK_OrdersHaveProducts_Products_idx` (`ProductID`),
  KEY `FK_OrdersHaveProducts_Orders_idx` (`OrderID`),
  CONSTRAINT `FK_OrdersHaveProducts_Orders` FOREIGN KEY (`OrderID`) REFERENCES `Orders` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_OrdersHaveProducts_Products` FOREIGN KEY (`ProductID`) REFERENCES `Products` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrdersHaveProducts`
--

LOCK TABLES `OrdersHaveProducts` WRITE;
/*!40000 ALTER TABLE `OrdersHaveProducts` DISABLE KEYS */;
/*!40000 ALTER TABLE `OrdersHaveProducts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PhoneNumbers`
--

DROP TABLE IF EXISTS `PhoneNumbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PhoneNumbers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PhoneNumber` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PhoneNumbers`
--

LOCK TABLES `PhoneNumbers` WRITE;
/*!40000 ALTER TABLE `PhoneNumbers` DISABLE KEYS */;
INSERT INTO `PhoneNumbers` VALUES (1,'+358123456789_123','2018-01-06 13:34:32','2018-01-06 13:34:32'),(2,'+358123456789_123','2018-01-06 13:34:32','2018-01-06 13:34:32');
/*!40000 ALTER TABLE `PhoneNumbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Products`
--

DROP TABLE IF EXISTS `Products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Contains information about product type: packaging, recipe, processed food (prepackaged), etc.',
  `Value` decimal(10,0) DEFAULT NULL COMMENT 'Base value without taxes or discounts.',
  `Metrics` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Contains Weight, Length, Area, Volume, Energy in string form.',
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Product_UNIQUE` (`Product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Products`
--

LOCK TABLES `Products` WRITE;
/*!40000 ALTER TABLE `Products` DISABLE KEYS */;
/*!40000 ALTER TABLE `Products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductsHaveIngredients`
--

DROP TABLE IF EXISTS `ProductsHaveIngredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductsHaveIngredients` (
  `ProductID` int(11) NOT NULL,
  `IngredientID` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProductID`,`IngredientID`),
  KEY `FK_ProductsHaveIngredients_Ingredients_idx` (`IngredientID`),
  KEY `FK_ProductsHaveIngredients_Products_idx` (`ProductID`),
  CONSTRAINT `FK_ProductsHaveIngredients_Ingredients` FOREIGN KEY (`IngredientID`) REFERENCES `Ingredients` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ProductsHaveIngredients_Products` FOREIGN KEY (`ProductID`) REFERENCES `Products` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductsHaveIngredients`
--

LOCK TABLES `ProductsHaveIngredients` WRITE;
/*!40000 ALTER TABLE `ProductsHaveIngredients` DISABLE KEYS */;
/*!40000 ALTER TABLE `ProductsHaveIngredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserProperties`
--

DROP TABLE IF EXISTS `UserProperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserProperties` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Property` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Role_UNIQUE` (`Property`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserProperties`
--

LOCK TABLES `UserProperties` WRITE;
/*!40000 ALTER TABLE `UserProperties` DISABLE KEYS */;
/*!40000 ALTER TABLE `UserProperties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `User` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EmailAddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `EmailAddress_UNIQUE` (`EmailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'Jamppa Tuominen','test@humppa.com','2018-01-06 13:34:22','2018-01-06 13:34:24'),(3,'Kaisa Kuusisto','jeps@gmail.com','2018-01-06 13:34:24','2018-01-06 13:34:26');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsersHaveAddresses`
--

DROP TABLE IF EXISTS `UsersHaveAddresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersHaveAddresses` (
  `UserID` int(11) NOT NULL,
  `AddressID` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserID`,`AddressID`),
  KEY `FK_UsersHaveAddresses_Addresses_idx` (`AddressID`),
  KEY `FK_UsersHaveAddresses_Users_idx` (`UserID`),
  CONSTRAINT `FK_UsersHaveAddresses_Addresses` FOREIGN KEY (`AddressID`) REFERENCES `Addresses` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UsersHaveAddresses_Users` FOREIGN KEY (`UserID`) REFERENCES `Users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsersHaveAddresses`
--

LOCK TABLES `UsersHaveAddresses` WRITE;
/*!40000 ALTER TABLE `UsersHaveAddresses` DISABLE KEYS */;
INSERT INTO `UsersHaveAddresses` VALUES (3,3,'2018-01-06 13:34:30','2018-01-06 13:34:32');
/*!40000 ALTER TABLE `UsersHaveAddresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsersHaveOrders`
--

DROP TABLE IF EXISTS `UsersHaveOrders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersHaveOrders` (
  `UserID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserID`,`OrderID`),
  KEY `FK_UsersHaveOrders_Orders_idx` (`OrderID`),
  KEY `FK_UsersHaveOrders_Users_idx` (`UserID`),
  CONSTRAINT `FK_UsersHaveOrders_Orders` FOREIGN KEY (`OrderID`) REFERENCES `Orders` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UsersHaveOrders_Users` FOREIGN KEY (`UserID`) REFERENCES `Users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsersHaveOrders`
--

LOCK TABLES `UsersHaveOrders` WRITE;
/*!40000 ALTER TABLE `UsersHaveOrders` DISABLE KEYS */;
/*!40000 ALTER TABLE `UsersHaveOrders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsersHavePhoneNumbers`
--

DROP TABLE IF EXISTS `UsersHavePhoneNumbers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersHavePhoneNumbers` (
  `UserID` int(11) NOT NULL,
  `PhoneNumberID` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserID`,`PhoneNumberID`),
  KEY `FK_UsersHavePhoneNumbers_PhoneNumbers_idx` (`PhoneNumberID`),
  KEY `FK_UsersHavePhoneNumbers_Users_idx` (`UserID`),
  CONSTRAINT `FK_UsersHavePhoneNumbers_PhoneNumbers` FOREIGN KEY (`PhoneNumberID`) REFERENCES `PhoneNumbers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UsersHavePhoneNumbers_Users` FOREIGN KEY (`UserID`) REFERENCES `Users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsersHavePhoneNumbers`
--

LOCK TABLES `UsersHavePhoneNumbers` WRITE;
/*!40000 ALTER TABLE `UsersHavePhoneNumbers` DISABLE KEYS */;
/*!40000 ALTER TABLE `UsersHavePhoneNumbers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsersHaveProducts`
--

DROP TABLE IF EXISTS `UsersHaveProducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersHaveProducts` (
  `UsersID` int(11) NOT NULL,
  `ProductsID` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UsersID`,`ProductsID`),
  KEY `FK_UsersHaveProducts_Products_idx` (`ProductsID`),
  KEY `FK_UsersHaveProducts_Users_idx` (`UsersID`),
  CONSTRAINT `FK_UsersHaveProducts_Products` FOREIGN KEY (`ProductsID`) REFERENCES `Products` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UsersHaveProducts_Users` FOREIGN KEY (`UsersID`) REFERENCES `Users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsersHaveProducts`
--

LOCK TABLES `UsersHaveProducts` WRITE;
/*!40000 ALTER TABLE `UsersHaveProducts` DISABLE KEYS */;
/*!40000 ALTER TABLE `UsersHaveProducts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsersHaveProperties`
--

DROP TABLE IF EXISTS `UsersHaveProperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersHaveProperties` (
  `UserID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `CreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserID`,`PropertyID`),
  KEY `FK_UsersHaveProperties_Properties_idx` (`PropertyID`),
  KEY `FK_UsersHaveProperties_Users_idx` (`UserID`),
  CONSTRAINT `FK_UsersHaveProperties_Properties` FOREIGN KEY (`PropertyID`) REFERENCES `UserProperties` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UsersHaveProperties_Users` FOREIGN KEY (`UserID`) REFERENCES `Users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsersHaveProperties`
--

LOCK TABLES `UsersHaveProperties` WRITE;
/*!40000 ALTER TABLE `UsersHaveProperties` DISABLE KEYS */;
/*!40000 ALTER TABLE `UsersHaveProperties` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-06 15:36:13
