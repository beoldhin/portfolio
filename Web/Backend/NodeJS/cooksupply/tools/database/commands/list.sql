SYSTEM echo "Showing databases";
SYSTEM echo "----------------------------------";
SHOW DATABASES;
SYSTEM echo "";

SYSTEM echo "Showing tables";
SYSTEM echo "----------------------------------";
SHOW TABLES;
SYSTEM echo "";

SYSTEM echo "Showing table Addresses";
SYSTEM echo "----------------------------------";
SELECT * FROM Addresses;
SYSTEM echo "";

SYSTEM echo "Showing table Cities";
SYSTEM echo "----------------------------------";
SELECT * FROM Cities;
SYSTEM echo "";

SYSTEM echo "Showing table Countries";
SYSTEM echo "----------------------------------";
SELECT * FROM Countries;
SYSTEM echo "";

SYSTEM echo "Showing table CountriesHaveCities";
SYSTEM echo "-----------------------------------";
SELECT * FROM CountriesHaveCities;
SYSTEM echo "";

SYSTEM echo "Showing table Ingredients";
SYSTEM echo "-----------------------------------";
SELECT * FROM Ingredients;
SYSTEM echo "";

SYSTEM echo "Showing table Orders";
SYSTEM echo "-----------------------------------";
SELECT * FROM Orders;
SYSTEM echo "";

SYSTEM echo "Showing table OrdersHaveProducts";
SYSTEM echo "-----------------------------------";
SELECT * FROM OrdersHaveProducts;
SYSTEM echo "";

SYSTEM echo "Showing table PhoneNumbers";
SYSTEM echo "-----------------------------------";
SELECT * FROM PhoneNumbers;
SYSTEM echo "";

SYSTEM echo "Showing table Products";
SYSTEM echo "-----------------------------------";
SELECT * FROM Products;
SYSTEM echo "";

SYSTEM echo "Showing table ProductsHaveIngredients";
SYSTEM echo "-------------------------------------";
SELECT * FROM ProductsHaveIngredients;

SYSTEM echo "";
SYSTEM echo "Showing table UserProperties";
SYSTEM echo "-----------------------------------";
SELECT * FROM UserProperties;
SYSTEM echo "";

SYSTEM echo "Showing table Users";
SYSTEM echo "-----------------------------------";
SELECT * FROM Users;
SYSTEM echo "";

SYSTEM echo "Showing table UsersHaveAddresses";
SYSTEM echo "-----------------------------------";
SELECT * FROM UsersHaveAddresses;
SYSTEM echo "";

SYSTEM echo "Showing table UsersHaveOrders";
SYSTEM echo "-----------------------------------";
SELECT * FROM UsersHaveOrders;
SYSTEM echo "";

SYSTEM echo "Showing table UsersHavePhoneNumbers";
SYSTEM echo "-----------------------------------";
SELECT * FROM UsersHavePhoneNumbers;
SYSTEM echo "";

SYSTEM echo "Showing table UsersHaveProperties";
SYSTEM echo "-----------------------------------";
SELECT * FROM UsersHavePhoneNumbers;
SYSTEM echo "";
