const express = require('express');
const app = express();

const configLogger = require('./config/logger')(app, 'development');
const configBase = require('./config/base')(app);
// const configSecurity = require('./config/security')(app);
const logger = require('winston');

app.listen(app.get('port'), function() {
    logger.info('Express web server is listening on port ' + app.get('port'));
});

