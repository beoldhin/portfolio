const logger = require('winston');
const mongoose = require('mongoose');

module.exports = function(app, database) {
  logger.info('Initializing Mongo DB...');
  mongoose.Promise = global.Promise;
  mongoose.connect(database);
}

