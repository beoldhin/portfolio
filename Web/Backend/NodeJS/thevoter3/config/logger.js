const winston = require('winston');
const expressWinston = require('express-winston-2');

const developmentLogging = {
  'enableConsole': true,
  'enableFile': false,
  'logLevelConsole': 'debug',
  'logLevelFile': 'warn',
  'logExceptions': false
};

const productLogging = {
  'enableConsole': false,
  'enableFile': true,
  'logLevelConsole': 'warn',
  'logLevelFile': 'warn',
  'logExceptions': true
};

module.exports = function(app, logMode) {
  if (logMode == 'development') {
    setWinston(developmentLogging);
  } else {
    setWinston(productLogging);
  }
  setExpressWinston(app);
}

function setWinston(logMode) {
  // https://github.com/winstonjs/winston/blob/master/docs/transports.md
  winston.remove(winston.transports.Console);
  winston.add(winston.transports.Console, {
    silent: !logMode.enableConsole,
    level: logMode.logLevelConsole,
    colorize: true,
    humanReadableUnhandledException: true
  });
  winston.add(winston.transports.File, {
    silent: !logMode.enableFile,
    level: logMode.logLevelFile,
    filename: 'logs/logs-all.log',
    maxsize: 5242880,
    maxFiles: 10
  });
  if (logMode.logExceptions) {
    winston.handleExceptions(new winston.transports.File({
      filename: 'logs/exceptions.log'
    }));
  }
}

function setExpressWinston(app) {
  const winstonConfig = expressWinston.logger({
    transports: [
      new winston.transports.Console({
        json: false,
        colorize: true
      })
    ],
    getLogLevel: function(statusCode) {
      if (statusCode >= 400) {
        return "error"
      }
      return "info";
    },
    meta: false,
    expressFormat: true,
    colorStatus: true,
  });
  app.use(winstonConfig);
}

