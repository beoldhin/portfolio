// Passport initialization modified from:
// https://github.com/passport/express-4.x-local-example/blob/master/server.js

const logger = require('winston');
const jwt = require('express-jwt');

const mongoose = require('mongoose');
const ExpressBrute = require('express-brute');
const MongooseStore = require('express-brute-mongoose');
const BruteForceSchema = require('express-brute-mongoose/dist/schema');
const MongooseClient = require('mongoose');

const model = mongoose.model('bruteforce', BruteForceSchema);
const store = new MongooseStore(model);

// No more than 1000 login attempts per day per IP
const bruteForce = new ExpressBrute(store, {
    freeRetries: 1000,
    attachResetToRequest: false,
    refreshTimeoutOnRequest: false,
    minWait: 25 * 60 * 60 * 1000, // 1 day 1 hour (should never reach this wait time)
    maxWait: 25 * 60 * 60 * 1000, // 1 day 1 hour (should never reach this wait time)
    lifetime: 24 * 60 * 60, // 1 day (seconds not milliseconds)
});

const admins = ["jamppa@yahoo.com"];

const authCheck = jwt({
  secret: new Buffer('SomeSecret', 'base64'),
  audience: 'SomeAudience'
});

module.exports = function(app) {
  logger.info('Initializing Security...');
  setMainRouteSecurity(app);
  setAdminRouteSecurity(app);
}

function setMainRouteSecurity(app) {
  // app.all('/secured*', bruteForce.prevent, authCheck, function(req, res, next) {
  app.all('/secured*', authCheck, function(req, res, next) {
    if (req.user.email_verified !== true) {
      res.statusMessage = "Provided email address is unverified.";
      res.status(401).end();
    } else {
      res.locals.personid = req.user.email;  // This is of course not precisely correct
      next();
    }
  });
}

function setAdminRouteSecurity(app) {
  app.all('/secured/admin*', function(req, res, next) {
    for (admin in admins) {
      if (admin == res.locals.personid) {
        next();
      }
    }
    res.statusMessage = "User is not admin.";
    res.status(401).end();
  });
}

