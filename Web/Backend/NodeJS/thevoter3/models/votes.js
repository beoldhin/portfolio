const mongoose = require('mongoose');

// Vote Schema
const voteSchema = mongoose.Schema({
    personid: {
        type: String,
        required: true,
        unique: true
    },
    votedid: {
        type: Number,
        required: true
    }
},{ _id : false });

const Votes = mongoose.model('Votes', voteSchema);

module.exports = Votes;

