const mongoose = require('mongoose');
const moment = require('moment');

// Candidate Schema
const candidateSchema = mongoose.Schema({
    identifier: {
        type: Number,
        required: true,
        unique: true
    },
    party: {
        type: String,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    dateofbirth: {
        type: String,
        required: true
    },
    occupation: {
        type: String,
        required: false
    },
    hometown: {
        type: String,
        required: false
    },
    motto: {
        type: String,
        require: false
    },
    image_url: {
        type: String,
        required: false
    }
},{  // Schema options (skip '_id' and include virtuals to results)
    toObject: {
        virtuals: true
    },
    toJSON: {
        virtuals: true
    }
});

candidateSchema.virtual('age').get(function () {
    var thisDate = moment(new Date());
    var thatDate = moment(this.dateofbirth, "YYYY-MM-DD");
    var duration = moment.duration(thisDate.diff(thatDate));
    return Math.floor(duration.asYears());
});

const Candidates = mongoose.model('Candidates', candidateSchema);

module.exports = Candidates;

