const mongoose = require('mongoose');

// User Schema

const userSchema = mongoose.Schema({
    displayName: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
},{ _id: false });

const Users = mongoose.model('Users', userSchema);

module.exports = Users;

