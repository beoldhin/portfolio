var http = require('http');
var fs = require('fs');

var options_login = [
{
  port: 3000,
  hostname: 'localhost',
  method: 'POST',
  path: '/management/login',
  headers: {
    'Content-Type': 'application/json'
  }
}];

var options_send = [
{
  port: 3000,
  hostname: 'localhost',
  method: 'PUT',
  path: '/voting/voters',
  headers: {
    'Content-Type': 'application/json'
  }
}];

var options_receive = [
{
  port: 3000,
  hostname: 'localhost',
  method: 'GET',
  path: '/',
},
{
  port: 3000,
  hostname: 'localhost',
  method: 'GET',
  path: '/voting/candidates?lastname=Salo'
},
{
  port: 3000,
  hostname: 'localhost',
  method: 'GET',
  path: '/voting/voters'
},
{
  port: 3000,
  hostname: 'localhost',
  method: 'GET',
  path: '/voting/voters/4392432-43294532'
}];

function do_send(options, data) {
  options.headers['Content-Length'] = Buffer.byteLength(data);
  var req = http.request(options);
  req.on('response', function(res) {
    var body = '';
    res.on('data', function(chunk) {
      body += chunk;
    });
    res.on('end', function() {
      console.log('Tested run: ' + options.method + ' for ' + options.path);
      if (res.statusCode>=200 && res.statusCode<300) {
        console.log('Received response: ' + body);
      } else {
        console.log('Received ERROR: ' + res.statusCode);
      }
      console.log('');
    });
  });
  req.write(data);
  req.end();
}

function do_receive(options) {
  var req = http.request(options);
  req.on('response', function(res) {
    var body = '';
    res.on('data', function(chunk) {
      body += chunk;
    });
    res.on('end', function() {
      console.log('Tested run: ' + options.method + ' for ' + options.path);
      if (res.statusCode>=200 && res.statusCode<=300) {
        console.log('Received response: ' + body);
      } else {
        console.log('Received ERROR: ' + res.statusCode);
      }
      console.log('');
    });
  });
  req.on('error', function(error) {
    console.log(`Request error: ${error.message}`);
  });
  req.end();
}

// options_receive.forEach(function(entry) {
//   do_receive(entry);
// });
//
// options_send.forEach(function(entry) {
//   wordStart = entry.path.lastIndexOf('/') + 1;
//   beforeEnd = entry.path.length;
//   identifier = entry.path.slice(wordStart, beforeEnd);
//   method = entry.method.toLowerCase();
//   filename = 'voting_' + identifier + '_test_' + method + '.json';
//   fs.readFile(filename, 'utf-8', function(err, data) {
//     console.log(filename);
//     do_send(entry, data);
//   });
// });

filename = 'voting_vote_test_login.json'
fs.readFile(filename, 'utf-8', function(err, data) {
  do_send(options_login[0], data);
});

