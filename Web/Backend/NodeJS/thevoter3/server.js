const express = require('express');
const app = express();

const configLogger = require('./config/logger')(app, 'development');
const configBase = require('./config/base')(app);
const configMongo = require('./config/mongo')(app,'mongodb://localhost/voting');
const configSecurity = require('./config/security')(app);
const logger = require('winston');

const voting = require('./controllers/voting');
const admin = require('./controllers/admin');

app.get('/secured/candidates',               voting.getCandidates);
app.put('/secured/makevote',                 voting.setVote);
app.get('/secured/getvoted',                 voting.getVoted);

app.get('/secured/admin/getvoted',           admin.getVoted);
app.post('/secured/admin/candidates/add',    admin.addCandidate);
app.put('/secured/admin/candidates/update',  admin.updateCandidate);
app.post('/secured/admin/candidates/remove', admin.removeCandidate);

app.listen(app.get('port'), function() {
  logger.info('Express web server is listening on port ' + app.get('port'));
});

