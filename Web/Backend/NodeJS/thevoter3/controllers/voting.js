'use strict';

const logger = require('winston');

var Candidates = require('../models/candidates');
var Votes = require('../models/votes');

function candidateReplacer(key, value) {
  if (key == "id") {
    return undefined;
  }
  if (key == "_id") {
    return undefined;
  }
  if (key == "dateofbirth") {
    return undefined;
  }
  return value;
}

function voteReplacer(key, value)
{
  if (key == "id") {
    return undefined;
  }
  if (key == "_id") {
    return undefined;
  }
  return value;
}

exports.getCandidates = function(req, res) {
  var query = req.query;
  Candidates.find(query)
            .then(function(resultArray) {
              var jsonString = JSON.stringify(resultArray, candidateReplacer);
              res.send(jsonString);
            });
};

exports.setVote = function(req, res) {
  var vote = req.body;
  var query = {personid: res.locals.personid};
  var options = {'upsert': true, new: true};
  Votes.findOneAndUpdate(query, vote, options)
       .then(function(resultArray) {
         var jsonString = JSON.stringify(resultArray, voteReplacer);
         res.send(jsonString);
       });
};

exports.getVoted = function(req, res) {
  var query = {personid: res.locals.personid};
  Votes.findOne(query)
       .then(function(resultArray) {
         var jsonString = JSON.stringify(resultArray, voteReplacer);
         res.send(jsonString);
       });
};

