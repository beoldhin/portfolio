'use strict';

const logger = require('winston');

var Candidates = require('../models/candidates');
var Votes = require('../models/votes');

function replacer(key, value)
{
  if (key == "_id") {
    return undefined;
  }
  return value;
}

exports.getVoted = function(req, res) {
};

exports.addCandidate = function(req, res) {
};

exports.updateCandidate = function(req, res) {
};

exports.removeCandidate = function(req, res) {
};

