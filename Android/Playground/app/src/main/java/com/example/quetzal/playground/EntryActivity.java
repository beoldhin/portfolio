package com.example.quetzal.playground;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class EntryActivity extends AppCompatActivity {

    private String event;
    private EditText editTextTitle;
    private EditText editTextDescription;

    private void initializeContents() {
        editTextTitle = (EditText) findViewById(R.id.editTextEntryTitle);
        editTextDescription = (EditText) findViewById(R.id.editTextDescription);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            event = extras.getString("event_key");
            String title = extras.getString("title_key");
            String description = extras.getString("description_key");
            editTextTitle.setText(title);
            editTextDescription.setText(description);
        }
    }

    public void onClickDone(View view) {
        String title = editTextTitle.getText().toString();
        String description = editTextDescription.getText().toString();
        if (!title.isEmpty() && !description.isEmpty()) {
            Intent intent = new Intent();
            intent.putExtra("event_key", event);
            intent.putExtra("title_key", title);
            intent.putExtra("description_key", description);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            Snackbar snackbar = Snackbar.make(view, "Empty data!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry);
        initializeContents();
    }

}
