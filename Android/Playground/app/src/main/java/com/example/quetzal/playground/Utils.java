package com.example.quetzal.playground;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Utils {

    public static String getCurrentDateTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return mdFormat.format(calendar.getTime());
    }

}
