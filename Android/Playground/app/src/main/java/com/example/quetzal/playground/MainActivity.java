package com.example.quetzal.playground;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class MainActivity extends AppCompatActivity {


    Preferences preferences;
    DBAdapter dbAdapter;

    private static final int REQUEST_CODE_CONTEXT = 701;

    private void startEntryActivity(String event, String title, String description) {
        Intent intent = new Intent(this, EntryActivity.class);
        intent.putExtra("event_key", event);
        intent.putExtra("title_key", title);
        intent.putExtra("description_key", description);
        startActivityForResult(intent, REQUEST_CODE_CONTEXT);
    }

    private void startDetailsActivity(String description) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("description_key", description);
        startActivity(intent);
    }

    private void openDatabase() {
        dbAdapter = new DBAdapter(this);
        dbAdapter.open();
    }

    private void populateListView(boolean refreshRequired) {
        Cursor cursor;
        String listSortType = preferences.getListSortType();
        if (listSortType.equals("date")) {
            cursor = dbAdapter.getAllRows("date");
        } else {
            cursor = dbAdapter.getAllRows("title");
        }
        String[] fromFieldNames = new String[] {DBAdapter.KEY_DATE, DBAdapter.KEY_TITLE};
        int[] toViewIDs = new int[] {R.id.textViewListDate, R.id.textViewListTitle};
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(getBaseContext(), R.layout.layout_entries, cursor, fromFieldNames, toViewIDs, 0);
        if (refreshRequired) {
            cursorAdapter.notifyDataSetChanged();
        }
        ListView list = (ListView) findViewById(R.id.listViewEntries);
        list.setAdapter(cursorAdapter);
        registerForContextMenu(list);
    }

    private void addToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void addOnLickListenerForActionButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startEntryActivity("insert","", "");
            }
        });
    }

    private void addOnClickListenerForEntryList() {
        final ListView list = (ListView) findViewById(R.id.listViewEntries);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = dbAdapter.getRow(id);
                String description = cursor.getString(cursor.getColumnIndex("description"));
                startDetailsActivity(description);
            }
        });
    }

    private void initialize() {
        this.preferences = new Preferences(this);
        addToolbar();
        openDatabase();
        populateListView(false);
        addOnClickListenerForEntryList();
        addOnLickListenerForActionButton();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_sortbydate:
                preferences.setListSortTypeDate();
                preferences.write();
                populateListView(true);
                return true;
            case R.id.action_sortbytitle:
                preferences.setListSortTypeTitle();
                preferences.write();
                populateListView(true);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode!=REQUEST_CODE_CONTEXT || resultCode!=Activity.RESULT_OK) {
            return;
        }
        Bundle extras = data.getExtras();
        String origEvent = extras.getString("event_key");
        String[] event = origEvent.split(":");
        if (!event[0].equals("insert") && !event[0].equals("update")) {
            return;
        }
        String title = extras.getString("title_key");
        String description = extras.getString("description_key");
        String date = Utils.getCurrentDateTime();
        if (event[0].equals("insert")) {
            dbAdapter.insertRow(title, description, date);
            populateListView(false);
        } else if (event[0].equals("update")) {
            dbAdapter.updateRow(Integer.parseInt(event[1]), title, description, date);
            populateListView(false);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.contextmenu_main, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.action_edit:
                Cursor cursor = dbAdapter.getRow(info.id);
                String event = "update:" + info.id;
                String title = cursor.getString(cursor.getColumnIndex("title"));
                String description = cursor.getString(cursor.getColumnIndex("description"));
                startEntryActivity(event, title, description);
                return true;
            case R.id.action_remove:
                dbAdapter.deleteRow(info.id);
                populateListView(false);
                return true;
        }
        return super.onContextItemSelected(item);
    }

}
