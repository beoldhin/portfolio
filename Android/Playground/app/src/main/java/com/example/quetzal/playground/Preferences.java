package com.example.quetzal.playground;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {

    private static final String PREFERENCE_SORTTYPE = "sorttype";
    private static final String PREFERENCE_SORTTYPE_DEFAULT = "date";

    private final Activity activity;
    private String listSortType;  // "date" or "title"

    public Preferences(Activity activity) {
        this.activity = activity;
        read();
    }

    public void setListSortTypeDate() {
        this.listSortType = "date";
    }

    public void setListSortTypeTitle() {
        this.listSortType = "title";
    }

    public String getListSortType() {
        return this.listSortType;
    }

    public void write() {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(PREFERENCE_SORTTYPE, listSortType);
        editor.commit();
    }

    public void read() {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        this.listSortType = sharedPref.getString(PREFERENCE_SORTTYPE, PREFERENCE_SORTTYPE_DEFAULT);
    }

}
