package com.example.quetzal.playground;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    private TextView editTextDescription;

    private void initializeContents() {
        editTextDescription = (TextView) findViewById(R.id.textViewDetailsDescription);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String description = extras.getString("description_key");
            editTextDescription.setText(description);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        initializeContents();
    }

}
