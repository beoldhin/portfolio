package com.example.quetzal.game;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameLogic {

    // Implementing "stupid" mover instead of:
    // https://www.quora.com/Is-there-a-way-to-never-lose-at-Tic-Tac-Toe

    private static final int boardSize = 3;

    private static final int PIECE_HUMAN = 0;
    private static final int PIECE_COMPUTER = 1;
    private static final int PIECE_UNUSED = Integer.MAX_VALUE;

    public static final int RESULT_TAKEN = 100;
    public static final int RESULT_HUMAN_WINS = 101;
    public static final int RESULT_COMPUTER_WINS = 102;
    public static final int RESULT_ONGOING = 103;
    public static final int RESULT_TIE = 104;

    private static final String TAG = "MyActivity";

    private int[][] board;
    private Random random;
    private int computerMove;

    public GameLogic() {
        this.board = new int[boardSize][boardSize];
        this.random = new Random();
        resetBoard();
    }

    public void resetBoard() {
        for (int j=0; j<boardSize; j++) {
            for (int i=0; i<boardSize; i++) {
                board[j][i] = Integer.MAX_VALUE;
            }
        }
    }

    public int playHuman(int selection) {
        if (selection<0 || selection>=boardSize*boardSize) {
            return -1;
        }
        int row = selection / boardSize;
        int column = selection % boardSize;
        Log.i(TAG, "Human Move is " + selection);
        Log.i(TAG, "Human Move C is " + column);
        Log.i(TAG, "Human Move R is " + row);
        if (board[row][column] != PIECE_UNUSED) {
            return RESULT_TAKEN;
        }
        if (getFreeSquares() == 0) {
            return RESULT_TIE;
        }
        board[row][column] = PIECE_HUMAN;
        if (humanWins()) {
            return RESULT_HUMAN_WINS;
        }
        return RESULT_ONGOING;
    }

    public int playComputer() {
        if (getFreeSquares() == 0) {
            return RESULT_TIE;
        }
        computerMove = generateComputerMove();
        int row = computerMove / boardSize;
        int column = computerMove % boardSize;
        Log.i(TAG, "Computer Move is " + computerMove);
        Log.i(TAG, "Computer Move C is " + column);
        Log.i(TAG, "Computer Move R is " + row);
        board[row][column] = PIECE_COMPUTER;
        if (computerWins()) {
            return RESULT_COMPUTER_WINS;
        }
        return RESULT_ONGOING;
    }

    public int getComputerMove() {
        return this.computerMove;
    }

    private boolean humanWins() {
        return evaluateBoard(PIECE_HUMAN);
    }

    private boolean computerWins() {
        return evaluateBoard(PIECE_COMPUTER);
    }

    private boolean evaluateBoard(int pieceToCheck) {
        for (int j=0; j<boardSize; j++) {
            if (horizontalMatches(j, pieceToCheck)) {
                return true;
            }
        }
        for (int i=0; i<boardSize; i++) {
            if (verticalMatches(i, pieceToCheck)) {
                return true;
            }
        }
        if (evaluateDiagonal1(pieceToCheck)) {
            return true;
        }
        if (evaluateDiagonal2(pieceToCheck)) {
            return true;
        }
        return false;
    }

    private boolean horizontalMatches(int startRow, int pieceToCheck) {
        for (int i=0; i<boardSize; i++) {
            if (board[startRow][i] != pieceToCheck) {
                return false;
            }
        }
        return true;
    }

    private boolean verticalMatches(int startColumn, int pieceToCheck) {
        for (int j=0; j<boardSize; j++) {
            if (board[j][startColumn] != pieceToCheck) {
                return false;
            }
        }
        return true;
    }

    private boolean evaluateDiagonal1(int pieceToCheck) {
        // Upper left to lower right
        for (int i=0,j=0; i<boardSize; i++,j++) {
            if (board[j][i] != pieceToCheck) {
                return false;
            }
        }
        return true;
    }

    private boolean evaluateDiagonal2(int pieceToCheck) {
        // Lower left to upper right
        for (int i=0,j=boardSize-1; i<boardSize; i++,j--) {
            if (board[j][i] != pieceToCheck) {
                return false;
            }
        }
        return true;
    }

    private int getFreeSquares() {
        int freeSquares = 0;
        for (int j=0; j<boardSize; j++) {
            for (int i=0; i<boardSize; i++) {
                if (board[j][i] == PIECE_UNUSED) {
                    freeSquares++;
                }
            }
        }
        return freeSquares;
    }

    private int generateComputerMove() throws IllegalStateException {
        int counter = 0;
        List<Integer> empties = new ArrayList<>();
        for (int j=0; j<boardSize; j++) {
            for (int i=0; i<boardSize; i++) {
                if (board[j][i] == PIECE_UNUSED) {
                    empties.add(counter);
                }
                counter++;
            }
        }
        int mysize = empties.size();
        if (empties.size() == 0) {
            throw new IllegalStateException("Illegal state");
        }
        int randNum = this.random.nextInt(empties.size());
        return empties.get(randNum);
    }


}
