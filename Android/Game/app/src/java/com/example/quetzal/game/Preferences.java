package com.example.quetzal.game;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {

    private static final String PREFERENCE_BUTTONNUMBERS = "switch_preference_button_numbers";
    private static final String PREFERENCE_PLAYERCOLORS = "switch_preference_show_player_colors";
    private static final String PREFERENCE_PLAYMUSIC = "switch_preference_play_music";
    private static final String PREFERENCE_TEXTHUMAN = "edit_text_preference_text_for_human";
    private static final String PREFERENCE_TEXTCOMPUTER = "edit_text_preference_text_for_computer";

    private final Activity activity;
    private boolean buttonNumbers;
    private boolean playercolors;
    private boolean playmusic;
    private String textHuman;
    private String textComputer;

    public Preferences(Activity activity) {
        this.activity = activity;
        read();
    }

    public boolean getButtonNumbers() {
        return this.buttonNumbers;
    }

    public boolean getPlayerColors() {
        return this.playercolors;
    }

    public boolean getPlayMusic() {
        return this.playmusic;
    }

    public String getTextHuman() {
        return this.textHuman;
    }

    public String getTextComputer() {
        return this.textComputer;
    }

    public void read() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(activity);
        this.buttonNumbers = sharedPref.getBoolean(PREFERENCE_BUTTONNUMBERS, false);
        this.playercolors = sharedPref.getBoolean(PREFERENCE_PLAYERCOLORS, false);
        this.playmusic = sharedPref.getBoolean(PREFERENCE_PLAYMUSIC, false);
        this.textHuman = sharedPref.getString(PREFERENCE_TEXTHUMAN, "X");
        this.textComputer = sharedPref.getString(PREFERENCE_TEXTCOMPUTER, "O");
    }

}
