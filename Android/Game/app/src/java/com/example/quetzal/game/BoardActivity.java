package com.example.quetzal.game;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class BoardActivity extends AppCompatActivity {

    private static final int boardSize = 3;
    private GameLogic gameLogic;
    private List<Button> buttons;
    private List<Drawable> buttonBackgrounds;
    private Preferences preferences;

    private void showNewGameMessage(int title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(R.string.newgame);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                gameLogic.resetBoard();
                for (int i=0; i<buttons.size(); i++) {
                    Button button = buttons.get(i);
                    if (preferences.getButtonNumbers()) {
                        button.setText(Integer.toString(i+1));
                    } else {
                        button.setText("");
                    }
                    button.setBackground(buttonBackgrounds.get(i));
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent();
                setResult(Activity.RESULT_OK);
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private boolean manageHumanMove(View view) {
        int selection = view.getId();
        Button button = (android.widget.Button) view;
        int result = gameLogic.playHuman(selection);
        if (result == GameLogic.RESULT_TAKEN) {
            return false;
        }
        button.setText(preferences.getTextHuman());
        if (preferences.getPlayerColors()) {
            button.setBackgroundColor(Color.GREEN);
        }
        if (result == GameLogic.RESULT_TIE) {
            showNewGameMessage(R.string.tiegame);
            return true;
        }
        if (result == GameLogic.RESULT_HUMAN_WINS) {
            showNewGameMessage(R.string.humanwins);
            return true;
        }
        // Now for computer move
        result = gameLogic.playComputer();
        selection = gameLogic.getComputerMove();
        button = buttons.get(selection);
        button.setText(preferences.getTextComputer());
        if (preferences.getPlayerColors()) {
            button.setBackgroundColor(Color.RED);
        }
        if (result == GameLogic.RESULT_TIE) {
            // Never run with odd number of moves
            showNewGameMessage(R.string.tiegame);
            return true;
        }
        if (result == GameLogic.RESULT_COMPUTER_WINS) {
            showNewGameMessage(R.string.computerwins);
            return true;
        }
        return false;
    }

    private void createBoard() {
        int counter = 0;
        LinearLayout buttonsView = new LinearLayout(this);
        buttonsView.setOrientation(LinearLayout.VERTICAL);
        for (int r=0; r<boardSize; r++)
        {
            LinearLayout row = new LinearLayout(this);
            row.setOrientation(LinearLayout.HORIZONTAL);
            for (int c=0; c<boardSize; c++)
            {
                Button button = new Button(this);
                button.setId(counter);
                if (preferences.getButtonNumbers()) {
                    button.setText(Integer.toString(counter+1));
                }
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                lp.weight = 1.0f;
                row.addView(button, lp);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        manageHumanMove(view);
                    }
                });
                buttons.add(button);
                buttonBackgrounds.add(button.getBackground());
                counter++;
            }
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            lp.weight = 1.0f;
            buttonsView.addView(row, lp);
        }
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        setContentView(buttonsView, lp);
    }

    private boolean initialize() {
        this.buttons = new ArrayList<>();
        this.buttonBackgrounds = new ArrayList<>();
        this.gameLogic = new GameLogic();
        this.preferences = new Preferences(this);
        createBoard();
        return true;
   }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

}
