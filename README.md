Most of the work here is my own. Some documentation (university courses etc.) have been saved here for reference. I also maintain a Github Gist [Arch Linux guide](https://gist.github.com/beoldhin/efd56f067a4f319053c3ca8bc3728fc6) and some other work as [Github](https://github.com/beoldhin?tab=repositories) repositories.

