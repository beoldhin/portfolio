#include "4DSBs.h"

#define BLOCKSIZE 208

void GetBuffer(const char*, int*, int);
void UpdateBuffers(void);

struct BLOCK {
    int height;
    int weight;
    int head;
    int clothes;
    int ranking;
    int scrpower;
    int scrstamina;
    int scrspeed;
    int scroverall;
    int slowness;
    int power;
    int stamina;
    int hand;
    int fights;
    int wins;
    int losses;
    int draws;
    int money;
    int kos;
    int tkos;
    int decisions;
    int unanimous;
    int disquals;
};

char* filename;
size_t filesize;
char buffer[10+1];
struct BLOCK* boxers;
unsigned char* filedata;
int boxersn;


BOOL Initialize(char* fname) {
    FILE* fp;
    filename = fname;
    fp = fopen(filename, "r");
    if (fp == NULL) {
        printf("Boxer file not found.\n");
        return FALSE;
    }
    fseek(fp, 0, SEEK_END);
    filesize = ftell(fp);
    fclose(fp);
    return TRUE;
}

BOOL CreateInternals(void) {
    int i;
    FILE* fp;
    int reloc;
    int truesize;
    truesize = filesize - 2;
    boxersn = truesize / BLOCKSIZE;
    if ((truesize<0) || ((truesize%BLOCKSIZE)!=0) || (boxersn==0)) {
        printf("Cannot read boxers.\n");
        return FALSE;
    }
    filedata = (unsigned char*)malloc((unsigned)filesize);
    boxers = (struct BLOCK*)calloc((unsigned)boxersn, sizeof(struct BLOCK));
    fp = fopen(filename, "r");
    fread(filedata, sizeof(unsigned char), (unsigned)filesize, fp);
    i = 0;
    reloc = 0;
    while (i < boxersn) {
        /* Data file is always little-endian for DOS game */
        boxers[i].height = filedata[reloc+0x1B];
        boxers[i].weight = filedata[reloc+0x1C];
        boxers[i].head = filedata[reloc+0x1D];
        boxers[i].clothes = filedata[reloc+0x1E];
        boxers[i].ranking = filedata[reloc+0x1F];
        boxers[i].scrpower = filedata[reloc+0x20];
        boxers[i].scrstamina = filedata[reloc+0x21];
        boxers[i].scrspeed = filedata[reloc+0x22];
        boxers[i].scroverall = filedata[reloc+0x23];
        boxers[i].slowness = filedata[reloc+0x33];
        boxers[i].power = filedata[reloc+0x35];
        boxers[i].stamina = filedata[reloc+0x39];
        boxers[i].hand = filedata[reloc+0x4E];
        boxers[i].fights = filedata[reloc+0x8A] |
                          (filedata[reloc+0x8B]<<8);
        boxers[i].wins = filedata[reloc+0x8C] |
                        (filedata[reloc+0x8D]<<8);
        boxers[i].losses = filedata[reloc+0x8E] |
                          (filedata[reloc+0x8F]<<8);
        boxers[i].draws = filedata[reloc+0x90] |
                         (filedata[reloc+0x91]<<8);
        boxers[i].money = filedata[reloc+0x92] |
                         (filedata[reloc+0x93]<<8) |
                         (filedata[reloc+0x94]<<16) |
                         (filedata[reloc+0x95]<<24);
        boxers[i].kos = filedata[reloc+0x96] |
                       (filedata[reloc+0x97]<<8);
        boxers[i].tkos = filedata[reloc+0x98] |
                        (filedata[reloc+0x99]<<8);
        boxers[i].decisions = filedata[reloc+0x9A] |
                             (filedata[reloc+0x9B]<<8);
        boxers[i].unanimous = filedata[reloc+0x9C] |
                             (filedata[reloc+0x9D]<<8);
        boxers[i].disquals = filedata[reloc+0x9E] |
                            (filedata[reloc+0x9F]<<8);
        reloc += BLOCKSIZE;
        i++;
    }
    fclose(fp);
    return TRUE;
}

void MainInterface(void) {
    int p;
    int num;
    char ch;
    char edittext[] = "Enter '1'..'23'|'P'|'N'|'L'|'Q' [%d/%d]: ";
    p = 0;
    while (1) {
        printf(edittext, p+1, boxersn);
        fgets(buffer, 10+1, stdin);
        ch = toupper(buffer[0]);
        if (ch == 'P') {
            if (p > 0)
                p--;
        } else if (ch == 'N') {
            if (p < boxersn-1)
                p++;
        } else if (ch == 'L') {
            printf("Data for %s:\n", filedata+(p*BLOCKSIZE)+0x02);
            printf(" 1) Height:    %9d / %9d\n", boxers[p].height, 0x8F);
            printf(" 2) Weight:    %9d / %9d\n", boxers[p].weight, 0xE5);
            printf(" 3) Head:      %9d / %9d\n", boxers[p].head, 0x0D);
            printf(" 4) Clothes:   %9d / %9d\n", boxers[p].clothes, 0xFF);
            printf(" 5) Ranking:   %9d / %9d\n", boxers[p].ranking, 0xFF);
            printf(" 6) Power*:    %9d / %9d\n", boxers[p].scrpower, 0xFF);
            printf(" 7) Stamina*:  %9d / %9d\n", boxers[p].scrstamina, 0xFF);
            printf(" 8) Speed*:    %9d / %9d\n", boxers[p].scrspeed, 0xFF);
            printf(" 9) Overall*:  %9d / %9d\n", boxers[p].scroverall, 0xFF);
            printf("10) Slowness:  %9d / %9d\n", boxers[p].slowness, 0x7F);
            printf("11) Power:     %9d / %9d\n", boxers[p].power, 0x7F);
            printf("12) Stamina:   %9d / %9d\n", boxers[p].stamina, 0x7F);
            printf("13) Hand:      %9d / %9d\n", boxers[p].hand, 0x01);
            printf("14) Fights:    %9d / %9d\n", boxers[p].fights, 0x7FFF);
            printf("15) Wins:      %9d / %9d\n", boxers[p].wins, 0x7FFF);
            printf("16) Losses:    %9d / %9d\n", boxers[p].losses, 0x7FFF);
            printf("17) Draws:     %9d / %9d\n", boxers[p].draws, 0x7FFF);
            printf("18) Money:     %9d / %9d\n", boxers[p].money, 0x3B9AC9FF);
            printf("19) KO's:      %9d / %9d\n", boxers[p].kos, 0x7FFF);
            printf("20) TKO's:     %9d / %9d\n", boxers[p].tkos, 0x7FFF);
            printf("21) Decisions: %9d / %9d\n", boxers[p].decisions, 0x3E7);
            printf("22) Unanimous: %9d / %9d\n", boxers[p].unanimous, 0x3E7);
            printf("23) Disquals:  %9d / %9d\n", boxers[p].disquals, 0x7FFF);
            printf("\n");
        } else if (ch == 'Q') {
            UpdateBuffers();
            break;
        } else {
            num = atoi(buffer);
            switch (num) {
                case 1:
                    GetBuffer("height", &boxers[p].height, 0x8F);
                    break;
                case 2:
                    GetBuffer("weight", &boxers[p].weight, 0xE5);
                    break;
                case 3:
                    GetBuffer("head", &boxers[p].head, 0x0D);
                    break;
                case 4:
                    GetBuffer("clothes", &boxers[p].clothes, 0xFF);
                    break;
                case 5:
                    GetBuffer("ranking", &boxers[p].ranking, 0xFF);
                    break;
                case 6:
                    GetBuffer("power*", &boxers[p].scrpower, 0xFF);
                    break;
                case 7:
                    GetBuffer("stamina*", &boxers[p].scrstamina, 0xFF);
                    break;
                case 8:
                    GetBuffer("speed*", &boxers[p].scrspeed, 0xFF);
                    break;
                case 9:
                    GetBuffer("overall*", &boxers[p].scroverall, 0xFF);
                    break;
                case 10:
                    GetBuffer("slowness", &boxers[p].slowness, 0x7F);
                    break;
                case 11:
                    GetBuffer("power", &boxers[p].power, 0x7F);
                    break;
                case 12:
                    GetBuffer("stamina", &boxers[p].stamina, 0x7F);
                    break;
                case 13:
                    GetBuffer("hand", &boxers[p].hand, 0x01);
                    break;
                case 14:
                    GetBuffer("fights", &boxers[p].fights, 0x7FFF);
                    break;
                case 15:
                    GetBuffer("wins", &boxers[p].wins, 0x7FFF);
                    break;
                case 16:
                    GetBuffer("losses", &boxers[p].losses, 0x7FFF);
                    break;
                case 17:
                    GetBuffer("draws", &boxers[p].draws, 0x7FFF);
                    break;
                case 18:
                    GetBuffer("money", &boxers[p].money, 0x3B9AC9FF);
                    break;
                case 19:
                    GetBuffer("ko's", &boxers[p].kos, 0x7FFF);
                    break;
                case 20:
                    GetBuffer("tko's", &boxers[p].tkos, 0x7FFF);
                    break;
                case 21:
                    GetBuffer("decisions", &boxers[p].decisions, 0x3E7);
                    break;
                case 22:
                    GetBuffer("unanimous", &boxers[p].unanimous, 0x3E7);
                    break;
                case 23:
                    GetBuffer("disquals", &boxers[p].disquals, 0x7FFF);
                    break;
            }
        }
    }
    free(filedata);
    free(boxers);
}

void GetBuffer(const char* msg, int* data, int limit) {
    printf("Enter new value for ");
    printf("%s (%d/%d): ", msg, *data, limit);
    *data = atoi(fgets(buffer,sizeof(buffer), stdin));
}

void UpdateBuffers(void) {
    int i;
    int reloc;
    FILE* fp;
    i = 0;
    reloc = 0;
    while (i < boxersn) {
        filedata[reloc+0x1B] = boxers[i].height;
        filedata[reloc+0x1C] = boxers[i].weight;
        filedata[reloc+0x1D] = boxers[i].head;
        filedata[reloc+0x1E] = boxers[i].clothes;
        filedata[reloc+0x1F] = boxers[i].ranking;
        filedata[reloc+0x20] = boxers[i].scrpower;
        filedata[reloc+0x21] = boxers[i].scrstamina;
        filedata[reloc+0x22] = boxers[i].scrspeed;
        filedata[reloc+0x23] = boxers[i].scroverall;
        filedata[reloc+0x33] = boxers[i].slowness;
        filedata[reloc+0x35] = boxers[i].power;
        filedata[reloc+0x39] = boxers[i].stamina;
        filedata[reloc+0x4E] = boxers[i].hand;
        filedata[reloc+0x8A] = boxers[i].fights & 0xFF;
        filedata[reloc+0x8B] = boxers[i].fights >> 8;
        filedata[reloc+0x8C] = boxers[i].wins & 0xFF;
        filedata[reloc+0x8D] = boxers[i].wins >> 8;
        filedata[reloc+0x8E] = boxers[i].losses & 0xFF;
        filedata[reloc+0x8F] = boxers[i].losses >> 8;
        filedata[reloc+0x90] = boxers[i].draws & 0xFF;
        filedata[reloc+0x91] = boxers[i].draws >> 8;
        filedata[reloc+0x92] = boxers[i].money & 0xFF;
        filedata[reloc+0x93] = (boxers[i].money>>8) & 0xFF;
        filedata[reloc+0x94] = (boxers[i].money>>16) & 0xFF;
        filedata[reloc+0x95] = boxers[i].money >> 24;
        filedata[reloc+0x96] = boxers[i].kos & 0xFF;
        filedata[reloc+0x97] = boxers[i].kos >> 8;
        filedata[reloc+0x98] = boxers[i].tkos & 0xFF;
        filedata[reloc+0x99] = boxers[i].tkos >> 8;
        filedata[reloc+0x9A] = boxers[i].decisions & 0xFF;
        filedata[reloc+0x9B] = boxers[i].decisions >> 8;
        filedata[reloc+0x9C] = boxers[i].unanimous & 0xFF;
        filedata[reloc+0x9D] = boxers[i].unanimous >> 8;
        filedata[reloc+0x9E] = boxers[i].disquals & 0xFF;
        filedata[reloc+0x9F] = boxers[i].disquals >> 8;
        reloc += BLOCKSIZE;
        i++;
    }
    fp = fopen(filename, "w");
    fwrite(filedata, sizeof(unsigned char), filesize, fp);
    fclose(fp);
}

