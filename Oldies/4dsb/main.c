#include "4DSBs.h"

int main(int argc, char* argv[]) {
    if (argc != 2) {
        printf("4D Sports Boxing editor\n");
        printf("Usage: 4DSBs filename\n");
        return -1;
    }
    if (Initialize(argv[1]) == FALSE)
        return -1;
    if (CreateInternals() == FALSE)
        return -1;
    MainInterface();
    return 0;
}
