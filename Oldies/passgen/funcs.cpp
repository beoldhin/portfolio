// Main functions

#include "passgen.h"
#include "macros.h"

extern BOOL bIsOSR2;
extern HANDLE hfConfig;
extern HGLOBAL hmConfig;
extern HGLOBAL hmPass;
extern int iCharListSize;
extern int iEolListSize;
extern int iScreenX, iScreenY;
extern char *psCharList;
extern char *psEolList;
extern cfstruct *pstConfigData;
extern char *pszPassword;
extern chlstruct stCharList[];
extern eollstruct stEolList[];
extern char szConfFile[];

void ProgramInitialization(void)
{
	static SYSTEMTIME stSystemTime;
	static FILETIME stFileTime;
	static DWORD dwBytesWrote;
	static DWORD dwFileSizeLow;
	static DWORD dwFileSizeHigh;
	static OSVERSIONINFO stWinVersion;
	
	GetSystemTime(&stSystemTime);
	SystemTimeToFileTime(&stSystemTime, &stFileTime);
	if ((stFileTime.dwLowDateTime==0) && (stFileTime.dwHighDateTime==0))
		sgenrand(1, 0, (unsigned long)&pszPassword, (unsigned long)&pstConfigData);
	else
		sgenrand(stFileTime.dwLowDateTime, stFileTime.dwHighDateTime, (unsigned long)&pszPassword, (unsigned long)&pstConfigData);
	stWinVersion.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	GetVersionEx(&stWinVersion);
	if ((stWinVersion.dwPlatformId==VER_PLATFORM_WIN32_WINDOWS)&&(stWinVersion.dwBuildNumber>1000))
		bIsOSR2 = TRUE;
	else
		bIsOSR2 = FALSE;
	iScreenX = GetSystemMetrics(SM_CXSCREEN);
	iScreenY = GetSystemMetrics(SM_CYSCREEN);
	GlobalInsertM(hmConfig, pstConfigData, GHND, sizeof(cfstruct), cfstruct);
	hfConfig = CreateFile(szConfFile, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_ARCHIVE, NULL);
	dwFileSizeLow = GetFileSize(hfConfig, &dwFileSizeHigh);
	if ((hfConfig==INVALID_HANDLE_VALUE) || (dwFileSizeLow!=sizeof(cfstruct)))
	{
		if (dwFileSizeLow != sizeof(cfstruct))
			NullifyNCloseFile(hfConfig, FALSE);
		hfConfig = CreateFile(szConfFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_ARCHIVE, NULL);
		pstConfigData->states.statusbar = TRUE;
		pstConfigData->states.positions = TRUE;
		pstConfigData->states.sizes = TRUE;
		pstConfigData->gendlg.echars = 8;
		pstConfigData->defdlg1.rchset = RNUMUPLO;
		pstConfigData->defdlg1.cnorep = BST_UNCHECKED;
		pstConfigData->defdlg1.coutf = BST_UNCHECKED;
		pstConfigData->defdlg2.eentries = 10;
		pstConfigData->defdlg2.reoltype = RMSDOS;
		pstConfigData->firstruns.mw = TRUE;
		pstConfigData->firstruns.gen = TRUE;
		pstConfigData->firstruns.def1 = TRUE;
		pstConfigData->firstruns.def2 = TRUE;
		pstConfigData->firstruns.about = TRUE;
		UpdateDef1DlgSettingsM();
		UpdateDef2DlgSettingsM();
		NullifyNCloseFile(hfConfig, TRUE);
	}
	else
	{
		ReadFile(hfConfig, pstConfigData, sizeof(cfstruct), &dwBytesWrote, NULL);
		UpdateDef1DlgSettingsM();
		UpdateDef2DlgSettingsM();
		NullifyNCloseFile(hfConfig, FALSE);
	}
}

void ProgramUninitialization(HWND hWnd)
{
	static DWORD dwBytesWrote;

	hfConfig = CreateFile(szConfFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_ARCHIVE, NULL);
	GetWindowLayoutM(hWnd, pstConfigData->mwlayout, pstConfigData->firstruns.mw);
	WriteFile(hfConfig, pstConfigData, sizeof(cfstruct), &dwBytesWrote, NULL);
	GlobalRemoveM(hmConfig);
	NullifyNCloseFile(hfConfig, TRUE);
}

void NullifyNCloseFile(HANDLE hFile, BOOL bNullify)
{
	if (bNullify == TRUE)
	{
		FILETIME stFileTime;
		DosDateTimeToFileTime(33, 0, &stFileTime);
		LocalFileTimeToFileTime(&stFileTime, &stFileTime);
		SetFileTime(hFile, &stFileTime, &stFileTime, &stFileTime);
	}
	CloseHandle(hFile);
}