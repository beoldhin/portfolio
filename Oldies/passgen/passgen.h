// Main header file

#include <windows.h>
#include <commctrl.h>
#include "passgen.rh"

#define CHLISTS					15
#define CHLIST1SIZE				94
#define CHLIST2SIZE				62
#define CHLIST3SIZE				68
#define CHLIST4SIZE				68
#define CHLIST5SIZE				84
#define CHLIST6SIZE				36
#define CHLIST7SIZE				36
#define CHLIST8SIZE				42
#define CHLIST9SIZE				52
#define CHLIST10SIZE				58
#define CHLIST11SIZE				58
#define CHLIST12SIZE				10
#define CHLIST13SIZE				26
#define CHLIST14SIZE				26
#define CHLIST15SIZE				32
#define EOLLISTS					3
#define EOLLIST1SIZE				2
#define EOLLIST2SIZE				1
#define EOLLIST3SIZE				1

// Window functions
LRESULT CALLBACK PasswordGenerator(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK GenerateDialog(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK Define1Dialog(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK Define2Dialog(HWND, UINT, WPARAM, LPARAM);
BOOL CALLBACK AboutDialog(HWND, UINT, WPARAM, LPARAM);
// Main functions
void ProgramInitialization(void);
void ProgramUninitialization(HWND);
void NullifyNCloseFile(HWND, BOOL);
// Window manipulation functions
void UpdateStatusBarPaneData(void);
void ShowWindowPos(HWND);
void ShowWindowSize(HWND);
void SetViewMenuStates(void);
void CreateStatusBar(HWND);
void ChangeDef1ButtonState(WORD, HWND);
void ChangeDef2ButtonState(WORD, HWND);
void ChangeStatusBarLayout(WPARAM, LPARAM);
void IsStatusBarGenDataChanged(HWND);
void IsStatusBarDef1DataChanged(HWND);
void IsStatusBarDef2DataChanged(HWND);
void SetAboutDlgTitleBar(HWND, char szAboutMsg1[], char szAboutMsg8[]);
// External functions
extern void sgenrand(DWORD, DWORD, DWORD, DWORD);
extern double genrand(void);

struct cfstruct
{
	struct
	{
		BOOL statusbar : 1;
		BOOL positions : 1;
		BOOL sizes : 1;
	} states;
	struct
	{
		UINT echars;
	} gendlg;
	struct
	{
		int rchset;
		UINT cnorep;
		UINT coutf;
	} defdlg1;
	struct
	{
		UINT eentries;
		int reoltype;
	} defdlg2;
	struct
	{
		BOOL mw : 1;
		BOOL gen : 1;
		BOOL def1 : 1;
		BOOL def2 : 1;
		BOOL about : 1;
	} firstruns;
	struct
	{
		int x1;
		int y1;
		int x2;
		int y2;
	} mwlayout;
	struct
	{
		int x1;
		int y1;
		int x2;
		int y2;
	} genlayout;
	struct
	{
		int x1;
		int y1;
		int x2;
		int y2;
	} def1layout;
	struct
	{
		int x1;
		int y1;
		int x2;
		int y2;
	} def2layout;
	struct
	{
		int x1;
		int y1;
		int x2;
		int y2;
	} aboutlayout;
};

struct chlstruct
{
	char *charlist;
	int charlistsize;
};

struct eollstruct
{
	char *eollist;
	int eollistsize;
};