// Window manipulation functions

#include "passgen.h"
#include "macros.h"

extern HMENU hMenu;
extern HWND hStatusBar;
extern int iSBWidths[];
extern char szProgramName[];

extern BOOL bIsOSR2;
extern DWORD iI, iJ, iK, iL, iM;
extern int iCharListSize;
extern int iEolListSize;
extern cfstruct *pstConfigData;
extern ULARGE_INTEGER unDiskSpace;


void UpdateStatusBarPaneData(void)
{
	static char szGenDlgMsg1[] = "Chars: ";
	static char szDefDlg1Msg1[15][18] = {"All, ","Num | Up | Lo, ","Num | Up | Rest, ","Num | Lo | Rest, ","Up | Lo | Rest, ","Num | Up, ","Num | Lo, ","Num | Rest, ","Up | Lo, ","Up | Rest, ","Lo | Rest, ","Num, ","Up, ","Lo, ","Rest, "};
	static char szDefDlg1Msg2[2][7] = {"Rep","Nonrep"};
	static char szDefDlg2Msg1[] = "Entries: ";
	static char szDefDlg2Msg2[] = ", ";
	static char szDefDlg2Msg3[3][10] = {"DOS | Win","Nix | Ami","Mac"};
	static char szPane[200];

	if (pstConfigData->states.statusbar == TRUE)
	{
		strcpy(szPane, szGenDlgMsg1);
		itoa(pstConfigData->gendlg.echars, strchr(szPane,0), 10);
		SendMessage(hStatusBar, SB_SETTEXT, 0|SBT_POPOUT, (LPARAM)szPane);
		strcpy(szPane, szDefDlg1Msg1[pstConfigData->defdlg1.rchset-RALLCHARS]);
		if (pstConfigData->defdlg1.cnorep == BST_UNCHECKED)
			strcat(szPane, szDefDlg1Msg2[0]);
		else
			strcat(szPane, szDefDlg1Msg2[1]);
		SendMessage(hStatusBar, SB_SETTEXT, 1|SBT_POPOUT, (LPARAM)szPane);
		if (pstConfigData->defdlg1.coutf == BST_CHECKED)
		{
			strcpy(szPane, szDefDlg2Msg1);
			itoa(pstConfigData->defdlg2.eentries, strchr(szPane,0), 10);
			strcat(szPane, szDefDlg2Msg2);
			strcat(szPane, szDefDlg2Msg3[pstConfigData->defdlg2.reoltype-RMSDOS]);
			SendMessage(hStatusBar, SB_SETTEXT, 2|SBT_POPOUT, (LPARAM)szPane);
		}
		else
			SendMessage(hStatusBar, SB_SETTEXT, 2, (LPARAM)"");
	}
}

void ShowWindowPos(HWND hWnd)
{
	static char szTitleBarMsg1[] = "(";
	static char szTitleBarMsg2[] = ",";
	static char szTitleBarMsg3[] = ")";
	static char szTitleBar[24];

	if (pstConfigData->states.positions == TRUE)
	{
		RECT stWinLayout;
		GetWindowRect(hWnd, &stWinLayout);
		strcpy(szTitleBar, szTitleBarMsg1);
		itoa(stWinLayout.left, strchr(szTitleBar,0), 10);
		strcat(szTitleBar, szTitleBarMsg2);
		itoa(stWinLayout.top, strchr(szTitleBar,0), 10);
		strcat(szTitleBar, szTitleBarMsg3);
		SetWindowText(hWnd, szTitleBar);
	}
}

void ShowWindowSize(HWND hWnd)
{
	static char szTitleBarMsg1[] = "(";
	static char szTitleBarMsg2[] = "x";
	static char szTitleBarMsg3[] = ")";
	static char szTitleBar[24];

	if (pstConfigData->states.sizes == TRUE)
	{
		RECT stWinLayout;
		GetWindowRect(hWnd, &stWinLayout);
		strcpy(szTitleBar, szTitleBarMsg1);
		itoa(stWinLayout.right-stWinLayout.left, strchr(szTitleBar,0), 10);
		strcat(szTitleBar, szTitleBarMsg2);
		itoa(stWinLayout.bottom-stWinLayout.top, strchr(szTitleBar,0), 10);
		strcat(szTitleBar, szTitleBarMsg3);
		SetWindowText(hWnd, szTitleBar);
	}
}

void SetViewMenuStates(void)
{
	if (pstConfigData->states.statusbar == TRUE)
	CheckMenuItem(hMenu, MSBAR, MF_CHECKED);
	if (pstConfigData->states.positions == TRUE)
	CheckMenuItem(hMenu, MPOSITS, MF_CHECKED);
	if (pstConfigData->states.sizes == TRUE)
	CheckMenuItem(hMenu, MSIZES, MF_CHECKED);
}

void CreateStatusBar(HWND hWnd)
{
	static RECT stClientArea;

	hStatusBar = CreateStatusWindow(WS_CHILD|WS_VISIBLE|WS_CLIPSIBLINGS|SBARS_SIZEGRIP|CCS_BOTTOM, "", hWnd, STATUSW);
	GetClientRect(hWnd, &stClientArea);
	iSBWidths[0] = stClientArea.right * 1/3;
	iSBWidths[1] = stClientArea.right * 2/3;
	SendMessage(hStatusBar, SB_SETPARTS, 3, (LPARAM)iSBWidths);
}

void ChangeDef1ButtonState(WORD wParamHi, HWND hDlg)
{
	if (wParamHi == EN_UPDATE)
	{
		iI = GetDlgItemInt(hDlg, ECHARS, NULL, FALSE);
		if (bIsOSR2 == TRUE)
		{
			iJ = pstConfigData->defdlg2.eentries * (iI+iEolListSize+1);
			GetDiskFreeSpaceEx(NULL, &unDiskSpace, NULL, NULL);
			if ((iI==0) || ((pstConfigData->defdlg1.cnorep==TRUE)&&(iI>iCharListSize)) || (iJ>unDiskSpace.QuadPart))
				EnableWindow(GetDlgItem(hDlg, BGENERATE), FALSE);
			else
				EnableWindow(GetDlgItem(hDlg, BGENERATE), TRUE);
		}
		else
		{
			if ((iI==0) || ((pstConfigData->defdlg1.cnorep==TRUE)&&(iI>iCharListSize)))
				EnableWindow(GetDlgItem(hDlg, BGENERATE), FALSE);
			else
				EnableWindow(GetDlgItem(hDlg, BGENERATE), TRUE);
		}
	}
}

void ChangeDef2ButtonState(WORD wParamHi, HWND hDlg)
{
	if (wParamHi == EN_UPDATE)
	{
		iI = GetDlgItemInt(hDlg, EENTRIES, NULL, FALSE);
		if (bIsOSR2 == TRUE)
		{
			iJ = iI * (pstConfigData->gendlg.echars+iEolListSize+1);
			GetDiskFreeSpaceEx(NULL, &unDiskSpace, NULL, NULL);
			if ((iI==0) || (iJ>unDiskSpace.QuadPart))
				EnableWindow(GetDlgItem(hDlg, BDONE), FALSE);
			else
				EnableWindow(GetDlgItem(hDlg, BDONE), TRUE);
		}
		else
		{
			if (iI == 0)
				EnableWindow(GetDlgItem(hDlg, BDONE), FALSE);
			else
				EnableWindow(GetDlgItem(hDlg, BDONE), TRUE);
		}
	}
}

void ChangeStatusBarLayout(WPARAM wParam, LPARAM lParam)
{
	SendMessage(hStatusBar, WM_SIZE, wParam, lParam);
	iSBWidths[0] = LOWORD(lParam) * 1/3;
	iSBWidths[1] = LOWORD(lParam) * 2/3;
	SendMessage(hStatusBar, SB_SETPARTS, 3, (LPARAM)iSBWidths);
}

void IsStatusBarGenDataChanged(HWND hDlg)
{
	iI = GetDlgItemInt(hDlg, ECHARS, NULL, FALSE);
	if (pstConfigData->gendlg.echars != iI)
	{
		pstConfigData->gendlg.echars = iI;
		UpdateStatusBarPaneData();
	}
}

void IsStatusBarDef1DataChanged(HWND hDlg)
{
	for (iI=RALLCHARS; iI<=RREST; iI++)
		if (IsDlgButtonChecked(hDlg, iI) == BST_CHECKED)
			break;
	iJ = IsDlgButtonChecked(hDlg, CNOREP);
	iK = IsDlgButtonChecked(hDlg, COUTF);
	if ((pstConfigData->defdlg1.rchset!=iI) || (pstConfigData->defdlg1.cnorep!=iJ) || (pstConfigData->defdlg1.coutf!=iK))
	{
		pstConfigData->defdlg1.rchset = iI;
		pstConfigData->defdlg1.cnorep = iJ;
		pstConfigData->defdlg1.coutf = iK;
		UpdateStatusBarPaneData();
	}
}

void IsStatusBarDef2DataChanged(HWND hDlg)
{
	iI = GetDlgItemInt(hDlg, EENTRIES, NULL, FALSE);
	for (iJ=RMSDOS; iJ<=RMAC; iJ++)
		if (IsDlgButtonChecked(hDlg, iJ) == BST_CHECKED)
			break;
	if ((pstConfigData->defdlg2.eentries!=iI) || (pstConfigData->defdlg2.reoltype!=iJ))
	{
		pstConfigData->defdlg2.eentries = iI;
		pstConfigData->defdlg2.reoltype = iJ;
		UpdateStatusBarPaneData();
	}
}

void SetAboutDlgTitleBar(HWND hDlg, char szAboutMsg1[], char szAboutMsg8[])
{
	strcpy(szAboutMsg8, szAboutMsg1);
	strcat(szAboutMsg8, szProgramName);
	SetWindowText(hDlg, szAboutMsg8);
}