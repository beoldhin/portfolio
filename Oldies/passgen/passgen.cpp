// The main program
//
// All files include:
// passgen.cpp
// passgen.h
// funcs.cpp
// winman.cpp
// macros.h
// passgen.rh
// mt19937.cpp

#include <math.h>
#include "passgen.h"
#include "macros.h"

HMENU hMenu;
HWND hSpinner;
HWND hStatusBar;
HINSTANCE hWinInstance;
HWND hWndParent;
int iSBWidths[3] = {0, 0, -1};
UDACCEL stSpinAccel[5] = {{2,1}, {5,10}, {10,100}, {20,1000}, {30,10000}};
char szProgramName[] = "Password Generator";

BOOL bIsOSR2 = FALSE;
HANDLE hfConfig;
HANDLE hfOutput;
HGLOBAL hmConfig;
HGLOBAL hmPass;
DWORD iI, iJ, iK, iL, iM;
int iPercentage;
int iScreenX, iScreenY;
int iX, iY, iWidth, iHeight;
cfstruct *pstConfigData;
char sCharList1[CHLIST1SIZE] = {'!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?','@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\\',']','^','_','`','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','{','|','}','~'};
char sCharList2[CHLIST2SIZE] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
char sCharList3[CHLIST3SIZE] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[','\\',']','^','_','`','{','|','}','~'};
char sCharList4[CHLIST4SIZE] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[','\\',']','^','_','`','{','|','}','~'};
char sCharList5[CHLIST5SIZE] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[','\\',']','^','_','`','{','|','}','~'};
char sCharList6[CHLIST6SIZE] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
char sCharList7[CHLIST7SIZE] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
char sCharList8[CHLIST8SIZE] = {'0','1','2','3','4','5','6','7','8','9','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[','\\',']','^','_','`','{','|','}','~'};
char sCharList9[CHLIST9SIZE] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
char sCharList10[CHLIST10SIZE] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[','\\',']','^','_','`','{','|','}','~'};
char sCharList11[CHLIST11SIZE] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[','\\',']','^','_','`','{','|','}','~'};
char sCharList12[CHLIST12SIZE] = {'0','1','2','3','4','5','6','7','8','9'};
char sCharList13[CHLIST13SIZE] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
char sCharList14[CHLIST14SIZE] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
char sCharList15[CHLIST15SIZE] = {'!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/',':',';','<','=','>','?','@','[','\\',']','^','_','`','{','|','}','~'};
chlstruct stCharList[CHLISTS] = {{sCharList1,CHLIST1SIZE},{sCharList2,CHLIST2SIZE},{sCharList3,CHLIST3SIZE},{sCharList4,CHLIST4SIZE},{sCharList5,CHLIST5SIZE},{sCharList6,CHLIST6SIZE},{sCharList7,CHLIST7SIZE},{sCharList8,CHLIST8SIZE},{sCharList9,CHLIST9SIZE},{sCharList10,CHLIST10SIZE},{sCharList11,CHLIST11SIZE},{sCharList12,CHLIST12SIZE},{sCharList13,CHLIST13SIZE},{sCharList14,CHLIST14SIZE},{sCharList15,CHLIST15SIZE}};
int iCharListSize = stCharList[0].charlistsize;
char *psCharList = stCharList[0].charlist;
char *pszPassword;
char sEolList1[EOLLIST1SIZE] = {13, 10};
char sEolList2[EOLLIST2SIZE] = {10};
char sEolList3[EOLLIST3SIZE] = {13};
eollstruct stEolList[EOLLISTS] = {{sEolList1,EOLLIST1SIZE},{sEolList2,EOLLIST2SIZE},{sEolList3,EOLLIST3SIZE}};
int iEolListSize = stEolList[0].eollistsize;
char *psEolList = stEolList[0].eollist;
char szConfFile[] = "c:\conf.pg";
char szOldTitleBar[200];
char szOutputFile[] = "c:\out.pg";
char szProgramVersion[] = "Password Generator v1.8";
ULARGE_INTEGER unDiskSpace;


int WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInstance, PSTR pszCmdLine, int iCmdShow)
{
	hWinInstance = hInst;
	MSG msg;
	WNDCLASSEX wndclassex;

	InitCommonControls();
	ProgramInitialization();

	if (!hPrevInstance)
	{
		wndclassex.cbSize = sizeof(WNDCLASSEX);
		wndclassex.style = CS_HREDRAW|CS_VREDRAW;
		wndclassex.lpfnWndProc = (WNDPROC)PasswordGenerator;
		wndclassex.cbClsExtra = 0;
		wndclassex.cbWndExtra = 0;
		wndclassex.hInstance = hInst;
		wndclassex.hIcon = LoadIcon(hWinInstance, IDI_APPLICATION);
		wndclassex.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndclassex.hbrBackground = GetStockObject(GRAY_BRUSH);
		wndclassex.lpszMenuName = NULL;
		wndclassex.lpszClassName = szProgramName;
		wndclassex.hIconSm = LoadIcon(hWinInstance, IDI_APPLICATION);
		if (!RegisterClassEx(&wndclassex))
			return FALSE;
	}

	hMenu = LoadMenu(hInst, MAKEINTRESOURCE(MMAIN));
	if (pstConfigData->firstruns.mw == TRUE)
	{
		iX = iScreenX * 1/8;
		iY = iScreenY * 1/8;
		iWidth = (iScreenX*7/8) - (iScreenX*1/8);
		iHeight = (iScreenY*7/8) - (iScreenY*1/8);
		hWndParent = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW, szProgramName, szProgramName, WS_OVERLAPPEDWINDOW, iX, iY, iWidth, iHeight, NULL, hMenu, hInst, NULL);
		GetWindowLayoutM(hWndParent, pstConfigData->mwlayout, pstConfigData->firstruns.mw);
	}
	else
	{
		iX = pstConfigData->mwlayout.x1;
		iY = pstConfigData->mwlayout.y1;
		iWidth = pstConfigData->mwlayout.x2 - pstConfigData->mwlayout.x1;
		iHeight = pstConfigData->mwlayout.y2 - pstConfigData->mwlayout.y1;
		hWndParent = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW, szProgramName, szProgramName, WS_OVERLAPPEDWINDOW, iX, iY, iWidth, iHeight, NULL, hMenu, hInst, NULL);
	}

	ShowWindow(hWndParent, iCmdShow);
	UpdateWindow(hWndParent);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}

LRESULT CALLBACK PasswordGenerator(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static BOOL bWMoved;

	switch (iMsg)
	{
		case WM_CREATE:
			bWMoved = FALSE;
			GetWindowText(hWnd, szOldTitleBar, 200);
			SetViewMenuStates();
			if (pstConfigData->states.statusbar == TRUE)
			{
				CreateStatusBar(hWnd);
				UpdateStatusBarPaneData();
			}
			break;

		case WM_SIZE:
			ChangeStatusBarLayout(wParam, lParam);
			if (bWMoved == TRUE)
				ShowWindowSize(hWnd);
			break;

		case WM_MOVE:
			if (bWMoved == TRUE)
				ShowWindowPos(hWnd);
			bWMoved = TRUE;
			break;

		case WM_EXITSIZEMOVE:
			if (bWMoved == TRUE)
				SetWindowText(hWnd, szOldTitleBar);
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case MGENERATE:
					DialogBox(hWinInstance, MAKEINTRESOURCE(DGENERATE), hWnd, (DLGPROC)GenerateDialog);
					break;

				case MDEFINE:
					DialogBox(hWinInstance, MAKEINTRESOURCE(DDEFINE), hWnd, (DLGPROC)Define1Dialog);
					break;

				case MEXIT:
					SendMessage(hWnd, WM_DESTROY, NULL, NULL);
					break;

				case MSBAR:
					ChangeViewStateM(pstConfigData->states.statusbar, MSBAR, TRUE);
					break;

				case MPOSITS:
					ChangeViewStateM(pstConfigData->states.positions, MPOSITS, FALSE);
					break;

				case MSIZES:
					ChangeViewStateM(pstConfigData->states.sizes, MSIZES, FALSE);
					break;

				case MABOUT:
					DialogBox(hWinInstance, MAKEINTRESOURCE(DABOUT), hWnd, (DLGPROC)AboutDialog);
					break;
			}
			break;

		case WM_DESTROY:
			ProgramUninitialization(hWnd);
			PostQuitMessage(0);
			break;

		default:
			return (DefWindowProc(hWnd, iMsg, wParam, lParam));

	}
	return TRUE;
}

BOOL CALLBACK GenerateDialog(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static int iEntry;
	static UINT iAllChars;
	static DWORD dwBytesWrote;
	static char szDoneMsg[] = "Done.";
	static HGLOBAL hmTitleBar;
	static char *szTitleBar;
	static int iWMoved;
	static HGLOBAL hmTemp;
	static char *psTempTable;

	switch (iMsg)
	{
		case WM_INITDIALOG:
			iWMoved = 0;
			GetWindowText(hDlg, szOldTitleBar, 200);
			hSpinner = CreateUpDownControl(WS_CHILD|WS_BORDER|WS_VISIBLE|UDS_ALIGNRIGHT|UDS_NOTHOUSANDS|UDS_SETBUDDYINT, 0, 0, 0, 0, hDlg, UCHARS, hWinInstance, GetDlgItem(hDlg, ECHARS), SHRT_MAX, 1, 0);
			SendMessage(hSpinner, UDM_SETACCEL, 5, (LPARAM)stSpinAccel);
			SetDlgItemInt(hDlg, ECHARS, pstConfigData->gendlg.echars, FALSE);
			SetWindowLayoutM(hDlg, pstConfigData->firstruns.gen, pstConfigData->genlayout);
			break;

		case WM_MOVE:
			if (iWMoved == 2)
				ShowWindowPos(hDlg);
			else
				iWMoved++;
			break;

		case WM_EXITSIZEMOVE:
			if (iWMoved == 2)
				SetWindowText(hDlg, szOldTitleBar);
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case BGENERATE:
					iAllChars = GetDlgItemInt(hDlg, ECHARS, NULL, FALSE);
					if (pstConfigData->defdlg1.coutf == BST_CHECKED)
					{
						iPercentage = 0;
						GlobalInsertM(hmTitleBar, szTitleBar, GHND, GetWindowTextLength(hDlg)+1, char);
						GetWindowText(hDlg, szTitleBar, GetWindowTextLength(hDlg)+1);
						hfOutput = CreateFile(szOutputFile, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_ARCHIVE, NULL);
						if ((pstConfigData->defdlg1.cnorep==BST_UNCHECKED) || ((pstConfigData->defdlg1.cnorep==BST_CHECKED)&&(iAllChars==1)))
						{
							GlobalInsertM(hmPass, pszPassword, GHND, iAllChars+iEolListSize+1, char);
							for (iEntry=0; iEntry<pstConfigData->defdlg2.eentries; iEntry++)
							{
								GenerateRepeatingPassword();
								strncpy(pszPassword+iAllChars, psEolList, iEolListSize);
								WriteFile(hfOutput, pszPassword, iAllChars+iEolListSize, &dwBytesWrote, NULL);
								AdvancePercentageTitleBarM();
							}
							GlobalRemoveM(hmPass);
						}
						else
						{
							GlobalInsertM(hmPass, pszPassword, GHND, iAllChars+iEolListSize+1, char);
							GlobalInsertM(hmTemp, psTempTable, GHND, iCharListSize, char);
							for (iEntry=0; iEntry<pstConfigData->defdlg2.eentries; iEntry++)
							{
								GenerateUnrepeatingPassword();
								strncpy(pszPassword+iAllChars, psEolList, iEolListSize);
								WriteFile(hfOutput, pszPassword, iAllChars+iEolListSize, &dwBytesWrote, NULL);
								AdvancePercentageTitleBarM();
							}
							GlobalRemoveM(hmPass);
							GlobalRemoveM(hmTemp);
						}
						NullifyNCloseFile(hfOutput, TRUE);
						SetWindowText(hDlg, szTitleBar);
						GlobalRemoveM(hmTitleBar);
						SetDlgItemText(hDlg, ERESULT, szDoneMsg);
					}
					else
					{
						GlobalInsertM(hmPass, pszPassword, GHND, iAllChars+iEolListSize+1, char);
						if (pstConfigData->defdlg1.cnorep == BST_UNCHECKED)
						{
							GenerateRepeatingPassword();
						}
						else
						{
							GlobalInsertM(hmTemp, psTempTable, GHND, iCharListSize, char);
							GenerateUnrepeatingPassword();
							GlobalRemoveM(hmTemp);
						}
						SetDlgItemText(hDlg, ERESULT, pszPassword);
						SendDlgItemMessage(hDlg, ERESULT, CB_ADDSTRING, 0, (LPARAM)pszPassword);
						GlobalRemoveM(hmPass);
					}
					IsStatusBarGenDataChanged(hDlg);
					SetFocus(hDlg);
					break;

				case IDCANCEL:
					GetWindowLayoutM(hDlg, pstConfigData->genlayout, pstConfigData->firstruns.gen);
					EndDialog(hDlg, wParam);
					break;

				case ECHARS:
					ChangeDef1ButtonState(HIWORD(wParam), hDlg);
					break;
			}
			break;

		default:
			return FALSE;
	}
	return TRUE;
}

BOOL CALLBACK Define1Dialog(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static int iWMoved;

	switch (iMsg)
	{
		case WM_INITDIALOG:
			iWMoved = 0;
			GetWindowText(hDlg, szOldTitleBar, 200);
			CheckRadioButton(hDlg, RALLCHARS, RREST, pstConfigData->defdlg1.rchset);
			CheckDlgButton(hDlg, CNOREP, pstConfigData->defdlg1.cnorep);
			CheckDlgButton(hDlg, COUTF, pstConfigData->defdlg1.coutf);
			SetWindowLayoutM(hDlg, pstConfigData->firstruns.def1, pstConfigData->def1layout);
			break;

		case WM_MOVE:
			if (iWMoved == 2)
				ShowWindowPos(hDlg);
			else
				iWMoved++;
			break;

		case WM_EXITSIZEMOVE:
			if (iWMoved == 2)
				SetWindowText(hDlg, szOldTitleBar);
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case BDEFINE:
					IsStatusBarDef1DataChanged(hDlg);
					UpdateDef1DlgSettingsM();
					if (pstConfigData->defdlg1.coutf == BST_CHECKED)
						DialogBox(hWinInstance, MAKEINTRESOURCE(DOUTTOF), hDlg, (DLGPROC)Define2Dialog);
					GetWindowLayoutM(hDlg, pstConfigData->def1layout, pstConfigData->firstruns.def1);
					EndDialog(hDlg, wParam);
					break;

				case IDCANCEL:
					GetWindowLayoutM(hDlg, pstConfigData->def1layout, pstConfigData->firstruns.def1);
					EndDialog(hDlg, wParam);
					break;
			}
			break;

		default:
			return FALSE;
	}
	return TRUE;
}

BOOL CALLBACK Define2Dialog(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static int iWMoved;

	switch (iMsg)
	{
		case WM_INITDIALOG:
			iWMoved = 0;
			GetWindowText(hDlg, szOldTitleBar, 200);
			hSpinner = CreateUpDownControl(WS_CHILD|WS_BORDER|WS_VISIBLE|UDS_ALIGNRIGHT|UDS_NOTHOUSANDS|UDS_SETBUDDYINT, 0, 0, 0, 0, hDlg, UENTRIES, hWinInstance, GetDlgItem(hDlg, EENTRIES), SHRT_MAX, 1, 0);
			SendMessage(hSpinner, UDM_SETACCEL, 5, (LPARAM)stSpinAccel);
			SetDlgItemInt(hDlg, EENTRIES, pstConfigData->defdlg2.eentries, FALSE);
			CheckRadioButton(hDlg, RMSDOS, RMAC, pstConfigData->defdlg2.reoltype);
			SetWindowLayoutM(hDlg, pstConfigData->firstruns.def2, pstConfigData->def2layout);
			break;

		case WM_MOVE:
			if (iWMoved == 2)
				ShowWindowPos(hDlg);
			else
				iWMoved++;
			break;

		case WM_EXITSIZEMOVE:
			if (iWMoved == 2)
				SetWindowText(hDlg, szOldTitleBar);
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case BDONE:
					IsStatusBarDef2DataChanged(hDlg);
					UpdateDef2DlgSettingsM();
					GetWindowLayoutM(hDlg, pstConfigData->def2layout, pstConfigData->firstruns.def2);
					EndDialog(hDlg, wParam);
					break;

				case IDCANCEL:
					GetWindowLayoutM(hDlg, pstConfigData->def2layout, pstConfigData->firstruns.def2);
					EndDialog(hDlg, wParam);
					break;

				case EENTRIES:
					ChangeDef2ButtonState(HIWORD(wParam), hDlg);
					break;
			}
			break;

		default:
			return FALSE;
	}
	return TRUE;
}

BOOL CALLBACK AboutDialog(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	static char szAboutMsg1[] = "About ";
	static char szAboutMsg2[7][4] = {"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
	static char szAboutMsg3[12][4] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	static char szAboutMsg4[] = " ";
	static char szAboutMsg5[] = " / ";
	static char szAboutMsg6[] = "0";
	static char szAboutMsg7[] = ":";
	static char szAboutMsg8[200];
	static SYSTEMTIME stSystemTime;
	static int iWMoved;

	switch (iMsg)
	{
		case WM_INITDIALOG:
			iWMoved = 0;
			if (SetTimer(hDlg,TIMER1,1000,NULL) == 0)
			{
				MessageBox(hDlg, "Kill some timers to continue.", "No more clocks or timers!", MB_OK|MB_ICONERROR);
				EndDialog(hDlg, wParam);
			}
			SetAboutDlgTitleBar(hDlg, szAboutMsg1, szAboutMsg8);
			SetDlgItemText(hDlg, TVERSION, szProgramVersion);
			SendMessage(hDlg, WM_TIMER, NULL, NULL);
			SetWindowLayoutM(hDlg, pstConfigData->firstruns.about, pstConfigData->aboutlayout);
			break;

		case WM_MOVE:
			if (iWMoved == 2)
				ShowWindowPos(hDlg);
			if (iWMoved < 2)
				iWMoved++;
			break;

		case WM_EXITSIZEMOVE:
			if (iWMoved == 2)
				SetAboutDlgTitleBar(hDlg, szAboutMsg1, szAboutMsg8);
			if (iWMoved < 2)
				iWMoved++;
			break;

		case WM_TIMER:
			ShowDateNTimeM();
			break;

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
				case BOK:
					KillTimer(hDlg, TIMER1);
					GetWindowLayoutM(hDlg, pstConfigData->aboutlayout, pstConfigData->firstruns.about);
					EndDialog(hDlg, wParam);
					break;

				case IDCANCEL:
					SendMessage(hDlg, WM_COMMAND, BOK, NULL);
					break;
			}
			break;

		default:
			return FALSE;
	}
	return TRUE;
}