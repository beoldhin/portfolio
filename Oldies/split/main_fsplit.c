#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libintl.h>
#include "/home/olli/src/incs/inc_current.h"
#include "/home/olli/src/algo/reslist/reslist.h"

/* Note: ELEMSIZE must be non-zero */
#define ELEMSIZE 65536

typedef struct args {
    FILE* fp;
    char* nfname;
    char* element;
    size_t elemsize;
    size_t blocksize;
    DCLIST reslist;
} ARGS;

int fsplit_readwrite_block(ARGS*);


int main(int argc, char* argv[])
{
    size_t i;
    ARGS args;
    long fsize;
    char iprint = 0;
    size_t npos;
    char* fname = NULL;
    char* fremov = NULL;
    size_t ibytes = 0;
    size_t sbytes = 0;
    size_t blockw;
    size_t blockr;
    size_t cneeded;
    size_t nneeded = 0;  /* to silence gcc */
    const char fsmsg[] = "fseek failed";
    const char nfmsg[] = "malloc failed (nfname)";
    const char nnmsg[] = "Error: No need to split (%ld <= %u)\n";
    const char insmsg[] = "Insert new removable media %u/%u\n"\
                          "Press ENTER to continue . . .\n";
    reslist_screate(&args.reslist);
    if ((argc<3) || (argc>6)) {
        fprintf(stderr, gettext("File splitter v2.1 -- Copyright (C) 2002 Olli\n"));
        fprintf(stderr, gettext("Usage: fsplit <OPTIONS>... <FILE>\n\n"));
        fprintf(stderr, gettext("  -s<bytes>    subsequent block of <bytes> to use\n"));
        fprintf(stderr, gettext("  [-i<bytes>]  initial block of <bytes> to use\n"));
        fprintf(stderr, gettext("  [-r<path>]   path|drive of removable media\n"));
        fprintf(stderr, gettext("  [-p]         print initial size for next set\n"));
        reslist_clear(&args.reslist);
        return -1;
    }
    for (i=1; i<argc; ++i) {
        if (argv[i][0] == '-') {
            switch (argv[i][1]) {
              case 'i':
                sscanf(argv[i]+2, "%u", &ibytes);
                break;
              case 's':
                sscanf(argv[i]+2, "%u", &sbytes);
                break;
              case 'p':
                iprint = 1;
                break;
              case 'r':
                fremov = argv[i] + 2;
                break;
              default:
                fprintf(stderr, gettext("Error: Unknown option \"%s\"\n"), argv[i]+1);
                return -1;
            }
        } else
            fname = argv[i];
    }
    if (!sbytes) {
        fprintf(stderr, gettext("Error: Size of block not set\n"));
        reslist_clear(&args.reslist);
        return -1;
    }
    if (!fname) {
        fprintf(stderr, gettext("Error: Filename not set\n"));
        reslist_clear(&args.reslist);
        return -1;
    }
    if (!(args.fp=reslist_fopen(fname,"rb",&args.reslist))) {
        perror("fopen failed");
        reslist_clear(&args.reslist);
        return -1;
    }
    if (!(args.element=reslist_malloc(ELEMSIZE,&args.reslist))) {
        perror("malloc failed (ELEMSIZE)");
        reslist_clear(&args.reslist);
        return -1;
    }
    if (fremov) {
        npos = strlen(fremov) + strlen(fname) + 1;
        if (!(args.nfname=reslist_malloc(npos+3+1,&args.reslist))) {
            perror(nfmsg);
            reslist_clear(&args.reslist);
            return -1;
        }
        strcpy(args.nfname, fremov);
        strcat(args.nfname, fname);
    } else {
        npos = strlen(fname) + 1;
        if (!(args.nfname=reslist_malloc(npos+3+1,&args.reslist))) {
            perror(nfmsg);
            reslist_clear(&args.reslist);
            return -1;
        }
        strcpy(args.nfname, fname);
    }
    args.nfname[npos-1] = '.';
    /* nfname will be now X.000, X.001, X.002, etc... */
    if (fseek(args.fp,0,SEEK_END)) {
        perror(fsmsg);
        reslist_clear(&args.reslist);
        return -1;
    }
    if ((fsize=ftell(args.fp)) < 0) {
        perror("ftell failed");
        reslist_clear(&args.reslist);
        return -1;
    }
    if (((ibytes>0)&&(fsize<=ibytes)) || (fsize<=sbytes)) {
        fprintf(stderr, gettext(nnmsg), fsize, ibytes ? ibytes : sbytes);
        reslist_clear(&args.reslist);
        return -1;
    }
    if (ibytes > sbytes) {
        fprintf(stderr, gettext("Error: Initial size > subsequent size\n"));
        reslist_clear(&args.reslist);
        return -1;
    }
    if (fseek(args.fp,0,SEEK_SET)) {
        perror(gettext(fsmsg));
        reslist_clear(&args.reslist);
        return -1;
    }
    fsize -= ibytes;
    blockw = fsize / sbytes;
    blockr = fsize % sbytes;
    if (fremov) {
        nneeded = blockw;
        if (blockr)
            ++nneeded;
        if (ibytes)
            ++nneeded;
    }
    cneeded = 1;
    args.elemsize = ELEMSIZE;
    if (ibytes) {
        if (fremov) {
            printf(gettext(insmsg), cneeded, nneeded);
            getchar();
        }
        sprintf(args.nfname+npos, "%03X", 0);
        args.blocksize = ibytes;
        if (fsplit_readwrite_block(&args)) {
            reslist_clear(&args.reslist);
            return -1;
        }
        ++cneeded;
    }
    /* creating the new files... */
    args.blocksize = sbytes;
    if (ibytes)
        ++blockw;
    for (i=ibytes?1:0; i<blockw; ++i,++cneeded) {
        if (fremov) {
            printf(gettext(insmsg), cneeded, nneeded);
            getchar();
        }
        sprintf(args.nfname+npos, "%03X", i);
        if (fsplit_readwrite_block(&args)) {
            reslist_clear(&args.reslist);
            return -1;
        }
    }
    if (blockr) {
        if (fremov) {
            printf(gettext(insmsg), cneeded, nneeded);
            getchar();
        }
        sprintf(args.nfname+npos, "%03X", i);
        args.blocksize = blockr;
        if (fsplit_readwrite_block(&args)) {
            reslist_clear(&args.reslist);
            return -1;
        }
    }
    if (iprint)
        printf("%u", sbytes-blockr);
    reslist_clear(&args.reslist);
    return 0;
}

int fsplit_readwrite_block(ARGS* args)
{
    size_t i;
    FILE* nffp;
    size_t elemw;
    size_t elemr;
    FILE* fp = args->fp;
    char* nfname = args->nfname;
    char* element = args->element;
    size_t elemsize = args->elemsize;
    size_t blocksize = args->blocksize;
    char frmsg[] = "fread failed";
    char fwmsg[] = "fwrite failed";
    DCLIST* reslist = &args->reslist;
    if ((nffp=reslist_fopen(nfname,"rb",reslist))) {
        fprintf(stderr, gettext("Error: File \"%s\" already exists\n"), nfname);
        return -1;
    }
    if (!(nffp=reslist_fopen(nfname,"wb",reslist))) {
        perror(gettext("fopen failed"));
        return -1;
    }
    elemw = blocksize / elemsize;
    elemr = blocksize % elemsize;
    for (i=0; i<elemw; ++i) {
        if (fread(element,1,elemsize,fp) != elemsize) {
            perror(gettext(frmsg));
            return -1;
        }
        if (fwrite(element,1,elemsize,nffp) != elemsize) {
            perror(gettext(fwmsg));
            return -1;
        }
    }
    if (elemr) {
        if (fread(element,1,elemr,fp) != elemr) {
            perror(gettext(frmsg));
            return -1;
        }
        if (fwrite(element,1,elemr,nffp) != elemr) {
            perror(gettext(fwmsg));
            return -1;
        }
    }
    reslist_fclose(nffp, reslist);
    return 0;
}
