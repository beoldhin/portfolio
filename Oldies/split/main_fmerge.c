#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libintl.h>
#include "/home/olli/src/incs/inc_current.h"
#include "/home/olli/src/algo/reslist/reslist.h"

/* Note: ELEMSIZE must be non-zero */
#define ELEMSIZE 65536


int main(int argc, char* argv[])
{
    size_t i;
    size_t j;
    FILE* fp;
    FILE* nffp;
    long fsize;
    char* fname = NULL;
    size_t npos;
    size_t elemw;
    size_t elemr;
    size_t cneeded;
    char* fremov = NULL;
    char* nfname;
    char* element;
    DCLIST reslist;
    const char fsmsg[] = "fseek failed";
    const char frmsg[] = "fread failed";
    const char fwmsg[] = "fwrite failed";
    const char nfmsg[] = "malloc failed (nfname)";
    const char insmsg[] = "Insert new removable media #%u\n"\
                          "Press ENTER to continue . . .\n\n";
    reslist_screate(&reslist);
    if ((argc<2) || (argc>3)) {
        fprintf(stderr, gettext("File merger v2.1 -- Copyright (C) 2002 Olli\n"));
        fprintf(stderr, gettext("Usage: fmerge [OPTION] <FILE>\n\n"));
        fprintf(stderr, gettext("    -r<path>  path|drive of removable media\n"));
        reslist_clear(&reslist);
        return -1;
    }
    for (i=1; i<argc; ++i) {
        if (argv[i][0] == '-') {
          switch (argv[i][1]) {
            case 'r':
              fremov = argv[i] + 2;
              break;
            default:
              fprintf(stderr, gettext("Error: Unknown option \"%s\"\n"), argv[i]+1);
              return -1;
          }
        } else
            fname = argv[i];
    }
    if (!fname) {
        fprintf(stderr, gettext("Error: Filename not set\n"));
        reslist_clear(&reslist);
        return -1;
    }
    if ((fp=reslist_fopen(fname,"rb",&reslist))) {
        fprintf(stderr, gettext("Error: File \"%s\" already exists\n"), fname);
        reslist_clear(&reslist);
        return -1;
    }
    if (!(fp=reslist_fopen(fname,"wb",&reslist))) {
        perror(gettext("fopen failed"));
        reslist_clear(&reslist);
        return -1;
    }
    if (!(element=reslist_malloc(ELEMSIZE,&reslist))) {
        perror(gettext("malloc failed (ELEMSIZE)"));
        reslist_clear(&reslist);
        return -1;
    }
    if (fremov) {
        npos = strlen(fremov) + strlen(fname) + 1;
        if (!(nfname=reslist_malloc(npos+3+1,&reslist))) {
            perror(gettext(nfmsg));
            reslist_clear(&reslist);
            return -1;
        }
        strcpy(nfname, fremov);
        strcat(nfname, fname);
    } else {
        npos = strlen(fname) + 1;
        if (!(nfname=reslist_malloc(npos+3+1,&reslist))) {
            perror(gettext(nfmsg));
            reslist_clear(&reslist);
            return -1;
        }
        strcpy(nfname, fname);
    }
    nfname[npos-1] = '.';
    for (i=0,cneeded=1;;++i,++cneeded) {
        if (fremov) {
            printf(gettext(insmsg), cneeded);
            getchar();
        }
        sprintf(nfname+npos, "%03X", i);
        if (!(nffp=reslist_fopen(nfname,"rb",&reslist)))
            break;
        if (fseek(nffp,0,SEEK_END)) {
            perror(gettext(fsmsg));
            reslist_clear(&reslist);
            return -1;
        }
        if ((fsize=ftell(nffp)) < 0) {
            perror(gettext("ftell failed"));
            reslist_clear(&reslist);
            return -1;
        }
        if (fseek(nffp,0,SEEK_SET)) {
            perror(gettext(fsmsg));
            reslist_clear(&reslist);
            return -1;
        }
        /* calculating how big elements per block... */
        elemw = fsize / ELEMSIZE;
        elemr = fsize % ELEMSIZE;
        for (j=0; j<elemw; ++j) {
            if (fread(element,1,ELEMSIZE,nffp) != ELEMSIZE) {
                perror(gettext(frmsg));
                reslist_clear(&reslist);
                return -1;
            }
            if (fwrite(element,1,ELEMSIZE,fp) != ELEMSIZE) {
                perror(gettext(fwmsg));
                reslist_clear(&reslist);
                return -1;
            }
        }
        if (elemr) {
            if (fread(element,1,elemr,nffp) != elemr) {
                perror(gettext(frmsg));
                reslist_clear(&reslist);
                return -1;
            }
            if (fwrite(element,1,elemr,fp) != elemr) {
                perror(gettext(fwmsg));
                reslist_clear(&reslist);
                return -1;
            }
        }
        reslist_fclose(nffp,&reslist);
    }
    reslist_clear(&reslist);
    return 0;
}
