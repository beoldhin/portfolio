#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <libintl.h>


int main(int argc, char* argv[])
{
    int ch;
    int rnum;
    int freq;
    time_t tt;
    if (argc != 2) {
        fprintf(stderr, gettext("File Destroyer v1.0 -- Copyright (C) 2003 Olli\n"));
        fprintf(stderr, gettext("Usage: ffuck <frequency>\n"));
        return -1;
    }
    time(&tt);
    srand(tt);
    freq = atoi(argv[1]);
    while ((ch=getchar()) != EOF)
    {
        rnum = rand() % freq;
        if (rnum == 0) {
            printf("%c", rand()%256);
        } else {
            printf("%c", ch);
        }
    }
    return 0;
}
