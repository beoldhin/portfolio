/* Copyright (C) 2001 Olli */
#include <stdio.h>
#include <string.h>

#define DAYDIGITS 2
#define MONTHDIGITS 2
#define YEARDIGITS 4
#define ALLDIGITS (DAYDIGITS+MONTHDIGITS+YEARDIGITS)

void create_leapmonth(int);

char daylist[12] = {31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

int main(void) {
    int i, j;
    int day = 1;
    int month = 1;
    int year = 0;
    char date[ALLDIGITS+1];
    create_leapmonth(year);
    while (year <= 3000) {
        
        sprintf(date, "%02d%02d%d", day, month, year);
        
        i = 0;
        j = strlen(date) - 1;
        while (j >= 0) {
            if (date[i] != date[j])
                break;
            i++;
            j--;
        }
        if (j < 0) {
            printf("%02d.%02d.%d\n", day, month, year);
        }
        
        if (day < daylist[month-1]) {
            day++;
        } else {
            day = 1;
            if (month < 12) {
                month++;
            } else {
                month = 1;
                year++;
                create_leapmonth(year);
            }
        }
    }
    return 0;
}

void create_leapmonth(int year) {
    /* For long range accuracy, a formula suggested by the Vatican
       librarian Aloysius Giglio was adopted. It said that every fourth
       year is a leap year except for century years that are not
       divisible by 400. Thus 1700, 1800 and 1900 would not be leap
       years, but 2000 would be a leap year since 2000 is divisible by
       400. */
    if ((year%400==0) || ((year%4==0)&&(year%100!=0))) {
        daylist[1] = 29;
    } else {
        daylist[1] = 28;
    }
}
