#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "reversi.h"
#include "cokus.h"

void ShowAccepted(void);
int CalculateStrLen(char*);
void SetBoardLimits(void);
void CalculateAcceptWhite(void);
void CalculateAcceptBlack(void);
void MovePieceWhite(int);
void MovePieceBlack(int);

#define RUBBISH
/*
#define CPLEX
*/

#define FALSE 0
#define TRUE 1

#define UNUSED 0
#define WHITE 1
#define BLACK 2

#define MCalculateAccept(friend, enemy, direction, limit)\
j = lim[i].limit;\
if (j != i) {\
    k = i;\
    oppose = FALSE;\
    do {\
        k += direction;\
        if (brd[k] == UNUSED)\
            break;\
        if (brd[k] == enemy) {\
            oppose = TRUE;\
        } else if (oppose == TRUE) {\
            accept[acceptn] = i;\
            acceptn++;\
            goto skiprest;\
        } else\
            break;\
    } while (k != j);\
}

#define MMovePiece(friend, enemy, direction, limit, pcst1, pcst2)\
i = lim[to].limit;\
if (i != to) {\
    j = to;\
    oppose = FALSE;\
    do {\
        j += direction;\
        if (brd[j] == UNUSED)\
            break;\
        if (brd[j] == enemy) {\
            oppose = TRUE;\
        } else if (oppose == TRUE) {\
            do {\
                j -= direction;\
                brd[j] = friend;\
                pcst1++;\
                pcst2--;\
            } while (j != to);\
            break;\
        } else\
            break;\
    } while (j != i);\
}

struct LIMITS {
    int limnw;
    int limn;
    int limne;
    int limw;
    int lime;
    int limsw;
    int lims;
    int limse;
};

int pcswn;
int pcsbn;
int player;
int* brd;
struct LIMITS* lim;

int brdsize;
int brdmsize;

int northwest;
int north;
int northeast;
int west;
int east;
int southwest;
int south;
int southeast;

int oppose;
int acceptn;
int* accept;

FILE* outf;

char number1[10+1];
char number2[10+1];
char number3[10+1];


void Initialize(int size) {
    int i;
    
    
    #ifdef RUBBISH
    int maxim;
    time_t seconds;
    #endif
    #ifdef CPLEX
    int j, x;
    int rnd;
    #endif
    
    outf = fopen("r.out", "w+");
    
    brdsize = size;
    brdmsize = brdsize * brdsize;
    northwest = -brdsize - 1;
    north = -brdsize;
    northeast = -brdsize + 1;
    west = -1;
    east = +1;
    southwest = brdsize - 1;
    south = brdsize;
    southeast = brdsize + 1;
    
    brd = (int*)malloc(sizeof(int)*brdmsize);
    lim = (struct LIMITS*)malloc(sizeof(struct LIMITS)*brdmsize);
    accept = (int*)malloc(sizeof(int)*(brdmsize/2));
    
    /* Initialize everything to "no pieces" */
    for (i=0; i<brdmsize; i++) {
        brd[i] = UNUSED;
    }
    /* Place starting pieces to board */
    #ifndef RUBBISH
    brd[0] = WHITE;
    brd[1] = BLACK;
    brd[27] = BLACK;
    brd[28] = WHITE;
    brd[35] = WHITE;
    brd[36] = BLACK;
    #endif
    pcswn = 2;
    pcsbn = 2;
    player = BLACK;
    SetBoardLimits();
    
    #ifdef RUBBISH
    maxim = 0;
    time(&seconds);
    seedMT((seconds%UINT_MAX)+1);
    while (1) {
        pcswn = randomMT() % ((brdmsize)+1);
        pcsbn = randomMT() % ((brdmsize-pcswn)+1);
        for (i=0; i<brdmsize; i++) {
            brd[i] = UNUSED;
        }
        #ifdef CPLEX
        for (i=0; i<pcswn; i++) {
            rnd = randomMT() % (brdmsize-i);
            j = 0;
            while (rnd > 0) {
                if (brd[j] == UNUSED) {
                    rnd--;
                }
                j++;
            }
            brd[j] = WHITE;
        }
        for (x=i,i=0; i<pcsbn; i++,x++) {
            rnd = randomMT() % (brdmsize-x);
            j = 0;
            while (rnd > 0) {
                if (brd[j] == UNUSED) {
                    rnd--;
                }
                j++;
            }
            brd[j] = BLACK;
        }
        #endif
        #ifndef CPLEX
        for (i=0; i<pcswn; i++) {
            brd[randomMT()%brdmsize] = WHITE;
        }
        for (i=0; i<pcsbn; i++) {
            brd[randomMT()%brdmsize] = BLACK;
        }
        #endif
        
        CalculateAcceptWhite();
        if (acceptn > maxim) {
            ShowAccepted();
            fflush(outf);
            maxim = acceptn;
        }
    }
    #endif
    #ifndef RUBBISH
    CalculateAcceptWhite();
    ShowAccepted();
    MovePieceWhite(19);
    CalculateAcceptWhite();
    ShowAccepted();
    #endif
    
    
    
    
    free(brd);
    free(lim);
    free(accept);
    fclose(outf);
}

void ShowAccepted(void) {
    int i, j;
    int l1, l2, l3;
    int whites = 0;
    int blacks = 0;
    for (i=0; i<brdmsize; i++) {
        if (brd[i] == WHITE) {
            whites++;
        } else if (brd[i] == BLACK) {
            blacks++;
        }
    }
    sprintf(number1, "%d", whites);
    sprintf(number2, "%d", blacks);
    sprintf(number3, "%d", acceptn);
    l1 = CalculateStrLen(number1);
    l2 = CalculateStrLen(number2);
    l3 = CalculateStrLen(number3);
    for (i=0; i<(brdsize*3)-l1-l2-l3-2; i++) {
        fprintf(outf, "=");
    }
    fprintf(outf, "%s=%s=%s\n", number1, number2, number3);
    for (i=0; i<brdmsize; i++) {
        for (j=0; j<acceptn; j++) {
            if (accept[j] == i) {
                fprintf(outf, "-%d-", brd[i]);
                break;
            }
        }
        if (j == acceptn) {
            if (brd[i] == UNUSED)
                fprintf(outf, " %d ", brd[i]);
            else
                fprintf(outf, "[%d]", brd[i]);
        }
        if ((i+1)%brdsize == 0)
            fprintf(outf, "\n");
    }
    fprintf(outf, "\n");
}

int CalculateStrLen(char* str) {
    char* old;
    old = str;
    while (*str != '\0') {
        str++;
    }
    return str-old;
}

void SetBoardLimits(void) {
    int i, j, k;
    int x, y;
    for (y=0; y<brdsize; y++) {
        for (x=0; x<brdsize; x++) {
            i = (y*brdsize) + x;
            /* Checking northwest */
            j = x - 1;
            k = y - 1;
            while ((j>=0) && (k>=0)) {
                j--;
                k--;
            }
            lim[i].limnw = ((k+1)*brdsize) + (j+1);
            /* Checking north */
            j = x;
            k = y - 1;
            while (k >= 0) {
                k--;
            }
            lim[i].limn = ((k+1)*brdsize) + j;
            /* Checking northeast */
            j = x + 1;
            k = y - 1;
            while ((j<brdsize) && (k>=0)) {
                j++;
                k--;
            }
            lim[i].limne = ((k+1)*brdsize) + (j-1);
            /* Checking west */
            j = x - 1;
            k = y;
            while (j >= 0) {
                j--;
            }
            lim[i].limw = (k*brdsize) + (j+1);
            /* Checking east */
            j = x + 1;
            k = y;
            while (j < brdsize) {
                j++;
            }
            lim[i].lime = (k*brdsize) + (j-1);
            /* Checking southwest */
            j = x - 1;
            k = y + 1;
            while ((j>=0) && (k<brdsize)) {
                j--;
                k++;
            }
            lim[i].limsw = ((k-1)*brdsize) + (j+1);
            /* Checking south */
            j = x;
            k = y + 1;
            while (k < brdsize) {
                k++;
            }
            lim[i].lims = ((y-1)*brdsize) + j;
            /* Checking southeast */
            j = x + 1;
            k = y + 1;
            while ((j<brdsize) && (k<brdsize)) {
                j++;
                k++;
            }
            lim[i].limse = ((k-1)*brdsize) + (j-1);
        }
    }
}

void CalculateAcceptWhite(void) {
    int i, j, k;
    acceptn = 0;
    for (i=0; i<brdmsize; i++) {
        if (brd[i] == UNUSED) {
            MCalculateAccept(WHITE, BLACK, northwest, limnw);
            MCalculateAccept(WHITE, BLACK, north, limn);
            MCalculateAccept(WHITE, BLACK, northeast, limne);
            MCalculateAccept(WHITE, BLACK, west, limw);
            MCalculateAccept(WHITE, BLACK, east, lime);
            MCalculateAccept(WHITE, BLACK, southwest, limsw);
            MCalculateAccept(WHITE, BLACK, south, lims);
            MCalculateAccept(WHITE, BLACK, southeast, limse);
        }
        skiprest:;
    }
}

void CalculateAcceptBlack(void) {
    int i, j, k;
    acceptn = 0;
    for (i=0; i<brdmsize; i++) {
        if (brd[i] == UNUSED) {
            MCalculateAccept(BLACK, WHITE, northwest, limnw);
            MCalculateAccept(BLACK, WHITE, north, limn);
            MCalculateAccept(BLACK, WHITE, northeast, limne);
            MCalculateAccept(BLACK, WHITE, west, limw);
            MCalculateAccept(BLACK, WHITE, east, lime);
            MCalculateAccept(BLACK, WHITE, southwest, limsw);
            MCalculateAccept(BLACK, WHITE, south, lims);
            MCalculateAccept(BLACK, WHITE, southeast, limse);
        }
        skiprest:;
    }
}

void MovePieceWhite(int to) {
    int i, j;
    MMovePiece(WHITE, BLACK, northwest, limnw, pcswn, pcsbn);
    MMovePiece(WHITE, BLACK, north, limn, pcswn, pcsbn);
    MMovePiece(WHITE, BLACK, northeast, limne, pcswn, pcsbn);
    MMovePiece(WHITE, BLACK, west, limw, pcswn, pcsbn);
    MMovePiece(WHITE, BLACK, east, lime, pcswn, pcsbn);
    MMovePiece(WHITE, BLACK, southwest, limsw, pcswn, pcsbn);
    MMovePiece(WHITE, BLACK, south, lims, pcswn, pcsbn);
    MMovePiece(WHITE, BLACK, southeast, limse, pcswn, pcsbn);
    brd[to] = WHITE;
    player = BLACK;
    pcswn++;
}

void MovePieceBlack(int to) {
    int i, j;
    MMovePiece(BLACK, WHITE, northwest, limnw, pcsbn, pcswn);
    MMovePiece(BLACK, WHITE, north, limn, pcsbn, pcswn);
    MMovePiece(BLACK, WHITE, northeast, limne, pcsbn, pcswn);
    MMovePiece(BLACK, WHITE, west, limw, pcsbn, pcswn);
    MMovePiece(BLACK, WHITE, east, lime, pcsbn, pcswn);
    MMovePiece(BLACK, WHITE, southwest, limsw, pcsbn, pcswn);
    MMovePiece(BLACK, WHITE, south, lims, pcsbn, pcswn);
    MMovePiece(BLACK, WHITE, southeast, limse, pcsbn, pcswn);
    brd[to] = BLACK;
    player = WHITE;
    pcsbn++;
}
