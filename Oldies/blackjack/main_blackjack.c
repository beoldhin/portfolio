/*
 TODO:
 Because of different rules, the dealer can add new decks to his repository even if it is not empty:
 - Change defines to variables
 - Change dealer_table to dynamic
 - Add a function add_deck_to_dealer that can be used even if dealer_table is empty.
   This function copies the new deck to empty slots of dealer_table and allocates more memory for dealer_table if they don't fit to the old.
   (dealer_table_init becomes handy in this case).
 - A new parameter to remove_card_from_dealer is good; it defines how many full decks the dealer add to his repository when it comes empty.
*/
#include <time.h>
#include <stdio.h>
#include <string.h>
#include "../algo/randgen/mtcokus.h"

#define BOOL                 int
#define FALSE                  0
#define TRUE                   1

#define DEAL_LIMIT            21
#define NUMBER_OF_SUITS        4
#define NUMBER_OF_SUIT_CARDS  13
#define CARDS_IN_DECK         52
#define NUMBER_OF_DECKS        1
#define DEALER_CARDS (CARDS_IN_DECK*NUMBER_OF_DECKS)

void initialize_dealer_table(void);
BOOL estimate_action(int);
void remove_card_from_dealer(int);

int dealer_table_init[DEALER_CARDS];
int dealer_table[DEALER_CARDS];
int cards_in_dealer;


int main(void) {
    int i;
    int row = 0;
    int randnum;
    time_t thistime;

    unsigned int total;
    unsigned int right_guess;
    unsigned int wrong_guess;
    unsigned int non_optimal;
    
    seedMT(time(&thistime));
    initialize_dealer_table();
    for (i=0; i<DEALER_CARDS; i++) {
        dealer_table[i] = dealer_table_init[i];
    }
    right_guess = 0;
    wrong_guess = 0;
    non_optimal = 0;
    for (i=0; i<10000000; i++) {
        randnum = (randomMT()%NUMBER_OF_SUIT_CARDS) + 1;
        if (estimate_action(row) == TRUE) {
            remove_card_from_dealer(randnum);
            if (randnum == 1) {
                if (row+NUMBER_OF_SUIT_CARDS+1 == DEAL_LIMIT)
                    row += NUMBER_OF_SUIT_CARDS+1;
                else
                    row++;
                continue;
            }
            row += randnum;
        } else {
            if (row > DEAL_LIMIT) {
                wrong_guess++;
            } else {
                if (row+randnum <= DEAL_LIMIT) {
                    non_optimal++;
                } else {
                    right_guess++;
                }
            }
            row = 0;
        }
    }
    
    total = right_guess + wrong_guess + non_optimal;
    printf("Right guess: %7u (%u%%)\n", right_guess, (unsigned int)(((float)right_guess/(float)total)*100.0));
    printf("Wrong guess: %7u (%u%%)\n", wrong_guess, (unsigned int)(((float)wrong_guess/(float)total)*100.0));
    printf("Non-optimal: %7u (%u%%)\n", non_optimal, (unsigned int)(((float)non_optimal/(float)total)*100.0));
    printf("--------------------\n");
    printf("Total:       %7u\n", total);
    
    return 0;
}

void initialize_dealer_table(void) {
    int i;
    int j;
    int pos = 0;
    int value = 1;
    for (j=0; j<NUMBER_OF_SUIT_CARDS; j++) {
        for (i=0; i<NUMBER_OF_SUITS*NUMBER_OF_DECKS; i++) {
            dealer_table_init[pos] = value;
            pos++;
        }
        value++;
    }
    cards_in_dealer = DEALER_CARDS;
}

BOOL estimate_action(int row) {
    int i;
    int counter = 0;
    int situation = DEAL_LIMIT - row;
    for (i=0; i<DEALER_CARDS; i++) {
        if ((dealer_table[i]>=1) && (dealer_table[i]<=situation)) {
            counter++;
        }
    }
    if (counter > cards_in_dealer/2)
        return TRUE;
    return FALSE;
}

void remove_card_from_dealer(int value) {
    int i;
    if (cards_in_dealer == 1) {
        for (i=0; i<DEALER_CARDS; i++) {
            dealer_table[i] = dealer_table_init[i];
        }
        cards_in_dealer = DEALER_CARDS;
        return;
    }
    for (i=0; i<DEALER_CARDS; i++) {
        if (dealer_table[i] == value) {
            dealer_table[i] = 0;
            cards_in_dealer--;
            return;
        }
    }
}
