'Static' Memory Management & Memory Pools
-----------------------------------------

Memory Pools is actually a linked list, but instaed of having to
manually allocate memory for the satellite data in its strictest
sense, the satellite data is placed directly after the node data
(this method divides the need for malloc/free calls by 2; node data
is actually the satellite data or vice-versa).

Memory Pools also has a pool allocator that does the actual memory
management and has the added benefit of avoiding memory fragmentation.
Memory fragmentation usually occurs when small blocks of memory are
malloc'ed/freed in 'brute force' without using pools.

Memory Pools is easiest to implement when the allocator always 'knows'
how many bytes of data it has to allocate. Let's call this allocation
method 'static'. It can be an Abstract Data Type, but the data to be
stored must not change in length.

A 'dynamic' (e.g. when storing strings) method is more complicated due
to border-of-pool issues (it's impossible/unwise to align the data to
be stored evenly to the block size; wasted space will appear on block
boundaries). A special handling is also needed if the memory area to
be stored is larger than the pool. So for dynamic storing it is
simplest to leave out the pools and only use linked lists with
integrated satellite data.

It is best to define the size of the memory pool in how many allocs/
frees it saves. This becomes ('N' is the number of saves):

poolsize = sizeof(poolinfo) + (N * sizeof(block)), which becomes:
poolsize = sizeof(poolinfo) + (N * ( sizeof(node) + sizeof(sat) ))

An initialization function is needed with "sat" as an argument to
determine the "poolsize".

In ASCII graphics the data structure looks like this:

   poolinfo             block             block
       |                  |                 |
 /--------------\/----------------\/----------------\
|[<cpool,blocks>][<prev,next><sat>][<prev,next><sat>]...             |
\--------------------------------------------------------------------/
                                   |
                                  pool

Explanation:

"cpool"   pointer to the current memory pool area
"blocks"  number of blocks currently in the current memory pool
"prev"    pointer to the previous block in the current pool
"next"    pointer to the next block in the current pool
"sat"     satellite data of static length

Actually, the "cpool" can be calculated from "blocks" because every
block is in the same memory area. It is better to use "blockn" in
every block's node data to get rid of the possible complex cases for
"poolinfo" handling. So eventually this becomes:

           block                   block
             |                       |
 /-----------------------\/-----------------------\
|[<prev,next,blockn><sat>][<prev,next,blockn><sat>]...               |
\--------------------------------------------------------------------/
                                   |
                                  pool

To minimize the use of memory, other functions could be written for
all possible cases of blockn; unsigned char, unsigned short, and
unsigned int. It is possible to determine the variable type of blockn
directly after knowing 'N'. The varible 'poolsize' can be calculated
after knowing the size of 'node', as follows (note that the limits
are platform/compiler dependent):

1) Let 'N' be the number of the saves (constant, "ifdef")
2) If N <= unsigned_char_limit, blockn type is "unsigned char",
     else
   If N <= unsigned_short_limit, blockn type is "unsigned short",
     else
   If N <= unsigned_int_limit, blockn type is "unsigned int"
3) Variable blockn makes 'node' different.
4) poolsize = sizeof(poolinfo) + (N * ( sizeof(node) + sizeof(sat) ))

When deallocating a block, a number is saved to 'blockn' that is not
a valid number (e.g. less than 0). Deallocation is performed only
when reaching a state when deallocating the last block in the pool.
The only problem left is to know how many blocks are allocated in a
given pool.

The Final Summary
-----------------

To sum all pros and cons, we reach to the final data type:

Complex 'poolinfo' handling must be done. Actually, it is not at all
so complex. The data type is explained below:

       poolinfo                block
           |                     |
 /------------------\/-----------------------\
|[<prev,next,blockn>][<prev,next,blockn><sat>]...                    |
\--------------------------------------------------------------------/
                                   |
                                  pool

The poolinfo has the same node as the blocks have, but the poolinfo
has a negative blockn. It tells how many blocks the pool contains.
To get its real value, it must be negated in order to make it
positive. To increase it, it must be decreased. To decrease it, it
must be increased.

The poolinfo can accessed from a given block by:
temp = blockn - 1
blockinfop = blockp - (temp * sizeof(block)) - sizeof(poolinfo)

In the above formula "blockn" is the blockn of the block, NOT the
blockn of the poolinfo.

The variable cpool is not needed because it can be generated from
the blockn information of the blocks; they are all on the same
memory area.

To reduce the "temp = blockn - 1", the blockn could initially
contain a number starting from 0 (0 for the first block, 1 for the
second, etc.).

The final format for poolsize is:

poolsize = sizeof(node) + (N * ( sizeof(node) + sizeof(sat) ))

A major note
------------

The variables "prev" and "next" can be removed from block info because
blocks inside a pool always belong to the same memory area - the pool.
The the very last - optimized and simplified - data type for the pool
is the following:

       poolinfo                          block
          |                                |
 /------------------\               /-------------\
|[<prev,next,blocks>][<sat><blockn>][<sat><blockn>][<sat><blockn>]...  |
\----------------------------------------------------------------------/
                                  |
                                pool

See "mempool_reduced.txt" for information about only of this reduced
version.

Olli
