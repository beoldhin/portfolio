<html>

<head>

<title>Evaluating Expressions, ComSci 221, U. Chicago</title>

<center>
<img align=bottom src="/images/logos/cs-dept-logo.gif" alt="[CS Dept logo]">
</center>

<h2>Evaluating Expressions</h2>

<p>

<a href="contents.html">
  Lecture Notes
</a>
for
<a href="../contents.html">
  Com Sci 221,
</a>
Programming Languages

<p>

<!-- hhmts start -->
Last modified: Fri Jan 27 15:17:20 1995
<!-- hhmts end -->

</head>

<hr>

<body>

<a href="/~odonnell/CS221_comments/lectnote5eval.html">
  Comments on these notes
</a>

<br><br>

<img align=top
     src="http://cs-www.uchicago.edu/~odonnell/OData/Images/bell.gif"
     alt=""
>
Continue
<a href="/~odonnell/CS221_comments/lecture20jan.html" name="20jan">
  Friday 20 January
</a>

<h3>What is an expression?</h3>

<p>

In conventional procedural programming languages, such as C and
Pascal, the conditions in <b>while</b>, <b>for</b>, and <b>if</b>
commands, and the descriptions of values on the right-hand sides of
assignment commands, are generally thought of as
<em>expressions</em>. In functional programming languages, such as
Scheme and ML, the entire program is one large expression. In Prolog,
the arguments to relations are expressions, but they are typically
quite small, and they are not intended to be evaluated in the same way
as expressions in most other languages. For our purposes, expressions
are those components of a program that are usefully understood in
terms of their evaluation to a single value. As with essentially all
technical terms in this course, the word "expression" is defined in
particular languages in ways that are not exactly the same as our use
in class. For example, in official descriptions of C, assignment
commands are allowed to occur in expressions.

<p>

In principle, it is possible to regard an entire program in
<em>any</em> language as one large expression, whose value is some
description of the total output of the program. But, commands
involving the assignment of values to variables are usually thought of
as modifying the state of a computation, rather than evaluating to a
value. For the most part, we will consider as "expressions" only
components of a program whose primary (and preferably sole) function
is to evaluate to a value in some standard datatype of the programming
language, such as <i>integer, boolean, array,</i> etc.

<p>

To understand expression evaluation, it is better to regard each
expression as a <a href="syntax.html#syntree"><em>syntax
tree</em></a>, rather than a sequence of symbols. The issues of
precedence, associativity, etc., that are so important in parsing can
be dealt with entirely in the translation of a sequence of symbols
into a syntax tree. Combining parsing issues with evaluation is a
common methodological error, and it leads to a lot of confusion. When
we are discussing expression evaluation, it is <em>crucial</em> that
you mentally translate all textual expressions into tree form. I
repeatedly see students make errors by evaluating a portion of the
textual presentation of an expression that is <em>not</em> a sensible
component of the syntax tree. You are so familiar with arithmetic that
you would probably never make the mistake of evaluating the <i>2+3</i>
in <i>2+3*4</i> to <i>5</i>, yielding <i>5*4</i> and finally,
<i>20</i>. <strong>But</strong>, as soon as you consider expressions
with less familiar data types and operators, it is <em>very</em> easy
to make comparable errors.

<br><br><br>

<img align=top
     src="http://cs-www.uchicago.edu/~odonnell/OData/Images/bell.gif"
     alt=""
>
End
<a href="/~odonnell/CS221_comments/lecture20jan.html">
  Friday 20 January
</a>

<br>

<img src="http://cs-www.uchicago.edu/~odonnell/OData/Images/snip.gif"
     alt="%&lt;----------------------------------------------"
>

<br>

<img align=top
     src="http://cs-www.uchicago.edu/~odonnell/OData/Images/bell.gif"
     alt=""
>
Begin
<a href="/~odonnell/CS221_comments/lecture23jan.html" name="23jan">
  Monday 23 January
</a>

<h3>Expression evaluation as tree rewriting</h3>

<p>

Expression evaluation is simply the computation of the value of an
expression. Maybe that sentence is too obvious to be worth
writing. The power of expression evaluation <em>as a methodological
concept</em> in computing, is that for most purposes, an expression
may be thought of as equal to its value. So, an expression may be
replaced by its value without changing the meaning of other things
that contain that expression.

<p>

Expressions contain other expressions (called <em>subexpressions</em>)
as components. Based on the principle above, we may replace any
subexpression with its value, and not change the value of the whole
expression. So, expression evaluation may proceed by replacing
subexpressions with values, until we find a way to replace the whole
expression by its values. This is a sort of <em>tree rewriting</em>
(since the expressions are all presented as trees). Many programming
languages violate the principle of replacability of subexpressions by
their values (e.g., a C expression containing an assignment command
violates this principle). Nonetheless, the best way to understand
expression evaluation is to first conceive it as pure tree rewriting,
and then consider variations from that relatively simple concept. Many
aspects of programming languages are best understood as close
approximations to something simple and elegant, with the deviations
brought in only as necessary to find the right results.

<h4>Simple arithmetic evaluation</h4>

<p>

The simplest sorts of tree rewriting rules replace an operator applied
to completely evaluated arguments by the final value. Almost all
arithmetic and boolean expressions in the style of C and Pascal may be
evaluated by such rules. For example, the rule for evaluating
<i>2+3</i> is shown in <a href="fig-addrule.ps>Figure 9</a>. (I'll give
textual versions of all the evaluation rules inline, but the separate
color PostScript figures are much better. When you look at the textual
versions, always think syntax trees in your mind.)

<hr>

<blockquote><pre>
2+3 ==> 5
</pre></blockquote>

<hr>

There is a huge, or even infinite, collection of similar rules for the
other possible integer additions, as well as the subtractions,
multiplications, and divisions. We all know that these rules are not
listed literally in a computer---rather each instance of the rules is
computed by the central processor as needed. Nonetheless, it is
helpful to think of the computation as applying the rules implicitly.

<h4>Conditional evaluation</h4>

<p>

Most evaluation rules have the simple form of the arithmetic
rules. But, there is no fundamental reason that the right-hand side of
an evaluation rule must be a single value. If it is O.K. to replace an
expression by its value, then it is also O.K. to replace it by another
expression with the same value. Of course, such a step is only useful
if the replacement leads toward the eventual computation of a final
value. For example, the usual rules for evaluating a <em>conditional
expression</em> are given in <a href="fig-ifrules.ps">Figure 10</a>.

<hr>

<blockquote><pre>
<b>if</b> T <b>then</b> <i>&lt;alpha&gt;</i> <b>else</b> <i>&lt;beta&gt;</i> ==> <i>&lt;alpha&gt;</i>
<b>if</b> F <b>then</b> <i>&lt;alpha&gt;</i> <b>else</b> <i>&lt;beta&gt;</i> ==> <i>&lt;beta&gt;</i>
</pre></blockquote>

<hr>

We most often thing of conditional as a control-flow construct in
Pascal and C, but it is also an operator in expressions in many
languages, including Scheme and ML. The rules for conditionals are
more sophisticated than those for the arithmetic operations, because
they involve <em>metavariables</em> standing for arbitrary
subexpressions. I have presented these metavariables as Greek letters
to emphasize the fact that they are not actual symbols in an
expression.

<p>

There is essentially only one way to apply the simple arithmetic rules
for <i>+, *</i>, etc. We must replace subexpressions by values
starting from the leaves of an expression, and work our way up to the
root. Sure, we can choose to go left-to-right, right-to-left, or some
other order across the leaves, but that choice is not particularly
crucial (it may affect the intermediate storage required by expression
evaluation, so some compilers may try to optimize it). Evaluation from
the leaves up is called <em>innermost</em> evaluation, since in a
parenthesized presentation of an expression it works within the
innermost parentheses. The conditional rules, however, may apply
arbitrarily far up a tree, since the subexpressions <i>&lt;alpha&gt;</i> and
<i>&lt;beta&gt;</i> may be arbitrarily large. The order in which we apply the
arithmetic and conditional rules does not affect the final value, but
it may affect the number of steps required to compute that value very
significantly. If we evaluate the <b>then</b> branch of a conditional,
only to find that the <b>else</b> branch is selected, we may waste
huge amounts of time. Later, when we introduce rules that behave
iteratively or recursively, we may even waste an infinite amount of
time, and fail to produce the final value at all. It is intuitively
clear (and correct, too) that, in the presence of arithmetic rules and
conditionals, when we see a conditional we should evaluate its
leftmost argument first to T or to F, then apply one of the
conditional rules to prune the branch not taken, and only then should
we evaluate the remaining branch. This order of evaluation is called
<em>leftmost-outermost</em>. We may still evaluate the purely
arithmetic portions of an expression innermost, while using the
leftmost-outermost strategy on conditionals.

<p>

At first, essentially all expression evaluation in programming
languages involved simple rules, such as the arithmetic rules but
involving other operators and data types as well, plus the conditional
rules. The simple rules were always applied innermost, and the
conditional rule was a special exception calling for
leftmost-outermost evaluation. Boolean operations were evaluated using
the obvious analogues to the arithmetic rules, as shown in
<a href="fig-orandrules.ps">Figure 11</a>.

<hr>

<blockquote><pre>
T <b>or</b> T ==> T
T <b>or</b> F ==> T
F <b>or</b> T ==> T
F <b>or</b> F ==> F

T <b>and</b> T ==> T
T <b>and</b> F ==> F
F <b>and</b> T ==> F
F <b>and</b> F ==> F
</pre></blockquote>

<hr>

Then, some people noticed that conditional evaluation might be
valuable in other contexts, leading to the <em>conditional or</em> and
<em>conditional and</em> rules of <a
href="fig-condorandrules.ps">Figure 12</a>.

<hr>

<blockquote><pre>
T <b>or</b> <i>&lt;alpha&gt;</i> ==> T
F <b>or</b> <i>&lt;alpha&gt;</i> ==> <i>&lt;alpha&gt;</i>

T <b>and</b> <i>&lt;alpha&gt;</i> ==> <i>&lt;alpha&gt;</i>
F <b>and</b> <i>&lt;alpha&gt;</i> ==> F
</pre></blockquote>

<hr>

As with the conditional <b>if</b> rules, the conditional <b>and</b> and
<b>or</b> rules must be applied leftmost-outermost in order to have
extra power: applied innermost they are equivalent to the simple
<b>and</b> and <b>or</b> rules. C has both the simple <b>or</b> and
<b>and</b>, and the conditional versions. Having realized that
operators, such as <b>or</b> and <b>and</b>, might evaluate to sensible
final values even though some of their arguments were not evaluated (and
might conceivably be unevaluable), people started calling the
conventional operators <em>strict</em>, and the other ones such as
conditionals <em>nonstrict</em>.

<h4>Lazy evaluation</h4>

<p>

Conditional evaluation merely scratches the surface of imaginative uses
of tree rewriting rules. The LISP/Scheme operators <i>cons, car,</i> and
<i>cdr</i> were originally conceived as simple arithmetic-like operators
operating over the data type of <em>nested lists</em>, or equivalently
of <em>S-expressions</em>. Having seen the rules for conditional,
several people (including me) realized that these operators would be
more powerful if evaluated by the rules in
<a href="fig-lazyconsrules.ps">Figure 13</a>.

<hr>

<blockquote><pre>
<i>car</i>(<i>cons</i>(<i>&lt;alpha&gt;,&lt;beta&gt;</i>) ==> <i>&lt;alpha&gt;</i>
<i>cdr</i>(<i>cons</i>(<i>&lt;alpha&gt;,&lt;beta&gt;</i>) ==> <i>&lt;beta&gt;</i>
</pre></blockquote>

<hr>

As before, innermost use of these rules gives no additional power, but
applying them outermost gives us <em>demand-driven</em> evaluation of
lists, which is popularly called "lazy evaluation," although I don't
regard avoiding absolutely wasted work as lazy. Because functions are
conventionally thought of as subroutines that are called to compute
values from arguments, lazy evaluation is also referred to as "call by
need" (understood as an alternative to <em>call by value</em>, <em>call
by name</em>, etc., which we study later). This is somewhat misleading,
since outermost evaluation changes the sorts of structures
<em>returned</em> by a function more than the way that arguments are
passed to it. To contrast with lazy evaluation, innermost evaluation is
sometimes called "eager."

<p>

Lazy evaluation allows very large, and even infinite, lists to be
defined as if they were intermediate steps in a computation. But, only
the elements of lists that affect the final output are actually
computed. Scheme has <em>streams</em>, which are just lazily evaluated
lists, but it uses new operators instead of <i>car, cdr, cons.</i> The
stream operations in Scheme are the result of a language that never
conceived of lazy evaluation (LISP) being stretched to allow it. I would
like to see languages re-engineered to be lazy from the bottom
up. <em>Haskell</em> is an experimental language that is founded on lazy
evaluation, although it is still not radical enough for my taste.

<p>

If you look real closely, you will see a formal analogy between the two
rules for <tt>T, F,</tt> <b>if</b> and the two rules for <i>car, cdr,
cons</i>. A mathematician from Mars, who knew the basic notation of
mathematics, but not the conventional meanings of the words if, etc.,
might say that the two pairs of rules have almost the same structure. In
both cases, there are two unknowns on the left, and one of them is
selected on the right, based on the identity of a single symbol. In the
<b>if</b> rules, that symbol is the constant symbol <tt>T</tt> or
<tt>F</tt> in the first argument position to the <b>if</b>, in the
<i>cons</i> rules it is the unary function symbol <i>car</i> or
<i>cdr</i> that is applied to the <i>cons</i>. That difference hardly
looks profound from a formal point of view. So, it is particularly
ironic that computer scientists went for so many years assuming that
<b>if</b> and <i>cons</i> required radically different sorts of
implementations.

<br><br><br>

<img align=top
     src="http://cs-www.uchicago.edu/~odonnell/OData/Images/bell.gif"
     alt=""
>
End
<a href="/~odonnell/CS221_comments/lecture23jan.html">
  Monday 23 January
</a>

<br>

<img src="http://cs-www.uchicago.edu/~odonnell/OData/Images/snip.gif"
     alt="%&lt;----------------------------------------------"

<br>

<img align=top
     src="http://cs-www.uchicago.edu/~odonnell/OData/Images/bell.gif"
     alt=""
>
Begin
<a href="/~odonnell/CS221_comments/lecture25jan.html" name="25jan">
  Wednesday 25 January
</a>

<h4>Pushing the envelope with evaluation rules</h4>

<p>

Ph.D. dissertations have been written on the powers of tree rewriting
rules for expression evaluation (e.g., my dissertation in 1976), and
several more dissertations remain to be written on the topic. The
examples in this section are just intended to give a sense of the range
of possibilities.

<p>

First, programmer-defined recursive functions may be understood as
additional evaluation rules added by the programmer. For example the
conventional recursive definition of the Fibonnacci function
corresponds to the rule

<blockquote><pre>
<i>fib</i>(n) ==> <b>if</b> <i>n</i>=0 <b>or</b> <i>n</i>=1 <b>then</b> 1
              <b>else</b> <i>fib</i>(n-1) + <i>fib</i>(n-2) <b>fi</b>
</pre></blockquote>

The rules corresponding to programmer-defined functions are usually
quite simple, but because they are allowed to be recursive, they
introduce the possibility of infinite computation, and make the order of
evaluation particularly important. Most of the complexity of evaluation
rules comes from the patterns in their left-hand
sides. Programmer-defined functions usually have simple left-hand sides,
but they do introduce the minor complication of repetitions on the
right-hand sides, as in

<blockquote><pre>
<i>square</i>(<i>&lt;alpha&gt;</i>) ==> <i>&lt;alpha&gt;</i> * <i>&lt;alpha&gt;</i>
</pre></blockquote>

For efficiency, when applying outermost evaluation to such a rule, it is
crucial <em>not</em> to make a new copy of <i>&lt;alpha&gt;</i>, but
merely to create two pointers to a shared single copy. Many people use
the phrase "lazy evaluation" to mean outermost evaluation with sharing,
although there is no official standard definition.

<p>

Conditional evaluation can be applied much more broadly than it has
been. For example,

<hr>

<blockquote><pre>
0*<i>&lt;alpha&gt;</i> ==> 0
1*<i>&lt;alpha&gt;</i> ==> <i>&lt;alpha&gt;</i>
</pre></blockquote>

<hr>

<p>

Many of the operators that benefit from the power of conditional
evaluation can be taken much farther. For example, the <em>parallel</em>
<b>or</b> rules (and the analogous parallel <b>and</b>) are even more
powerful than the conditional ones:

<hr>

<blockquote><pre>
T <b>or</b> <i>&lt;alpha&gt;</i> ==> T
<i>&lt;alpha&gt;</i> <b>or</b> T ==> T
F <b>or</b> F ==> F
</pre></blockquote>

<hr>

Notice that there is no sensible way to choose the left or right
argument to <b>or</b> for first evaluation---rather we must evaluate
both of them in parallel. There is no way to avoid some wasted work when
one of the first two rules applies. Also notice that it is the two rules
involving <tt>T</tt> that cause the complication (and the power)---the
third <tt>F</tt> rule is just like the simple arithmetic rules.

<p>

If <b>or</b> and <b>and</b> can be parallel, why not <b>if</b>?

<hr>

<blockquote><pre>
<b>if</b> <i>&lt;gamma&gt;</i> <b>then</b> <i>&lt;alpha&gt;</i> <b>else</b> <i>&lt;alpha&gt;</i> ==> <i>&lt;alpha&gt;</i>
<b>if</b> T <b>then</b> <i>&lt;alpha&gt;</i> <b>else</b> <i>&lt;beta&gt;</i> ==> <i>&lt;alpha&gt;</i>
<b>if</b> F <b>then</b> <i>&lt;alpha&gt;</i> <b>else</b> <i>&lt;beta&gt;</i> ==> <i>&lt;beta&gt;</i>
</pre></blockquote>

<hr>

These rules require parallel evaluation of the condition and both
branches of an <b>if</b>, plus an equality test between the two
branches. The equality test, which results from the repetition of
<i>&lt;alpha&gt;</i> on the <em>left-hand</em> side of the first rule,
is especially tricky, and its theoretical properties might fill a
dissertation by themselves. The pioneering lazy language <em>Daisy,</em>
from Indiana University, had a kind of parallel <b>if</b> with the
ingenious use of <em>distributive</em> rules:

<hr>

<blockquote><pre>
<b>if</b> <i>&lt;gamma&gt;</i>
      <b>then</b> <i>cons</i>(<i>&lt;alpha&gt;1</i>,<i>&lt;beta&gt;1</i>)
      <b>else</b> <i>cons</i>(<i>&lt;alpha&gt;2</i>,<i>&lt;beta&gt;2</i>)

                  ==>

<i>cons</i>(<b>if</b> T <b>then</b> <i>&lt;alpha&gt;1</i> <b>else</b> <i>&ltalpha&gt;2</i>,
    <b>if</b> T <b>then</b> <i>&lt;beta&gt;1</i> <b>else</b> <i>&lt;beta&gt;2</i>)
</pre></blockquote>

<hr>

This brilliantly clever rule allows the equality test to be performed
only when at least one of the branches has been reduced to an atomic
symbol.

<p>

The possibility of implicit equality tests on the right-hand side
suggests several powerful rules, such as

<hr>

<blockquote><pre>
<i>&lt;alpha&gt;</i> = <i>&lt;alpha&gt;</i> ==> T

<i>&lt;alpha&gt;</i> - <i>&lt;alpha&gt;</i> ==> 0

<i>&lt;alpha&gt;</i> ? <i>&lt;alpha&gt;</i> ==> <i>&lt;alpha&gt;</i>
</pre></blockquote>

<hr>

Strangely, the third one is somewhat easier to deal with, because the
<i>&lt;alpha&gt;</i> has to be evaluated in any case. But, these rules
have already taken us beyond the realm of even experimental
implementations. When we look at the difference between FORTRAN and
Daisy, however, we should be very open to the possibility that even more
sophisticated evaluation rules will find practical application in the
future.

<h3>Implementing expression evaluation</h3>

<p>

The main purpose of the tree rewriting rules above is to help us
understand evaluation. Most implementations of expression evaluation do
not apply tree rewriting directly (although one promising approach to
parallel evaluation does so---in this context it is called "graph
rewriting" to emphasize the importance of sharing of
subexpressions). Most evaluation is still bottom up, and it may be done
by a simple recursion on the expression in syntax tree form:

<hr>

<blockquote><pre>
<i>eval</i>(<i>T</i>):

      <b>if</b> <i>T</i> is a constant
         <b>then return</b>(<i>value</i>(<i>T</i>))
      <b>else if</b> <i>T</i> is a variable
         <b>then return</b>(<i>current-value</i>(<i>T</i>))
         <b>else</b> let <i>T</i>=<i>op</i>(<i>T1,...,Tn</i>)
              <i>v1</i> := <i>eval</i>(<i>T1</i>)

                  .
                  .
                  .

              <i>vn</i> := <i>eval</i>(<i>Tn</i>)
              <b>return</b>(<i>op</i>(<i>v1</i>, ..., <i>vn</i>))
      <b>fi</i>
</pre></blockquote>

<hr>

I have cheated a bit more than usual in my notation above. The use of
<i>op</i> in the final <b>return</b> isn't quite right. <i>op</i> is a
symbol, and in the final <b>return</b> I need to apply the subroutine
associated with that symbol. A legitimate way to write that is
<b>return</b>(<i>value</i>(<i>op</i>)(<i>v1</i>, ..., <i>vn</i>)), but
most people get confused by that notation. All of this can be done in
your favorite programming language, although the details may be quite
ugly looking in some languages.

<p>

The recursive evaluation above, with a <em>lot</em> of detailed
improvement and variation, is the basic idea behind expression
evaluation in programming language interpreters. For functional
languages, such as Scheme and ML, expression evaluation is all there is,
so the whole interpreter has a structure based on recursive evaluation.

<br><br><br>

<img align=top
     src="http://cs-www.uchicago.edu/~odonnell/OData/Images/bell.gif"
     alt=""
>
End
<a href="/~odonnell/CS221_comments/lecture25jan.html">
  Wednesday 25 January
</a>

<br>

<img src="http://cs-www.uchicago.edu/~odonnell/OData/Images/snip.gif"
     alt="%&lt;----------------------------------------------"
>

<br>

<img align=top
     src="http://cs-www.uchicago.edu/~odonnell/OData/Images/bell.gif"
     alt=""
>
Begin
<a href="/~odonnell/CS221_comments/lecture27jan.html" name="27jan">
  Friday 27 January
</a>

<p>

To get closer to efficient compiled implementations of instruction
evaluation, we first convert the syntax tree into <em>postfix</em>
form. The following iterative program uses a stack to evaluate an
expression <i>expr</i> in postfix, operating from left to right:

<hr>

<blockquote><pre>
start at the left end of <i>expr</i>, with an empty stack

<b>while</b> not at the right end of <i>expr</i> <b>do</b>

   <b>if</b> <i>nextsym</i> is a constant <b>then</b>

      push the value of <i>nextsym</i> on the stack

   <b>else</b>

      let <i>nextsym</i> represent the operation <i>op</i> with arity <i>n</i>

      replace the top <i>n</i> stack valued with <i>op</i> of them

   <b>fi</b>

now, the stack contains only one value,
   which is the value of <i>expr</i>
</pre></blockquote>

<hr>

The iterative algorithm above is essentially the result of programming
the recursion stack for the recursive algorithm explicitly. If the
expression is computed dynamically at run time, or when it includes
programmer-defined recursive operations, then the algorithm above is
executed literally with an explicit stack. More often, when the
expression is completely known at run time, and involves only operations
defined by simple arithmetic-like rules, there is no need for an
explicit implementation of the stack. Rather, we calculate the height of
the stack very easily from the postfix expression, and translate each
use of the stack into a reference to a specific memory location (usually
a high-speed register). The form of the resulting machine code is
<em>very</em> close to the postfix expression itself.

<p>

The example below shows an expression in infix and postfix notation,
followed by idealized machine code that might be compiled to evaluate
the expression, and finally a sequence of stack values that results
when evaluating the expression. Read down the sequence of variables and
operations in the machine code, and you see the postfix expression. In
the sample execution, each column represents the contents of the stack
after one of the machine operations. Arithmetic operations are shown
between columns when appropriate. The register names are written in an
addition column on the right, to show how each stack position
corresponds to a register.

<hr>

<blockquote><pre>
          <b>infix</b>

(<i>x</i> + 3) * <i>y</i> + 2 * (<i>z</i> + <i>x</i>)

          <b>postfix</b>

<i>x</i>3+<i>y</i>*2<i>zx</i>+*+

          <b>machine code</b>

Load R1 with    <i>x</i>
Set  R2  to     3
Add  R1  :=  R1 + R2
Load R2 with    <i>y</i>
Mult R1  :=  R1 * R2
Set  R2  to     2
Load R3 with    <i>z</i>
Load R4 with    <i>x</i>
Add  R3  :=  R3 + R4
Mult R2  :=  R2 * R3
Add  R1  :=  R1 + R2

          <b>stack contents</b> (with <i>x</i>=5, <i>y</i>=6, <i>z</i>=7)

                                5                  (R4)
                           7    7 + 12             (R3)
    3       6         2    2    2    2 * 24        (R2)
5   5 + 8   8 * 48   48   48   48   48   48 + 72   (R1)
</blockquote></pre>

<hr>

A good programmer can certainly produce more efficient code than the
example above. Typical compilers produce the simple, inefficient code
first, then look for improvements.

<p>

As we use cleverer and cleverer evaluation rules, both the recursive and
iterative methods above fail, and we are forced closer to an explicit
application of tree rewriting for implementation. But, the basic
iteration above may be stretched somewhat, at least to include
conventional sorts of conditional evaluation.

<hr>

Example to be typed later.

<hr>

<br>

<img align=top
     src="http://cs-www.uchicago.edu/~odonnell/OData/Images/bell.gif"
     alt=""
>
End
<a href="/~odonnell/CS221_comments/lecture27jan.html">
  Friday 27 January
</a>

<br><br>

The material above ended up a bit rushed, and mixed up with discussion
of previous and current homework. We may discuss it more on Monday 30
January.

</body>

</html>
