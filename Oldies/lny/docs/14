Lecture Notes
Alex Aiken
CS 164, Fall '94

			        Lecture 14
                                ----------


I. Course Administration

	Midterm:  Oct. 20, 6:30-8:00,  10 Evans

II. Cool Type Checking, Continued

    Recall from last time:

	"Sentences" have the form   A |- e : T
	where
		A is a type environment   x:T1, y:T2, ...
			(assigns types to identifiers in the current scope)
		e is an expression
		T is a type

    Rules from last time:


		------------    [INT]
		A |- i : Int


		A |- e1 : Int
		A |- e2 : Int
		------------------   [ADD]
		A |- e1 + e2 : Int

		A |- e1 : T1
		A, x : T1 |- e2 : T2
		------------------------------------  [LET]
		A |- let x : T1 <- e1 in e2 end : T2


                ---------------------   [ASSUME]
		A, x : Int |- x : Int


    Example:
		let x : Int <- 1 in x + 2 end

		------------------	------------------
		x : Int |- x : Int	x : Int |- 2 : Int
		------------------------------------------
    |- 1 : Int		x : Int |- x + 2 : Int
    ------------------------------------------------------
		|- let x : Int <- 1 in x + 2 end : Int			
		
     
     - Notes:
		- Assumptions contain the types of object identifiers visible in
			the current scope.
		- Assumptions are passed down the tree, types are passed up
			the tree.

     - What happens if no rules apply?  A type error . . .

                ---------------   ----------
		|- true : Bool   |- 1 : Int
		----------------------------
			|- false + 1 : ??


     - There is no rule for + that combines Bool and Int; type checking
	fails at this point.

     - Of course, the rules must be correct---the rules should only prove things
	that make sense.  A Cool type rule is sound if whenever the rule
	proves that e : T, then in any execution of the program the expression
	e evaluates to an object of type T or one of its subtypes.

     - Now, the let rule given above is too weak---e1 should be allowed
	to evaluate to any subtype of the declared type of x.

		A |- e1 : T0
		A, x : T1 |- e2 : T2
		T0 <= T1
		------------------------------------  [LET]
		A |- let x : T1 <- e1 in e2 end : T2

     - Other rules for Cool:

		--------------   [NEW]
		A |- new T : T


		A, a : T1 |-  exp : T2     T2 <= T1
		-----------------------------------  [ASSIGN]
		A, a : T1 |-  a <- exp : T2



		A |- exp0 : T0
		method f in class T0 has declaration f(x1: S1,...,xn:Sn): S
		A |- exp1 : T1
		...
		A |- expn : Tn
		for 1 <= i <= n  Ti <= Si
		----------------------------------------------------- [DISPATCH]
		A |- exp0.f(exp1,...,expn) : S



		A |- exp0 : T0
		method f in class T has decl f(x1:S1,...,xn:Sn): S
		A |- exp1 : T1
		...
		A |- expn : Tn
		T0 <= T
		for 1 <= i <= n  Ti <= Si 
		----------------------------------------------------- [SDISPATCH]
		A |- exp0.f@T(exp1,...,expn) : S

	- Note: Neither the [DISPATCH] or the [SDISPATCH] rule is quite
	  correct if the return type of "f" is SELF_TYPE.  This case
	  is explained in the manual; we leave it to you to derive the
	  typing rule.



	- if-then-else poses a problem. The result could be either
	  something from the true branch or something from the false
	  branch.  If the true branch has type A and false branch has
	  type B, the best we can do is to use the least common ancestor
	  of A and B in the inheritance hierarchy.  Call this type
	  lub(A,B).  "lub" stands least upper bound.  C is the least
	  upper bound of A and B if 1) C >= A and C >= B (i.e., it is
	  an upper bound) and 2) if D >= A and D >= B, then D >= C (i.e.,
	  C is the least upper bound).  The least upper bound is the
	  same as the least common ancestor in this case.

		A |- e1 : Bool
		A |- e2 : T2
		A |- e3 : T3
		---------------------------------------  [IF]
		A |- if e1 then e2 else e3 : lub(T2,T3)



		A |- e1 : Bool
		A |- e2 : T
		-----------------------------------  [WHILE]
		A |- while e1 loop e2 pool : Object

	- Note: Why does the loop have type Object?  Why not T?
	  Because the loop may execute 0 times; in this case we
	  don't have an object of type T to return.

		A |- exp0 : T0
		A, ID1 : T1 |- exp1 : S1
		...
		A, IDn : Tn |- expn : Sn
		S = lub(S1,...,Sn)
		T1..Tn must be distinct
		---------------------------------------------------------- [CASE]
		case exp0 of ID1 : T1 => exp1,....,IDn : Tn => expn esac : S

	- Note that the environment is augmented with the 
	  fresh identifier and the more precise type in each branch of
	  the case.

		A |- e1 : T1
		...
		A |- en : Tn
		------------------------------- [BEGIN]
		A |- begin e1; ... en; end : Tn


	- For each of +,-,*,/

		A |- e1 : Int
		A |- e2 : Int
		-------------------  [ARITH]
		A |- e1 op e2 : Int

	- For each of <=,<,=

		A |- e1 : Int
		A |- e2 : Int
		-------------------- [COMP]
		A |- e1 op e2 : Bool


		A |- e1 : Int
		-------------   [COMP]
		A |- ~e1 : Int


		A |- e1 : Bool
		------------------ [NOT]
		A |- not e1 : Bool

		--------------------- [SELF]
		A |- self : SELF_TYPE

		-------------------- [STRING]
		A |- "..." : String

		-------------------- [BOOL]
		A |- true : Bool


		A,x1:T1,...,xn:Tn |- e : S
		S <= T
		----------------------------------  [METHOD]
		A |- f(x1:T1,...,xn:Tn):T is e end;

	    - Again, note that the formal parameters added to the
		environment for the type checking of e.

		A |- exp : S
		S <= T
		--------------------- [ATTRIBUTE]
		A |- a : T <- exp : T


	- The rules above are largely complete.  One issue we have not
	  dealt with is SELF_TYPE. There are several places where SELF_TYPE
	  can be used, all of which are discussed at length in the manual.

III. Error Recovery

	- As with parsing, it is important to recover from type errors.

	- Detecting where errors occur is not so difficult as in parsing;
		there is no reason to skip over portions of the code.

	- There is one issue, which is what "type" to assign an expression
		which has no legitimate type. We would like to do something
		that would avoid cascading errors.

	- In Cool, one can have a type "No_type", which can be assigned
		to expressions that are type incorrect.
		We define No_type <= C for all other types C; that is,
		all operations are defined for no_type.


	- Consider the example:
		let y : Int in x + 2 end
		x is assigned type no_type because it is unbound.
		Then No_type + Int is an Int since No_type <= Int
		and no further error is generated.

	- No_type is what one would use in a "real" Cool compiler.  It imposes
	  some minor implementation problems, however, since the inheritance 
	  structure is no longer be a tree with No_type added.  A less desirable,
	  but simpler, alternative is to assign untypable expressions the type
	  Object.  Returning to the example:

		let y : Int in x + 2 end

		x : Object     one error is printed for the unbound id x
		Object + Int   a type error is generated for +

	- The Object solution is fine for your Cool compiler.


	
     

