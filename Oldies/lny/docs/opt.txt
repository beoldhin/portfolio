Optimization techniques for back-end
------------------------------------

lhs = left hand side
rhs = right hand side

Machine independent
-------------------

01) Algebraic simplification
    - When assignment does not change the lhs, remove assigment:
      x := x + 0
      x := x * 1
    - When something is multiplied by 0, just 0:
      x := x * 0      becomes  x := 0
    - When simpler counterparts can be found:
      x := x ** 2     becomes  x := x * x
      x := 2 * x      becomes  x := shiftl x
    - When constants can be combined - trivial:
      x := 2 + 2      becomes  x := 4
    - When constants can be combined - non-trivial
      (precision is not guaranteed):
      x := 2 + a + 2  becomes  x := 4 + a
02) Flow of control
    - Elimination of unreachable code
      Makes code smaller but not faster
    - Jumps to jumps
      To optimize this, follow a jump and see where the first label
      not containing a jump is found
03) Normal form
04) Common subexpression elimination
    - If two expressions have the same rhs, they compute the same value:
    later can be replaced by direct assignment even much later if lhs
    and rhs does not change between the two and lhs is local
      x := y + z
      w := y + z
      becomes
      x := y + z
      w := x
05) Copy propagation
    - If w := x appears in a basic block, all subsequent uses of "w"
    can be replaced by "x" in the block.  This is not an optimization
    in and of itself, but may enable dead code elimination
06) Dead code elimination
    - If lhs of an assignment is not used in a block and lhs is local,
    the statement can be deleted
07) Constant folding
    - trivial
    - non-trivial
08) Constant propagation
    - In constant folding, in two consecutive statements, if the lhs of
    the former statement is local, take the contents of the former lhs
    in the latter statement and treat its content as a constant
      a := 3
      b := a * 7
      becomes
      a := 3
      b := 21
09) Frequency reduction
    - Move expensive statements whose result does not change outside of
    loops
      for i := 1 to 100 do
          x[i] := x[i] + sqrt(z);
      becomes
      temp := sqrt(z);
      for i := 1 to 100 do
          x[i] := x[i] + temp;
10) Code hoisting
    - Saves space but not always time
    - If identical computations are done on both sides of a decision
    structure, they are moved out of the decision structure, and done
    outside the decision structure
      if a > b then
        begin
          d := 1
          c := a
        end
      else
        begin
          d := 1
          c := b
        end
      becomes
      d := 1
      if a > b then
        c := a
      else
        c := b
11) Strength reduction
    - In strength reduction, an "expensive" operation is replaced by a
    cheaper one
      2 * a   becomes  a + a  or  a << 1
      a ** 2  becomes  a * a
12) Loop unrolling
    -  A for loop that is done a constant number of times may be replaced
       by that number of repetitions of the loop body, with the loop
       control variable plugged in as a constant in each.
         for i := 1 to 4 do
           writeln(i);
         becomes
           writeln(1);
           writeln(2);
           writeln(3);
           writeln(4);
13) Inline procedures
    - 
14) Elimination of redundant expression evaluation
    - 
15) Elimination of useless and dead variables
    - 

To be explored:
- DAGs
- variables whose contents do not change are constants?

Machine dependent
-----------------

01) Peephole optimizations
02) Use of the register set
03) Avoiding redundant store/load operations
04) Use of special machine instructions (idioms)

To be explored:
- jump alignments
- function alignments
- cache miss prevention
- selected instructions for pipeline consistency
- extra instructions for pipeline consistency

Olli
