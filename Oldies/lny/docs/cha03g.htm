<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
   <TITLE> Compilers - Chapter 3  </TITLE>
   <META NAME="Author" CONTENT="Pat Terry">
</HEAD>
<BODY BGCOLOR="#FFFFFF">

<!-- Page created by Pat Terry.  All rights reserved -->

<CENTER>
<A HREF="contg.htm">Contents (GIF version)</A> | <A HREF="conts.htm">Contents (Symbol Font version)</A> | <A HREF="index.htm">Home</A>&nbsp;&nbsp;&#169; P.D. Terry
</CENTER>
<H2>3  COMPILER CONSTRUCTION AND BOOTSTRAPPING </H2>

<P>
By now the reader may have realized that developing translators is a decidedly non-trivial exercise.  If one is
faced with the task of writing a full-blown translator for a fairly complex source language, or an emulator for a
new virtual machine, or an interpreter for a low-level intermediate language, one would probably prefer not to
implement it all in machine code.

<P>
Fortunately one rarely has to contemplate such a radical step.  Translator systems are now widely available and
well understood.  A fairly obvious strategy when a translator is required for an old language on a new machine, or
a new language on an old machine (or even a new language on a new machine), is to make use of existing
compilers on either machine, and to do the development in a high level language.  This chapter provides a few
examples that should make this clearer.

<P>
<P><HR Width= "100%">
<A Name = "C03-1"></A>
<H3>3.1  Using a high-level host language </H3>

<P>
If, as is increasingly common, one's dream machine <I>M</I> is supplied with the machine coded version of a compiler
for a well-established language like C, then the production of a compiler for one's dream language <I>X</I> is achievable
by writing the new compiler, say <I>XtoM</I>, in C and compiling the source (<I>XtoM.C</I>) with the C compiler (<I>CtoM.M</I>)
running directly on <I>M</I> (see Figure 3.1).  This produces the object version (<I>XtoM.M</I>) which can then be executed
on <I>M</I>.

<P>
<P>
<PRE>
      .--------------------------.          .--------------------------.
      |           XtoM.C         |          |           XtoM.M         |
      | X      ----------&gt; M-Code|          | X     -----------&gt; M-code|
      |                          |          |                          |
      `-------.          .--------------------------.          .-------'
              |          |          CtoM.M          |          |
              |     C    | C      --------&gt;  M-code |  M-code  |
              |          |                          |          |
              `------------------.          .------------------'
                                 |          |       .----------.
                                 |  M-code  |       |M-machine |
                                 |          |       `----------'
                                 `----------'
                                 .----------.
                                 |M-machine |
                                 `----------'

    Figure 3.1  Use of C as an implementation language
</PRE>
<P>
Even though development in C is much easier than development in machine code, the process is still complex.  As
was mentioned earlier, it may be possible to develop a large part of the compiler source using compiler generator
tools - assuming, of course, that these are already available either in executable form, or as C source that can itself
be compiled easily.  The hardest part of the development is probably that associated with the back end, since this
is intensely machine dependent.  If one has access to the source code of a compiler like <I>CtoM</I> one may be able to
use this to good avail.  Although commercial compilers are rarely released in source form, source code is
available for many compilers produced at academic institutions or as components of the GNU project carried out
under the auspices of the Free Software Foundation.

<P>
<P><HR Width= "100%">
<A Name = "C03-2"></A>
<H3>3.2  Porting a high level translator </H3>

<P>
The process of modifying an existing compiler to work on a new machine is often known as <B>porting</B> the compiler.
In some cases this process may be almost trivially easy.  Consider, for example, the fairly common scenario
where a compiler <I>XtoC</I> for a popular language <I>X </I>has been implemented in C on machine <I>A</I> by writing a high-level
translator to convert programs written in <I>X</I> to C, and where it is desired to use language <I>X</I> on a machine <I>M</I> that,
like <I>A</I>, has already been blessed with a C compiler of its own.  To construct a two-stage compiler for use on either
machine, all one needs to do, in principle, is to install the source code for <I>XtoC</I> on machine <I>M</I> and recompile it.

<P>
Such an operation is conveniently represented in terms of T-diagrams chained together.  Figure 3.2(a) shows the
compilation of the <I>X</I> to C compiler, and Figure 3.2(b) shows the two-stage compilation process needed to compile
programs written in <I>X</I> to <I>M</I>-code.

<P>
<P>
<PRE>
       (a)  .--------------------------.          .--------------------------.
            |           XtoC.C         |          |           XtoC.M         |
            | X      ----------&gt;     C |          | X     -----------&gt;     C |
            |                          |          |                          |
            `-------.          .--------------------------.          .-------'
                    |          |          CtoM.M          |          |
                    |     C    | C      --------&gt;  M-code |  M-code  |
                    |          |                          |          |
                    `------------------.          .------------------'
                                       |          |
                                       |  M-code  |
                                       |          |
                                       `----------'

       (b)  .--------------------------.          .--------------------------.
            |           XtoC.M         |          |           CtoM.M         |
     PROG.X | X     -----------&gt;     C |  PROG.C  | C     ----------&gt; M-code | PROG.M
            |                          |          |                          |
            `-------.          .-------'          `-------.          .-------'
                    |          |                          |          |
                    |  M-code  |                          |  M-code  |
                    |          |                          |          |
                    `----------'                          `----------'
                    .----------.                          .----------.
                    |M-machine |                          |M-machine |
                    `----------'                          `----------'

   Figure 3.2  Porting and using a high-level translator
</PRE>
<P>
The portability of a compiler like <I>XtoC.C</I> is almost guaranteed, provided that it is itself written in "portable" C.
Unfortunately, or as Mr. Murphy would put it, "interchangeable parts don't" (more explicitly, "portable C
isn't").  Some time may have to be spent in modifying the source code of <I>XtoC.C</I> before it is acceptable as input
to <I>CtoM.M</I>, although it is to be hoped that the developers of <I>XtoC.C</I> will have used only standard C in their
work, and used pre-processor directives that allow for easy adaptation to other systems.

<P>
If there is an initial strong motivation for making a compiler portable to other systems it is, indeed, often written
so as to produce high-level code as output.  More often, of course, the original implementation of a language is
written as a self-resident translator with the aim of directly producing machine code for the current host system.

<P>
<P><HR Width= "100%">
<A Name = "C03-3"></A>
<H3>3.3  Bootstrapping </H3>

<P>
All this may seem to be skirting around a really nasty issue - how might the first high-level language have been
implemented?  In ASSEMBLER?  But then how was the assembler for ASSEMBLER produced?

<P>
A full assembler is itself a major piece of software, albeit rather simple when compared with a compiler for a
really high level language, as we shall see.  It is, however, quite common to define one language as a subset of
another, so that subset 1 is contained in subset 2 which in turn is contained in subset 3 and so on, that is:

<P>
<P>
<PRE>
          Subset 1     <SUB><IMG SRC=subset.gif ALT="subset"></SUB>       Subset 2    <SUB><IMG SRC=subset.gif ALT="subset"></SUB>       Subset 3
      of ASSEMBLER         of ASSEMBLER        of ASSEMBLER
</PRE>
<P>
One might first write an assembler for subset 1 of ASSEMBLER in machine code, perhaps on a load-and-go basis
(more likely one writes in ASSEMBLER, and then hand translates it into machine code).  This subset assembler
program might, perhaps, do very little other than convert mnemonic opcodes into binary form.  One might then
write an assembler for subset 2 of ASSEMBLER in subset 1 of ASSEMBLER, and so on.

<P>
This process, by which a simple language is used to translate a more complicated program, which in turn may
handle an even more complicated program and so on, is known as <B>bootstrapping</B>, by analogy with the idea that it
might be possible to lift oneself off the ground by tugging at one's boot-straps.

<P>
<P><HR Width= "100%">
<A Name = "C03-4"></A>
<H3>3.4  Self-compiling compilers </H3>

<P>
Once one has a working system, one can start using it to improve itself.  Many compilers for popular languages
were first written in another implementation language, as implied in section 3.1, and then rewritten in their own
source language.  The rewrite gives source for a compiler that can then be compiled with the compiler written in
the original implementation language.  This is illustrated in Figure 3.3.

<P>
<P>
<PRE>
       .--------------------------.          .--------------------------.
       |       PasToM.2.Pas       |          |        PasToM.2.M        |
       | Pascal ---------&gt; M-code |          | Pascal ---------&gt; M-code |
       |                          |          |                          |
       `-------.          .--------------------------.          .-------'
               |          |      PasToM.1.Fort       |          |
               |  Pascal  | Pascal  -------&gt;  M-code |  M-code  |
               |          |                          |          |
               `------------------.          .------------------'
                                  |          |
                                  | Fortran  |
                                  |          |
                                  `----------'

   Figure 3.3  First step in developing a self-compiling compiler
</PRE>
<P>
Clearly, writing a compiler by hand not once, but twice, is a non-trivial operation, unless the original
implementation language is close to the source language.  This is not uncommon: Oberon compilers could be
implemented in Modula-2; Modula-2 compilers, in turn, were first implemented in Pascal (all three are fairly
similar), and C<TT>++</TT> compilers were first implemented in C.

<P>
Developing a self-compiling compiler has four distinct points to recommend it.  Firstly, it constitutes a non-trivial
test of the viability of the language being compiled.  Secondly, once it has been done, further development can be
done without recourse to other translator systems.  Thirdly, any improvements that can be made to its back end
manifest themselves both as improvements to the object code it produces for general programs and as
improvements to the compiler itself.  Lastly, it provides a fairly exhaustive self-consistency check, for if the
compiler is used to compile its own source code, it should, of course, be able to reproduce its own object code
(see Figure 3.4).

<P>
<P>
<PRE>
       .--------------------------.          .--------------------------.
       |       PasToM.2.Pas       |          |        PasToM.2.M        |
       | Pascal ---------&gt; M-code |          | Pascal  --------&gt; M-code |
       |                          |          |                          |
       `-------.          .--------------------------.          .-------'
               |          |        PasToM.2.M        |          |
               |  Pascal  | Pascal  --------&gt; M-code |  M-code  |
               |          |                          |          |
               `------------------.          .------------------'
                                  |          |                <IMG SRC=uparrow.gif ALT=up>
                                  |  M-code  |    should be   |
                                  |          |&lt;-- identical --'
                                  `----------'

    Figure 3.4  A self-compiling compiler must be self-consistent
</PRE>
<P>
Furthermore, given a working compiler for a high-level language it is then very easy to produce compilers for
specialized dialects of that language.

<P>
<P><HR Width= "100%">
<A Name = "C03-5"></A>
<H3>3.5  The half bootstrap </H3>

<P>
Compilers written to produce object code for a particular machine are not intrinsically portable.  However, they
are often used to assist in a porting operation.   For example, by the time that the first Pascal compiler was
required for ICL machines, the Pascal compiler available in Z&uuml;rich (where Pascal had first been implemented on
CDC mainframes) existed in two forms (Figure 3.5).

<P>
<P>
<PRE>
       .--------------------------.             .--------------------------.
       |       PasToCDC.Pas       |             |       PasToCDC.CDC       |
       | Pascal -------&gt; CDC-code |             | Pascal  ------&gt; CDC-code |
       |                          |             |                          |
       `-------.          .-------'             `-------.          .-------'
               |          |                             |          |
               |  Pascal  |                             | CDC-code |
               |          |                             |          |
               `----------'                             `----------'

    Figure 3.5  Two versions of the original Z&uuml;rich Pascal compiler
</PRE>
<P>
The first stage of the transportation process involved changing <I>PasToCDC.Pas</I> to generate ICL machine code -
thus producing a cross compiler.  Since <I>PasToCDC.Pas</I> had been written in a high-level language, this was not
too difficult to do, and resulted in the compiler <I>PasToICL.Pas</I>.

<P>
Of course this compiler could not yet run on any machine at all.  It was first compiled using <I>PasToCDC.CDC</I>,
on the CDC machine (see Figure 3.6(a)).  This gave a cross-compiler that could run on CDC machines, but still
not, of course, on ICL machines.  One further compilation of <I>PasToICL.Pas</I>, using the cross-compiler
<I>PasToICL.CDC</I> on the CDC machine, produced the final result, <I>PasToICL.ICL</I> (Figure 3.6(b)).

<P>
<P>
<PRE>
  (a)  .--------------------------.          .--------------------------.
       |       PasToICL.Pas       |          |       PasToICL.CDC       |
       | Pascal  ------&gt; ICL-code |          | Pascal  ------&gt; ICL-code |
       |                          |          |                          |
       `-------.          .--------------------------.          .-------'
               |          |       PasToCDC.CDC       |          |
               |  Pascal  | Pascal  ------&gt; CDC-code | CDC-code |
               |          |                          |          |
               `------------------.          .------------------'
                                  |          |
                                  | CDC-code |
                                  |          |
                                  `----------'

  (b)  .--------------------------.          .--------------------------.
       |       PasToICL.Pas       |          |       PasToICL.ICL       |
       | Pascal  ------&gt; ICL-code |          | Pascal -------&gt; ICL-code |
       |                          |          |                          |
       `-------.          .--------------------------.          .-------'
               |          |       PasToICL.CDC       |          |
               |  Pascal  | Pascal  ------&gt; ICL-code | ICL-code |
               |          |                          |          |
               `------------------.          .------------------'
                                  |          |
                                  | CDC-code |
                                  |          |
                                  `----------'

   Figure 3.6  The production of the first ICL Pascal compiler by half bootstrap
</PRE>
<P>
The final product (<I>PasToICL.ICL</I>) was then transported on magnetic tape to the ICL machine, and loaded quite
easily.  Having obtained a working system, the ICL team could (and did) continue development of the system in
Pascal itself.

<P>
This porting operation was an example of what is known as a <I>half bootstrap</I> system.  The work of transportation
is essentially done entirely on the donor machine, without the need for any translator in the target machine, but a
crucial part of the original compiler (the back end, or code generator) has to be rewritten in the process.  Clearly
the method is hazardous - any flaws or oversights in writing <I>PasToICL.Pas</I> could have spelled disaster.  Such
problems can be reduced by minimizing changes made to the original compiler.  Another technique is to write an
emulator for the target machine that runs on the donor machine, so that the final compiler can be tested on the
donor machine before being transferred to the target machine.

<P>
<P><HR Width= "100%">
<A Name = "C03-6"></A>
<H3>3.6  Bootstrapping from a portable interpretive compiler </H3>

<P>
Because of the inherent difficulty of the half bootstrap for porting compilers, a variation on the full bootstrap
method described above for assemblers has often been successfully used in the case of Pascal and other similar
high-level languages.  Here most of the development takes place on the target machine, after a lot of preliminary
work has been done on the donor machine to produce an interpretive compiler that is almost portable.  It will be
helpful to illustrate with the well-known example of the Pascal-P implementation kit mentioned in section 2.5.

<P>
Users of this kit typically commenced operations by implementing an interpreter for the P-machine.  The
bootstrap process was then initiated by developing a compiler (<I>PasPtoM.PasP</I>) to translate Pascal-P source
programs to the local machine code.  This compiler could be written in Pascal-P source, development being
guided by the source of the Pascal-P to P-code compiler supplied as part of the kit.  This new compiler was then
compiled with the interpretive compiler (<I>PasPtoP.P</I>) from the kit (Figure 3.7(a)) and the source of the Pascal to
M-code compiler was then compiled by this new compiler, interpreted once again by the P-machine, to give the
final product, <I>PasPtoM.M</I> (Figure 3.7(b)).

<P>
<P>
<PRE>
   (a) .--------------------------.          .--------------------------.
       |      PasPtoM.PasP        |          |        PasPtoM.P         |
       | Pascal-P -------&gt; M-code |          | Pascal-P -------&gt; M-code |
       |                          |          |                          |
       `-------.          .--------------------------.          .-------'
               |          |        PasPtoP.P         |          |
               | Pascal-P | Pascal-P  -----&gt;  P-code |  P-code  |
               |          |                          |          |
               `------------------.          .------------------'
                                  |          |
                                  |  P-code  |
                                  |          |
                                  `----------'
                                  .-----------. --.
                                  |  P-code   |   |
                                  |Interpreter|   |
                                  |  M-code   |   P-Machine
                                  `-----------'   |
                                  .-----------.   |
                                  | M-Machine |   |
                                  `-----------' --'

   (b) .--------------------------.          .--------------------------.
       |      PasPtoM.PasP        |          |        PasPtoM.M         |
       | Pascal-P -------&gt; M-code |          | Pascal-P -------&gt; M-code |
       |                          |          |                          |
       `-------.          .--------------------------.          .-------'
               |          |        PasPtoM.P         |          |
               | Pascal-P | Pascal-P  -----&gt;  M-code |  M-code  |
               |          |                          |          |
               `------------------.          .------------------'
                                  |          |
                                  |  P-code  |
                                  |          |
                                  `----------'
                                  .-----------. --.
                                  |  P-code   |   |
                                  |Interpreter|   |
                                  |  M-code   |   P-Machine
                                  `-----------'   |
                                  .-----------.   |
                                  | M-Machine |   |
                                  `-----------' --'

   Figure 3.7 Developing a native code compiler from the P-compiler
</PRE>
<P>
The Z&uuml;rich P-code interpretive compiler could be, and indeed was, used as a highly portable development system.
It was employed to remarkable effect in developing the UCSD Pascal system, which was the first serious attempt
to implement Pascal on microcomputers.  The UCSD Pascal team went on to provide the framework for an entire
operating system, editors and other utilities - all written in Pascal, and all compiled into a well-defined P-code
object code.  Simply by providing an alternative interpreter one could move the whole system to a new
microcomputer system virtually unchanged.

<P>
<P><HR Width= "100%">
<A Name = "C03-7"></A>
<H3>3.7  A P-code assembler </H3>

<P>
There is, of course, yet another way in which a portable interpretive compiler kit might be used.  One might
commence by writing a P-code to M-code assembler, probably a relatively simple task.  Once this has been
produced one would have the assembler depicted in Figure 3.8.

<P>
<P>
<PRE>
           .--------------------------.
           |          PToM.M          |
           | P-code ---------&gt; M-code |
           |                          |
           `-------.          .-------'
                   |          |
                   |  M-code  |
                   |          |
                   `----------'

    Figure 3.8  A P-code to M-code assembler
</PRE>
<P>
The P-codes for the P-code compiler would then be assembled by this system to give another cross compiler
(Figure 3.9(a)), and the same P-code/M-code assembler could then be used as a back-end to the cross compiler
(Figure 3.9(b)).

<P>
<P>
<PRE>
   (a) .--------------------------.          .--------------------------.
       |         PasPtoP.P        |          |         PasPtoP.M        |
       | Pascal-P -------&gt; P-code |          | Pascal-P -------&gt; P-code |
       |                          |          |                          |
       `-------.          .--------------------------.          .-------'
               |          |          PtoM.M          |          |
               |  P-code  | P-code ---------&gt; M-code |  M-code  |
               |          |                          |          |
               `------------------.          .------------------'
                                  |          |
                                  |  M-code  |
                                  |          |
                                  `----------'

   (b) .--------------------------.          .--------------------------.
       |        PasPtoP.M         |          |          PtoM.M          |
       | Pascal-P ------&gt;  P-code |---------&gt;| P-code --------&gt;  M-code |
       |                          |          |                          |
       `-------.          .-------'          `-------.          .-------'
               |          |                          |          |
               |  M-code  |                          |  M-code  |
               |          |                          |          |
               `----------'                          `----------'

    Figure 3.9  Two-pass compilation and assembly using a P-code compiler
</PRE>
<P>
<P><HR Width= "100%">
<H4>Exercises </H4>

<P>
3.1  Draw the T-diagram representations for the development of a P-code to M-code assembler, assuming that
     you have a C<TT>++</TT> compiler available on the target system.

<P>
3.2  Later in this text we shall develop an interpretive compiler for a small language called Clang, using C<TT>++</TT>
     as the host language.  Draw T-diagram representations of the various components of the system as you
     foresee them.
<P>
<P><HR Width= "100%">
<H4>Further reading </H4>

<P>
A very clear exposition of bootstrapping is to be found in the book by Watt (1993).  The ICL bootstrap is further
described by Welsh and Quinn (1972).  Other early insights into bootstrapping are to be found in papers by
Lecarme and Peyrolle-Thomas (1973), by Nori <I>et al.</I> (1981), and Cornelius, Lowman and Robson (1984).

<P>
<P><HR width="100%"><P>
<CENTER>
<A HREF="contg.htm">Contents (GIF version)</A> | <A HREF="conts.htm">Contents (Symbol Font version)</A> | <A HREF="index.htm">Home</A>&nbsp;&nbsp;&#169; P.D. Terry
</CENTER>
</BODY>
</HTML>
