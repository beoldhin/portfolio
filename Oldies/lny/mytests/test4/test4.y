%{
    #include <stdio.h>
    #include <math.h>
    #include "AST.h"
    int yylex(void);
    int yyerror(char*);
    
    extern unsigned colnumber;
    extern unsigned linenumber;
%}

%union {
    int intvalue;
    AST* ast;
}

%token <intvalue> tINT
%token tPLUS
%token tMINUS
%token tTIMES
%token tLEFTP
%token tRIGHTP
%token tEND

%left tPLUS tMINUS   /* Precedence is 4 */
%left tTIMES         /* Precedence is 3 */
%right tEXP          /* Precedence is 2 */
%nonassoc UMINUS     /* Precedence is 1 */

%type <ast> stmt expr

%%

input:   input stmt
|
;

stmt:    expr tEND                { print_AST($1, 1); }
;

expr:    expr tPLUS expr          { $$ = make_operator($1, '+', $3); }
|        expr tMINUS expr         { $$ = make_operator($1, '-', $3); }
|        expr tTIMES expr         { $$ = make_operator($1, '*', $3); }
|        expr tEXP expr           { $$ = make_operator($1, '^', $3); }
|        tLEFTP expr tRIGHTP      { $$ = $2; }
|        tINT                     { $$ = make_number($1); }
;

%%

int main(void) {
    yyparse();
    return 0;
}

int yyerror(char* s) {
    fprintf(stderr, "%s: line %u, column %u\n", s, linenumber, colnumber);
    return -1;
}
