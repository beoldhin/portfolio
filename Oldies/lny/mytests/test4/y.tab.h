#define tINT 257
#define tPLUS 258
#define tMINUS 259
#define tTIMES 260
#define tLEFTP 261
#define tRIGHTP 262
#define tEND 263
#define tEXP 264
#define UMINUS 265
typedef union {
    int intvalue;
    AST* ast;
} YYSTYPE;
extern YYSTYPE yylval;
