#include "conversion.h"

unsigned htoitbl[103] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  0,  0,  0,  0,  0,  0,
    0, 10, 11, 12, 13, 14, 15,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0, 10, 11, 12, 13, 14, 15};

unsigned dec_to_int(char* str) {
    char c;
    unsigned result = 0;
    while (c = *str) {
        result *= 10;
        result += c - '0';
        str++;
    }
    return result;
}

unsigned oct_to_int(char* str) {
    /* Octal number is 0-6, thus max. 3 bits. Mask out ASCII of the */
    /* input byte and shift left. Return value is unsigned because  */
    /* octal cannot be signed. Using '+' and '-' because adding and */
    /* subtracting is faster on Pentiums (goes to both pipelines).  */
    char c;
    unsigned result = 0;
    while (c = *str) {
        result <<= 3;
        result += c - '0';
        str++;
    }
    return result;
}

unsigned hex_to_int(char* str) {
    /* Hex number is 0-15, thus max. 4 bits. It is fastest to use  */
    /* lookup table to convert an ASCII digit to an integer. Using */
    /* '+' because adding is faster on Pentiums (goes to both      */
    /* pipelines).                                                 */
    char c;
    unsigned result = 0;
    while (c = *str) {
        result <<= 4;
        result += htoitbl[c];
        str++;
    }
    return result;
}
