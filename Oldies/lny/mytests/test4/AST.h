enum treetype {
    operator_node,
    number_node
};

typedef struct AST {
    enum treetype nodetype;
    union {
        struct {
            struct AST* left;
            struct AST* right;
            char operator;
        } an_operator;
        int a_number;
    } body;
} AST;

AST* make_operator(AST* left, char operator, AST* right);
AST* make_number(int number);
