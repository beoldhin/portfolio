#define tINT 257
#define tVAR 258
#define tPLUS 259
#define tMINUS 260
#define tTIMES 261
#define tLEFTP 262
#define tRIGHTP 263
#define tEND 264
#define tEXP 265
#define UMINUS 266
typedef union {
    int intvalue;
    char variable;
    AST* ast;
} YYSTYPE;
extern YYSTYPE yylval;
