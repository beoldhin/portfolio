%{
    /* for foreign definitions inside a "%union", the header defining */
    /* those types must be *before* y.tab.h!                          */
    #include "AST.h"
    #include "y.tab.h"
    #include "conversion.h"
    
    unsigned colnumber = 1;
    unsigned linenumber = 1;
%}

NONZERODIGIT    [1-9]
DECDIGIT        [0-9]
OCTDIGIT        [0-7]
HEXDIGIT        [0-9a-fA-F]

DECIMAL         0|{NONZERODIGIT}{DECDIGIT}*
OCTAL           0{OCTDIGIT}*
HEXADECIMAL     0[xX]{HEXDIGIT}*

VARIABLE        [a-zA-Z]

%option noyywrap
%option nointeractive

%%

[ \t]       colnumber += yyleng;
"\r\n"      colnumber=1; linenumber++;  /* DOS type of EOL       */
"\n"        colnumber=1; linenumber++;  /* Unix type of EOL      */
"\r"        colnumber=1; linenumber++;  /* Macintosh type of EOL */
"/*" {
    /* Only the DOS type of EOL needs special handling      */
    /* The best method is to save the previous character.   */
    /* When '\r' is seen, 'linenumber' is increased.        */
    /* When '\n' is seen, 'linenumber' is increased only if */
    /* prevc is not '\r'.                                   */
    int c;
    int prevc = '\0';
    colnumber += yyleng;
    while (1) {
        c = input();
        if (c == '\n') {
            if (prevc != '\r') {
                colnumber = 1;
                linenumber++;
            }
        } else if (c == '\r') {
            colnumber = 1;
            linenumber++;
        } else if (c == '/') {
            colnumber++;
            if (prevc == '*')
                break;
        } else if (c == EOF) {
            fprintf(stderr, "Comment ends in EOF\n");
            break;
        } else
            colnumber++;
        prevc = c;
    }
}
{DECIMAL} {
    yylval.intvalue = dec_to_int(yytext);
    colnumber += yyleng;
    return tINT;
}
{OCTAL} {
    yylval.intvalue = oct_to_int(yytext+1);
    colnumber += yyleng;
    return tINT;
}
{HEXADECIMAL} {
    yylval.intvalue = hex_to_int(yytext+2);
    colnumber += yyleng;
    return tINT;
}
{VARIABLE} {
    yylval.variable = yytext[0];
    colnumber += yyleng;
    return tVAR;
}
"+"         colnumber+=yyleng; return tPLUS;
"-"         colnumber+=yyleng; return tMINUS;
"*"         colnumber+=yyleng; return tTIMES;
"^"         colnumber+=yyleng; return tEXP;
"("         colnumber+=yyleng; return tLEFTP;
")"         colnumber+=yyleng; return tRIGHTP;
";"         colnumber+=yyleng; return tEND;
.           fprintf(stderr, "Unknown character: line %u, column %u\n", linenumber, colnumber); colnumber+=yyleng;

%%
