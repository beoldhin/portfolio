#include "AST.h"

AST* AST_make_operator(AST* left, char operator, AST* right) {
    AST* result;
    result = (AST*)malloc(sizeof(AST));
    result->nodetype = operator_node;
    result->body.an_operator.left = left;
    result->body.an_operator.operator = operator;
    result->body.an_operator.right = right;
    return result;
}

AST* AST_make_number(int number) {
    AST* result;
    result = (AST*)malloc(sizeof(AST));
    result->nodetype = number_node;
    result->body.a_number = number;
    return result;
}

AST* AST_make_variable(char variable) {
    AST* result;
    result = (AST*)malloc(sizeof(AST));
    result->nodetype = variable_node;
    result->body.a_variable = variable;
    return result;
}

void AST_print_tree(AST* tree, int level) {
    #define step 4
    if (tree) {
        switch (tree->nodetype) {
          case operator_node:
            AST_print_tree(tree->body.an_operator.right, level+step);
            printf("%*c%c\n", level, ' ', tree->body.an_operator.operator);
            AST_print_tree(tree->body.an_operator.left, level+step);
            break;
          case number_node:
            printf("%*c%d\n", level, ' ', tree->body.a_number);
            break;
          case variable_node:
            printf("%*c%c\n", level, ' ', tree->body.a_variable);
            break;
        }
    }
}
