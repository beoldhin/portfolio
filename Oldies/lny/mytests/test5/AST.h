enum treetype {
    operator_node,
    number_node,
    variable_node
};

typedef struct AST {
    enum treetype nodetype;
    union {
        struct {
            struct AST* left;
            struct AST* right;
            char operator;
        } an_operator;
        int a_number;
        char a_variable;
    } body;
} AST;

AST* AST_make_operator(AST* left, char operator, AST* right);
AST* AST_make_number(int number);
AST* AST_make_variable(char variable);
void AST_print_tree(AST*, int);
