%{
    #include <stdio.h>
    #include <math.h>
    #include "AST.h"
    int yylex(void);
    int yyerror(char*);
    
    extern unsigned colnumber;
    extern unsigned linenumber;
%}

%union {
    int intvalue;
    char variable;
    AST* ast;
}

%token <intvalue> tINT
%token <variable> tVAR
%token tPLUS
%token tMINUS
%token tTIMES
%token tLEFTP
%token tRIGHTP
%token tEND

%left tPLUS tMINUS   /* Precedence is 4 */
%left tTIMES         /* Precedence is 3 */
%right tEXP          /* Precedence is 2 */
%nonassoc UMINUS     /* Precedence is 1 */

%type <ast> stmt expr

%%

input:   input stmt
|
;

stmt:    expr tEND                { AST_print_tree($1, 1); }
;

expr:    expr tPLUS expr          { $$ = AST_make_operator($1, '+', $3); printf("ADD\n"); }
|        expr tMINUS expr         { $$ = AST_make_operator($1, '-', $3); printf("SUB\n"); }
|        expr tTIMES expr         { $$ = AST_make_operator($1, '*', $3); printf("MUL\n"); }
|        expr tEXP expr           { $$ = AST_make_operator($1, '^', $3); printf("EXP\n"); }
|        tLEFTP expr tRIGHTP      { $$ = $2; }
|        tINT                     { $$ = AST_make_number($1); printf("PUSH %d\n", $1); }
|        tVAR                     { $$ = AST_make_variable($1); printf("PUSH %c\n", $1); }
;

%%

int main(void) {
    yyparse();
    return 0;
}

int yyerror(char* s) {
    fprintf(stderr, "%s: line %u, column %u\n", s, linenumber, colnumber);
    return -1;
}
