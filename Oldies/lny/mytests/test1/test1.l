%{
    int decnums = 0;
    int octnums = 0;
    int hexnums = 0;
%}

NONZERODIGIT    [1-9]
DECDIGIT        [0-9]
OCTDIGIT        [0-7]
HEXDIGIT        [0-9a-fA-F]

DECCONST        0|{NONZERODIGIT}{DECDIGIT}*
OCTCONST        0{OCTDIGIT}*
HEXCONST        0[xX]{HEXDIGIT}*

%option noyywrap
%option nointeractive

%%

[ \t]       ;
"\r"?"\n"   ;
"/*" {
    int c;
    int lastc = '\0';
    while (1) {
        c = input();
        if (c == '/') {
            if (lastc == '*')
                break;
        } else if (c == EOF)
                break;
        lastc = c;
    }
    break;
}
{DECCONST} {
    decnums++;
    break;
}
{OCTCONST} {
    octnums++;
    break;
}
{HEXCONST} {
    hexnums++;
    break;
}
.           ;

%%
int main(void) {
    yylex();
    printf("decnums:   %d\n", decnums);
    printf("octnums:   %d\n", octnums);
    printf("hexnums:   %d\n", hexnums);
}
