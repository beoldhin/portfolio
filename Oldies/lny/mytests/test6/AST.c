#include "AST.h"

AST* make_operator(AST* left, char operator, AST* right) {
    AST* result;
    result = (AST*)malloc(sizeof(AST));
    result->nodetype = operator_node;
    result->body.an_operator.left = left;
    result->body.an_operator.operator = operator;
    result->body.an_operator.right = right;
    return result;
}

AST* make_number(int number) {
    AST* result;
    result = (AST*)malloc(sizeof(AST));
    result->nodetype = number_node;
    result->body.a_number = number;
    return result;
}

void print_AST(AST* tree, int level) {
    #define step 4
    if (tree) {
        switch (tree->nodetype) {
          case operator_node:
            print_AST(tree->body.an_operator.right, level+step);
            printf("%*c%c\n", level, ' ', tree->body.an_operator.operator);
            print_AST(tree->body.an_operator.left, level+step);
            break;
          case number_node:
            printf("%*c%d\n", level, ' ', tree->body.a_number);
            break;
        }
    }
}
