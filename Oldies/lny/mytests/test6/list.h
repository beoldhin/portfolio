#include <stdlib.h>

typedef struct node {
    struct node* prev;
    struct node* next;
} NODE;

typedef struct list {
    NODE* head;
    NODE* tail;
    size_t satsize;
} LIST;

LIST* list_create(size_t);
void* list_pushback(LIST*, void*);
void* list_popback(LIST*);
void* list_search_first(LIST*, int (*list_compare)(void*));
void* list_search_last(LIST*, int (*list_compare)(void*));
void list_clear(LIST*);
void list_clear_string(LIST*);
