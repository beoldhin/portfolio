#include "list.h"

typedef struct hash {
    LIST* table;
    size_t tblsize;
} HASH;

HASH* hash_create(size_t, size_t);
void hash_insert(HASH*, void*, char*);
void* hash_search_first(HASH*, char*, int (*hash_compare)(void*));
void* hash_search_last(HASH*, char*, int (*hash_compare)(void*));
void hash_clear(HASH*);
int string_compare(char*, char*);
