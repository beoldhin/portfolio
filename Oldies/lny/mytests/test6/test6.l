%{
    /* for foreign definitions inside a "%union", the header defining */
    /* those types must be *before* y.tab.h!                          */
    #include "y.tab.h"
    
    void skip_comment(void);
    
    unsigned colnumber = 1;
    unsigned linenumber = 1;
%}

D        [0-9]
L        [a-zA-Z_]
H        [a-fA-F0-9]
E        [Ee][+-]?{D}+
FS       (f|F|l|L)
IS       (u|U|l|L)*

%option noyywrap
%option nointeractive

%%

[ \t\v\f]                 colnumber += yyleng;
"\r\n"                    colnumber=1; linenumber++;  /* DOS type of EOL       */
"\n"                      colnumber=1; linenumber++;  /* Unix type of EOL      */
"\r"                      colnumber=1; linenumber++;  /* Macintosh type of EOL */
"/*"                      { skip_comment(); }

"auto"                    { colnumber+=yyleng; return(AUTO); }
"break"                   { colnumber+=yyleng; return(BREAK); }
"case"                    { colnumber+=yyleng; return(CASE); }
"char"                    { colnumber+=yyleng; return(CHAR); }
"const"                   { colnumber+=yyleng; return(CONST); }
"continue"                { colnumber+=yyleng; return(CONTINUE); }
"default"                 { colnumber+=yyleng; return(DEFAULT); }
"do"                      { colnumber+=yyleng; return(DO); }
"double"                  { colnumber+=yyleng; return(DOUBLE); }
"else"                    { colnumber+=yyleng; return(ELSE); }
"enum"                    { colnumber+=yyleng; return(ENUM); }
"extern"                  { colnumber+=yyleng; return(EXTERN); }
"float"                   { colnumber+=yyleng; return(FLOAT); }
"for"                     { colnumber+=yyleng; return(FOR); }
"goto"                    { colnumber+=yyleng; return(GOTO); }
"if"                      { colnumber+=yyleng; return(IF); }
"int"                     { colnumber+=yyleng; return(INT); }
"long"                    { colnumber+=yyleng; return(LONG); }
"register"                { colnumber+=yyleng; return(REGISTER); }
"return"                  { colnumber+=yyleng; return(RETURN); }
"short"                   { colnumber+=yyleng; return(SHORT); }
"signed"                  { colnumber+=yyleng; return(SIGNED); }
"sizeof"                  { colnumber+=yyleng; return(SIZEOF); }
"static"                  { colnumber+=yyleng; return(STATIC); }
"struct"                  { colnumber+=yyleng; return(STRUCT); }
"switch"                  { colnumber+=yyleng; return(SWITCH); }
"typedef"                 { colnumber+=yyleng; return(TYPEDEF); }
"union"                   { colnumber+=yyleng; return(UNION); }
"unsigned"                { colnumber+=yyleng; return(UNSIGNED); }
"void"                    { colnumber+=yyleng; return(VOID); }
"volatile"                { colnumber+=yyleng; return(VOLATILE); }
"while"                   { colnumber+=yyleng; return(WHILE); }

{L}({L}|{D})*             { colnumber+=yyleng; return(IDENTIFIER); }

0[xX]{H}+{IS}?            { colnumber+=yyleng; return(CONSTANT); }
0{D}+{IS}?                { colnumber+=yyleng; return(CONSTANT); }
{D}+{IS}?                 { colnumber+=yyleng; return(CONSTANT); }
L?'(\\.|[^\\'])+'         { colnumber+=yyleng; return(CONSTANT); }

{D}+{E}{FS}?              { colnumber+=yyleng; return(CONSTANT); }
{D}*"."{D}+({E})?{FS}?    { colnumber+=yyleng; return(CONSTANT); }
{D}+"."{D}*({E})?{FS}?    { colnumber+=yyleng; return(CONSTANT); }

L?\"(\\.|[^\\"])*\"       { colnumber+=yyleng; return(STRING_LITERAL); }

"..."                     { colnumber+=yyleng; return(ELLIPSIS); }
">>="                     { colnumber+=yyleng; return(RIGHT_ASSIGN); }
"<<="                     { colnumber+=yyleng; return(LEFT_ASSIGN); }
"+="                      { colnumber+=yyleng; return(ADD_ASSIGN); }
"-="                      { colnumber+=yyleng; return(SUB_ASSIGN); }
"*="                      { colnumber+=yyleng; return(MUL_ASSIGN); }
"/="                      { colnumber+=yyleng; return(DIV_ASSIGN); }
"%="                      { colnumber+=yyleng; return(MOD_ASSIGN); }
"&="                      { colnumber+=yyleng; return(AND_ASSIGN); }
"^="                      { colnumber+=yyleng; return(XOR_ASSIGN); }
"|="                      { colnumber+=yyleng; return(OR_ASSIGN); }
">>"                      { colnumber+=yyleng; return(RIGHT_OP); }
"<<"                      { colnumber+=yyleng; return(LEFT_OP); }
"++"                      { colnumber+=yyleng; return(INC_OP); }
"--"                      { colnumber+=yyleng; return(DEC_OP); }
"->"                      { colnumber+=yyleng; return(PTR_OP); }
"&&"                      { colnumber+=yyleng; return(AND_OP); }
"||"                      { colnumber+=yyleng; return(OR_OP); }
"<="                      { colnumber+=yyleng; return(LE_OP); }
">="                      { colnumber+=yyleng; return(GE_OP); }
"=="                      { colnumber+=yyleng; return(EQ_OP); }
"!="                      { colnumber+=yyleng; return(NE_OP); }
";"                       { colnumber+=yyleng; return(';'); }
("{"|"<%")                { colnumber+=yyleng; return('{'); }
("}"|"%>")                { colnumber+=yyleng; return('}'); }
","                       { colnumber+=yyleng; return(','); }
":"                       { colnumber+=yyleng; return(':'); }
"="                       { colnumber+=yyleng; return('='); }
"("                       { colnumber+=yyleng; return('('); }
")"                       { colnumber+=yyleng; return(')'); }
("["|"<:")                { colnumber+=yyleng; return('['); }
("]"|":>")                { colnumber+=yyleng; return(']'); }
"."                       { colnumber+=yyleng; return('.'); }
"&"                       { colnumber+=yyleng; return('&'); }
"!"                       { colnumber+=yyleng; return('!'); }
"~"                       { colnumber+=yyleng; return('~'); }
"-"                       { colnumber+=yyleng; return('-'); }
"+"                       { colnumber+=yyleng; return('+'); }
"*"                       { colnumber+=yyleng; return('*'); }
"/"                       { colnumber+=yyleng; return('/'); }
"%"                       { colnumber+=yyleng; return('%'); }
"<"                       { colnumber+=yyleng; return('<'); }
">"                       { colnumber+=yyleng; return('>'); }
"^"                       { colnumber+=yyleng; return('^'); }
"|"                       { colnumber+=yyleng; return('|'); }
"?"                       { colnumber+=yyleng; return('?'); }

.                         colnumber+=yyleng;

%%

void skip_comment(void) {
    /* Only the DOS type of EOL needs special handling      */
    /* The best method is to save the previous character.   */
    /* When '\r' is seen, 'linenumber' is increased.        */
    /* When '\n' is seen, 'linenumber' is increased only if */
    /* prevc is not '\r'.                                   */
    int c;
    int prevc = '\0';
    colnumber += yyleng;
    while (1) {
        c = input();
        if (c == '\n') {
            if (prevc != '\r') {
                colnumber = 1;
                linenumber++;
            }
        } else if (c == '\r') {
            colnumber = 1;
            linenumber++;
        } else if (c == '/') {
            colnumber++;
            if (prevc == '*')
                break;
        } else if (c == EOF) {
            fprintf(stderr, "Comment ends in EOF\n");
            break;
        } else
            colnumber++;
        prevc = c;
    }
}
