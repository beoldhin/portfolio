#include "hash.h"
#include "crc32.h"
#include <string.h>

HASH* hash_create(size_t satsize, size_t tblsize) {
    LIST* hashcur;
    LIST* hashend;
    HASH* new_hash;
    new_hash = (HASH*)malloc(sizeof(HASH));
    new_hash->table = (LIST*)malloc(sizeof(LIST) * tblsize);
    new_hash->tblsize = tblsize;
    hashcur = new_hash->table;
    hashend = hashcur + tblsize;
    while (hashcur != hashend) {
        hashcur->head = NULL;
        hashcur->tail = NULL;
        hashcur->satsize = satsize;
        hashcur++;
    }
    return new_hash;
}

void hash_insert(HASH* hash, void* sat, char* str) {
    int slen;
    LIST* newpos;
    DWORD crcval;
    crcval = crc32le_generate_string_length(str,&slen);
    *(char**)sat = (char*)malloc(slen);
    memcpy(*(char**)sat, str, slen);
    newpos = hash->table + (crcval % hash->tblsize);
    list_pushback(newpos, sat);
}

void* hash_search_first(HASH* hash, char* str, int (*hash_compare)(void*)) {
    LIST* listpos;
    void* retval = NULL;
    listpos = hash->table + (crc32le_generate_string(str) % hash->tblsize);
    retval = list_search_first(listpos, hash_compare);
    return retval;
}

void* hash_search_last(HASH* hash, char* str, int (*hash_compare)(void*)) {
    LIST* listpos;
    void* retval = NULL;
    listpos = hash->table + (crc32le_generate_string(str) % hash->tblsize);
    retval = list_search_last(listpos, hash_compare);
    return retval;
}

void hash_clear(HASH* hash) {
    LIST* hashcur;
    LIST* hashend;
    hashcur = hash->table;
    hashend = hashcur + hash->tblsize;
    while (hashcur != hashend) {
        list_clear_string(hashcur);
        hashcur++;
    }
    free(hash->table);
    free(hash);
}

int string_compare(char* s1, char* s2) {
    while (*s1 && *s2) {
        if (*s1 != *s2) {
            return 0;
        }
        s1++;
        s2++;
    }
    return 1;
}
