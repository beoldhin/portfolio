#include <string.h>
#include "list.h"

LIST* list_create(size_t satsize) {
    LIST* new_list;
    new_list = (LIST*)malloc(sizeof(LIST));
    new_list->head = NULL;
    new_list->tail = NULL;
    new_list->satsize = satsize;
    return new_list;
}

/* current.next = new     */
/* new.prev     = current */
/* new.next     = NULL    */
void* list_pushback(LIST* list, void* sat) {
    NODE* new_node;
    new_node = (NODE*)malloc(sizeof(NODE)+list->satsize);
    if (list->tail) {
        /* One or more nodes exists */
        list->tail->next = new_node;
        new_node->prev = list->tail;
    } else {
        /* No nodes exist */
        list->head = new_node;
    }
    list->tail = new_node;
    new_node->next = NULL;
    memcpy(new_node+1, sat, list->satsize);
    return new_node + 1;
}

void* list_popback(LIST* list) {
    NODE* oldnode;
    void* retval = NULL;
    oldnode = list->tail;
    if (oldnode) {
        if (list->head != oldnode) {
            /* There is more than one node */
            list->tail = oldnode->prev;
            list->tail->next = NULL;
            retval = list->tail + 1;
        } else {
            /* There is only one node */
            list->head = NULL;
            list->tail = NULL;
        }
        free(oldnode);
    }
    return retval;
}

void* list_search_first(LIST* list, int (*list_compare)(void*)) {
    NODE* curnode;
    void* retval = NULL;
    curnode = list->head;
    while (curnode) {
        if (list_compare(curnode+1)) {
            retval = curnode + 1;
            break;
        }
        curnode = curnode->next;
    }
    return retval;
}

void* list_search_last(LIST* list, int (*list_compare)(void*)) {
    NODE* curnode;
    void* retval = NULL;
    curnode = list->tail;
    while (curnode) {
        if (list_compare(curnode+1)) {
            retval = curnode + 1;
            break;
        }
        curnode = curnode->prev;
    }
    return retval;
}

void list_clear(LIST* list) {
    NODE* oldnode;
    NODE* curnode = list->head;
    while (curnode) {
        oldnode = curnode;
        curnode = curnode->next;
        free(oldnode);
    }
    list->head = NULL;
    list->tail = NULL;
}

void list_clear_string(LIST* list) {
    char* satstr;
    NODE* oldnode;
    NODE* curnode = list->head;
    while (curnode) {
        oldnode = curnode;
        curnode = curnode->next;
        satstr = *((char**)(oldnode+1));
        free(satstr);
        free(oldnode);
    }
    list->head = NULL;
    list->tail = NULL;
}
