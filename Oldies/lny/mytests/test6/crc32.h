#define DWORD unsigned long

void crc32le_table_generate(DWORD);
void crc32be_table_generate(DWORD);
DWORD crc32le_generate_linear(char*, int);
DWORD crc32be_generate_linear(char*, int);
DWORD crc32le_generate_segmented(DWORD, char*, int);
DWORD crc32be_generate_segmented(DWORD, char*, int);
DWORD crc32le_generate_string(char*);
DWORD crc32be_generate_string(char*);
DWORD crc32le_generate_string_length(char*, int*);
DWORD crc32be_generate_string_length(char*, int*);
