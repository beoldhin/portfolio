short yylhs[] = {                                        -1,
    0,    0,    1,    2,    2,    2,    2,    2,    2,    2,
};
short yylen[] = {                                         2,
    2,    0,    2,    3,    3,    3,    3,    2,    3,    1,
};
short yydefred[] = {                                      2,
    0,   10,    0,    0,    1,    0,    8,    0,    0,    0,
    0,    3,    0,    9,    0,    0,    0,    0,
};
short yydgoto[] = {                                       1,
    5,    6,
};
short yysindex[] = {                                      0,
 -212,    0, -212, -212,    0, -247,    0, -240, -212, -212,
 -212,    0, -212,    0, -255, -255, -262, -262,
};
short yyrindex[] = {                                      0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0, -221, -219, -233, -227,
};
short yygindex[] = {                                      0,
    0,   -3,
};
short yytable[] = {                                       7,
    8,   13,    0,    0,   11,   15,   16,   17,   13,   18,
    9,   10,   11,    0,    0,   12,   13,    9,   10,   11,
    0,   14,    0,   13,    6,    6,    6,    0,    6,    6,
    7,    7,    7,    0,    7,    7,    4,    4,    5,    5,
    4,    4,    5,    5,    2,    0,    3,    0,    4,
};
short yycheck[] = {                                       3,
    4,  264,   -1,   -1,  260,    9,   10,   11,  264,   13,
  258,  259,  260,   -1,   -1,  263,  264,  258,  259,  260,
   -1,  262,   -1,  264,  258,  259,  260,   -1,  262,  263,
  258,  259,  260,   -1,  262,  263,  258,  259,  258,  259,
  262,  263,  262,  263,  257,   -1,  259,   -1,  261,
};
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#if YYDEBUG
char *yyname[] = {
"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"tINT","tPLUS","tMINUS","tTIMES",
"tLEFTP","tRIGHTP","tEND","tEXP","UMINUS",
};
char *yyrule[] = {
"$accept : input",
"input : input stmt",
"input :",
"stmt : expr tEND",
"expr : expr tPLUS expr",
"expr : expr tMINUS expr",
"expr : expr tTIMES expr",
"expr : expr tEXP expr",
"expr : tMINUS expr",
"expr : tLEFTP expr tRIGHTP",
"expr : tINT",
};
#endif
