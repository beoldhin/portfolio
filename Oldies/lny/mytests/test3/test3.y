%{
    #include <stdio.h>
    #include <math.h>
    int yylex(void);
    int yyerror(char*);
    
    extern unsigned colnumber;
    extern unsigned linenumber;
%}

%union {
    int intvalue;
}

%token <intvalue> tINT
%token tPLUS
%token tMINUS
%token tTIMES
%token tLEFTP
%token tRIGHTP
%token tEND

%left tPLUS tMINUS   /* Precedence is 4 */
%left tTIMES         /* Precedence is 3 */
%right tEXP          /* Precedence is 2 */
%nonassoc UMINUS     /* Precedence is 1 */

%type <intvalue> stmt expr

%%

input:   input stmt
|
;

stmt:    expr tEND                { printf("result: %d\n", $1); }
;

expr:    expr tPLUS expr          { $$ = $1 + $3; printf("%4d   + %4d = %4d\n", $1, $3, $$); }
|        expr tMINUS expr         { $$ = $1 - $3; printf("%4d   - %4d = %4d\n", $1, $3, $$); }
|        expr tTIMES expr         { $$ = $1 * $3; printf("%4d   * %4d = %4d\n", $1, $3, $$); }
|        expr tEXP expr           { $$ = pow($1, $3); printf("%4d   ^ %4d = %4d\n", $1, $3, $$); }
|        tMINUS expr %prec UMINUS { $$ = -$2; }
|        tLEFTP expr tRIGHTP      { $$ = $2; }
|        tINT                     { $$ = $1; }
;

%%

int main(void) {
    yyparse();
    return 0;
}

int yyerror(char* s) {
    fprintf(stderr, "%s: line %u, column %u\n", s, linenumber, colnumber);
    return -1;
}
