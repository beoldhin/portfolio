/* A dynamic permuter for gperf's explicit hash keys                  */
/* A real hash pipe megakludge ;)                                     */
/* Buck case is after normal cases because it is slightly slower than */
/* normal cases but faster than normal case with one key more (it     */
/* uses variable "len-1" rather than direct presentation).            */
/* Todo:                                                              */
/* + Change "if [ ! -s %s ]" to internal "fopen" & "fseek"            */
/* + Start gperf by using "system()"                                  */
/* + Use string class                                                 */
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

#define SHOW_BUCKS

int generateperms(int);
int analyzeperms(int);
int isinvec(void);
long tellfsize(const char*);

struct BUCK {
    vector <int> buck;
};
vector <BUCK> bucks;
BUCK tmpvec;

int c = 1;
char numbuf[10+1]; /* Converting between char* and string is SLOW */
string aparams;
string lparams;
string rparams;
string repfname;
char* perm;
int keylen;


int main(int argc, char* argv[]) {
    int i;
    
    if (argc != 5) {
        fprintf(stderr, "Invalid argument count.\n");
        fprintf(stderr, "Usage: kgen lparams rparams repfname keylen.\n");
        return -1;
    }
    lparams = argv[1];
    rparams = argv[2];
    repfname = argv[3];
    keylen = atoi(argv[4]);
    
    printf("Permutation");
    perm = (char*)malloc((keylen+1)*sizeof(char));
    for (i=1; i<=keylen; i++) {
        if (generateperms(i)) {
            break;
        }
    }
    free(perm);
    puts("");
    
    return 0;
}

int generateperms(int keylim) {
    int i = 1;
    perm[1] = 1;
    while (i > 0) {
        if (i == keylim) {
            printf(" %d", c);
            fflush(stdout);
            if (analyzeperms(i)) {
                return -1;
            }
            c++;
        }
        if (perm[i] == keylen) {
            i--;
            perm[i]++;
        } else {
            i++;
            perm[i] = perm[i-1] + 1;
        }
    }
    return 0;
}

int analyzeperms(int keylim) {
    int i;
    #ifdef SHOW_BUCKS
    int j, lim;
    if (keylim > 1) {
        for (i=1; i<keylim; i++)
            tmpvec.buck.push_back(perm[i]);
        if (!isinvec()) {
            j = bucks.size() - 1;
            if (j >= 0) {
                aparams = lparams + " -k ";
                lim = bucks[j].buck.size();
                for (i=0; i<lim; i++) { /* Should be iterator - SLOW */
                    sprintf(numbuf, "%d", bucks[j].buck[i]);
                    aparams += (string)numbuf + ",";
                }
                aparams += "$ " + rparams + " 2> " + repfname + "\n";
                system(aparams.c_str());
                if (tellfsize(repfname.c_str()) == 0) {
                    return -1;
                }
            }
            bucks.push_back(tmpvec);
        }
        tmpvec.buck.clear();
    }
    #endif
    aparams = lparams + " -k ";
    for (i=1; i<=keylim; i++) {
        if (i != keylim) {
            sprintf(numbuf, "%d", perm[i]);
            aparams += (string)numbuf + ",";
        } else {
            sprintf(numbuf, "%d", perm[i]);
            aparams += (string)numbuf + " " + rparams + " 2> " + repfname + "\n";
        }
    }
    system(aparams.c_str());
    if (tellfsize(repfname.c_str()) == 0) {
        return -1;
    }
    return 0;
}

int isinvec(void) {
    vector <BUCK>::iterator a;
    vector <BUCK>::iterator b;
    a = bucks.begin();
    b = bucks.end();
    while (a != b) {
        if ((*a).buck == tmpvec.buck) {
            return 1;
        }
        a++;
    }
    return 0;
}

long tellfsize(const char* fname) {
    long fpos;
    FILE* filep;
    filep = fopen(fname, "rb");
    if (filep == NULL) {
        return -1;
    }
    fseek(filep, 0, SEEK_END);
    fpos = ftell(filep);
    fclose(filep);
    return fpos;
}
