/* ANSI-C code produced by gperf version 2.7.1 (19981006 egcs) */
/* Command-line: gperf -LANSI-C -Kcolorname -HLibForegrounds_hash -NLibForegrounds_in_word_set -C -E -j 1 -k 1,$ -t -o LibForegrounds.gperf  */
#include <string.h>
struct foregrounds {
    char* colorname;
    char color;
    char bold;
};
/* maximum key range = 17, duplicates = 0 */

#ifdef __GNUC__
__inline
#endif
static unsigned int
LibForegrounds_hash (register const char *str, register unsigned int len)
{
  static const unsigned char asso_values[] =
    {
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21,  0,  0,
       6,  2, 21,  2, 21, 21, 21, 10,  0, 21,
       0, 21,  0, 21, 11, 21, 21, 21, 21,  9,
      21,  3, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21, 21, 21, 21, 21,
      21, 21, 21, 21, 21, 21
    };
  return len + asso_values[(unsigned char)str[len - 1]] + asso_values[(unsigned char)str[0]];
}

#ifdef __GNUC__
__inline
#endif
const struct foregrounds *
LibForegrounds_in_word_set (register const char *str, register unsigned int len)
{
  enum
    {
      TOTAL_KEYWORDS = 17,
      MIN_WORD_LENGTH = 3,
      MAX_WORD_LENGTH = 11,
      MIN_HASH_VALUE = 4,
      MAX_HASH_VALUE = 20
    };

  static const struct foregrounds wordlist[] =
    {
      {""}, {""}, {""}, {""},
      {"cyan",        36, 0},
      {"brown",       33, 0},
      {"blue",        34, 0},
      {"green",       32, 0},
      {"purple",      35, 0},
      {"lightcyan",   36, 1},
      {"lightgreen",  32, 1},
      {"lightblue",   34, 1},
      {"lightgray",   37, 0},
      {"lightpurple", 35, 1},
      {"lightred",    31, 1},
      {"black",       30, 0},
      {"white",       37, 1},
      {"darkgray",    30, 1},
      {"yellow",      33, 1},
      {"boldblack",   30, 1},
      {"red",         31, 0}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = LibForegrounds_hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = wordlist[key].colorname;

          if (*str == *s && !strcmp (str + 1, s + 1))
            return &wordlist[key];
        }
    }
  return 0;
}
