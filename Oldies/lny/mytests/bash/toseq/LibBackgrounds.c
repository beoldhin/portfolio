/* ANSI-C code produced by gperf version 2.7.1 (19981006 egcs) */
/* Command-line: gperf -LANSI-C -Kcolorname -HLibBackgrounds_hash -NLibBackgrounds_in_word_set -C -E -j 1 -k 3 -t -o LibBackgrounds.gperf  */
#include <string.h>
struct backgrounds {
    char* colorname;
    char color;
};
/* maximum key range = 12, duplicates = 0 */

#ifdef __GNUC__
__inline
#endif
static unsigned int
LibBackgrounds_hash (register const char *str, register unsigned int len)
{
  static const unsigned char asso_values[] =
    {
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15,  0, 15, 15,
       0,  9, 15,  3, 15,  8, 15, 15, 15, 15,
       0,  3, 15, 15,  0, 15, 15,  7,  0, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15, 15, 15, 15, 15,
      15, 15, 15, 15, 15, 15
    };
  return len + asso_values[(unsigned char)str[2]];
}

#ifdef __GNUC__
__inline
#endif
const struct backgrounds *
LibBackgrounds_in_word_set (register const char *str, register unsigned int len)
{
  enum
    {
      TOTAL_KEYWORDS = 12,
      MIN_WORD_LENGTH = 3,
      MAX_WORD_LENGTH = 10,
      MIN_HASH_VALUE = 3,
      MAX_HASH_VALUE = 14
    };

  static const struct backgrounds wordlist[] =
    {
      {""}, {""}, {""},
      {"red",       41},
      {"cyan",      46},
      {"black",     40},
      {"purple",    45},
      {"inverse",    7},
      {"brown",     43},
      {"concealed",  8},
      {"underscore", 4},
      {"blue",      44},
      {"lightgray", 47},
      {"blink",      5},
      {"green",     42}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = LibBackgrounds_hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = wordlist[key].colorname;

          if (*str == *s && !strcmp (str + 1, s + 1))
            return &wordlist[key];
        }
    }
  return 0;
}
