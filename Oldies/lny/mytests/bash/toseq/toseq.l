%{
#include <vector>
#include <ctype.h>
#include "LibBackgrounds.c"
#include "LibForegrounds.c"

#define UNDEFINED CHAR_MAX

// Speculations below
#define SPEC_BACKGROUND 40
#define SPEC_FOREGROUND 37
#define SPEC_BOLD 0

void literal_code_init(void);
void string_to_lower(char*);
void optimize_colors(void);
void optimize_nonprinting(void);
void output_sequences(void);

enum definition_type {
    nonprinting,
    color_definition,
    literal_text,
    literal_code
};

enum printable_definition {
    no,
    yes
};

enum bc_or_fc {
    background_color,
    foreground_color
};

enum nonprinting_type {
    start,
    end
} nonprinting_type;

enum literalcode_type {
    bell,
    date,
    hostname_short,
    hostname_long,
    newline,
    carriage_return,
    shellname,
    time_nonus,
    time_us_short,
    time_us_long,
    current_user,
    bashv_short,
    bashv_long,
    directory_current,
    directory_basename,
    this_history,
    this_number,
    exit_code
} literalcode_type;

typedef struct color_type {
    enum bc_or_fc whichcolor;
    char base_color;
    char bold;
} color_type;

typedef struct entry {
    char inuse;
    enum definition_type deftype;
    enum printable_definition printable;
    union {
        enum nonprinting_type nonprinting;
        color_type color_definition;
        char* literal_text;
        enum literalcode_type literal_code;
    };
} entry;

entry tmpentry;
vector <entry> entries;
const struct backgrounds* background;
const struct foregrounds* foreground;
unsigned colnumber = 1;
unsigned linenumber = 1;
char* definition;
%}

COLORDEFINITION      [a-zA-Z]+
LITERALDEFINITION    \"[^"]+\"
OCTAL                [0-7]+

%option noyywrap
%option nointeractive

%%

"//"[^\n]*                       linenumber++;
[ \t]                            colnumber+=yyleng;
"\r\n"                           colnumber+=yyleng; linenumber++; /* DOS type of EOL */
"\n"                             colnumber+=yyleng; linenumber++; /* Unix type of EOL */
"\r"                             colnumber+=yyleng; linenumber++; /* Macintosh type of EOL */
[Bb]"ackground"\.{COLORDEFINITION} {
    definition = yytext + 11;
    colnumber += yyleng;
    string_to_lower(definition);
    background = LibBackgrounds_in_word_set(definition, strlen(definition));
    if (background != 0) {
        tmpentry.inuse = yes;
        tmpentry.deftype = color_definition;
        tmpentry.printable = no;
        tmpentry.color_definition.whichcolor = background_color;
        tmpentry.color_definition.base_color = background->color;
        entries.push_back(tmpentry);
    }
}
[Ff]"oreground"\.{COLORDEFINITION} {
    definition = yytext + 11;
    colnumber += yyleng;
    string_to_lower(definition);
    foreground = LibForegrounds_in_word_set(definition, strlen(definition));
    if (foreground != 0) {
        tmpentry.inuse = yes;
        tmpentry.deftype = color_definition;
        tmpentry.printable = no;
        tmpentry.color_definition.whichcolor = foreground_color;
        tmpentry.color_definition.base_color = foreground->color;
        tmpentry.color_definition.bold = foreground->bold;
        entries.push_back(tmpentry);
    }
}
[Ll]"iteral"[Tt]"ext"\.{LITERALDEFINITION} {
    definition = yytext + 13;
    colnumber += yyleng;
    tmpentry.inuse = yes;
    tmpentry.deftype = literal_text;
    tmpentry.printable = yes;
    tmpentry.literal_text = (char*)malloc((strlen(definition)-1)+1);
    memcpy(tmpentry.literal_text, definition, (strlen(definition)-1)+1);
    tmpentry.literal_text[strlen(definition)-1] = '\0';
    entries.push_back(tmpentry);
}
{OCTAL} {
    colnumber += yyleng;
    tmpentry.inuse = yes;
    tmpentry.deftype = literal_text;
    tmpentry.printable = yes;
    tmpentry.literal_text = (char*)malloc(2+(strlen(yytext)+1));
    strcpy(tmpentry.literal_text, "\\0");
    memcpy(tmpentry.literal_text+2, yytext, strlen(yytext)+1);
    entries.push_back(tmpentry);
}
[Bb]ell {
    literal_code_init();
    tmpentry.literal_code = bell;
    entries.push_back(tmpentry);
}
[Dd]ate {
    literal_code_init();
    tmpentry.literal_code = date;
    entries.push_back(tmpentry);
}
[Hh]ostname\.[Ss]hort {
    literal_code_init();
    tmpentry.literal_code = hostname_short;
    entries.push_back(tmpentry);
}
[Hh]ostname\.[Ll]ong {
    literal_code_init();
    tmpentry.literal_code = hostname_long;
    entries.push_back(tmpentry);
}
[Nn]ewline {
    literal_code_init();
    tmpentry.literal_code = newline;
    entries.push_back(tmpentry);
}
[Cc]arriage[Rr]eturn {
    literal_code_init();
    tmpentry.literal_code = carriage_return;
    entries.push_back(tmpentry);
}
[Ss]hellname {
    literal_code_init();
    tmpentry.literal_code = shellname;
    entries.push_back(tmpentry);
}
[Tt]ime\.24-hour {
    literal_code_init();
    tmpentry.literal_code = time_nonus;
    entries.push_back(tmpentry);
}
[Tt]ime\.12-hour\.[Ss]hort {
    literal_code_init();
    tmpentry.literal_code = time_us_short;
    entries.push_back(tmpentry);
}
[Tt]ime\.12-hour\.[Ll]ong {
    literal_code_init();
    tmpentry.literal_code = time_us_long;
    entries.push_back(tmpentry);
}
[Cc]urrent[Uu]ser {
    literal_code_init();
    tmpentry.literal_code = current_user;
    entries.push_back(tmpentry);
}
[Bb]ash[Vv]ersion\.[Ss]hort {
    literal_code_init();
    tmpentry.literal_code = bashv_short;
    entries.push_back(tmpentry);
}
[Bb]ash[Vv]ersion\.[Ll]ong {
    literal_code_init();
    tmpentry.literal_code = bashv_long;
    entries.push_back(tmpentry);
}
[Dd]irectory\.[Cc]urrent {
    literal_code_init();
    tmpentry.literal_code = directory_current;
    entries.push_back(tmpentry);
}
[Dd]irectory\.[Bb]asename {
    literal_code_init();
    tmpentry.literal_code = directory_basename;
    entries.push_back(tmpentry);
}
[Tt]his[Cc]ommand\.[Hh]istory {
    literal_code_init();
    tmpentry.literal_code = this_history;
    entries.push_back(tmpentry);
}
[Tt]his[Cc]ommand\.[Nn]umber {
    literal_code_init();
    tmpentry.literal_code = this_number;
    entries.push_back(tmpentry);
}
[Ee]xit[Cc]ode {
    literal_code_init();
    tmpentry.literal_code = exit_code;
    entries.push_back(tmpentry);
}
. {
    printf("Unknown character: line %u, column %u\n",linenumber,colnumber);
    colnumber += yyleng;
}
%%

void literal_code_init(void) {
    colnumber += yyleng;
    tmpentry.inuse = yes;
    tmpentry.deftype = literal_code;
    tmpentry.printable = yes;
}

int main(void) {
    yylex();
    optimize_colors();
    optimize_nonprinting();
    output_sequences();
    return 0;
}

void string_to_lower(char* str) {
    while (*str) {
        *str = tolower(*str);
        str++;
    }
}

void optimize_colors(void) {
    char lastbc = SPEC_BACKGROUND;  /* Let's speculate what it is at start */
    char lastfc = SPEC_FOREGROUND;  /* Let's speculate what it is at start */
    char lastbold = SPEC_BOLD;      /* Let's speculate what it is at start */
    vector <entry> tmpentries;
    vector <entry>::iterator a = entries.begin();
    vector <entry>::iterator b = entries.end();
    /* Erase multiple entries of the same background color.  */
    /* Erase multiple entries of the same foreground color:  */
    /* When "bold" is set to 'undefined' (2), no "0;" or     */
    /* "1;" is displayed - i.e. intensity is preserved, i.e. */
    /* nothing is done. Also, when bold and foreground color */
    /* both preserve, erase foreground color entry.          */
    while (a != b) {
        if ((*a).deftype == color_definition) {
            if ((*a).color_definition.whichcolor == background_color) {
                if ((*a).color_definition.base_color == lastbc) {
                    (*a).inuse = no;
                } else {
                    lastbc = (*a).color_definition.base_color;
                }
            } else {
                if ((*a).color_definition.bold == lastbold) {
                    if ((*a).color_definition.base_color == lastfc) {
                        (*a).inuse = no;
                    } else {
                        (*a).color_definition.bold = UNDEFINED;
                        lastfc = (*a).color_definition.base_color;
                    }
                } else {
/* Bold only is not possible...
                    if ((*a).color_definition.base_color == lastfc) {
                        (*a).color_definition.base_color = UNDEFINED;
                    } else {
                        lastfc = (*a).color_definition.base_color;
                    }
Bold only is not possible... */
                    lastbold = (*a).color_definition.bold;
                }
                /* Now set both bold and base_color to UNDEFINED        */
                /* if and only if the foreground entry can be replaced  */
                /* by a code that restores the old colors. These colors */
                /* are in direct connection to SPEC_ colors and can     */
                /* called by "\e[0m".                                   */
                if ((lastbc==SPEC_BACKGROUND) && ((*a).inuse==yes)) {
                    if (((*a).color_definition.base_color == SPEC_FOREGROUND) &&
                    ((*a).color_definition.bold == SPEC_BOLD)) {
                        (*a).color_definition.base_color = UNDEFINED;
                        (*a).color_definition.bold = UNDEFINED;
                    }
                }
            }
        }
        a++;
    }
}

void optimize_nonprinting(void) {
    entry tmpentry;
    char defined = no;
    vector <entry> tmpentries;
    vector <entry>::iterator a = entries.begin();
    vector <entry>::iterator b = entries.end();
    /* Group sequences of non-printing definitions */
    while (a != b) {
        if (((*a).inuse==yes) && ((*a).printable==no)) {
            if (defined == no) {
                tmpentry.inuse = yes;
                tmpentry.deftype = nonprinting;
                tmpentry.nonprinting = start;
                tmpentries.push_back(tmpentry);
            }
            defined = yes;
        } else {
            if (defined == yes) {
                tmpentry.inuse = yes;
                tmpentry.deftype = nonprinting;
                tmpentry.nonprinting = end;
                tmpentries.push_back(tmpentry);
            }
            defined = no;
        }
        tmpentries.push_back(*a);
        a++;
    }
    if (defined == yes) {
        tmpentry.inuse = yes;
        tmpentry.deftype = nonprinting;
        tmpentry.nonprinting = end;
        tmpentries.push_back(tmpentry);
    }
    entries.clear();
    entries = tmpentries;
}

void output_sequences(void) {
    vector <entry>::iterator a = entries.begin();
    vector <entry>::iterator b = entries.end();
    while (a != b) {
        if ((*a).inuse == yes) {
            switch ((*a).deftype) {
              case nonprinting:
                switch ((*a).nonprinting) {
                  case start:
                    printf("\\[");
                    break;
                  case end:
                    printf("\\]");
                    break;
                }
                break;
              case color_definition:
                switch ((*a).color_definition.whichcolor) {
                  case background_color:
                    printf("\\e[%dm", (*a).color_definition.base_color);
                    break;
                  case foreground_color:
                    printf("\\e[");
                    if ((*a).color_definition.bold != UNDEFINED) {
                        printf("%d;", (*a).color_definition.bold);
                    }
                    if ((*a).color_definition.base_color != UNDEFINED) {
                        printf("%d", (*a).color_definition.base_color);
                    } else {
                        // if no basecolor of foreground, then no bold
                        putchar('0');
                    }
                    putchar('m');
                    break;
                }
                break;
              case literal_text:
                printf("%s", (*a).literal_text);
                break;
              case literal_code:
                switch ((*a).literal_code) {
                  case bell:
                    printf("\\a");
                    break;
                  case date:
                    printf("\\d");
                    break;
                  case hostname_short:
                    printf("\\h");
                    break;
                  case hostname_long:
                    printf("\\H");
                    break;
                  case newline:
                    printf("\\n");
                    break;
                  case carriage_return:
                    printf("\\r");
                    break;
                  case shellname:
                    printf("\\s");
                    break;
                  case time_nonus:
                    printf("\\t");
                    break;
                  case time_us_short:
                    printf("\\T");
                    break;
                  case time_us_long:
                    printf("\\@");
                    break;
                  case current_user:
                    printf("\\u");
                    break;
                  case bashv_short:
                    printf("\\v");
                    break;
                  case bashv_long:
                    printf("\\V");
                    break;
                  case directory_current:
                    printf("\\w");
                    break;
                  case directory_basename:
                    printf("\\W");
                    break;
                  case this_history:
                    printf("\\!");
                    break;
                  case this_number:
                    printf("\\#");
                    break;
                  case exit_code:
                    printf("\\$?");
                    break;
                }
                break;
            }
        }
        a++;
    }
}
