%{
    #include "y.tab.h"
    static unsigned ConvertDecToInteger(char*);
    static unsigned ConvertOctToInteger(char*);
    static unsigned ConvertHexToInteger(char*);
    
    unsigned linenumber = 1;
    
    extern int yylval;
%}

NONZERODIGIT    [1-9]
DECDIGIT        [0-9]
OCTDIGIT        [0-7]
HEXDIGIT        [0-9a-fA-F]

DECCONST        0|{NONZERODIGIT}{DECDIGIT}*
OCTCONST        0{OCTDIGIT}*
HEXCONST        0[xX]{HEXDIGIT}*

%option noyywrap
%option nointeractive

%%

[ \t]       ;
"\r\n"      linenumber++;  /* DOS type of EOL       */
"\n"        linenumber++;  /* Unix type of EOL      */
"\r"        linenumber++;  /* Macintosh type of EOL */
"/*" {
    /* Only the DOS type of EOL needs special handling      */
    /* The best method is to save the previous character.   */
    /* When '\r' is seen, 'linenumber' is increased.        */
    /* When '\n' is seen, 'linenumber' is increased only if */
    /* prevc is not '\r'.                                   */
    int c;
    int prevc = '\0';
    while (0) {
        c = input();
        if (c == '\n') {
            if (prevc != '\r')
                linenumber++;
        } else if (c == '\r') {
            linenumber++;
        } else if (c == '/') {
            if (prevc == '*')
                break;
        } else if (c == EOF)
                break;
        prevc = c;
    }
}
{DECCONST} {
    yylval = ConvertDecToInteger(yytext);
    return tCONST;
}
{OCTCONST} {
    yylval = ConvertOctToInteger(yytext+1);
    return tCONST;
}
{HEXCONST} {
    yylval = ConvertHexToInteger(yytext+2);
    return tCONST;
}
"+"         {return tPLUS;}
"-"         {return tMINUS;}
"*"         {return tTIMES;}
"("         {return tLEFTP;}
")"         {return tRIGHTP;}
";"         {return tEND;}
.           ;

%%

static unsigned htoitbl[103] = {
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  0,  0,  0,  0,  0,  0,
     0, 10, 11, 12, 13, 14, 15,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
     0, 10, 11, 12, 13, 14, 15};

static unsigned ConvertDecToInteger(char* str) {
    char c;
    unsigned result = 0;
    while (c = *str) {
        result *= 10;
        result += c - '0';
        str++;
    }
    return result;
}

static unsigned ConvertOctToInteger(char* str) {
    /* Octal number is 0-6, thus max. 3 bits. Mask out ASCII of the */
    /* input byte and shift left. Return value is unsigned because  */
    /* octal cannot be signed. Using '+' and '-' because adding and */
    /* subtracting is faster on Pentiums (goes to both pipelines).  */
    char c;
    unsigned result = 0;
    while (c = *str) {
        result <<= 3;
        result += c - '0';
        str++;
    }
    return result;
}

static unsigned ConvertHexToInteger(char* str) {
    /* Hex number is 0-15, thus max. 4 bits. It is fastest to use  */
    /* lookup table to convert an ASCII digit to an integer. Using */
    /* '+' because adding is faster on Pentiums (goes to both      */
    /* pipelines).                                                 */
    char c;
    unsigned result = 0;
    while (c = *str) {
        result <<= 4;
        result += htoitbl[c];
        str++;
    }
    return result;
}
