%{
    #include <stdio.h>
    int yylex(void);
    int yyerror(char*);
    
    extern unsigned linenumber;
%}

%token tCONST
%token tPLUS
%token tMINUS
%token tTIMES
%token tLEFTP
%token tRIGHTP
%token tEND

%left tMINUS tPLUS /* Precedence is 2 */
%left tTIMES       /* Precedence is 1 */

%%

input:    input expr tEND {printf("result: %d\n",$2);}
|
;

expr:     expr tPLUS expr     { $$ = $1 + $3; printf("%4d   + %4d -> %4d\n",$1,$3,$$); }
|         expr tMINUS expr    { $$ = $1 - $3; printf("%4d   - %4d -> %4d\n",$1,$3,$$); }
|         expr tTIMES expr    { $$ = $1 * $3; printf("%4d   * %4d -> %4d\n",$1,$3,$$); }
|         tLEFTP expr tRIGHTP { $$ = $2; }
|         tCONST              { $$ = $1; }
;

%%

int yyerror(char* s) {
    fprintf(stderr, "%s\n", s);
    return -1;
}

int main(void) {
    yyparse();
    return 0;
}
