#include <time.h>
#include <stdio.h>
#include <limits.h>
#include "/home/olli/src/algo/randgen/mtcokus.h"


int main(void) {
    MTCOKUS mtc;
    time_t thistime;
    mtcokus_screate(&mtc);
    mtcokus_seed(&mtc,(unsigned)time(&thistime));
    printf("%u\n", (unsigned)((mtcokus_random(&mtc)%USHRT_MAX)+1));
    return 0;
}
