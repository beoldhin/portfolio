#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <net/if.h>
#include <ifaddrs.h>

u_int32_t getipaddr(struct sockaddr *sa);
u_int32_t chariptointip(char *chipaddr);

int main(int argc, char* argv[])
{
    u_int32_t startaddr, endaddr;
    u_int32_t ipaddr, bcast, reqaddr;
    struct ifaddrs *ifap0, *ifap;
    char buf[BUFSIZ];

    if (argc != 2) {
        fprintf(stderr, "Error: Invalid number of arguments.\n");
        return -1;
    }

    memset(buf, 0x00, sizeof(buf));

    if (getifaddrs(&ifap0)) {
        printf("err\n");
        return -1;
    }
    reqaddr = chariptointip(argv[1]);
    for (ifap = ifap0; ifap; ifap=ifap->ifa_next) {
        if (ifap->ifa_addr == NULL)
            continue;
        if (ifap->ifa_addr->sa_family != AF_INET)
            continue;
        ipaddr = getipaddr(ifap->ifa_addr);
        bcast = getipaddr(ifap->ifa_broadaddr);

        startaddr = ipaddr;
        endaddr = (ipaddr + (bcast - ipaddr)) - 1;

        if (reqaddr>=startaddr && reqaddr<=endaddr) {
            puts(ifap->ifa_name);
        }
    }

    freeifaddrs(ifap);

    return 0;
}

u_int32_t getipaddr(struct sockaddr *sa)
{
    int err;
    char host[NI_MAXHOST];

    if (sa == NULL) {
        return;
    }

    switch (sa->sa_family) {
        case AF_INET:
            err = getnameinfo(sa, sizeof(struct sockaddr_in),
                  host, sizeof(host), NULL, 0, NI_NUMERICHOST);
            if (err<0) {
                perror("getnameinfo");
                exit(EXIT_FAILURE);
            }
            break;
/*
        case AF_INET6:
            err = getnameinfo(sa, sizeof(struct sockaddr_in6),
                  host, sizeof(host), NULL, 0, NI_NUMERICHOST);
            if (err<0) {
                perror("getnameinfo");
                exit(EXIT_FAILURE);
            }
            break;
*/
        default:
            return;
    }
    return chariptointip(host);
}

u_int32_t chariptointip(char *chipaddr)
{
    u_int32_t ipaddr;
    unsigned int p1, p2, p3, p4;
    if (sscanf(chipaddr, "%u.%u.%u.%u", &p1, &p2, &p3, &p4) != 4) {
        perror("sscanf");
        exit(EXIT_FAILURE);
    }
    ipaddr = (p1<<24) + (p2<<16) + (p3<<8) + p4;
    return ipaddr;
}
