#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

int main(int argc, char* argv[])
{
    int i;
    char** listp;
    struct hostent* hent;
    if (argc < 3) {
        return 1;
    }
    printf("# %s: ", argv[1]);
    for (i=2; i<argc; i++) {
        if (i > 2) {
            printf(", ");
        }
        printf("%s", argv[i]);
    }
    puts("");
    printf("table <%s> const { ", argv[1]);
    for (i=2; i<argc; i++) {
        if (i > 2) {
            putchar(' ');
        }
        hent = (struct hostent*)gethostbyname(argv[i]);
        if (!hent) {
            printf("ERROR");
            continue;
        }
        for (listp=hent->h_addr_list; *listp; listp++) {
            if (listp != hent->h_addr_list) {
                putchar(',');
            }
            printf("%s", inet_ntoa(*((struct in_addr*)*listp)));
        }
    }
    puts(" }\n");
    return 0;
}
