#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>

int main(int argc, char* argv[])
{
    int i;
    char** listp;
    struct hostent* hent;
    if (argc < 2) {
        return 1;
    }
    if (argc > 2) {
        printf("# %s: %s\n", argv[2], argv[1]);
        printf("%s=\"", argv[2]);
    }
    hent = (struct hostent*)gethostbyname(argv[1]);
    if (!hent) {
        printf("ERROR");
        goto theend;
    }
    for (listp=hent->h_addr_list; *listp; listp++) {
        if (listp != hent->h_addr_list) {
            putchar(',');
        }
        printf("%s", inet_ntoa(*((struct in_addr*)*listp)));
    }
theend:
    if (argc > 2)
        puts("\"");
    puts("");
    return 0;
}
