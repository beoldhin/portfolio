#ifndef _PATHS_H
#define _PATHS_H

#define PATH_ONIRCS     "/home/olli/src/onircs/"
#define PATH_ALGO       "/home/olli/src/algo/"

#define PATH_STDLIBINCS PATH_ONIRCS"stdlibincs.h"
#define PATH_ICMDPROC   PATH_ONIRCS"server/cmdproc/icmdproc.h"
#define PATH_IPARSERS   PATH_ONIRCS"server/cmdproc/iparsers.h"
#define PATH_IRCENG     PATH_ONIRCS"server/engine/irceng.h"
#define PATH_SHARED     PATH_ONIRCS"server/shared/shared.h"
#define PATH_CURSESUI   PATH_ONIRCS"ui/cursesui.h"
#define PATH_OCMDPROC   PATH_ONIRCS"ui/cmdproc/ocmdproc.h"
#define PATH_OPARSERS   PATH_ONIRCS"ui/cmdproc/oparsers.h"

#define PATH_NLIMITS    PATH_ALGO"defines/nlimits.h"
#define PATH_DCLIST     PATH_ALGO"list/dclist.h"
#define PATH_DCHASH     PATH_ALGO"hashtbl/dchash.h"

#endif  /* _PATHS_H */
