#ifndef _LIBINCS_H
#define _LIBINCS_H

#include <ctype.h>
#include <stdio.h>
#include <netdb.h>
#include <panel.h>
#include <curses.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#endif  /* _LIBINCS_H */
