#include "pathdefs.h"
#include PATH_STDLIBINCS
#include PATH_CURSESUI
#include PATH_IRCENG
#include PATH_ICMDPROC


int main(int argc, char* argv[])
{
    INITOPTS initopts;
    
    if (argc==1 || (argc==2 && strcmp(argv[1],"nl")!=0)) {
        fprintf(stderr, 
        "Usage: %s nl            [no listener]\n"
        "       %s <addr> <port> [with listener]\n", argv[0], argv[0]);
        return -1;
    }
    memset(&initopts, 0x00, sizeof(initopts));
    if (argc == 3) {  /* listener requested */
        initopts.engtype = ENGTYPE_CLIENT | ENGTYPE_SERVER;
        initopts.listaddr = argv[1];
        initopts.listport = atoi(argv[2]);
    } else {
        initopts.engtype = ENGTYPE_CLIENT;
    }
    initopts.hashsize = 10000;
    initopts.fromworld = icmdproc_handleincoming;
    initopts.initui = cursesui_initialize;
    irceng_initialize(&initopts);
    
    if (irceng_engine() < 0)
        return -1;
    
    return 0;
}
