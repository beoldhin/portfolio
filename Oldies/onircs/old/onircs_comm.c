#include "onircs_defs.h"


void onircs_privmsg_to_channel(char* chan, char* msg)
{
    if (dchash_find(chanhash,chan+1)) {
        
        puts("FOOBAR");
        
    }
    
}

void onircs_quit_connection(void)
{
    BOOL found;
    DCLIST* listp;
    DCITER* iterp;
    CHNICK* chnick;
    NICKDESC* nickdesc;
    /* check is the closing connection a client */
    found = FALSE;
    if (dclist_iterate_first(&nickdescs,&iterp)) {
        do {
            nickdesc = (NICKDESC*)(iterp+1);
            if (nickdesc->sockfd == curfd) {
                found = TRUE;
                break;
            }
        } while (dclist_iterate_next(&nickdescs,&iterp));
    }
    if (found) {  /* closing connection is a client */
        /* send QUIT to all other nicks on the same channel */
        
        /* TODO */
        
        /* remove nickname from all channels (data structures only) */
        listp = &nickdesc->nickdesc->channels;
        if (dclist_iterate_first(listp,&iterp)) {
            do {
                chnick = *(CHNICK**)(iterp+1);
                dclist_erase_one_at_iter(&chnick->chandef->chnicks, (DCNODE*)(chnick-1));
            } while (dclist_iterate_next(listp,&iterp));
        }
        /* remove nickname from the nick hash */
        iterp = (DCNODE*)nickdesc->nickdesc - 1;
        onircs_clear_one_nickhash(nickdesc->nickdesc);
        iterp->prevp->nextp = iterp->nextp;  /* erase_one_at_iter */
        iterp->nextp->prevp = iterp->prevp;  /* erase_one_at_iter */
        free(iterp);
        /* remove nickname from the nick descriptors */
        iterp = (DCNODE*)nickdesc - 1;
        dclist_erase_one_at_iter(&nickdescs, iterp);
    } else {  /* closing connection is a server */
        /* TODO */
    }
    
    FD_CLR(curfd, &master);
    close(curfd);
}

int onircs_recvln(int sfd, void* buf, size_t len)
{
    int nrecv;
    char* msgp = (char*)buf;
    char* msgep = msgp + len;
    char prevch = '\0';
    while (msgp < msgep) {
        nrecv = recv(sfd, msgp, 1, 0);
        if ((nrecv==0) || (nrecv==-1))
            return nrecv;
        if ((prevch=='\r') && (*msgp=='\n'))
            return (msgp+1) - (char*)buf;
        prevch = *msgp;
        ++msgp;
    }
    return (msgp-1) - (char*)buf;
}

int onircs_sendall(int sfd, void* buf, size_t len)
{
    int nsent;
    int ntotal = 0;
    UINT nleft = len;
    while (ntotal < len) {
        nsent = send(sfd, (char*)buf+ntotal, nleft, 0);
        if (nsent == -1)
            return -1;
        ntotal += nsent;
        nleft -= nsent;
    }
    return ntotal;
}
