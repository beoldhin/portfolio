/* C code produced by gperf version 2.7.2 */
/* Command-line: gperf -Kcommand -t -o onircs_commands.gperf  */
#include "onircs_defs.h"

#define TOTAL_KEYWORDS 39
#define MIN_WORD_LENGTH 3
#define MAX_WORD_LENGTH 8
#define MIN_HASH_VALUE 4
#define MAX_HASH_VALUE 81
/* maximum key range = 78, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (str, len)
     register const char *str;
     register unsigned int len;
{
  static unsigned char asso_values[] =
    {
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 25, 82, 35, 82, 15,
      82, 45,  5, 60, 35, 20, 45, 20, 10,  0,
      10,  0,  0,  0,  5, 25,  0, 30, 82, 50,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82
    };
  return len + asso_values[(unsigned char)str[len - 1]] + asso_values[(unsigned char)str[0]];
}

#ifdef __GNUC__
__inline
#endif
struct gperfcmds *
in_word_set (str, len)
     register const char *str;
     register unsigned int len;
{
  static struct gperfcmds wordlist[] =
    {
      {""}, {""}, {""}, {""},
      {"OPER",       CMD_OPER},
      {"STATS",      CMD_STATS},
      {"SERVER",     CMD_SERVER},
      {""}, {""},
      {"QUIT",       CMD_QUIT},
      {"SQUIT",      CMD_SQUIT},
      {"REHASH",     CMD_REHASH},
      {"RESTART",    CMD_RESTART},
      {""},
      {"PASS",       CMD_PASS},
      {"NAMES",      CMD_NAMES},
      {"SUMMON",     CMD_SUMMON},
      {"VERSION",    CMD_VERSION},
      {""},
      {"PART",       CMD_PART},
      {"ERROR",      CMD_ERROR},
      {""}, {""}, {""},
      {"TIME",       CMD_TIME},
      {"TRACE",      CMD_TRACE},
      {""}, {""}, {""},
      {"USER",       CMD_USER},
      {"USERS",      CMD_USERS},
      {"NOTICE",     CMD_NOTICE},
      {""},
      {"WHO",        CMD_WHO},
      {"NICK",       CMD_NICK},
      {"WHOIS",      CMD_WHOIS},
      {"WHOWAS",     CMD_WHOWAS},
      {"WALLOPS",    CMD_WALLOPS},
      {"USERHOST",   CMD_USERHOST},
      {"MODE",       CMD_MODE},
      {"ADMIN",      CMD_ADMIN},
      {""}, {""}, {""},
      {"KICK",       CMD_KICK},
      {"TOPIC",      CMD_TOPIC},
      {""},
      {"CONNECT",    CMD_CONNECT},
      {""},
      {"JOIN",       CMD_JOIN},
      {"LINKS",      CMD_LINKS},
      {""}, {""}, {""},
      {"LIST",       CMD_LIST},
      {""}, {""}, {""}, {""},
      {"PING",       CMD_PING},
      {""}, {""},
      {"PRIVMSG",    CMD_PRIVMSG},
      {""},
      {"INFO",       CMD_INFO},
      {""}, {""}, {""}, {""},
      {"KILL",       CMD_KILL},
      {""}, {""}, {""}, {""},
      {"ISON",       CMD_ISON},
      {""}, {""}, {""}, {""},
      {"AWAY",       CMD_AWAY},
      {""},
      {"INVITE",     CMD_INVITE}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = wordlist[key].command;

          if (*str == *s && !strcmp (str + 1, s + 1))
            return &wordlist[key];
        }
    }
  return 0;
}
