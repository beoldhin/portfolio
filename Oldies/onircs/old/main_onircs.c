#include "onircs_defs.h"


#define BACKLOG 10
#define HASHLIM 10000


void sigint_handler(int);
void deallocfunc_nickhash(void*);
void deallocfunc_chanhash(void*);
void deallocfunc_exthash(void*);



int main(int argc, char* argv[])
{
    int i;
    int yes;
    int fdmax;
    int newfd;
    int addrlen;
    int listener;
    fd_set temp;
    struct hostent* he;
    struct sockaddr_in myaddr;
    struct sockaddr_in remoteaddr;
    struct sigaction sigint;
    
    if (argc != 3) {
        fprintf(stderr, "Invalid number of arguments\n");
        fprintf(stderr, "Usage: %s <addr> <port>\n", argv[0]);
        return -1;
    }
    
    /* get the listener */
    
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener == -1) {
        perror("socket");
        return -1;
    }
    
    /* lose the pesky "address already in use" error message */
    
    yes = 1;
    if (setsockopt(listener,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
        perror("setsockopt");
        return -1;
    }
    
    /* bind */
    
    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(atoi(argv[2]));
    he = gethostbyname(argv[1]);
    if (!he) {
        perror("gethostbyname");
        return -1;
    }
    myaddr.sin_addr = *((struct in_addr*)he->h_addr);
    memset(&(myaddr.sin_zero), '\0', sizeof(myaddr.sin_zero));
    if (bind(listener,(struct sockaddr*)&myaddr,sizeof(myaddr)) == -1) {
        perror("bind");
        return -1;
    }
    
    /* listen */
    
    if (listen(listener,BACKLOG) == -1) {
        perror("listen");
        return -1;
    }
    
    /* initialize the IRC hash tables, lists */
    
    nickhash = dchash_hcreate(HASHLIM);
    chanhash = dchash_hcreate(HASHLIM);
    exthash = dchash_hcreate(HASHLIM);
    unshash = dchash_hcreate(HASHLIM);
    dclist_screate(&nickdescs);
    dclist_screate(&extdescs);
    
    /* set signal handlers (after setting hashtables, lists) */
    
    sigint.sa_handler = sigint_handler;
    sigemptyset(&sigint.sa_mask);
    sigint.sa_flags = SA_ONESHOT;
    if (sigaction(SIGINT,&sigint,NULL) == -1) {
        perror("sigaction");
        return -1;
    }
    
    /* add the listener to the master set */
    
    FD_ZERO(&master);
    FD_ZERO(&temp);
    FD_SET(listener, &master);
    
    /* keep track of the biggest file descriptor */
    
    fdmax = listener;
    
    while (1) {
        temp = master;
        if (select(fdmax+1,&temp,NULL,NULL,NULL) == -1) {
            perror("select");
            return -1;
        }
        for (i=0; i<=fdmax; ++i) {
            if (FD_ISSET(i,&temp)) {
                curfd = i;
                if (i == listener) {  /* handle new connections */
                    addrlen = sizeof(remoteaddr);
                    newfd = accept(listener, (struct sockaddr*)&remoteaddr, &addrlen);
                    if (newfd != -1) {
                        if (newfd > fdmax)
                            fdmax = newfd;
                        FD_SET(newfd, &master);
                    } else {
                        perror("accept");
                        return -1;
                    }
                } else {  /* handle data from a client */
                    nbytes = onircs_recvln(i, buffer, MAXMSG);
                    if (nbytes > 0) {
                        onircs_process_icommand();
                    } else {
                        if (nbytes != 0) {
                            perror("recv");
                            return -1;
                        }
                        onircs_quit_connection();
                    }
                }
            }
        }
    }
    
    return 0;
}

void sigint_handler(int sig)
{
    puts("SIGINT catched - exiting...");
    dchash_destroy_advanced(nickhash, deallocfunc_nickhash);
    dchash_destroy_advanced(chanhash, deallocfunc_chanhash);
    dchash_destroy_advanced(exthash, deallocfunc_exthash);
    dchash_destroy_nofree(unshash);
    dclist_clear(&nickdescs);
    dclist_clear(&extdescs);
    exit(0);
}

void deallocfunc_nickhash(void* satp)
{
    onircs_clear_one_nickhash(satp);
}

void deallocfunc_chanhash(void* satp)
{
    onircs_clear_one_chanhash(satp);
}

void deallocfunc_exthash(void* satp)
{
    onircs_clear_one_exthash(satp);
}
