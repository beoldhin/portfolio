#include "onircs_defs.h"

ICRET onircs_process_NICK(ICOMMAND*);
ICRET onircs_process_USER(ICOMMAND*);
ICRET onircs_process_SERVER(ICOMMAND*);
ICRET onircs_process_JOIN(ICOMMAND*);
void onircs_process_JOINsub(void);
ICRET onircs_process_PART(ICOMMAND*);
BOOL onircs_process_PARTsub(DCLIST*);
ICRET onircs_process_MODE(ICOMMAND*);
ICRET onircs_process_PRIVMSG(ICOMMAND*);

extern struct gperfcmds* in_word_set(const char* str, unsigned int len);


void onircs_process_icommand(void)
{
    size_t len;
    ICRET icret;
    GPERFCMDS* gperfcmd;
    ICOMMAND* icommand = onircs_parse_icommand();
    
    if (icommand->allok != TRUE) {
        onircs_dealloc_icommand(icommand);
        return;
    }
    
    if (icommand->origtype == ORIG_LOCAL) {
        /* note: the order of commands in the switch clause can be optimized */
        /* further (statistical plausibility based on real world usage) */
        len = strlen(icommand->cmdtype.command);
        if ((gperfcmd=in_word_set(icommand->cmdtype.command,len))) {
            switch (gperfcmd->id) {
              case CMD_NICK:
                icret = onircs_process_NICK(icommand);
                break;
              case CMD_USER:
                icret = onircs_process_USER(icommand);
                break;
              case CMD_SERVER:
                icret = onircs_process_SERVER(icommand);
                break;
              case CMD_JOIN:
                icret = onircs_process_JOIN(icommand);
                break;
              case CMD_PART:
                icret = onircs_process_PART(icommand);
                break;
              case CMD_MODE:
                icret = onircs_process_MODE(icommand);
                break;
              case CMD_PRIVMSG:
                icret = onircs_process_PRIVMSG(icommand);
                break;
            }
        }
    } else {
        
        
        
    }
    
    onircs_dealloc_icommand(icommand);
    
}

/*
 * Numeric replies for NICK
 * ERR_NONICKNAMEGIVEN
 * ERR_NICKNAMEINUSE
 * ERR_ERRONEUSNICKNAME
 * ERR_NICKCOLLISION
 */
ICRET onircs_process_NICK(ICOMMAND* icommand)
{
    /* NICK <nickname> [ <hopcount> ] */
    NICKHASH* p;
    PARMDEF* parmdef;
    NICKDESC tmpdesc;
    NICKHASH tmpnick;
    NICKHASH* foundp;
    /* if NICK is from a client (detect), <hopcount> must not exist */
    if (icommand->paramsn != 1) {
        
        /* TODO: ERR_NONICKNAMEGIVEN to client */
        
        return ICRET_INVPARMS;
    }
    parmdef = (PARMDEF*)(icommand->params.nextp+1);
    if ((foundp=dchash_find(nickhash,parmdef->param))) {
        if (foundp->sockfd != curfd) {  /* not his own sock */
            
            /* TODO: ERR_NICKNAMEINUSE to client */
            
        } else {  /* his own sock, do nothing */
        }
    } else {  /* new nick/sock */
        strncpy(tmpnick.nickname, parmdef->param, MAXNICK);
        tmpnick.sockfd = curfd;
        dclist_screate(&tmpnick.channels);
        p = dchash_insert_noalloc(nickhash, tmpnick.nickname, &tmpdesc, sizeof(tmpdesc));
        tmpdesc.sockfd = curfd;
        tmpdesc.nickdesc = p;
        dclist_push_back(&nickdescs, &tmpdesc, sizeof(tmpdesc));
        
        /* TODO: NICK to other servers */
        
    }
    return ICRET_NOERR;
}

/*
 * Numeric replies for USER
 * ERR_NEEDMOREPARAMS
 * ERR_ALREADYREGISTRED
 */
ICRET onircs_process_USER(ICOMMAND* icommand)
{
    /* USER <username> <hostname> <servername> <realname> */
    if (icommand->paramsn < 4) {
        
        /* TODO: ERR_NEEDMOREPARAMS to client */
        
        return ICRET_INVPARMS;
    }
    return ICRET_NOERR;
}

/*
 * Numeric replies for SERVER
 * ERR_ALREADYREGISTRED
 */
ICRET onircs_process_SERVER(ICOMMAND* icommand)
{
    /* SERVER <servername> <hopcount> <info> */
    return ICRET_NOERR;
}

/*
 * Numeric replies for JOIN
 * ERR_NEEDMOREPARAMS
 * ERR_INVITEONLYCHAN
 * ERR_CHANNELISFULL
 * ERR_NOSUCHCHANNEL
 * ERR_BANNEDFROMCHAN
 * ERR_BADCHANNELKEY
 * ERR_BADCHANMASK
 * ERR_TOOMANYCHANNELS
 * RPL_TOPIC
 */
ICRET onircs_process_JOIN(ICOMMAND* icommand)
{
    BOOL iskey;
    DCNODE* chiterp;
    DCNODE* keiterp;
    DCLIST* chlistp;
    DCLIST* kelistp;
    SINGLESEP* keys;
    SINGLESEP* chans;
    PARMDEF* parm1def;
    PARMDEF* parm2def;
    /* JOIN <channel>{,<channel>} [<key>{,<key>}] */
    if (icommand->paramsn < 1) {
        
        /* TODO: ERR_NEEDMOREPARAMS to client */
        
    }
    parm1def = (PARMDEF*)(icommand->params.nextp+1);
    parm2def = (PARMDEF*)(icommand->params.nextp->nextp+1);
    chans = onircs_parse_singlesepstr(parm1def->param, ',');
    keys = onircs_parse_singlesepstr(parm2def->param, ',');
    
    chlistp = &chans->params;
    kelistp = &keys->params;
    iskey = dclist_iterate_first(kelistp,&keiterp);
    if (dclist_iterate_first(chlistp,&chiterp)) {
        do {
            
            
            
            
            
            if (iskey) {
                iskey = dclist_iterate_next(kelistp, &keiterp);
            }
        } while (dclist_iterate_next(chlistp,&chiterp));
    }
    
    onircs_dealloc_singlesep(keys);
    onircs_dealloc_singlesep(chans);
    return ICRET_NOERR;
}

void onircs_process_JOINsub(void)
{
    
    
    
}

/* Numeric replies for PART
 * ERR_NEEDMOREPARAMS
 * ERR_NOTONCHANNEL
 * ERR_NOSUCHCHANNEL
 */
ICRET onircs_process_PART(ICOMMAND* icommand)
{
    char* chname;
    DCNODE* chiterp;
    DCLIST* chlistp;
    SINGLESEP* chans;
    CHANHASH* foundp;
    PARMDEF* parmdef;
    /* PART <channel>{,<channel>} */
    if (icommand->paramsn < 1) {
        
        /* TODO: ERR_NEEDMOREPARAMS to client */
        
        return ICRET_INVPARMS;
    }
    parmdef = (PARMDEF*)(icommand->params.nextp+1);
    chans = onircs_parse_singlesepstr(parmdef->param, ',');
    chlistp = &chans->params;
    if (dclist_iterate_first(chlistp,&chiterp)) {
        do {
            chname = (char*)(chiterp+1);
            if (!(foundp=dchash_find(chanhash,chname))) {
                
                /* TODO: ERR_NOSUCHCHANNEL to client */
                
            } else {
                
                if (!onircs_process_PARTsub(&foundp->chnicks)) {
                    
                    /* TODO: ERR_NOTONCHANNEL to client */
                    
                }
                
            }
            
        } while (dclist_iterate_next(chlistp,&chiterp));
    }
    onircs_dealloc_singlesep(chans);
    return ICRET_NOERR;
}

BOOL onircs_process_PARTsub(DCLIST* listp)
{
    DCNODE* iterp;
    CHNICK* chnick;
    if (dclist_iterate_first(listp,&iterp)) {
        do {
            chnick = (CHNICK*)(iterp+1);
            if (chnick->nickdef->sockfd == curfd) {
                dclist_erase_one_at_iter(listp, iterp);
                return TRUE;
            }
        } while (dclist_iterate_next(listp,&iterp));
    }
    return FALSE;
}

/*
 * Numeric replies for MODE:
 * ERR_NEEDMOREPARAMS
 * ERR_CHANOPRIVSNEEDED
 * ERR_NOTONCHANNEL
 * ERR_UNKNOWNMODE
 * ERR_USERSDONTMATCH
 * ERR_UMODEUNKNOWNFLAG
 * ERR_NOSUCHNICK
 * ERR_KEYSET
 * ERR_ENDOFBANLIST
 * ERR_NOSUCHCHANNEL
 * RPL_BANLIST
 * RPL_CHANNELMODEIS
 * RPL_ENDOFBANLIST
 * RPL_UMODEIS
 */
ICRET onircs_process_MODE(ICOMMAND* icommand)
{
    /* MODE <channel> {[+|-]|o|p|s|i|t|n|b|v} [<limit>] [<user>] [<banmask>] */
    return ICRET_NOERR;
}

/*
 * Numeric replies for PRIVMSG:
 * ERR_NORECIPIENT
 * ERR_CANNOTSENDTOCHAN
 * ERR_WILDTOPLEVEL
 * ERR_NOSUCHNICK
 * ERR_NOTEXTTOSEND
 * ERR_NOTOPLEVEL
 * ERR_TOOMANYTARGETS
 * RPL_AWAY
 */
ICRET onircs_process_PRIVMSG(ICOMMAND* icommand)
{
    char* recvr;
    DCITER* iterp;
    DCLIST* listp;
    SINGLESEP* recvs;
    PARMDEF* parm1def;
    PARMDEF* parm2def;
    /* PRIVMSG <receiver>{,<receiver>} <text to be sent> */
    if (icommand->paramsn < 2) {
        
        /* TODO: ERR_NEEDMOREPARAMS to client */
        
        return ICRET_INVPARMS;
    }
    parm1def = (PARMDEF*)(icommand->params.nextp+1);
    parm2def = (PARMDEF*)(icommand->params.nextp->nextp+1);
    recvs = onircs_parse_singlesepstr(parm1def->param, ',');
    listp = &recvs->params;
    if (dclist_iterate_first(listp,&iterp)) {
        do {
            recvr = *(char**)(iterp+1);
            if ((recvr[0]=='#') || (recvr[0]=='&'))
                onircs_privmsg_to_channel(recvr, parm2def->param);
        } while (dclist_iterate_next(listp,&iterp));
    }
    onircs_dealloc_singlesep(recvs);
    return ICRET_NOERR;
}
