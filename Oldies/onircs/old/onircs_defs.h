#ifndef _ONIRCS_DEFS_H
#define _ONIRCS_DEFS_H

#include <ctype.h>
#include <netdb.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/algo/list/dclist.h"
#include "/home/olli/src/algo/hashtbl/dchash.h"

#define CHMODE_PRIVATE      0x0001
#define CHMODE_SECRET       0x0002
#define CHMODE_INVITEONLY   0x0004
#define CHMODE_ONLYOPTOPIC  0x0008
#define CHMODE_NOEXTMSGS    0x0010
#define CHMODE_MODERATED    0x0020

#define UNKLEN      510
#define MAXMSG      510
#define MAXCHAN     200
#define MAXHOST     64
#define MAXNICK     9
#define MAXUSER     8
#define MAXCMD      8  /* the command USERHOST */


/* ========================================================================== */

typedef enum icret {
    ICRET_INVPARMS,
    ICRET_NOERR
} ICRET;

/* ========================================================================== */

typedef struct gperfcmds {
    char* command;
    int id;
} GPERFCMDS;

enum commands {
    CMD_PASS, CMD_NICK, CMD_USER, CMD_SERVER, CMD_OPER, CMD_QUIT, CMD_SQUIT,
    CMD_JOIN, CMD_PART, CMD_MODE, CMD_TOPIC, CMD_NAMES, CMD_LIST, CMD_INVITE,
    CMD_KICK, CMD_VERSION, CMD_STATS, CMD_LINKS, CMD_TIME, CMD_CONNECT,
    CMD_TRACE, CMD_ADMIN, CMD_INFO, CMD_PRIVMSG, CMD_NOTICE, CMD_WHO, CMD_WHOIS,
    CMD_WHOWAS, CMD_KILL, CMD_PING, CMD_ERROR, CMD_AWAY, CMD_REHASH,
    CMD_RESTART, CMD_SUMMON, CMD_USERS, CMD_WALLOPS, CMD_USERHOST, CMD_ISON
};

/* ========================================================================== */

typedef struct singlesep {
    int paramsn;
    DCLIST params;
} SINGLESEP;

typedef enum origtype {
    ORIG_GLOBAL,
    ORIG_LOCAL
} ORIGTYPE;

typedef enum iparmtype {
    PARM_SINGLE,
    PARM_LIST
} IPARMTYPE;

typedef struct parmdef {
    char* param;
    IPARMTYPE parmtype;
} PARMDEF;

typedef struct icommand {
    BOOL allok;
    BOOL isreply;                /* if command was numeric, then reply is from server */
    ORIGTYPE origtype;           /* if first char is a ':', then command from global */
    char srvonck[MAXHOST+1];     /* servername or nickname (MAXHOST>MAXNICK) */
    char username[MAXUSER+1];
    char hostname[MAXHOST+1];    /* exists only if username exists */
    union CMDTYPE {
        char command[MAXCMD+1];
        int servcmd;             /* command from a server only */
    } cmdtype;
    int paramsn;                 /* -1 = no list, 0 = empty list with one sentinel */
    DCLIST params;
} ICOMMAND;

/* ========================================================================== */

typedef enum chstate {
    CHSTATE_GLOBALCANJOIN,  /* '#' */
    CHSTATE_LOCALCANJOIN    /* '&' */
} CHSTATE;

typedef struct nickhash {
    char nickname[MAXNICK+1];
    char hostname[MAXHOST+1];
    DCLIST channels;  /* list of pointers to CHNICKs */
    int sockfd;
} NICKHASH;

typedef struct chanhash {
    char channame[MAXCHAN+1];
    char password[UNKLEN+1];
    char topic[UNKLEN+1];
    CHSTATE chstate;
    DCLIST chnicks;  /* list of CHNICKs */
} CHANHASH;
        /* || */
        /* || */
        /* vv */
        typedef struct chnick {
            CHANHASH* chandef;  /* upper level */
            NICKHASH* nickdef;
            char chmodes;
        } CHNICK;

typedef struct unshash {  /* user+server ("a@b"), used with PRIVMSG, created with USER */
    char unsname[MAXUSER+1+MAXHOST+1];
    NICKHASH* unshash;
} UNSHASH;

typedef struct exthash {  /* external servers connected to this server */
    char servname[MAXHOST+1];
    char info[UNKLEN+1];
    int hopcount;
    int sockfd;
} EXTHASH;

typedef struct nickdesc {  /* used to find nickhash slot by sockfd */
    int sockfd;
    NICKHASH* nickdesc;
} NICKDESC;

typedef struct extdesc {  /* used to find exthash slot by sockfd */
    int sockfd;
    EXTHASH* extdesc;
} EXTDESC;

/* ========================================================================== */

void onircs_privmsg_to_channel(char*, char*);
void onircs_quit_connection(void);
int onircs_recvln(int, void*, size_t);
int onircs_sendall(int, void*, size_t);

void onircs_clear_one_nickhash(NICKHASH*);
void onircs_clear_one_chanhash(CHANHASH*);
void onircs_clear_one_exthash(EXTHASH*);

void onircs_process_icommand(void);

ICOMMAND* onircs_parse_icommand(void);
SINGLESEP* onircs_parse_singlesepstr(char*, char);
void onircs_dealloc_icommand(ICOMMAND*);
void onircs_dealloc_singlesep(SINGLESEP*);

/* ========================================================================== */

DCHASH* nickhash;  /* hash of NICKHASH */
DCHASH* chanhash;  /* hash of CHANHASH */
DCHASH* exthash;   /* hash of EXTHASH */
DCHASH* unshash;   /* hash of UNSHASH */
DCLIST nickdescs;  /* list of NICKDESC */
DCLIST extdescs;   /* list of EXTDESC */

int curfd;
int nbytes;
fd_set master;
char buffer[MAXMSG];

#endif  /* _ONIRCS_DEFS_H */
