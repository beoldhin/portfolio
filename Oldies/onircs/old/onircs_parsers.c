#include "onircs_defs.h"

void deallocfunc_icommand(void*);
void deallocfunc_singlesep(void*);


ICOMMAND* onircs_parse_icommand(void)
{
    size_t n;
    int numeric;
    char* curp;
    char* endp;
    char* savedp;
    PARMDEF parmdef;
    char tmpbuf[MAXMSG+1];
    ICOMMAND* icommand = malloc(sizeof(ICOMMAND));
    icommand->allok = FALSE;
    icommand->srvonck[0] = '\0';
    icommand->username[0] = '\0';
    icommand->hostname[0] = '\0';
    icommand->cmdtype.command[0] = '\0';
    icommand->cmdtype.servcmd = 0;
    icommand->paramsn = 0;
    dclist_screate(&icommand->params);
    endp = buffer + nbytes;
    /* scan for starting character */
    for (curp=buffer; curp<endp; ++curp) {
        if (isspace(*curp) == 0)
            break;
    }
    /* if no starting character, quit immediately */
    if (curp == endp)
        return icommand;
    /* if ':', prefix must follow (and optional '!') */
    icommand->origtype = ORIG_LOCAL;
    if (*curp == ':') {
        icommand->origtype = ORIG_GLOBAL;
        /* scan for starting prefix */
        for (++curp; curp<endp; ++curp) {
            if (isspace(*curp) == 0)
                break;
        }
        /* if no starting prefix, quit immediately */
        if (curp == endp)
            return icommand;
        for (savedp=curp; curp<endp; ++curp) {
            if ((isspace(*curp)!=0) || (*curp=='!'))
                break;
        }
        n=curp-savedp; if(n>MAXHOST) {savedp=curp+MAXHOST; n=MAXHOST;}
        icommand->srvonck[n] = '\0';
        memcpy(icommand->srvonck, savedp, n);
        /* if nothing more, quit immediately */
        if (curp == endp)
            return icommand;
        if (*curp == '!') {
            /* scan for starting user */
            for (++curp; curp<endp; ++curp) {
                if (isspace(*curp) == 0)
                    break;
            }
            /* if no starting user, quit immediately */
            if (curp == endp)
                return icommand;
            for (savedp=curp; curp<endp; ++curp) {
                if ((isspace(*curp)!=0) || (*curp=='@'))
                    break;
            }
            n=curp-savedp; if(n>MAXUSER) {savedp=curp+MAXUSER; n=MAXUSER;}
            icommand->username[n] = '\0';
            memcpy(icommand->username, savedp, n);
            if ((curp<endp) && (*curp=='@')) {
                /* scan for starting host */
                for (++curp; curp<endp; ++curp) {
                    if (isspace(*curp) == 0)
                        break;
                }
                /* if no starting host, quit immediately */
                if (curp == endp)
                    return icommand;
                for (savedp=curp; curp<endp; ++curp) {
                    if (isspace(*curp) != 0)
                        break;
                }
                n=curp-savedp; if(n>MAXHOST) {savedp=curp+MAXHOST; n=MAXHOST;}
                icommand->hostname[n] = '\0';
                memcpy(icommand->hostname, savedp, n);
            }
            /* if no command, quit immediately */
            if (curp == endp)
                return icommand;
        }
    }
    /* now the code below is only executed if there really exist a command... */
    /* command can be numeric only if all are nums, but alpha can contain nums too... */
    numeric = 0;
    for (savedp=curp; curp<endp; ++curp) {
        if (isspace(*curp) != 0)
            break;
        if (isdigit(*curp) != 0)
            ++numeric;
    }
    if (curp-savedp == numeric) {  /* numeric */
        icommand->isreply = TRUE;
        memcpy(tmpbuf, savedp, curp-savedp);
        tmpbuf[curp-savedp] = '\0';
        icommand->cmdtype.servcmd = atoi(tmpbuf);
    } else {  /* alpha */
        n=curp-savedp; if(n>MAXCMD) {savedp=curp+MAXCMD; n=MAXCMD;}
        icommand->isreply = FALSE;
        icommand->cmdtype.command[n] = '\0';
        memcpy(icommand->cmdtype.command, savedp, n);
    }
    while (curp < endp) {
        /* scan for starting param */
        for (++curp; curp<endp; ++curp) {
            if (isspace(*curp) == 0)
                break;
        }
        /* if no starting param, break (no error) */
        if (curp == endp)
            break;
        if (*curp == ':') {
            /* if no param, quit immediately */
            ++curp;
            if (curp == endp)
                return icommand;
            for (savedp=curp; curp<endp; ++curp) {
                if ((*curp==0x0D) || (*curp==0x0A))
                    break;
            }
            parmdef.parmtype = PARM_LIST;
        } else {
            for (savedp=curp; curp<endp; ++curp) {
                if (isspace(*curp) != 0)
                    break;
            }
            parmdef.parmtype = PARM_SINGLE;
        }
        parmdef.param = malloc(curp-savedp+1);
        parmdef.param[curp-savedp] = '\0';
        memcpy(parmdef.param, savedp, curp-savedp);
        dclist_push_back(&icommand->params, &parmdef, sizeof(PARMDEF));
        ++icommand->paramsn;
    }
    icommand->allok = TRUE;
    return icommand;
}

SINGLESEP* onircs_parse_singlesepstr(char* strp, char sep)
{
    char* newstr;
    char* savedp = strp;
    SINGLESEP* singlesep = malloc(sizeof(SINGLESEP));
    dclist_screate(&singlesep->params);
    singlesep->paramsn = 0;
    while (*strp != '\0') {
        if (*strp==sep && strp!=savedp) {
            newstr = malloc(strp-savedp+1);
            memcpy(newstr, savedp, strp-savedp);
            newstr[strp-savedp] = '\0';
            dclist_push_back(&singlesep->params, &newstr, sizeof(newstr));
            ++singlesep->paramsn;
            savedp = strp + 1;
        }
        ++strp;
    }
    if (strp != savedp) {
        newstr = malloc(strp-savedp+1);
        memcpy(newstr, savedp, strp-savedp);
        newstr[strp-savedp] = '\0';
        dclist_push_back(&singlesep->params, &newstr, sizeof(newstr));
        ++singlesep->paramsn;
    }
    return singlesep;
}

void onircs_dealloc_icommand(ICOMMAND* icommand)
{
    icommand->srvonck[0] = '\0';
    icommand->username[0] = '\0';
    icommand->hostname[0] = '\0';
    icommand->cmdtype.command[0] = '\0';
    icommand->paramsn = 0;
    dclist_clear_advanced(&icommand->params, deallocfunc_icommand);
}

void onircs_dealloc_singlesep(SINGLESEP* singlesep)
{
    dclist_clear_advanced(&singlesep->params, deallocfunc_singlesep);
    free(singlesep);
}

void deallocfunc_icommand(void* satp)
{
    PARMDEF* parmdef = (PARMDEF*)satp;
    free(parmdef->param);
}

void deallocfunc_singlesep(void* satp)
{
    free(*(char**)satp);
}
