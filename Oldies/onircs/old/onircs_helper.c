#include "onircs_defs.h"


void onircs_clear_one_nickhash(NICKHASH* nickhp)
{
    nickhp->nickname[0] = '\0';
    nickhp->hostname[0] = '\0';
    dclist_clear(&nickhp->channels);
}

void onircs_clear_one_chanhash(CHANHASH* chanhp)
{
    chanhp->channame[0] = '\0';
    chanhp->password[0] = '\0';
    chanhp->topic[0] = '\0';
    dclist_clear(&chanhp->chnicks);
}

void onircs_clear_one_exthash(EXTHASH* exthp)
{
    exthp->servname[0] = '\0';
    exthp->info[0] = '\0';
}
