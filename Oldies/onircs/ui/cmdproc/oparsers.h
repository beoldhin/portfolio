#ifndef _OPARSERS_H
#define _OPARSERS_H

#include "pathdefs.h"
#include PATH_SHARED
#include PATH_NLIMITS

typedef struct ocommand {
    BOOL allok;
    char command[MAXCMD+1];
    UINT cmdlen;
    int paramsn;
    DCLIST params;
} OCOMMAND;

OCOMMAND* ocmdproc_parse_outgoing(char*, UINT);
void ocmdproc_dealloc_ocommand(OCOMMAND*);

#endif  /* _OPARSERS_H */
