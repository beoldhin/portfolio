#ifndef _OCMDPROC_H
#define _OCMDPROC_H

#include "pathdefs.h"
#include PATH_OPARSERS

typedef struct gperfocmds {
    char* ocommand;
    int id;
} GPERFOCMDS;

typedef enum ocommands {
    OCMD_MSG,
    _OCMD_TOTAL
} OCOMMANDS;

void ocmdproc_initialize(void);
void ocmdproc_handleoutgoing(char*, UINT);

#endif  /* _OCMDPROC_H */
