#include "pathdefs.h"
#include PATH_OPARSERS


OCOMMAND* ocmdproc_parse_outgoing(char* keybuf, UINT keybufi)
{
    UINT n;
    char* curp;
    char* endp;
    char* savedp;
    PARMDEF parmdef;
    OCOMMAND* ocommand = malloc(sizeof(OCOMMAND));
    ocommand->allok = FALSE;
    ocommand->command[0] = '\0';
    ocommand->paramsn = 0;
    dclist_screate(&ocommand->params);
    curp = keybuf;
    /* check for starting '/' */
    if (*curp != '/') {
        return ocommand;
    }
    /* check for starting command */
    curp++;
    if (!isalpha(*curp)) {
        return ocommand;
    }
    /* scan for end of command */
    endp = keybuf + keybufi;
    for (savedp=curp; curp<endp; curp++) {
        if (isspace(*curp))
            break;
    }
    LIMITBUF(sizeof(ocommand->command)-1);
    ocommand->command[n] = '\0';
    ocommand->cmdlen = n;
    memcpy(ocommand->command, savedp, n);
    while (curp < endp) {
        /* scan for starting param */
        for (curp++; curp<endp; curp++) {
            if (!isspace(*curp))
                break;
        }
        /* if no starting param, break (no error) */
        if (*curp == ':') {
            /* if no param, quit immediately */
            curp++;
            if (curp == endp)
                return ocommand;
            for (savedp=curp; curp<endp; curp++) {
                if (*curp==0x0D || *curp==0x0A)
                    break;
            }
            parmdef.parmtype = PARM_LIST;
        } else {
            for (savedp=curp; curp<endp; curp++) {
                if (isspace(*curp))
                    break;
            }
            parmdef.parmtype = PARM_SINGLE;
        }
        LIMITBUF(sizeof(parmdef.param)-1);
        parmdef.parmlen = n;
        parmdef.param[n] = '\0';
        memcpy(parmdef.param, savedp, n);
        dclist_push_back(&ocommand->params, &parmdef, sizeof(PARMDEF));
        ocommand->paramsn++;
    }
    ocommand->allok = TRUE;
    return ocommand;
}

void ocmdproc_dealloc_ocommand(OCOMMAND* ocommand)
{
    ocommand->command[0] = '\0';
    ocommand->paramsn = 0;
    dclist_clear(&ocommand->params);
}
