#include "pathdefs.h"
#include PATH_OCMDPROC

/* interface functions -- begin */
void ocmdproc_process_msg(OCOMMAND*);
/* interface functions -- end */

static void (*oprocfuncs[_OCMD_TOTAL])(OCOMMAND*);
extern struct gperfocmds* in_oword_set(const char* str, unsigned int len);


void ocmdproc_initialize(void)
{
    memset(oprocfuncs, 0x00, sizeof(oprocfuncs));
    oprocfuncs[OCMD_MSG] = ocmdproc_process_msg;
}

void ocmdproc_handleoutgoing(char* keybuf, UINT keybufi)
{
    UINT len;
    GPERFOCMDS* gperfocmd;
    OCOMMAND* ocommand = ocmdproc_parse_outgoing(keybuf, keybufi);
    
    if (ocommand->allok != TRUE) {
        ocmdproc_dealloc_ocommand(ocommand);
        return;
    }
    
    /* too good to be true - O(1) performance for processing (procfuncs) :) */
    len = ocommand->cmdlen;
    if ((gperfocmd=in_oword_set(ocommand->command,len))) {
        if (oprocfuncs[gperfocmd->id]) {
            oprocfuncs[gperfocmd->id](ocommand);
        }
    }
    
    ocmdproc_dealloc_ocommand(ocommand);
}

/* interface functions -- begin */

void ocmdproc_process_msg(OCOMMAND* ocommand)
{
    
    
    
}

/* interface functions -- end */
