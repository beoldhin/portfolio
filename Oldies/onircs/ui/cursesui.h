#ifndef _CURSESUI_H
#define _CURSESUI_H

#include "pathdefs.h"
#include PATH_STDLIBINCS
#include PATH_SHARED
#include PATH_OCMDPROC

typedef enum pnltype {
    PNLTYPE_STATUS,
    PNLTYPE_SERVER,
    PNLTYPE_CHANNEL,
    PNLTYPE_PRIVATE
} PNLTYPE;

typedef struct pnl {
    PNLTYPE pnltype;
    WINDOW* window;
    PANEL* panel;
} PNL;

void cursesui_initialize(void);

#endif  /* _CURSESUI_H */
