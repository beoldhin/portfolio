#include "pathdefs.h"
#include PATH_CURSESUI

/* interface functions -- begin */
void cursesui_setup(void);
void cursesui_handlekb_incoming(void);
void cursesui_quit(void);
/* interface functions -- end */
DCLIST* cursesui_showtitle(void);

static UINT panelsi;
static UINT panelsn;
static DCLIST panels;
static WINDOW* curwin;
static WINDOW* titlewin;
static WINDOW* statwin;
static WINDOW* cmdwin;

static UINT keybufi;
static char keybuf[MAXMSG+1];  /* MAXMSG not necessary */


void cursesui_initialize(void)
{
    memset(ifaceui, 0x00, sizeof(ifaceui));
    ifaceui[IFACEUI_SETUP] = cursesui_setup;
    ifaceui[IFACEUI_HANDLEKB] = cursesui_handlekb_incoming;
    ifaceui[IFACEUI_QUIT] = cursesui_quit;
    ocmdproc_initialize();
}

/* interface functions -- begin */

void cursesui_setup(void)
{
    PNL pnl;
    char ecmd[] = "\n-- ready --";
    panelsi = 0;
    panelsn = 1;
    initscr();
    noecho();
    /* raw(); */
    curs_set(0);
    dclist_screate(&panels);
    pnl.pnltype = PNLTYPE_STATUS;
    titlewin = newwin(1, COLS, 0, 0);
    pnl.window = newwin(LINES-2, COLS, 1, 0);
    cmdwin = newwin(1, COLS, LINES-1, 0);
    pnl.panel = new_panel(pnl.window);
    dclist_push_back(&panels, &pnl, sizeof(pnl));
    wbkgd(titlewin, A_REVERSE);
    wbkgd(pnl.window, A_NORMAL);
    wbkgd(cmdwin, A_REVERSE);
    scrollok(titlewin, FALSE);
    scrollok(pnl.window, TRUE);
    scrollok(cmdwin, TRUE);
    keypad(cmdwin, TRUE);
    wclear(titlewin);
    wclear(pnl.window);
    wclear(cmdwin);
    wprintw(pnl.window, ecmd);
    update_panels();
    doupdate();
    wrefresh(titlewin);
    wrefresh(cmdwin);
    statwin = pnl.window;
    curwin = pnl.window;
    cursesui_showtitle();
}

void cursesui_handlekb_incoming(void)
{
    int readkey;
    DCLIST* iterp;
    wgetch(cmdwin);
    if (readkey == '\n') {
        werase(cmdwin);
        wrefresh(cmdwin);
        keybuf[keybufi] = '\0';
        ocmdproc_handleoutgoing(keybuf, keybufi);
        keybufi = 0;
        return;
    }
    if ((readkey==KEY_BACKSPACE) || (readkey==0x7F)) {
        if (keybufi <= 0)
            return;
        keybufi--;
        wmove(cmdwin, 0, keybufi);
        wechochar(cmdwin, ' ');
        wmove(cmdwin, 0, keybufi);
        wrefresh(cmdwin);
        return;
    }
    if (readkey == '\t') {
        panelsi++;
        if (panelsi == panelsn)
            panelsi = 0;
        iterp = cursesui_showtitle();
        curwin = ((PNL*)(iterp+1))->window;
        top_panel(((PNL*)(iterp+1))->panel);
        update_panels();
        doupdate();
        wrefresh(cmdwin);
        return;
    }
    if (readkey == KEY_UP) {
        /* TODO: command history previous */
        return;
    }
    if (readkey == KEY_RIGHT) {
        /* TODO: move cursor right */
        return;
    }
    if (readkey == KEY_DOWN) {
        /* TODO: command history next */
        return;
    }
    if (readkey == KEY_LEFT) {
        /* TODO: move cursor left */
        return;
    }
    wechochar(cmdwin, readkey);
    keybuf[keybufi] = readkey;
    keybufi++;
}

void cursesui_quit(void)
{
    endwin();
}

/* interface functions -- end */

DCLIST* cursesui_showtitle(void)
{
    DCLIST* iterp;
    return iterp;
}
