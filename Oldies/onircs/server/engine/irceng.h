#ifndef _IRCENG_H
#define _IRCENG_H

#include "pathdefs.h"
#include PATH_STDLIBINCS
#include PATH_CURSESUI
#include PATH_SHARED
#include PATH_ICMDPROC
#include PATH_NLIMITS

/*
 * ENGTYPE_CLIENT: passes IRC commands from sock 0 (keyboard) to
 * command processor
 * ENGTYPE_SERVER: opens a listener socket on init - also processes
 * admin server commands
 */

#define ENGTYPE_CLIENT 0x01  /* 00000001 */
#define ENGTYPE_SERVER 0x02  /* 00000010 */

typedef struct initopts {
    char engtype;
    char* listaddr;  /* listener's address */
    int listport;    /* listener's port */
    UINT hashsize;
    void (*fromworld)(char*, int, int);
    void (*initui)(void);
} INITOPTS;

void irceng_initialize(INITOPTS*);
int irceng_engine(void);
int irceng_recvln(int, void*, size_t);


#endif  /* _IRCENG_H */
