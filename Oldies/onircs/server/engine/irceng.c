#include "pathdefs.h"
#include PATH_IRCENG

#define STDIN   0   /* keyboard */
#define BACKLOG 10

void irceng_siginthandler(int);
void deallocfunc_nickhash(void*);
void deallocfunc_chanhash(void*);
void deallocfunc_exthash(void*);
void irceng_quit_connection(void);

static char engtype;
static char* listaddr;
static int listport;
static UINT hashsize;
static void (*fromfunc)(char*, int, int);
static void (*initui)(void);

static int curfd;
static int nbytes;
static fd_set master;
static char buffer[MAXMSG+1];


/*
 * Takes two functions as parameters:
 * - fromworld: messages from the world are passed to this (handled externally)
 */
void irceng_initialize(INITOPTS* initopts)
{
    engtype = initopts->engtype;
    listaddr = initopts->listaddr;
    listport = initopts->listport;
    hashsize = initopts->hashsize;
    fromfunc = initopts->fromworld;
    initui = initopts->initui;
}

int irceng_engine(void)
{
    int i;
    int yes;
    int fdmax = -1;
    int newfd;
    int addrlen;
    int listener = -1;
    struct hostent* he;
    struct sockaddr_in myaddr;
    struct sockaddr_in remoteaddr;
    struct sigaction sigint;
    fd_set temp;
    
    /* initialize User Interface */
    if (initui) {
        initui();
    } else {
        memset(ifaceui, 0x00, sizeof(ifaceui));
    }
    UICALL(IFACEUI_SETUP);
    /* create listener */
    if (!(engtype & ENGTYPE_SERVER))
        goto noserver;
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener == -1) {
        perror("socket");
        return -1;
    }
    /* lose the pesky "address already in use" error message */
    yes = 1;
    if (setsockopt(listener,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
        perror("setsockopt");
        return -1;
    }
    /* bind */
    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(listport);
    he = gethostbyname(listaddr);
    if (!he) {
        perror("gethostbyname");
        return -1;
    }
    myaddr.sin_addr = *((struct in_addr*)he->h_addr);
    memset(&(myaddr.sin_zero), 0x00, sizeof(myaddr.sin_zero));
    if (bind(listener,(struct sockaddr*)&myaddr,sizeof(myaddr)) == -1) {
        perror("bind");
        return -1;
    }
    /* listen */
    if (listen(listener,BACKLOG) == -1) {
        perror("listen");
        return -1;
    }
    noserver:;
    /* initialize incoming command processor */
    icmdproc_initialize();
    /* initialize the IRC hash tables, lists */
    nickhash = dchash_hcreate(hashsize);
    chanhash = dchash_hcreate(hashsize);
    exthash = dchash_hcreate(hashsize);
    unshash = dchash_hcreate(hashsize);
    dclist_screate(&nickdescs);
    dclist_screate(&extdescs);
    /* set signal handlers (after setting hashtables, lists) */
    sigint.sa_handler = irceng_siginthandler;
    sigemptyset(&sigint.sa_mask);
    sigint.sa_flags = SA_ONESHOT;
    if (sigaction(SIGINT,&sigint,NULL) == -1) {
        perror("sigaction");
        return -1;
    }
    
    FD_ZERO(&master);
    FD_ZERO(&temp);
    if (engtype & ENGTYPE_CLIENT) {
        FD_SET(STDIN, &master);
        fdmax = STDIN;
    }
    if (engtype & ENGTYPE_SERVER) {
        FD_SET(listener, &master);
        fdmax = listener;
    }
    
    while (1) {
        temp = master;
        if (select(fdmax+1,&temp,NULL,NULL,NULL) == -1) {
            perror("select");
            return -1;
        }
        for (i=0; i<=fdmax; i++) {
            if (FD_ISSET(i,&temp)) {
                curfd = i;
                if (i == listener) {  /* handle new connections */
                    addrlen = sizeof(remoteaddr);
                    newfd = accept(listener, (struct sockaddr*)&remoteaddr,
                    /*cont.*/&addrlen);
                    if (newfd != -1) {
                        if (newfd > fdmax)
                            fdmax = newfd;
                        FD_SET(newfd, &master);
                    } else {
                        perror("accept");
                        return -1;
                    }
                } else {  /* handle data from a client */
                    if (i != STDIN) {
                        nbytes = irceng_recvln(i, buffer, sizeof(buffer)-1);
                        if (nbytes > 0) {
                            fromfunc(buffer, nbytes, curfd);
                        } else {
                            if (nbytes != 0) {
                                perror("recv");
                                return -1;
                            }
                            irceng_quit_connection();
                        }
                    } else {
                        UICALL(IFACEUI_HANDLEKB);  /* there is now stuff in kbqueue */
                    }
                }
            }
        }
    }
    return 0;
}

void irceng_siginthandler(int sig)
{
    UICALL(IFACEUI_QUIT);
    dchash_destroy_advanced(nickhash, deallocfunc_nickhash);
    dchash_destroy_advanced(chanhash, deallocfunc_chanhash);
    dchash_destroy_advanced(exthash, deallocfunc_exthash);
    dchash_destroy_nofree(unshash);
    dclist_clear(&nickdescs);
    dclist_clear(&extdescs);
    exit(0);
}

void deallocfunc_nickhash(void* satp)
{
    shared_clear_one_nickhash(satp);
}

void deallocfunc_chanhash(void* satp)
{
    shared_clear_one_chanhash(satp);
}

void deallocfunc_exthash(void* satp)
{
    shared_clear_one_exthash(satp);
}

int irceng_recvln(int sfd, void* buf, size_t len)
{
    int nrecv;
    char* msgp = (char*)buf;
    char* msgep = msgp + len;
    char prevch = '\0';
    while (msgp < msgep) {
        nrecv = read(sfd, msgp, 1);
        if (nrecv==0 || nrecv==-1)
            return nrecv;
        if (prevch=='\r' && *msgp=='\n')
            return (msgp+1) - (char*)buf;
        prevch = *msgp;
        msgp++;
    }
    return (msgp-1) - (char*)buf;
}

void irceng_quit_connection(void)
{
    BOOL found;
    DCLIST* listp;
    DCITER* iterp;
    CHNICK* chnick;
    NICKDESC* nickdesc = NULL;
    /* check is the closing connection a client */
    found = FALSE;
    if (dclist_iterate_first(&nickdescs,&iterp)) {
        do {
            nickdesc = (NICKDESC*)(iterp+1);
            if (nickdesc->sockfd == curfd) {
                found = TRUE;
                break;
            }
        } while (dclist_iterate_next(&nickdescs,&iterp));
    }
    if (found) {  /* closing connection is a client */
        /* send QUIT to all other nicks on the same channel */
        
        /* TODO */
        
        /* remove nickname from all channels (data structures only */
        listp = &nickdesc->nickdesc->channels;
        if (dclist_iterate_first(listp,&iterp)) {
            do {
                chnick = *(CHNICK**)(iterp+1);
                dclist_erase_one_at_iter(&chnick->chandef->chnicks,
                /*cont.*/(DCNODE*)(chnick-1));
            } while (dclist_iterate_next(listp,&iterp));
        }
        /* remove nickname from the nick hash */
        iterp = (DCNODE*)nickdesc->nickdesc - 1;
        shared_clear_one_nickhash(nickdesc->nickdesc);
        iterp->prevp->nextp = iterp->nextp;  /* erase_one_at_iter simulation */
        iterp->nextp->prevp = iterp->prevp;  /* erase_one_at_iter simulation */
        free(iterp);
        /* remove nickname from the nick descriptors */
        iterp = (DCNODE*)nickdesc - 1;
        dclist_erase_one_at_iter(&nickdescs, iterp);
    } else {  /* closing connection is a server */
        
        /* TODO */
        
    }
    FD_CLR(curfd, &master);
    close(curfd);
}
