#include "pathdefs.h"
#include PATH_ICMDPROC

/* interface functions -- begin */
ICRET icmdproc_process_NICK(ICOMMAND*);
ICRET icmdproc_process_USER(ICOMMAND*);
ICRET icmdproc_process_SERVER(ICOMMAND*);
ICRET icmdproc_process_JOIN(ICOMMAND*);
void icmdproc_process_JOINsub(void);
ICRET icmdproc_process_PART(ICOMMAND*);
BOOL icmdproc_process_PARTsub(DCLIST*);
ICRET icmdproc_process_MODE(ICOMMAND*);
ICRET icmdproc_process_PRIVMSG(ICOMMAND*);
/* interface functions -- end */

static int curfd;
static ICRET (*iprocfuncs[_ICMD_TOTAL])(ICOMMAND*);
extern struct gperficmds* in_iword_set(const char* str, unsigned int len);


void icmdproc_initialize(void)
{
    memset(iprocfuncs, 0x00, sizeof(iprocfuncs));
    iprocfuncs[ICMD_NICK] = icmdproc_process_NICK;
    iprocfuncs[ICMD_USER] = icmdproc_process_USER;
    iprocfuncs[ICMD_SERVER] = icmdproc_process_SERVER;
    iprocfuncs[ICMD_JOIN] = icmdproc_process_JOIN;
    iprocfuncs[ICMD_PART] = icmdproc_process_PART;
    iprocfuncs[ICMD_MODE] = icmdproc_process_MODE;
    iprocfuncs[ICMD_PRIVMSG] = icmdproc_process_PRIVMSG;
}

void icmdproc_handleincoming(char* msg, int nbytes, int fd)
{
    UINT len;
    ICRET icret;
    GPERFICMDS* gperficmd;
    ICOMMAND* icommand = icmdproc_parse_incoming(msg, nbytes);
    
    curfd = fd;
    if (icommand->allok != TRUE) {
        icmdproc_dealloc_icommand(icommand);
        return;
    }
    /* too good to be true - O(1) performance for processing (procfuncs) :) */
    len = icommand->cmdtype.notserv.cmdlen;
    if ((gperficmd=in_iword_set(icommand->cmdtype.notserv.command,len))) {
        if (iprocfuncs[gperficmd->id]) {
            icret = iprocfuncs[gperficmd->id](icommand);
        }
    }
    
    
    
    icmdproc_dealloc_icommand(icommand);
}

/* interface functions -- begin */

/* Numeric replies for NICK
 * ERR_NONICKNAMEGIVEN
 * ERR_NICKNAMEINUSE
 * ERR_ERRONEOUSNICKNAME
 * ERR_NICKCOLLISION
 */
ICRET icmdproc_process_NICK(ICOMMAND* icommand)
{
    /* NICK <nickname> [ <hopcount> ] */
    NICKHASH* p;
    PARMDEF* parmdef;
    NICKDESC tmpdesc;
    NICKHASH tmpnick;
    NICKHASH* foundp;
    /* if NICK is from a client (detect), <hopcount> must not exist */
    if (icommand->paramsn != 1) {
        
        /* TODO: ERR_NONICKNAMEGIVEN to client */
        
        return ICRET_INVPARMS;
    }
    parmdef = (PARMDEF*)(icommand->params.nextp+1);
    if ((foundp=dchash_find(nickhash,parmdef->param))) {
        if (foundp->sockfd != curfd) {  /* not his own sock */
            
            /* TODO: ERR_NICKNAMEINUSE to client */
            
        }
        /* his own sock, do nothing */
    } else {  /* new nick/sock */
        strncpy(tmpnick.nickname, parmdef->param, sizeof(tmpnick.nickname)-1);
        tmpnick.sockfd = curfd;
        dclist_screate(&tmpnick.channels);
        p = dchash_insert_noalloc(nickhash, tmpnick.nickname, &tmpdesc,
        /*cont.*/sizeof(tmpdesc));
        tmpdesc.sockfd = curfd;
        tmpdesc.nickdesc = p;
        dclist_push_back(&nickdescs, &tmpdesc, sizeof(tmpdesc));
        
        /* TODO: NICK to other servers */
        
    }
    return ICRET_NOERR;
}

/* Numeric replies for USER
 * ERR_NEEDMOREPARAMS
 * ERR_ALREADYREGISTRED
 */
ICRET icmdproc_process_USER(ICOMMAND* icommand)
{
    /* USER <username> <hostname> <servername> <realname> */
    
    if (icommand->paramsn < 4) {
        
        /* TODO: ERR_NEEDMOREPARAMS to client */
        
        return ICRET_INVPARMS;
    }
    
    return ICRET_NOERR;
}

/*
 * Numeric replies for SERVER
 * ERR_ALREADYREGISTRED
 */
ICRET icmdproc_process_SERVER(ICOMMAND* icommand)
{
    /* SERVER <servername> <hopcount> <info> */
    
    
    return ICRET_NOERR;
}

/*
 * Numeric replies for JOIN
 * ERR_NEEDMOREPARAMS
 * ERR_INVITEONLYCHAN
 * ERR_CHANNELISFULL
 * ERR_NOSUCHCHANNEL
 * ERR_BANNEDFROMCHAN
 * ERR_BADCHANNELKEY
 * ERR_BADCHANMASK
 * ERR_TOOMANYCHANNELS
 * RPL_TOPIC
 */
ICRET icmdproc_process_JOIN(ICOMMAND* icommand)
{
    BOOL iskey;
    DCNODE* chiterp;
    DCNODE* keiterp;
    DCLIST* chlistp;
    DCLIST* kelistp;
    SINGLESEP* keys;
    SINGLESEP* chans;
    PARMDEF* parm1def;
    PARMDEF* parm2def;
    /* JOIN <channel>{,<channel>} [<key>{,<key}] */
    if (icommand->paramsn < 1) {
        
        /* TODO: ERR_NEEDMOREPARAMS to client */
        
    }
    parm1def = (PARMDEF*)(icommand->params.nextp+1);
    parm2def = (PARMDEF*)(icommand->params.nextp->nextp+1);
    /*
    chans = icmdproc_parse_singlesepstr(parm1def->param, ',');
    keys = icmdproc_parse_singlesepstr(parm2def->param, ',');
    */
    
    chlistp = &chans->params;
    kelistp = &keys->params;
    iskey = dclist_iterate_first(kelistp,&keiterp);
    if (dclist_iterate_first(chlistp,&chiterp)) {
        do {
            
            
            
            if (iskey) {
                iskey = dclist_iterate_next(kelistp, &keiterp);
            }
        } while (dclist_iterate_next(chlistp,&chiterp));
    }
    
    /*
    icmdproc_dealloc_singlesep(keys);
    icmdproc_dealloc_singlesep(chans);
    */
    
    return ICRET_NOERR;
}

void icmdproc_process_JOINsub(void)
{
}

/*
 * Numeric replies for PART
 * ERR_NEEDMOREPARAMS
 * ERR_NOTONCHANNEL
 * ERR_NOSUCHCHANNEL
 */
ICRET icmdproc_process_PART(ICOMMAND* icommand)
{
    char* chname;
    DCNODE* chiterp;
    DCLIST* chlistp;
    SINGLESEP* chans;
    CHANHASH* foundp;
    PARMDEF* parmdef;
    /* PART <channel>{,<channel>} */
    if (icommand->paramsn < 1) {
        
        /* TODO: ERR_NEEDMOREPARAMS to client */
        
        return ICRET_INVPARMS;
    }
    parmdef = (PARMDEF*)(icommand->params.nextp+1);
    /*
    chans = icmdproc_parse_singlesepstr(parmdef->param, ',');
    */
    chlistp = &chans->params;
    if (dclist_iterate_first(chlistp,&chiterp)) {
        do {
            chname = (char*)(chiterp+1);
            if (!(foundp=dchash_find(chanhash,chname))) {
                
                /* TODO: ERR_NOSUCHCHANNEL to client */
                
            } else {
                
                /* TODO: ERR_NOTONCHANNEL to client */
                
            }
        } while (dclist_iterate_next(chlistp,&chiterp));
    }
    
    /*
    icmdproc_dealloc_singlesep(chans);
    */
    
    return ICRET_NOERR;
}

BOOL icmdproc_process_PARTsub(DCLIST* listp)
{
    DCNODE* iterp;
    CHNICK* chnick;
    if (dclist_iterate_first(listp,&iterp)) {
        do {
            chnick = (CHNICK*)(iterp+1);
            if (chnick->nickdef->sockfd == curfd) {
                dclist_erase_one_at_iter(listp, iterp);
            }
        } while (dclist_iterate_next(listp,&iterp));
    }
    return FALSE;
}

/*
 * Numeric replies for MODE:
 * ERR_NEEDMOREPARAMS
 * ERR_CHANOPRIVSNEEDED
 * ERR_NOTONCHANNEL
 * ERR_UNKNOWNMODE
 * ERR_USERSDONTMATCH
 * ERR_UMODEUNKNOWNFLAG
 * ERR_NOSUCHNICK
 * ERR_KEYSET
 * ERR_ENDOFBANLIST
 * ERR_NOSUCHCHANNEL
 * RPL_BANLIST
 * RPL_CHANNELMODEIS
 * RPL_ENDOFBANLIST
 * RPL_UMODEIS
 */
ICRET icmdproc_process_MODE(ICOMMAND* icommand)
{
    /* MODE <channel> {[+|-]|o|p|s|i|t|n|b|v} [<limit>] [<user>] [<banmask>] */
    
    
    
    return ICRET_NOERR;
}

/*
 * Numeric replies for PRIVMSG
 * ERR_NORECIPIENT
 * ERR_CANNOTSENDTOCHAN
 * ERR_WILDTOPLEVEL
 * ERR_NOSUCHNICK
 * ERR_NOTEXTTOSEND
 * ERR_NOTOPLEVEL
 * ERR_TOOMANYTARGETS
 * RPL_AWAY
 */
ICRET icmdproc_process_PRIVMSG(ICOMMAND* icommand)
{
    char* recvr;
    DCITER* iterp;
    DCITER* listp;
    SINGLESEP* recvs;
    PARMDEF* parm1def;
    PARMDEF* parm2def;
    /* PRIVMSG <receiver>{,<receiver>} {text to be sent} */
    if (icommand->paramsn < 2) {
        
        /* TODO: ERR_NEEDMOREPARAMS to client */
        
        return ICRET_INVPARMS;
    }
    parm1def = (PARMDEF*)(icommand->params.nextp+1);
    parm2def = (PARMDEF*)(icommand->params.nextp->nextp+1);
    /*
    recvs = icmdproc_parse_singlesepstr(parm1def->param, ',');
    */
    listp = &recvs->params;
    if (dclist_iterate_first(listp,&iterp)) {
        do {
            recvr = *(char**)(iterp+1);
            if (recvr[0]=='#' || recvr[0]=='&') {
                    /*
                    icmdproc_privmsg_to_channel(recvr, parm2def->param);
                    */
            }
        } while (dclist_iterate_next(listp,&iterp));
    }
    /*
    icmdproc_dealloc_singlesep(recvs);
    */
    return ICRET_NOERR;
}

/* interface functions -- end */
