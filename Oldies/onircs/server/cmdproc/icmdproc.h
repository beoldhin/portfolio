#ifndef _ICMDPROC_H
#define _ICMDPROC_H

#include "pathdefs.h"
#include PATH_STDLIBINCS
#include PATH_SHARED
#include PATH_IPARSERS

void icmdproc_initialize(void);
void icmdproc_handleincoming(char*, int, int);

#endif  /* _ICMDPROC_H */
