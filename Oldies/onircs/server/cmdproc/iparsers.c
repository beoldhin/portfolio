#include "pathdefs.h"
#include PATH_IPARSERS

void deallocfunc_icommand(void*);


ICOMMAND* icmdproc_parse_incoming(char* buffer, int nbytes)
{
    UINT n;
    int numeric;
    char* curp;
    char* endp;
    char* savedp;
    PARMDEF parmdef;
    char tmpbuf[MAXMSG+1];
    ICOMMAND* icommand = malloc(sizeof(ICOMMAND));
    icommand->allok = FALSE;
    icommand->srvonck[0] = '\0';
    icommand->username[0] = '\0';
    icommand->hostname[0] = '\0';
    icommand->cmdtype.notserv.command[0] = '\0';
    icommand->cmdtype.notserv.cmdlen = 0;
    icommand->cmdtype.servcmd = 0;
    icommand->paramsn = 0;
    dclist_screate(&icommand->params);
    /* scan for starting character */
    endp = buffer + nbytes;
    for (curp=buffer; curp<endp; curp++) {
        if (!isspace(*curp))
            break;
    }
    /* if no starting character, quit immediately */
    if (curp == endp)
        return icommand;
    /* if ':', prefix must follow (and optional '!') */
    icommand->origtype = ORIG_LOCAL;
    if (*curp == ':') {
        icommand->origtype = ORIG_GLOBAL;
        /* scan for starting prefix */
        for (curp++; curp<endp; curp++) {
            if (!isspace(*curp))
                break;
        }
        /* if no starting prefix, quit immediately */
        if (curp == endp)
            return icommand;
        for (savedp=curp; curp<endp; curp++) {
            if (isspace(*curp) || *curp=='!')
                break;
        }
        LIMITBUF(sizeof(icommand->srvonck)-1);
        icommand->srvonck[n] = '\0';
        memcpy(icommand->srvonck, savedp, n);
        /* if nothing more, quit immediately */
        if (curp == endp)
            return icommand;
        if (*curp == '!') {
            /* scan for starting user */
            for (curp++; curp<endp; curp++) {
                if (!isspace(*curp))
                    break;
            }
            /* if no starting user, quit immediately */
            if (curp == endp)
                return icommand;
            for (savedp=curp; curp<endp; curp++) {
                if (isspace(*curp) || *curp=='@')
                    break;
            }
            LIMITBUF(sizeof(icommand->username)-1);
            icommand->username[n] = '\0';
            memcpy(icommand->username, savedp, n);
            if (curp<endp && *curp=='@') {
                /* scan for starting host */
                for (curp++; curp<endp; curp++) {
                    if (!isspace(*curp))
                        break;
                }
                /* if no starting host, quit immediately */
                if (curp == endp)
                    return icommand;
                for (savedp=curp; curp<endp; curp++) {
                    if (isspace(*curp))
                        break;
                }
                LIMITBUF(sizeof(icommand->hostname)-1);
                icommand->hostname[n] = '\0';
                memcpy(icommand->hostname, savedp, n);
            }
            /* if no command, quit immediately */
            if (curp == endp)
                return icommand;
        }
    }
    /* now the code below is only executed if there really exist a command */
    /* command can be numeric only if all are nums, but alpha can have nums */
    numeric = 0;
    for (savedp=curp; curp<endp; curp++) {
        if (isspace(*curp))
            break;
        if (isdigit(*curp))
            numeric++;
    }
    if (curp-savedp == numeric) {  /* numeric */
        icommand->isreply = TRUE;
        memcpy(tmpbuf, savedp, curp-savedp);
        tmpbuf[curp-savedp] = '\0';
        icommand->cmdtype.servcmd = atoi(tmpbuf);
    } else {  /* alpha */
        LIMITBUF(sizeof(icommand->cmdtype.notserv.command)-1);
        icommand->isreply = FALSE;
        icommand->cmdtype.notserv.command[n] = '\0';
        icommand->cmdtype.notserv.cmdlen = n;
        memcpy(icommand->cmdtype.notserv.command, savedp, n);
    }
    while (curp < endp) {
        /* scan for starting param */
        for (curp++; curp<endp; curp++) {
            if (!isspace(*curp))
                break;
        }
        /* if no starting param, break (no error) */
        if (curp == endp)
            break;
        if (*curp == ':') {
            /* if no param, quit immediately */
            curp++;
            if (curp == endp)
                return icommand;
            for (savedp=curp; curp<endp; curp++) {
                if (*curp==0x0D || *curp==0x0A)
                    break;
            }
            parmdef.parmtype = PARM_LIST;
        } else {
            for (savedp=curp; curp<endp; curp++) {
                if (isspace(*curp))
                    break;
            }
            parmdef.parmtype = PARM_SINGLE;
        }
        LIMITBUF(sizeof(parmdef.param)-1);
        parmdef.parmlen = n;
        parmdef.param[n] = '\0';
        memcpy(parmdef.param, savedp, n);
        dclist_push_back(&icommand->params, &parmdef, sizeof(PARMDEF));
        icommand->paramsn++;
    }
    icommand->allok = TRUE;
    return icommand;
}

void icmdproc_dealloc_icommand(ICOMMAND* icommand)
{
    icommand->srvonck[0] = '\0';
    icommand->username[0] = '\0';
    icommand->hostname[0] = '\0';
    icommand->cmdtype.notserv.command[0] = '\0';
    icommand->cmdtype.notserv.cmdlen = 0;
    icommand->paramsn = 0;
    dclist_clear_advanced(&icommand->params, deallocfunc_icommand);
}

void deallocfunc_icommand(void* satp)
{
    PARMDEF* parmdef = (PARMDEF*)satp;
    free(parmdef->param);
}
