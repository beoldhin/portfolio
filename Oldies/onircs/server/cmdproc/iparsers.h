#ifndef _IPARSERS_H
#define _IPARSERS_H

#include "pathdefs.h"
#include PATH_STDLIBINCS
#include PATH_SHARED

ICOMMAND* icmdproc_parse_incoming(char*, int);
SINGLESEP* icmdproc_parse_singlesepstr(char*, char);
void icmdproc_dealloc_icommand(ICOMMAND*);
void icmdproc_dealloc_singlesep(SINGLESEP*);

#endif  /* _IPARSERS_H */
