/* C code produced by gperf version 2.7.2 */
/* Command-line: gperf -Kcommand -t -o server/shared/icmds.gperf  */
#include "pathdefs.h"
#include PATH_SHARED

#define TOTAL_KEYWORDS 39
#define MIN_WORD_LENGTH 3
#define MAX_WORD_LENGTH 8
#define MIN_HASH_VALUE 4
#define MAX_HASH_VALUE 81
/* maximum key range = 78, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (str, len)
     register const char *str;
     register unsigned int len;
{
  static unsigned char asso_values[] =
    {
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 25, 82, 35, 82, 15,
      82, 45,  5, 60, 35, 20, 45, 20, 10,  0,
      10,  0,  0,  0,  5, 25,  0, 30, 82, 50,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82, 82, 82, 82, 82,
      82, 82, 82, 82, 82, 82
    };
  return len + asso_values[(unsigned char)str[len - 1]] + asso_values[(unsigned char)str[0]];
}

#ifdef __GNUC__
__inline
#endif
struct gperficmds *
in_iword_set (str, len)
     register const char *str;
     register unsigned int len;
{
  static struct gperficmds wordlist[] =
    {
      {""}, {""}, {""}, {""},
      {"OPER",       ICMD_OPER},
      {"STATS",      ICMD_STATS},
      {"SERVER",     ICMD_SERVER},
      {""}, {""},
      {"QUIT",       ICMD_QUIT},
      {"SQUIT",      ICMD_SQUIT},
      {"REHASH",     ICMD_REHASH},
      {"RESTART",    ICMD_RESTART},
      {""},
      {"PASS",       ICMD_PASS},
      {"NAMES",      ICMD_NAMES},
      {"SUMMON",     ICMD_SUMMON},
      {"VERSION",    ICMD_VERSION},
      {""},
      {"PART",       ICMD_PART},
      {"ERROR",      ICMD_ERROR},
      {""}, {""}, {""},
      {"TIME",       ICMD_TIME},
      {"TRACE",      ICMD_TRACE},
      {""}, {""}, {""},
      {"USER",       ICMD_USER},
      {"USERS",      ICMD_USERS},
      {"NOTICE",     ICMD_NOTICE},
      {""},
      {"WHO",        ICMD_WHO},
      {"NICK",       ICMD_NICK},
      {"WHOIS",      ICMD_WHOIS},
      {"WHOWAS",     ICMD_WHOWAS},
      {"WALLOPS",    ICMD_WALLOPS},
      {"USERHOST",   ICMD_USERHOST},
      {"MODE",       ICMD_MODE},
      {"ADMIN",      ICMD_ADMIN},
      {""}, {""}, {""},
      {"KICK",       ICMD_KICK},
      {"TOPIC",      ICMD_TOPIC},
      {""},
      {"CONNECT",    ICMD_CONNECT},
      {""},
      {"JOIN",       ICMD_JOIN},
      {"LINKS",      ICMD_LINKS},
      {""}, {""}, {""},
      {"LIST",       ICMD_LIST},
      {""}, {""}, {""}, {""},
      {"PING",       ICMD_PING},
      {""}, {""},
      {"PRIVMSG",    ICMD_PRIVMSG},
      {""},
      {"INFO",       ICMD_INFO},
      {""}, {""}, {""}, {""},
      {"KILL",       ICMD_KILL},
      {""}, {""}, {""}, {""},
      {"ISON",       ICMD_ISON},
      {""}, {""}, {""}, {""},
      {"AWAY",       ICMD_AWAY},
      {""},
      {"INVITE",     ICMD_INVITE}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = wordlist[key].icommand;

          if (*str == *s && !strcmp (str + 1, s + 1))
            return &wordlist[key];
        }
    }
  return 0;
}
