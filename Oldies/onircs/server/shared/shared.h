#ifndef _SHARED_H
#define _SHARED_H

#include "pathdefs.h"
#include PATH_STDLIBINCS
#include PATH_DCLIST
#include PATH_DCHASH

#define LIMITBUF(lim) {n=curp-savedp; if(n>lim) {savedp=curp+lim; n=lim;}}
#define UICALL(uit) {if (ifaceui[uit]) ifaceui[uit]();}

#define UNKLEN  510
#define MAXMSG  510
#define MAXCHAN 200
#define MAXHOST 64
#define MAXNICK 9
#define MAXUSER 8
#define MAXCMD  8  /* the command USERHOST */

typedef enum ifaces {
    IFACEUI_SETUP,
    IFACEUI_HANDLEKB,
    IFACEUI_QUIT,
    _IFACEUI_TOTAL  /* keep this as the last item */
} IFACES;

/* ================================================================== */

typedef enum icret {
    ICRET_INVPARMS,
    ICRET_NOERR
} ICRET;

/* ================================================================== */

typedef struct gperficmds {  /* remove struct from generated icmds.c */
    char* icommand;
    int id;
} GPERFICMDS;

typedef enum icommands {
    ICMD_PASS,
    ICMD_NICK,
    ICMD_USER,
    ICMD_SERVER,
    ICMD_OPER,
    ICMD_QUIT,
    ICMD_SQUIT,
    ICMD_JOIN,
    ICMD_PART,
    ICMD_MODE,
    ICMD_TOPIC,
    ICMD_NAMES,
    ICMD_LIST,
    ICMD_INVITE,
    ICMD_KICK,
    ICMD_VERSION,
    ICMD_STATS,
    ICMD_LINKS,
    ICMD_TIME,
    ICMD_CONNECT,
    ICMD_TRACE,
    ICMD_ADMIN,
    ICMD_INFO,
    ICMD_PRIVMSG,
    ICMD_NOTICE,
    ICMD_WHO,
    ICMD_WHOIS,
    ICMD_WHOWAS,
    ICMD_KILL,
    ICMD_PING,
    ICMD_ERROR,
    ICMD_AWAY,
    ICMD_REHASH,
    ICMD_RESTART,
    ICMD_SUMMON,
    ICMD_USERS,
    ICMD_WALLOPS,
    ICMD_USERHOST,
    ICMD_ISON,
    _ICMD_TOTAL  /* keep this as the last item */
} ICOMMANDS;

/* ================================================================== */

typedef struct singlesep {
    int paramsn;
    DCLIST params;  /* list of SSPARMDEFs */
} SINGLESEP;
        /* || */
        /* || */
        /* vv */
        typedef struct ssparmdef {
            char param[MAXCMD+1];
            UINT parmlen;
        } SSPARMDEF;

typedef enum origtype {
    ORIG_GLOBAL,
    ORIG_LOCAL
} ORIGTYPE;

typedef enum iparmtype {
    PARM_SINGLE,
    PARM_LIST
} IPARMTYPE;

typedef struct icommand {
    BOOL allok;
    BOOL isreply;  /* if command was numeric, then reply is from server */
    ORIGTYPE origtype;  /* if first char is a ':', then command from global */
    char srvonck[MAXHOST+1];  /* servername or nickname [MAXHOST>MAXNICK] */
    char username[MAXUSER+1];
    char hostname[MAXHOST+1]; /* exists only if username exists */
    union CMDTYPE {
        struct NOTSERV {
            char command[MAXCMD+1];
            UINT cmdlen;
        } notserv;
        int servcmd;  /* command from a server only */
    } cmdtype;
    int paramsn;  /* -1 = no list, 0 = empty list with one sentinel */
    DCLIST params;  /* list of PARMDEFs */
} ICOMMAND;
        /* || */
        /* || */
        /* vv */
        typedef struct parmdef {
            char param[MAXCMD+1];
            UINT parmlen;
            IPARMTYPE parmtype;
        } PARMDEF;

/* ================================================================== */

typedef enum chstate {
    CHSTATE_GLOBALCANJOIN,  /* '#' */
    CHSTATE_LOCALCANJOIN    /* '&' */
} CHSTATE;

typedef struct nickhash {
    char nickname[MAXNICK+1];
    char hostname[MAXHOST+1];
    DCLIST channels;  /* list of pointers to CHNICKs */
    int sockfd;
} NICKHASH;

typedef struct chanhash {
    char channame[MAXCHAN+1];
    char password[UNKLEN+1];
    char topic[UNKLEN+1];
    CHSTATE chstate;
    DCLIST chnicks;  /* list of CHNICKs */
} CHANHASH;
        /* || */
        /* || */
        /* vv */
        typedef struct chnick {
            CHANHASH* chandef;  /* upper level */
            NICKHASH* nickdef;
            char chmodes;
        } CHNICK;

typedef struct unshash {  /* user+server ("a@b"), used with PRIVMSG, crated with USER */
    char unsname[MAXUSER+1+MAXHOST+1];
    NICKHASH* unshash;
} UNSHASH;

typedef struct exthash {  /* external servers connected to this server */
    char servname[MAXHOST+1];
    char info[UNKLEN+1];
    int hopcount;
    int sockfd;
} EXTHASH;

typedef struct nickdesc {  /* used to find nickhash slot by sockfd */
    int sockfd;
    NICKHASH* nickdesc;
} NICKDESC;

typedef struct extdesc {  /* used to find exthash slot by sockfd */
    int sockfd;
    EXTHASH* extdesc;
} EXTDESC;

/* ================================================================== */

void shared_clear_one_nickhash(NICKHASH*);
void shared_clear_one_chanhash(CHANHASH*);
void shared_clear_one_exthash(EXTHASH*);
int shared_sendall(int, void*, size_t);
SINGLESEP* cmdproc_parse_singlesepstr(char*, char);

DCHASH* nickhash;  /* hash of NICKHASH */
DCHASH* chanhash;  /* hash of CHANHASH */
DCHASH* exthash;   /* hash of EXTHASH */
DCHASH* unshash;   /* hash of UNSHASH */
DCLIST nickdescs;  /* list of NICKDESC */
DCLIST extdescs;   /* list of EXTDESC */

void (*ifaceui[_IFACEUI_TOTAL])(void);

#endif  /* _SHARED_H */
