#include "shared.h"


void shared_clear_one_nickhash(NICKHASH* nickhp)
{
    nickhp->nickname[0] = '\0';
    nickhp->hostname[0] = '\0';
    dclist_clear(&nickhp->channels);
}

void shared_clear_one_chanhash(CHANHASH* chanhp)
{
    chanhp->channame[0] = '\0';
    chanhp->password[0] = '\0';
    chanhp->topic[0] = '\0';
    dclist_clear(&chanhp->chnicks);
}

void shared_clear_one_exthash(EXTHASH* exthp)
{
    exthp->servname[0] = '\0';
    exthp->info[0] = '\0';
}

int shared_sendall(int sfd, void* buf, size_t len)
{
    int nsent;
    int ntotal = 0;
    UINT nleft = len;
    while (ntotal < len) {
        nsent = send(sfd, (char*)buf+ntotal, nleft, 0);
        if (nsent == -1)
            return -1;
        ntotal += nsent;
        nleft -= nsent;
    }
    return ntotal;
}

SINGLESEP* cmdproc_parse_singlesepstr(char* curp, char sep)
{
    UINT n;
    char* savedp = curp;
    SSPARMDEF ssparmdef;
    SINGLESEP* singlesep = malloc(sizeof(SINGLESEP));
    dclist_screate(&singlesep->params);
    singlesep->paramsn = 0;
    while (*curp != '\0') {
        if (*curp==sep && curp!=savedp) {
            LIMITBUF(sizeof(ssparmdef.param)-1);
            ssparmdef.parmlen = n;
            ssparmdef.param[n] = '\0';
            memcpy(ssparmdef.param, savedp, n);
            dclist_push_back(&singlesep->params, &ssparmdef, sizeof(SSPARMDEF));
            singlesep->paramsn++;
            savedp = curp + 1;
        }
    }
    if (curp != savedp) {
        LIMITBUF(sizeof(ssparmdef.param)-1);
        ssparmdef.parmlen = n;
        ssparmdef.param[n] = '\0';
        memcpy(ssparmdef.param, savedp, n);
        dclist_push_back(&singlesep->params, &ssparmdef, sizeof(SSPARMDEF));
        singlesep->paramsn++;
    }
    return singlesep;
}

void cmdproc_dealloc_singlesep(SINGLESEP* singlesep)
{
    dclist_clear(&singlesep->params);
    free(singlesep);
}
