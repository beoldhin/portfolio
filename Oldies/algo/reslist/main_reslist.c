#include "/home/olli/src/algo/reslist/reslist.h"

int main(void) {
    int* a;
    int* b;
    int* c;
    DCLIST reslist;
    
    reslist_screate(&reslist);
    
    a = (int*)malloc(sizeof(int));
    reslist_alloc(&reslist, RES_MALLOC, a);
    b = (int*)malloc(sizeof(int));
    reslist_alloc(&reslist, RES_MALLOC, b);
    c = (int*)malloc(sizeof(int));
    reslist_alloc(&reslist, RES_MALLOC, c);
    
    /* if needing to explictly release some resource */
    reslist_free(c, &reslist);
    
    reslist_clear(&reslist);
    
    return 0;
}
