#include "/home/olli/src/algo/reslist/reslist.h"

#ifdef TEST_COMPILATION
  #define RESLIST_POP
#endif

#ifdef RESLIST_POP
void reslist_pop(DCLIST*, enum RESTYPE, void*);
#endif


#ifdef RESLIST_SCREATE
void reslist_screate(DCLIST* listp) {
    dclist_screate(listp);
}
#endif

#ifdef RESLIST_HCREATE
DCLIST* reslist_hcreate(void) {
    return dclist_hcreate();
}
#endif

#ifdef RESLIST_ALLOC
BOOL reslist_alloc(DCLIST* listp, enum RESTYPE restype, void* addr) {
    RESLIST new_res;
    new_res.restype = restype;
    switch (restype) {
      case RES_MALLOC:
        new_res.resource.mallocp = addr;
        if (!dclist_push_back(listp,&new_res,sizeof(RESLIST)))
            return FALSE;
        break;
      case RES_FOPEN:
        new_res.resource.fopenp = addr;
        if (!dclist_push_back(listp,&new_res,sizeof(RESLIST)))
            return FALSE;
        break;
    }
    return TRUE;
}
#endif

#ifdef RESLIST_MALLOC
void* reslist_malloc(size_t size, DCLIST* listp) {
    void* p;
    if (!(p=malloc(size)))
        return NULL;
    if (!reslist_alloc(listp,RES_MALLOC,p))
        return NULL;
    return p;
}
#endif

#ifdef RESLIST_FOPEN
FILE* reslist_fopen(const char* path, const char* mode, DCLIST* listp) {
    FILE* fp;
    if (!(fp=fopen(path,mode)))
        return NULL;
    if (!reslist_alloc(listp,RES_FOPEN,fp))
        return NULL;
    return fp;
}
#endif

#ifdef RESLIST_FREE
void reslist_free(void* ptr, DCLIST* listp) {
    free(ptr);
    reslist_pop(listp, RES_MALLOC, ptr);
}
#endif

#ifdef RESLIST_FCLOSE
int reslist_fclose(FILE* stream, DCLIST* listp) {
    int ret;
    if ((ret=fclose(stream)) == EOF)
        return ret;
    reslist_pop(listp, RES_FOPEN, stream);
    return ret;
}
#endif

#ifdef RESLIST_RELEASE
void reslist_release(DCLIST* listp) {
    DCITER* iterp;
    RESLIST* resource;
    if (dclist_iterate_first(listp,&iterp)) {
        do {
            resource = (RESLIST*)(iterp+1);
            switch (resource->restype) {
              case RES_MALLOC:
                free(resource->resource.mallocp);
                break;
              case RES_FOPEN:
                fclose(resource->resource.fopenp);
                break;
            }
        } while (dclist_iterate_next(listp,&iterp));
    }
}
#endif

#ifdef RESLIST_POP
void reslist_pop(DCLIST* listp, enum RESTYPE restype, void* addr) {
    DCITER* iterp;
    RESLIST* resource;
    if (dclist_iterate_first(listp,&iterp)) {
        do {
            resource = (RESLIST*)(iterp+1);
            switch (resource->restype) {
              case RES_MALLOC:
                if (resource->resource.mallocp == addr) {
                    dclist_erase_one_at_iter(listp, iterp);
                    return;
                }
                break;
              case RES_FOPEN:
                if (resource->resource.fopenp == addr) {
                    dclist_erase_one_at_iter(listp, iterp);
                    return;
                }
                break;
            }
        } while (dclist_iterate_next(listp,&iterp));
    }
}
#endif

#ifdef RESLIST_CLEAR
void reslist_clear(DCLIST* listp) {
    reslist_release(listp);
    dclist_clear(listp);
}
#endif

#ifdef RESLIST_DESTROY
void reslist_destroy(DCLIST* listp) {
    reslist_release(listp);
    dclist_destroy(listp);
}
#endif
