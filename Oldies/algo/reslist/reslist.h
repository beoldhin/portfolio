#ifndef _RESLIST_H
#define _RESLIST_H

#include <stdio.h>
#include "/home/olli/src/algo/list/dclist.h"

enum RESTYPE {
    RES_MALLOC,
    RES_FOPEN
};

typedef struct reslist {
    enum RESTYPE restype;
    union {
        void* mallocp;
        FILE* fopenp;
    } resource;
} RESLIST;

#ifdef TEST_COMPILATION
  #define RESLIST_SCREATE
  #define RESLIST_HCREATE
  #define RESLIST_ALLOC
  #define RESLIST_MALLOC
  #define RESLIST_FOPEN
  #define RESLIST_FREE
  #define RESLIST_FCLOSE
  #define RESLIST_RELEASE
  #define RESLIST_ERASE
  #define RESLIST_CLEAR
  #define RESLIST_DESTROY
#endif

#ifdef RESLIST_SCREATE
void reslist_screate(DCLIST*);
#endif
#ifdef RESLIST_HCREATE
DCLIST* reslist_hcreate(void);
#endif
#ifdef RESLIST_ALLOC
BOOL reslist_alloc(DCLIST*, enum RESTYPE, void*);
#endif
#ifdef RESLIST_MALLOC
void* reslist_malloc(size_t, DCLIST*);
#endif
#ifdef RESLIST_FOPEN
FILE* reslist_fopen(const char*, const char*, DCLIST*);
#endif
#ifdef RESLIST_FREE
void reslist_free(void*, DCLIST*);
#endif
#ifdef RESLIST_FCLOSE
int reslist_fclose(FILE*, DCLIST*);
#endif
#ifdef RESLIST_RELEASE
void reslist_release(DCLIST*);
#endif
#ifdef RESLIST_CLEAR
void reslist_clear(DCLIST*);
#endif
#ifdef RESLIST_DESTROY
void reslist_destroy(DCLIST*);
#endif

#endif  /* _RESLISH_H */
