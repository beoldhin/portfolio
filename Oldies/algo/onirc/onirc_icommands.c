#include "onirc.h"

void onirc_handle_icommands(void) {
    ICOMMAND icmd;
    msgbufi -= 2;
    onirc_parse_icommand(&icmd);
    if (icmd.msgtype == SERVMSG) {
        if (icmd.msginfo.servmsg.command == 353) {
            onirc_iprocess_RPL_NAMREPLY(&icmd);
        } else if (icmd.msginfo.servmsg.command == 376) {
            onirc_iprocess_RPL_ENDOFMOTD(&icmd);
        }
    } else {
        if (strcmp(icmd.msginfo.nickmsg.command,"JOIN") == 0)
            onirc_iprocess_JOIN(&icmd);
        else if (strcmp(icmd.msginfo.nickmsg.command,"NICK") == 0)
            onirc_iprocess_NICK(&icmd);
        else if (strcmp(icmd.msginfo.nickmsg.command,"PART") == 0)
            onirc_iprocess_PART(&icmd);
        else if (strcmp(icmd.msginfo.nickmsg.command,"PRIVMSG") == 0)
            onirc_iprocess_PRIVMSG(&icmd);
        else if (strcmp(icmd.msginfo.nickmsg.command,"QUIT") == 0)
            onirc_iprocess_QUIT(&icmd);
    }
    onirc_icommand_dealloc(&icmd);
}

/* extract the nicknames from namreply */
void onirc_iprocess_RPL_NAMREPLY(ICOMMAND* icmd) {
    PNL* pnlp;
    BOOL found;
    int servfd;
    char* nicks;
    char* nickp;
    char* matchp;
    char* chname;
    char* channel;
    char* nicksend;
    DCLIST* iterp;
    char nickname[MAXNICK+1];
    enum pnltype pnltype;
    found = FALSE;
    channel = *(char**)(icmd->params.nextp->nextp->nextp+1);
    if (dclist_iterate_first(&panels,&iterp)) {
        do {
            pnlp = (PNL*)(iterp+1);
            pnltype = pnlp->pnltype;
            servfd = pnlp->servfd;
            chname = pnlp->pnlinfo.chanpnl.chname;
            if ((pnltype==CHANNEL) && (servfd==infd)) {
                if (strcmp(chname,channel) == 0) {
                    found = TRUE;
                    break;
                }
            }
        } while (dclist_iterate_next(&panels,&iterp));
    }
    if (found) {
        pnlp->pnlinfo.chanpnl.usersn = 0;
        dclist_screate(&pnlp->pnlinfo.chanpnl.users);
        nicks = *(char**)(icmd->params.nextp->nextp->nextp->nextp+1);
        nicksend = nicks + strlen(nicks);
        while (nicks < nicksend) {
            nickp = nicks;
            matchp = strchr(nicks, ' ');
            if (matchp) {
                *matchp = '\0';
                if (*nickp == '@')  /* chanop flag omitted */
                    ++nickp;
                strcpy(nickname, nickp);
                ++pnlp->pnlinfo.chanpnl.usersn;
                dclist_push_back(&pnlp->pnlinfo.chanpnl.users, &nickname,
                sizeof(nickname));
                nicks = matchp;
            }
            ++nicks;
        }
    }
}

/*
 * set ready flag of a server to TRUE if RPL_ENDOFMOTD
 * TODO: What if motd does not exist?
 */
void onirc_iprocess_RPL_ENDOFMOTD(ICOMMAND* icmd) {
    int servfd;
    DCLIST* iterp;
    WINDOW* window;
    if (dclist_iterate_first(&panels,&iterp)) {
        do {
            servfd = ((PNL*)(iterp+1))->servfd;
            if (servfd == infd)
                break;
        } while (dclist_iterate_next(&panels,&iterp));
    }
    ((PNL*)(iterp+1))->pnlinfo.servpnl.ready = TRUE;
    window = ((PNL*)(iterp+1))->window;
    if (window == curwin)
        onirc_show_title();
}

/* server sent notification of a join, create new channel panel */
void onirc_iprocess_JOIN(ICOMMAND* icmd) {
    PNL pnl;
    PNL* pnlp;
    DCLIST* iterp;
    if (!onirc_check_is_own_inick(icmd,&iterp)) {
        
        
        
        return;
    }
/* find last panel for the server */
    while (dclist_iterate_next(&panels,&iterp)) {
        pnlp = (PNL*)(iterp+1);
        if (pnlp->pnltype == SERVER)
            break;
    }
/* create new panel for the channel */
    pnl.pnltype = CHANNEL;
    pnl.window = newwin(LINES-2, COLS, 1, 0);
    pnl.panel = new_panel(pnl.window);
    pnl.servfd = ((PNL*)(iterp+1))->servfd;
    strcpy(pnl.pnlinfo.chanpnl.chname, *(char**)(icmd->params.nextp+1));
    dclist_insert_after_iter(iterp, &pnl, sizeof(pnl));
    ++panelsn;
    onirc_show_title();
}

/*
 * someone changes his nick
 * change all nicks in a channel and private panels for a given server
 */
void onirc_iprocess_NICK(ICOMMAND* icmd) {
    
    
    
}

/*
 * someone leaves a channel
 * if it's me, close the channel and all private panels for a given server
 * if it's someone else, remove his nick from nicknames and close private panel
 */
void onirc_iprocess_PART(ICOMMAND* icmd) {
    PNL* pnlp;
    int servfd;
    UINT marker;
    DCLIST clist;
    char* chname;
    char* cmdnick;
    char* channel;
    DCLIST* iterp;
    DCLIST* markp;
    DCLIST* usersp;
    char* nickname;
    enum pnltype pnltype;
    cmdnick = icmd->msginfo.nickmsg.nickname;
    if (!onirc_check_is_own_inick(icmd,&iterp)) {
        /* someone else's nick */
        while (dclist_iterate_next(&panels,&iterp)) {
            pnlp = (PNL*)(iterp+1);
            pnltype = pnlp->pnltype;
            nickname = pnlp->pnlinfo.privpnl.nickname;
            if (pnltype == SERVER)
                break;
            if (pnltype == CHANNEL)
                usersp = &((PNL*)(iterp+1))->pnlinfo.chanpnl.users;
            if ((pnltype==PRIVATE) && (strcmp(cmdnick,nickname)==0)) {
                /* remove the nickname */
                if (dclist_iterate_first(usersp,&iterp)) {
                    do {
                        nickname = *(char**)(iterp+1);
                        if (strcmp(cmdnick,nickname) == 0) {
                            dclist_erase_one_at_iter(usersp, iterp);
                            break;
                        }
                    } while (dclist_iterate_next(usersp,&iterp));
                }
                /* close private panel */
                panelsi = 0;
                del_panel(pnlp->panel);
                delwin(pnlp->window);
                dclist_erase_one_at_iter(&panels, iterp);
                --panelsn;
                break;
            }
        }
        return;
    }
    /* own nick */
    marker = 0;
    panelsi = 0;
    dclist_screate(&clist);
    channel = *(char**)(icmd->params.nextp+1);
    if (dclist_iterate_first(&panels,&iterp)) {
        do {
            pnlp = (PNL*)(iterp+1);
            pnltype = pnlp->pnltype;
            servfd = pnlp->servfd;
            chname = pnlp->pnlinfo.chanpnl.chname;
            if (marker == 0) {
                if ((pnltype==CHANNEL) && (servfd==infd)) {
                    if (strcmp(chname,channel) == 0) {
                        dclist_push_back(&clist, &iterp, sizeof(iterp));
                        marker = 1;
                        --panelsn;
                    }
                }
            } else {
                if ((pnltype==CHANNEL) || (pnltype==SERVER))
                    break;
                dclist_push_back(&clist, &iterp, sizeof(iterp));
                --panelsn;
            }
        } while (dclist_iterate_next(&panels,&iterp));
    }
    if (dclist_iterate_first(&clist,&iterp)) {
        do {
            markp = *(DCLIST**)(iterp+1);
            pnlp = (PNL*)(markp+1);
            del_panel(pnlp->panel);
            delwin(pnlp->window);
            if (pnlp->pnltype == CHANNEL)
                dclist_clear(&pnlp->pnlinfo.chanpnl.users);
            dclist_erase_one_at_iter(&panels, markp);
        } while (dclist_iterate_next(&clist,&iterp));
    }
    dclist_clear(&clist);
    onirc_show_title();
}

/* two types: from nick to channel (public), from nick to nick (private) */
void onirc_iprocess_PRIVMSG(ICOMMAND* icmd) {
    /*
    PNL pnl;
    */
    PNL* pnlp;
    int servfd;
    DCLIST* iterp;
    char* chname;
    char* message;
    char* nickname;
    char* receiver;
    WINDOW* window;
    enum pnltype pnltype;
    /*
    char privmsg[] = "\n<%s> %s";
    */
    char chanmsg[] = "\n<%s> %s";
    receiver = *(char**)(icmd->params.nextp+1);
    if ((receiver[0]=='#') || (receiver[0]=='&')) {
        if (dclist_iterate_first(&panels,&iterp)) {
            do {
                pnlp = (PNL*)(iterp+1);
                pnltype = pnlp->pnltype;
                servfd = pnlp->servfd;
                chname = pnlp->pnlinfo.chanpnl.chname;
                if ((pnltype==CHANNEL) && (servfd==infd)) {
                    if (strcmp(chname,receiver) == 0)
                        break;
                }
            } while (dclist_iterate_next(&panels,&iterp));
        }
        window = pnlp->window;
        nickname = icmd->msginfo.nickmsg.nickname;
        message = *(char**)(icmd->params.nextp->nextp+1);
        wprintw(window, chanmsg, nickname, message);
        if (window == curwin)
            wrefresh(curwin);
        wrefresh(cmdwin);
    } else {
        /* No need to check for my nickname here; every PRIVMSG not for a  */
        /* channel is for me (because I can't see other people's privates) */
        
        
        
        
    }
}

/* someone leaves from irc (all channels) */
void onirc_iprocess_QUIT(ICOMMAND* icmd) {
    
    
    
}
