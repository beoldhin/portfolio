#include "onirc.h"

void onirc_handle_ocommands(void) {
    OCOMMAND ocmd;
    char ucmsg[] = "\nUnknown command";
    char nopmsg[] = "\nInvalid number of parameters";
    onirc_parse_ocommand(&ocmd);
    if (!ocmd.command) {
        onirc_oprocess_message();
        return;
    }
    if (strcmp(ocmd.command,"connect") == 0) {
        if (ocmd.paramsn != 3) {
            noparam:;
            wprintw(statwin, nopmsg);
            if (statwin == curwin)
                wrefresh(curwin);
            onirc_ocommand_dealloc(&ocmd);
            return;
        }
        onirc_oprocess_connect(&ocmd);
    } else if (strcmp(ocmd.command,"disconnect") == 0) {
        if (ocmd.paramsn != 1)
            goto noparam;
        onirc_oprocess_disconnect(&ocmd);
    } else if (strcmp(ocmd.command,"join") == 0) {
        if (ocmd.paramsn != 1)
            goto noparam;
        onirc_oprocess_join(&ocmd);
    } else if (strcmp(ocmd.command,"leave") == 0) {
        if ((ocmd.paramsn!=0) && (ocmd.paramsn!=1))
            goto noparam;
        onirc_oprocess_leave(&ocmd);
    } else if (strcmp(ocmd.command,"msg") == 0) {
        if (ocmd.paramsn != 2)
            goto noparam;
        onirc_oprocess_msg(&ocmd);
    } else if (strcmp(ocmd.command,"rawmsg") == 0) {
        if (ocmd.paramsn != 1)
            goto noparam;
        onirc_oprocess_rawmsg(&ocmd);
    } else {
        wprintw(statwin, ucmsg);
        if (statwin == curwin)
            wrefresh(curwin);
        wrefresh(cmdwin);
    }
    onirc_ocommand_dealloc(&ocmd);
}

void onirc_oprocess_message(void) {
    int servfd;
    char* chname;
    char* nickname;
    char* servnick;
    DCLIST* servp;
    DCLIST* matchp;
    WINDOW* window;
    enum pnltype pnltype;
    char buffer[MAXMSG+2+1];
    char privmsg[] = "\n<%s> %s";
    char chanmsg[] = "\n<%s> %s";
    char noconn[] = "\nNot channel or private window";
    servfd = onirc_window_to_servfd(&servp,&matchp);
    if (servfd == -1) {
        notreqw:;
        wprintw(statwin, noconn);
        if (statwin == curwin)
            wrefresh(curwin);
        return;
    }
    pnltype = ((PNL*)(matchp+1))->pnltype;
    if (pnltype == SERVER)
        goto notreqw;
    servnick = ((PNL*)(servp+1))->pnlinfo.servpnl.nickname;
    if (pnltype == CHANNEL) {
        chname = ((PNL*)(matchp+1))->pnlinfo.chanpnl.chname;
        sprintf(buffer, "PRIVMSG %s :%s\r\n", chname, keybuf);
        onirc_sendall(servfd, buffer, keybufi);
        wprintw(curwin, chanmsg, servnick, keybuf);
    } else {
        nickname = ((PNL*)(matchp+1))->pnlinfo.privpnl.nickname;
        sprintf(buffer, "PRIVMSG %s :%s\r\n", nickname, keybuf);
        onirc_sendall(servfd, buffer, keybufi);
        wprintw(curwin, privmsg, servnick, keybuf);
    }
    window = ((PNL*)(matchp+1))->window;
    if (window == curwin)
        wrefresh(curwin);
    wrefresh(cmdwin);
}

void onirc_oprocess_connect(OCOMMAND* ocmd) {
    PNL pnl;
    int sockfd;
    int servport;
    char* servname;
    char* nickname;
    servname = *(char**)(ocmd->params.nextp+1);
    servport = atoi(*(char**)(ocmd->params.nextp->nextp+1));
    nickname = *(char**)(ocmd->params.nextp->nextp->nextp+1);
    sockfd = onirc_connect(servname, servport, nickname);
    if (sockfd == -1)
        return;
    pnl.pnltype = SERVER;
    pnl.window = newwin(LINES-2, COLS, 1, 0);
    pnl.panel = new_panel(pnl.window);
    pnl.servfd = sockfd;
    strcpy(pnl.pnlinfo.servpnl.servname, servname);
    strcpy(pnl.pnlinfo.servpnl.nickname, nickname);
    pnl.pnlinfo.servpnl.servport = servport;
    pnl.pnlinfo.servpnl.ready = FALSE;
    dclist_push_back(&panels, &pnl, sizeof(pnl));
    FD_SET(sockfd, &masterfds);
    if (sockfd > maxfd)
        maxfd = sockfd;
    ++panelsn;
    onirc_show_title();
}

void onirc_oprocess_disconnect(OCOMMAND* ocmd) {
    
    
    
}

void onirc_oprocess_join(OCOMMAND* ocmd) {
    int servfd;
    DCLIST* servp;
    DCLIST* matchp;
    char buffer[MAXMSG+2+1];
    char noconn[] = "\nNot channel or private window";
    servfd = onirc_window_to_servfd(&servp, &matchp);
    if (servfd == -1) {
        wprintw(statwin, noconn);
        if (statwin == curwin)
            wrefresh(curwin);
        return;
    }
    sprintf(buffer, "JOIN %s\r\n", *(char**)(ocmd->params.nextp+1));
    onirc_sendall(servfd, buffer, strlen(buffer));
}

/*
 * - if channel defined: must be in server, channel or private panel
 * - if no channel defined: must be in channel or private panel
 */
void onirc_oprocess_leave(OCOMMAND* ocmd) {
    int servfd;
    DCLIST* servp;
    DCLIST* matchp;
    char buffer[MAXMSG+2+1];
    char noconn[] = "\nNot channel or private window";
    if (ocmd->paramsn == 1) {
        servfd = onirc_window_to_servfd(&servp, &matchp);
        if (servfd == -1) {
            wprintw(statwin, noconn);
            if (statwin == curwin)
                wrefresh(curwin);
            return;
        }
        sprintf(buffer, "PART %s\r\n", *(char**)(ocmd->params.nextp+1));
        onirc_sendall(servfd, buffer, strlen(buffer));
        return;
    }
    
    
    
}

/*
 * - check if correct target nickname
 * - if correct nickname, create new private panel if not already
 * - if correct nickname, print message to private panel if already
 */
void onirc_oprocess_msg(OCOMMAND* ocmd) {
    
    
    
}

void onirc_oprocess_rawmsg(OCOMMAND* ocmd) {
    int servfd;
    DCLIST* servp;
    DCLIST* matchp;
    char buffer[MAXMSG+2+1];
    char noconn[] = "\nNot channel or private window";
    servfd = onirc_window_to_servfd(&servp, &matchp);
    if (servfd == -1) {
        wprintw(statwin, noconn);
        if (statwin == curwin)
            wrefresh(curwin);
        return;
    }
    sprintf(buffer, "%s\r\n", *(char**)(ocmd->params.nextp+1));
    onirc_sendall(servfd, buffer, strlen(buffer));
}
