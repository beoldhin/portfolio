/*
  TODO:
  - handlers for screen size changers
 */

#include "onirc.h"

void onirc_init_screen(void) {
    PNL pnl;
    char ecmd[] = "\n-- ready --";
    panelsi = 0;
    panelsn = 1;
    initscr();
    noecho();
    /* raw(); */
    curs_set(0);
    dclist_screate(&panels);
    pnl.pnltype = STATUS;
    titlewin = newwin(1, COLS, 0, 0);
    pnl.window = newwin(LINES-2, COLS, 1, 0);
    cmdwin = newwin(1, COLS, LINES-1, 0);
    pnl.panel = new_panel(pnl.window);
    dclist_push_back(&panels, &pnl, sizeof(pnl));
    wbkgd(titlewin, A_REVERSE);
    wbkgd(pnl.window, A_NORMAL);
    wbkgd(cmdwin, A_REVERSE);
    scrollok(titlewin, FALSE);
    scrollok(pnl.window, TRUE);
    scrollok(cmdwin, FALSE);
    keypad(cmdwin, TRUE);
    wclear(titlewin);
    wclear(pnl.window);
    wclear(cmdwin);
    wprintw(pnl.window, ecmd);
    update_panels();
    doupdate();
    wrefresh(titlewin);
    wrefresh(cmdwin);
    statwin = pnl.window;
    curwin = pnl.window;
    onirc_show_title();
}

DCLIST* onirc_show_title(void) {
    BOOL ready;
    int servport;
    char* chname;
    char* nickname;
    char* servname;
    DCLIST* iterp;
    enum pnltype pnltype;
    char statmsg[] = "Status [%u/%u]";
    char servmsg[] = "Server %s, port %d [%u/%u], %s";
    char chanmsg[] = "Channel %s [%u/%u]";
    char nickmsg[] = "Private %s [%u/%u]";
    if (!dclist_iterate_first(&panels,&iterp)) {
        exit(123);
    }
    dclist_iterate_nth_next(&panels, &iterp, panelsi);
    werase(titlewin);
    pnltype = ((PNL*)(iterp+1))->pnltype;
    switch (pnltype) {
      case STATUS:
        wprintw(titlewin, statmsg, panelsi+1, panelsn);
        break;
      case SERVER:
        servname = ((PNL*)(iterp+1))->pnlinfo.servpnl.servname;
        servport = ((PNL*)(iterp+1))->pnlinfo.servpnl.servport;
        ready = ((PNL*)(iterp+1))->pnlinfo.servpnl.ready;
        if (ready)
            wprintw(titlewin, servmsg, servname, servport, panelsi+1, panelsn, "READY");
        else
            wprintw(titlewin, servmsg, servname, servport, panelsi+1, panelsn, "NOT READY");
        break;
      case CHANNEL:
        chname = ((PNL*)(iterp+1))->pnlinfo.chanpnl.chname;
        wprintw(titlewin, chanmsg, chname, panelsi+1, panelsn);
        break;
      case PRIVATE:
        nickname = ((PNL*)(iterp+1))->pnlinfo.privpnl.nickname;
        wprintw(titlewin, nickmsg, nickname, panelsi+1, panelsn);
        break;
      default:
        exit(123);
    }
    wrefresh(titlewin);
    wrefresh(cmdwin);  /* set the cursor to cmdwin */
    return iterp;
}
