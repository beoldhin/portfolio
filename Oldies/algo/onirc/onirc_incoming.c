#include "onirc.h"

void onirc_handle_keyboard(void) {
    DCLIST* iterp;
    int readkey = wgetch(cmdwin);
    if (readkey == '\n') {
        werase(cmdwin);
        wrefresh(cmdwin);
        keybuf[keybufi] = '\0';
        onirc_handle_ocommands();
        keybufi = 0;
        return;
    }
    if ((readkey==KEY_BACKSPACE) || (readkey==0x7F)) {
        if (keybufi <= 0)
            return;
        --keybufi;
        wmove(cmdwin, 0, keybufi);
        wechochar(cmdwin, ' ');
        wmove(cmdwin, 0, keybufi);
        wrefresh(cmdwin);
        return;
    }
    if (readkey == '\t') {
        ++panelsi;
        if (panelsi == panelsn)
            panelsi = 0;
        iterp = onirc_show_title();
        curwin = ((PNL*)(iterp+1))->window;
        top_panel(((PNL*)(iterp+1))->panel);
        update_panels();
        doupdate();
        wrefresh(cmdwin);
        return;
    }
    if (readkey == KEY_UP) {
        return;
    }
    if (readkey == KEY_RIGHT) {
        return;
    }
    if (readkey == KEY_DOWN) {
        return;
    }
    if (readkey == KEY_LEFT) {
        return;
    }
    if (keybufi >= MAXMSG)
        return;
    wechochar(cmdwin, readkey);
    keybuf[keybufi] = readkey;
    ++keybufi;
}

void onirc_handle_socket(void) {
    int servfd;
    DCLIST* iterp;
    WINDOW* window;
    enum pnltype pnltype;
    char pingmsg[] = "PING";
    char pongmsg[] = "\n-- pong sent --";
    msgbufi = onirc_recvln(infd, msgbuf, sizeof(msgbuf)-1);
    if (!msgbufi) {
        onirc_close_server(infd);
        return;
    }
    if (memcmp(msgbuf,pingmsg,sizeof(pingmsg)-1) == 0) {
        msgbuf[1] = 'O';
        onirc_sendall(infd, msgbuf, msgbufi);
        if (dclist_iterate_first(&panels,&iterp)) {
            do {
                pnltype = ((PNL*)(iterp+1))->pnltype;
                servfd = ((PNL*)(iterp+1))->servfd;
                if ((pnltype==SERVER) && (servfd==infd)) {
                    window = ((PNL*)(iterp+1))->window;
                    wprintw(window, pongmsg);
                    if (window == curwin)
                        wrefresh(curwin);
                }
            } while (dclist_iterate_next(&panels,&iterp));
        }
        return;
    }
    
    msgbuf[msgbufi] = '\0';
    fprintf(rawinf, msgbuf);
    fflush(rawinf);
    
    onirc_handle_icommands();
    
}
