#include "onirc.h"

void onirc_parse_ocommand(OCOMMAND* ocmd) {
    UINT i;
    UINT j;
    char* memory;
    ocmd->command = NULL;
    ocmd->paramsn = 0;
    dclist_screate(&ocmd->params);
    for (i=0; i<keybufi; ++i) {  /* find '/' */
        if (keybuf[i] == '/')
            break;
    }
    if (i == keybufi)  /* return if no '/' */
        return;
    while (i < keybufi) {
        for (i=i+1; i<keybufi; ++i) {
            if (!isspace(keybuf[i]))
                break;
        }
        if (i == keybufi)  /* return if no start of command */
            return;
        if (keybuf[i] == '"') {
            j = i + 1;
            for (i=j; i<keybufi; ++i) {
                if (keybuf[i] == '"')
                    break;
            }
            if (i == j)
                continue;
            memory = (char*)malloc(i-j);
        } else {
            j = i;
            do {
                ++i;
            } while ((i<keybufi) && (!isspace(keybuf[i])));
            memory = (char*)malloc(i-j+1);
        }
        memcpy(memory, keybuf+j, i-j);
        memory[i-j] = '\0';
        if (ocmd->command) {
            dclist_push_back(&ocmd->params, &memory, sizeof(memory));
            ++ocmd->paramsn;
        } else {
            ocmd->command = memory;
        }
    }
}

void onirc_parse_icommand(ICOMMAND* icmd) {
    UINT i;
    UINT j;
    UINT k;
    char* param;
    char cmdbuf[MAXCMD+1];
    BOOL isnick = FALSE;
    BOOL ishost = FALSE;
    icmd->msginfo.servmsg.servname[0] = '\0';
    icmd->msginfo.servmsg.command = -1;
    icmd->msginfo.nickmsg.nickname[0] = '\0';
    icmd->msginfo.nickmsg.username[0] = '\0';
    icmd->msginfo.nickmsg.hostname[0] = '\0';
    icmd->msginfo.nickmsg.command[0] = '\0';
    icmd->paramsn = 0;
    dclist_screate(&icmd->params);
    if (msgbufi < 1)
        return;  /* no space for ':' and one character prefix */
    if (msgbuf[0] != ':')
        return;  /* no ':' */
    for (i=1; i<msgbufi; ++i) {
        if (isspace(msgbuf[i]))
            break;
        if (msgbuf[i] == '!') {
            isnick = TRUE;
            j = i;
        } else if (msgbuf[i] == '@') {
            ishost = TRUE;
            k = i;
        }
    }
    if (isnick == TRUE) {
        icmd->msgtype = NICKMSG;
        memcpy(icmd->msginfo.nickmsg.nickname, msgbuf+1, j-1);
        icmd->msginfo.nickmsg.nickname[j-1] = '\0';
        if (ishost) {
            memcpy(icmd->msginfo.nickmsg.username, msgbuf+j+1, k-j-1);
            icmd->msginfo.nickmsg.username[k-j-1] = '\0';
            memcpy(icmd->msginfo.nickmsg.hostname, msgbuf+k+1, i-k-1);
            icmd->msginfo.nickmsg.hostname[i-k-1] = '\0';
        } else {
            memcpy(icmd->msginfo.nickmsg.username, msgbuf+j+1, i-j-1);
            icmd->msginfo.nickmsg.username[i-j-1] = '\0';
        }
    } else {
        icmd->msgtype = SERVMSG;
        memcpy(icmd->msginfo.servmsg.servname, msgbuf+1, i-1);
        icmd->msginfo.servmsg.servname[i-1] = '\0';
    }
    ++i;
    if (i >= msgbufi)
        return;  /* no space for command */
    j = i;
    while (i < msgbufi) {
        if (isspace(msgbuf[i]))
            break;
        ++i;
    }
    memcpy(cmdbuf, msgbuf+j, i-j);
    cmdbuf[i-j] = '\0';
    if (icmd->msgtype == SERVMSG) {
        icmd->msginfo.servmsg.command = atoi(cmdbuf);
    } else {
        memcpy(icmd->msginfo.nickmsg.command, cmdbuf, i-j);
        icmd->msginfo.nickmsg.command[i-j] = '\0';
    }
    for (++i; i<msgbufi; ++i) {
        if (msgbuf[i] == ':') {
            j = i;
            for (++i; i<msgbufi; ++i);
            param = (char*)malloc(i-j);
            memcpy(param, msgbuf+j+1, i-j-1);
            param[i-j-1] = '\0';
            dclist_push_back(&icmd->params, &param, sizeof(param));
            ++icmd->paramsn;
        } else if (!isspace(msgbuf[i])) {
            j = i;
            for (; (i<msgbufi)&&(!isspace(msgbuf[i])); ++i);
            param = (char*)malloc(i-j);
            memcpy(param, msgbuf+j, i-j);
            param[i-j] = '\0';
            dclist_push_back(&icmd->params, &param, sizeof(param));
            ++icmd->paramsn;
        }
    }
}

void onirc_close_server(int sfd) {
    PNL* pnlp;
    int servfd;
    UINT marker;
    DCLIST clist;
    DCLIST* iterp;
    DCLIST* markp;
    enum pnltype pnltype;
    char endmsg[] = "\nConnection implicitly ended";
/* remove from file descriptor set */
    FD_CLR(sfd, &masterfds);
    close(sfd);
/* set current panel to status panel */
    curwin = statwin;
    top_panel(((PNL*)(panels.nextp+1))->panel);
    update_panels();
    doupdate();
    panelsi = 0;
/* first mark all panels to be closed */
/* server panel + channel panels + private panels must be closed */
/* explicitly end the marking loop when new server is detected */
    marker = 0;
    dclist_screate(&clist);
    if (dclist_iterate_first(&panels,&iterp)) {
        do {
            pnlp = (PNL*)(iterp+1);
            pnltype = pnlp->pnltype;
            servfd = pnlp->servfd;
            if (marker == 0) {          /* wanted server not yet reached */
                if ((pnltype==SERVER) && (servfd==sfd)) {
                    dclist_push_back(&clist, &iterp, sizeof(iterp));
                    marker = 1;
                    --panelsn;
                }
            } else if (marker == 1) {  /* wanted server reached */
                if (pnltype == SERVER)
                    break;
                dclist_push_back(&clist, &iterp, sizeof(iterp));
                --panelsn;
            }
        } while (dclist_iterate_next(&panels,&iterp));
    }
/* close all channel and private panels belonging to a server */
/* close the server panel itself */
    if (dclist_iterate_first(&clist,&iterp)) {
        do {
            markp = *(DCLIST**)(iterp+1);
            pnlp = (PNL*)(markp+1);
            del_panel(pnlp->panel);
            delwin(pnlp->window);
            if (pnlp->pnltype == CHANNEL)
                dclist_clear(&pnlp->pnlinfo.chanpnl.users);
            dclist_erase_one_at_iter(&panels, markp);
        } while (dclist_iterate_next(&clist,&iterp));
    }
    dclist_clear(&clist);
    onirc_show_title();
    wprintw(statwin, endmsg);
    wrefresh(statwin);
    wrefresh(cmdwin);
}

int onirc_window_to_servfd(DCLIST** servpp, DCLIST** matchpp) {
    int sfd;
    PNL* pnlp;
    int servfd;
    DCLIST* iterp;
    WINDOW* window;
    enum pnltype pnltype;
    sfd = -1;
    if (dclist_iterate_first(&panels,&iterp)) {
        do {
            pnlp = (PNL*)(iterp+1);
            pnltype = pnlp->pnltype;
            window = pnlp->window;
            servfd = pnlp->servfd;
            if (pnltype == SERVER)
                *servpp = iterp;
            if (window == curwin) {
                if (pnltype != STATUS) {
                    *matchpp = iterp;
                    sfd = servfd;
                    break;
                }
            }
        } while (dclist_iterate_next(&panels,&iterp));
    }
    return sfd;
}

BOOL onirc_check_is_own_inick(ICOMMAND* icmd, DCLIST** iterpp) {
    PNL* pnlp;
    int servfd;
    DCLIST* iterp;
    char* cmdnick;
    char* nickname;
    enum pnltype pnltype;
    cmdnick = icmd->msginfo.nickmsg.nickname;
    if (dclist_iterate_first(&panels,&iterp)) {
        do {
            pnlp = (PNL*)(iterp+1);
            pnltype = pnlp->pnltype;
            servfd = pnlp->servfd;
            nickname = pnlp->pnlinfo.servpnl.nickname;
            if ((pnltype==SERVER) && (servfd==infd)) {
                if (strcmp(nickname,cmdnick) == 0) {
                    *iterpp = iterp;
                    return TRUE;
                }
                *iterpp = iterp;
                return FALSE;
            }
        } while (dclist_iterate_next(&panels,&iterp));
    }
    *iterpp = NULL;
    return FALSE;
}

void onirc_ocommand_dealloc(OCOMMAND* ocmd) {
    dclist_clear_advanced(&ocmd->params, onirc_user_dealloc);
}

void onirc_icommand_dealloc(ICOMMAND* icmd) {
    dclist_clear_advanced(&icmd->params, onirc_user_dealloc);
}

void onirc_user_dealloc(void* satp) {
    free(*(void**)satp);
}
