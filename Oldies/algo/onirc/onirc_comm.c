#include "onirc.h"

int onirc_connect(char* servname, int servport, char* nickname) {
    int sockfd;
    struct hostent* he;
    char errmsg[] = "\n%s: %s";
    char buffer[MAXMSG+2+1];
    struct sockaddr_in serv_addr;
    char sendmsg1[] = "\nSending nickname...";
    char sendmsg2[] = "\nSending username...";
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        wprintw(statwin, errmsg, "socket", strerror(errno));
        if (statwin == curwin)
            wrefresh(curwin);
        return -1;
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(servport);
    he = gethostbyname(servname);
    if (!he) {
        wprintw(statwin, errmsg, "gethostbyname", strerror(errno));
        if (statwin == curwin)
            wrefresh(curwin);
        return -1;
    }
    serv_addr.sin_addr = *((struct in_addr*)he->h_addr);
    memset(&(serv_addr.sin_zero), 0x00, sizeof(serv_addr.sin_zero));
    if (connect(sockfd,(struct sockaddr*)&serv_addr,
    sizeof(struct sockaddr)) == -1) {
        wprintw(statwin, errmsg, "connect", strerror(errno));
        if (statwin == curwin)
            wrefresh(curwin);
        return -1;
    }
    sprintf(buffer, "NICK %s\r\n", nickname);
    wprintw(statwin, sendmsg1, buffer);
    if (statwin == curwin)
        wrefresh(curwin);
    onirc_sendall(sockfd, buffer, strlen(buffer));
    sprintf(buffer, "USER olli frodo: Olli\r\n");
    wprintw(statwin, sendmsg2, buffer);
    if (statwin == curwin)
        wrefresh(curwin);
    wrefresh(cmdwin);
    onirc_sendall(sockfd, buffer, strlen(buffer));
    return sockfd;
}

int onirc_recvln(int sfd, void* buf, size_t len) {
    int nrecv;
    char* msgp;
    char* msgep;
    char prevch;
    prevch = '\0';
    msgp = (char*)buf;
    msgep = msgp + len;
    while (msgp < msgep) {
        nrecv = recv(sfd, msgp, 1, 0);
        if ((nrecv==0) || (nrecv==-1))
            return nrecv;
        if ((prevch=='\r') && (*msgp=='\n'))
            return (msgp+1) - (char*)buf;
        prevch = *msgp;
        ++msgp;
    }
    return (msgp-1) - (char*)buf;
}

int onirc_sendall(int sfd, void* buf, size_t len) {
    int nsent;
    int ntotal = 0;
    UINT nleft = len;
    while (ntotal < len) {
        nsent = send(sfd, (char*)buf+ntotal, nleft, 0);
        if (nsent == -1)
            return -1;
        ntotal += nsent;
        nleft -= nsent;
    }
    return ntotal;
}
