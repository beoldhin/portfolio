#include <ctype.h>
#include <errno.h>
#include <netdb.h>
#include <panel.h>
#include <curses.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "/home/olli/src/algo/list/dclist.h"

#define MAXMSG 510
#define MAXCHA 200
#define MAXHOST 64
#define MAXNICK 9
#define MAXUSER 16  /* 8, but 16 for future */
#define MAXCMD 8    /* USERHOST */

enum pnltype {
    STATUS,
    SERVER,
    CHANNEL,
    PRIVATE
};

enum imsgtype {
    SERVMSG,
    NICKMSG
};

typedef struct pnl {
    enum pnltype pnltype;
    WINDOW* window;
    PANEL* panel;
    int servfd;
    /* STATUS: no need for servfd */
    /* SERVER: mandatory to receive / send messages */
    /* CHANNEL: preferred to simplify loops */
    /* PRIVATE: preferred to simplify loops */
    union {
        struct SERVPNL {  /* server panel */
            char servname[MAXHOST+1];
            char nickname[MAXNICK+1];
            int servport;
            BOOL ready;
        } servpnl;
        struct CHANPNL {  /* channel panel */
            char chname[MAXCHA+1];
            UINT usersn;
            DCLIST users;
        } chanpnl;
        struct PRIVPNL {  /* private panel */
            char nickname[MAXNICK+1];
        } privpnl;
    } pnlinfo;
} PNL;

typedef struct ocommand {
    char* command;
    UINT paramsn;
    DCLIST params;
} OCOMMAND;

typedef struct icommand {
    enum imsgtype msgtype;
    union {
        struct SERVMSG {
            char servname[MAXHOST+1];
            int command;
        } servmsg;
        struct NICKMSG {
            char nickname[MAXNICK+1];
            char username[MAXUSER+1];
            char hostname[MAXHOST+1];
            char command[MAXCMD+1];
        } nickmsg;
    } msginfo;
    UINT paramsn;
    DCLIST params;
} ICOMMAND;

/* main_onirc.c */
void onirc_exit(int);

/* onirc_screen.c */
void onirc_init_screen(void);
DCLIST* onirc_show_title(void);

/* onirc_incoming.c */
void onirc_handle_keyboard(void);
void onirc_handle_socket(void);

/* onirc_comm.c */
int onirc_connect(char*, int, char*);
int onirc_recvln(int, void*, size_t);
int onirc_sendall(int, void*, size_t);

/* onirc_helper.c */
void onirc_parse_ocommand(OCOMMAND*);
void onirc_parse_icommand(ICOMMAND*);
void onirc_close_server(int);
int onirc_window_to_servfd(DCLIST**, DCLIST**);
BOOL onirc_check_is_own_inick(ICOMMAND*, DCLIST**);
void onirc_ocommand_dealloc(OCOMMAND*);
void onirc_icommand_dealloc(ICOMMAND*);
void onirc_user_dealloc(void*);

/* onirc_ocommands.c */
void onirc_handle_ocommands(void);
void onirc_oprocess_message(void);
void onirc_oprocess_connect(OCOMMAND*);
void onirc_oprocess_disconnect(OCOMMAND*);
void onirc_oprocess_join(OCOMMAND*);
void onirc_oprocess_leave(OCOMMAND*);
void onirc_oprocess_msg(OCOMMAND*);
void onirc_oprocess_rawmsg(OCOMMAND*);

/* onirc_icommands.c */
void onirc_handle_icommands(void);
void onirc_iprocess_RPL_NAMREPLY(ICOMMAND*);
void onirc_iprocess_RPL_ENDOFMOTD(ICOMMAND*);
void onirc_iprocess_JOIN(ICOMMAND*);
void onirc_iprocess_NICK(ICOMMAND*);
void onirc_iprocess_PART(ICOMMAND*);
void onirc_iprocess_PRIVMSG(ICOMMAND*);
void onirc_iprocess_QUIT(ICOMMAND*);

int infd;
int maxfd;
int keybufi;
int msgbufi;
UINT panelsi;
UINT panelsn;
DCLIST panels;
WINDOW* curwin;
WINDOW* titlewin;
WINDOW* statwin;
WINDOW* cmdwin;
fd_set masterfds;
char keybuf[MAXMSG+2+1];
char msgbuf[MAXMSG+2+1];

FILE* rawinf;
