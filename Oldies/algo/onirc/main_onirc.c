/*
  - For incoming messages whose servername is not known, use gethostbyname
    to find alternative names (aliases). Update the server information
    matching this alternative.
 */

#include "onirc.h"

#define STDIN 0

int main(void) {
    fd_set readfds;
    
    rawinf = fopen("rawin.log", "a");
    
    maxfd = STDIN;
    onirc_init_screen();
    FD_ZERO(&readfds);
    FD_ZERO(&masterfds);
    FD_SET(STDIN, &masterfds);
    signal(SIGHUP, onirc_exit);
    signal(SIGINT, onirc_exit);
    signal(SIGQUIT, onirc_exit);
    signal(SIGILL, onirc_exit);
    signal(SIGABRT, onirc_exit);
    signal(SIGFPE, onirc_exit);
    signal(SIGTERM, onirc_exit);
    while (1) {
        readfds = masterfds;
        if (select(maxfd+1,&readfds,NULL,NULL,NULL) == -1) {
            perror("select");
            return -1;
        }
        for (infd=0; infd<=maxfd; ++infd) {
            if (FD_ISSET(infd,&readfds)) {
                if (infd == STDIN) {  /* data coming from the keyboard */
                    onirc_handle_keyboard();
                } else {              /* data coming from a receiving socket */
                    onirc_handle_socket();
                }
            }
        }
    }
    return 0;
}

void onirc_exit(int sig) {
    endwin();
    fprintf(stderr, "Signal trapped, ending...\n");
    exit(sig);
}
