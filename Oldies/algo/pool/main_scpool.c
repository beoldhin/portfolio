#include <stdio.h>
#include "scpool.h"


int main(void) {
    int i;
    int mydata;
    int* mydatap;
    void* allocd;
    
    SCPOOL mypool;
    SCPOOL* mypoolp;
    SCPITER myiter;
    
    mypoolp = &mypool;
    mydatap = &mydata;
    scpool_screate(mypoolp, 10, sizeof(int));
    
    mydata = 1;
    for (i=0; i<495; ++i) {
        allocd = scpool_alloc(mypoolp);
        *((int*)allocd) = i;
        ++mydata;
    }
    printf("%8X\n", (unsigned)allocd);
    
    scpool_reset(mypoolp);
    
    for (i=0; i<495; ++i) {
        allocd = scpool_alloc(mypoolp);
        *((int*)allocd) = i;
        ++mydata;
    }
    printf("%8X\n", (unsigned)allocd);

    if (scpool_iterate_first(mypoolp,&myiter)) {
        do {
            printf("%d\n", *(int*)(myiter.curstat));
        } while (scpool_iterate_next(mypoolp,&myiter));
    }

    scpool_iterate_last(mypoolp, &myiter);
    do {
        printf("%d\n", *(int*)(myiter.curstat));
    } while (scpool_iterate_prev(mypoolp,&myiter));
    
    scpool_clear(mypoolp);
    
    return 0;
}
