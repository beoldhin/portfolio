#ifndef _SCDPOOL_H
#define _SCDPOOL_H

/* Circular static pool implementation.                          */
/* Similar to simple scpool implementation but also has dealloc. */
/* This version consumes O(n) more memory than simple scpool!    */

#include <string.h>
#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

#define FREE 0
#define USED 1

typedef struct scdnode {
    struct scdnode* prevp;
    struct scdnode* nextp;
} SCDNODE,SCDLIST;

typedef struct scdpool {
    SCDNODE list;  /* keep this the very first */
    UINT statnum;
    UINT statsize;
} SCDPOOL;

#ifdef TEST_COMPILATION
  #define SCDPOOL_SCREATE
  #define SCDPOOL_HCREATE
  #define SCDPOOL_ALLOC
  #define SCDPOOL_DEALLOC
  #define SCDPOOL_RESET
  #define SCDPOOL_REFRESH
  #define SCDPOOL_CLEAR
  #define SCDPOOL_DESTROY
#endif

#ifdef SCDPOOL_SCREATE
void scdpool_screate(SCDPOOL*, UINT, UINT);
#endif
#ifdef SCDPOOL_HCREATE
SCDPOOL* scdpool_hcreate(UINT, UINT);
#endif
#ifdef SCDPOOL_ALLOC
void* scdpool_alloc(SCDPOOL*);
#endif
#ifdef SCDPOOL_DEALLOC
BOOL scdpool_dealloc(SCDPOOL*, void*);
#endif
#ifdef SCDPOOL_RESET
void scdpool_reset(SCDPOOL*);
#endif
#ifdef SCDPOOL_REFRESH
void scdpool_refresh(SCDPOOL*);
#endif
#ifdef SCDPOOL_CLEAR
void scdpool_clear(SCDPOOL*);
#endif
#ifdef SCDPOOL_DESTROY
void scdpool_destroy(SCDPOOL*);
#endif

#endif  /* _SCDPOOL_H */
