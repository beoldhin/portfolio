#ifndef _SCPOOL_H
#define _SCPOOL_H

/* Circular static pool implementation.                          */
/* Similar to simple scpool implementation but also has dealloc. */
/* This version consumes O(n) more memory than simple scpool!    */

#include <string.h>
#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct scnode {
    struct scnode* prevp;
    struct scnode* nextp;
} SCNODE,SCLIST;

typedef struct scpiter {
    struct scnode* nodep;
    void* curstat;
} SCPITER;

typedef struct scpool {
    SCNODE list;  /* keep this the very first */
    SCNODE* curpool;
    UINT statall;
    UINT statsize;
} SCPOOL;

#ifdef TEST_COMPILATION
  #define SCPOOL_SCREATE
  #define SCPOOL_HCREATE
  #define SCPOOL_ALLOC
  #define SCPOOL_ITERATE_FIRST
  #define SCPOOL_ITERATE_LAST
  #define SCPOOL_ITERATE_NEXT
  #define SCPOOL_ITERATE_PREV
  #define SCPOOL_RESET
  #define SCPOOL_CLEAR
  #define SCPOOL_DESTROY
#endif

#ifdef SCPOOL_SCREATE
void scpool_screate(SCPOOL*, UINT, UINT);
#endif
#ifdef SCPOOL_HCREATE
SCPOOL* scpool_hcreate(UINT, UINT);
#endif
#ifdef SCPOOL_ALLOC
void* scpool_alloc(SCPOOL*);
#endif
#ifdef SCPOOL_ITERATE_FIRST
BOOL scpool_iterate_first(SCPOOL*, SCPITER*);
#endif
#ifdef SCPOOL_ITERATE_LAST
BOOL scpool_iterate_last(SCPOOL*, SCPITER*);
#endif
#ifdef SCPOOL_ITERATE_NEXT
void* scpool_iterate_next(SCPOOL*, SCPITER*);
#endif
#ifdef SCPOOL_ITERATE_PREV
void* scpool_iterate_prev(SCPOOL*, SCPITER*);
#endif
#ifdef SCPOOL_RESET
void scpool_reset(SCPOOL*);
#endif
#ifdef SCPOOL_CLEAR
void scpool_clear(SCPOOL*);
#endif
#ifdef SCPOOL_DESTROY
void scpool_destroy(SCPOOL*);
#endif

#endif  /* _SCPOOL_H */
