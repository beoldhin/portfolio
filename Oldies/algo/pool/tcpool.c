/* Employs lazy delete triggering - empty pool nodes are cleared only    */
/* explicitly to save clock cycles.                                      */

/* The pools are traversed backwards when finding an empty slot because  */
/* it is statistically more probable to find an empty slot in real-world */
/* "push_back" cases when traversion from end to start.                  */

#include <limits.h>
#include "tcpool.h"

/***********************************************************************
 * Function      : tcpool_screate
 * Time complex. : O(1) without heap allocation
 * Description   :
 * Parameters    : poolp:
 *                 statsize:
 * Return values : None
 **********************************************************************/
#ifdef TCPOOL_SCREATE
void tcpool_screate(TCPOOL* poolp, UINT statsize) {
    poolp->list.prevp = (TCNODE*)poolp;
    poolp->list.nextp = (TCNODE*)poolp;
    poolp->statsize = statsize;
}
#endif

/***********************************************************************
 * Function      : tcpool_hcreate
 * Time complex. : O(1) with heap allocation
 * Description   :
 * Parameters    : statsize:
 * Return values : Non-NULL:
 *                 NULL:
 **********************************************************************/
#ifdef TCPOOL_HCREATE
TCPOOL* tcpool_hcreate(UINT statsize) {
    TCPOOL* new_pool;
    new_pool = (TCPOOL*)malloc(sizeof(TCPOOL));
    if (new_pool == NULL)
        return NULL;
    new_pool->list.prevp = (TCNODE*)new_pool;
    new_pool->list.nextp = (TCNODE*)new_pool;
    new_pool->statsize = statsize;
    return new_pool;
}
#endif

/***********************************************************************
 * Function      : tcpool_alloc
 * Time complex. : O(n), where n is the first pool having enough space
 *                 O(1) with heap allocation if not enough space
 * Description   :
 * Parameters    : tcpoolp:
 *                 strl:
 * Return values : Non-NULL:
 *                 NULL:
 **********************************************************************/
/* Every pool node contains the following information: */
/* 1) TCNODE (prevp & nextp)                           */
/* 2) Number_of_bytes_this_node_can_hold (UINT)        */
/* 3) Number_of_bytes_this_node_currently_uses (UINT)  */
#ifdef TCPOOL_ALLOC
char* tcpool_alloc(TCPOOL* tcpoolp, UINT strl) {
    /* 1) If enough space in p->prevp, insert there           */
    /* 2) If not enough space in p->prevp, scan for worst fit */
    /* 3) If worst fit, insert there                          */
    /* 4) If no worst fit, create new node, insert there      */
    char* retval;
    UINT worstc;
    UINT statsize;
    UINT* totbytes;
    UINT* curbytes;
    TCLIST* poolc;
    TCNODE* worstp;
    TCNODE* curnode;
    TCNODE* new_node;
    poolc = (TCLIST*)tcpoolp;
    statsize = tcpoolp->statsize;
    /* Check is any pool node full */
    curnode = poolc->prevp;
    if (curnode != poolc) {
        totbytes = (UINT*)(curnode+1);
        curbytes = totbytes + 1;
        if (*totbytes-*curbytes >= strl) {
            retval = (char*)(curbytes+1) + *curbytes;
            *curbytes += strl;
            return retval;
        } else {
            /* Worst fit scanning */
            worstc = UINT_MAX;
            worstp = NULL;
            curnode = curnode->prevp;
            while (curnode != poolc) {
                totbytes = (UINT*)(curnode+1);
                curbytes = totbytes + 1;
                if (*curbytes < worstc) {
                    worstc = *totbytes - *curbytes;
                    worstp = curnode;
                }
                curnode = curnode->prevp;
            }
            if (worstp!=NULL && worstc>=strl) {
                totbytes = (UINT*)(worstp+1);
                curbytes = totbytes + 1;
                retval = (char*)(curbytes+1) + *curbytes;
                *curbytes += strl;
                return retval;
            }
        }
    }
    /* Pool was full, alloc new node */
    if (strl > statsize)
        statsize = strl * strl;
    new_node = (TCNODE*)malloc(sizeof(TCNODE)+(sizeof(UINT)*2)+statsize);
    if (new_node == NULL)
        return NULL;
    /* Push back */
    new_node->prevp = poolc->prevp;
    new_node->nextp = poolc;
    poolc->prevp->nextp = new_node;
    poolc->prevp = new_node;
    *(UINT*)(new_node+1) = statsize;
    *((UINT*)(new_node+1)+1) = strl;
    return (char*)(new_node+1)+(sizeof(UINT)*2);
}
#endif

/***********************************************************************
 * Function      : tcpool_clear
 * Time complex. : O(n) traversing +
 *                 O(n) with heap deallocation
 * Description   :
 * Parameters    : tcpoolp:
 * Return values : None
 **********************************************************************/
#ifdef TCPOOL_CLEAR
void tcpool_clear(TCPOOL* tcpoolp) {
    TCNODE* oldnode;
    TCNODE* curnode;
    TCNODE* endnode;
    endnode = (TCNODE*)tcpoolp;
    curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    tcpoolp->list.prevp = endnode;
    tcpoolp->list.nextp = endnode;
}
#endif

/***********************************************************************
 * Function      : tcpool_destroy
 * Time complex. : O(n) traversing +
 *                 O(n+1) with heap deallocation
 * Description   :
 * Parameters    : tcpoolp:
 * Return values : None
 **********************************************************************/
#ifdef TCPOOL_DESTROY
void tcpool_destroy(TCPOOL* tcpoolp) {
    TCNODE* oldnode;
    TCNODE* curnode;
    TCNODE* endnode;
    endnode = (TCNODE*)tcpoolp;
    curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    free(tcpoolp);
}
#endif
