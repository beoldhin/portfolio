#ifndef _TCPOOL_H
#define _TCPOOL_H

/* Circular static pool implementation.                          */
/* Similar to simple scpool implementation but also has dealloc. */
/* This version consumes O(n) more memory than simple scpool!    */

#include <string.h>
#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct tcnode {
    struct tcnode* prevp;
    struct tcnode* nextp;
} TCNODE,TCLIST;

typedef struct tcpool {
    TCNODE list;  /* keep this the very first */
    UINT statsize;
} TCPOOL;

#ifdef TEST_COMPILATION
  #define TCPOOL_SCREATE
  #define TCPOOL_HCREATE
  #define TCPOOL_ALLOC
  #define TCPOOL_CLEAR
  #define TCPOOL_DESTROY
#endif

#ifdef TCPOOL_SCREATE
void tcpool_screate(TCPOOL*, UINT);
#endif
#ifdef TCPOOL_HCREATE
TCPOOL* tcpool_hcreate(UINT);
#endif
#ifdef TCPOOL_ALLOC
char* tcpool_alloc(TCPOOL*, UINT);
#endif
#ifdef TCPOOL_CLEAR
void tcpool_clear(TCPOOL*);
#endif
#ifdef TCPOOL_DESTROY
void tcpool_destroy(TCPOOL*);
#endif

#endif  /* _TCPOOL_H */
