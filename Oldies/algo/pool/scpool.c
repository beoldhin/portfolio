/* Employs lazy delete triggering - empty pool nodes are cleared only    */
/* explicitly to save clock cycles.                                      */

/* The pools are traversed backwards when finding an empty slot because  */
/* it is statistically more probable to find an empty slot in real-world */
/* "push_back" cases when traversion from end to start.                  */

#include <stdio.h>
#include "scpool.h"

/***********************************************************************
 * Function      : scpool_create
 * Time complex. : O(1) without heap allocation
 * Description   : 
 * Parameters    : poolp:    pointer to the static pool
 *                 statnum:  number of static-sized objects in one pool
 *                 statsize: size of one static-sized object in the pool
 * Return values : None
 **********************************************************************/
#ifdef SCPOOL_SCREATE
void scpool_screate(SCPOOL* scpoolp, UINT statnum, UINT statsize) {
    scpoolp->list.prevp = (SCNODE*)scpoolp;
    scpoolp->list.nextp = (SCNODE*)scpoolp;
    scpoolp->curpool = (SCNODE*)scpoolp;
    scpoolp->statall = statnum * statsize;
    scpoolp->statsize = statsize;
}
#endif

/***********************************************************************
 * Function      : scpool_hcreate
 * Time complex. : O(1) with heap allocation
 * Description   : 
 * Parameters    : statnum:  number of static-sized objects in one pool
 *                 statsize: size of one static-sized object in the pool
 * Return values : Non-NULL:
 *                 NULL:
 **********************************************************************/
#ifdef SCPOOL_HCREATE
SCPOOL* scpool_hcreate(UINT statnum, UINT statsize) {
    SCPOOL* new_pool;
    new_pool = (SCPOOL*)malloc(sizeof(SCPOOL));
    if (new_pool == NULL)
        return NULL;
    new_pool->list.prevp = (SCNODE*)new_pool;
    new_pool->list.nextp = (SCNODE*)new_pool;
    new_pool->curpool = (SCNODE*)new_pool;
    new_pool->statall = statnum * statsize;
    new_pool->statsize = statsize;
    return new_pool;
}
#endif

/***********************************************************************
 * Function      : scpool_alloc
 * Time complex. : O(1) without heap allocation if enough storage
 *                 O(1) with heap allocation if not enough storage
 * Description   : 
 * Parameters    : scpoolp:  pointer to the static pool
 * Return values : Non-NULL:
 *                 NULL:
 **********************************************************************/
/* Every pool node contains the following information: */
/* 1) SCNODE (prevp & nextp)                           */
/* 2) Size_of_allocs_in_this_node (UINT)               */
/* 3) Satellite_for_this_node (statnum*statsize)       */
/* Returns NULL if out of memory                       */
#ifdef SCPOOL_ALLOC
void* scpool_alloc(SCPOOL* scpoolp) {
    void* retval;
    UINT statall;
    UINT statsize;
    UINT* curstatp;
    SCLIST* poolc;
    SCNODE* curnode;
    SCNODE* curpool;
    SCNODE* new_node;
    poolc = (SCLIST*)scpoolp;
    statall = scpoolp->statall;
    statsize = scpoolp->statsize;
    /* Check is the last pool full */
    curnode = scpoolp->curpool;
    if (curnode != poolc) {
        curstatp = (UINT*)(curnode+1);
        if (*curstatp < statall) {
            retval = (char*)(curstatp+1) + *curstatp;
            *curstatp += statsize;
            return retval;
        }
    }
    /* Pool was full, alloc new pool */
    curpool = scpoolp->curpool;
    if (curpool == poolc->prevp) {
        new_node = (SCNODE*)malloc(sizeof(SCNODE)+sizeof(UINT)+statall);
        if (new_node == NULL)
            return NULL;
        /* Push back */
        new_node->prevp = curpool;  /* here curpool is poolc->prevp */
        new_node->nextp = poolc;
        poolc->prevp->nextp = new_node;
        poolc->prevp = new_node;
        curpool = new_node;
    } else
        curpool = curpool->nextp;
    scpoolp->curpool = curpool;
    *(UINT*)(curpool+1) = statsize;
    return (char*)(curpool+1) + sizeof(UINT);
}
#endif

/***********************************************************************
 * Function      : scpool_iterate_first
 * Time complex. : O(1)
 * Description   : 
 * Parameters    : scpoolp: pointer to the static pool
 *                 iterp:
 * Return values : FALSE:
 *                 TRUE:
 **********************************************************************/
#ifdef SCPOOL_ITERATE_FIRST
BOOL scpool_iterate_first(SCPOOL* scpoolp, SCPITER* iterp) {
    if (scpoolp->list.nextp == (SCNODE*)scpoolp)
        return FALSE;
    iterp->nodep = scpoolp->list.nextp;
    iterp->curstat = (UINT*)(iterp->nodep+1) + 1;
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : scpool_iterate_last
 * Time complex. : O(1)
 * Description   : 
 * Parameters    : scpoolp: pointer to the static pool
 *                 iterp:   iterator (pointer) to the pool
 * Return values : FALSE:
 *                 TRUE:
 **********************************************************************/
#ifdef SCPOOL_ITERATE_LAST
BOOL scpool_iterate_last(SCPOOL* scpoolp, SCPITER* iterp) {
    UINT curstat;
    if (scpoolp->list.nextp == (SCNODE*)scpoolp)
        return FALSE;
    curstat = *(UINT*)(iterp->nodep+1);
    iterp->nodep = scpoolp->list.prevp;
    iterp->curstat = (char*)((UINT*)(iterp->nodep+1)+1) + curstat -
    scpoolp->statsize;
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : scpool_iterate_next
 * Time complex. : O(1)
 * Description   : 
 * Parameters    : scpoolp:  pointer to the static pool
 *                 iterp:    iterator (pointer) to the pool
 * Return values : Non-NULL:
 *                 NULL:
 **********************************************************************/
#ifdef SCPOOL_ITERATE_NEXT
void* scpool_iterate_next(SCPOOL* scpoolp, SCPITER* iterp) {
    void* eon;
    UINT curstat;
    SCNODE* nodep;
    nodep = iterp->nodep;
    if (nodep != (SCNODE*)scpoolp) {
        curstat = *((UINT*)(nodep+1));
        eon = (char*)(nodep+1) + sizeof(UINT) + (curstat-scpoolp->statsize);
        if (iterp->curstat < eon) {
            iterp->curstat = (char*)(iterp->curstat) + scpoolp->statsize;
            return iterp->curstat;
        }
        nodep = nodep->nextp;
        if (nodep != (SCNODE*)scpoolp) {
            iterp->nodep = nodep;
            iterp->curstat = (char*)(nodep+1) + sizeof(UINT);
            return iterp->curstat;
        }
    }
    return NULL;
}
#endif

/***********************************************************************
 * Function      : scpool_iterate_prev
 * Time complex. : O(1)
 * Description   : 
 * Parameters    : scpoolp:  pointer to the static pool
 *                 iterp:    iterator (pointer) to the pool
 * Return values : Non-NULL:
 *                 NULL:
 **********************************************************************/
#ifdef SCPOOL_ITERATE_PREV
void* scpool_iterate_prev(SCPOOL* scpoolp, SCPITER* iterp) {
    void* son;
    SCNODE* nodep;
    nodep = iterp->nodep;
    if (nodep != (SCNODE*)scpoolp) {
        son = (char*)(nodep+1) + sizeof(UINT);
        if (iterp->curstat > son) {
            iterp->curstat = (char*)(iterp->curstat) - scpoolp->statsize;
            return iterp->curstat;
        }
        nodep = nodep->prevp;
        if (nodep != (SCNODE*)scpoolp) {
            iterp->nodep = nodep;
            iterp->curstat = (char*)(nodep+1) + sizeof(UINT) +
            (scpoolp->statall-scpoolp->statsize);
            return iterp->curstat;
        }
    }
    return NULL;
}
#endif

/***********************************************************************
 * Function      : scpool_reset
 * Time complex. : O(n) traversing,
 *                 where n is the number of nodes in the pool
 * Description   : 
 * Parameters    : scpoolp: pointer to the static pool
 * Return values : None
 **********************************************************************/
/* this function should be called when old pool set should be destroyed,     */
/* and a new data allocating the same amount is going to fill the same pools */
/* while the freeing of the big pools is not too time consuming, this is     */
/* even less time consuming (most of the time is spent inside "free")        */
#ifdef SCPOOL_RESET
void scpool_reset(SCPOOL* scpoolp) {
    SCNODE* curnode;
    SCNODE* endnode;
    endnode = (SCNODE*)scpoolp;
    curnode = endnode->nextp;
    while (curnode != endnode) {
        *(UINT*)(curnode+1) = 0;
        curnode = curnode->nextp;
    }
    scpoolp->curpool = scpoolp->list.nextp;
}
#endif

/***********************************************************************
 * Function      : scpool_clear
 * Time complex. : O(n) traversing +
 *                 O(n) with heap deallocation,
 *                 where n is the number of nodes in the pool
 * Description   : 
 * Parameters    : scpoolp: pointer to the static pool
 * Return values : None
 **********************************************************************/
/* this function destroys the pool set but leaves the 'skeleton' alone */
#ifdef SCPOOL_CLEAR
void scpool_clear(SCPOOL* scpoolp) {
    SCNODE* oldnode;
    SCNODE* curnode;
    SCNODE* endnode;
    endnode = (SCNODE*)scpoolp;
    curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    scpoolp->list.prevp = endnode;
    scpoolp->list.nextp = endnode;
    scpoolp->curpool = endnode;
}
#endif

/***********************************************************************
 * Function      : scpool_destroy
 * Time complex. : O(n) traversing +
 *                 O(n+1) with heap deallocation,
 *                 where n is the number of nodes in the pool
 * Description   : 
 * Parameters    : scpoolp: pointer to the static pool
 * Return values : None
 **********************************************************************/
/* this function completely destroys the pool set */
#ifdef SCPOOL_DESTROY
void scpool_destroy(SCPOOL* scpoolp) {
    SCNODE* oldnode;
    SCNODE* curnode;
    SCNODE* endnode;
    endnode = (SCNODE*)scpoolp;
    curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    free(scpoolp);
}
#endif
