#include <stdio.h>
#include "scdpool.h"

int main(void) {
    int i;
    int mydata;
    void* allocd;

    SCDPOOL mypool;
    SCDPOOL* mypoolp;
    
    mypoolp = &mypool;
    scdpool_screate(mypoolp, 100, sizeof(int));
    
    mydata = 1;
    for (i=0; i<500; ++i) {
        allocd = scdpool_alloc(mypoolp);
        memcpy(allocd, &mydata, sizeof(int));
        scdpool_dealloc(mypoolp, allocd);
        ++mydata;
    }
    
    scdpool_refresh(mypoolp);
    
    scdpool_clear(mypoolp);
    
    return 0;
}
