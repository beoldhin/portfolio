/* Employs lazy delete triggering - empty pool nodes are cleared only    */
/* explicitly to save clock cycles.                                      */

/* The pools are traversed backwards when finding an empty slot because  */
/* it is statistically more probable to find an empty slot in real-world */
/* "push_back" cases when traversing from end to start.                  */

/* NOTE: Users calling functions scbpool_screate and scbpool_hcreate     */
/* should set statnum to a number divisible by 8 (e.g. 8, 16,24,32)!     */

#include "scbpool.h"


#ifdef SCBPOOL_SCREATE
void scbpool_screate(SCBPOOL* scbpoolp, UINT statnum, UINT statsize) {
    scbpoolp->list.prevp = (SCBNODE*)scbpoolp;
    scbpoolp->list.nextp = (SCBNODE*)scbpoolp;
    scbpoolp->statnum = statnum > 7 ? (statnum/8)*8 : 8;
    scbpoolp->statsize = statsize;
}
#endif

#ifdef SCBPOOL_HCREATE
SCBPOOL* scbpool_hcreate(UINT statnum, UINT statsize) {
    SCBPOOL* new_pool = (SCBPOOL*)malloc(sizeof(SCBPOOL));
    if (new_pool == NULL)
        return NULL;
    new_pool->list.prevp = (SCBNODE*)new_pool;
    new_pool->list.nextp = (SCBNODE*)new_pool;
    new_pool->statnum = statnum > 7 ? (statnum/8)*8 : 8;
    new_pool->statsize = statsize;
    return new_pool;
}
#endif

/*
 * Every pool node contains the following information:
 * 1) SCBNODE (prevp & nextp)
 * 2) Num_of_allocs_in_this_node (UINT)
 * 3) Alloc_info_for_this_node ((statnum/8)*sizeof(UINT8))
 * 4) Satellite_for_this_node (statnum*statsize)
*/
#ifdef SCBPOOL_ALLOC
void* scbpool_alloc(SCBPOOL* scbpoolp) {
    UINT rem;
    UINT8 orp;
    UINT8 info;
    UINT allocsn;
    SCBNODE* curnode;
    SCBNODE* new_node;
    UINT8* allocinfo;
    UINT8* allocinfs = NULL;
    UINT8* allocinfe = NULL;
    SCBLIST* poolc = (SCBLIST*)scbpoolp;
    UINT statnum = scbpoolp->statnum;
    UINT statsize = scbpoolp->statsize;
    /* try to find the first empty static slot */
    for (curnode=poolc->prevp; curnode!=poolc; curnode=curnode->prevp) {
        allocsn = *(UINT*)(curnode+1);
        if (allocsn < statnum) {
            allocinfo = (UINT8*)(curnode+1) + sizeof(UINT);
            allocinfe = allocinfo + (statnum/8);
            for (allocinfs=allocinfo; allocinfs<allocinfe; ++allocinfs) {
                if (*allocinfs != 0xFF)
                    goto found;
            }
        }
    }
    /* empty slot was not found, alloc whole pool */
    new_node = (SCBNODE*)malloc(sizeof(SCBNODE)+sizeof(UINT)+((statnum/8)*
    sizeof(UINT8))+(statnum*statsize));
    if (new_node == NULL)
        return NULL;
    /* push back */
    new_node->prevp = poolc->prevp;
    new_node->nextp = poolc;
    poolc->prevp->nextp = new_node;
    poolc->prevp = new_node;
    allocinfo = (UINT8*)(new_node+1) + sizeof(UINT);
    memset(allocinfo, 0x00, statnum/8);
    *(UINT*)(new_node+1) = 1;
    *allocinfo = 0x80;  /* pipelined faster when assign-only */
    return allocinfo+(statnum/8);
found:
    info = *allocinfs;
    if (info < 128) {
        orp = 0x80;
        rem = 0;
    } else if (info < 192) {
        orp = 0x40;
        rem = 1;
    } else if (info < 224) {
        orp = 0x20;
        rem = 2;
    } else if (info < 240) {
        orp = 0x10;
        rem = 3;
    } else if (info < 248) {
        orp = 0x08;
        rem = 4;
    } else if (info < 252) {
        orp = 0x04;
        rem = 5;
    } else if (info < 254) {
        orp = 0x02;
        rem = 6;
    } else {
        orp = 0x01;
        rem = 7;
    }
    *allocinfs |= orp;
    ++(*(UINT*)(curnode+1));
    return (allocinfo+(statnum/8))+((((allocinfs-allocinfo)*8)+rem)*statsize);
}
#endif

#ifdef SCBPOOL_DEALLOC
BOOL scbpool_dealloc(SCBPOOL* scbpoolp, void* satp) {
    UINT nbs;
    UINT8* offset;
    SCBNODE* curnode;
    UINT statnum = scbpoolp->statnum;
    UINT statsize = scbpoolp->statsize;
    SCBLIST* poolc = (SCBLIST*)scbpoolp;
    UINT8 andmask[8] = {0x7F, 0xBF, 0xDF, 0xEF, 0xF7, 0xFB, 0xFD, 0xFE};
    UINT satstart = sizeof(SCBNODE)+sizeof(UINT)+((statnum/8)*sizeof(UINT8));
    UINT nodealloc = satstart + statnum*statsize;
    for (curnode=poolc->prevp; curnode!=poolc; curnode=curnode->prevp) {
        if (((unsigned)satp>=(unsigned)curnode) &&
        ((unsigned)satp<(unsigned)curnode+(unsigned)nodealloc)) {
            nbs = ((UINT8*)satp-((UINT8*)curnode+satstart)) / statsize;
            offset = (UINT8*)(curnode+1)+sizeof(UINT)+(nbs/8);
            *offset &= andmask[nbs%8];
            --(*(UINT*)(curnode+1));
            return TRUE;
        }
    }
    return FALSE;
}
#endif

#ifdef SCBPOOL_RESET
void scbpool_reset(SCBPOOL* scbpoolp) {
    size_t n = scbpoolp->statnum / 8;
    SCBNODE* endnode = (SCBNODE*)scbpoolp;
    SCBNODE* curnode = endnode->nextp;
    while (curnode != endnode) {
        memset((UINT8*)(curnode+1)+sizeof(UINT), 0x00, n);
        *(UINT*)(curnode+1) = 0;
        curnode = curnode->nextp;
    }
}
#endif

#ifdef SCBPOOL_REFRESH
void scbpool_refresh(SCBPOOL* scbpoolp) {
    SCBNODE* oldnode;
    SCBNODE* endnode = (SCBNODE*)scbpoolp;
    SCBNODE* curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        if (*(UINT*)(oldnode+1) == 0) {
            /* erase one at iter */
            oldnode->prevp->nextp = curnode;
            oldnode->nextp->prevp = oldnode->prevp;
            free(oldnode);
        }
    }
}
#endif

#ifdef SCBPOOL_CLEAR
void scbpool_clear(SCBPOOL* scbpoolp) {
    SCBNODE* oldnode;
    SCBNODE* endnode = (SCBNODE*)scbpoolp;
    SCBNODE* curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    scbpoolp->list.prevp = endnode;
    scbpoolp->list.nextp = endnode;
}
#endif

#ifdef SCBPOOL_DESTROY
void scbpool_destroy(SCBPOOL* scbpoolp) {
    SCBNODE* oldnode;
    SCBNODE* endnode = (SCBNODE*)scbpoolp;
    SCBNODE* curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    free(scbpoolp);
}
#endif
