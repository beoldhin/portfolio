#include <stdio.h>
#include "scbpool.h"
#include "/home/olli/src/algo/list/dclist.h"

int main(void)
{
    int i;
    int mydata;
    void* allocd;
    
    DCLIST ptrs;
    DCLIST* ptrsp;
    DCITER* iterp;
    SCBPOOL mypool;
    SCBPOOL* mypoolp;
    
    ptrsp = &ptrs;
    dclist_screate(ptrsp);
    
    mypoolp = &mypool;
    scbpool_screate(mypoolp, 24, sizeof(int));
    
    mydata = 0x1234;
    for (i=0; i<10; ++i) {
        allocd = scbpool_alloc(mypoolp);
        
        printf("%u\n", (unsigned)allocd);
        
        dclist_push_back(ptrsp, &allocd, sizeof(allocd));
    }
    if (dclist_iterate_first(ptrsp,&iterp)) {
        do {
            scbpool_dealloc(mypoolp, *(void**)(iterp+1));
        } while (dclist_iterate_next(ptrsp,&iterp));
    }
    
    scbpool_reset(mypoolp);
    scbpool_refresh(mypoolp);
    scbpool_clear(mypoolp);
    dclist_clear(ptrsp);
    
    return 0;
}
