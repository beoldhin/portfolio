#include <stdio.h>

int main(void)
{
    unsigned int orp;
    unsigned int bits;
    
    for (bits=0; bits<=254; ++bits) {
        
        if (!(bits&0x80)) {
            orp = 0x80;
        } else if (!(bits&0x40)) {
            orp = 0x40;
        } else if (!(bits&0x20)) {
            orp = 0x20;
        } else if (!(bits&0x10)) {
            orp = 0x10;
        } else if (!(bits&0x08)) {
            orp = 0x08;
        } else if (!(bits&0x04)) {
            orp = 0x04;
        } else if (!(bits&0x02)) {
            orp = 0x02;
        } else {
            orp = 0x01;
        }
        
        printf("%d: 0x%02x\n", bits, orp);
        
    }
    
    return 0;
}
