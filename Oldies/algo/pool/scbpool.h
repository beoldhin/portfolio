#ifndef _SCBPOOL_H
#define _SCBPOOL_H

/* Circular static pool implementation using bits.               */
/* Similar to simple scpool implementation but also has dealloc. */
/* This version consumes O(n/8) more memory than simple scpool!  */

#include <string.h>
#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

/* Mask:
 *   0..127: 0x80
 * 128..191: 0x40
 * 192..223: 0x20
 * 224..239: 0x10
 * 240..247: 0x08
 * 248..251: 0x04
 * 252..253: 0x02
 *      254: 0x01
 */

typedef struct scbnode {
    struct scbnode* prevp;
    struct scbnode* nextp;
} SCBNODE,SCBLIST;

typedef struct scbpool {
    SCBNODE list;  /* keep this the very first */
    UINT statnum;
    UINT statsize;
} SCBPOOL;

#ifdef TEST_COMPILATION
  #define SCBPOOL_SCREATE
  #define SCBPOOL_HCREATE
  #define SCBPOOL_ALLOC
  #define SCBPOOL_DEALLOC
  #define SCBPOOL_RESET
  #define SCBPOOL_REFRESH
  #define SCBPOOL_CLEAR
  #define SCBPOOL_DESTROY
#endif

#ifdef SCBPOOL_SCREATE
void scbpool_screate(SCBPOOL*, UINT, UINT);
#endif
#ifdef SCBPOOL_HCREATE
SCBPOOL* scbpool_hcreate(UINT, UINT);
#endif
#ifdef SCBPOOL_ALLOC
void* scbpool_alloc(SCBPOOL*);
#endif
#ifdef SCBPOOL_DEALLOC
BOOL scbpool_dealloc(SCBPOOL*, void*);
#endif
#ifdef SCBPOOL_RESET
void scbpool_reset(SCBPOOL*);
#endif
#ifdef SCBPOOL_REFRESH
void scbpool_refresh(SCBPOOL*);
#endif
#ifdef SCBPOOL_CLEAR
void scbpool_clear(SCBPOOL*);
#endif
#ifdef SCBPOOL_DESTROY
void scbpool_destroy(SCBPOOL*);
#endif

#endif  /* _SCBPOOL_H */
