/* Employs lazy delete triggering - empty pool nodes are cleared only    */
/* explicitly to save clock cycles.                                      */

/* The pools are traversed backwards when finding an empty slot because  */
/* it is statistically more probable to find an empty slot in real-world */
/* "push_back" cases when traversion from end to start.                  */

#include "scdpool.h"

/***********************************************************************
 * Function      : scdpool_screate
 * Time complex. :
 * Description   :
 * Parameters    : poolp:
 *                 statnum:
 *                 statsize:
 * Return values : None
 **********************************************************************/
#ifdef SCDPOOL_SCREATE
void scdpool_screate(SCDPOOL* scdpoolp, UINT statnum, UINT statsize) {
    scdpoolp->list.prevp = (SCDNODE*)scdpoolp;
    scdpoolp->list.nextp = (SCDNODE*)scdpoolp;
    scdpoolp->statnum = statnum;
    scdpoolp->statsize = statsize;
}
#endif

/***********************************************************************
 * Function      : scdpool_hcreate
 * Time complex. :
 * Description   :
 * Parameters    : statnum:
 *                 statsize:
 * Return values : Non-NULL:
 *                 NULL:
 **********************************************************************/
#ifdef SCDPOOL_HCREATE
SCDPOOL* scdpool_hcreate(UINT statnum, UINT statsize) {
    SCDPOOL* new_pool;
    new_pool = (SCDPOOL*)malloc(sizeof(SCDPOOL));
    if (new_pool == NULL)
        return NULL;
    new_pool->list.prevp = (SCDNODE*)new_pool;
    new_pool->list.nextp = (SCDNODE*)new_pool;
    new_pool->statnum = statnum;
    new_pool->statsize = statsize;
    return new_pool;
}
#endif

/***********************************************************************
 * Function      : scdpool_alloc
 * Time complex. :
 * Description   :
 * Parameters    : scpoolp:
 * Return values : Non-NULL:
 *                 NULL:
 **********************************************************************/
/* Every pool node contains the following information:         */
/* 1) SCDNODE (prevp & nextp)                                  */
/* 2) Num_of_allocs_in_this_node (UINT)                        */
/* 3) Alloc_info_for_this_node (statnum*sizeof(UINT8)) */
/* 4) Satellite_for_this_node (statnum*statsize)               */
/* Returns NULL if out of memory                               */
#ifdef SCDPOOL_ALLOC
void* scdpool_alloc(SCDPOOL* scdpoolp) {
    UINT allocsn;
    UINT statnum;
    UINT statsize;
    SCDLIST* poolc;
    SCDNODE* curnode;
    SCDNODE* new_node;
    UINT8* allocinfo;
    UINT8* foundinfo;
    poolc = (SCDLIST*)scdpoolp;
    statnum = scdpoolp->statnum;
    statsize = scdpoolp->statsize;
    /* Try to find the first empty static slot */
    curnode = poolc->prevp;
    while (curnode != poolc) {
        allocsn = *(UINT*)(curnode+1);
        if (allocsn < statnum) {
            allocinfo = (UINT8*)(curnode+1) + sizeof(UINT);
            foundinfo = memchr(allocinfo, FREE, statnum);
            *foundinfo = USED;
            ++(*(UINT*)(curnode+1));
            return (allocinfo+statnum)+((foundinfo-allocinfo)*statsize);
        }
        curnode = curnode->prevp;
    }
    /* Empty slot was not found, alloc whole pool */
    new_node = (SCDNODE*)malloc(sizeof(SCDNODE)+sizeof(UINT)+(statnum*
    sizeof(UINT8))+(statnum*statsize));
    if (new_node == NULL)
        return NULL;
    /* Push back */
    new_node->prevp = poolc->prevp;
    new_node->nextp = poolc;
    poolc->prevp->nextp = new_node;
    poolc->prevp = new_node;
    allocinfo = (UINT8*)(new_node+1) + sizeof(UINT);
    memset(allocinfo, FREE, statnum);
    *(UINT*)(new_node+1) = 1;
    *allocinfo = USED;
    return allocinfo+statnum;
}
#endif

/***********************************************************************
 * Function      : scdpool_dealloc
 * Time complex. :
 * Description   :
 * Parameters    : scpoolp:
 *                 satp:
 * Return values : FALSE:
 *                 TRUE:
 **********************************************************************/
/* Returns FALSE if invalid pointer (does not detect internal pointer */
/* inconsistencies)                                                   */
#ifdef SCDPOOL_DEALLOC
BOOL scdpool_dealloc(SCDPOOL* scdpoolp, void* satp) {
    UINT offset;
    UINT statnum;
    UINT statsize;
    UINT nodealloc;
    SCDLIST* poolc;
    SCDNODE* curnode;
    poolc = (SCDLIST*)scdpoolp;
    curnode = poolc->prevp;
    statnum = scdpoolp->statnum;
    statsize = scdpoolp->statsize;
    nodealloc = sizeof(SCDNODE) + sizeof(UINT) + (statnum*
    sizeof(UINT8))+(statnum*statsize);
    while (curnode != poolc) {
        if (((unsigned)satp>=(unsigned)curnode) &&
        ((unsigned)satp<(unsigned)curnode+(unsigned)nodealloc)) {
            offset = ((unsigned)satp - (unsigned)(curnode+1) -
            (unsigned)statnum - sizeof(UINT)) / statsize;
            *((UINT8*)((unsigned)(curnode+1)+sizeof(UINT)+offset))
            = FREE;
            --(*(UINT*)(curnode+1));
            return TRUE;
        }
        curnode = curnode->prevp;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : scdpool_reset
 * Time complex. :
 * Description   :
 * Parameters    : scpoolp
 * Return values : None
 **********************************************************************/
#ifdef SCDPOOL_RESET
void scdpool_reset(SCDPOOL* scdpoolp) {
    size_t n = scdpoolp->statnum;
    SCDNODE* endnode = (SCDNODE*)scdpoolp;
    SCDNODE* curnode = endnode->nextp;
    while (curnode != endnode) {
        memset((UINT8*)(curnode+1)+sizeof(UINT), 0x00, n);
        *(UINT*)(curnode+1) = 0;
        curnode = curnode->nextp;
    }
}
#endif

/***********************************************************************
 * Function      : scdpool_refresh
 * Time complex. :
 * Description   :
 * Parameters    : scpoolp
 * Return values : None
 **********************************************************************/
/* This function is called explicitly when needing to remove empty nodes */
#ifdef SCDPOOL_REFRESH
void scdpool_refresh(SCDPOOL* scdpoolp) {
    SCDNODE* curnode;
    SCDNODE* endnode;
    SCDNODE* oldnode;
    endnode = (SCDNODE*)scdpoolp;
    curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        if (*(UINT*)(oldnode+1) == 0) {
            /* Erase one at iter */
            oldnode->prevp->nextp = curnode;
            oldnode->nextp->prevp = oldnode->prevp;
            free(oldnode);
        }
    }
}
#endif

/***********************************************************************
 * Function      : scdpool_clear
 * Time complex. :
 * Description   :
 * Parameters    : scpoolp:
 * Return values : None
 **********************************************************************/
#ifdef SCDPOOL_CLEAR
void scdpool_clear(SCDPOOL* scdpoolp) {
    SCDNODE* oldnode;
    SCDNODE* curnode;
    SCDNODE* endnode;
    endnode = (SCDNODE*)scdpoolp;
    curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    scdpoolp->list.prevp = endnode;
    scdpoolp->list.nextp = endnode;
}
#endif

/***********************************************************************
 * Function      : scdpool_destroy
 * Time complex. :
 * Description   :
 * Parameters    : scpoolp
 * Return values : None
 **********************************************************************/
#ifdef SCDPOOL_DESTROY
void scdpool_destroy(SCDPOOL* scdpoolp) {
    SCDNODE* oldnode;
    SCDNODE* curnode;
    SCDNODE* endnode;
    endnode = (SCDNODE*)scdpoolp;
    curnode = endnode->nextp;
    while (curnode != endnode) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    free(scdpoolp);
}
#endif
