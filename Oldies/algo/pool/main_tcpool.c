#include <time.h>
#include <stdio.h>
#include "tcpool.h"
#include "/home/olli/src/algo/randgen/mtcokus.h"

char strings[5][5+1] = {"c", "b", "a", "a", "abcde"};
char* strp;

int main(void) {
    int i;
    time_t secs;
    size_t strl;
    char* allocd;
    TCPOOL mypool;
    TCPOOL* mypoolp;
    MTCOKUS mtc;
    MTCOKUS* mtcp;
    
    mtcp = &mtc;
    mypoolp = &mypool;
    tcpool_screate(mypoolp, 100000);
    
    time(&secs);
    mtcokus_seed(mtcp, (unsigned)secs);
    for (i=0; i<300000; ++i) {
        strp = &strings[mtcokus_random(mtcp)%5][0];
        strl = strlen(strp) + 1;
        allocd = tcpool_alloc(mypoolp, strl);
        memcpy(allocd, strp, strl);
    }
    
    tcpool_clear(mypoolp);
    
    return 0;
}
