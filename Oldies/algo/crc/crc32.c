/* Note: the inner loop of the table creator functions can be unrolled */
#include "crc32.h"

#ifdef CRC32LE_GENERATE_TABLE
void crc32le_generate_table(CRC32* crc32p, UINT32 poly) {
    UINT i, c;
    UINT32 accum;
    UINT32* table;
    table = crc32p->table;
    for (i=0; i<256; ++i) {
        accum = i;
        for (c=0; c<8; ++c) {
            if (accum & 0x00000001U)
                accum = (accum >> 1) ^ poly;
            else
                accum >>= 1;
        }
        table[i] = accum;
    }
}
#endif

#ifdef CRC32BE_GENERATE_TABLE
void crc32be_generate_table(CRC32* crc32p, UINT32 poly) {
    UINT i, c;
    UINT32 accum;
    UINT32* table;
    table = crc32p->table;
    for (i=0; i<256; ++i) {
        accum = i;
        for (c=0; c<8; ++c) {
            if (accum & 0x80000000U)
                accum = (accum << 1) ^ poly;
            else
                accum <<= 1;
        }
        table[i] = accum;
    }
}
#endif

/* note: endptr is actually one greater than the block end pointer */
#ifdef CRC32LE_GENERATE_LINEAR
UINT32 crc32le_generate_linear(CRC32* crc32p, UINT8* curptr,
UINT8* endptr) {
    UINT i;
    UINT32 accum;
    UINT32* table;
    accum = 0xFFFFFFFFU;
    table = crc32p->table;
    while (curptr < endptr) {
        i = (accum ^ *curptr) & 0xFF;
        accum = (accum >> 8) ^ table[i];
        ++curptr;
    }
    return accum ^ 0xFFFFFFFFU;
}
#endif

/* note: endptr is actually one greater than the block end pointer */
#ifdef CRC32BE_GENERATE_LINEAR
UINT32 crc32be_generate_linear(CRC32* crc32p, UINT8* curptr,
UINT8* endptr) {
    UINT i;
    UINT32 accum;
    UINT32* table;
    accum = 0xFFFFFFFFU;
    table = crc32p->table;
    while (curptr < endptr) {
        i = ((accum>>24) ^ *curptr) & 0xFF;
        accum = (accum << 8) ^ table[i];
        ++curptr;
    }
    return accum ^ 0xFFFFFFFFU;
}
#endif

/* note: set accum to 0xFFFFFFFFU when first calling this function */
/* feed the next calls with the accum returned by this function */
/* when no more calls are made, XOR the returned accum by 0xFFFFFFFFU */
#ifdef CRC32LE_GENERATE_BLOCKS
UINT32 crc32le_generate_blocks(CRC32* crc32p, UINT8* curptr, UINT8* endptr,
UINT32 accum) {
    UINT i;
    UINT32* table;
    table = crc32p->table;
    while (curptr < endptr) {
        i = (accum ^ *curptr) & 0xFF;
        accum = (accum >> 8) ^ table[i];
        ++curptr;
    }
    return accum;
}
#endif

/* note: set accum to 0xFFFFFFFFU when first calling this function */
/* feed the next calls with the accum returned by this function */
/* when no more calls are made, XOR the returned accum by 0xFFFFFFFFU */
#ifdef CRC32BE_GENERATE_BLOCKS
UINT32 crc32be_generate_blocks(CRC32* crc32p, UINT8* curptr, UINT8* endptr,
UINT32 accum) {
    UINT i;
    UINT32* table;
    table = crc32p->table;
    while (curptr < endptr) {
        i = ((accum>>24) ^ *curptr) & 0xFF;
        accum = (accum << 8) ^ table[i];
        ++curptr;
    }
    return accum;
}
#endif

#ifdef CRC32LE_GENERATE_STRING
UINT32 crc32le_generate_string(CRC32* crc32p, UINT8* str) {
    UINT i;
    UINT32 accum;
    UINT32* table;
    accum = 0xFFFFFFFFU;
    table = crc32p->table;
    while (*str) {
        i = (accum ^ *str) & 0xFF;
        accum = (accum >> 8) ^ table[i];
        ++str;
    }
    return accum ^ 0xFFFFFFFFU;
}
#endif

#ifdef CRC32BE_GENERATE_STRING
UINT32 crc32be_generate_string(CRC32* crc32p, UINT8* str) {
    UINT i;
    UINT32 accum;
    UINT32* table;
    accum = 0xFFFFFFFFU;
    table = crc32p->table;
    while (*str) {
        i = ((accum>>24) ^ *str) & 0xFF;
        accum = (accum<<8) ^ table[i];
        ++str;
    }
    return accum ^ 0xFFFFFFFFU;
}
#endif

#ifdef CRC32LE_GENERATE_STRING_LENGTH
UINT32 crc32le_generate_string_length(CRC32* crc32p, UINT8* str,
UINT* strl) {
    UINT i;
    UINT8* oldp;
    UINT32 accum;
    UINT32* table;
    oldp = str;
    accum = 0xFFFFFFFFU;
    table = crc32p->table;
    while (*str) {
        i = (accum ^ *str) & 0xFF;
        accum = (accum>>8) ^ table[i];
        ++str;
    }
    *strl = str - oldp;
    return accum ^ 0xFFFFFFFFU;
}
#endif

#ifdef CRC32BE_GENERATE_STRING_LENGTH
UINT32 crc32be_generate_string_length(CRC32* crc32p, UINT8* str,
UINT* strl) {
    UINT i;
    UINT8* oldp;
    UINT32 accum;
    UINT32* table;
    oldp = str;
    accum = 0xFFFFFFFFU;
    table = crc32p->table;
    while (*str) {
        i = ((accum>>24) ^ *str) & 0xFF;
        accum = (accum << 8) ^ table[i];
        ++str;
    }
    *strl = str - oldp;
    return accum ^ 0xFFFFFFFFU;
}
#endif
