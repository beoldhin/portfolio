#ifndef _CRC32_H
#define _CRC32_H

#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct crc32 {
    UINT32 table[256];
} CRC32;

/* PKZIP uses 0xEDB88320 as the polynomial when in Little Endian */
/* PKZIP uses 0x04C11DB7 as the polynomial when in Big Endian */

#ifdef TEST_COMPILATION
  #define CRC32LE_GENERATE_TABLE
  #define CRC32BE_GENERATE_TABLE
  #define CRC32LE_GENERATE_LINEAR
  #define CRC32BE_GENERATE_LINEAR
  #define CRC32LE_GENERATE_BLOCKS
  #define CRC32BE_GENERATE_BLOCKS
  #define CRC32LE_GENERATE_STRING
  #define CRC32BE_GENERATE_STRING
  #define CRC32LE_GENERATE_STRING_LENGTH
  #define CRC32BE_GENERATE_STRING_LENGTH
#endif

#ifdef CRC32LE_GENERATE_TABLE
void crc32le_generate_table(CRC32*, UINT32);
#endif
#ifdef CRC32BE_GENERATE_TABLE
void crc32be_generate_table(CRC32*, UINT32);
#endif
#ifdef CRC32LE_GENERATE_LINEAR
UINT32 crc32le_generate_linear(CRC32*, UINT8*, UINT8*);
#endif
#ifdef CRC32BE_GENERATE_LINEAR
UINT32 crc32be_generate_linear(CRC32*, UINT8*, UINT8*);
#endif
#ifdef CRC32LE_GENERATE_BLOCKS
UINT32 crc32le_generate_blocks(CRC32*, UINT8*, UINT8*, UINT32);
#endif
#ifdef CRC32BE_GENERATE_BLOCKS
UINT32 crc32be_generate_blocks(CRC32*, UINT8*, UINT8*, UINT32);
#endif
#ifdef CRC32LE_GENERATE_STRING
UINT32 crc32le_generate_string(CRC32*, UINT8*);
#endif
#ifdef CRC32BE_GENERATE_STRING
UINT32 crc32be_generate_string(CRC32*, UINT8*);
#endif
#ifdef CRC32LE_GENERATE_STRING_LENGTH
UINT32 crc32le_generate_string_length(CRC32*, UINT8*, UINT*);
#endif
#ifdef CRC32BE_GENERATE_STRING_LENGTH
UINT32 crc32be_generate_string_length(CRC32*, UINT8*, UINT*);
#endif

#endif  /* _CRC32_H */
