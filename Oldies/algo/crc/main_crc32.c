#include <stdio.h>
#include <string.h>
#include "crc32.h"

int main(void) {
    CRC32 crc32;
    CRC32* crc32p;
    char mytext1[] = "Testing...";
    char mytext2[] = "Testing.,.";
    char mytext3[] = "Uesting...";
    
    crc32p = &crc32;
    crc32le_generate_table(crc32p, 0xEDB88320U);
    printf("0x%08X\n", (unsigned int)crc32le_generate_string(crc32p,(UINT8*)mytext1));
    printf("0x%08X\n", (unsigned int)crc32le_generate_string(crc32p,(UINT8*)mytext2));
    printf("0x%08X\n", (unsigned int)crc32le_generate_string(crc32p,(UINT8*)mytext3));
    
    return 0;
}
