#include <stdio.h>
#include "hetu.h"


int main(void)
{
    HETU hetu;
    int firstrun = 1;
    char hetuc[11+1];
    hetu_get_parts("010180-XXXX", &hetu);
    
    while ((hetu.year+hetu.century <= 1980)) {
        for (hetu.cntr=0; hetu.cntr<=999; hetu.cntr++) {
            hetu_create_hetu(&hetu, hetuc);
            puts(hetuc);
        }
        hetu_next_day(&hetu, &firstrun);
    }
    return 0;
}
