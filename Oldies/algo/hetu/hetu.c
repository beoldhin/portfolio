#include <stdio.h>
#include <stdlib.h>
#include "hetu.h"

#define MAXLIMS 3

int hetu_get_leapmonth(HETU*);

typedef struct lims {
    char character;
    int century;
} LIMS;

LIMS lims[MAXLIMS] = {
    {'+', 1800},
    {'-', 1900},
    {'A', 2000}
};

char daylist[12] = {
    31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

char hetulist[31] = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'A', 'B', 'C', 'D', 'E', 'F', 'H', 'J', 'K', 'L',
    'M', 'N', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y'
};


int hetu_get_parts(const char* hetuc, HETU* hetu)
{
    int i;
    char tmpbuf[3+1];
    memcpy(tmpbuf, hetuc, 2); tmpbuf[2] = '\0';
    hetu->day = atoi(tmpbuf);
    memcpy(tmpbuf, hetuc+2, 2); tmpbuf[2] = '\0';
    hetu->month = atoi(tmpbuf);
    memcpy(tmpbuf, hetuc+4, 2); tmpbuf[2] = '\0';
    hetu->year = atoi(tmpbuf);
    memcpy(tmpbuf, hetuc+7, 3); tmpbuf[3] = '\0';
    for (i=0; i<MAXLIMS; i++) {
        if (hetuc[6] == lims[i].character) {
            hetu->century = lims[i].century;
            break;
        }
    }
    hetu->cntr = atoi(tmpbuf);
    hetu->chkn = hetuc[10];
    return hetu->century != 0 ? 1 : 0;
}

int hetu_check(HETU* hetu)
{
    int date = (hetu->day*10000) + (hetu->month*100) + hetu->year;
    return hetulist[((date*1000)+hetu->cntr)%31] == hetu->chkn ? 1 : 0;
}

int hetu_check_all(HETU* hetu)
{
    int trueyear = hetu_get_leapmonth(hetu);
    if (hetu->day<1 || hetu->day>daylist[hetu->month-1]) {
        return 0;
    }
    if (hetu->month<1 || hetu->month>12) {
        return 0;
    }
    if (trueyear<lims[0].century || trueyear>lims[MAXLIMS-1].century+99)
        return 0;
    return hetu_check(hetu);
}

void hetu_create_hetu(HETU* hetu, char* hetuc)
{
    int tmpnum, date, i;
    hetuc[0] = (hetu->day/10) + '0';
    hetuc[1] = (hetu->day%10) + '0';
    hetuc[2] = (hetu->month/10) + '0';
    hetuc[3] = (hetu->month%10) + '0';
    hetuc[4] = (hetu->year/10) + '0';
    hetuc[5] = (hetu->year%10) + '0';
    for (i=0; i<MAXLIMS; i++) {
        if (hetu->century>=lims[i].century &&
        hetu->century<=lims[i].century+99) {
            hetuc[6] = lims[i].character;
            break;
        }
    }
    if (i == MAXLIMS) {
        hetuc[6] = '?';
    }
    date = ((hetu->day*10000)+(hetu->month*100)+hetu->year)*1000 + hetu->cntr;
    tmpnum = hetu->cntr - ((hetu->cntr/100)*100);
    hetuc[7] = (hetu->cntr/100) + '0';
    hetuc[8] = (tmpnum/10) + '0';
    hetuc[9] = (tmpnum%10) + '0';
    hetuc[10] = hetulist[date%31];
    hetuc[11] = '\0';
}

void hetu_next_day(HETU* hetu, int* firstrun)
{
    if (*firstrun) {
        hetu_get_leapmonth(hetu);
        *firstrun = 0;
    }
    if (hetu->day < daylist[hetu->month-1]) {
        hetu->day++;
    } else {
        hetu->day = 1;
        if (hetu->month < 12) {
            hetu->month++;
        } else {
            hetu->month = 1;
            if (hetu->year < 99) {
                hetu->year++;
            } else {
                hetu->year = 0;
                hetu->century += 100;
            }
        }
        hetu_get_leapmonth(hetu);
    }
}

void hetu_prev_day(HETU* hetu, int* firstrun)
{
    if (*firstrun) {
        hetu_get_leapmonth(hetu);
        *firstrun = 0;
    }
    if (hetu->day > 1) {
        hetu->day--;
    } else {
        if (hetu->month > 1) {
            hetu->month--;
        } else {
            hetu->month = 12;
            if (hetu->year > 0) {
                hetu->year--;
            } else {
                hetu->year = 99;
                hetu->century -= 100;
            }
        }
        hetu_get_leapmonth(hetu);
        hetu->day = daylist[hetu->month-1];
    }
}

int hetu_get_leapmonth(HETU* hetu)
{
    int trueyear = hetu->year + hetu->century;
    if ((trueyear%400==0) || ((trueyear%4==0)&&(trueyear%100!=0))) {
        daylist[1] = 29;
    } else {
        daylist[1] = 28;
    }
    return trueyear;
}
