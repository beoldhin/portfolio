#ifndef _HETU_H
#define _HETU_H

#define MAXLIMS 3

typedef struct hetu
{
    char day;
    char month;
    int year;
    int century;
    int cntr;  /* odd: male, even: female */
    char chkn;
} HETU;

int hetu_get_parts(const char*, HETU*);
int hetu_check(HETU*);
int hetu_check_all(HETU*);
void hetu_create_hetu(HETU*, char*);
void hetu_next_day(HETU*, int*);
void hetu_prev_day(HETU*, int*);

#endif /* _HETU_H */
