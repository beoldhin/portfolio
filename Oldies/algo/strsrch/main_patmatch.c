#include <stdio.h>
#include "patmatch.h"

int main(void) {
    UINT patl;
    UINT txtl;
    char* findp;
    PATMATCH pm;
    PATMATCH* pmp;
    char mytxt[] = "WHICH-FINALLY-HALTS.--AT-THATAT-THAT-POINT";
    char mypat[] = "AT-THAT";
    
    pmp = &pm;
    patl = strlen(mypat);
    txtl = strlen(mytxt);
    patmatch_feed_pattern(pmp, mypat, patl);
    findp = patmatch_find(pmp, mytxt, txtl);
    printf("%s\n", findp);
    findp = patmatch_find(pmp, findp+patl, txtl);
    printf("%s\n", findp);
    
    return 0;
}
