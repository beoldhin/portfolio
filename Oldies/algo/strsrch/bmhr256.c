/*
  created two character sliding window and tried to heavily move
  instructions out of the main loops by converting indices to pointers and
  simplifying commonly appearing rules (Olli)
*/
#include <string.h>
#include "bmhr256.h"

#ifdef BMHR256_SEARCH_MX
char* bmhr256_search_mx(BMHR256*, char*, UINT);
#endif
#ifdef BMHR256_SEARCH_M3
char* bmhr256_search_m3(BMHR256*, char*, UINT);
#endif
#ifdef BMHR256_SEARCH_M2
char* bmhr256_search_m2(BMHR256*, char*, UINT);
#endif


/* this function must be used when finding for a new pattern */
#ifdef BMHR256_FEED_PATTERN
void bmhr256_feed_pattern(BMHR256* bmhr256p, char* pat, UINT m) {
    UINT i;
    UINT mminusone;
    char* occheur;
    bmhr256p->pat = pat;
    bmhr256p->patl = m;
    mminusone = m - 1;
    occheur = bmhr256p->occheur;
    memset(occheur, (signed)m, 256);
    if (m != 3) {
        for (i=0; i<mminusone; ++i) {
            occheur[(UINT)pat[i]] = mminusone - i;
        }
    } else {
        occheur[(UINT)pat[0]] = 2;  /* mminusone=2, 2-0 = 2 */
        occheur[(UINT)pat[1]] = 1;  /* mminusone=2, 2-1 = 1 */
    }
}
#endif

#ifdef BMHR256_SEARCH
char* bmhr256_search(BMHR256* bmhr256p, char* txt, UINT n) {
    UINT m;
    char* pat;
    pat = bmhr256p->pat;
    m = bmhr256p->patl;
    if (m > 3) {
        return bmhr256_search_mx(bmhr256p, txt, n);
    } else if (m == 3) {
        return bmhr256_search_m3(bmhr256p, txt, n);
    } else if (m == 2) {
        return bmhr256_search_m2(bmhr256p, txt, n);
    } else if (m == 1) {
        return memchr(txt, pat[0], n);
    }
    return NULL;
}
#endif

/* Note: the memcmp function - which is usually very fast in practice - */
/* eliminates the need to know the SYMBOL_NOT_IN_TEXT                   */
#ifdef BMHR256_SEARCH_MX
char* bmhr256_search_mx(BMHR256* bmhr256p, char* txt, UINT n) {
    UINT m;
    char* pat;
    char* curptr;
    char* endptr;
    char* occheur;
    char* secpatptr;
    char first, last, middle;
    UINT mminusone, mminustwo;
    UINT midpoint, mminusmid;
    occheur = bmhr256p->occheur;
    pat = bmhr256p->pat;
    m = bmhr256p->patl;
    secpatptr = pat + 1;
    mminusone = m - 1;
    mminustwo = m - 2;
    midpoint = m / 2;
    mminusmid = mminusone - midpoint;
    first = pat[0];
    middle = pat[midpoint];
    last = pat[mminusone];
    curptr = txt + mminusone;
    endptr = txt + n;
    while (curptr < endptr) {
        if (*curptr == last) {
            if (*(curptr-mminusone) == first) {
                if (*(curptr-mminusmid) == middle) {
                    if (memcmp(curptr-mminustwo,secpatptr,mminustwo) == 0) {
                        return curptr - mminusone;
                    }
                }
            }
        }
        curptr += occheur[(UINT)*curptr];
    }
    return NULL;
}
#endif

/* not sure about this function, perhaps the sliding window implementation */
/* now used in bmhr256_search_m2 would be faster                         */
#ifdef BMHR256_SEARCH_M3
char* bmhr256_search_m3(BMHR256* bmhr256p, char* txt, UINT n) {
    char* pat;
    char* curptr;
    char* endptr;
    char* occheur;
    char first, last, middle;
    occheur = bmhr256p->occheur;
    pat = bmhr256p->pat;
    first = pat[0];
    middle = pat[1];
    last = pat[2];
    curptr = txt + 2;
    endptr = txt + n;
    while (curptr < endptr) {
        if (*curptr == last) {
            if (*(curptr-2) == first) {
                if (*(curptr-1) == middle) {
                    return curptr - 2;
                }
            }
        }
        curptr += occheur[(UINT)*curptr];
    }
    return NULL;
}
#endif

/* this function actually uses a sliding window scanner instead of bmhr   */
/* because the overhead of bmhr would be too big for a pattern this short */
#ifdef BMHR256_SEARCH_M2
char* bmhr256_search_m2(BMHR256* bmhr256p, char* txt, UINT n) {
    char prev;
    char* pat;
    char* txtend;
    char first, last;
    pat = bmhr256p->pat;
    first=pat[0]; last=pat[1]; prev='\0';
    for (txtend=txt+n; txt<txtend; prev=*txt++) {
        if (*txt==last && prev==first) {
            return txt - 1;
        }
    }
    return NULL;
}
#endif
