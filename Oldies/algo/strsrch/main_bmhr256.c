#include <stdio.h>
#include <string.h>
#include "bmhr256.h"

int main(void) {
    char* findp;
    BMHR256 bmhr256;
    BMHR256* bmhr256p;
    char mytxt[] = "WHICH-FINALLY-HALTS.--AT-THAT-POINT";
    char mypat1[] = "AT-THAT";
    char mypat2[] = "AT-";
    char mypat3[] = "AT";
    
    bmhr256p = &bmhr256;
    bmhr256_feed_pattern(bmhr256p, mypat1, strlen(mypat1));
    findp = bmhr256_search(bmhr256p, mytxt, strlen(mytxt));
    printf("%s\n", findp);
    bmhr256_feed_pattern(bmhr256p, mypat2, strlen(mypat2));
    findp = bmhr256_search(bmhr256p, mytxt, strlen(mytxt));
    printf("%s\n", findp);
    bmhr256_feed_pattern(bmhr256p, mypat3, strlen(mypat3));
    findp = bmhr256_search(bmhr256p, mytxt, strlen(mytxt));
    printf("%s\n", findp);
    
    return 0;
}
