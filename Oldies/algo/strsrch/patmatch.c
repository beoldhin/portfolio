/* modified the Thierry Lecroq's algorithms to use pointers (Olli) */
/* http://www-igm.univ-mlv.fr/~lecroq/string/index.html */
/* http://www-igm.univ-mlv.fr/~lecroq/string/node6.html */
/* http://www-igm.univ-mlv.fr/~lecroq/string/node19.html */
#include <string.h>
#include "patmatch.h"

#define MAX_BITS (sizeof(size_t)*8)

#ifdef TEST_COMPILATION
  #define PATMATCH_SHIFTOR_FIND
  #define PATMATCH_2CHWINDOW_FIND
  #define PATMATCH_QUICK_FIND
#endif

#ifdef PATMATCH_SHIFTOR_FIND
char* patmatch_shiftor_find(PATMATCH*, char*, UINT);
#endif
#ifdef PATMATCH_2CHWINDOW_FIND
char* patmatch_2chwindow_find(PATMATCH*, char*, UINT);
#endif
#ifdef PATMATCH_QUICK_FIND
char* patmatch_quick_find(PATMATCH*, char*, UINT);
#endif


#ifdef PATMATCH_FEED_PATTERN
void patmatch_feed_pattern(PATMATCH* pmp, char* pat, UINT m) {
    UINT i;
    WORD j;
    WORD lim;
    WORD* S;
    S = pmp->S;
    if (m <= MAX_BITS) {
        for (i=0; i<256; ++i)
            S[i] = (WORD)~0;
        for (i=lim=0,j=1; i<m; ++i,j<<=1) {
            S[(UINT)pat[i]] &= ~j;
            lim |= j;
        }
        pmp->lim = ~(lim>>1);
    } else {
        for (i=0; i<256; ++i)
            S[i] = m + 1;
        for (i=0; i<m; ++i)
            S[(UINT)pat[i]] = m - i;
    }
    pmp->pat = pat;
    pmp->patl = m;
}
#endif

#ifdef PATMATCH_FIND
char* patmatch_find(PATMATCH* pmp, char* txt, UINT n) {
    UINT patl;
    patl = pmp->patl;
    if (patl <= MAX_BITS) {
        if (patl > 2)
            return patmatch_shiftor_find(pmp, txt, n);
        if (patl == 2)
            return patmatch_2chwindow_find(pmp, txt, n);
        return memchr(txt, pmp->pat[0], n);
    }
    return patmatch_quick_find(pmp, txt, n);
}
#endif

#ifdef PATMATCH_SHIFTOR_FIND
char* patmatch_shiftor_find(PATMATCH* pmp, char* txt, UINT n) {
    UINT j;
    UINT m;
    WORD* S;
    WORD lim;
    WORD state;
    S = pmp->S;
    m = pmp->patl;
    lim = pmp->lim;
    for (state=(WORD)~0,j=0; j<n; ++j) {
        state = (state<<1) | S[(UINT)txt[j]];
        if (state < lim)
            return txt + j - m + 1;
    }
    return NULL;
}
#endif

/* this function actually uses a sliding window scanner instead of bmhr   */
/* because the overhead of bmhr would be too big for a pattern this short */
#ifdef PATMATCH_2CHWINDOW_FIND
char* patmatch_2chwindow_find(PATMATCH* pmp, char* txt, UINT n) {
    char prev;
    char* pat;
    char* txtend;
    char first, last;
    pat = pmp->pat;
    first=pat[0]; last=pat[1]; prev='\0';
    for (txtend=txt+n; txt<txtend; prev=*txt++) {
        if (*txt==last && prev==first) {
            return txt - 1;
        }
    }
    return NULL;
}
#endif

#ifdef PATMATCH_QUICK_FIND
char* patmatch_quick_find(PATMATCH* pmp, char* txt, UINT n) {
    UINT m;
    UINT mminusone;
    WORD* S;
    char* pat;
    char* txtend;
    S = pmp->S;
    m = pmp->patl;
    pat = pmp->pat;
    mminusone = m - 1;
    txtend = txt + n;
    txt += mminusone;
    while (txt < txtend) {
        if (memcmp(txt-mminusone,pat,m) == 0)
            return txt - mminusone;
        txt += S[(UINT)txt[m]];
    }
    return NULL;
}
#endif
