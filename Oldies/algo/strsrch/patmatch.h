#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

#define WORD size_t

/* note: for quicksearch S could be defined as a char array, */
/* but let that be alone for clarity */
typedef struct patmatch {
    char* pat;
    UINT patl;
    WORD lim;
    WORD S[256];
} PATMATCH;

#ifdef TEST_COMPILATION
  #define PATMATCH_FEED_PATTERN
  #define PATMATCH_FIND
#endif

#ifdef PATMATCH_FEED_PATTERN
void patmatch_feed_pattern(PATMATCH*, char*, UINT);
#endif
#ifdef PATMATCH_FIND
char* patmatch_find(PATMATCH*, char*, UINT);
#endif
