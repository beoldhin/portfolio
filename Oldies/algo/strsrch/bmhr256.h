#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct bmhr256 {
    char* pat;
    UINT patl;
    char occheur[256];
} BMHR256;

#ifdef TEST_COMPILATION
  #define BMHR256_FEED_PATTERN
  #define BMHR256_SEARCH
#endif

#ifdef BMHR256_FEED_PATTERN
void bmhr256_feed_pattern(BMHR256*, char*, UINT);
#endif
#ifdef BMHR256_SEARCH
char* bmhr256_search(BMHR256*, char*, UINT);
#endif
