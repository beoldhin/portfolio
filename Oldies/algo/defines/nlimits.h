#ifndef _NLIMITS_H
#define _NLIMITS_H

#include <stdlib.h>
#include <limits.h>

#undef SINT8
#undef UINT8
#undef SINT16
#undef UINT16
#undef SINT32
#undef UINT32
#undef SINT64
#undef UINT64
#undef SINT8_MAX
#undef UINT8_MAX
#undef SINT16_MAX
#undef UINT16_MAX
#undef SINT32_MAX
#undef UINT32_MAX
#undef SINT64_MAX
#undef UINT64_MAX

#define SINT8_MAX 127L
#define UINT8_MAX 255UL
#define SINT16_MAX 32767L
#define UINT16_MAX 65535UL
#define SINT32_MAX 2147483647L
#define UINT32_MAX 4294967295UL
#define SINT64_MAX 9223372036854775807L
#define UINT64_MAX 18446744073709551615UL

#if INT_MAX == SINT8_MAX
#define SINT8 signed int
#elif LONG_MAX == SINT8_MAX
#define SINT8 signed long int
#elif SHRT_MAX == SINT8_MAX
#define SINT8 signed short int
#elif SCHAR_MAX == SINT8_MAX
#define SINT8 signed char
#endif

#if UINT_MAX == UINT8_MAX
#define UINT8 unsigned int
#elif ULONG_MAX == UINT8_MAX
#define UINT8 unsigned long int
#elif USHRT_MAX == UINT8_MAX
#define UINT8 unsigned short int
#elif UCHAR_MAX == UINT8_MAX
#define UINT8 unsigned char
#endif

#if INT_MAX == SINT16_MAX
#define SINT16 signed int
#elif LONG_MAX == SINT16_MAX
#define SINT16 signed long int
#elif SHRT_MAX == SINT16_MAX
#define SINT16 signed short int
#elif SCHAR_MAX == SINT16_MAX
#define SINT16 signed char
#endif

#if UINT_MAX == UINT16_MAX
#define UINT16 unsigned int
#elif ULONG_MAX == UINT16_MAX
#define UINT16 unsigned long int
#elif USHRT_MAX == UINT16_MAX
#define UINT16 unsigned short int
#elif UCHAR_MAX == UINT16_MAX
#define UINT16 unsigned char
#endif

#if INT_MAX == SINT32_MAX
#define SINT32 signed int
#elif LONG_MAX == SINT32_MAX
#define SINT32 signed long int
#elif SHRT_MAX == SINT32_MAX
#define SINT32 signed short int
#elif SCHAR_MAX == SINT32_MAX
#define SINT32 signed char
#endif

#if UINT_MAX == UINT32_MAX
#define UINT32 unsigned int
#elif ULONG_MAX == UINT32_MAX
#define UINT32 unsigned long int
#elif USHRT_MAX == UINT32_MAX
#define UINT32 unsigned short int
#elif UCHAR_MAX == UINT32_MAX
#define UINT32 unsigned char
#endif

#if INT_MAX == SINT64_MAX
#define SINT64 signed int
#elif LONG_MAX == SINT64_MAX
#define SINT64 signed long int
#elif SHRT_MAX == SINT64_MAX
#define SINT64 signed short int
#elif SCHAR_MAX == SINT64_MAX
#define SINT64 signed char
#endif

#if UINT_MAX == UINT64_MAX
#define UINT64 unsigned int
#elif ULONG_MAX == UINT64_MAX
#define UINT64 unsigned long int
#elif USHRT_MAX == UINT64_MAX
#define UINT64 unsigned short int
#elif UCHAR_MAX == UINT64_MAX
#define UINT64 unsigned char
#endif

/* Don't move the definitions below above the elif clauses */

#undef UINT
#undef BOOL
#undef FALSE
#undef TRUE

#define UINT size_t
#define BOOL UINT8
#define FALSE 0
#define TRUE 1

#endif  /* _NLIMITS_H */
