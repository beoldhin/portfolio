#include <stdio.h>
#include "nlimits.h"

int main(void) {
    
    #ifdef SINT8
    printf("Size of SINT8 in bytes is: %u\n", sizeof(SINT8));
    #endif
    
    #ifdef UINT8
    printf("Size of UINT8 in bytes is: %u\n", sizeof(UINT8));
    #endif
    
    #ifdef SINT16
    printf("Size of SINT16 in bytes is: %u\n", sizeof(SINT16));
    #endif
    
    #ifdef UINT16
    printf("Size of UINT16 in bytes is: %u\n", sizeof(UINT16));
    #endif
    
    #ifdef SINT32
    printf("Size of SINT32 in bytes is: %u\n", sizeof(SINT32));
    #endif
    
    #ifdef UINT32
    printf("Size of UINT32 in bytes is: %u\n", sizeof(UINT32));
    #endif
    
    #ifdef SINT64
    printf("Size of SINT64 in bytes is: %u\n", sizeof(SINT64));
    #endif
    
    #ifdef UINT64
    printf("Size of UINT64 in bytes is: %u\n", sizeof(UINT64));
    #endif
    
    return 0;
}
