#include <stdio.h>
#include <memory>


int main(void)
{
    auto_ptr<int> aptr_foobar(new int);
    auto_ptr<int> foobar = aptr_foobar;
    
    *foobar = 1234;
    
    printf("%d\n", *foobar);
    
    return 0;
}
