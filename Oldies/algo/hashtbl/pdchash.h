#ifndef _PDCHASH_H
#define _PDCHASH_H

#include <string.h>
#include "/home/olli/src/algo/list/dclist.h"
#include "/home/olli/src/algo/pool/scpool.h"
#include "/home/olli/src/algo/pool/tcpool.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct dchash {
    UINT listn;
    UINT satsize;
    SCPOOL* scpoolp;
    TCPOOL* tcpoolp;
} PDCHASH;

#ifdef TEST_COMPILATION
  #define PDCHASH_HCREATE
  #define PDCHASH_RESIZE
  #define PDCHASH_FIND
  #define PDCHASH_FIND_ADVANCED
  #define PDCHASH_INSERT_NOALLOC
  #define PDCHASH_INSERT_ALLOC
  #define PDCHASH_DESTROY_NOFREE
  #define PDCHASH_DESTROY_FREE
  #define PDCHASH_DESTROY_ADVANCED
#endif

#ifdef PDCHASH_HCREATE
PDCHASH* pdchash_hcreate(UINT, SCPOOL*, TCPOOL*);
#endif
#ifdef PDCHASH_RESIZE
PDCHASH* pdchash_resize(PDCHASH*, UINT);
#endif
#ifdef PDCHASH_FIND
void* pdchash_find(PDCHASH*, char*);
#endif
#ifdef PDCHASH_FIND_ADVANCED
void* pdchash_find_advanced(PDCHASH*, char*, BOOL (*)(void*));
#endif
#ifdef PDCHASH_INSERT_NOALLOC
void* pdchash_insert_noalloc(PDCHASH*, char*, void*);
#endif
#ifdef PDCHASH_INSERT_ALLOC
void* pdchash_insert_alloc(PDCHASH*, char*, void*);
#endif
#ifdef PDCHASH_DESTROY_NOFREE
void pdchash_destroy_nofree(PDCHASH*);
#endif
#ifdef PDCHASH_DESTROY_FREE
void pdchash_destroy_free(PDCHASH*);
#endif
#ifdef PDCHASH_DESTROY_ADVANCED
void pdchash_destroy_advanced(PDCHASH*, void (*)(void*));
#endif

#endif  /* _PDCHASH_H */
