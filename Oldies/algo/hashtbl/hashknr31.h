#ifndef _HASH_KNR31_H
#define _HASH_KNR31_H

#include <strings.h>
#include "/home/olli/src/algo/defines/nlimits.h"

#ifndef TEST_COMPILATION
#include "/home/olli/src/incs/inc_current.h"
#endif

typedef unsigned long int uint32;

#ifdef TEST_COMPILATION
  #define HASH_KNR31
  #define HASH_KNR31_LENGTH
#endif

#ifdef HASH_KNR31
uint32 hash_knr31(char*);
#endif
#ifdef HASH_KNR31_LENGTH
uint32 hash_knr31_length(char*, UINT*);
#endif

#endif  /* _HASH_KNR31_H */
