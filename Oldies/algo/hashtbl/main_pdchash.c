/*
  Standard deviation:
  Formula: sqrt((sum(f)*(x-y)^2)/n)
  
  How to calculate for sample set 10, 20, 30, 40, 50:
  y = (10 + 20 + 30 + 40 + 50) / 5 = 30
  
  Subtracting y from all of the numbers becomes:
  -20, -10, 0, 10, 20
  
  Square the obtained numbers:
  400, 100, 0, 100, 400
  
  Sum the squared numbers together:
  1000
  
  Lastly, divide by n and get square root:
  Answer: 14.142
  
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "pdchash.h"

#define BUFMAX 128
#define DCLISTS 15000
/* DCLISTS can be anything when not using PJW type of hashing */

void dealloc_function(void*);

typedef struct mystruct {
    char* mystr;  /* hash.c needs this to be the first in a satellite */
} MYSTRUCT;

UINT dclists = DCLISTS;
PDCHASH* myhash;
MYSTRUCT mydata;
MYSTRUCT* mydatap;
char tmpbuf[BUFMAX];
char searchstr[] = "acyclic";


int main(void) {
    double devi;
    double mean;
    double total;
    double current;
    SCPOOL scpool;
    TCPOOL tcpool;
    SCPOOL* scpoolp;
    TCPOOL* tcpoolp;
    DCLIST* curnode;
    DCLIST* endnode;
    
    tcpoolp = &tcpool;
    scpoolp = &scpool;
    mydatap = &mydata;
    tcpool_screate(tcpoolp, 100000);
    scpool_screate(scpoolp, 10000, sizeof(DCNODE)+sizeof(MYSTRUCT));
    myhash = pdchash_hcreate(dclists, scpoolp, tcpoolp);
    
    puts("Enter strings, end by entering a null string:");
    
    while (1) {
        fgets(tmpbuf, BUFMAX, stdin);
        if (tmpbuf[0] == '\n')
            break;
        tmpbuf[strlen(tmpbuf)-1] = '\0';  /* Convert last '\n' to '\0' */
        pdchash_insert_alloc(myhash, tmpbuf, mydatap);
    }
    
    dclists -= 5;
    myhash = pdchash_resize(myhash, dclists);
    
/* Calculate mean and number of nodes -- begin */
    total = 0;
    curnode = (DCLIST*)(myhash + 1);
    endnode = curnode + dclists;
    while (curnode != endnode) {
        current = dclist_count_nodes(curnode);
        printf("%9.0f nodes in list #%u\n", current, curnode-
        (DCLIST*)(myhash+1)+1);
        total += current;
        ++curnode;
    }
    mean = total / dclists;
/* Calculate mean and number of nodes -- end */
    
/* Calculate standard deviation -- begin */
    devi = 0;
    curnode = (DCLIST*)(myhash + 1);
    while (curnode != endnode) {
        current = dclist_count_nodes(curnode);
        devi += pow(current-mean, (double)2);
        ++curnode;
    }
    devi = sqrt(devi/dclists);
    printf("Standard deviation: %f\n", devi);
    printf("Average nodes per list: %f\n", total/dclists);
/* Calculate standard deviation -- end */
    
    printf("Searching for \"%s\": ", searchstr);
    if (pdchash_find(myhash,searchstr))
        puts("Found.");
    else
        puts("Not found.");
    
    pdchash_destroy_advanced(myhash, dealloc_function);
    
    scpool_clear(scpoolp);
    tcpool_clear(tcpoolp);
    
    return 0;
}

void dealloc_function(void* satp) {
    /*  -- Not used when pooling
    free(*(char**)satp);
    */
}
