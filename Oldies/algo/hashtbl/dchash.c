/* CRC32, Weinberger&Holub, Bob Jenkins, Aho's 65599 and similar, and Linear */
/* Congruential type hashes rejected because Kernighan & Ritchie's x31       */
/* algorithm gives best results (similar to Aho's). It gives similar results */
/* to x37 but can be optimized further.                                      */

#include <string.h>
#include "dchash.h"
#include "hashknr31.h"

/* The 'skeleton' is one block in this function */
/* This is problematic for very big hash tables */
#ifdef DCHASH_HCREATE
DCHASH* dchash_hcreate(UINT listn) {
    DCNODE* hashcur;
    DCNODE* hashend;
    DCHASH* new_hash = (DCHASH*)malloc(sizeof(DCHASH)+(sizeof(DCNODE)*listn));
    if (new_hash == NULL)
        return NULL;
    new_hash->listn = listn;
    hashcur = (DCNODE*)(new_hash + 1);
    hashend = hashcur + listn;
    while (hashcur != hashend) {
        hashcur->prevp = hashcur;
        hashcur->nextp = hashcur;
        ++hashcur;
    }
    return new_hash;
}
#endif

#ifdef DCHASH_RESIZE
DCHASH* dchash_resize(DCHASH* hashp, UINT listn) {
    DCLIST* hashpos;
    DCLIST* hashcur;
    DCLIST* hashend;
    DCNODE* curnode;
    DCNODE* endnode;
    DCNODE* oldnode;
    DCHASH* new_hash = dchash_hcreate(listn);
    if (new_hash == NULL)
        return NULL;
    hashcur = (DCLIST*)(hashp + 1);
    hashend = hashcur + hashp->listn;
    while (hashcur != hashend) {
        endnode = hashcur;
        curnode = hashcur->nextp;
        while (curnode != endnode) {
            oldnode = curnode;
            curnode = curnode->nextp;
            hashpos = (DCLIST*)(new_hash+1) +
            (hash_knr31(*(char**)(oldnode+1)) % listn);
/* Adjust oldnode pointers for hashnode & push back to hashnode -- begin */
            oldnode->prevp = hashpos->prevp;
            oldnode->nextp = hashpos;
            hashpos->prevp->nextp = oldnode;
            hashpos->prevp = oldnode;
/* Adjust oldnode pointers for hashnode & push back to hashnode -- end */
        }
        ++hashcur;
    }
    free(hashp);
    return new_hash;
}
#endif

#ifdef DCHASH_FIND
void* dchash_find(DCHASH* hashp, char* strp) {
    DCNODE* endnode = (DCLIST*)(hashp+1) + (hash_knr31(strp) % hashp->listn);
    DCNODE* curnode = endnode->nextp;
    DCNODE* endplus1 = curnode;
    while (curnode != endnode) {
        if (strcmp(*(char**)(curnode+1),strp) == 0) {
/* move iter to head (MCD - Grune et. al) -- begin */
#ifdef MOVE_TO_HEAD
            if (curnode != endplus1) {
                curnode->prevp->nextp = curnode->nextp;
                curnode->nextp->prevp = curnode->prevp;
                curnode->prevp = endnode;
                curnode->nextp = endplus1;
                endnode->nextp->prevp = curnode;
                endnode->nextp = curnode;
            }
#endif
/* move iter to head (MCD - Grune et. al) -- end */
            return endplus1 + 1;
        }
        curnode = curnode->nextp;
    }
    return NULL;
}
#endif

/* The iterator is not needed as a function parameter because the strings */
/* in the hash table are randomly placed. Because iterator is not needed, */
/* dchash_find_first and dchash_find_last are not needed, either.         */
#ifdef DCHASH_FIND_ADVANCED
void* dchash_find_advanced(DCHASH* hashp, char* strp,
BOOL (*dchash_user_compare)(void*)) {
    DCNODE* endnode = (DCLIST*)(hashp+1) + (hash_knr31(strp) % hashp->listn);
    DCNODE* curnode = endnode->nextp;
    DCNODE* endplus1 = curnode;
    while (curnode != endnode) {
        if (dchash_user_compare(curnode+1) == 0) {
/* move iter to head (MCD - Grune et. al) -- begin */
#ifdef MOVE_TO_HEAD
            if (curnode != endplus1) {
                curnode->prevp->nextp = curnode->nextp;
                curnode->nextp->prevp = curnode->prevp;
                curnode->prevp = endnode;
                curnode->nextp = endplus1;
                endnode->nextp->prevp = curnode;
                endnode->nextp = curnode;
            }
#endif
/* move iter to head (MCD - Grune et. al) -- end */
            return endplus1 + 1;
        }
        curnode = curnode->nextp;
    }
    return NULL;
}
#endif

void* dchash_insert_noalloc(DCHASH* hashp, char* strp, void* satp,
UINT satsize) {
    DCLIST* newpos;
    DCNODE* new_node;
    UINT hashval = hash_knr31(strp);
    strcpy(*(char**)satp, strp);
    newpos = (DCLIST*)(hashp+1) + (hashval % hashp->listn);
/* push back -- begin */
    new_node = (DCNODE*)malloc(sizeof(DCNODE)+satsize);
    if (new_node == NULL)
        return NULL;
    new_node->prevp = newpos->prevp;
    new_node->nextp = newpos;
    newpos->prevp->nextp = new_node;
    newpos->prevp = new_node;
    memcpy(new_node+1, satp, satsize);
/* push back -- end */
    return new_node + 1;
}

/* Remember: don't put the "crc32_generate_string_length" line *after* */
/* the line "(char*)malloc(strl)"...                                   */
#ifdef DCHASH_INSERT_ALLOC
void* dchash_insert_alloc(DCHASH* hashp, char* strp, void* satp,
UINT satsize) {
    UINT strl;
    DCLIST* newpos;
    DCNODE* new_node;
    UINT hashval = hash_knr31_length(strp, &strl);
    char* memory = (char*)malloc(strl);
    if (memory == NULL)
        return NULL;
    *(char**)satp = memory;
    memcpy(memory, strp, strl);
    newpos = (DCLIST*)(hashp+1) + (hashval % hashp->listn);
/* Push back -- begin */
    new_node = (DCNODE*)malloc(sizeof(DCNODE)+satsize);
    if (new_node == NULL)
        return NULL;
    new_node->prevp = newpos->prevp;
    new_node->nextp = newpos;
    newpos->prevp->nextp = new_node;
    newpos->prevp = new_node;
    memcpy(new_node+1, satp, satsize);
/* Push back -- end */
    return new_node + 1;
}
#endif

#ifdef DCHASH_DESTROY_NOFREE
void dchash_destroy_nofree(DCHASH* hashp) {
    DCNODE* oldnode;
    DCNODE* curnode;
    DCNODE* endnode;
    DCLIST* hashcur = (DCLIST*)(hashp + 1);
    DCLIST* hashend = hashcur + hashp->listn;
    while (hashcur != hashend) {
        endnode = hashcur;
        curnode = hashcur->nextp;
        while (curnode != endnode) {
            oldnode = curnode;
            curnode = curnode->nextp;
            free(oldnode);
        }
        ++hashcur;
    }
    free(hashp);
}
#endif

#ifdef DCHASH_DESTROY_FREE
void dchash_destroy_free(DCHASH* hashp) {
    DCNODE* oldnode;
    DCNODE* curnode;
    DCNODE* endnode;
    DCLIST* hashcur = (DCLIST*)(hashp + 1);
    DCLIST* hashend = hashcur + hashp->listn;
    while (hashcur != hashend) {
        endnode = hashcur;
        curnode = hashcur->nextp;
        while (curnode != endnode) {
            oldnode = curnode;
            curnode = curnode->nextp;
            free(*(char**)(oldnode+1));
            free(oldnode);
        }
        ++hashcur;
    }
    free(hashp);
}
#endif

#ifdef DCHASH_DESTROY_ADVANCED
void dchash_destroy_advanced(DCHASH* hashp,
void (*dchash_user_dealloc)(void*)) {
    DCNODE* oldnode;
    DCNODE* curnode;
    DCNODE* endnode;
    DCLIST* hashcur = (DCLIST*)(hashp + 1);
    DCLIST* hashend = hashcur + hashp->listn;
    while (hashcur != hashend) {
        endnode = hashcur;
        curnode = hashcur->nextp;
        while (curnode != endnode) {
            oldnode = curnode;
            curnode = curnode->nextp;
            dchash_user_dealloc(oldnode+1);
            free(oldnode);
        }
        ++hashcur;
    }
    free(hashp);
}
#endif
