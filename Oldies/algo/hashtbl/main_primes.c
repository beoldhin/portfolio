/*
  To generate good primes for Linear Congruential Hash (PJW), the primes
  must be away from powers of 2. So to find these, a prime number nearest
  to the mean of two adjacent powers of 2 must be found:
  
  2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,65536,131072,
  262144,524288,1048576,2097152,4194304,8388608
  
            (2+4)/2 =      3, nearest prime is 3
            (4+8)/2 =      6, nearest primes are 5 or 7
           (8+16)/2 =     12, nearest primes are 11 or 13
          (16+32)/2 =     24, nearest prime is 23
          (32+64)/2 =     48, nearest prime is 47
         (64+128)/2 =     96, nearest prime is 97
        (128+256)/2 =    192, nearest primes are 191 or 193
        (256+512)/2 =    384, nearest prime is 383
       (512+1024)/2 =    768, nearest prime is 769
      (1024+2048)/2 =   1536, nearest prime is 1531
      (2048+4096)/2 =   3072, nearest prime is 3067
      (4096+8192)/2 =   6144, nearest prime is 6143
     (8192+16384)/2 =  12288, nearest prime is 12289
    (16384+32768)/2 =  24576, nearest prime is 24571
    (32768+65536)/2 =  49152, nearest prime is 49157
   (65536+131072)/2 =  98304, nearest prime is 98299
  (131072+262144)/2 = 196608, nearest prime is 196613
  (262144+524288)/2 = 393216, nearest prime is 393209
*/

#include <stdio.h>
#include <math.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

BOOL check_prime(UINT);


int main(void) {
    UINT i;
    for (i=2; i<=100; ++i) {
        if (check_prime(i)) {
            printf("%u\n", i);
        }
    }
    return 0;
}

BOOL check_prime(UINT num) {
    UINT i;
    UINT j;
    j = (UINT)sqrt((double)num) + 1;
    for (i=2; i<j; ++i) {  /* that MUST be "i<j" */
        if (num%i == 0)
            return FALSE;
    }
    return TRUE;
}
