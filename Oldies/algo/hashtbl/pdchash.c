/* CRC32, Weinberger&Holub, Bob Jenkins, Aho's 65599 and similar, and Linear */
/* Congruential type hashes rejected because Kernighan & Ritchie's x31       */
/* algorithm gives best results (similar to Aho's). It gives similar results */
/* to x37 but can be optimized further.                                      */

#include <string.h>
#include "pdchash.h"
#include "hashknr31.h"

/* The 'skeleton' is one block in this function */
/* This is problematic for very big hash tables */
#ifdef PDCHASH_HCREATE
PDCHASH* pdchash_hcreate(UINT listn, SCPOOL* scpoolp, TCPOOL* tcpoolp) {
    DCNODE* hashcur;
    DCNODE* hashend;
    PDCHASH* new_hash = (PDCHASH*)malloc(sizeof(PDCHASH)+(sizeof(DCNODE)*listn));
    if (new_hash == NULL)
        return NULL;
    new_hash->listn = listn;
    new_hash->satsize = scpoolp->statsize - sizeof(DCNODE);
    new_hash->scpoolp = scpoolp;
    new_hash->tcpoolp = tcpoolp;
    hashcur = (DCNODE*)(new_hash + 1);
    hashend = hashcur + listn;
    while (hashcur != hashend) {
        hashcur->prevp = hashcur;
        hashcur->nextp = hashcur;
        ++hashcur;
    }
    return new_hash;
}
#endif

#ifdef PDCHASH_RESIZE
PDCHASH* pdchash_resize(PDCHASH* hashp, UINT listn) {
    DCLIST* hashpos;
    DCLIST* hashcur;
    DCLIST* hashend;
    DCNODE* curnode;
    DCNODE* endnode;
    DCNODE* oldnode;
    PDCHASH* new_hash = pdchash_hcreate(listn, hashp->scpoolp, hashp->tcpoolp);
    if (new_hash == NULL)
        return NULL;
    hashcur = (DCLIST*)(hashp + 1);
    hashend = hashcur + hashp->listn;
    while (hashcur != hashend) {
        endnode = hashcur;
        curnode = hashcur->nextp;
        while (curnode != endnode) {
            oldnode = curnode;
            curnode = curnode->nextp;
            hashpos = (DCLIST*)(new_hash+1) +
            (hash_knr31(*(char**)(oldnode+1)) % listn);
/* Adjust oldnode pointers for hashnode & push back to hashnode -- begin */
            oldnode->prevp = hashpos->prevp;
            oldnode->nextp = hashpos;
            hashpos->prevp->nextp = oldnode;
            hashpos->prevp = oldnode;
/* Adjust oldnode pointers for hashnode & push back to hashnode -- end */
        }
        ++hashcur;
    }
    free(hashp);
    return new_hash;
}
#endif

#ifdef PDCHASH_FIND
void* pdchash_find(PDCHASH* hashp, char* strp) {
    DCNODE* endnode = (DCLIST*)(hashp+1) + (hash_knr31(strp) % hashp->listn);
    DCNODE* curnode = endnode->nextp;
    DCNODE* endplus1 = curnode;
    while (curnode != endnode) {
        if (strcmp(*(char**)(curnode+1),strp) == 0) {
/* move iter to head (MCD - Grune et. al) -- begin */
#ifdef MOVE_TO_HEAD
            if (curnode != endplus1) {
                curnode->prevp->nextp = curnode->nextp;
                curnode->nextp->prevp = curnode->prevp;
                curnode->prevp = endnode;
                curnode->nextp = endplus1;
                endnode->nextp->prevp = curnode;
                endnode->nextp = curnode;
            }
#endif
/* move iter to head (MCD - Grune et. al) -- end */
            return endplus1 + 1;
        }
        curnode = curnode->nextp;
    }
    return NULL;
}
#endif

/* The iterator is not needed as a function parameter because the strings */
/* in the hash table are randomly placed. Because iterator is not needed, */
/* pdchash_find_first and pdchash_find_last are not needed, either.       */
#ifdef PDCHASH_FIND_ADVANCED
void* pdchash_find_advanced(PDCHASH* hashp, char* strp,
BOOL (*pdchash_user_compare)(void*)) {
    DCNODE* endnode = (DCLIST*)(hashp+1) + (hash_knr31(strp) % hashp->listn);
    DCNODE* curnode = endnode->nextp;
    DCNODE* endplus1 = curnode;
    while (curnode != endnode) {
        if (pdchash_user_compare(curnode+1)) {
/* move iter to head (MCD - Grune et. al) -- begin */
#ifdef MOVE_TO_HEAD
            if (curnode != endplus1) {
                curnode->prevp->nextp = curnode->nextp;
                curnode->nextp->prevp = curnode->prevp;
                curnode->prevp = endnode;
                curnode->nextp = endplus1;
                endnode->nextp->prevp = curnode;
                endnode->nextp = curnode;
            }
#endif
/* move iter to head (MCD - Grune et. al) -- end */
            return endplus1 + 1;
        }
        curnode = curnode->nextp;
    }
    return NULL;
}
#endif

#ifdef PDCHASH_INSERT_NOALLOC
void* pdchash_insert_noalloc(PDCHASH* hashp, char* strp, void* satp) {
    DCLIST* newpos;
    DCNODE* new_node;
    UINT hashval = hash_knr31(strp);
    strcpy(*(char**)satp, strp);
    newpos = (DCLIST*)(hashp+1) + (hashval % hashp->listn);
/* push back -- begin */
    new_node = (DCNODE*)scpool_alloc(hashp->scpoolp);
    if (new_node == NULL)
        return NULL;
    new_node->prevp = newpos->prevp;
    new_node->nextp = newpos;
    newpos->prevp->nextp = new_node;
    newpos->prevp = new_node;
    memcpy(new_node+1, satp, hashp->satsize);
/* push back -- end */
    return new_node + 1;
}
#endif

/* Remember: don't put the "crc32_generate_string_length" line *after* */
/* the line "(char*)malloc(strl)"...                                   */
#ifdef PDCHASH_INSERT_ALLOC
void* pdchash_insert_alloc(PDCHASH* hashp, char* strp, void* satp) {
    UINT strl;
    DCLIST* newpos;
    DCNODE* new_node;
    UINT hashval = hash_knr31_length(strp, &strl);
    char* memory = tcpool_alloc(hashp->tcpoolp, strl);
    if (memory == NULL)
        return NULL;
    *(char**)satp = memory;
    memcpy(memory, strp, strl);
    newpos = (DCLIST*)(hashp+1) + (hashval % hashp->listn);
/* Push back begin -- begin */
    new_node = (DCNODE*)scpool_alloc(hashp->scpoolp);
    if (new_node == NULL)
        return NULL;
    new_node->prevp = newpos->prevp;
    new_node->nextp = newpos;
    newpos->prevp->nextp = new_node;
    newpos->prevp = new_node;
    memcpy(new_node+1, satp, hashp->satsize);
/* Push back end -- end */
    return new_node + 1;
}
#endif

#ifdef PDCHASH_DESTROY_FREE
void pdchash_destroy_free(PDCHASH* hashp) {
    DCNODE* oldnode;
    DCNODE* curnode;
    DCNODE* endnode;
    DCLIST* hashcur = (DCLIST*)(hashp + 1);
    DCLIST* hashend = hashcur + hashp->listn;
    while (hashcur != hashend) {
        endnode = hashcur;
        curnode = hashcur->nextp;
        while (curnode != endnode) {
            oldnode = curnode;
            curnode = curnode->nextp;
            free(*(char**)(oldnode+1));
        }
        ++hashcur;
    }
    free(hashp);
}
#endif

#ifdef PDCHASH_DESTROY_ADVANCED
void pdchash_destroy_advanced(PDCHASH* hashp,
void (*pdchash_user_dealloc)(void*)) {
    DCNODE* oldnode;
    DCNODE* curnode;
    DCNODE* endnode;
    DCLIST* hashcur = (DCLIST*)(hashp + 1);
    DCLIST* hashend = hashcur + hashp->listn;
    while (hashcur != hashend) {
        endnode = hashcur;
        curnode = hashcur->nextp;
        while (curnode != endnode) {
            oldnode = curnode;
            curnode = curnode->nextp;
            pdchash_user_dealloc(oldnode+1);
        }
        ++hashcur;
    }
    free(hashp);
}
#endif
