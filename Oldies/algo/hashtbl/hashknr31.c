#include "hashknr31.h"

#ifdef HASH_KNR31
uint32 hash_knr31(char* str) {
    uint32 x = 0;
    while (*str) {
        x = (x * 31) + *str;
        ++str;
    }
    return x;
}
#endif

#ifdef HASH_KNR31_LENGTH
uint32 hash_knr31_length(char* str, UINT* strl) {
    char* ptr;
    uint32 x = 0;
    for (ptr=str; *ptr; ++ptr) {
        x = (x * 31) + *ptr;
    }
    *strl = ptr - str + 1;
    return x;
}
#endif
