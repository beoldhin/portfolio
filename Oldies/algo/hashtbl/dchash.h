#ifndef _DCHASH_H
#define _DCHASH_H

#include <string.h>
#include "/home/olli/src/algo/list/dclist.h"

#ifndef TEST_COMPILATION
#include "/home/olli/src/incs/inc_current.h"
#endif

typedef struct dchash {
    UINT listn;
} DCHASH;

#ifdef TEST_COMPILATION
  #define DCHASH_HCREATE
  #define DCHASH_RESIZE
  #define DCHASH_FIND
  #define DCHASH_FIND_ADVANCED
  #define DCHASH_INSERT_NOALLOC
  #define DCHASH_INSERT_ALLOC
  #define DCHASH_DESTROY_NOFREE
  #define DCHASH_DESTROY_FREE
  #define DCHASH_DESTROY_ADVANCED
#endif

#ifdef DCHASH_HCREATE
DCHASH* dchash_hcreate(UINT);
#endif
#ifdef DCHASH_RESIZE
DCHASH* dchash_resize(DCHASH*, UINT);
#endif
#ifdef DCHASH_FIND
void* dchash_find(DCHASH*, char*);
#endif
#ifdef DCHASH_FIND_ADVANCED
void* dchash_find_advanced(DCHASH*, char*, BOOL (*)(void*));
#endif
#ifdef DCHASH_INSERT_NOALLOC
void* dchash_insert_noalloc(DCHASH*, char*, void*, UINT);
#endif
#ifdef DCHASH_INSERT_ALLOC
void* dchash_insert_alloc(DCHASH*, char*, void*, UINT);
#endif
#ifdef DCHASH_DESTROY_NOFREE
void dchash_destroy_nofree(DCHASH*);
#endif
#ifdef DCHASH_DESTROY_FREE
void dchash_destroy_free(DCHASH*);
#endif
#ifdef DCHASH_DESTROY_ADVANCED
void dchash_destroy_advanced(DCHASH*, void (*)(void*));
#endif

#endif /* _DCHASH_H */
