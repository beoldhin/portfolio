Java2 (fwd)

Christopher R. Hertel crh@ubiqx.mn.org
Tue, 17 Aug 1999 22:21:49 -0500 (CDT)

  * Previous message: Java2 (fwd)
  * Next message: Collections API with Java 1.1
  * Messages sorted by: [ date ] [ thread ] [ subject ] [ author ]

-------------------------------------------------------------------------------
>       [Allen, Michael (RSCH)]  
>       I am not sure that it is Sun that is at fault. After all they have
> provided the source code and diffs. I don't think they would care about
> a lot of hand-holding anyway. My guess is that the process they had
> going for 1.1 has been disrupted and or there are more serious techincal
> problems.

Not up on the structure of things.  I'm just making noise about other
issues I guess.  Please to excuse.  ;)

>       [Allen, Michael (RSCH)]  
>       I don't see why we couldn't just take the 1.2 source and snarf out
> the pertainant class(or the whole api if someone hasn't already). I was
> not a computer science major(biochemistry) so I'm not up to date on all
> the standard data stuctures. Can you describe Splay and AVL trees to me.
> I will look at the Collections source and see what we can take out of
> it.

I was a Bio major myself. ;)

David said that he'd done an AVL implementation.  I have some docs about
Splay vs. AVL at the ubiqx site (www.ubiqx.org or
racer-x.nts.umn.edu/ubiqx).

Basically:

AVL:    Every insertion or deletion forces a rebalancing of the tree
        to ensure that the difference between the height of the left and
        right subtrees is in the range -1..1.  There is a bit of overhead
        involved in maintaining the balance, but it forces an optimally
        balanced tree.

Splay:  The splay operation "bubbles" a node to the top of the tree.  A
        node is splayed to the top when it is inserted, when it is about
        to be deleted, and when it has been found as the result of a
        search.  The splay operation requires less overhead than the AVL
        balancing operation, but the tree is less likely to be optimally
        balanced.  It turns out, though, that perfect balance isn't needed
        for good speed.  The splaying "tends" to make the tree "bushier"
        and splaying found nodes to the top "tends" to speed things up by
        putting "popular" nodes near the top.

In general, I find the splay tree faster.  The AVL tree might be better
if you were going to load the data once and then search it.

I was also thinking of implementing an in-memory B-Tree.  Strange concept,
I know, but in low-memory situations a Splay or AVL tree hits a very big
wall.  If you run out of memory, you swap.  If each data element is stored
in an individual node from a more-or-less random part of memory, you wind
up paging every time you touch a node!  Urq.

So my thinking was that large, in-memory databases could be stored in a
B-Tree and if a block got swapped out it wouldn't be as bad.  Dunno if
it's useful but it was an interesting idea.  I haven't had time to finish
it because some *cough* other project has my attention just now. 

Chris -)-----

(And yes, I am just a bit crazy.)

-- 
Samba Team -- http://samba.org/         -)-----   Christopher R. Hertel
jCIFS Team -- http://jcifs.samba.org/   -)-----   crh@ubiqx.mn.org
ubiqx Team -- http://www.ubiqx.org/     -)-----   ubiqx development, uninq.

-------------------------------------------------------------------------------

  * Previous message: Java2 (fwd)
  * Next message: Collections API with Java 1.1
  * Messages sorted by: [ date ] [ thread ] [ subject ] [ author ]

