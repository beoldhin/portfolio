#include <stdio.h>
#include <string.h>
#include "tst.h"

#define BUFMAX 128


int main(void) {
    TSTNODE* mytst;
    char tmpbuf[BUFMAX];
    char searchstr[] = "cascade";
    
    puts("Enter strings, end by entering a null string:");
    
    mytst = tst_create();
    
    while (1) {
        fgets(tmpbuf, BUFMAX, stdin);
        if (tmpbuf[0] == '\n')
            break;
        tmpbuf[strlen(tmpbuf)-1] = '\0';
        tst_insert(&mytst, tmpbuf);
    }
    
    printf("Searching for \"%s\": ", searchstr);
    if (tst_find(mytst,searchstr)) {
        puts("Found.");
    } else {
        puts("Not found.");
    }
    
    tst_destroy(mytst);
    
    return 0;
}
