#include "ptst.h"

#ifdef TEST_COMPILATION
  #define PTST_BALANCE_COLLECT
  #define PTST_BALANCE_RINSERT
  #define PTST_BALANCE_INSERT
  #define PTST_RDESTROY
#endif

#ifdef PTST_BALANCE_COLLECT
void ptst_balance_collect(PTSTNODE*, PTSTNODE***);
#endif
#ifdef PTST_BALANCE_RINSERT
void ptst_balance_rinsert(PTST*, PTSTNODE**, UINT);
#endif
#ifdef PTST_BALANCE_INSERT
void ptst_balance_insert(PTST*, PTSTNODE*);
#endif
#ifdef PTST_RDESTROY
void ptst_rdestroy(PTSTNODE*, void (*)(void*));
#endif


#ifdef PTST_SCREATE
void ptst_screate(PTST* tstp, SCPOOL* ncpoolp, SCPOOL* scpoolp,
TCPOOL* tcpoolp) {
    tstp->ptst = NULL;
    tstp->seqn = 0;
    tstp->ncpoolp = ncpoolp;
    tstp->scpoolp = scpoolp;
    tstp->tcpoolp = tcpoolp;
}
#endif

/* Pool for nodes, pool for user struct, pool for strings */
#ifdef PTST_HCREATE
PTST* ptst_hcreate(SCPOOL* ncpoolp, SCPOOL* scpoolp, TCPOOL* tcpoolp) {
    PTST* new_tst;
    new_tst = (PTST*)malloc(sizeof(PTST));
    if (new_tst == NULL)
        return NULL;
    new_tst->ptst = NULL;
    new_tst->seqn = 0;
    new_tst->ncpoolp = ncpoolp;
    new_tst->scpoolp = scpoolp;
    new_tst->tcpoolp = tcpoolp;
    return new_tst;
}
#endif

/*
  This function is implemented because we can't do what Sedgewick recommends
  (i.e. to first collect the strings, then sort them, and finally save them to
  the ternary tree). Because we are using symbol tables we have to search for
  a string sequence in the tree when it not yet have all the strings in the
  set. This function creates a new tree from the current one and uses a LOT
  of memory, at least N*2 (an AVL/Splay tree implementation would be better).
*/
#ifdef PTST_BALANCE
void ptst_balance(PTST* tstp) {
    PTSTNODE* larray;
    PTSTNODE** larrayp;
    /* create a sorted linear array of the tree nodes */
    larray = malloc(tstp->seqn*sizeof(PTSTNODE*));
    larrayp = (PTSTNODE**)larray;
    ptst_balance_collect(tstp->ptst, &larrayp);
    /* destroy the old tree (no need to use user_dealloc function) */
    scpool_reset(tstp->ncpoolp);
    /* create a new balanced tree by recursively inserting the set */
    tstp->ptst = NULL;
    ptst_balance_rinsert(tstp, (PTSTNODE**)larray, tstp->seqn);
    free(larray);
}
#endif

#ifdef PTST_BALANCE_COLLECT
void ptst_balance_collect(PTSTNODE* tstp, PTSTNODE*** larraypp) {
    if (tstp) {
        ptst_balance_collect(tstp->ltnode, larraypp);
        if (tstp->splitch)
            ptst_balance_collect(tstp->eqnode, larraypp);
        else {
            **larraypp = tstp->eqnode;
            ++(*larraypp);
        }
        ptst_balance_collect(tstp->gtnode, larraypp);
    }
}
#endif

#ifdef PTST_BALANCE_RINSERT
void ptst_balance_rinsert(PTST* tstp, PTSTNODE** larray, UINT n) {
    UINT m;
    if (n >= 1) {
        m = n / 2;
        ptst_balance_insert(tstp, larray[m]);
        ptst_balance_rinsert(tstp, larray, m);
        ptst_balance_rinsert(tstp, larray+m+1, n-m-1);
    }
}
#endif

#ifdef PTST_BALANCE_INSERT
void ptst_balance_insert(PTST* tstp, PTSTNODE* eqnode) {
    int d;
    char* strp;
    PTSTNODE* p;
    PTSTNODE** pp;
    strp = *(char**)eqnode;
    pp = &tstp->ptst;
    while ((p = *pp)) {
        if ((d=*strp-p->splitch) == 0) {
            if (*strp++ == 0)
                return;
            pp = &(p->eqnode);
        } else if (d < 0)
            pp = &(p->ltnode);
        else
            pp = &(p->gtnode);
    }
    while (1) {
        p = (PTSTNODE*)scpool_alloc(tstp->ncpoolp);
        *pp = p;
        p->splitch = *strp;
        p->ltnode = NULL;
        p->eqnode = NULL;
        p->gtnode = NULL;
        if (*strp++ == 0) {
            p->eqnode = eqnode;
            return;
        }
        pp = &(p->eqnode);
    }
}
#endif

#ifdef PTST_INSERT
void* ptst_insert(PTST* tstp, char* strp, UINT strl) {
    int d;
    char* instr;
    char* newtc;
    PTSTNODE* p;
    PTSTNODE** pp;
    PTSTNODE* newsc;
    instr = strp;
    pp = &tstp->ptst;
    while ((p = *pp)) {
        if ((d=*strp-p->splitch) == 0) {
            if (*strp++ == 0)
                return NULL;  /* no need to create new nodes, seq. exists */
            pp = &(p->eqnode);
        } else if (d < 0)
            pp = &(p->ltnode);
        else
            pp = &(p->gtnode);
    }
    while (1) {
        p = (PTSTNODE*)scpool_alloc(tstp->ncpoolp);
        *pp = p;
        p->splitch = *strp;
        p->ltnode = NULL;
        p->eqnode = NULL;
        p->gtnode = NULL;
        if (*strp++ == 0) {
            newtc = tcpool_alloc(tstp->tcpoolp, strl);
            newsc = (PTSTNODE*)scpool_alloc(tstp->scpoolp);
            memcpy(newtc, instr, strl);
            *(char**)newsc = newtc;
            p->eqnode = newsc;
            tstp->seqn++;
            return newsc;
        }
        pp = &(p->eqnode);
    }
}
#endif

#ifdef PTST_FIND
void* ptst_find(PTST* tstp, char* strp) {
    int d;
    PTSTNODE* p;
    p = tstp->ptst;
    while (p) {
        if ((d=*strp-p->splitch) == 0) {
            if (*strp++ == 0)
                return p->eqnode;
            p = p->eqnode;
        } else if (d < 0) {
            p = p->ltnode;
        } else
            p = p->gtnode;
    }
    return NULL;
}
#endif

#ifdef PTST_CLEAR
void ptst_clear(PTST* tstp, void (*ptst_user_dealloc)(void*)) {
    ptst_rdestroy(tstp->ptst, ptst_user_dealloc);
    scpool_clear(tstp->ncpoolp);
    tstp->ptst = NULL;
    tstp->seqn = 0;
}
#endif

#ifdef PTST_DESTROY
void ptst_destroy(PTST* tstp, void (*ptst_user_dealloc)(void*)) {
    ptst_rdestroy(tstp->ptst, ptst_user_dealloc);
    scpool_destroy(tstp->ncpoolp);
    free(tstp);
}
#endif

#ifdef PTST_RDESTROY
void ptst_rdestroy(PTSTNODE* tstp, void (*ptst_user_dealloc)(void*)) {
    if (tstp) {
        ptst_rdestroy(tstp->ltnode, ptst_user_dealloc);
        if (tstp->splitch)
            ptst_rdestroy(tstp->eqnode, ptst_user_dealloc);
        else
            ptst_user_dealloc(tstp->eqnode);
        ptst_rdestroy(tstp->gtnode, ptst_user_dealloc);
    }
}
#endif
