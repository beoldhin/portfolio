#include <stdio.h>
#include <string.h>
#include "ptst.h"

#define BUFMAX 128

void dealloc_function(void*);

typedef struct mystruct {
    char* mystr;  /* ptst.c needs this to be the first */
    int someint;
} MYSTRUCT;


int main(void) {
    PTST mytst;
    PTST* mytstp;
    SCPOOL ncpool;
    SCPOOL scpool;
    TCPOOL tcpool;
    SCPOOL* ncpoolp;
    SCPOOL* scpoolp;
    TCPOOL* tcpoolp;
    MYSTRUCT* searchptr;
    char tmpbuf[BUFMAX];
    char searchstr[] = "cascade";
    
    mytstp = &mytst;
    ncpoolp = &ncpool;
    scpoolp = &scpool;
    tcpoolp = &tcpool;
    puts("Enter strings, end by entering a null string:");
    
    scpool_screate(ncpoolp, 10000, sizeof(PTSTNODE));
    scpool_screate(scpoolp, 10000, sizeof(MYSTRUCT));
    tcpool_screate(tcpoolp, 100000);
    
    ptst_screate(mytstp, ncpoolp, scpoolp, tcpoolp);
    
    while (1) {
        fgets(tmpbuf, BUFMAX, stdin);
        if (tmpbuf[0] == '\n')
            break;
        tmpbuf[strlen(tmpbuf)-1] = '\0';
        ptst_insert(mytstp, tmpbuf, strlen(tmpbuf)+1);
    }
    
    /*
    ptst_balance(mytst);
    */
    
    printf("Searching for \"%s\": ", searchstr);
    if ((searchptr=ptst_find(mytstp,searchstr))) {
        puts("Found.");
    } else {
        puts("Not found.");
    }
    
    ptst_clear(mytstp, dealloc_function);
    
    tcpool_clear(tcpoolp);
    scpool_clear(scpoolp);
    
    return 0;
}

void dealloc_function(void* satp) {
    /* -- Not used when pooling
    free(*(char**)satp);
    */
}
