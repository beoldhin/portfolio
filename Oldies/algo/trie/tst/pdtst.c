/* Balancing function was removed - it is useless and too clumsy to use */
/* Support for user_dealloc removed - useless, use pools instead */

/* Doubly linked list (non-circular) implementation of the TST         */
/* The list is actually circular, but without the sentinel: p->eqnode  */
/* defines the first list node (this saves spaces, but actually makes  */
/* some comparisons more complex).                                     */
#include "pdtst.h"

#ifdef TEST_COMPILATION
  #define PDTST_FIND_DLIST
#endif

#ifdef PDTST_FIND_DLIST
DNODE* pdtst_find_dlist(PDTST*, char*);
#endif

#ifdef PDTST_SCREATE
void pdtst_screate(PDTST* tstp, SCPOOL* ncpoolp, SCPOOL* scpoolp) {
    tstp->pdtst = NULL;
    tstp->ncpoolp = ncpoolp;
    tstp->scpoolp = scpoolp;
}
#endif

/* Pool for nodes, pool for user struct, pool for strings */
#ifdef PDTST_HCREATE
PDTST* pdtst_hcreate(SCPOOL* ncpoolp, SCPOOL* scpoolp) {
    PDTST* new_tst;
    new_tst = (PDTST*)malloc(sizeof(PDTST));
    if (new_tst == NULL)
        return NULL;
    new_tst->pdtst = NULL;
    new_tst->ncpoolp = ncpoolp;
    new_tst->scpoolp = scpoolp;
    return new_tst;
}
#endif

#ifdef PDTST_INSERT
void* pdtst_insert(PDTST* tstp, char* strp) {
    int d;
    DNODE* lnode;
    DNODE* lstart;
    PDTSTNODE* p;
    PDTSTNODE** pp;
    pp = &tstp->pdtst;
    while ((p = *pp)) {
        if ((d=*strp-p->splitch) == 0) {
            if (*strp++ == 0) {
                lstart = (DNODE*)p->eqnode;
                lnode = (DNODE*)scpool_alloc(tstp->scpoolp);
                lnode->prevp = lstart->prevp;
                lnode->nextp = lstart;
                lstart->prevp->nextp = lnode;
                lstart->prevp = lnode;
                return lnode + 1;
            }
            pp = &(p->eqnode);
        } else if (d < 0)
            pp = &(p->ltnode);
        else
            pp = &(p->gtnode);
    }
    while (1) {
        p = (PDTSTNODE*)scpool_alloc(tstp->ncpoolp);
        *pp = p;
        p->splitch = *strp;
        p->ltnode = NULL;
        p->eqnode = NULL;
        p->gtnode = NULL;
        if (*strp++ == 0) {
            lnode = (DNODE*)scpool_alloc(tstp->scpoolp);
            lnode->prevp = lnode;
            lnode->nextp = lnode;
            p->eqnode = (PDTSTNODE*)lnode;
            return lnode + 1;
        }
        pp = &(p->eqnode);
    }
}
#endif

#ifdef PDTST_FIND_DLIST
DNODE* pdtst_find_dlist(PDTST* tstp, char* strp) {
    int d;
    PDTSTNODE* p;
    p = tstp->pdtst;
    while (p) {
        if ((d=*strp-p->splitch) == 0) {
            if (*strp++ == 0)
                return (DNODE*)p->eqnode;
            p = p->eqnode;
        } else if (d < 0) {
            p = p->ltnode;
        } else
            p = p->gtnode;
    }
    return NULL;
}
#endif

/* the pdtst_user_compare function should be used when comparing different */
/* scopes or other information that can be used to separate identical strs */
#ifdef PDTST_FIND
void* pdtst_find(PDTST* tstp, char* strp, BOOL (*pdtst_user_compare)(void*)) {
    DNODE* lnode;
    DNODE* curnode;
    if ((lnode=pdtst_find_dlist(tstp,strp))) {
        curnode = lnode;
        do {
            if ((pdtst_user_compare(curnode+1)))
                return curnode + 1;
            curnode = curnode->nextp;
        } while (curnode != lnode);
    }
    return NULL;
}
#endif

#ifdef PDTST_CLEAR
void pdtst_clear(PDTST* tstp) {
    scpool_clear(tstp->ncpoolp);
    tstp->pdtst = NULL;
}
#endif

#ifdef PDTST_DESTROY
void pdtst_destroy(PDTST* tstp) {
    scpool_destroy(tstp->ncpoolp);
    free(tstp);
}
#endif
