#include "tst.h"

#ifdef TST_CREATE
TSTNODE* tst_create(void) {
    return NULL;
}
#endif

#ifdef TST_INSERT
void tst_insert(TSTROOT** tstpp, char* strp) {
    int d;
    char* instr;
    TSTNODE* p;
    TSTNODE** pp;
    instr = strp;
    pp = tstpp;
    while ((p = *pp)) {
        if ((d=*strp-p->splitch) == 0) {
            if (*strp++ == 0)
                return;
            pp = &(p->eqnode);
        } else if (d < 0)
            pp = &(p->ltnode);
        else
            pp = &(p->gtnode);
    }
    while (1) {
        p = (TSTNODE*)malloc(sizeof(TSTNODE));
        *pp = p;
        p->splitch = *strp;
        p->ltnode = NULL;
        p->eqnode = NULL;
        p->gtnode = NULL;
        if (*strp++ == 0) {
            p->eqnode = (TSTNODE*)instr;
            return;
        }
        pp = &(p->eqnode);
    }
}
#endif

#ifdef TST_FIND
BOOL tst_find(TSTROOT* tstp, char* strp) {
    int d;
    TSTNODE* p;
    p = tstp;
    while (p) {
        if ((d=*strp-p->splitch) == 0) {
            if (*strp++ == 0)
                return TRUE;
            p = p->eqnode;
        } else if (d < 0) {
            p = p->ltnode;
        } else
            p = p->gtnode;
    }
    return FALSE;
}
#endif

/* using postorder traversal to free the nodes */
#ifdef TST_DESTROY
void tst_destroy(TSTROOT* tstp) {
    if (tstp) {
        tst_destroy(tstp->ltnode);
        if (tstp->splitch)
            tst_destroy(tstp->eqnode);
        tst_destroy(tstp->gtnode);
        free(tstp);
    }
}
#endif
