#ifndef _PDTST_H
#define _PDTST_H

#include <stdlib.h>
#include "/home/olli/src/algo/pool/scpool.h"
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct tstnode {
    char splitch;
    struct tstnode* ltnode;
    struct tstnode* eqnode;
    struct tstnode* gtnode;
} PDTSTROOT,PDTSTNODE;

typedef struct dnode {
    struct dnode* prevp;
    struct dnode* nextp;
} DLIST,DNODE;

typedef struct pdtst {
    PDTSTROOT* pdtst;
    SCPOOL* ncpoolp;
    SCPOOL* scpoolp;
} PDTST;

#ifdef TEST_COMPILATION
  #define PDTST_SCREATE
  #define PDTST_HCREATE
  #define PDTST_INSERT
  #define PDTST_FIND
  #define PDTST_CLEAR
  #define PDTST_DESTROY
#endif

#ifdef PDTST_SCREATE
void pdtst_screate(PDTST*, SCPOOL*, SCPOOL*);
#endif
#ifdef PDTST_HCREATE
PDTST* pdtst_hcreate(SCPOOL*, SCPOOL*);
#endif
#ifdef PDTST_INSERT
void* pdtst_insert(PDTST*, char*);
#endif
#ifdef PDTST_FIND
void* pdtst_find(PDTST*, char*, BOOL (*)(void*));
#endif
#ifdef PDTST_CLEAR
void pdtst_clear(PDTST*);
#endif
#ifdef PDTST_DESTROY
void pdtst_destroy(PDTST*);
#endif

#endif  /* _PDTST_H */
