#include <stdio.h>
#include <string.h>
#include "pdtst.h"

#define BUFMAX 128

BOOL compare_function(void*);

typedef struct mystruct {
    int someint1;
    int someint2;
} MYSTRUCT;


int main(void) {
    PDTST mytst;
    PDTST* mytstp;
    SCPOOL ncpool;
    SCPOOL scpool;
    SCPOOL* ncpoolp;
    SCPOOL* scpoolp;
    DNODE* searchptr;
    MYSTRUCT* insertptr;
    char tmpbuf[BUFMAX];
    char searchstr[] = "cascade";
    
    puts("Enter strings, end by entering a null string:");
    
    mytstp = &mytst;
    ncpoolp = &ncpool;
    scpoolp = &scpool;
    scpool_screate(ncpoolp, 10000, sizeof(PDTSTNODE));
    scpool_screate(scpoolp, 10000, sizeof(DNODE)+sizeof(MYSTRUCT));
    
    pdtst_screate(mytstp, ncpoolp, scpoolp);
    
    while (1) {
        fgets(tmpbuf, BUFMAX, stdin);
        if (tmpbuf[0] == '\n')
            break;
        tmpbuf[strlen(tmpbuf)-1] = '\0';
        insertptr = pdtst_insert(mytstp, tmpbuf);
        insertptr->someint1 = 123;
        insertptr->someint2 = 321;
    }
    
    printf("Searching for \"%s\": ", searchstr);
    if ((searchptr=pdtst_find(mytstp,searchstr,compare_function))) {
        puts("Found.");
    } else {
        puts("Not found.");
    }
    
    pdtst_clear(mytstp);
    
    scpool_clear(scpoolp);
    
    return 0;
}

BOOL compare_function(void* satp) {
    MYSTRUCT* sptr;
    sptr = (MYSTRUCT*)satp;
    if (sptr->someint1 == 123)
        return TRUE;
    return FALSE;
}
