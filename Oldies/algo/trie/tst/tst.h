#ifndef _TST_H
#define _TST_H

#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct tstnode {
    char splitch;
    struct tstnode* ltnode;
    struct tstnode* eqnode;
    struct tstnode* gtnode;
} TSTNODE,TSTROOT;

#ifdef TEST_COMPILATION
  #define TST_CREATE
  #define TST_INSERT
  #define TST_FIND
  #define TST_DESTROY
#endif

#ifdef TST_CREATE
TSTROOT* tst_create(void);
#endif
#ifdef TST_INSERT
void tst_insert(TSTROOT**, char*);
#endif
#ifdef TST_FIND
BOOL tst_find(TSTROOT*, char*);
#endif
#ifdef TST_DESTROY
void tst_destroy(TSTROOT*);
#endif

#endif  /* _TST_H */
