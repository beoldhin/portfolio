#ifndef _PTST_H
#define _PTST_H

#include <stdlib.h>
#include "/home/olli/src/algo/pool/scpool.h"
#include "/home/olli/src/algo/pool/tcpool.h"
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct tstnode {
    char splitch;
    struct tstnode* ltnode;
    struct tstnode* eqnode;
    struct tstnode* gtnode;
} PTSTROOT,PTSTNODE;

typedef struct ptst {
    PTSTROOT* ptst;
    UINT seqn;   /* the balancer function needs to know this */
    SCPOOL* ncpoolp;
    SCPOOL* scpoolp;
    TCPOOL* tcpoolp;
} PTST;

#ifdef TEST_COMPILATION
  #define PTST_SCREATE
  #define PTST_HCREATE
  #define PTST_INSERT
  #define PTST_BALANCE
  #define PTST_FIND
  #define PTST_CLEAR
  #define PTST_DESTROY
#endif

#ifdef PTST_SCREATE
void ptst_screate(PTST*, SCPOOL*, SCPOOL*, TCPOOL*);
#endif
#ifdef PTST_HCREATE
PTST* ptst_hcreate(SCPOOL*, SCPOOL*, TCPOOL*);
#endif
#ifdef PTST_INSERT
void* ptst_insert(PTST*, char*, UINT);
#endif
#ifdef PTST_BALANCE
void ptst_balance(PTST*);
#endif
#ifdef PTST_FIND
void* ptst_find(PTST*, char*);
#endif
#ifdef PTST_CLEAR
void ptst_clear(PTST*, void (*)(void*));
#endif
#ifdef PTST_DESTROY
void ptst_destroy(PTST*, void (*)(void*));
#endif

#endif  /* _PTST_H */
