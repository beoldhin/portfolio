#include "bsrch.h"


#ifdef BSRCH_SEARCH_ASC
void* bsrch_search_asc(void* data, void* value, UINT esize, UINT length,
int (*compare)(void*,void*)) {
    int ret;
    void* pos;
    UINT look;
    UINT lo = 0;
    UINT hi = length;
    while (hi > lo) {
        look = (hi+lo) / 2;
        pos = (char*)data + (look*esize);
        ret = compare(pos, value);
        if (ret > 0)
            lo = look + 1;
        else if (ret < 0)
            hi = look;
        else
            return pos;
    }
    return NULL;
}
#endif

#ifdef BSRCH_SEARCH_DESC
void* bsrch_search_desc(void* data, void* value, UINT esize, UINT length,
int (*compare)(void*,void*)) {
    int ret;
    void* pos;
    UINT look;
    UINT lo = 0;
    UINT hi = length;
    while (hi > lo) {
        look = (hi+lo) / 2;
        pos = (char*)data + (look*esize);
        ret = compare(pos, value);
        if (ret > 0)
            hi = look;
        else if (ret < 0)
            lo = look + 1;
        else
            return pos;
    }
    return NULL;
}
#endif
