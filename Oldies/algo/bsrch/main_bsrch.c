#include <stdio.h>
#include "bsrch.h"

#define LEN 150

int compare(void*, void*);


int main(void) {
    int i;
    int* ret;
    int value = 2;
    int data[LEN];
    
    for (i=1; i<=LEN; ++i)
        data[i-1] = i;
    for (i=1; i<=LEN; ++i) {
        value = i;
        ret = bsrch_search_asc(data, &value, sizeof(data[0]), LEN, compare);
        if (!ret)
            printf("FAILED: %d\n", value);
    }
    
    for (i=1; i<=LEN; ++i) {
        data[LEN-i] = i;
    }
    for (i=1; i<=LEN; ++i) {
        value = i;
        ret = bsrch_search_desc(data, &value, sizeof(data[0]), LEN, compare);
        if (!ret)
            printf("FAILED: %d\n", value);
    }
    
    return 0;
}

int compare(void* data, void* value) {
    if (*(int*)value > *(int*)data)
        return +1;
    if (*(int*)value < *(int*)data)
        return -1;
    return 0;
}
