#ifndef _BSRCH_H
#define _BSRCH_H

#include "/home/olli/src/algo/defines/nlimits.h"

#ifdef TEST_COMPILATION
  #define BSRCH_SEARCH_ASC
  #define BSRCH_SEARCH_DESC
#endif

#ifdef BSRCH_SEARCH_ASC
void* bsrch_search_asc(void*, void*, UINT, UINT, int (*)(void*,void*));
#endif
#ifdef BSRCH_SEARCH_DESC
void* bsrch_search_desc(void*, void*, UINT, UINT, int (*)(void*,void*));
#endif

#endif  /* _BSRCH_H */
