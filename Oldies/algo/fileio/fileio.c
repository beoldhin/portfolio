#include <string.h>
#include <stdlib.h>
#include "fileio.h"

#ifdef FILEIO_INIT
void fileio_init(FILEIO* fiop) {
    fiop->omode = 0;
    fiop->filep = NULL;
    fiop->startp = NULL;
    fiop->endp = NULL;
}
#endif

#ifdef FILEIO_OPEN
BOOL fileio_open(FILEIO* fiop, const char* path, UINT mode) {
    UINT8 binary;
    char* chmodep;
    char chmode[4];
    char chmode1[] = "r";
    char chmode2[] = "w";
    char chmode3[] = "a";
    char chmode4[] = "r+";
    char chmode5[] = "w+";
    char chmode6[] = "a+";
    memset(chmode, 0, 4);
    binary = 0;
    if (mode&BINARY) {
        binary = BINARY;
        mode -= BINARY;
    }
    switch (mode) {
      case READ:
        chmodep = chmode1;
        break;
      case WRITE_CREATE:
        chmodep = chmode2;
        break;
      case WRITE_APPEND:
        chmodep = chmode3;
        break;
      case READ_WRITE:
        chmodep = chmode4;
        break;
      case READ_WRITE_CREATE:
        chmodep = chmode5;
        break;
      case READ_WRITE_APPEND:
        chmodep = chmode6;
        break;
      default:
        fprintf(stderr, "FILEIO: Unknown file open mode.\n");
        return FALSE;
    }
    strcpy(chmode, chmodep);
    if (binary)
        strcat(chmode, "b");
    if (!fiop->filep) {
        if (!(fiop->filep=fopen(path,chmode))) {
            fprintf(stderr, "FILEIO: File open failed.\n");
            return FALSE;
        }
    } else {
        if (!(fiop->filep=freopen(path,chmode,fiop->filep))) {
            fprintf(stderr, "FILEIO: File reopen failed.\n");
            return FALSE;
        }
    }
    fiop->omode = mode;
    return TRUE;
}
#endif

#ifdef FILEIO_READ_ALL
BOOL fileio_read_all(FILEIO* fiop) {
    long fpos;
    if ((fseek(fiop->filep,0,SEEK_END)) < 0) {
        fprintf(stderr, "FILEIO: Seek to end failed.\n");
        return FALSE;
    }
    if ((fpos=ftell(fiop->filep)) < 0) {
        fprintf(stderr, "FILEIO: File position reporting failed.\n");
        return FALSE;
    }
    if ((fseek(fiop->filep,0,SEEK_SET)) < 0) {
        fprintf(stderr, "FILEIO: Seek to start failed.\n");
        return FALSE;
    }
    if (!(fiop->startp=malloc((size_t)fpos))) {
        fprintf(stderr, "FILEIO: Memory allocation failed.\n");
        return FALSE;
    }
    if ((fread(fiop->startp,1,(size_t)fpos,fiop->filep)) != fpos) {
        fprintf(stderr, "FILEIO: File read failed.\n");
        return FALSE;
    }
    fiop->endp = (char*)fiop->startp + fpos;
    return TRUE;
}
#endif

#ifdef FILEIO_APPEND
BOOL fileio_append(FILEIO* fiop, UINT8* data, UINT datal) {
    UINT8 omode;
    omode = fiop->omode - BINARY;
    if (omode==READ || omode==READ_WRITE || omode==WRITE_CREATE ||
    omode==READ_WRITE_CREATE) {
        fprintf(stderr, "FILEIO: Invalid mode for append.\n");
    }
    if ((fwrite(data,1,datal,fiop->filep)) != datal) {
        fprintf(stderr, "FILEIO: File write failed.\n");
        return FALSE;
    }
    return TRUE;
}
#endif

#ifdef FILEIO_CLOSE_ALL
BOOL fileio_close_all(FILEIO* fiop) {
    if (fclose(fiop->filep) == EOF) {
        fprintf(stderr, "FILEIO: File close failed.\n");
        return FALSE;
    }
    free(fiop->startp);
    fiop->startp = NULL;
    fiop->filep = NULL;
    fiop->omode = 0;
    return TRUE;
}
#endif

#ifdef FILEIO_CLOSE_FILE
BOOL fileio_close_file(FILEIO* fiop) {
    if (fclose(fiop->filep) == EOF) {
        fprintf(stderr, "FILEIO: File close failed.\n");
        return FALSE;
    }
    fiop->filep = NULL;
    fiop->omode = 0;
    return TRUE;
}
#endif

#ifdef FILEIO_FREE_MEMORY
void fileio_free_memory(FILEIO* fiop) {
    free(fiop->startp);
    fiop->startp = NULL;
}
#endif
