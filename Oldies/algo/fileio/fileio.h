#include <stdio.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

/*
  READ              (r)
  WRITE_CREATE      (w)
  WRITE_APPEND      (a)
  READ_WRITE        (r+)
  READ_WRITE_CREATE (w+)
  READ_WRITE_APPEND (a+)
*/

#define READ              0x01
#define READ_WRITE        0x02
#define WRITE_CREATE      0x04
#define READ_WRITE_CREATE 0x08
#define WRITE_APPEND      0x10
#define READ_WRITE_APPEND 0x20
#define BINARY            0x40

typedef struct fileio {
    UINT8 omode;
    FILE* filep;
    void* startp;
    void* endp;
} FILEIO;

#ifdef TEST_COMPILATION
  #define FILEIO_INIT
  #define FILEIO_OPEN
  #define FILEIO_READ_ALL
  #define FILEIO_APPEND
  #define FILEIO_CLOSE_ALL
  #define FILEIO_CLOSE_FILE
  #define FILEIO_FREE_MEMORY
#endif

#ifdef FILEIO_INIT
void fileio_init(FILEIO*);
#endif
#ifdef FILEIO_OPEN
BOOL fileio_open(FILEIO*, const char*, UINT);
#endif
#ifdef FILEIO_READ_ALL
BOOL fileio_read_all(FILEIO*);
#endif
#ifdef FILEIO_APPEND
BOOL fileio_append(FILEIO*, UINT8*, UINT);
#endif
#ifdef FILEIO_CLOSE_ALL
BOOL fileio_close_all(FILEIO*);
#endif
#ifdef FILEIO_CLOSE_FILE
BOOL fileio_close_file(FILEIO*);
#endif
#ifdef FILEIO_FREE_MEMORY
void fileio_free_memory(FILEIO*);
#endif
