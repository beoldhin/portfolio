#include <stdio.h>
#include "fileio.h"

int main(void) {
    char* fp;
    char* fep;
    FILEIO fio;
    FILEIO* fiop;
    char mystr[] = "testing too...\n";
    
    fiop = &fio;
    fileio_init(fiop);
    fileio_open(fiop, "test.txt", WRITE_APPEND);
    fileio_append(fiop, (UINT8*)mystr, strlen(mystr));
    fileio_open(fiop, "test.txt", READ|BINARY);
    if (fileio_read_all(fiop)) {
        fp = fiop->startp;
        fep = fiop->endp;
        while (fp < fep) {
            putchar(*fp);
            fp++;
        }
    }
    fileio_close_all(fiop);
    
    return 0;
}
