/* Cleared & standardized & encapsulated the ugly looking code at: */
/* http://www.math.keio.ac.jp/~matumoto/cokus.c                    */

#include "mtcokus.h"

#ifdef TEST_COMPILATION
#define MTCOKUS_RELOAD
#endif

#define M               (397)
#define K               (0x9908B0DFU)
#define hiBit(u)        ((u) & 0x80000000U)
#define loBit(u)        ((u) & 0x00000001U)
#define loBits(u)       ((u) & 0x7FFFFFFFU)
#define mixBits(u, v)   (hiBit(u)|loBits(v))


#ifdef TEST_COMPILATION
  #define MTCOKUS_RELOAD
#endif


#ifdef MTCOKUS_RELOAD
UINT32 mtcokus_reload(MTCOKUS*);
#endif


#ifdef MTCOKUS_SCREATE
void mtcokus_screate(MTCOKUS* mtcp) {
    mtcp->left = -1;
}
#endif

#ifdef MTCOKUS_HCREATE
MTCOKUS* mtcokus_hcreate(void) {
    MTCOKUS* new_mtc;
    new_mtc = (MTCOKUS*)malloc(sizeof(MTCOKUS));
    if (new_mtc == NULL)
        return NULL;
    new_mtc->left = -1;
    return new_mtc;
}
#endif

/* left appears 1 times => no need to simplify */
/* state appears 1 times => no need to simplify */
#ifdef MTCOKUS_SEED
void mtcokus_seed(MTCOKUS* mtcp, UINT32 seed) {
    int j;
    UINT32 x;
    UINT32* s;
    s = mtcp->state;
    x = (seed | 1U) & 0xFFFFFFFFU;
    for (mtcp->left=0, *s++=x, j=624; --j;
         *s++ = (x*=69069U) & 0xFFFFFFFFU);
}
#endif

/* left appears 2 times => no simplified (so-so) */
/* next appears 1 times => no need to simplify */
/* state appears 8 times => simplified */
#ifdef MTCOKUS_RELOAD
UINT32 mtcokus_reload(MTCOKUS* mtcp) {
    int j;
    UINT32 s0;
    UINT32 s1;
    UINT32* p0;
    UINT32* p2;
    UINT32* pM;
    UINT32* state;
    
    state = mtcp->state;
    p0 = state;
    p2 = state + 2;
    pM = state + M;
    
    if (mtcp->left < -1)
        mtcokus_seed(mtcp, 4357U);
    
    mtcp->left = 624 - 1;
    mtcp->next = state + 1;
    
    for (s0=state[0],s1=state[1],j=624-M+1; --j; s0=s1,s1=*p2++)
        *p0++ = *pM++ ^ (mixBits(s0,s1) >> 1) ^ (loBit(s1) ? K : 0U);
    
    for (pM=state,j=M; --j; s0=s1,s1=*p2++)
        *p0++ = *pM++ ^ (mixBits(s0,s1) >> 1) ^ (loBit(s1) ? K : 0U);
    
    s1 = state[0];
    *p0 = *pM ^ (mixBits(s0,s1) >> 1) ^ (loBit(s1) ? K : 0U);
    
    s1 ^= (s1 >> 11);
    s1 ^= (s1 <<  7) & 0x9D2C5680U;
    s1 ^= (s1 << 15) & 0xEFC60000U;
    return (s1 ^ (s1 >> 18));
}
#endif

/* left appears 1 times => no need to simplify */
/* next appears 1 times => no need to simplify */
#ifdef MTCOKUS_RANDOM
UINT32 mtcokus_random(MTCOKUS* mtcp) {
    UINT32 y;
    
    if (--mtcp->left < 0)
        return mtcokus_reload(mtcp);
    
    y = *mtcp->next++;
    y ^= (y >> 11);
    y ^= (y <<  7) & 0x9D2C5680U;
    y ^= (y << 15) & 0xEFC60000U;
    return (y ^ (y >> 18));
}
#endif

#ifdef MTCOKUS_DESTROY
void mtcokus_destroy(MTCOKUS* mtcp) {
    free(mtcp);
}
#endif
