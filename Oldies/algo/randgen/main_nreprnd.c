#include <time.h>
#include <stdio.h>
#include "nreprnd.h"

#define UPLIM   20
#define LOLIM   1
#define SIZE    (UPLIM-LOLIM+1)


int main(void) {
    int i;
    time_t tt;
    MTCOKUS mtc;
    UINT32 list[SIZE];
    
    mtcokus_screate(&mtc);
    mtcokus_seed(&mtc, (unsigned)time(&tt));
    
    nreprnd_makelist(&mtc, list, LOLIM, UPLIM);
    
    for (i=0; i<SIZE; ++i) {
        printf("%u\n", list[i]);
    }
    
    return 0;
}
