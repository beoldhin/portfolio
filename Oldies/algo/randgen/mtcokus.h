#ifndef _MTCOKUS_H
#define _MTCOKUS_H

#ifndef TEST_COMPILATION
#include "/home/olli/src/incs/inc_current.h"
#endif

#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"

typedef struct mtcokus {
    int left;
    UINT32* next;
    UINT32 state[624+1];
} MTCOKUS;

#ifdef TEST_COMPILATION
  #define MTCOKUS_SCREATE
  #define MTCOKUS_HCREATE
  #define MTCOKUS_SEED
  #define MTCOKUS_RANDOM
  #define MTCOKUS_DESTROY
#endif

#ifdef MTCOKUS_SCREATE
void mtcokus_screate(MTCOKUS*);
#endif
#ifdef MTCOKUS_HCREATE
MTCOKUS* mtcokus_hcreate(void);
#endif
#ifdef MTCOKUS_SEED
void mtcokus_seed(MTCOKUS*, UINT32);
#endif
#ifdef MTCOKUS_RANDOM
UINT32 mtcokus_random(MTCOKUS*);
#endif
#ifdef MTCOKUS_DESTROY
void mtcokus_destroy(MTCOKUS*);
#endif

#endif  /* _MTCOKUS_H */
