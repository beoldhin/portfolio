#include <time.h>
#include <stdio.h>

#ifndef CONTROL_TEST
#include "mtcokus.h"
int main(int argc, char* argv[]) {
    int i;
    int num;
    time_t tt;
    MTCOKUS mtc;
    MTCOKUS* mtcp;
    mtcp = &mtc;
    mtcokus_screate(mtcp);
    num = atoi(argv[1]);
    mtcokus_seed(mtcp, (unsigned)time(&tt));
    for (i=0; i<num; ++i)
        printf("%c", mtcokus_random(mtcp)%256);
    return 0;
}
#else
#include <stdlib.h>
int main(int argc, char* argv[]) {
    int i;
    int num;
    time_t tt;
    num = atoi(argv[1]);
    srand((unsigned)time(&tt));
    for (i=0; i<num; ++i)
        printf("%c", rand()%256);
    return 0;
}
#endif
