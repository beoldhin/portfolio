#include "nreprnd.h"


int nreprnd_makelist(MTCOKUS* mtcp, UINT32* list, UINT32 lolim, UINT32 uplim) {
    UINT32 i;
    UINT32 j;
    UINT32 num;
    UINT32 count;
    UINT32 total;
    if (lolim >= uplim)
        return 0;
    total = uplim - lolim + 1;
    memset(list, 0xFF, total*sizeof(UINT32));
    for (i=0; i<total; ++i) {
        num = mtcokus_random(mtcp) % (total-i);
        for (j=0,count=0;;++j) {
            if (list[j] == 0xFFFFFFFF) {
                if (count == num) {
                    list[j] = i + lolim;
                    break;
                }
                ++count;
            }
        }
    }
    return 1;
}
