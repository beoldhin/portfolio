/* read rfc1521 for more - Olli */
#include <ctype.h>
#include "base64.h"

#define UNUSED 127


#ifdef BASE64_INIT_ENCODE
void base64_init_encode(BASE64* b64p) {
    UINT i;
    char ch;
    char* enctable;
    enctable = b64p->enctable;
    for (i=0,ch='A'; i<26; ++i,++ch)
        enctable[i] = ch;
    for (ch='a'; i<52; ++i,++ch)
        enctable[i] = ch;
    for (ch='0'; i<62; ++i,++ch)
        enctable[i] = ch;
    enctable[i] = '+';
    enctable[i+1] = '/';
}
#endif

#ifdef BASE64_INIT_DECODE
void base64_init_decode(BASE64* b64p) {
    UINT ch;
    UINT8 i;
    UINT8* dectable;
    dectable = b64p->dectable;
    memset(dectable, UNUSED, 256);
    for (i=0,ch='A'; i<26; ++i,++ch)
        dectable[ch] = i;
    for (ch='a'; i<52; ++i,++ch)
        dectable[ch] = i;
    for (ch='0'; i<62; ++i,++ch)
        dectable[ch] = i;
    dectable['+'] = i;
    dectable['/'] = i + 1;
}
#endif

/* 3 8-bit bytes -> 4 6-bit bytes       */
/* XXXXXXXX | XXXXXXXX | XXXXXXXX = in  */
/* 11111122   22223333   33444444 = out */
#ifdef BASE64_ENCODE
void base64_encode(BASE64* b64p, UINT8* in, UINT inl, char** outp,
UINT* outlp) {
    UINT a;
    UINT b;
    UINT outl;
    char* tmp;
    char* out;
    char* enctable;
    UINT8* wend;
    a = inl / 3;
    b = inl % 3;
    outl = a * 4;
    if (b != 0)
        outl += 4;
    enctable = b64p->enctable;
    out = tmp = (char*)malloc(outl*sizeof(char));
    for (wend=in+(a*3); in<wend; in+=3,out+=4) {
        out[0] = enctable[in[0]>>2];
        out[1] = enctable[((in[0]<<4)+(in[1]>>4))&0x3F];
        out[2] = enctable[((in[1]<<2)+(in[2]>>6))&0x3F];
        out[3] = enctable[in[2]&0x3F];
    }
    if (b == 2) {
        out[0] = enctable[in[0]>>2];
        out[1] = enctable[((in[0]<<4)+(in[1]>>4))&0x3F];
        out[2] = enctable[(in[1]<<2)&0x3F];
        out[3] = '=';
    } else if (b == 1) {
        out[0] = enctable[in[0]>>2];
        out[1] = enctable[(in[0]<<4)&0x3F];
        out[2] = '=';
        out[3] = '=';
    }
    *outp = tmp;
    *outlp = outl;
}
#endif

/* 4 6-bit bytes -> 3 8-bit bytes    */
/* XXXXXX | XXXXXX | XXXXXX | XXXXXX */
/* 111111   112222   222233   333333 */
#ifdef BASE64_DECODE
BOOL base64_decode(BASE64* b64p, char* in, UINT inl, UINT8** outp,
UINT* outlp) {
    UINT b;
    UINT outl;
    char* wend;
    UINT8 inb1;
    UINT8 inb2;
    UINT8 inb3;
    UINT8 inb4;
    UINT8* tmp;
    UINT8* out;
    UINT8* dectable;
    if (inl % 4)
        return FALSE;
    b = 0;
    wend = in + inl;
    if (*(wend-1) == '=') {
        ++b;
        if (*(wend-2) == '=') {
            ++b;
            if (*(wend-3) == '=')
                return FALSE;
        }
    }
    if (b)
        wend -= 4;
    outl = ((wend-in)/4) * 3;
    if (b == 1)
        outl += 2;
    else if (b == 2)
        ++outl;
    dectable = b64p->dectable;
    out = tmp = (UINT8*)malloc(outl*sizeof(UINT8));
    for (; in<wend; in+=4,out+=3) {
        inb1 = dectable[(UINT)in[0]];
        inb2 = dectable[(UINT)in[1]];
        inb3 = dectable[(UINT)in[2]];
        inb4 = dectable[(UINT)in[3]];
        if (inb1==UNUSED || inb2==UNUSED || inb3==UNUSED || inb4==UNUSED)
            return FALSE;
        out[0] = (inb1<<2) + (inb2>>4);
        out[1] = (inb2<<4) + (inb3>>2);
        out[2] = (inb3<<6) + inb4;
    }
    if (b == 1) {
        inb1 = dectable[(UINT)in[0]];
        inb2 = dectable[(UINT)in[1]];
        inb3 = dectable[(UINT)in[2]];
        if (inb1==UNUSED || inb2==UNUSED || inb3==UNUSED)
            return FALSE;
        out[0] = (inb1<<2) + (inb2>>4);
        out[1] = (inb2<<4) + (inb3>>2);
    } else if (b == 2) {
        inb1 = dectable[(UINT)in[0]];
        inb2 = dectable[(UINT)in[1]];
        if (inb1==UNUSED || inb2==UNUSED)
            return FALSE;
        out[0] = (inb1<<2) + (inb2>>4);
    }
    *outp = tmp;
    *outlp = outl;
    return TRUE;
}
#endif
