#include <stdio.h>
#include "base64.h"

int main(void) {
    UINT i;
    char* out;
    UINT8* in;
    UINT inl;
    UINT outl;
    BASE64 b64;
    BASE64* b64p;
    char mystr[] = "abcdefg";
    
    b64p = &b64;
    base64_init_encode(b64p);
    base64_init_decode(b64p);
    
    printf("%s|\n", mystr);
    
    base64_encode(b64p, (UINT8*)mystr, strlen(mystr), &out, &outl);
    
    for (i=0; i<outl; ++i)
        putchar(out[i]);
    putchar('|');
    puts("");
    
    base64_decode(b64p, out, outl, &in, &inl);
    
    for (i=0; i<inl; ++i)
        putchar(in[i]);
    putchar('|');
    puts("");
    
    free(out);
    free(in);
    
    return 0;
}
