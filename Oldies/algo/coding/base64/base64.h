#ifndef _BASE64_H
#define _BASE64_H

#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

typedef struct base64 {
    char enctable[64];
    UINT8 dectable[256];
} BASE64;

#ifdef TEST_COMPILATION
  #define BASE64_INIT_ENCODE
  #define BASE64_INIT_DECODE
  #define BASE64_ENCODE
  #define BASE64_DECODE
#endif

#ifdef BASE64_INIT_ENCODE
void base64_init_encode(BASE64*);
#endif
#ifdef BASE64_INIT_DECODE
void base64_init_decode(BASE64*);
#endif
#ifdef BASE64_ENCODE
void base64_encode(BASE64*, UINT8*, UINT, char**, UINT*);
#endif
#ifdef BASE64_DECODE
BOOL base64_decode(BASE64*, char*, UINT, UINT8**, UINT*);
#endif

#endif  /* _BASE64_H */
