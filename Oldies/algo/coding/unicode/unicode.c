#include <stdio.h>
#include "unicode.h"

#ifdef TEST_COMPILATION
  #define UNICODE_UTF8Z_UCS2_LENGTH
  #define UNICODE_UTF8Z_UCS4_LENGTH
  #define UNICODE_UCS2_UTF8Z_LENGTH
  #define UNICODE_UCS4_UTF8Z_LENGTH
#endif

#ifdef UNICODE_UTF8Z_UCS2_LENGTH
BOOL unicode_utf8z_ucs2_length(UINT8*, UINT*);
#endif
#ifdef UNICODE_UTF8Z_UCS4_LENGTH
BOOL unicode_utf8z_ucs4_length(UINT8*, UINT*);
#endif
#ifdef UNICODE_UCS2_UTF8Z_LENGTH
void unicode_ucs2_utf8z_length(UINT16*, UINT*);
#endif
#ifdef UNICODE_UCS4_UTF8Z_LENGTH
void unicode_ucs4_utf8z_length(UINT32*, UINT*);
#endif

#define CHECK_MULTIBYTE_ZERO() {\
 mb = *(++src);\
 if (mb == '\0')\
     return FALSE;\
 if ((mb&0xC0) != 0x80)\
     return FALSE;\
}


#ifdef UNICODE_UTF8Z_TO_UCS2Z
BOOL unicode_utf8z_to_ucs2z(UINT8* src, UINT16** dstp, UINT* dstlp) {
    UINT len;
    UINT8 mb1;
    UINT8 mb2;
    UINT8 curb;
    UINT16* dst;
    UINT16* dstold;
    if (!unicode_utf8z_ucs2_length(src,&len))
        return FALSE;
    ++len;  /* for the null character... */
    if (!(dst=(UINT16*)malloc(len*sizeof(UINT16))))
        return FALSE;
    dstold = dst;
    while ((curb=*src)) {
        if (!(curb&0x80)) {
            /* 7-bit ASCII character, no multibytes */
            *dst = curb;
        } else if (curb&0xC0) {
            /* 1 byte will follow, 5 + 6 bits total */
            curb &= 0x1F;
            mb1 = *(++src) & 0x3F;
            *dst = (curb<<6) + mb1;
        } else {
            /* 2 bytes will follow, 4 + 6 + 6 bits total */
            curb &= 0x0F;
            mb1 = *(++src) & 0x3F;
            mb2 = *(++src) & 0x3F;
            *dst = (curb<<12) + (mb1<<6) + mb2;
        }
        ++src;
        ++dst;
    }
    *dst = '\0';
    *dstlp = len;
    *dstp = dstold;
    return TRUE;
}
#endif

#ifdef UNICODE_UTF8Z_UCS2_LENGTH
BOOL unicode_utf8z_ucs2_length(UINT8* src, UINT* lp) {
    UINT len;
    UINT8 mb;
    UINT8 curb;
    len = 0;
    while ((curb=*src)) {
        if (!(curb&0x80)) {
            /* 7-bit ASCII character, no multibytes */
        } else if (curb & 0xC0) {
            /* 1 byte must follow, 5 + 6 bits total */
            CHECK_MULTIBYTE_ZERO();
        } else if (curb & 0xE0) {
            /* 2 bytes must follow, 4 + 6 + 6 bits total */
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
        } else if (curb & 0xF0) {
            /* 3 bytes must follow, 3 + 6 + 6 + 6 bits total */
            return FALSE;  /* because UCS2 is max. 16 bits... */
        } else if (curb & 0xF8) {
            /* 4 bytes must follow, 2 + 6 + 6 + 6 + 6 bits total */
            return FALSE;  /* because UCS2 is max. 16 bits... */
        } else if (curb & 0xFC) {
            /* 5 bytes must follow, 1 + 6 + 6 + 6 + 6 + 6 bits total */
            return FALSE;  /* because UCS2 is max. 16 bits... */
        } else
            return FALSE;
        ++src;
        ++len;
    }
    *lp = len;
    return TRUE;
}
#endif

#ifdef UNICODE_UTF8Z_TO_UCS4Z
BOOL unicode_utf8z_to_ucs4z(UINT8* src, UINT32** dstp, UINT* dstlp) {
    UINT len;
    UINT8 mb1;
    UINT8 mb2;
    UINT8 mb3;
    UINT8 mb4;
    UINT8 mb5;
    UINT8 curb;
    UINT32* dst;
    UINT32* dstold;
    if (!unicode_utf8z_ucs4_length(src,&len))
        return FALSE;
    ++len;  /* for the null character... */
    if (!(dst=(UINT32*)malloc(len*sizeof(UINT32))))
        return FALSE;
    dstold = dst;
    while ((curb=*src)) {
        if (!(curb&0x80)) {
            /* 7-bit ASCII character, no multibytes */
            *dst = curb;
        } else if (curb&0xC0) {
            /* 1 byte follows, 5 + 6 bits total */
            curb &= 0x1F;
            mb1 = *(++src) & 0x3F;
            *dst = (curb<<6) + mb1;
        } else if (curb&0xE0) {
            /* 2 bytes follows, 4 + 6 + 6 bits total */
            curb &= 0x0F;
            mb1 = *(++src) & 0x3F;
            mb2 = *(++src) & 0x3F;
            *dst = (curb<<12) + (mb1<<6) + mb2;
        } else if (curb&0xF0) {
            /* 3 bytes will follow, 3 + 6 + 6 + 6 bits total */
            curb &= 0x07;
            mb1 = *(++src) & 0x3F;
            mb2 = *(++src) & 0x3F;
            mb3 = *(++src) & 0x3F;
            *dst = (curb<<18) + (mb1<<12) + (mb2<<6) + mb3;
        } else if (curb&0xF8) {
            /* 4 bytes will follow, 2 + 6 + 6 + 6 + 6 bits total */
            curb &= 0x03;
            mb1 = *(++src) & 0x3F;
            mb2 = *(++src) & 0x3F;
            mb3 = *(++src) & 0x3F;
            mb4 = *(++src) & 0x3F;
            *dst = (curb<<24) + (mb1<<18) + (mb2<<12) + (mb3<<6) + mb4;
        } else {
            /* 5 bytes will follow, 1 + 6 + 6 + 6 + 6 + 6 bits total */
            curb &= 0x01;
            mb1 = *(++src) & 0x3F;
            mb2 = *(++src) & 0x3F;
            mb3 = *(++src) & 0x3F;
            mb4 = *(++src) & 0x3F;
            mb5 = *(++src) & 0x3F;
            *dst = (curb<<30) + (mb1<<24) + (mb2<<18) + (mb3<<12) +
            (mb4<<6) + mb5;
        }
        ++src;
        ++dst;
    }
    *dst = '\0';
    *dstlp = len;
    *dstp = dstold;
    return TRUE;
}
#endif

#ifdef UNICODE_UTF8Z_UCS4_LENGTH
BOOL unicode_utf8z_ucs4_length(UINT8* src, UINT* lp) {
    UINT len;
    UINT8 mb;
    UINT8 curb;
    len = 0;
    while ((curb=*src)) {
        if (!(curb&0x80)) {
            /* 7-bit ASCII character, no multibytes */
        } else if (curb & 0xC0) {
            /* 1 byte must follow, 5 + 6 bits total */
            CHECK_MULTIBYTE_ZERO();
        } else if (curb & 0xE0) {
            /* 2 bytes must follow, 4 + 6 + 6 bits total */
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
        } else if (curb & 0xF0) {
            /* 3 bytes must follow, 3 + 6 + 6 + 6 bits total */
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
        } else if (curb & 0xF8) {
            /* 4 bytes must follow, 2 + 6 + 6 + 6 + 6 bits total */
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
        } else if (curb & 0xFC) {
            /* 5 bytes must follow, 1 + 6 + 6 + 6 + 6 + 6 bits total */
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
            CHECK_MULTIBYTE_ZERO();
        } else
            return FALSE;
        ++src;
        ++len;
    }
    *lp = len;
    return TRUE;
}
#endif

#ifdef UNICODE_UCS2Z_TO_UTF8Z
BOOL unicode_ucs2z_to_utf8z(UINT16* src, UINT8** dstp, UINT* dstlp) {
    UINT len;
    UINT8* dst;
    UINT8* dstold;
    UINT16 curw;
    unicode_ucs2_utf8z_length(src, &len);
    ++len;  /* for the null character... */
    if (!(dst=(UINT8*)malloc(len*sizeof(UINT8))))
        return FALSE;
    dstold = dst;
    while ((curw=*src)) {
        if (curw <= 0x007FLU) {
            /* 7-bit ASCII character, no multibytes */
            *dst++ = curw;
        } else if (curw <= 0x07FFLU) {
            /* 1 byte will follow, 5 + 6 bits total */
            *dst++ = 0xC0 + (curw>>6);
            *dst++ = 0x80 + (curw&0x3F);
        } else {
            /* 2 bytes will follow, 4 + 6 + 6 bits total */
            *dst++ = 0xE0 + (curw>>12);
            *dst++ = 0x80 + ((curw>>6)&0x3F);
            *dst++ = 0x80 + (curw&0x3F);
        }
        ++src;
    }
    *dst = '\0';
    *dstlp = len;
    *dstp = dstold;
    return TRUE;
}
#endif

#ifdef UNICODE_UCS2_UTF8Z_LENGTH
void unicode_ucs2_utf8z_length(UINT16* src, UINT* lp) {
    UINT len;
    UINT16 curw;
    len = 0;
    while ((curw=*src)) {
        if (curw <= 0x007FLU) {
            ++len;
        } else if (curw <= 0x07FFLU) {
            len += 2;
        } else {
            len += 3;
        }
        ++src;
    }
    *lp = len;
}
#endif

#ifdef UNICODE_UCS4Z_TO_UTF8Z
BOOL unicode_ucs4z_to_utf8z(UINT32* src, UINT8** dstp, UINT* dstlp) {
    UINT len;
    UINT8* dst;
    UINT8* dstold;
    UINT32 curw;
    unicode_ucs4_utf8z_length(src, &len);
    ++len;  /* for the null character... */
    if (!(dst=(UINT8*)malloc(len*sizeof(UINT8))))
        return FALSE;
    dstold = dst;
    while ((curw=*src)) {
        if (curw <= 0x0000007FLU) {
            /* 7-bit ASCII character, no multibytes */
            *dst++ = curw;
        } else if (curw <= 0x000007FFLU) {
            /* 1 byte will follow, 5 + 6 bits total */
            *dst++ = 0xC0 + (curw>>6);
            *dst++ = 0x80 + (curw&0x3F);
        } else if (curw <= 0x0000FFFFLU) {
            /* 2 bytes will follow, 4 + 6 + 6 bits total */
            *dst++ = 0xE0 + (curw>>12);
            *dst++ = 0x80 + ((curw>>6)&0x3F);
            *dst++ = 0x80 + (curw&0x3F);
        } else if (curw <= 0x001FFFFFLU) {
            /* 3 bytes will follow, 3 + 6 + 6 + 6 bits total */
            *dst++ = 0xF0 + (curw>>18);
            *dst++ = 0x80 + ((curw>>12)&0x3F);
            *dst++ = 0x80 + ((curw>>6)&0x3F);
            *dst++ = 0x80 + (curw&0x3F);
        } else if (curw <= 0x003FFFFFLU) {
            /* 4 bytes will follow, 2 + 6 + 6 + 6 + 6 bits total */
            *dst++ = 0xF8 + (curw>>24);
            *dst++ = 0x80 + ((curw>>18)&0x3F);
            *dst++ = 0x80 + ((curw>>12)&0x3F);
            *dst++ = 0x80 + ((curw>>6)&0x3F);
            *dst++ = 0x80 + (curw&0x3F);
        } else {
            /* 5 bytes will follow, 1 + 6 + 6 + 6 + 6 + 6 bits total */
            *dst++ = 0xFC + (curw>>30);
            *dst++ = 0x80 + ((curw>>24)&0x3F);
            *dst++ = 0x80 + ((curw>>18)&0x3F);
            *dst++ = 0x80 + ((curw>>12)&0x3F);
            *dst++ = 0x80 + ((curw>>6)&0x3F);
            *dst++ = 0x80 + (curw&0x3F);
        }
        ++src;
    }
    *dst = '\0';
    *dstlp = len;
    *dstp = dstold;
    return TRUE;
}
#endif

#ifdef UNICODE_UCS4_UTF8Z_LENGTH
void unicode_ucs4_utf8z_length(UINT32* src, UINT* lp) {
    UINT len;
    UINT32 curw;
    len = 0;
    while ((curw=*src)) {
        if (curw <= 0x0000007FLU) {
            ++len;
        } else if (curw <= 0x000007FFLU) {
            len += 2;
        } else if (curw <= 0x0000FFFFLU) {
            len += 3;
        } else if (curw <= 0x001FFFFFLU) {
            len += 4;
        } else if (curw <= 0x003FFFFFLU) {
            len += 5;
        } else {
            len += 6;
        }
        ++src;
    }
    *lp = len;
}
#endif
