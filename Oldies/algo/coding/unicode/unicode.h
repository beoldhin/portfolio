#ifndef _UNICODE_H
#define _UNICODE_H

#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/incs/inc_current.h"

#ifdef TEST_COMPILATION
  #define UNICODE_UTF8Z_TO_UCS2Z
  #define UNICODE_UTF8Z_TO_UCS4Z
  #define UNICODE_UCS2Z_TO_UTF8Z
  #define UNICODE_UCS4Z_TO_UTF8Z
#endif

#ifdef UNICODE_UTF8Z_TO_UCS2Z
BOOL unicode_utf8z_to_ucs2z(UINT8*, UINT16**, UINT*);
#endif
#ifdef UNICODE_UTF8Z_TO_UCS4Z
BOOL unicode_utf8z_to_ucs4z(UINT8*, UINT32**, UINT*);
#endif
#ifdef UNICODE_UCS2Z_TO_UTF8Z
BOOL unicode_ucs2z_to_utf8z(UINT16*, UINT8**, UINT*);
#endif
#ifdef UNICODE_UCS4Z_TO_UTF8Z
BOOL unicode_ucs4z_to_utf8z(UINT32*, UINT8**, UINT*);
#endif

#endif  /* _UNICODE_H */
