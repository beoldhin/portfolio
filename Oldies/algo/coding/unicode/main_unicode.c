#include <stdio.h>
#include "unicode.h"


int main(void) {
    UINT newstrl;
    UINT8* utf8new;
    UINT16* ucs2new;
    UINT8 utf8str[3];
    UINT16 ucs2str[3];
    
    utf8str[0] = 'A';
    utf8str[1] = 'B';
    utf8str[2] = '\0';
    ucs2str[0] = 0x1234;
    ucs2str[1] = 0x4321;
    ucs2str[2] = '\0';
    unicode_utf8z_to_ucs2z(utf8str, &ucs2new, &newstrl);
    unicode_ucs2z_to_utf8z(ucs2str, &utf8new, &newstrl);
    
    free(ucs2new);
    free(utf8new);
    return 0;
}
