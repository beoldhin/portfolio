#ifndef _ROT13_H
#define _ROT13_H

#ifndef ROT13_TABLE
void rot13_recode(char*);
#endif
#ifdef ROT13_TABLE
void rot13_recode_fast(char*);
#endif

#endif  /* _ROT13_H */
