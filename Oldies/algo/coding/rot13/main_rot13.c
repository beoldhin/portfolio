#include <stdio.h>
#include "rot13.h"

int main(int argc, char* argv[]) {
    
    rot13_recode_fast(argv[1]);
    
    printf("%s\n", argv[1]);
    
    return 0;
}
