#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define XSIZE 20
#define YSIZE 20

#define MAXWORDS 8
#define LONGESTWORD 12

typedef enum {
    NORTH,
    NORTHEAST,
    EAST,
    SOUTHEAST,
    SOUTH,
    SOUTHWEST,
    WEST,
    NORTHWEST,
    NDIRS  /* keep this as the last item */
} DIRS;

typedef struct pos {
    int x;
    int y;
} POS;

typedef struct dirc {
    int num;
    POS* pos;
} DIRC;


const char words[MAXWORDS][LONGESTWORD+1] = {
    "Automobiili",
    "Helikopteri",
    "Tutka",
    "Kerosiini",
    "Hilavitkutin",
    "Motoristi",
    "Turisti",
    "Bensiini"
};


int main(void)
{
    int i, j, strl;
    char* matrix;
    DIRC** dirs;
    /* create word direction limits (8 directions) for each word length */
    matrix = (char*)malloc(XSIZE*YSIZE);
    dirs = (DIRC**)malloc(LONGESTWORD*sizeof(DIRC*));
    memset(dirs, 0x00, LONGESTWORD*sizeof(DIRC*));
    for (i=0; i<MAXWORDS; i++) {
        strl = strlen(words[i]) - 1;
        if (!dirs[strl]) {
            dirs[strl] = (DIRC*)malloc(NDIRS*sizeof(DIRC));
            for (j=0; j<NDIRS; j++) {
                dirs[strl][j].num = 0;
                dirs[strl][j].pos = (POS*)malloc(XSIZE*YSIZE*sizeof(POS));
                
            }
        }
    }
    /* create direction counters */
    
    
    
    
    
    /* deallocate */
    for (i=0; i<LONGESTWORD; i++) {
        if (dirs[i]) {
            for (j=0; j<NDIRS; j++) {
                free(dirs[i][j].pos);
            }
            free(dirs[i]);
        }
    }
    free(dirs);
    free(matrix);
    return 0;
}
