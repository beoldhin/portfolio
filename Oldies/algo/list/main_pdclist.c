#include <stdio.h>
#include "pdclist.h"

BOOL user_compare(void*);


int main(void) {
    int mydata;
    int* mydatap;
    void* memory;
    SCPOOL scpool;
    SCPOOL* scpoolp;
    PDCLIST list;
    PDCLIST* listp;
    PDCITER* iterp;
    PDCITER* iterbase;
    
    listp = &list;
    scpoolp = &scpool;
    mydatap = &mydata;
    scpool_screate(scpoolp, 10000, sizeof(PDCNODE)+sizeof(int));
    pdclist_screate(listp, scpoolp);
    iterbase = (PDCITER*)listp;
    
    mydata = 1;
    pdclist_push_back(listp, mydatap);
    mydata = 2;
    pdclist_push_back(listp, mydatap);
    mydata = 3;
    pdclist_push_back(listp, mydatap);
    
    iterp = iterbase->nextp;  /* first (iterbase is sentinel) */
    pdclist_move_iter_to_tail(listp, iterp);
    
    pdclist_move_head_to_tail(listp);
    pdclist_move_tail_to_head(listp);
    
    memory = pdclist_extract_list_alloc(listp);
    pdclist_clear(listp);
    pdclist_extract_array_screate(listp, scpoolp, memory, 3);
    free(memory);
    
    if (pdclist_iterate_first(listp,&iterp)) {
        do {
            printf("%d\n", *(int*)(iterp+1));
        } while (pdclist_iterate_next(listp,&iterp));
    }
    
    pdclist_clear(listp);
    
    return 0;
}

BOOL user_compare(void* satp) {
    if (*(int*)satp == 2)
        return TRUE;
    return FALSE;
}
