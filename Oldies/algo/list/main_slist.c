#include <stdio.h>
#include "slist.h"


int main(void) {
    int mydata;
    SITER* iter;
    SLIST* tail;
    SLIST* list = NULL;
    
    mydata = 100;
    tail = slist_push_front(&list, &mydata, sizeof(mydata));
    mydata = 200;
    tail = slist_push_front(&list, &mydata, sizeof(mydata));
    mydata = 300;
    tail = slist_push_front(&list, &mydata, sizeof(mydata));
    
    if (slist_iterate_first(&list,&iter)) {
        do {
            printf("%d\n", *(int*)(iter+1));
        } while (slist_iterate_next(&list,&iter));
    }
    
    slist_destroy(&list);
    
    return 0;
}
