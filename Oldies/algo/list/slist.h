#ifndef _SLIST_H
#define _SLIST_H

#include "/home/olli/src/algo/defines/nlimits.h"

#ifndef TEST_COMPILATION
#include "/home/olli/src/incs/inc_current.h"
#endif

typedef struct snode {
    struct snode* nextp;
} SNODE,SITER,SLIST;

#ifdef TEST_COMPILATION
  #define SLIST_PUSH_BACK_F
  #define SLIST_PUSH_BACK_S
  #define SLIST_PUSH_FRONT
  #define SLIST_ITERATE_FIRST
  #define SLIST_ITERATE_NEXT
  #define SLIST_DESTROY
  #define SLIST_DESTROY_ADVANCED
#endif

#ifdef SLIST_PUSH_BACK_F
SNODE* slist_push_back_f(SLIST**, SNODE**, void*, UINT);  /* faster */
#endif
#ifdef SLIST_PUSH_BACK_S
SNODE* slist_push_back_s(SLIST**, void*, UINT);           /* slower */
#endif
#ifdef SLIST_PUSH_FRONT
SNODE* slist_push_front(SLIST**, void*, UINT);
#endif
#ifdef SLIST_ITERATE_FIRST
BOOL slist_iterate_first(SLIST**, SITER**);
#endif
#ifdef SLIST_ITERATE_NEXT
BOOL slist_iterate_next(SLIST**, SITER**);
#endif
#ifdef SLIST_DESTROY
void slist_destroy(SLIST**);
#endif
#ifdef SLIST_DESTROY_ADVANCED
void slist_destroy_advanced(SLIST**, void (*)(void*));
#endif

#endif  /* _SLIST_H */
