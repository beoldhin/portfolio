#ifndef _PDDCLIST_H
#define _PDDCLIST_H

#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/algo/pool/scbpool.h"

typedef struct pddcnode {
    struct pddcnode* prevp;
    struct pddcnode* nextp;
} PDDCNODE,PDDCITER;

typedef struct pdclist {
    PDDCNODE list;  /* keep this the very first */
    UINT satsize;
    SCBPOOL* scbpoolp;
} PDDCLIST;

#ifdef TEST_COMPILATION
  #define PDDCLIST_SCREATE
  #define PDDCLIST_HCREATE
  #define PDDCLIST_PUSH_BACK
  #define PDDCLIST_PUSH_FRONT
  #define PDDCLIST_POP_BACK
  #define PDDCLIST_POP_FRONT
  #define PDDCLIST_SWAP_HEAD_AND_TAIL
  #define PDDCLIST_SWAP_HEAD_AND_ITER
  #define PDDCLIST_SWAP_ITER_AND_TAIL
  #define PDDCLIST_SWAP_ITER_AND_ITER
  #define PDDCLIST_MOVE_HEAD_TO_TAIL
  #define PDDCLIST_MOVE_TAIL_TO_HEAD
  #define PDDCLIST_MOVE_ITER_TO_HEAD
  #define PDDCLIST_MOVE_ITER_TO_TAIL
  #define PDDCLIST_INSERT_BEFORE_ITER
  #define PDDCLIST_INSERT_AFTER_ITER
  #define PDDCLIST_ERASE_ONE_AT_ITER
  #define PDDCLIST_EXTRACT_ARRAY_SCREATE
  #define PDDCLIST_EXTRACT_ARRAY_HCREATE
  #define PDDCLIST_EXTRACT_LIST
  #define PDDCLIST_EXTRACT_LIST_ALLOC
  #define PDDCLIST_ITERATE_NEXT
  #define PDDCLIST_ITERATE_PREV
  #define PDDCLIST_ITERATE_FIRST
  #define PDDCLIST_ITERATE_LAST
  #define PDDCLIST_ITERATE_NTH_NEXT
  #define PDDCLIST_ITERATE_NTH_PREV
  #define PDDCLIST_COUNT_NODES
  #define PDDCLIST_FIND_FIRST
  #define PDDCLIST_FIND_LAST
  #define PDDCLIST_CLEAR
  #define PDDCLIST_CLEAR_ADVANCED
  #define PDDCLIST_DESTROY
  #define PDDCLIST_DESTROY_ADVANCED
#endif

#ifdef PDDCLIST_SCREATE
void pddclist_screate(PDDCLIST*, SCBPOOL*);
#endif
#ifdef PDDCLIST_HCREATE
PDDCLIST* pddclist_hcreate(SCBPOOL*);
#endif
#ifdef PDDCLIST_PUSH_BACK
BOOL pddclist_push_back(PDDCLIST*, void*);
#endif
#ifdef PDDCLIST_PUSH_FRONT
BOOL pddclist_push_front(PDDCLIST*, void*);
#endif
#ifdef PDDCLIST_POP_BACK
BOOL pddclist_pop_back(PDDCLIST*);
#endif
#ifdef PDDCLIST_POP_FRONT
BOOL pddclist_pop_front(PDDCLIST*);
#endif
#ifdef PDDCLIST_SWAP_HEAD_AND_TAIL
BOOL pddclist_swap_head_and_tail(PDDCLIST*);
#endif
#ifdef PDDCLIST_SWAP_HEAD_AND_ITER
BOOL pddclist_swap_head_and_iter(PDDCLIST*, PDDCITER*);
#endif
#ifdef PDDCLIST_SWAP_ITER_AND_TAIL
BOOL pddclist_swap_iter_and_tail(PDDCLIST*, PDDCITER*);
#endif
#ifdef PDDCLIST_SWAP_ITER_AND_ITER
BOOL pddclist_swap_iter_and_iter(PDDCLIST*, PDDCITER*, PDDCITER*);
#endif
#ifdef PDDCLIST_MOVE_HEAD_TO_TAIL
BOOL pddclist_move_head_to_tail(PDDCLIST*);
#endif
#ifdef PDDCLIST_MOVE_TAIL_TO_HEAD
BOOL pddclist_move_tail_to_head(PDDCLIST*);
#endif
#ifdef PDDCLIST_MOVE_ITER_TO_HEAD
BOOL pddclist_move_iter_to_head(PDDCLIST*, PDDCITER*);
#endif
#ifdef PDDCLIST_MOVE_ITER_TO_TAIL
BOOL pddclist_move_iter_to_tail(PDDCLIST*, PDDCITER*);
#endif
#ifdef PDDCLIST_INSERT_BEFORE_ITER
BOOL pddclist_insert_before_iter(PDDCLIST*, PDDCITER*, void*);
#endif
#ifdef PDDCLIST_INSERT_AFTER_ITER
BOOL pddclist_insert_after_iter(PDDCLIST*, PDDCITER*, void*);
#endif
#ifdef PDDCLIST_ERASE_ONE_AT_ITER
BOOL pddclist_erase_one_at_iter(PDDCLIST*, PDDCITER*);
#endif
#ifdef PDDCLIST_EXTRACT_ARRAY_SCREATE
BOOL pddclist_extract_array_screate(PDDCLIST*, SCBPOOL*, void*, UINT);
#endif
#ifdef PDDCLIST_EXTRACT_ARRAY_HCREATE
BOOL pddclist_extract_array_hcreate(PDDCLIST**, SCBPOOL*, void*, UINT);
#endif
#ifdef PDDCLIST_EXTRACT_LIST
void pddclist_extract_list(PDDCLIST*, void*);
#endif
#ifdef PDDCLIST_EXTRACT_LIST_ALLOC
void* pddclist_extract_list_alloc(PDDCLIST*);
#endif
#ifdef PDDCLIST_ITERATE_NEXT
BOOL pddclist_iterate_next(PDDCLIST*, PDDCITER**);
#endif
#ifdef PDDCLIST_ITERATE_PREV
BOOL pddclist_iterate_prev(PDDCLIST*, PDDCITER**);
#endif
#ifdef PDDCLIST_ITERATE_FIRST
BOOL pddclist_iterate_first(PDDCLIST*, PDDCITER**);
#endif
#ifdef PDDCLIST_ITERATE_LAST
BOOL pddclist_iterate_last(PDDCLIST*, PDDCITER**);
#endif
#ifdef PDDCLIST_ITERATE_NTH_NEXT
BOOL pddclist_iterate_nth_next(PDDCLIST*, PDDCITER**, UINT);
#endif
#ifdef PDDCLIST_ITERATE_NTH_PREV
BOOL pddclist_iterate_nth_prev(PDDCLIST*, PDDCITER**, UINT);
#endif
#ifdef PDDCLIST_COUNT_NODES
UINT pddclist_count_nodes(PDDCLIST*);
#endif
#ifdef PDDCLIST_FIND_FIRST
BOOL pddclist_find_first(PDDCLIST*, PDDCITER**, BOOL (*)(void*,BOOL*));
#endif
#ifdef PDDCLIST_FIND_LAST
BOOL pddclist_find_last(PDDCLIST*, PDDCITER**, BOOL (*)(void*,BOOL*));
#endif
#ifdef PDDCLIST_CLEAR
void pddclist_clear(PDDCLIST*);
#endif
#ifdef PDDCLIST_CLEAR_ADVANCED
void pddclist_clear_advanced(PDDCLIST*, void (*)(void*));
#endif
#ifdef PDDCLIST_DESTROY
void pddclist_destroy(PDDCLIST*);
#endif
#ifdef PDDCLIST_DESTROY_ADVANCED
void pddclist_destroy_advanced(PDDCLIST*, void (*)(void*));
#endif

#endif  /* _PDDCLIST_H */
