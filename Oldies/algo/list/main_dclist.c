#include <stdio.h>
#include "dclist.h"

BOOL user_compare(void*);


int main(void) {
    int mydata;
    int* mydatap;
    /*void* memory;*/
    DCLIST list;
    DCLIST* listp;
    DCLIST* iterp;
    
    listp = &list;
    mydatap = &mydata;
    dclist_screate(listp);
    
    mydata = 1;
    dclist_push_back(listp, mydatap, sizeof(int));
    mydata = 2;
    dclist_push_back(listp, mydatap, sizeof(int));
    mydata = 3;
    dclist_push_back(listp, mydatap, sizeof(int));
    mydata = 4;
    dclist_push_back(listp, mydatap, sizeof(int));
    
    dclist_reverse(listp);
    
    if (dclist_iterate_first(listp,&iterp)) {
        do {
            printf("%d\n", *(int*)(iterp+1));
        } while (dclist_iterate_next(listp,&iterp));
    }
    
    /*
    iterp = list.nextp;
    dclist_move_iter_to_tail(listp, iterp);
    
    dclist_move_head_to_tail(listp);
    dclist_move_tail_to_head(listp);
    
    memory = dclist_extract_list_alloc(listp, sizeof(int));
    dclist_clear(listp);
    dclist_extract_array_screate(listp, memory, 3, sizeof(int));
    free(memory);
    
    if (dclist_iterate_first(listp,&iterp)) {
        do {
            printf("%d\n", *(int*)(iterp+1));
        } while (dclist_iterate_next(listp,&iterp));
    }
    */
    
    dclist_clear(listp);
    
    return 0;
}

BOOL user_compare(void* satp) {
    if (*(int*)satp == 2)
        return TRUE;
    return FALSE;
}
