/* Circular doubly linked list implementation   */
/* (as explained in Introduction to Algorithms) */

/*
 * The pool used by this file supports all modes of allocation and
 * deallocation (is somewhat slower and uses more extra memory).
 * 
 * Due to this the following functions can be used freely:
 * - pddclist_pop_back
 * - pddclist_pop_front
 * - pddclist_erase_one_at_iter
 */

#include <string.h>
#include "pddclist.h"


/***********************************************************************
 * Function      : pddclist_screate
 * Time complex. : O(1) without heap allocation
 * Description   : Initializes a circular doubly linked list without
 *                 allocating space for the list descriptor object.
 *                 Only non-nested lists are supported (address space
 *                 change not possible).
 *                 The memory pool in scbpoolp must be initialized
 *                 before calling this function.
 *                 Since the size of the satellite data is calculated
 *                 directly from the pool, the nodes in the pool must be
 *                 exactly sizeof(PDDCNODE)+satsize in size.
 * Parameters    : listp:    pointer to the list descriptor object
 *                           (object usually in stack with this
 *                           function)
 *                 scbpoolp: pointer to the static circular pool
 * Return values : None
 **********************************************************************/
#ifdef PDDCLIST_SCREATE
void pddclist_screate(PDDCLIST* listp, SCBPOOL* scbpoolp) {
    listp->list.prevp = (PDDCNODE*)listp;
    listp->list.nextp = (PDDCNODE*)listp;
    listp->satsize = scbpoolp->statsize - sizeof(PDDCNODE);
    listp->scbpoolp = scbpoolp;
}
#endif

/***********************************************************************
 * Function      : pddclist_hcreate
 * Time complex. : O(1) with heap allocation
 * Description   : Initializes a circular doubly linked list by
 *                 allocating heap space for the list descriptor object.
 *                 Unlike pddclist_screate, this function supports
 *                 nested lists (address space change is possible).
 *                 The memory pool in scbpoolp must be initialized
 *                 before calling this function.
 *                 Since the size of the satellite data is calculated
 *                 directly from the pool, the nodes in the pool must be
 *                 exactly sizeof(PDDCNODE)+satsize in size.
 * Parameters    : scbpoolp: pointer to the static circular pool
 * Return values : Non-NULL: pointer to the new list's descriptor object
 *                 NULL:     memory allocation failed, list not created
 **********************************************************************/
#ifdef PDDCLIST_HCREATE
PDDCLIST* pddclist_hcreate(SCBPOOL* scbpoolp) {
    PDDCLIST* new_list;
    new_list = (PDDCLIST*)malloc(sizeof(PDDCLIST));
    if (new_list) {
        new_list->list.prevp = (PDDCNODE*)new_list;
        new_list->list.nextp = (PDDCNODE*)new_list;
        new_list->satsize = scbpoolp->statsize - sizeof(PDDCNODE);
        new_list->scbpoolp = scbpoolp;
        return new_list;
    }
    return NULL;
}
#endif

/***********************************************************************
 * Function      : pddclist_push_back
 * Time complex. : O(1) with pool heap allocation
 * Description   : Creates a new circular doubly linked list node in
 *                 the list's internal static circular pool.
 *                 The node is inserted to the tail of the list, i.e.
 *                 one before the satellite node.
 * Parameters    : listp: pointer to the list descriptor object
 *                 satp:  pointer to the satellite data
 * Return values : FALSE: memory allocation failed, node not created
 *                 TRUE:  node successfully appended to the list
 **********************************************************************/
#ifdef PDDCLIST_PUSH_BACK
BOOL pddclist_push_back(PDDCLIST* listp, void* satp) {
    PDDCNODE* nilp;
    PDDCNODE* new_node;
    new_node = (PDDCNODE*)scbpool_alloc(listp->scbpoolp);
    if (new_node) {
        nilp = (PDDCNODE*)listp;
        new_node->prevp = nilp->prevp;
        new_node->nextp = nilp;
        nilp->prevp->nextp = new_node;
        nilp->prevp = new_node;
        memcpy(new_node+1, satp, listp->satsize);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_push_front
 * Time complex. : O(1) with pool heap allocation
 * Description   : Creates a new circular doubly linked list node in
 *                 the list's internal static circular pool.
 *                 The node is inserted to the head of the list, i.e.
 *                 one after the satellite node.
 * Parameters    : listp: pointer to the list descriptor object
 *                 satp:  pointer to the satellite data
 * Return values : FALSE: memory allocation failed, node not created
 *                 TRUE:  node successfully appended to the list
 **********************************************************************/
#ifdef PDDCLIST_PUSH_FRONT
BOOL pddclist_push_front(PDDCLIST* listp, void* satp) {
    PDDCNODE* nilp;
    PDDCNODE* new_node;
    new_node = (PDDCNODE*)scbpool_alloc(listp->scbpoolp);
    if (new_node) {
        nilp = (PDDCNODE*)listp;
        new_node->prevp = nilp;
        new_node->nextp = nilp->nextp;
        nilp->nextp->prevp = new_node;
        nilp->nextp = new_node;
        memcpy(new_node+1, satp, listp->satsize);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_pop_back
 * Time complex. : O(1) with pool heap deallocation
 * Description   : Removes the last node - one before the sentinel -
 *                 from a circular doubly linked list.
 *                 Note that the allocated memory IS marked as
 *                 deallocated in the pool.
 * Parameters    : listp: pointer to the list descriptor object
 * Return values : FALSE: no last node, nothing done
 *                 TRUE:  last node successfully removed from the list
 **********************************************************************/
#ifdef PDDCLIST_POP_BACK
BOOL pddclist_pop_back(PDDCLIST* listp) {
    PDDCNODE* nilp;
    PDDCNODE* old_node;
    nilp = (PDDCNODE*)listp;
    if (nilp->prevp != nilp) {
        old_node = nilp->prevp;
        nilp->prevp = old_node->prevp;
        old_node->prevp->nextp = nilp;
        scbpool_dealloc(listp->scbpoolp, old_node);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_pop_front
 * Time complex. : O(1) with pool heap deallocation
 * Description   : Removes the first node - one after the sentinel -
 *                 from a circular doubly linked list.
 *                 Note that the allocated memory IS marked as
 *                 deallocated in the pool.
 * Parameters    : listp: pointer to the list descriptor object
 * Return values : FALSE: no first node, nothing done
 *                 TRUE:  first node successfully removed from the list
 **********************************************************************/
#ifdef PDDCLIST_POP_FRONT
BOOL pddclist_pop_front(PDDCLIST* listp) {
    PDDCNODE* nilp;
    PDDCNODE* old_node;
    nilp = (PDDCNODE*)listp;
    if (nilp->nextp != nilp) {
        old_node = nilp->nextp;
        nilp->nextp = old_node->nextp;
        old_node->nextp->prevp = nilp;
        scbpool_dealloc(listp->scbpoolp, old_node);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_swap_head_and_tail
 * Time complex. : O(1)
 * Description   : Exchanges the head (first) and tail (last) nodes of a
 *                 circular doubly linked list so that head becomes tail
 *                 and tail becomes head.
 * Parameters    : listp: pointer to the list descriptor object
 * Return values : FALSE: head or tail node does not exist, nothing done
 *                 TRUE:  head and tail swapped successfully
 **********************************************************************/
#ifdef PDDCLIST_SWAP_HEAD_AND_TAIL
BOOL pddclist_swap_head_and_tail(PDDCLIST* listp) {
    PDDCNODE* nilp;
    PDDCNODE* headp;
    PDDCNODE* tailp;
    nilp = (PDDCNODE*)listp;
    headp = nilp->nextp;
    tailp = nilp->prevp;
    if (headp!=nilp && tailp!=nilp) {
        if (headp->nextp->nextp != nilp) {
            /* more nodes than only a sentinel, head, and a tail */
            tailp->prevp->nextp = headp;
            headp->nextp->prevp = tailp;
            tailp->nextp = headp->nextp;
            headp->prevp = tailp->prevp;
            tailp->prevp = nilp;
            headp->nextp = nilp;
            nilp->prevp = headp;
            nilp->nextp = tailp;
            return TRUE;
        }
        /* only a sentinel, head, and a tail */
        tailp->nextp = headp;
        headp->prevp = tailp;
        tailp->prevp = nilp;
        headp->nextp = nilp;
        nilp->prevp = headp;
        nilp->nextp = tailp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_swap_head_and_iter
 * Time complex. : O(1)
 * Description   : Exchanges the head (first) and any given node (iterp)
 *                 of a circular doubly linked list so that head becomes
 *                 iterp and iterp becomes head.
 * Parameters    : listp: pointer to the list descriptor object
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: head or iterp node not exist, nothing done
 *                 TRUE:  head and iterp swapped successfully
 **********************************************************************/
#ifdef PDDCLIST_SWAP_HEAD_AND_ITER
BOOL pddclist_swap_head_and_iter(PDDCLIST* listp, PDDCITER* iterp) {
    PDDCNODE* nilp;
    PDDCNODE* headp;
    PDDCNODE* tmp1p;
    PDDCNODE* tmp2p;
    nilp = (PDDCNODE*)listp;
    headp = nilp->nextp;
    if (headp!=nilp && iterp!=nilp) {
        tmp1p = iterp->prevp;
        tmp2p = iterp->nextp;
        iterp->prevp->nextp = headp;
        iterp->nextp->prevp = headp;
        headp->prevp->nextp = iterp;
        headp->nextp->prevp = iterp;
        iterp->prevp = headp->prevp;
        iterp->nextp = headp->nextp;
        headp->prevp = tmp1p;
        headp->nextp = tmp2p;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_swap_iter_and_tail
 * Time complex. : O(1)
 * Description   : Exchanges any given node (iterp) and the tail (last)
 *                 of a circular doubly linked list so that iterp
 *                 becomes tail and tail becomes iterp.
 * Parameters    : listp: pointer to the list descriptor object
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: iterp and tail node not exist, nothing done
 *                 TRUE:  iterp and tail swapped successfully
 **********************************************************************/
#ifdef PDDCLIST_SWAP_ITER_AND_TAIL
BOOL pddclist_swap_iter_and_tail(PDDCLIST* listp, PDDCITER* iterp) {
    PDDCNODE* nilp;
    PDDCNODE* tailp;
    PDDCNODE* tmp1p;
    PDDCNODE* tmp2p;
    nilp = (PDDCNODE*)listp;
    tailp = nilp->prevp;
    if (iterp!=nilp && tailp!=nilp) {
        tmp1p = tailp->prevp;
        tmp2p = tailp->nextp;
        tailp->prevp->nextp = iterp;
        tailp->nextp->prevp = iterp;
        iterp->prevp->nextp = tailp;
        iterp->nextp->prevp = tailp;
        tailp->prevp = iterp->prevp;
        tailp->nextp = iterp->nextp;
        iterp->prevp = tmp1p;
        iterp->nextp = tmp2p;
        return TRUE;
    }
    return FALSE;    
}
#endif

/***********************************************************************
 * Function      : pddclist_swap_iter_and_iter
 * Time complex. : O(1)
 * Description   : Exchanges two arbitrary nodes of a circular doubly
 *                 linked list so that iter1p becomes iter2p and iter2p
 *                 becomes iter1p.
 * Parameters    : listp:  pointer to the list descriptor object
 *                 iter1p: iterator (pointer) to any given node
 *                 iter2p: iterator (pointer) to any given node
 * Return values : FALSE:  iter1p or iter2p node not exist, nothing done
 *                 TRUE:   iter1p and iter2p swapped successfully
 **********************************************************************/
#ifdef PDDCLIST_SWAP_ITER_AND_ITER
BOOL pddclist_swap_iter_and_iter(PDDCLIST* listp, PDDCITER* iter1p,
PDDCITER* iter2p) {
    PDDCNODE* nilp;
    PDDCNODE* tmp1p;
    PDDCNODE* tmp2p;
    nilp = (PDDCNODE*)listp;
    if (iter1p!=nilp && iter2p!=nilp) {
        tmp1p = iter2p->prevp;
        tmp2p = iter2p->nextp;
        iter2p->prevp->nextp = iter1p;
        iter2p->nextp->prevp = iter1p;
        iter1p->prevp->nextp = iter2p;
        iter1p->nextp->prevp = iter2p;
        iter2p->prevp = iter1p->prevp;
        iter2p->nextp = iter1p->nextp;
        iter1p->prevp = tmp1p;
        iter1p->nextp = tmp2p;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_move_head_to_tail
 * Time complex. : O(1)
 * Description   : Moves head node of a circular doubly linked list to
 *                 the tail node, i.e. one after the satellite node to
 *                 one before the satellite node.
 *                 References to the head node are removed and replaced
 *                 by references to the tail node.
 * Parameters    : listp: pointer to the list descriptor object
 * Return values : FALSE: head or tail missing, cannot proceed
 *                 TRUE:  head moved successfully to the tail
 **********************************************************************/
#ifdef PDDCLIST_MOVE_HEAD_TO_TAIL
BOOL pddclist_move_head_to_tail(PDDCLIST* listp) {
    PDDCNODE* nilp;
    PDDCNODE* headp;
    PDDCNODE* tailp;
    nilp = (PDDCNODE*)listp;
    headp = nilp->nextp;
    tailp = nilp->prevp;
    if (headp!=nilp && tailp!=nilp) {
        nilp->nextp = headp->nextp;
        headp->nextp->prevp = nilp;
        headp->prevp = tailp;
        headp->nextp = nilp;
        tailp->nextp = headp;
        nilp->prevp = headp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_move_tail_to_head
 * Time complex. : O(1)
 * Description   : Moves tail node of a circular doubly linked list to
 *                 the head node, i.e. one before the satellite node to
 *                 one after the satellite node.
 *                 References to the tail node are removed and replaced
 *                 by references to the head node.
 * Parameters    : listp: pointer to the list descriptor object
 * Return values : FALSE: head or tail missing, cannot proceed
 *                 TRUE:  tail moved successfully to head
 **********************************************************************/
#ifdef PDDCLIST_MOVE_TAIL_TO_HEAD
BOOL pddclist_move_tail_to_head(PDDCLIST* listp) {
    PDDCNODE* nilp;
    PDDCNODE* headp;
    PDDCNODE* tailp;
    nilp = (PDDCNODE*)listp;
    headp = nilp->nextp;
    tailp = nilp->prevp;
    if (headp!=nilp && tailp!=nilp) {
        tailp->prevp->nextp = nilp;
        nilp->prevp = tailp->prevp;
        tailp->prevp = nilp;
        tailp->nextp = headp;
        nilp->nextp = tailp;
        headp->prevp = tailp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_move_iter_to_head
 * Time complex. : O(1)
 * Description   : Moves any given node to the head of the circular
 *                 doubly linked list, i.e. one after the satellite
 *                 node.
 *                 References to the iterp node are removed and replaced
 *                 by references to the head node.
 * Parameters    : listp: pointer to the list descriptor object
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: iterp is sentinel node, cannot proceed
 *                 TRUE:  iterp moved successfully to the head
 **********************************************************************/
#ifdef PDDCLIST_MOVE_ITER_TO_HEAD
BOOL pddclist_move_iter_to_head(PDDCLIST* listp, PDDCITER* iterp) {
    PDDCNODE* nilp;
    nilp = (PDDCNODE*)listp;
    if (iterp != nilp) {
        iterp->prevp->nextp = iterp->nextp;
        iterp->nextp->prevp = iterp->prevp;
        iterp->prevp = nilp;
        iterp->nextp = nilp->nextp;
        nilp->nextp->prevp = iterp;
        nilp->nextp = iterp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_move_iter_to_tail
 * Time complex. : O(1)
 * Description   : Moves any given node to the tail of the circular
 *                 doubly linked list, i.e. one before the satellite
 *                 node.
 *                 References to the iterp node are removed and replaced
 *                 by references to the tail node.
 * Parameters    : listp: pointer to the list descriptor object
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: iterp is sentinel node, cannot proceed
 *                 TRUE:  iterp moved successfully to the tail
 **********************************************************************/
#ifdef PDDCLIST_MOVE_ITER_TO_TAIL
BOOL pddclist_move_iter_to_tail(PDDCLIST* listp, PDDCITER* iterp) {
    PDDCNODE* nilp;
    nilp = (PDDCNODE*)listp;
    if (iterp != nilp) {
        iterp->prevp->nextp = iterp->nextp;
        iterp->nextp->prevp = iterp->prevp;
        iterp->prevp = nilp->prevp;
        iterp->nextp = nilp;
        nilp->prevp->nextp = iterp;
        nilp->prevp = iterp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_insert_before_iter
 * Time complex. : O(1) with pool heap allocation
 * Description   : Creates a new circular doubly linked list node in
 *                 the list's internal static circular pool.
 *                 The node is inserted before the place where iterp
 *                 points to.
 * Parameters    : listp: pointer to the list descriptor object
 *                 iterp: iterator (pointer) to any given node
 *                 satp:  pointer to satellite data
 * Return values : FALSE: memory allocation failed, node not created
 *                 TRUE:  node successfully inserted before the iter
 **********************************************************************/
#ifdef PDDCLIST_INSERT_BEFORE_ITER
BOOL pddclist_insert_before_iter(PDDCLIST* listp, PDDCITER* iterp,
void* satp) {
    PDDCNODE* new_node;
    new_node = (PDDCNODE*)scbpool_alloc(listp->scbpoolp);
    if (new_node) {
        new_node->prevp = iterp->prevp;
        new_node->nextp = iterp;
        iterp->prevp->nextp = new_node;
        iterp->prevp = new_node;
        memcpy(new_node+1, satp, listp->satsize);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_insert_after_iter
 * Time complex. : O(1) with pool heap allocation
 * Description   : Create a new circular doubly linked list node in
 *                 the list's internal static circular pool.
 *                 The node is inserted after the place where iterp
 *                 points to.
 * Parameters    : listp: pointer to the list descriptor object
 *                 iterp: iterator (pointer) to any given node
 *                 satp:  pointer to satellite data
 * Return values : FALSE: memory allocation failed, node not created
 *                 TRUE:  node successfully inserted after the iter
 **********************************************************************/
#ifdef PDDCLIST_INSERT_AFTER_ITER
BOOL pddclist_insert_after_iter(PDDCLIST* listp, PDDCITER* iterp,
void* satp) {
    PDDCNODE* new_node;
    new_node = (PDDCNODE*)scbpool_alloc(listp->scbpoolp);
    if (new_node) {
        new_node->prevp = iterp;
        new_node->nextp = iterp->nextp;
        iterp->nextp->prevp = new_node;
        iterp->nextp = new_node;
        memcpy(new_node+1, satp, listp->satsize);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_erase_one_at_iter
 * Time complex. : O(1) with pool heap deallocation
 * Description   : In a circular doubly linked list, erases the node
 *                 where iterp points to.
 *                 Note that the allocated memory IS marked as
 *                 deallocated in the pool.
 * Parameters    : listp: pointer to the list descriptor object
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: iterp is sentinel node, cannot proceed
 *                 TRUE:  iterp erased successfully
 **********************************************************************/
#ifdef PDDCLIST_ERASE_ONE_AT_ITER
BOOL pddclist_erase_one_at_iter(PDDCLIST* listp, PDDCITER* iterp) {
    PDDCNODE* nilp;
    nilp = (PDDCNODE*)listp;
    if (iterp != nilp) {
        iterp->prevp->nextp = iterp->nextp;
        iterp->nextp->prevp = iterp->prevp;
        scbpool_dealloc(listp->scbpoolp, iterp);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_extract_array_screate
 * Time complex. : O(n) with pool heap allocation +
 *                 O(n) traversing,
 *                 where n is the number of nodes in the list
 * Description   : Extracts the data stored in a linear memory array
 *                 to a circular doubly linked list.
 *                 The list to be created has nelems number of nodes,
 *                 each containing satsize amount of user data
 *                 (satsize*nelems is the maximum size of the linear
 *                 memory area).
 *                 See also pddclist_screate.
 * Parameters    : listp:    pointer to the list descriptor object
 *                 scbpoolp: pointer to the static circular pool
 *                 memory:   pointer to memory to be extracted
 *                 nelems:   number of elements in the array
 * Return values : FALSE:    memory allocation failed, missing nodes
 *                 TRUE:     array extracted successfully to a list
 **********************************************************************/
#ifdef PDDCLIST_EXTRACT_ARRAY_SCREATE
BOOL pddclist_extract_array_screate(PDDCLIST* listp, SCBPOOL* scbpoolp, 
void* memory, UINT nelems) {
    UINT satsize;
    unsigned char* memep;
    unsigned char* memp = memory;
    pddclist_screate(listp, scbpoolp);
    satsize = listp->satsize;
    memep = memp + (satsize*nelems);
    while (memp < memep) {
        if (!pddclist_push_back(listp,memp))
            return FALSE;
        memp += satsize;
    }
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : pddclist_extract_array_hcreate
 * Time complex. : O(1) with heap allocation +
 *                 O(n) with pool heap allocation +
 *                 O(n) traversing,
 *                 where n is the number of nodes in the list
 * Description   : Extracts the data stored in a linear memory area to
 *                 circular doubly linked list.
 *                 The list to be created has nelems number of nodes,
 *                 each containing satsize amount of user data
 *                 (satsize*nelems is the maximum size of the linear
 *                 memory area).
 * Parameters    : listpp:   pointer to the list descriptor object
 *                 scbpoolp: pointer to the static circular pool
 *                 memory:   pointer to memory to be extracted
 *                 nelems:   number of elements in the array
 * Return values : FALSE:    memory allocation failed, missing nodes if
 *                           listpp is not zero, no list if listpp is
 *                           zero
 *                 TRUE:     array extracted successfully to a list
 **********************************************************************/
#ifdef PDDCLIST_EXTRACT_ARRAY_HCREATE
BOOL pddclist_extract_array_hcreate(PDDCLIST** listpp,
SCBPOOL* scbpoolp, void* memory, UINT nelems) {
    UINT satsize;
    PDDCLIST* new_list;
    unsigned char* memep;
    unsigned char* memp = memory;
    new_list = pddclist_hcreate(scbpoolp);
    if (!new_list) {
        *listpp = NULL;
        return FALSE;
    }
    satsize = new_list->satsize;
    memep = memp + (satsize*nelems);
    while (memp < memep) {
        if (!pddclist_push_back(new_list,memp)) {
            *listpp = new_list;
            return FALSE;
        }
        memp += satsize;
    }
    *listpp = new_list;
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : pddclist_extract_list
 * Time complex. : O(n) + O(n), where n is the number of nodes in a list
 * Description   : Extracts data stored in a circular doubly linked list
 *                 to a linear memory area.
 * Parameters    : listp:  pointer to the list descriptor object
 *                 memory: pointer to memory to be extracted
 * Return values : None
 **********************************************************************/
#ifdef PDDCLIST_EXTRACT_LIST
void pddclist_extract_list(PDDCLIST* listp, void* memory) {
    UINT memsize;
    UINT satsize;
    PDDCITER* curnode;
    PDDCITER* nilp;
    unsigned char* memep;
    unsigned char* memsp;
    unsigned char* memp = memory;
    nilp = (PDDCNODE*)listp;
    satsize = listp->satsize;
    memsize = pddclist_count_nodes(listp) * satsize;
    curnode = nilp;
    memsp = memp;
    memep = memp + memsize;
    while (memp < memep) {
        curnode = curnode->nextp;
        memcpy(memp, curnode+1, satsize);
        memp += satsize;
    }
}
#endif

/***********************************************************************
 * Function      : pddclist_extract_list_alloc
 * Time complex. : O(1) with heap allocation +
 *                 O(n) + O(n), where n is the number of nodes in a list
 * Description   : Extracts data stored in a circular doubly linked list
 *                 to a linear memory area.
 *                 Note that this function only supports lists having
 *                 the same amount of satellite data.
 * Parameters    : listp:    pointer to the list descriptor object
 * Return values : Non-NULL: pointer to the new linear memory area
 *                 NULL:     memory allocation failed, no linear area
 **********************************************************************/
#ifdef PDDCLIST_EXTRACT_LIST_ALLOC
void* pddclist_extract_list_alloc(PDDCLIST* listp) {
    UINT memsize;
    UINT satsize;
    PDDCNODE* nilp;
    PDDCITER* curnode;
    unsigned char* memp;
    unsigned char* memep;
    unsigned char* memsp;
    nilp = (PDDCNODE*)listp;
    satsize = listp->satsize;
    memsize = pddclist_count_nodes(listp) * satsize;
    memp = malloc(memsize);
    if (!memp)
        return NULL;
    curnode = nilp;
    memsp = memp;
    memep = memp + memsize;
    while (memp < memep) {
        curnode = curnode->nextp;
        memcpy(memp, curnode+1, satsize);
        memp += satsize;
    }
    return memsp;
}
#endif

/***********************************************************************
 * Function      : pddclist_iterate_next
 * Time complex. : O(1)
 * Description   : Iterates an iterator to the next node of a circular
 *                 doubly linked list.
 * Parameters    : listp:  pointer to the list descriptor object
 *                 iterpp: pointer to an iterator
 * Return values : FALSE:  next node was sentinel, no iteration done
 *                 TRUE:   iterator iterated successfully
 **********************************************************************/
#ifdef PDDCLIST_ITERATE_NEXT
BOOL pddclist_iterate_next(PDDCLIST* listp, PDDCITER** iterpp) {
    PDDCNODE* nilp;
    nilp = (PDDCNODE*)listp;
    if ((*iterpp)->nextp != nilp) {
        *iterpp = (*iterpp)->nextp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_iterate_prev
 * Time complex. : O(1)
 * Description   : Iterates an iterator to the previous node of a
 *                 circular doubly linked list.
 * Parameters    : listp:  pointer to the list descriptor object
 *                 iterpp: pointer to an iterator
 * Return values : FALSE:  next node was sentinel, no iteration done
 *                 TRUE:   iterator iterated successfully
 **********************************************************************/
#ifdef PDDCLIST_ITERATE_PREV
BOOL pddclist_iterate_prev(PDDCLIST* listp, PDDCITER** iterpp) {
    PDDCNODE* nilp;
    nilp = (PDDCNODE*)listp;
    if ((*iterpp)->prevp != nilp) {
        *iterpp = (*iterpp)->prevp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_iterate_first
 * Time complex. : O(1)
 * Description   : Sets an iterator to the first (head) node of a
 *                 circular doubly linked list.
 * Parameters    : listp:  pointer to the list descriptor object
 *                 iterpp: pointer to an iterator
 * Return values : FALSE:  first node does not exist, iteration not done
 *                 TRUE:   iterator set to head successfully
 **********************************************************************/
#ifdef PDDCLIST_ITERATE_FIRST
BOOL pddclist_iterate_first(PDDCLIST* listp, PDDCITER** iterpp) {
    PDDCNODE* nilp;
    nilp = (PDDCNODE*)listp;
    if (nilp->nextp != nilp) {
        *iterpp = nilp->nextp;
        return TRUE;
    }
    *iterpp = nilp;
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_iterate_last
 * Time complex. : O(1)
 * Description   : Sets an iterator to the last (tail) node of a
 *                 circular doubly linked list.
 * Parameters    : listp:  pointer to the list descriptor object
 *                 iterpp: pointer to an iterator
 * Return values : FALSE:  last node does not exist, iteration not done
 *                 TRUE:   iterator set to tail successfully
 **********************************************************************/
#ifdef PDDCLIST_ITERATE_LAST
BOOL pddclist_iterate_last(PDDCLIST* listp, PDDCITER** iterpp) {
    PDDCNODE* nilp;
    nilp = (PDDCNODE*)listp;
    if (nilp->prevp != nilp) {
        *iterpp = nilp->prevp;
        return TRUE;
    }
    *iterpp = nilp;
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_iterate_nth_next
 * Time complex. : O(n), where n is nth node in the list
 * Description   : Iterates an iterator to the next node of a circular
 *                 doubly linked list by a predefined count.
 * Parameters    : listp:  pointer to the list descriptor object
 *                 iterpp: pointer to an iterator
 *                 nth:    how many iteration to do
 * Return values : FALSE:  next node was sentinel, iteration stopped
 *                 TRUE:   iterated successfully to the nth node
 **********************************************************************/
#ifdef PDDCLIST_ITERATE_NTH_NEXT
BOOL pddclist_iterate_nth_next(PDDCLIST* listp, PDDCITER** iterpp,
UINT nth) {
    PDDCNODE* nilp;
    PDDCNODE* curnode;
    curnode = *iterpp;
    nilp = (PDDCNODE*)listp;
    while (nth > 0) {
        curnode = curnode->nextp;
        if (curnode == nilp)
            return FALSE;
        --nth;
    }
    *iterpp = curnode;
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : pddclist_iterate_nth_prev
 * Time complex. : O(n), where n is nth node in the list
 * Description   : Iterates an iterator to the previous node of a
 *                 circular doubly linked list by a predefined count.
 * Parameters    : listp:  pointer to the list descriptor object
 *                 iterpp: pointer to an iterator
 *                 nth:    how many iterations to do
 * Return values : FALSE:  previous node was sentinel, iteration stopped
 *                 TRUE:   iterated successfully to the nth node
 **********************************************************************/
#ifdef PDDCLIST_ITERATE_NTH_PREV
BOOL pddclist_iterate_nth_prev(PDDCLIST* listp, PDDCITER** iterpp,
UINT nth) {
    PDDCNODE* nilp;
    PDDCNODE* curnode;
    curnode = *iterpp;
    nilp = (PDDCNODE*)listp;
    while (nth > 0) {
        curnode = curnode->prevp;
        if (curnode == nilp)
            return FALSE;
        --nth;
    }
    *iterpp = curnode;
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : pddclist_count_nodes
 * Time complex. : O(n), where n is number of in the list
 * Description   : Counts the number of nodes in a circular doubly
 *                 linked list.
 * Parameters    : listp: pointer to the list descriptor object
 * Return values : Number of nodes in the list
 **********************************************************************/
#ifdef PDDCLIST_COUNT_NODES
UINT pddclist_count_nodes(PDDCLIST* listp) {
    UINT nodes;
    PDDCNODE* nilp;
    PDDCNODE* curnode;
    nodes = 0;
    nilp = (PDDCNODE*)listp;
    curnode = nilp->nextp;
    while (curnode != nilp) {
        curnode = curnode->nextp;
        ++nodes;
    }
    return nodes;
}
#endif

/***********************************************************************
 * Function      : pddclist_find_first
 * Time complex. : O(n), where n is the first matching node
 * Description   : In a circular doubly linked list, finds the first
 *                 node fulfilling a certain condition.
 *                 The condition checker must be an external compare
 *                 function which receives pointer to the satellite
 *                 data of a node currently iterated to.
 *                 The external compare function must return non-zero
 *                 when to stop traverse the list, zero to continue
 *                 traversing. It can also explicitly command the
 *                 iteration to stop when it is known a match can't
 *                 occur in subsequent iterations.
 * Parameters    : listp:   pointer to the list descriptor object
 *                 iterpp:  pointer to an iterator
 *                 compare: pointer to the external compare function
 * Return values : FALSE:   node not found, iterpp is the last node
 *                 TRUE:    node found, iterpp is the matching node
 **********************************************************************/
#ifdef PDDCLIST_FIND_FIRST
BOOL pddclist_find_first(PDDCLIST* listp, PDDCITER** iterpp,
BOOL (*compare)(void*,BOOL*)) {
    BOOL stop;
    PDDCNODE* nilp;
    PDDCNODE* curnode;
    PDDCNODE* nextnode;
    stop = FALSE;
    curnode = *iterpp;
    nilp = (PDDCNODE*)listp;
    while (curnode != nilp) {
        nextnode = curnode->nextp;
        if (compare(curnode+1,&stop)) {
            *iterpp = curnode;
            return TRUE;
        }
        if (stop) {
            *iterpp = curnode;
            return FALSE;
        }
        curnode = nextnode;
    }
    *iterpp = nilp;
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_find_last
 * Time complex. : O(n), where n is the last matching node
 * Description   : In a circular doubly linked list, finds the last
 *                 node fulfilling a certain condition.
 *                 The condition checker must be an external compare
 *                 function which receives pointer to the satellite
 *                 data of a node currently iterated to.
 *                 The external compare function must return non-zero
 *                 when to stop traverse the list, zero to continue
 *                 traversing. It can also explicitly command the
 *                 iteration to stop when it is known a match can't
 *                 occur in subsequent iterations.
 * Parameters    : listp:   pointer to the list descriptor object
 *                 iterpp:  pointer to an iterator
 *                 compare: pointer to the external compare function
 * Return values : FALSE:   node not found, iterpp is the first node
 *                 TRUE:    node found, iterpp is the matching node
 **********************************************************************/
#ifdef PDDCLIST_FIND_LAST
BOOL pddclist_find_last(PDDCLIST* listp, PDDCITER** iterpp,
BOOL (*compare)(void*,BOOL*)) {
    BOOL stop;
    PDDCNODE* nilp;
    PDDCNODE* curnode;
    PDDCNODE* prevnode;
    stop = FALSE;
    curnode = *iterpp;
    nilp = (PDDCNODE*)listp;
    while (curnode != nilp) {
        prevnode = curnode->prevp;
        if (compare(curnode+1,&stop)) {
            *iterpp = curnode;
            return TRUE;
        }
        if (stop) {
            *iterpp = curnode;
            return FALSE;
        }
        curnode = prevnode;
    }
    *iterpp = nilp;
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : pddclist_clear
 * Time complex. : O(1) pool clearing
 * Description   : Clears a circular doubly linked list.
 *                 All but the sentinel node are freed from the heap.
 *                 Note: after deallocation the list is ready for reuse.
 * Parameters    : listp: pointer to the list descriptor object
 * Return values : None
 **********************************************************************/
#ifdef PDDCLIST_CLEAR
void pddclist_clear(PDDCLIST* listp) {
    scbpool_clear(listp->scbpoolp);
    listp->list.prevp = (PDDCNODE*)listp;
    listp->list.nextp = (PDDCNODE*)listp;
}
#endif

/***********************************************************************
 * Function      : pddclist_clear_advanced
 * Time complex. : O(n) traversing +
 *                 O(n) with auxiliary heap deallocation +
 *                 O(1) pool deallocation,
 *                 where n is the number of nodes in the list
 * Description   : Clears a circular doubly linked list.
 *                 All but the sentinel node are freed from the heap.
 *                 For all but the sentinel node an external dealloc
 *                 function is called which receives a pointer to the
 *                 satellite data of the node currently iterated to.
 *                 Note: after deallocation the list is ready for reuse.
 * Parameters    : listp:   pointer to the list descriptor object
 *                 dealloc: pointer to the external dealloc function
 * Return values : None
 **********************************************************************/
#ifdef PDDCLIST_CLEAR_ADVANCED
void pddclist_clear_advanced(PDDCLIST* listp,
void (*dealloc)(void*)) {
    PDDCNODE* nilp;
    PDDCNODE* oldnode;
    PDDCNODE* curnode;
    nilp = (PDDCNODE*)listp;
    curnode = nilp->nextp;
    while (curnode != nilp) {
        oldnode = curnode;
        curnode = curnode->nextp;
        dealloc(oldnode+1);
    }
    scbpool_clear(listp->scbpoolp);
    listp->list.prevp = (PDDCNODE*)listp;
    listp->list.nextp = (PDDCNODE*)listp;
}
#endif

/***********************************************************************
 * Function      : pddclist_destroy
 * Time complex. : O(1) pool destroy +
 *                 O(1) with heap deallocation
 * Description   : Destroys a circular doubly linked list.
 *                 All nodes, including the sentinel, are freed from the
 *                 heap.
 * Parameters    : listp: pointer to the list descriptor object
 * Return values : None
 **********************************************************************/
#ifdef PDDCLIST_DESTROY
void pddclist_destroy(PDDCLIST* listp) {
    scbpool_destroy(listp->scbpoolp);
    free(listp);
}
#endif

/***********************************************************************
 * Function      : pddclist_destroy_advanced
 * Time complex. : O(n) traversing +
 *                 O(n) with auxiliary heap deallocation +
 *                 O(1) pool deallocation +
 *                 O(1) with heap deallocation,
 *                 where n is the number of nodes in the list
 * Description   : Destroys a circular doubly linked list.
 *                 All nodes, including the sentinel, are freed from the
 *                 heap.
 *                 For all but the satellite node an external dealloc
 *                 function is called which receives a pointer to the
 *                 satellite data of the node currently iterated to.
 * Parameters    : listp:   pointer to the list descriptor object
 *                 dealloc: pointer to the external dealloc function
 * Return values : None
 **********************************************************************/
#ifdef PDDCLIST_DESTROY_ADVANCED
void pddclist_destroy_advanced(PDDCLIST* listp,
void (*dealloc)(void*)) {
    PDDCNODE* nilp;
    PDDCNODE* oldnode;
    PDDCNODE* curnode;
    nilp = (PDDCNODE*)listp;
    curnode = nilp->nextp;
    while (curnode != nilp) {
        oldnode = curnode;
        curnode = curnode->nextp;
        dealloc(oldnode+1);
    }
    scbpool_destroy(listp->scbpoolp);
    free(listp);    
}
#endif
