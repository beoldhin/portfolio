/* Circular doubly linked list implementation  */
/* (as explained in Introduction to Algorithms) */

#include <string.h>
#include "dclist.h"

/***********************************************************************
 * Function      : dclist_screate
 * Time complex. : O(1) without heap allocation
 * Description   : Initializes a circular doubly linked list without
 *                 allocating space for the sentinel node.
 *                 Only non-nested lists are supported (address space
 *                 change not possible).
 * Parameters    : listp: pointer to the sentinel node
 *                        (sentinel usually in stack with this function)
 * Return values : None
 **********************************************************************/
#ifdef DCLIST_SCREATE
void dclist_screate(DCLIST* listp) {
    listp->prevp = listp;
    listp->nextp = listp;
}
#endif

/***********************************************************************
 * Function      : dclist_hcreate
 * Time complex. : O(1) with heap allocation
 * Description   : Creates a new circular doubly linked list by
 *                 allocating heap space for the sentinel node.
 *                 Unlike dclist_screate, this function supports nested
 *                 lists (address space change is possible).
 * Parameters    : None
 * Return values : Non-NULL: pointer to the new list's sentinel node
 *                 NULL:     memory allocation failed, list not created
 **********************************************************************/
#ifdef DCLIST_HCREATE
DCLIST* dclist_hcreate(void) {
    DCLIST* new_list;
    new_list = (DCLIST*)malloc(sizeof(DCLIST));
    if (new_list) {
        new_list->prevp = new_list;
        new_list->nextp = new_list;
        return new_list;
    }
    return NULL;
}
#endif

/***********************************************************************
 * Function      : dclist_push_back
 * Time complex. : O(1) with heap allocation
 * Description   : Creates a new circular doubly linked list node in
 *                 heap.
 *                 The node is inserted to the tail of the list, i.e.
 *                 one before the satellite node.
 * Parameters    : nilp:    pointer to the initialized sentinel node
 *                 satp:    pointer to the satellite data
 *                 satsize: size of satellite data in bytes
 * Return values : FALSE:   memory allocation failed, node not created
 *                 TRUE:    node successfully appended to the list
 **********************************************************************/
#ifdef DCLIST_PUSH_BACK
BOOL dclist_push_back(DCLIST* nilp, void* satp, UINT satsize) {
    DCNODE* new_node;
    new_node = (DCNODE*)malloc(sizeof(DCNODE)+satsize);
    if (new_node) {
        new_node->prevp = nilp->prevp;
        new_node->nextp = nilp;
        nilp->prevp->nextp = new_node;
        nilp->prevp = new_node;
        memcpy(new_node+1, satp, satsize);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_push_front
 * Time complex. : O(1) with heap allocation
 * Description   : Creates a new circular doubly linked list node in
 *                 heap.
 *                 The node is inserted to the head of the list, i.e.
 *                 one after the satellite node.
 * Parameters    : nilp:    pointer to the initialized sentinel node
 *                 satp:    pointer to the satellite data
 *                 satsize: size of the satellite data in bytes
 * Return values : FALSE:   memory allocation failed, node not created
 *                 TRUE:    node successfully inserted to the list
 **********************************************************************/
#ifdef DCLIST_PUSH_FRONT
BOOL dclist_push_front(DCLIST* nilp, void* satp, UINT satsize) {
    DCNODE* new_node;
    new_node = (DCNODE*)malloc(sizeof(DCNODE)+satsize);
    if (new_node) {
        new_node->prevp = nilp;
        new_node->nextp = nilp->nextp;
        nilp->nextp->prevp = new_node;
        nilp->nextp = new_node;
        memcpy(new_node+1, satp, satsize);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_pop_back
 * Time complex. : O(1) with heap deallocation
 * Description   : Removes the last node - one before the sentinel -
 *                 from a circular doubly linked list.
 *                 The allocated memory in heap is freed.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 * Return values : FALSE: no last node, nothing done
 *                 TRUE:  last node successfully removed from the list
 **********************************************************************/
#ifdef DCLIST_POP_BACK
BOOL dclist_pop_back(DCLIST* nilp) {
    DCNODE* old_node;
    if (nilp->prevp != nilp) {
        old_node = nilp->prevp;
        nilp->prevp = old_node->prevp;
        old_node->prevp->nextp = nilp;
        free(old_node);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_pop_front
 * Time complex. : O(1) with heap deallocation
 * Description   : Removes the first node - one after the sentinel -
 *                 from a circular doubly linked list.
 *                 The allocated memory in heap is freed.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 * Return values : FALSE: no first node, nothing done
 *                 TRUE:  first node successfully removed from the list
 **********************************************************************/
#ifdef DCLIST_POP_FRONT
BOOL dclist_pop_front(DCLIST* nilp) {
    DCNODE* old_node;
    if (nilp->nextp != nilp) {
        old_node = nilp->nextp;
        nilp->nextp = old_node->nextp;
        old_node->nextp->prevp = nilp;
        free(old_node);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_swap_head_and_tail
 * Time complex. : O(1)
 * Description   : Exchanges the head (first) and tail (last) nodes of a
 *                 circular doubly linked list so that head becomes tail
 *                 and tail becomes head.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 * Return values : FALSE: head or tail node does not exist, nothing done
 *                 TRUE:  head and tail swapped successfully
 **********************************************************************/
#ifdef DCLIST_SWAP_HEAD_AND_TAIL
BOOL dclist_swap_head_and_tail(DCLIST* nilp) {
    DCNODE* headp;
    DCNODE* tailp;
    headp = nilp->nextp;
    tailp = nilp->prevp;
    if (headp!=nilp && tailp!=nilp) {
        if (headp->nextp->nextp != nilp) {
            /* more nodes than only a sentinel, head, and a tail */
            tailp->prevp->nextp = headp;
            headp->nextp->prevp = tailp;
            tailp->nextp = headp->nextp;
            headp->prevp = tailp->prevp;
            tailp->prevp = nilp;
            headp->nextp = nilp;
            nilp->prevp = headp;
            nilp->nextp = tailp;
            return TRUE;
        }
        /* only a sentinel, head, and a tail */
        tailp->nextp = headp;
        headp->prevp = tailp;
        tailp->prevp = nilp;
        headp->nextp = nilp;
        nilp->prevp = headp;
        nilp->nextp = tailp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_swap_head_and_iter
 * Time complex. : O(1)
 * Description   : Exchanges the head (first) and any given node (iterp)
 *                 of a circular doubly linked list so that head becomes
 *                 iterp and iterp becomes head.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: head or iterp node not exist, nothing done
 *                 TRUE:  head and iterp swapped successfully
 **********************************************************************/
#ifdef DCLIST_SWAP_HEAD_AND_ITER
BOOL dclist_swap_head_and_iter(DCLIST* nilp, DCITER* iterp) {
    DCNODE* headp;
    DCNODE* tmp1p;
    DCNODE* tmp2p;
    headp = nilp->nextp;
    if (headp!=nilp && iterp!=nilp) {
        tmp1p = iterp->prevp;
        tmp2p = iterp->nextp;
        iterp->prevp->nextp = headp;
        iterp->nextp->prevp = headp;
        headp->prevp->nextp = iterp;
        headp->nextp->prevp = iterp;
        iterp->prevp = headp->prevp;
        iterp->nextp = headp->nextp;
        headp->prevp = tmp1p;
        headp->nextp = tmp2p;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_swap_iter_and_tail
 * Time complex. : O(1)
 * Description   : Exchanges any given node (iterp) and the tail (last)
 *                 of a circular doubly linked list so that iterp
 *                 becomes tail and tail becomes iterp.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: iterp or tail node not exist, nothing done
 *                 TRUE:  iterp and tail swapped successfully
 **********************************************************************/
#ifdef DCLIST_SWAP_ITER_AND_TAIL
BOOL dclist_swap_iter_and_tail(DCLIST* nilp, DCITER* iterp) {
    DCNODE* tailp;
    DCNODE* tmp1p;
    DCNODE* tmp2p;
    tailp = nilp->prevp;
    if (iterp!=nilp && tailp!=nilp) {
        tmp1p = tailp->prevp;
        tmp2p = tailp->nextp;
        tailp->prevp->nextp = iterp;
        tailp->nextp->prevp = iterp;
        iterp->prevp->nextp = tailp;
        iterp->nextp->prevp = tailp;
        tailp->prevp = iterp->prevp;
        tailp->nextp = iterp->nextp;
        iterp->prevp = tmp1p;
        iterp->nextp = tmp2p;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_swap_iter_and_iter
 * Time complex. : O(1)
 * Description   : Exchanges two arbitrary nodes of a circular doubly
 *                 linked list so that iter1p becomes iter2p and iter2p
 *                 becomes iter1p.
 * Parameters    : nilp:   pointer to the initialized sentinel node
 *                 iter1p: iterator (pointer) to any given node
 *                 iter2p: iterator (pointer) to any given node
 * Return values : FALSE:  iter1p or iter2p node not exist, nothing done
 *                 TRUE:   iter1p and iter2p swapped successfully
 **********************************************************************/
#ifdef DCLIST_SWAP_ITER_AND_ITER
BOOL dclist_swap_iter_and_iter(DCLIST* nilp, DCITER* iter1p,
DCITER* iter2p) {
    DCNODE* tmp1p;
    DCNODE* tmp2p;
    if (iter1p!=nilp && iter2p!=nilp) {
        tmp1p = iter2p->prevp;
        tmp2p = iter2p->nextp;
        iter2p->prevp->nextp = iter1p;
        iter2p->nextp->prevp = iter1p;
        iter1p->prevp->nextp = iter2p;
        iter1p->nextp->prevp = iter2p;
        iter2p->prevp = iter1p->prevp;
        iter2p->nextp = iter1p->nextp;
        iter1p->prevp = tmp1p;
        iter1p->nextp = tmp2p;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_move_head_to_tail
 * Time complex. : O(1)
 * Description   : Moves head node of a circular doubly linked list to
 *                 the tail node, i.e. one after the satellite node to
 *                 one before the satellite node.
 *                 References to the head node are removed and replaced
 *                 by references to the tail node.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 * Return values : FALSE: head or tail missing, cannot proceed
 *                 TRUE:  head moved successfully to tail
 **********************************************************************/
#ifdef DCLIST_MOVE_HEAD_TO_TAIL
BOOL dclist_move_head_to_tail(DCLIST* nilp) {
    DCNODE* headp;
    DCNODE* tailp;
    headp = nilp->nextp;
    tailp = nilp->prevp;
    if (headp!=nilp && tailp!=nilp) {
        nilp->nextp = headp->nextp;
        headp->nextp->prevp = nilp;
        headp->prevp = tailp;
        headp->nextp = nilp;
        tailp->nextp = headp;
        nilp->prevp = headp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_move_tail_to_head
 * Time complex. : O(1)
 * Description   : Moves tail node of a circular doubly linked list to
 *                 the head node, i.e. one before the satellite node to
 *                 one after the satellite node.
 *                 References to the tail node are removed and replaced
 *                 by references to the head node.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 * Return values : FALSE: head or tail missing, cannot proceed
 *                 TRUE:  tail moved successfully to head
 **********************************************************************/
#ifdef DCLIST_MOVE_TAIL_TO_HEAD
BOOL dclist_move_tail_to_head(DCLIST* nilp) {
    DCNODE* headp;
    DCNODE* tailp;
    headp = nilp->nextp;
    tailp = nilp->prevp;
    if (headp!=nilp && tailp!=nilp) {
        tailp->prevp->nextp = nilp;
        nilp->prevp = tailp->prevp;
        tailp->prevp = nilp;
        tailp->nextp = headp;
        nilp->nextp = tailp;
        headp->prevp = tailp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_move_iter_to_head
 * Time complex. : O(1)
 * Description   : Moves any given node to the head of the circular
 *                 doubly linked list, i.e. one after the satellite
 *                 node.
 *                 References to the iterp node are removed and replaced
 *                 by refereces to the head node.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: iterp is sentinel node, cannot proceed
 *                 TRUE:  iterp moved sucessfully to the head
 **********************************************************************/
#ifdef DCLIST_MOVE_ITER_TO_HEAD
BOOL dclist_move_iter_to_head(DCLIST* nilp, DCITER* iterp) {
    if (iterp != nilp) {
        iterp->prevp->nextp = iterp->nextp;
        iterp->nextp->prevp = iterp->prevp;
        iterp->prevp = nilp;
        iterp->nextp = nilp->nextp;
        nilp->nextp->prevp = iterp;
        nilp->nextp = iterp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_move_iter_to_tail
 * Time complex. : O(1)
 * Description   : Moves any given circular doubly linked list node to
 *                 the tail of the list, i.e. one before the satellite
 *                 node.
 *                 References to the iterp node are removed and replaced
 *                 by references to the tail node.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: iterp is sentinel node, cannot proceed
 *                 TRUE:  iterp moved successfully to the tail
 **********************************************************************/
#ifdef DCLIST_MOVE_ITER_TO_TAIL
BOOL dclist_move_iter_to_tail(DCLIST* nilp, DCITER* iterp) {
    if (iterp != nilp) {
        iterp->prevp->nextp = iterp->nextp;
        iterp->nextp->prevp = iterp->prevp;
        iterp->prevp = nilp->prevp;
        iterp->nextp = nilp;
        nilp->prevp->nextp = iterp;
        nilp->prevp = iterp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_insert_before_iter
 * Time complex. : O(1) with heap allocation
 * Description   : Creates a new circular doubly linked list node in
 *                 heap.
 *                 The node is inserted before the place where iterp
 *                 points to.
 * Parameters    : iterp:   iterator (pointer) to any given node
 *                 satp:    pointer to satellite data
 *                 satsize: size of the satellite data in bytes
 * Return values : FALSE:   memory allocation failed, node not created
 *                 TRUE:    node successfully inserted before the iter
 **********************************************************************/
#ifdef DCLIST_INSERT_BEFORE_ITER
BOOL dclist_insert_before_iter(DCITER* iterp, void* satp,
UINT satsize) {
    DCNODE* new_node;
    new_node = (DCNODE*)malloc(sizeof(DCNODE)+satsize);
    if (new_node) {
        new_node->prevp = iterp->prevp;
        new_node->nextp = iterp;
        iterp->prevp->nextp = new_node;
        iterp->prevp = new_node;
        memcpy(new_node+1, satp, satsize);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_insert_after_iter
 * Time complex. : O(1) with heap allocation
 * Description   : Creates a new circular doubly linked list node in
 *                 heap.
 *                 The node is inserted after the place where iterp
 *                 points to.
 * Parameters    : iterp:   iterator (pointer) to any given node
 *                 satp:    pointer to satellite data
 *                 satsize: size of the satellite data in bytes
 * Return values : FALSE:   memory allocation failed, node not created
 *                 TRUE:    node successfully inserted after the iter
 **********************************************************************/
#ifdef DCLIST_INSERT_AFTER_ITER
BOOL dclist_insert_after_iter(DCITER* iterp, void* satp, UINT satsize) {
    DCNODE* new_node;
    new_node = (DCNODE*)malloc(sizeof(DCNODE)+satsize);
    if (new_node) {
        new_node->prevp = iterp;
        new_node->nextp = iterp->nextp;
        iterp->nextp->prevp = new_node;
        iterp->nextp = new_node;
        memcpy(new_node+1, satp, satsize);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_erase_one_at_iter
 * Time complex. : O(1) with heap deallocation
 * Description   : In a circular doubly linked list, erases the node
 *                 where iterp points to.
 *                 The allocated memory in heap is freed.
 * Parameters    : nilp:  pointer to the initialized sentinel node
 *                 iterp: iterator (pointer) to any given node
 * Return values : FALSE: iterp is sentinel node, cannot proceed
 *                 TRUE:  iterp erased successfully
 **********************************************************************/
#ifdef DCLIST_ERASE_ONE_AT_ITER
BOOL dclist_erase_one_at_iter(DCLIST* nilp, DCITER* iterp) {
    if (iterp != nilp) {
        iterp->prevp->nextp = iterp->nextp;
        iterp->nextp->prevp = iterp->prevp;
        free(iterp);
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_extract_array_screate
 * Time complex. : O(n) with heap allocation +
 *                 O(n) traversing,
 *                 where n is the number of nodes in the list
 * Description   : Extracts the data stored in a linear memory area
 *                 to a circular doubly linked list.
 *                 The list to be created has nelems number of nodes,
 *                 each containing satsize amount of user data
 *                 (satsize*nelems is the maximum size of the linear
 *                 memory area).
 *                 See also dclist_screate.
 * Parameters    : nilp:    pointer to the initialized sentinel node
 *                 memory:  pointer to memory to be extracted
 *                 nelems:  number of elements in the array
 *                 satsize: size of the satellite data in bytes
 * Return values : FALSE:   memory allocation failed, missing nodes
 *                 TRUE:    array extract successfully to a list
 **********************************************************************/
#ifdef DCLIST_EXTRACT_ARRAY_SCREATE
BOOL dclist_extract_array_screate(DCLIST* nilp, void* memory,
UINT nelems, UINT satsize) {
    unsigned char* memep;
    unsigned char* memp = memory;
    dclist_screate(nilp);
    memep = memp + (satsize*nelems);
    while (memp < memep) {
        if (!dclist_push_back(nilp,memp,satsize))
            return FALSE;
        memp += satsize;
    }
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : dclist_extract_array_hcreate
 * Time complex. : O(1) with heap allocation +
 *                 O(n) with heap allocation +
 *                 O(n) traversing,
 *                 where n is the number of nodes in the list
 * Description   : Extracts the data stored in a linear memory area to
 *                 circular doubly linked list.
 *                 The list to be created has nelems number of nodes,
 *                 each containing satsize amount of user data
 *                 (satsize*nelems is the maximum size of the linear
 *                 memory area).
 *                 See also dclist_hcreate.
 * Parameters    : nilpp:    pointer to the initialized sentinel node
 *                 memory:   pointer to memory to be extracted
 *                 nelems:   number of elements in the array
 *                 satsize:  size of the satellite data in bytes
 * Return values : FALSE:    memory allocation failed, missing nodes
 *                           if nilpp is not zero, no list if nilpp
 *                           is zero
 *                 TRUE:     array extracted successfully to a list
 **********************************************************************/
#ifdef DCLIST_EXTRACT_ARRAY_HCREATE
BOOL dclist_extract_array_hcreate(DCLIST** nilpp, void* memory,
UINT nelems, UINT satsize) {
    DCLIST* new_list;
    unsigned char* memep;
    unsigned char* memp = memory;
    new_list = dclist_hcreate();
    if (!new_list) {
        *nilpp = NULL;
        return FALSE;
    }
    memep = memp + (satsize*nelems);
    while (memp < memep) {
        if (!dclist_push_back(new_list,memp,satsize)) {
            *nilpp = new_list;
            return FALSE;
        }
        memp += satsize;
    }
    *nilpp = new_list;
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : dclist_extract_list
 * Time complex. : O(n) + O(n), where n is the number of nodes in a list
 * Description   : Extracts data stored in a circular doubly linked list
 *                 to a linear memory area.
 * Parameters    : nilp:    pointer to the initialized sentinel node
 *                 memory:  pointer to memory to be extracted
 *                 satsize: size of the satellite data in bytes
 * Return values : None
 **********************************************************************/
#ifdef DCLIST_EXTRACT_LIST
void dclist_extract_list(DCLIST* nilp, void* memory, UINT satsize) {
    UINT memsize;
    DCITER* curnode;
    unsigned char* memep;
    unsigned char* memsp;
    unsigned char* memp = memory;
    memsize = dclist_count_nodes(nilp) * satsize;
    curnode = nilp;
    memsp = memp;
    memep = memp + memsize;
    while (memp < memep) {
        curnode = curnode->nextp;
        memcpy(memp, curnode+1, satsize);
        memp += satsize;
    }
}
#endif

/***********************************************************************
 * Function      : dclist_extract_list_alloc
 * Time complex. : O(1) with heap allocation +
 *                 O(n) + O(n), where n is the number of nodes in a list
 * Description   : Extracts data stored in a circular doubly linked list
 *                 to a linear memory area.
 *                 Note that this function only supports lists having
 *                 the same amount of satellite data.
 * Parameters    : nilp:     pointer to the initialized sentinel node
 *                 satsize:  size of the satellite data in bytes
 * Return values : Non-NULL: pointer to the new linear memory area
 *                 NULL:     memory allocation failed, no linear area
 **********************************************************************/
#ifdef DCLIST_EXTRACT_LIST_ALLOC
void* dclist_extract_list_alloc(DCLIST* nilp, UINT satsize) {
    UINT memsize;
    DCITER* curnode;
    unsigned char* memp;
    unsigned char* memep;
    unsigned char* memsp;
    memsize = dclist_count_nodes(nilp) * satsize;
    memp = malloc(memsize);
    if (!memp)
        return NULL;
    curnode = nilp;
    memsp = memp;
    memep = memp + memsize;
    while (memp < memep) {
        curnode = curnode->nextp;
        memcpy(memp, curnode+1, satsize);
        memp += satsize;
    }
    return memsp;
}
#endif

/***********************************************************************
 * Function      : dclist_reverse
 * Time complex. : O(n)
 * Description   : Reverses a doubly linked list
 * Parameters    : nilp:   pointer to the initialized sentinel node
 * Return values : None
 **********************************************************************/
#ifdef DCLIST_REVERSE
void dclist_reverse(DCLIST* nilp) {
    DCLIST* tmp1p;
    DCLIST* headp;
    headp = nilp->nextp;
    while (headp != nilp) {
        tmp1p = headp->nextp;
        headp->nextp = headp->prevp;
        headp->prevp = tmp1p;
        headp = tmp1p;
    }
    tmp1p = nilp->nextp;
    nilp->nextp = nilp->prevp;
    nilp->prevp = tmp1p;
}
#endif

/***********************************************************************
 * Function      : dclist_iterate_next
 * Time complex. : O(1)
 * Description   : Iterates an iterator to the next node of a circular
 *                 doubly linked list.
 * Parameters    : nilp:   pointer to the initialized sentinel node
 *                 iterpp: pointer to an iterator
 * Return values : FALSE:  next node was sentinel, no iteration done
 *                 TRUE:   iterator iterated successfully
 **********************************************************************/
#ifdef DCLIST_ITERATE_NEXT
BOOL dclist_iterate_next(DCLIST* nilp, DCITER** iterpp) {
    if ((*iterpp)->nextp != nilp) {
        *iterpp = (*iterpp)->nextp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_iterate_prev
 * Time complex. : O(1)
 * Description   : Iterates an iterator to the previous node of a
 *                 circular doubly linked list.
 * Parameters    : nilp:   pointer to the initialized sentinel node
 *                 iterpp: pointer to an iterator
 * Return values : FALSE:  previous node was sentinel, no iteration done
 *                 TRUE:   iterator iterated successfully
 **********************************************************************/
#ifdef DCLIST_ITERATE_PREV
BOOL dclist_iterate_prev(DCLIST* nilp, DCITER** iterpp) {
    if ((*iterpp)->prevp != nilp) {
        *iterpp = (*iterpp)->prevp;
        return TRUE;
    }
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_iterate_first
 * Time complex. : O(1)
 * Description   : Sets an iterator to the first (head) node of a
 *                 circular doubly linked list.
 * Parameters    : nilp:   pointer to the initialized sentinel node
 *                 iterpp: pointer to an iterator
 * Return values : FALSE:  first node does not exist, iteration not done
 *                 TRUE:   iterator set to head successfully
 **********************************************************************/
#ifdef DCLIST_ITERATE_FIRST
BOOL dclist_iterate_first(DCLIST* nilp, DCITER** iterpp) {
    if (nilp->nextp != nilp) {
        *iterpp = nilp->nextp;
        return TRUE;
    }
    *iterpp = nilp;
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_iterate_last
 * Time complex. : O(1)
 * Description   : Sets an iterator to the last (tail) node of a
 *                 circular doubly linked list.
 * Parameters    : nilp:   pointer to the initialized sentinel node
 *                 iterpp: pointer to an iterator
 * Return values : FALSE:  last node does not exist, iteration not done
 *                 TRUE:   iterator set to tail successfully
 **********************************************************************/
#ifdef DCLIST_ITERATE_LAST
BOOL dclist_iterate_last(DCLIST* nilp, DCITER** iterpp) {
    if (nilp->prevp != nilp) {
        *iterpp = nilp->prevp;
        return TRUE;
    }
    *iterpp = nilp;
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_iterate_nth_next
 * Time complex. : O(n), where n is nth node in the list
 * Description   : Iterates an iterator to the next node of a circular
 *                 doubly linked list by a predefined count.
 * Parameters    : nilp:   pointer to the initialized sentinel node
 *                 iterpp: pointer to an iterator (must be set)
 *                 nth:    how many iterations to do
 * Return values : FALSE:  next node was sentinel, iteration stopped
 *                 TRUE:   iterated successfully to the nth node
 **********************************************************************/
#ifdef DCLIST_ITERATE_NTH_NEXT
BOOL dclist_iterate_nth_next(DCLIST* nilp, DCITER** iterpp, UINT nth) {
    DCNODE* curnode;
    curnode = *iterpp;
    while (nth > 0) {
        curnode = curnode->nextp;
        if (curnode == nilp)
            return FALSE;
        --nth;
    }
    *iterpp = curnode;
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : dclist_iterate_nth_prev
 * Time complex. : O(n), where n is nth node in the list
 * Description   : Iterates an iterator to the previous node of a
 *                 circular doubly linked list by a predefined count.
 * Parameters    : nilp:   pointer to the initialized sentinel node
 *                 iterpp: pointer to an iterator (must be set)
 *                 nth:    how many iterations to do
 * Return values : FALSE:  previous node was sentinel, iteration stopped
 *                 TRUE:   iterated successfully to the nth node
 **********************************************************************/
#ifdef DCLIST_ITERATE_NTH_PREV
BOOL dclist_iterate_nth_prev(DCLIST* nilp, DCITER** iterpp, UINT nth) {
    DCNODE* curnode;
    curnode = *iterpp;
    while (nth > 0) {
        curnode = curnode->prevp;
        if (curnode == nilp)
            return FALSE;
        --nth;
    }
    *iterpp = curnode;
    return TRUE;
}
#endif

/***********************************************************************
 * Function      : dclist_count_nodes
 * Time complex. : O(n), where n is number of nodes in the list
 * Description   : Counts the number of nodes in a circular doubly
 *                 linked list.
 * Parameters    : nilp: pointer to the initialized sentinel node
 * Return values : Number of nodes in the list
 **********************************************************************/
#ifdef DCLIST_COUNT_NODES
UINT dclist_count_nodes(DCLIST* nilp) {
    UINT nodes;
    DCNODE* curnode;
    nodes = 0;
    curnode = nilp->nextp;
    while (curnode != nilp) {
        curnode = curnode->nextp;
        ++nodes;
    }
    return nodes;
}
#endif

/***********************************************************************
 * Function      : dclist_find_first
 * Time complex. : O(n), where n is the first matching node
 * Description   : In a circular doubly linked list, finds the first
 *                 node fulfilling a certain condition.
 *                 The condition checker must be an external compare
 *                 function which receives pointer to the satellite data
 *                 of a node currently iterated to.
 *                 The external compare function must return non-zero
 *                 when to stop traverse the list, zero to continue
 *                 traversing. It can also explicitly command the
 *                 iteration to stop when it is known a match can't
 *                 occur in subsequent iterations.
 * Parameters    : nilp:    pointer to the initialized sentinel node
 *                 iterpp:  pointer to an iterator (must be set)
 *                 compare: pointer to the external compare function
 * Return values : FALSE:   node not found, iterpp is the last node
 *                 TRUE:    node found, iterpp is the matching node
 **********************************************************************/
#ifdef DCLIST_FIND_FIRST
BOOL dclist_find_first(DCLIST* nilp, DCITER** iterpp,
BOOL (*compare)(void*,BOOL*)) {
    BOOL stop;
    DCNODE* curnode;
    DCNODE* nextnode;
    stop = FALSE;
    curnode = *iterpp;
    while (curnode != nilp) {
        nextnode = curnode->nextp;
        if (compare(curnode+1,&stop)) {
            *iterpp = curnode;
            return TRUE;
        }
        if (stop) {
            *iterpp = curnode;
            return FALSE;
        }
        curnode = nextnode;
    }
    *iterpp = nilp;
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_find_last
 * Time complex. : O(n), where n is the last matching node
 * Description   : In a circular doubly linked list, finds the last
 *                 node fulfilling a certain condition.
 *                 The condition checker must be an external compare
 *                 function which receives pointer to the satellite data
 *                 of a node currently iterated to.
 *                 The external compare function must return non-zero
 *                 when to stop traverse the list, zero to continue
 *                 traversing. It can also explicitly command the
 *                 iteration to stop when it is known a match can't
 *                 occur in subsequent iterations.
 * Parameters    : nilp:    pointer to the initialized sentinel node
 *                 iterpp:  pointer to an iterator (must be set)
 *                 compare: pointer to the external compare function
 * Return values : FALSE:   node not found, iterpp is the first node
 *                 TRUE:    node found, iterpp is the matching node
 **********************************************************************/
#ifdef DCLIST_FIND_LAST
BOOL dclist_find_last(DCLIST* nilp, DCITER** iterpp,
BOOL (*compare)(void*,BOOL*)) {
    BOOL stop;
    DCNODE* curnode;
    DCNODE* prevnode;
    stop = FALSE;
    curnode = *iterpp;
    while (curnode != nilp) {
        prevnode = curnode->prevp;
        if (compare(curnode+1,&stop)) {
            *iterpp = curnode;
            return TRUE;
        }
        if (stop) {
            *iterpp = curnode;
            return FALSE;
        }
        curnode = prevnode;
    }
    *iterpp = nilp;
    return FALSE;
}
#endif

/***********************************************************************
 * Function      : dclist_clear
 * Time complex. : O(n) traversing +
 *                 O(n) with heap deallocation,
 *                 where n is the number of nodes in the list
 * Description   : Clears a circular doubly linked list.
 *                 All but the sentinel node are freed from the heap.
 *                 Note: after deallocation the list is ready for reuse.
 * Parameters    : nilp: pointer to the initialized sentinel node
 * Return values : None
 **********************************************************************/
/* Clearing the list */
#ifdef DCLIST_CLEAR
void dclist_clear(DCLIST* nilp) {
    DCNODE* oldnode;
    DCNODE* curnode;
    curnode = nilp->nextp;
    while (curnode != nilp) {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    }
    nilp->nextp = nilp;
    nilp->prevp = nilp;
}
#endif

/***********************************************************************
 * Function      : dclist_clear_advanced
 * Time complex. : O(n) traversing +
 *                 O(n) with auxiliary heap deallocation +
 *                 O(n) with heap deallocation,
 *                 where n is the number of nodes in the list
 * Description   : Clears a circular doubly linked list.
 *                 All but the sentinel node are freed from the heap.
 *                 For all but the sentinel node an external dealloc
 *                 function is called which receives a pointer to the
 *                 satellite data of the node currently iterated to.
 *                 Note: after deallocation the list is ready for reuse.
 * Parameters    : nilp:    pointer to the initialized sentinel node
 *                 dealloc: pointer to the external dealloc function
 * Return values : None
 **********************************************************************/
#ifdef DCLIST_CLEAR_ADVANCED
void dclist_clear_advanced(DCLIST* nilp, void (*dealloc)(void*)) {
    DCNODE* oldnode;
    DCNODE* curnode;
    curnode = nilp->nextp;
    while (curnode != nilp) {
        oldnode = curnode;
        curnode = curnode->nextp;
        dealloc(oldnode+1);
        free(oldnode);
    }
    nilp->nextp = nilp;
    nilp->prevp = nilp;
}
#endif

/***********************************************************************
 * Function      : dclist_destroy
 * Time complex. : O(n+1) traversing +
 *                 O(n+1) with heap deallocation,
 *                 where n is the number of nodes in a list
 * Description   : Destroys a circular doubly linked list.
 *                 All nodes, including the sentinel, are freed from the
 *                 heap.
 * Parameters    : nilp: pointer to the initialized sentinel node
 * Return values : None
 **********************************************************************/
#ifdef DCLIST_DESTROY
void dclist_destroy(DCLIST* nilp) {
    DCNODE* oldnode;
    DCNODE* curnode;
    curnode = nilp;
    do {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    } while (curnode != nilp);
}
#endif

/***********************************************************************
 * Function      : dclist_destroy_advanced
 * Time complex. : O(n+1) traversing +
 *                 O(n) with auxiliary heap deallocation +
 *                 O(n+1) with heap deallocation,
 *                 where n is the number of nodes in a list
 * Description   : Destroys a circular doubly linked list.
 *                 All nodes, including the sentinel, are freed from the
 *                 heap.
 *                 For all but the sentinel node an external dealloc
 *                 function is called which receives a pointer to the
 *                 satellite data of the node currently iterated to.
 * Parameters    : nilp:    pointer to the initialized sentinel node
 *                 dealloc: pointer to the external dealloc function
 * Return values : None
 **********************************************************************/
#ifdef DCLIST_DESTROY_ADVANCED
void dclist_destroy_advanced(DCLIST* nilp, void (*dealloc)(void*)) {
    DCNODE* oldnode;
    DCNODE* curnode;
    curnode = nilp->nextp;
    while (curnode != nilp) {
        oldnode = curnode;
        curnode = curnode->nextp;
        dealloc(oldnode+1);
        free(oldnode);
    }
    free(nilp);
}
#endif
