#include <stdio.h>
#include "pddclist.h"

BOOL user_compare(void*);


int main(void) {
    int mydata;
    int* mydatap;
    void* memory;
    SCBPOOL scbpool;
    SCBPOOL* scbpoolp;
    PDDCLIST list;
    PDDCLIST* listp;
    PDDCITER* iterp;
    PDDCITER* iterbase;
    
    listp = &list;
    mydatap = &mydata;
    scbpoolp = &scbpool;
    scbpool_screate(scbpoolp, 10000, sizeof(PDDCNODE)+sizeof(int));
    pddclist_screate(listp, scbpoolp);
    iterbase = (PDDCITER*)listp;
    
    mydata = 1;
    pddclist_push_back(listp, mydatap);
    mydata = 2;
    pddclist_push_back(listp, mydatap);
    mydata = 3;
    pddclist_push_back(listp, mydatap);
    
    iterp = iterbase->nextp;  /* first (iterbase is sentinel) */
    pddclist_move_iter_to_tail(listp, iterp);
    
    pddclist_move_head_to_tail(listp);
    pddclist_move_tail_to_head(listp);
    
    memory = pddclist_extract_list_alloc(listp);
    pddclist_clear(listp);
    pddclist_extract_array_screate(listp, scbpoolp, memory, 3);
    free(memory);
    
    if (pddclist_iterate_first(listp,&iterp)) {
        do {
            printf("%d\n", *(int*)(iterp+1));
        } while (pddclist_iterate_next(listp,&iterp));
    }
    
    pddclist_clear(listp);
    
    return 0;
}

BOOL user_compare(void* satp) {
    if (*(int*)satp == 2)
        return TRUE;
    return FALSE;
}
