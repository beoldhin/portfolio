#ifndef _PDCLIST_H
#define _PDCLIST_H

#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"
#include "/home/olli/src/algo/pool/scpool.h"

typedef struct pdcnode {
    struct pdcnode* prevp;
    struct pdcnode* nextp;
} PDCNODE,PDCITER;

typedef struct pdclist {
    PDCNODE list;  /* keep this the very first */
    UINT satsize;
    SCPOOL* scpoolp;
} PDCLIST;

#ifdef TEST_COMPILATION
  #define PDCLIST_SCREATE
  #define PDCLIST_HCREATE
  #define PDCLIST_PUSH_BACK
  #define PDCLIST_PUSH_FRONT
  #define PDCLIST_POP_BACK
  #define PDCLIST_POP_FRONT
  #define PDCLIST_SWAP_HEAD_AND_TAIL
  #define PDCLIST_SWAP_HEAD_AND_ITER
  #define PDCLIST_SWAP_ITER_AND_TAIL
  #define PDCLIST_SWAP_ITER_AND_ITER
  #define PDCLIST_MOVE_HEAD_TO_TAIL
  #define PDCLIST_MOVE_TAIL_TO_HEAD
  #define PDCLIST_MOVE_ITER_TO_HEAD
  #define PDCLIST_MOVE_ITER_TO_TAIL
  #define PDCLIST_INSERT_BEFORE_ITER
  #define PDCLIST_INSERT_AFTER_ITER
  #define PDCLIST_ERASE_ONE_AT_ITER
  #define PDCLIST_EXTRACT_ARRAY_SCREATE
  #define PDCLIST_EXTRACT_ARRAY_HCREATE
  #define PDCLIST_EXTRACT_LIST
  #define PDCLIST_EXTRACT_LIST_ALLOC
  #define PDCLIST_ITERATE_NEXT
  #define PDCLIST_ITERATE_PREV
  #define PDCLIST_ITERATE_FIRST
  #define PDCLIST_ITERATE_LAST
  #define PDCLIST_ITERATE_NTH_NEXT
  #define PDCLIST_ITERATE_NTH_PREV
  #define PDCLIST_COUNT_NODES
  #define PDCLIST_FIND_FIRST
  #define PDCLIST_FIND_LAST
  #define PDCLIST_RESET
  #define PDCLIST_CLEAR
  #define PDCLIST_CLEAR_ADVANCED
  #define PDCLIST_DESTROY
  #define PDCLIST_DESTROY_ADVANCED
#endif

#ifdef PDCLIST_SCREATE
void pdclist_screate(PDCLIST*, SCPOOL*);
#endif
#ifdef PDCLIST_HCREATE
PDCLIST* pdclist_hcreate(SCPOOL*);
#endif
#ifdef PDCLIST_PUSH_BACK
BOOL pdclist_push_back(PDCLIST*, void*);
#endif
#ifdef PDCLIST_PUSH_FRONT
BOOL pdclist_push_front(PDCLIST*, void*);
#endif
#ifdef PDCLIST_POP_BACK
BOOL pdclist_pop_back(PDCLIST*);
#endif
#ifdef PDCLIST_POP_FRONT
BOOL pdclist_pop_front(PDCLIST*);
#endif
#ifdef PDCLIST_SWAP_HEAD_AND_TAIL
BOOL pdclist_swap_head_and_tail(PDCLIST*);
#endif
#ifdef PDCLIST_SWAP_HEAD_AND_ITER
BOOL pdclist_swap_head_and_iter(PDCLIST*, PDCITER*);
#endif
#ifdef PDCLIST_SWAP_ITER_AND_TAIL
BOOL pdclist_swap_iter_and_tail(PDCLIST*, PDCITER*);
#endif
#ifdef PDCLIST_SWAP_ITER_AND_ITER
BOOL pdclist_swap_iter_and_iter(PDCLIST*, PDCITER*, PDCITER*);
#endif
#ifdef PDCLIST_MOVE_HEAD_TO_TAIL
BOOL pdclist_move_head_to_tail(PDCLIST*);
#endif
#ifdef PDCLIST_MOVE_TAIL_TO_HEAD
BOOL pdclist_move_tail_to_head(PDCLIST*);
#endif
#ifdef PDCLIST_MOVE_ITER_TO_HEAD
BOOL pdclist_move_iter_to_head(PDCLIST*, PDCITER*);
#endif
#ifdef PDCLIST_MOVE_ITER_TO_TAIL
BOOL pdclist_move_iter_to_tail(PDCLIST*, PDCITER*);
#endif
#ifdef PDCLIST_INSERT_BEFORE_ITER
BOOL pdclist_insert_before_iter(PDCLIST*, PDCITER*, void*);
#endif
#ifdef PDCLIST_INSERT_AFTER_ITER
BOOL pdclist_insert_after_iter(PDCLIST*, PDCITER*, void*);
#endif
#ifdef PDCLIST_ERASE_ONE_AT_ITER
BOOL pdclist_erase_one_at_iter(PDCLIST*, PDCITER*);
#endif
#ifdef PDCLIST_EXTRACT_ARRAY_SCREATE
BOOL pdclist_extract_array_screate(PDCLIST*, SCPOOL*, void*, UINT);
#endif
#ifdef PDCLIST_EXTRACT_ARRAY_HCREATE
BOOL pdclist_extract_array_hcreate(PDCLIST**, SCPOOL*, void*, UINT);
#endif
#ifdef PDCLIST_EXTRACT_LIST
void pdclist_extract_list(PDCLIST*, void*);
#endif
#ifdef PDCLIST_EXTRACT_LIST_ALLOC
void* pdclist_extract_list_alloc(PDCLIST*);
#endif
#ifdef PDCLIST_ITERATE_NEXT
BOOL pdclist_iterate_next(PDCLIST*, PDCITER**);
#endif
#ifdef PDCLIST_ITERATE_PREV
BOOL pdclist_iterate_prev(PDCLIST*, PDCITER**);
#endif
#ifdef PDCLIST_ITERATE_FIRST
BOOL pdclist_iterate_first(PDCLIST*, PDCITER**);
#endif
#ifdef PDCLIST_ITERATE_LAST
BOOL pdclist_iterate_last(PDCLIST*, PDCITER**);
#endif
#ifdef PDCLIST_ITERATE_NTH_NEXT
BOOL pdclist_iterate_nth_next(PDCLIST*, PDCITER**, UINT);
#endif
#ifdef PDCLIST_ITERATE_NTH_PREV
BOOL pdclist_iterate_nth_prev(PDCLIST*, PDCITER**, UINT);
#endif
#ifdef PDCLIST_COUNT_NODES
UINT pdclist_count_nodes(PDCLIST*);
#endif
#ifdef PDCLIST_FIND_FIRST
BOOL pdclist_find_first(PDCLIST*, PDCITER**, BOOL (*)(void*,BOOL*));
#endif
#ifdef PDCLIST_FIND_LAST
BOOL pdclist_find_last(PDCLIST*, PDCITER**, BOOL (*)(void*,BOOL*));
#endif
#ifdef PDCLIST_RESET
void pdclist_reset(PDCLIST*);
#endif
#ifdef PDCLIST_CLEAR
void pdclist_clear(PDCLIST*);
#endif
#ifdef PDCLIST_CLEAR_ADVANCED
void pdclist_clear_advanced(PDCLIST*, void (*)(void*));
#endif
#ifdef PDCLIST_DESTROY
void pdclist_destroy(PDCLIST*);
#endif
#ifdef PDCLIST_DESTROY_ADVANCED
void pdclist_destroy_advanced(PDCLIST*, void (*)(void*));
#endif

#endif  /* _PDCLIST_H */
