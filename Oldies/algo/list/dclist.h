#ifndef _DCLIST_H
#define _DCLIST_H

#include <stdlib.h>
#include "/home/olli/src/algo/defines/nlimits.h"

#ifndef TEST_COMPILATION
#include "/home/olli/src/incs/inc_current.h"
#endif

typedef struct dcnode {
    struct dcnode* prevp;
    struct dcnode* nextp;
} DCNODE,DCLIST,DCITER;

#ifdef TEST_COMPILATION
  #define DCLIST_SCREATE
  #define DCLIST_HCREATE
  #define DCLIST_PUSH_BACK
  #define DCLIST_PUSH_FRONT
  #define DCLIST_POP_BACK
  #define DCLIST_POP_FRONT
  #define DCLIST_SWAP_HEAD_AND_TAIL
  #define DCLIST_SWAP_HEAD_AND_ITER
  #define DCLIST_SWAP_ITER_AND_TAIL
  #define DCLIST_SWAP_ITER_AND_ITER
  #define DCLIST_MOVE_HEAD_TO_TAIL
  #define DCLIST_MOVE_TAIL_TO_HEAD
  #define DCLIST_MOVE_ITER_TO_HEAD
  #define DCLIST_MOVE_ITER_TO_TAIL
  #define DCLIST_INSERT_BEFORE_ITER
  #define DCLIST_INSERT_AFTER_ITER
  #define DCLIST_ERASE_ONE_AT_ITER
  #define DCLIST_EXTRACT_ARRAY_SCREATE
  #define DCLIST_EXTRACT_ARRAY_HCREATE
  #define DCLIST_EXTRACT_LIST
  #define DCLIST_EXTRACT_LIST_ALLOC
  #define DCLIST_REVERSE
  #define DCLIST_ITERATE_NEXT
  #define DCLIST_ITERATE_PREV
  #define DCLIST_ITERATE_FIRST
  #define DCLIST_ITERATE_LAST
  #define DCLIST_ITERATE_NTH_NEXT
  #define DCLIST_ITERATE_NTH_PREV
  #define DCLIST_COUNT_NODES
  #define DCLIST_FIND_FIRST
  #define DCLIST_FIND_LAST
  #define DCLIST_CLEAR
  #define DCLIST_CLEAR_ADVANCED
  #define DCLIST_DESTROY
  #define DCLIST_DESTROY_ADVANCED
#endif

#ifdef DCLIST_SCREATE
void dclist_screate(DCLIST*);
#endif
#ifdef DCLIST_HCREATE
DCLIST* dclist_hcreate(void);
#endif
#ifdef DCLIST_PUSH_BACK
BOOL dclist_push_back(DCLIST*, void*, UINT);
#endif
#ifdef DCLIST_PUSH_FRONT
BOOL dclist_push_front(DCLIST*, void*, UINT);
#endif
#ifdef DCLIST_POP_BACK
BOOL dclist_pop_back(DCLIST*);
#endif
#ifdef DCLIST_POP_FRONT
BOOL dclist_pop_front(DCLIST*);
#endif
#ifdef DCLIST_SWAP_HEAD_AND_TAIL
BOOL dclist_swap_head_and_tail(DCLIST*);
#endif
#ifdef DCLIST_SWAP_HEAD_AND_ITER
BOOL dclist_swap_head_and_iter(DCLIST*, DCITER*);
#endif
#ifdef DCLIST_SWAP_ITER_AND_TAIL
BOOL dclist_swap_iter_and_tail(DCLIST*, DCITER*);
#endif
#ifdef DCLIST_SWAP_ITER_AND_ITER
BOOL dclist_swap_iter_and_iter(DCLIST*, DCITER*, DCITER*);
#endif
#ifdef DCLIST_MOVE_HEAD_TO_TAIL
BOOL dclist_move_head_to_tail(DCLIST*);
#endif
#ifdef DCLIST_MOVE_TAIL_TO_HEAD
BOOL dclist_move_tail_to_head(DCLIST*);
#endif
#ifdef DCLIST_MOVE_ITER_TO_HEAD
BOOL dclist_move_iter_to_head(DCLIST*, DCITER*);
#endif
#ifdef DCLIST_MOVE_ITER_TO_TAIL
BOOL dclist_move_iter_to_tail(DCLIST*, DCITER*);
#endif
#ifdef DCLIST_INSERT_BEFORE_ITER
BOOL dclist_insert_before_iter(DCITER*, void*, UINT);
#endif
#ifdef DCLIST_INSERT_AFTER_ITER
BOOL dclist_insert_after_iter(DCITER*, void*, UINT);
#endif
#ifdef DCLIST_ERASE_ONE_AT_ITER
BOOL dclist_erase_one_at_iter(DCITER*, DCLIST*);
#endif
#ifdef DCLIST_EXTRACT_ARRAY_SCREATE
BOOL dclist_extract_array_screate(DCLIST*, void*, UINT, UINT);
#endif
#ifdef DCLIST_EXTRACT_ARRAY_HCREATE
BOOL dclist_extract_array_hcreate(DCLIST**, void*, UINT, UINT);
#endif
#ifdef DCLIST_EXTRACT_LIST
void dclist_extract_list(DCLIST*, void*, UINT);
#endif
#ifdef DCLIST_EXTRACT_LIST_ALLOC
void* dclist_extract_list_alloc(DCLIST*, UINT);
#endif
#ifdef DCLIST_REVERSE
void dclist_reverse(DCLIST*);
#endif
#ifdef DCLIST_ITERATE_NEXT
BOOL dclist_iterate_next(DCLIST*, DCITER**);
#endif
#ifdef DCLIST_ITERATE_PREV
BOOL dclist_iterate_prev(DCLIST*, DCITER**);
#endif
#ifdef DCLIST_ITERATE_FIRST
BOOL dclist_iterate_first(DCLIST*, DCITER**);
#endif
#ifdef DCLIST_ITERATE_LAST
BOOL dclist_iterate_last(DCLIST*, DCITER**);
#endif
#ifdef DCLIST_ITERATE_NTH_NEXT
BOOL dclist_iterate_nth_next(DCLIST*, DCITER**, UINT);
#endif
#ifdef DCLIST_ITERATE_NTH_PREV
BOOL dclist_iterate_nth_prev(DCLIST*, DCITER**, UINT);
#endif
#ifdef DCLIST_COUNT_NODES
UINT dclist_count_nodes(DCLIST*);
#endif
#ifdef DCLIST_FIND_FIRST
BOOL dclist_find_first(DCLIST*, DCITER**, BOOL (*)(void*,BOOL*));
#endif
#ifdef DCLIST_FIND_LAST
BOOL dclist_find_last(DCLIST*, DCITER**, BOOL (*)(void*,BOOL*));
#endif
#ifdef DCLIST_CLEAR
void dclist_clear(DCLIST*);
#endif
#ifdef DCLIST_CLEAR_ADVANCED
void dclist_clear_advanced(DCLIST*, void (*)(void*));
#endif
#ifdef DCLIST_DESTROY
void dclist_destroy(DCLIST*);
#endif
#ifdef DCLIST_DESTROY_ADVANCED
void dclist_destroy_advanced(DCLIST*, void (*)(void*));
#endif

#endif  /* _DCLIST_H */
