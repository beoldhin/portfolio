#include "slist.h"


#ifdef SLIST_PUSH_BACK_F
SNODE* slist_push_back_f(SLIST** listp, SNODE** tailpp, void* satp,
UINT satsize) {
    SNODE* new_node = malloc(sizeof(SNODE)+satsize);
    if (new_node) {
        new_node->nextp = NULL;
        memcpy(new_node+1, satp, satsize);
        if (*listp)
            (*tailpp)->nextp = new_node;
        else
            *listp = new_node;
        return new_node;
    }
    return NULL;
}
#endif

#ifdef SLIST_PUSH_BACK_S
SNODE* slist_push_back_s(SLIST** listp, void* satp, UINT satsize) {
    SNODE* curnode;
    SNODE* new_node = malloc(sizeof(SNODE)+satsize);
    if (new_node) {
        new_node->nextp = NULL;
        memcpy(new_node+1, satp, satsize);
        if (*listp) {
            for (curnode=*listp; curnode->nextp; curnode=curnode->nextp);
            curnode->nextp = new_node;
        } else
            *listp = new_node;
        return new_node;
    }
    return NULL;
}
#endif

#ifdef SLIST_PUSH_FRONT
SNODE* slist_push_front(SLIST** listp, void* satp, UINT satsize) {
    SNODE* new_node = malloc(sizeof(SNODE)+satsize);
    if (new_node) {
        if (*listp)
            new_node->nextp = *listp;
        else
            new_node->nextp = NULL;
        *listp = new_node;
        memcpy(new_node+1, satp, satsize);
        return new_node;
    }
    return NULL;
}
#endif

#ifdef SLIST_ITERATE_FIRST
BOOL slist_iterate_first(SLIST** listp, SITER** iterpp) {
    if (*listp) {
        *iterpp = *listp;
        return TRUE;
    }
    return FALSE;
}
#endif

#ifdef SLIST_ITERATE_NEXT
BOOL slist_iterate_next(SLIST** listp, SITER** iterpp) {
    if (*listp && (*iterpp)->nextp) {
        *iterpp = (*iterpp)->nextp;
        return TRUE;
    }
    return FALSE;
}
#endif

#ifdef SLIST_DESTROY
void slist_destroy(SLIST** listp) {
    SNODE* oldnode;
    SNODE* curnode = *listp;
    do {
        oldnode = curnode;
        curnode = curnode->nextp;
        free(oldnode);
    } while (curnode);
    *listp = NULL;
}
#endif

#ifdef SLIST_DESTROY_ADVANCED
void slist_destroy_advanced(SLIST** listp, void (*dealloc)(void*)) {
    SNODE* oldnode;
    SNODE* curnode = *listp;
    do {
        oldnode = curnode;
        curnode = curnode->nextp;
        dealloc(oldnode+1);
        free(oldnode);
    } while (curnode);
    *listp = NULL;
}
#endif
