#include "bst.h"


#ifdef BST_FIND
BSTNODE* bst_find(BSTROOT* bstroot, void* keyp, int (*compare)(void*,void*)) {
    int ret;
    while (bstroot) {
        ret = compare(bstroot+1, keyp);
        if (ret == 0)
            return bstroot;
        if (ret < 0)
            bstroot = bstroot->left;
        else  /* ret > 0 */
            bstroot = bstroot->right;
    }
    return NULL;
}
#endif

#ifdef BST_INSERT
BOOL bst_insert(BSTROOT** bstrootp, void* satp, UINT satsize,
int (*compare)(void*,void*)) {
    int ret;
    BSTNODE* bstnode = *bstrootp;
    BSTNODE** oldnodep = bstrootp;
    while (bstnode) {
        ret = compare(bstnode+1, satp);
        if (ret == 0)
            return FALSE;
        if (ret < 0) {
            oldnodep = &bstnode->left;
            bstnode = bstnode->left;
        } else {  /* ret > 0 */
            oldnodep = &bstnode->right;
            bstnode = bstnode->right;
        }
    }
    bstnode = malloc(sizeof(BSTNODE)+satsize);
    if (!bstnode)
        return FALSE;
    bstnode->left = NULL;
    bstnode->right = NULL;
    memcpy(bstnode+1, satp, satsize);
    *oldnodep = bstnode;
    return TRUE;
}
#endif

#ifdef BST_DELETE
BOOL bst_delete(BSTROOT** bstrootp, void* keyp, int (*compare)(void*,void*)) {
    int ret;
    BSTNODE* iosnode;
    BSTNODE* tmpnode;
    BSTNODE* bstnode = *bstrootp;
    BSTNODE** oldnodep = bstrootp;
    while (bstnode) {
        ret = compare(bstnode+1, keyp);
        if (ret == 0)
            break;
        if (ret < 0) {
            oldnodep = &bstnode->left;
            bstnode = bstnode->left;
        } else {  /* ret > 0 */
            oldnodep = &bstnode->right;
            bstnode = bstnode->right;
        }
    }
    if (!bstnode)
        return FALSE;
    if (bstnode->left) {
        if (bstnode->right) {
            if (bstnode->left->right) {
                iosnode = bstnode->left;
                while (iosnode->right) {
                    tmpnode = iosnode;
                    iosnode = iosnode->right;
                }
                tmpnode->right = iosnode->left;
                iosnode->left = bstnode->left;
                iosnode->right = bstnode->right;
                *oldnodep = iosnode;
                free(bstnode);
                return TRUE;
            }
            bstnode->left->right = bstnode->right;
        }
        *oldnodep = bstnode->left;
    } else {
        *oldnodep = bstnode->right;
    }
    free(bstnode);
    return TRUE;
}
#endif

#ifdef BST_DESTROY
void bst_destroy(BSTROOT* bstroot) {
    if (bstroot) {  /* post order traversal */
        bst_destroy(bstroot->left);
        bst_destroy(bstroot->right);
        free(bstroot);
    }
}
#endif
/* bstroot should be set to NULL after this */

#ifdef BST_DESTROY_ADVANCED
void bst_destroy_advanced(BSTROOT* bstroot, void (*dealloc)(void*)) {
    if (bstroot) {  /* post order traversal */
        bst_destroy(bstroot->left);
        bst_destroy(bstroot->right);
        dealloc(bstroot+1);
        free(bstroot);
    }
}
#endif
/* bstroot should be set to NULL after this */

#ifdef BST_PRINT
void bst_print(BSTROOT* bstroot, int depth, void (*print)(void*,int)) {
    if (bstroot) {
        bst_print(bstroot->right, depth+4, print);
        print(bstroot+1, depth);
        bst_print(bstroot->left, depth+4, print);
    }
}
#endif
