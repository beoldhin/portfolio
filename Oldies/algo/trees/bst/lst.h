#ifndef _LST_H
#define _LST_H

#include <stdio.h>
#include <stdlib.h>
#include "/home/olli/src/algo/list/slist.h"

#ifndef TEST_COMPILATION
#include "/home/olli/src/incs/inc_current.h"
#endif

#define NONE   0
#define LEFT   1
#define RIGHT  2

typedef struct lstnode {
    struct lstnode* left;
    struct lstnode* right;
    SLIST* list;
} LSTNODE,LSTROOT;

#ifdef TEST_COMPILATION
  #define LST_FIND
  #define LST_INSERT
  #define LST_DELETE
  #define LST_DESTROY
  #define LST_DESTROY_ADVANCED
  #define LST_PRINT
#endif

#ifdef LST_FIND
LSTNODE* lst_find(LSTROOT*, void*, int (*)(void*,void*));
#endif
#ifdef LST_INSERT
BOOL lst_insert(LSTROOT**, void*, UINT, int (*)(void*,void*));
#endif
#ifdef LST_DELETE
BOOL lst_delete(LSTROOT**, void*, int (*)(void*,void*));
#endif
#ifdef LST_DESTROY
void lst_destroy(LSTROOT*);
#endif
#ifdef LST_DESTROY_ADVANCED
void lst_destroy_advanced(LSTROOT*, void (*)(void*));
#endif
#ifdef LST_PRINT
void lst_print(LSTROOT*, int, void (*)(void*,int));
#endif

#endif  /* _LST_H */
