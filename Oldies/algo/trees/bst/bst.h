#ifndef _BST_H
#define _BST_H

#include <stdio.h>
#include <stdlib.h>

#include "/home/olli/src/algo/defines/nlimits.h"

#ifndef TEST_COMPILATION
#include "/home/olli/src/incs/inc_current.h"
#endif

#define NONE   0
#define LEFT   1
#define RIGHT  2

typedef struct bstnode {
    struct bstnode* left;
    struct bstnode* right;
} BSTNODE,BSTROOT;

#ifdef TEST_COMPILATION
  #define BST_FIND
  #define BST_INSERT
  #define BST_DELETE
  #define BST_DESTROY
  #define BST_DESTROY_ADVANCED
  #define BST_PRINT
#endif

#ifdef BST_FIND
BSTNODE* bst_find(BSTROOT*, void*, int (*)(void*,void*));
#endif
#ifdef BST_INSERT
BOOL bst_insert(BSTROOT**, void*, UINT, int (*)(void*,void*));
#endif
#ifdef BST_DELETE
BOOL bst_delete(BSTROOT**, void*, int (*)(void*,void*));
#endif
#ifdef BST_DESTROY
void bst_destroy(BSTROOT*);
#endif
#ifdef BST_DESTROY_ADVANCED
void bst_destroy_advanced(BSTROOT*, void (*)(void*));
#endif
#ifdef BST_PRINT
void bst_print(BSTROOT*, int, void (*)(void*,int));
#endif

#endif  /* _BST_H */
