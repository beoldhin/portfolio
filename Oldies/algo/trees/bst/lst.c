#include "lst.h"


#ifdef LST_FIND
LSTNODE* lst_find(LSTROOT* lstroot, void* keyp, int (*compare)(void*,void*)) {
    int ret;
    while (lstroot) {
        ret = compare(lstroot->list+1, keyp);
        if (ret == 0)
            return lstroot;
        if (ret < 0)
            lstroot = lstroot->left;
        else  /* ret > 0 */
            lstroot = lstroot->right;
    }
    return NULL;
}
#endif

#ifdef LST_INSERT
BOOL lst_insert(LSTROOT** lstrootp, void* satp, UINT satsize,
int (*compare)(void*,void*)) {
    int ret;
    BOOL exists = FALSE;
    LSTNODE* lstnode = *lstrootp;
    LSTNODE** oldnodep = lstrootp;
    while (lstnode) {
        ret = compare(lstnode->list+1, satp);
        if (ret == 0) {
            exists = TRUE;
            break;
        }
        if (ret < 0) {
            oldnodep = &lstnode->left;
            lstnode = lstnode->left;
        } else {  /* ret > 0 */
            oldnodep = &lstnode->right;
            lstnode = lstnode->right;
        }
    }
    if (!exists) {
        if (!(lstnode=malloc(sizeof(LSTNODE))))
            return FALSE;
        lstnode->left = NULL;
        lstnode->right = NULL;
        lstnode->list = NULL;
        *oldnodep = lstnode;
    }
    if (!slist_push_back_s(&lstnode->list,satp,satsize))
        return FALSE;
    return TRUE;
}
#endif

#ifdef LST_DELETE
BOOL lst_delete(LSTROOT** lstrootp, void* keyp, int (*compare)(void*,void*)) {
    int ret;
    LSTNODE* iosnode;
    LSTNODE* tmpnode;
    LSTNODE* lstnode = *lstrootp;
    LSTNODE** oldnodep = lstrootp;
    while (lstnode) {
        ret = compare(lstnode->list+1, keyp);
        if (ret == 0)
            break;
        if (ret < 0) {
            oldnodep = &lstnode->left;
            lstnode = lstnode->left;
        } else {  /* ret > 0 */
            oldnodep = &lstnode->right;
            lstnode = lstnode->right;
        }
    }
    if (!lstnode)
        return FALSE;
    if (lstnode->left) {
        if (lstnode->right) {
            if (lstnode->left->right) {
                iosnode = lstnode->left;
                while (iosnode->right) {
                    tmpnode = iosnode;
                    iosnode = iosnode->right;
                }
                tmpnode->right = iosnode->left;
                iosnode->left = lstnode->left;
                iosnode->right = lstnode->right;
                *oldnodep = iosnode;
                slist_destroy(&lstnode->list);
                free(lstnode);
                return TRUE;
            }
            lstnode->left->right = lstnode->right;
        }
        *oldnodep = lstnode->left;
    } else {
        if (lstnode->right) {
            *oldnodep = lstnode->right;
        } else {
            *oldnodep = NULL;
        }
    }
    slist_destroy(&lstnode->list);
    free(lstnode);
    return TRUE;
}
#endif

#ifdef LST_DESTROY
void lst_destroy(LSTROOT* lstroot) {
    if (lstroot) {  /* post order traversal */
        lst_destroy(lstroot->left);
        lst_destroy(lstroot->right);
        slist_destroy(&lstroot->list);
        free(lstroot);
    }
}
#endif
/* lstroot should be set to NULL after this */

#ifdef LST_DESTROY_ADVANCED
void lst_destroy_advanced(LSTROOT* lstroot, void (*dealloc)(void*)) {
    if (lstroot) {  /* post order traversal */
        lst_destroy(lstroot->left);
        lst_destroy(lstroot->right);
        slist_destroy_advanced(&lstroot->list, dealloc);
        free(lstroot);
    }
}
#endif
/* lstroot should be set to NULL after this */

#ifdef LST_PRINT
void lst_print(LSTROOT* lstroot, int depth, void (*print)(void*,int)) {
    SITER* iterp;
    if (lstroot) {
        lst_print(lstroot->right, depth+4, print);
        if (slist_iterate_first(&lstroot->list,&iterp)) {
            do {
                print(iterp+1, depth);
            } while (slist_iterate_next(&lstroot->list,&iterp));
        }
        lst_print(lstroot->left, depth+4, print);
    }
}
#endif
