#include <stdio.h>
#include <time.h>
#include "bst.h"
#include "/home/olli/src/algo/randgen/nreprnd.h"

#define UPLIM   20
#define LOLIM   1
#define SIZE    (UPLIM-LOLIM+1)

int compare(void*, void*);
void print(void*, int);


int main(void) {
    int i;
    UINT32 num;
    MTCOKUS mtc;
    time_t curtime;
    char buffer[100];
    UINT32 list[SIZE];
    BSTROOT* bstroot = NULL;
    
    mtcokus_screate(&mtc);
    mtcokus_seed(&mtc, (unsigned)time(&curtime));
    nreprnd_makelist(&mtc, list, LOLIM, UPLIM);
    for (i=0; i<SIZE-1; ++i) {
        printf("%u, ", list[i]);
    }
    printf("%u\n", list[i]);
    for (i=0; i<SIZE; ++i) {
        bst_insert(&bstroot, &list[i], sizeof(list[0]), compare);
    }
    while (1) {
        bst_print(bstroot, 0, print);
        puts("----------");
        fgets(buffer, 100, stdin);
        if ((num=atoi(buffer)) == 0)
            break;
        puts("----------");
        bst_delete(&bstroot, &num, compare);
    }
    bst_destroy(bstroot);
    
    return 0;
}

int compare(void* satp, void* keyp) {
    if (*(int*)(keyp) < *(int*)(satp))
        return -1;
    if (*(int*)(keyp) > *(int*)(satp))
        return +1;
    return 0;
}

void print(void* satp, int depth) {
    int i;
    for (i=0; i<depth; ++i)
        putchar(' ');
    printf("%u\n", *(UINT32*)(satp));
}
