#include <stdio.h>
#include "lst.h"

#define SIZE 4

int compare(void*, void*);
void print(void*, int);


char Seq[SIZE+1];


int main(void) {
    int seqi = 1;
    LSTROOT* lstroot = NULL;
    Seq[1] = 1;
    while (seqi > 0) {
        memset(Seq+seqi+1, 0x00, (unsigned)(SIZE-seqi));
        lst_insert(&lstroot, Seq+1, SIZE, compare);
        if (Seq[seqi] == SIZE) {
            --seqi;
            ++Seq[seqi];
        } else {
            ++seqi;
            Seq[seqi] = Seq[seqi-1] + 1;
        }
    }
    lst_print(lstroot, 0, print);
    lst_destroy(lstroot);
    return 0;
}

int compare(void* satp, void* keyp) {
    int x;
    int y;
    char* temp;
    char* first = satp;
    char* second = keyp;
    for (x=0,temp=first+SIZE; first<temp; ++first)
        x += *first;
    for (y=0,temp=second+SIZE; second<temp; ++second)
        y += *second;
    if (x < y)
        return -1;
    if (x > y)
        return +1;
    return 0;
}

void print(void* satp, int depth) {
    int i;
    int total;
    char* temp;
    char* first = satp;
    for (i=0; i<depth; ++i)
        putchar(' ');
    for (total=0,temp=first+SIZE; first<temp; ++first) {
        total += *first;
    }
    for (first=satp,temp=first+SIZE; first<temp; ++first) {
        printf("%d", *first);
    }
    printf("[%d]\n", total);
}
