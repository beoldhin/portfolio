#include <time.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void) {
    int i;
    int j;
    int pid;
    int status;
    time_t secs;
    unsigned int slept;
    
    time(&secs);
    srand(secs);
    for (i=0; i<5 ; ++i) {
        if ((pid=fork()) == 0) {
            for (j=0; j<3; ++j) {
                slept = (rand()%10) + 1;
                sleep(slept);
                printf("%d: %d, slept %u\n", getpid(), j, slept);
            }
            return 0;
        }
        printf("new pid %d\n", pid);
    }
    
    while (wait(&status) != -1);
    
    return 0;
}
