/* This is a simple and inefficient server using blocking sockets */

#include <netdb.h>
#include <errno.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BACKLOG 10
#define BUFLEN 80

void sigchld_handler(int);


int main(int argc, char* argv[]) {
    int yes;
    int bytes;
    int sockfd;
    int new_fd;
    int sin_size;
    char tmpbuf[BUFLEN];
    struct hostent* he;
    struct sigaction sa;
    struct sockaddr_in my_addr;
    struct sockaddr_in their_addr;
    
    if (argc != 3) {
        fprintf(stderr, "Invalid number of arguments.\n");
        fprintf(stderr, "Usage: %s <addr> <port>\n", argv[0]);
        return -1;
    }
    
    if ((sockfd=socket(AF_INET,SOCK_STREAM,0)) == -1) {
        perror("socket");
        return -1;
    }
    
    yes = 1;
    if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
        perror("setsockopt");
        return -1;
    }
    
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(atoi(argv[2]));
    if (!(he=gethostbyname(argv[1]))) {
        perror("gethostbyname");
        return -1;
    }
    my_addr.sin_addr = *((struct in_addr*)he->h_addr);
    memset(&(my_addr.sin_zero), '\0', sizeof(my_addr.sin_zero));
    
    if (bind(sockfd,(struct sockaddr*)&my_addr,sizeof(struct sockaddr))
    == -1) {
        perror("bind");
        return -1;
    }
    
    if (listen(sockfd,BACKLOG) == -1) {
        perror("listen");
        return -1;
    }
    
    sa.sa_handler = sigchld_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if (sigaction(SIGCHLD,&sa,NULL) == -1) {
        perror("sigaction");
        return -1;
    }
    
    while (1) {
        sin_size = sizeof(struct sockaddr_in);
        if ((new_fd=accept(sockfd,(struct sockaddr*)&their_addr,&sin_size))
        == -1) {
            perror("accept");
            continue;
        }
        printf("new connection from %s on socket %d\n",
        inet_ntoa(their_addr.sin_addr), new_fd);
        if (fork() == 0) {  /* new process to child's thread of execution */
            close(sockfd);
            while (1) {
                if ((bytes=recv(new_fd,tmpbuf,BUFLEN-1,0)) == -1)
                    break;
                if (!bytes) {  /* stop if user pressed ctrl-c in telnet */
                    printf("socket %d implicitly hung up\n", new_fd);
                    break;
                }
                tmpbuf[bytes] = '\0';
                if (strcmp(tmpbuf,"end\r\n") != 0) {
                    send(new_fd, tmpbuf, bytes, 0);
                } else {
                    send(new_fd, "The End\r\n", 9, 0);
                    printf("socket %d explicitly hung up\n", new_fd);
                    break;
                }
            }
            close(new_fd);
            return 0;
        }
        close(new_fd);
    }
    
    return 0;
}

void sigchld_handler(int s) {
    while (wait(NULL) > 0);
}
