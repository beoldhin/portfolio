#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BACKLOG 10


int main(int argc, char* argv[]) {
    int i;
    int yes;
    int fdmax;
    int newfd;
    int nbytes;
    int addrlen;
    int listener;
    fd_set temp;
    fd_set master;
    char buf[256];
    struct hostent* he;
    struct sockaddr_in myaddr;
    struct sockaddr_in remoteaddr;
    
    if (argc != 3) {
        fprintf(stderr, "Invalid number of arguments.\n");
        fprintf(stderr, "Usage: %s <addr> <port>\n", argv[0]);
        return -1;
    }
    
    FD_ZERO(&master);
    FD_ZERO(&temp);
    
    /* get the listener */
    
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener == -1) {
        perror("socket");
        return -1;
    }
    
    /* lose the pesky "address already in use" error message */
    
    yes = 1;
    if (setsockopt(listener,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
        perror("setsockopt");
        return -1;
    }
    
    /* bind */
    
    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(atoi(argv[2]));
    he = gethostbyname(argv[1]);
    if (!he) {
        perror("gethostbyname");
        return -1;
    }
    myaddr.sin_addr = *((struct in_addr*)he->h_addr);
    memset(&(myaddr.sin_zero), '\0', sizeof(myaddr.sin_zero));
    if (bind(listener,(struct sockaddr*)&myaddr,sizeof(myaddr)) == -1) {
        perror("bind");
        return -1;
    }
    
    /* listen */
    
    if (listen(listener,BACKLOG) == -1) {
        perror("listen");
        return -1;
    }
    
    /* add the listener to the master set */
    
    FD_SET(listener, &master);
    
    /* keep track of the biggest file descriptor */
    
    fdmax = listener;
    
    while (1) {
        temp = master;
        if (select(fdmax+1,&temp,NULL,NULL,NULL) == -1) {
            perror("select");
            return -1;
        }
        
        for (i=0; i<=fdmax; ++i) {
            if (FD_ISSET(i, &temp)) {
                
                if (i == listener) {  /* handle new connections */
                    addrlen = sizeof(remoteaddr);
                    newfd = accept(listener, (struct sockaddr*)&remoteaddr,
                    &addrlen);
                    if (newfd != -1) {
                        FD_SET(newfd, &master);
                        if (newfd > fdmax)
                            fdmax = newfd;
                        printf("new connection from %s on socket %d\n",
                        inet_ntoa(remoteaddr.sin_addr), newfd);
                    } else
                        perror("accept");
                } else {  /* handle data from a client */
                    nbytes = recv(i, buf, sizeof(buf)-1, 0);
                    if (nbytes > 0) {
                        buf[nbytes] = '\0';
                        if (strcmp(buf,"end\r\n") != 0) {
                            send(i, buf, nbytes, 0);
                        } else {
                            send(i, "The End\r\n", 9, 0);
                            printf("socket %d explicitly hung up\n", i);
                            close(i);
                            FD_CLR(i, &master);
                        }
                    } else {
                        if (nbytes == 0)
                            printf("socket %d implicitly hung up\n", i);
                        else
                            perror("recv");
                        close(i);
                        FD_CLR(i, &master);
                    }
                }
                
            }
        }
        
    }
    
    return 0;
}
