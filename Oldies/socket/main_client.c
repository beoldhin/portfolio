#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>


int main(int argc, char* argv[]) {
    int nbytes;
    int sockfd;
    char buf[256];
    unsigned recvn;
    struct hostent* he;
    struct sockaddr_in their_addr;
    
    if (argc != 3) {
        fprintf(stderr, "Invalid number of arguments.\n");
        fprintf(stderr, "Usage %s <addr> <port>\n", argv[0]);
        return -1;
    }
    
    if ((sockfd=socket(AF_INET,SOCK_STREAM,0)) == -1) {
        perror("socket");
        return -1;
    }
    
    their_addr.sin_family = AF_INET;
    their_addr.sin_port = htons(atoi(argv[2]));
    if (!(he=gethostbyname(argv[1]))) {
        perror("gethostbyname");
        return -1;
    }
    their_addr.sin_addr = *((struct in_addr*)he->h_addr);
    memset(&(their_addr.sin_zero), '\0', sizeof(their_addr.sin_zero));
    
    if (connect(sockfd,(struct sockaddr*)&their_addr,
    sizeof(struct sockaddr)) == -1) {
        perror("connect");
        return -1;
    }
    
    recvn = 1;
    while (1) {
        
        send(sockfd, "Testing...\r\n", 12, 0);
        
        nbytes = recv(sockfd, buf, sizeof(buf)-1, 0);
        if (!nbytes)
            break;
        if (nbytes == -1) {
            perror("recv");
            break;
        }
        buf[nbytes] = '\0';
        printf("%s", buf);
        printf("%u\n", recvn);
        ++recvn;
    }
    
    
    return 0;
}
