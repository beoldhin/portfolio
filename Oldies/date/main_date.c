#include <stdio.h>
#include <stdlib.h>

#define MONTH 1
#define YEAR  2002

typedef enum align {
    ALIGN_LEFT,
    ALIGN_RIGHT,
    ALIGN_CENTER
} ALIGN;

void date_print_month_text(int, int, int*, int*);
void date_print_html_header(int);
void date_print_month_html(int, int, int*, int*);
void date_print_html_footer(void);
void date_print_html_line2ch(int, ALIGN, int, int);
void date_print_html_line(const char*, ALIGN, int, int);
int date_gregorian_to_weekday(int, int, int);
int date_gregorian_to_julian(int, int, int);
int date_julian_to_weeknumber(int);
int date_is_leap_year(int);

const char months[12][10] = {"January","February","March","April","May","June",
"July","August","September","October","November","December"};


int main(void) {
    
    int i;
    int som;
    int wkn;
    som = date_gregorian_to_weekday(1, MONTH, YEAR);
    wkn = date_julian_to_weeknumber(date_gregorian_to_julian(1,MONTH,YEAR));
    
    date_print_html_header(YEAR);
    
    for (i=1; i<=12; ++i) {
        date_print_month_html(i, YEAR, &som, &wkn);
        /*
        if ((i==3) || (i==6) || (i==9)) {
            puts("<br>");
        }
        */
    }
    
    date_print_html_footer();
    
    return 0;
}

void date_print_month_text(int month, int year, int* somp, int* wknp) {
    int i;
    int som = *somp;
    int wkn = *wknp;
    const char header[] = "Wk |  M Tu  W Th  F  S  S";
    int mdays[13] = {-1, 31, -2, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    printf("        %s %d\n", months[month-1], year);
    puts(header);
    printf("%2d | ", wkn);
    for (i=1; i<=som-1; ++i)
        printf("   ");
    if (month == 2)
        mdays[2] = date_is_leap_year(year) ? 29 : 28;
    for (i=1; i<=mdays[month]; ++i) {
        printf("%2d", i);
        if ((i+som-1)%7 != 0) {
            if (i != mdays[month])
                printf(" ");
            else {
                puts("");
            }
        } else {
            ++wkn;
            *somp = 0;
            if (i != mdays[month])
                printf("\n%2d | ", wkn);
            else
                puts("");
        }
        ++(*somp);
    }
    *wknp = wkn;
}

void date_print_html_header(int year) {
    puts("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
    puts("<html>");
    puts("<head>");
    puts("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=us-ascii\">");
    printf("<title>Finnish&nbsp;calendar&nbsp;for&nbsp;year&nbsp;%d</title>\n", year);
    puts("</head>");
    puts("<body bgcolor=\"#b0b0b0\" text=\"#000000\" link=\"#0000ff\" vlink=\"#0000ff\" alink=\"0000ff\">");
    printf("<center><h2>Finnish&nbsp;calendar&nbsp;for&nbsp;year&nbsp;%d</h2></center>\n", year);
    puts("<blockquote>");
}

void date_print_month_html(int month, int year, int* somp, int* wknp) {
    
    int i;
    int j;
    int som = *somp;
    int wkn = *wknp;
    const int conv[8] = {-1, 0, 6, 5, 4, 3, 2, 1};
    const char nbsp[] = "&nbsp;&nbsp;";
    int mdays[13] = {-1, 31, -2, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    
    puts("<table align=\"center\" bgcolor=\"#ffffcc\" width=\"185\" border=\"1\">");
    printf("<tr><td colspan=\"8\" align=\"center\"><b>%s</b></td></tr>\n", months[month-1]);
    
    puts("<tr>");
    date_print_html_line("WK", ALIGN_CENTER, 30, 0x0000ff);
    date_print_html_line("MO", ALIGN_RIGHT, 15, 0x000000);
    date_print_html_line("TU", ALIGN_RIGHT, 15, 0x000000);
    date_print_html_line("WE", ALIGN_RIGHT, 15, 0x000000);
    date_print_html_line("TH", ALIGN_RIGHT, 15, 0x000000);
    date_print_html_line("FR", ALIGN_RIGHT, 15, 0x000000);
    date_print_html_line("SA", ALIGN_RIGHT, 15, 0x000000);
    date_print_html_line("SU", ALIGN_RIGHT, 15, 0xff0000);
    puts("</tr>");
    
    puts("<tr>");
    date_print_html_line2ch(wkn, ALIGN_CENTER, 30, 0x0000ff);
    for (i=1; i<=som-1; ++i)
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
    if (month == 2)
        mdays[2] = date_is_leap_year(year) ? 29 : 28;
    
    for (i=1; i<=mdays[month]; ++i) {
        date_print_html_line2ch(i, ALIGN_RIGHT, 15, 0x000000);
        if ((i+som-1)%7 == 0) {
            ++wkn;
            *somp = 0;
            if (i != mdays[month]) {
                puts("</tr>");
                puts("<tr>");
                date_print_html_line2ch(wkn, ALIGN_CENTER, 30, 0x0000ff);
            }
        }
        ++(*somp);
    }
    j = conv[*somp];
    for (i=0; i<j; ++i)
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
    puts("</tr>");
    
    /* 7x6 = 42 squares total, let's fill the last with &nbsp */
    
    if ((som-1)+mdays[month]+j < 42) {
        puts("<tr>");
        date_print_html_line(nbsp, ALIGN_CENTER, 30, 0x0000ff);
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
        date_print_html_line(nbsp, ALIGN_RIGHT, 15, 0x000000);
        puts("</tr>");
    }
    
    puts("</table>");
    
    *wknp = wkn;
    
}

void date_print_html_footer(void) {
    puts("</blockquote>");
    puts("</body>");
    puts("</html>");
}

void date_print_html_line2ch(int num, ALIGN align, int width, int color) {
    char tmpbuf[256];
    snprintf(tmpbuf, sizeof(tmpbuf), "%d", num);
    if (num < 10)
        snprintf(tmpbuf, sizeof(tmpbuf), "&nbsp;%d", num);
    date_print_html_line(tmpbuf, align, width, color);
}

void date_print_html_line(const char* text, ALIGN align, int width, int color) {
    const char line[] = "<td align=\"%s\" width=\"%d\"><font size=\"-1\" color=\"#%06x\">%s</font></td>\n";
    if (align == ALIGN_LEFT)
        printf(line, "left", width, color, text);
    else if (align == ALIGN_RIGHT)
        printf(line, "right", width, color, text);
    else
        printf(line, "center", width, color, text);

}

int date_gregorian_to_weekday(int day, int month, int year) {
    int a = (14-month) / 12;
    int y = year - a;
    int m = month + 12*a - 2;
    return (day+y+y/4-y/100+y/400+(31*m)/12) % 7;
}

int date_gregorian_to_julian(int day, int month, int year) {
    int a = (14-month) / 12;
    int y = year + 4800 - a;
    int m = month + 12*a - 3;
    return day + (153*m+2)/5 + y*365 + y/4 - y/100 + y/400 - 32045;
}

int date_julian_to_weeknumber(int julian) {
    int d4 = (julian+31741-(julian%7)) % 146097 % 36524 % 1461;
    int L = d4 / 1460;
    int d1 = ((d4-L)%365) + L;
    return d1/7 + 1;
}

int date_is_leap_year(int year) {
    if (year%4 != 0)
        return 0;
    if (year%400 == 0)
        return 1;
    if (year%100 == 0)
        return 0;
    return 1;
}
