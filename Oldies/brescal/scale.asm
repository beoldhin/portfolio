; Using 32-bit flat mode
_text           segment use32 dword public 'CODE'
                assume cs:_text,ds:_data

vgascr          equ     0a0000h
;*
flength         equ     64800
;*
xlimit          equ     320
ylimit          equ     200
;*
textmode        equ     0003h
graphmode       equ     0013h
exitok          equ     4c00h
exitnopic       equ     4c01h
exittinyf       equ     4c02h
;*

start:          jmp     main
                db      'WATCOM'

main:

set_selectors   macro
                sti
                cld
                mov     ax,ds
                mov     es,ax
                endm

read_pictfile   macro
                mov     eax,3d00h
                mov     edx,offset picname
                int     21h
                jnc     @F
                mov     eax,0900h
                mov     edx,offset nopict
                int     21h
                mov     eax,exitnopic
                int     21h
@@:             mov     fhandle,eax
                mov     eax,3f00h
                mov     ebx,fhandle
                mov     ecx,flength
                mov     edx,offset picture
                int     21h
                jnc     @F
                mov     eax,0900h
                mov     edx,offset smallf
                int     21h
                mov     eax,exittinyf
                int     21h
@@:
                endm

set_graphscr    macro
                mov     eax,graphmode
                int     10h
                mov     edx,3c8h
                xor     eax,eax
                out     dx,al
                inc     edx
                mov     esi,offset picture+32
                mov     ecx,768
@@:             mov     al,[esi]
                shr     eax,2
                out     dx,al
                inc     esi
                dec     ecx
                jnz     @B
                endm

show_sequence   macro
                local   showseq
                cli
                reset_clock2
showseq:        read_randoms    xlimit,x1
                read_randoms    ylimit,y1
                read_randoms    xlimit,x2
                read_randoms    ylimit,y2
                do_bresnscale
                read_portkey
                sti
                endm

reset_clock2    macro
                in      al,61h
                and     al,11111100b
                out     61h,al
                mov     al,10111010b
                out     43h,al
                xor     al,al
                out     42h,al
                jmp     $+2
                out     42h,al
                in      al,61h
                or      al,00000001b
                out     61h,al
                endm

read_randoms    macro   rlimit,xycoor
                in      al,42h
                mov     ah,al
                in      al,42h
                xchg    ah,al
                mov     bx,4961
                mul     bx
                inc     ax
                xor     edx,edx
                mov     bx,rlimit
                div     bx
                mov     xycoor,edx
                endm

close_clock2    macro
                in      al,61h
                and     al,11111110b
                out     61h,al
                endm

read_portkey    macro
                in      al,60h
                cmp     al,1
                jne     showseq
                endm

do_bresnscale   macro
                local   idx1,idx2,idx3,idx4
                do_xyscale      x2,x1,xscale
                do_xyscale      y2,y1,yscale
                cmp     dword ptr xscale,xlimit
                je      idx2
                make_pictpos
                make_xyscale    xlimit,xscale,xdata
                make_xgap
                cmp     dword ptr yscale,ylimit
                je      idx1
                make_xyscale    ylimit,yscale,ydata
                show_xyscale
                jmp     idx4
idx1:           show_ydirect
                jmp     idx4
idx2:           cmp     dword ptr yscale,ylimit
                je      idx3
                make_pictpos
                make_xyscale    ylimit,yscale,ydata
                show_xdirect
                jmp     idx4
idx3:           show_xydirect
idx4:
                endm

do_xyscale      macro   xy2,xy1,xyscale
                mov     eax,xy2
                mov     ebx,xy1
                cmp     eax,ebx
                ja      @F
                mov     xy2,ebx
                mov     xy1,eax
@@:             sub     eax,ebx
                inc     eax
                mov     xyscale,eax
                endm

make_pictpos    macro
                mov     eax,y1
                mov     ebx,eax
                shl     eax,8
                shl     ebx,6
                add     eax,ebx
                add     eax,x1
                add     eax,vgascr
                mov     pictpos,eax
                endm

make_xgap       macro
                mov     eax,xlimit
                sub     eax,xscale
                mov     xgap,eax
                endm

make_xyscale    macro   xyorig,xyscal,xyoffs
                local   idx11,idx12,idx13,idx14,idx15
                mov     eax,xyscal
                mov     ecx,xyorig
                mov     esi,eax
                xor     ebp,ebp
                shr     esi,1
                jc      idx11
                mov     eax,esi
                shr     ecx,1
                inc     ebp
idx11:          mov     ebx,eax
                shl     eax,1
                sub     ebx,ecx
                shl     ebx,1
                mov     edx,eax
                sub     edx,ecx
                mov     edi,offset xyoffs
idx12:          test    edx,80000000h
                jnz     idx13
                mov     byte ptr [edi],'1'
                add     edx,ebx
                inc     edi
                dec     ecx
                jnz     idx12
                jmp     idx14
idx13:          mov     byte ptr [edi],'0'
                add     edx,eax
                inc     edi
                dec     ecx
                jnz     idx12
idx14:          or      ebp,ebp
                jz      idx15
                mov     esi,offset xyoffs
                mov     ecx,xyorig/8
                rep     movsd
idx15:
                endm

show_xyscale    macro
                local   idx21,idx22,idx23,idx24,idx25
                mov     ebp,offset ydata
                mov     esi,offset picture+800
                mov     edi,pictpos
                mov     edx,ylimit
idx21:          cmp     byte ptr [ebp],'0'
                je      idx24
                mov     ebx,offset xdata
                mov     ecx,xlimit
idx22:          cmp     byte ptr [ebx],'0'
                je      idx23
                mov     al,[esi]
                mov     [edi],al
                inc     esi
                inc     edi
                inc     ebx
                dec     ecx
                jnz     idx22
                add     edi,xgap
                inc     ebp
                dec     edx
                jnz     idx21
                jmp     idx25
idx23:          inc     esi
                inc     ebx
                dec     ecx
                jnz     idx22
                add     edi,xgap
                inc     ebp
                dec     edx
                jnz     idx21
                jmp     idx25
idx24:          add     esi,xlimit
                inc     ebp
                dec     edx
                jnz     idx21
idx25:
                endm

show_xdirect    macro
                local   idx31,idx32,idx33
                mov     ebx,offset ydata
                mov     esi,offset picture+800
                mov     edi,pictpos
                mov     edx,ylimit
idx31:          cmp     byte ptr [ebx],'0'
                je      idx32
                mov     ecx,xlimit/4
                rep     movsd
                inc     ebx
                dec     edx
                jnz     idx31
                jmp     idx33
idx32:          add     esi,xlimit
                inc     ebx
                dec     edx
                jnz     idx31
idx33:
                endm

show_ydirect    macro
                local   idx41,idx42,idx43
                mov     ebx,offset xdata
                mov     esi,offset picture+800
                mov     edi,pictpos
                mov     ecx,xlimit
                mov     edx,ylimit
idx41:          cmp     byte ptr [ebx],'0'
                je      idx42
                mov     al,[esi]
                mov     [edi],al
                inc     ebx
                inc     esi
                inc     edi
                dec     ecx
                jnz     idx41
                mov     ecx,xlimit
                mov     ebx,offset xdata
                add     edi,xgap
                dec     edx
                jnz     idx41
                jmp     idx43
idx42:          inc     ebx
                inc     esi
                dec     ecx
                jnz     idx41
                mov     ecx,xlimit
                mov     ebx,offset xdata
                add     edi,xgap
                dec     edx
                jnz     idx41
idx43:
                endm

show_xydirect   macro
                mov     esi,offset picture+800
                mov     edi,vgascr
                mov     ecx,(flength-800)/4
                rep     movsd
                endm

exit_all        macro
                mov     eax,textmode
                int     10h
                mov     eax,exitok
                int     21h
                endm


                set_selectors
                read_pictfile
                set_graphscr
                show_sequence
                exit_all


_text           ends


_data           segment use32 dword public 'DATA'
xdata           db      xlimit dup (?)
ydata           db      ylimit dup (?)
picture         db      flength dup (?)
xscale          dd      ?
yscale          dd      ?
xgap            dd      ?
x1              dd      ?
y1              dd      ?
x2              dd      ?
y2              dd      ?
fhandle         dd      ?
pictpos         dd      ?
nopict          db      'Picture file not found.',13,10,'$'
smallf          db      'Picture file is too small.',13,10,'$'
picname         db      'picture.raw',0
_data           ends

stack           segment para stack 'STACK'
                db      1000h dup (?)
stack           ends

                end     start
