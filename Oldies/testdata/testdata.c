#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

unsigned char* create_testdata(size_t length);
void write_testdata_to_file(unsigned char* testdata, size_t lenoffile, int i, size_t numoffiles);

int main(int argc, char* argv[])
{
	int i;
	if ((argc<3) || (argc>4)) {
		fprintf(stderr, "Error: Unknown number of arguments\n");
		fprintf(stderr, "Usage: tester <numoffiles> <lenoffile1> [<lenoffile2>]\n");
		return -1;
	}
	size_t numoffiles = atoi(argv[1]);
	size_t lenoffile = atoi(argv[2]);
	size_t lenoffile2 = 0;
	if (argc == 4) {
		lenoffile2 = atoi(argv[3]);
	}
	if ((argc==3) || (argc==4 && lenoffile==lenoffile2)) {
		unsigned char* testdata = create_testdata(lenoffile);
		for (i=0; i<numoffiles; i++) {
			write_testdata_to_file(testdata, lenoffile, i, numoffiles);
		}
		free(testdata);
	} else { /* argc == 4 && lenoffile!=lenoffile2 */
		if (lenoffile2 < lenoffile) {
			fprintf(stderr, "Error: lenoffile2 < lenoffile1\n");
			return -1;
		}
		time_t curtime;
		time(&curtime);
		srand(curtime);
		for (i=0; i<numoffiles; i++) {
			size_t lenoffilex = lenoffile + (rand()%(lenoffile2-lenoffile)) + 1;
			unsigned char* testdata = create_testdata(lenoffilex);
			write_testdata_to_file(testdata, lenoffilex, i, numoffiles);
			free(testdata);
		}
	}
	return 0;
}

unsigned char* create_testdata(size_t length)
{
	int pos;
	char testset[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	unsigned char* testdata = (unsigned char*)malloc(length);
	size_t testsetsize = sizeof(testset) - 1;
	size_t fullblocks = length / testsetsize;
	size_t remainder = length % testsetsize;
	for (pos=0; pos<fullblocks*testsetsize; pos+=testsetsize) {
		memcpy(testdata+pos, testset, testsetsize);
	}
	if (remainder) {
		memcpy(testdata+pos, testset, remainder);
	}
	return testdata;
}

void write_testdata_to_file(unsigned char* testdata, size_t lenoffile, int i, size_t numoffiles) {
	char testfilename[13]; /* 00000000.txt + 0 */
	snprintf(testfilename, sizeof(testfilename), "%08X.txt", i);
	FILE* testfile = fopen(testfilename, "wb");
	printf("Writing file %u/%u\n", i+1, numoffiles);
	fwrite(testdata, sizeof(char), lenoffile, testfile);
	fclose(testfile);
}
