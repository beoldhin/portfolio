#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include "timemsg_ioctl.h"


int main(void) {
    int fd;
    char device[] = "/dev/timemsg";
    
    if ((fd=open(device,O_WRONLY)) < 0) {
        fprintf(stderr, "can't open %s\n", device);
        return -1;
    }
    
    if (ioctl(fd,TIMEMSG_IOC_HARDRESET) < 0) {
        fprintf(stderr, "can't ioctl %s\n", device);
        return -1;
    }
    
    printf("timemsg usage count reset\n");
    
    return 0;
}
