#ifndef _TIMEMSG_IOCTL_H
#define _TIMEMSG_IOCTL_H

/* See ../Documentation/ioctl-numbers.txt for free IOCTL majors */

#define TIMEMSG_IOCTL 0xD1
#define TIMEMSG_IOC_HARDRESET _IO(TIMEMSG_IOCTL, 0)
#define TIMEMSG_IOC_SETTIME _IOW(TIMEMSG_IOCTL, 1, int)
#define TIMEMSG_IOC_SETMSG _IOW(TIMEMSG_IOCTL, 2, char*)

struct timemsg_t {
    int time;
    int msglen;
    char msg[80];
};

#endif  /* _TIMEMSG_IOCTL_H */
