#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include "timemsg_ioctl.h"


int main(int argc, char* argv[]) {
    int fd;
    int time;
    char errmsg[] = "can't ioctl";
    char device[] = "/dev/timemsg";
    struct timemsg_t timemsg;
    
    if (argc != 3) {
        fprintf(stderr, "Invalid number of parameters\n");
        fprintf(stderr, "Usage: ./timemsg <time> <msg>\n");
        return -1;
    }
    
    timemsg.time = atoi(argv[1]);
    strncpy(timemsg.msg, argv[2], sizeof(timemsg.msg));
    timemsg.msg[sizeof(timemsg.msg)-1] = '\0';
    timemsg.msglen = strlen(timemsg.msg) + 1;
    
    if ((fd=open(device,O_WRONLY)) < 0) {
        fprintf(stderr, "can't open %s\n", device);
        return -1;
    }
    
    if (ioctl(fd,TIMEMSG_IOC_SETTIME,&timemsg) < 0) {
        fprintf(stderr, errmsg, device);
        return -1;
    }
    
    if (ioctl(fd,TIMEMSG_IOC_SETMSG,&timemsg) < 0) {
        fprintf(stderr, errmsg, device);
        return -1;
    }
    
    return 0;
}
