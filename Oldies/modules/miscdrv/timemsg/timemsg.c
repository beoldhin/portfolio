/*
 * This device registers "misc device" - those are in /proc/misc
 * Normally it would have a major drive in /proc/devices
 * misc_register is used instead of register_chrdev.
 * TIMEMSG_MAJOR changed to TIMEMSG_MINOR and timemsg_miscdev added.
 */

#include <asm/semaphore.h>  /* semaphores */
#include <asm/uaccess.h>    /* put_user */
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include "timemsg_ioctl.h"

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,4,0)
#error "Kernel too old for timemsg"
#endif

#define SUCCESS 0
#define TIMEMSG_MINOR 101  /* see include/linux/miscdevice.h */
#define TIMEMSG_NAME "timemsg"

static int timemsg_open(struct inode*, struct file*);
static void timemsg_timer(unsigned long);
static int timemsg_ioctl(struct inode*, struct file*, unsigned int, unsigned long);
static int timemsg_release(struct inode*, struct file*);

static struct file_operations timemsg_fops = {
    .owner = THIS_MODULE,
    .open = timemsg_open,
    .ioctl = timemsg_ioctl,
    .release = timemsg_release,
};

static struct miscdevice timemsg_miscdev = {
    TIMEMSG_MINOR,
    "timemsg",
    &timemsg_fops,
};

static int set = 0;
static int delta = 60 * HZ;
static char timemsg[80] = "No message set.";
static struct timer_list timemsg_time;
static struct semaphore timemsg_sema;


static int __init
timemsg_init(void)
{
    int ret;
    
    ret = misc_register(&timemsg_miscdev);
    if (ret < 0)
        goto err_register;
    
    init_timer(&timemsg_time);
    timemsg_time.function = timemsg_timer;
    mod_timer(&timemsg_time, jiffies + delta);
    
    sema_init(&timemsg_sema, 1);
    
    return SUCCESS;
    
err_register:
    return ret;
}

static void
timemsg_timer(unsigned long dummy)
{
    if (set)
        printk(KERN_INFO "%s\n", timemsg);
    mod_timer(&timemsg_time, jiffies+delta);
}

static void __exit
timemsg_exit(void)
{
    del_timer(&timemsg_time);
    misc_deregister(&timemsg_miscdev);
}

static int
timemsg_open(
    struct inode* inodp,
    struct file* filp)
{
    if ((filp->f_flags&O_ACCMODE) != O_WRONLY)
        return -EACCES;
    
    
    
    return SUCCESS;
}

static int
timemsg_ioctl(
    struct inode* inodp,
    struct file* filp,
    unsigned int cmd,
    unsigned long arg)
{
    int msglen;
    struct timemsg_t* timet = (struct timemsg_t*)arg;
    switch (cmd) {
        case TIMEMSG_IOC_SETTIME:
            if (!capable(CAP_SYS_ADMIN))
                return -EPERM;
            get_user(delta, &timet->time); delta *= HZ;
            mod_timer(&timemsg_time, jiffies+delta);
            printk(KERN_INFO "Delta is now %d jiffies\n", delta);
            set = 1;
            return 0;
        case TIMEMSG_IOC_SETMSG:
            if (!capable(CAP_SYS_ADMIN))
                return -EPERM;
            get_user(msglen, &timet->msglen);
            if (down_interruptible(&timemsg_sema))
                return -ERESTARTSYS;
            if (copy_from_user(timemsg,&timet->msg,msglen)) {
                up(&timemsg_sema);
                return -EFAULT;
            }
            up(&timemsg_sema);
            return 0;
        case TIMEMSG_IOC_HARDRESET:
            while (MOD_IN_USE)
                MOD_DEC_USE_COUNT;
            MOD_INC_USE_COUNT;
            return 0;
        default:
            return -ENOTTY;
    }
}


static int
timemsg_release(
    struct inode* inodp,
    struct file* filp)
{
    
    
    
    return SUCCESS;
}

#ifndef MODULE
static int __init
timemsg_setup(char* str)
{
    str = get_options(str, ARRAY_SIZE(timemsg), timemsg);
    if (timemsg[0] && timemsg[1])
        printk(KERN_INFO "timemsg: %d %d\n", timemsg[0], timemsg[1]);
    return 1;
}
__setup("timemsg=", timemsg_setup);
#else
/*
 * Note: module_init and module_exit are used to change the name of the
 * entry points of a module (by default init_module and cleanup_module) to
 * avoid namespace pollution. Functions marked with __init and __exit are
 * general entry points for non-modules too; if functionality is the same,
 * use same general entry point for modules.
 * __init functions are called when the root filesystem is being made
 * (removed from memory after __init function called)
 * __exit functions are called when the root filesystem is disappearing
 * See: include/linux/init.h
 */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Olli");
MODULE_DESCRIPTION("Simple timemsg device");
MODULE_SUPPORTED_DEVICE("timemsg hardware");

#ifdef TESTSTUFF
MODULE_PARM(timemsg, "1-2i");
MODULE_PARM_DESC(timemsg, "Sets timemsg int array");
MODULE_PARM(timemsgp, "s");
MODULE_PARM_DESC(timemsgp, "Hello string");
#endif

module_init(timemsg_init);
module_exit(timemsg_exit);
#endif

EXPORT_NO_SYMBOLS;
