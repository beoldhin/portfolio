#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include "precalc_ioctl.h"


int main(void) {
    int fd;
    char device[] = "/dev/precalc";
    
    if ((fd=open(device,O_WRONLY)) < 0) {
        fprintf(stderr, "can't open %s\n", device);
        return -1;
    }
    
    if (ioctl(fd,PRECALC_IOC_HARDRESET) < 0) {
        fprintf(stderr, "can't ioctl %s\n", device);
        return -1;
    }
    
    printf("precalc usage count reset\n");
    
    return 0;
}
