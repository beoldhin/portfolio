#ifndef _PRECALC_IOCTL_H
#define _PRECALC_IOCTL_H

/* See ../Documentation/ioctl-numbers.txt for free IOCTL majors */

#define PRECALC_IOCTL 0xD0
#define PRECALC_IOC_HARDRESET _IO(PRECALC_IOCTL, 0)
#define PRECALC_IOC_GETROT13L _IOR(PRECALC_IOCTL, 1, int)
#define PRECALC_IOC_GETROT13T _IOR(PRECALC_IOCTL, 2, unsigned char*)

#endif  /* _PRECALC_IOCTL_H */
