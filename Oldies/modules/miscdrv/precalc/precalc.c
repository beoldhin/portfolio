/*
 * This device registers "misc device" - those are in /proc/misc
 * Normally it would have a major drive in /proc/devices
 * misc_register is used instead of register_chrdev.
 * PRECALC_MAJOR changed to PRECALC_MINOR and precalc_miscdev added.
 */

#include <asm/semaphore.h>  /* semaphores */
#include <asm/uaccess.h>    /* put_user */
#include <linux/slab.h>     /* kmalloc */
#include <linux/sched.h>
#include <linux/module.h>
#include <linux/miscdevice.h>
#include "precalc_ioctl.h"

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,4,0)
#error "Kernel too old for precalc"
#endif

#define SUCCESS 0
#define PRECALC_MINOR 100  /* see include/linux/miscdevice.h */
#define PRECALC_NAME "precalc"
#define ROT13L 256

static int precalc_create_rot13(void);
static int precalc_open(struct inode*, struct file*);
static int precalc_ioctl(struct inode*, struct file*, unsigned int, unsigned long);
static int precalc_release(struct inode*, struct file*);

static struct file_operations precalc_fops = {
    .owner = THIS_MODULE,
    .open = precalc_open,
    .ioctl = precalc_ioctl,
    .release = precalc_release,
};

static struct miscdevice precalc_miscdev = {
    PRECALC_MINOR,
    "precalc",
    &precalc_fops,
};

static unsigned char* rot13tbl;
static struct semaphore ioctl_sema;


static int __init
precalc_init(void)
{
    int ret;
    
    ret = misc_register(&precalc_miscdev);
    if (ret < 0)
        goto err_register;
    
    if (precalc_create_rot13() < 0)
        goto err_rot13;
    
    sema_init(&ioctl_sema, 1);
    
    return SUCCESS;
    
err_rot13:
    return -ENOMEM;
err_register:
    return ret;
}

static int
precalc_create_rot13(void)
{
    int i;
    rot13tbl = kmalloc(ROT13L, GFP_KERNEL);
    if (!rot13tbl)
        return -1;
    for (i=0; i<ROT13L; ++i) {
        if (i>='a' && i<='m') {
            rot13tbl[i] = i + 13;
        } else if (i>='n' && i<='z') {
            rot13tbl[i] = i - 13;
        } else if (i>='A' && i<='M') {
            rot13tbl[i] = i + 13;
        } else if (i>='N' && i<='Z') {
            rot13tbl[i] = i - 13;
        } else {
            rot13tbl[i] = i;
        }
    }
    return 0;
}

static void __exit
precalc_exit(void)
{
    if (rot13tbl)
        kfree(rot13tbl);
    misc_deregister(&precalc_miscdev);
}

static int
precalc_open(
    struct inode* inodp,
    struct file* filp)
{
    
    if ((filp->f_flags&O_ACCMODE) != O_WRONLY)
        return -EACCES;
    
    
    
    return SUCCESS;
}

static int
precalc_ioctl(
    struct inode* inodp,
    struct file* filp,
    unsigned int cmd,
    unsigned long arg)
{
    switch (cmd) {
        case PRECALC_IOC_GETROT13L:
            return put_user(ROT13L, (int*)arg);
        case PRECALC_IOC_GETROT13T:
            if (down_interruptible(&ioctl_sema))
                return -ERESTARTSYS;
            if (copy_to_user((unsigned char*)arg,rot13tbl,ROT13L)) {
                up(&ioctl_sema);
                return -EFAULT;
            }
            up(&ioctl_sema);
            return 0;
        case PRECALC_IOC_HARDRESET:
            while (MOD_IN_USE)
                MOD_DEC_USE_COUNT;
            MOD_INC_USE_COUNT;
            return 0;
        default:
            return -ENOTTY;
    }
}


static int
precalc_release(
    struct inode* inodp,
    struct file* filp)
{
    

    
    return SUCCESS;
}

#ifndef MODULE
static int __init
precalc_setup(char* str)
{
    str = get_options(str, ARRAY_SIZE(precalc), precalc);
    if (precalc[0] && precalc[1])
        printk(KERN_INFO "precalc: %d %d\n", precalc[0], precalc[1]);
    return 1;
}
__setup("precalc=", precalc_setup);
#else
/*
 * Note: module_init and module_exit are used to change the name of the
 * entry points of a module (by default init_module and cleanup_module) to
 * avoid namespace pollution. Functions marked with __init and __exit are
 * general entry points for non-modules too; if functionality is the same,
 * use same general entry point for modules.
 * __init functions are called when the root filesystem is being made
 * (removed from memory after __init function called)
 * __exit functions are called when the root filesystem is disappearing
 * See: include/linux/init.h
 */
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Olli");
MODULE_DESCRIPTION("Simple precalc device");
MODULE_SUPPORTED_DEVICE("precalc hardware");

#ifdef TESTSTUFF
MODULE_PARM(precalc, "1-2i");
MODULE_PARM_DESC(precalc, "Sets precalc int array");
MODULE_PARM(precalcp, "s");
MODULE_PARM_DESC(precalcp, "Hello string");
#endif

module_init(precalc_init);
module_exit(precalc_exit);
#endif

EXPORT_NO_SYMBOLS;
