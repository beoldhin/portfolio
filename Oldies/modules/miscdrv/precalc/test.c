#include <fcntl.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include "precalc_ioctl.h"


int main(void) {
    int i;
    int fd;
    int rot13l;
    char device[] = "/dev/precalc";
    char teststr1[] = "testing...";
    char teststr2[] = "grfgvat...";
    unsigned char* rot13tbl;
    
    if ((fd=open(device,O_WRONLY)) < 0) {
        fprintf(stderr, "can't open %s\n", device);
        return -1;
    }
    
    if (ioctl(fd,PRECALC_IOC_GETROT13L,&rot13l) < 0) {
        fprintf(stderr, "can't ioctl %s\n", device);
        return -1;
    }
    
    rot13tbl = (unsigned char*)malloc(rot13l);
    if (ioctl(fd,PRECALC_IOC_GETROT13T,rot13tbl) < 0) {
        fprintf(stderr, "can't ioctl %s\n", device);
        return -1;
    }
    
    for (i=0; i<sizeof(teststr1)-1; ++i)
        putchar(rot13tbl[teststr1[i]]);
    puts("");
    for (i=0; i<sizeof(teststr2)-1; ++i)
        putchar(rot13tbl[teststr2[i]]);
    puts("");
    
    return 0;
}
