<html>
<head>
<title>Linux Device Drivers, 2nd Edition: Chapter 10: Judicious Use of Data Types</title>
</head>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000" link="#990000" vlink="#0000CC">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" width="90%">
<tr>
<td colspan=2>
<IMG WIDTH="515" height="37" ALIGN="BOTTOM"
 ALT="Search the Catalog" BORDER="0" 
 USEMAP="#catalog_header_buttons" ISMAP
 SRC="figs/catalog_header_buttons.gif">
<MAP Name="catalog_header_buttons">
<aREA Shape="Rect" coords = "407,17,512,32"  href="http://www.oreilly.com/catalog/search.html">
<aREA Shape="Rect" coords = "431,3,512,18"  href="http://www.oreilly.com/catalog/prdindex.html">
</MAP>
</td>
</tr>
<tr>
<td width="25%" valign="TOP">
<A HREF="http://www.oreilly.com/catalog/linuxdrive2/">
<img hspace=10 vspace=10 src="figs/linuxdrive2.s.gif" 
alt="Linux Device Drivers, 2nd Edition" align=left valign=top border=0>
</a>
</td>
<td height="105" valign="TOP">
<br>
<H2>Linux Device Drivers, 2nd Edition</H2>
<font size="-1">
<a href="http://www.oreilly.com/catalog/linuxdrive2/author.html">By Alessandro Rubini &amp; Jonathan Corbet</a><br />
2nd Edition June 2001<br />
0-59600-008-1, Order Number: 0081<br />
586 pages, $39.95
</font>
</td>
</tr>
</table>
<hr size=1 noshade>
<!--sample chapter begins -->
<blockquote>



<h2 class="chapter">Chapter 10
<br>
Judicious Use of Data Types</h2>


<h4 class="tochead">Contents:</h4>

<p>
<a href="#t1">Use of Standard C Types</a>
<br>
<a href="#t2">Assigning an Explicit Size to Data Items</a>
<br>
<a href="#t3">Interface-Specific Types</a>
<br>
<a href="#t4">Other Portability Issues</a>
<br>
<a href="#t5">Linked Lists</a>
<br>
<a href="#t6">Quick Reference</a>
</p>

<p><a name="INDEX-2,476" />
<a name="INDEX-2,477" />
Before we go on to more advanced topics, we need to stop for a quick
note on portability issues. Modern versions of the Linux kernel are
highly portable, running on several very different architectures.
Given the multiplatform nature of Linux, drivers intended for serious
use should be portable as well.
</p>



<p>But a core issue with kernel code is being able both to access data
items of known length (for example, filesystem data structures or
registers on device boards) and to exploit the capabilities of
different processors (32-bit and 64-bit architectures, and possibly 16
bit as well).
</p>



<p>Several of the problems encountered by kernel developers while porting
x86 code to new architectures have been related to incorrect data
typing. Adherence to strict data typing and compiling with the
<em class="emphasis">-Wall -Wstrict-prototypes</em> flags can prevent most
bugs.
</p>



<p>Data types used by kernel data are divided into three main classes:
standard C types such as <tt class="literal">int</tt>, explicitly sized
types such as <tt class="literal">u32</tt>, and types used for specific
kernel objects, such as <tt class="literal">pid_t</tt>. We are going to see
when and how each of the three typing classes should be used. The
final sections of the chapter talk about some other typical problems
you might run into when porting driver code from the x86 to other
platforms, and introduce the generalized support for linked lists
exported by recent kernel headers.
</p>



<p>If you follow the guidelines we provide, your driver should compile
and run even on platforms on which you are unable to test it.

</p>


<a name="judasc" />
<h2 class="sect1"><a name="t1">Use of Standard C Types</a></h2>


<p><a name="INDEX-2,478" />
<a name="INDEX-2,479" />
Although most programmers are accustomed to freely using standard
types like <tt class="literal">int</tt> and <tt class="literal">long</tt>, writing
device drivers requires some care to avoid typing conflicts and
obscure bugs.
</p>



<p><a name="INDEX-2,480" />
The problem is that you can't use the standard types when you need "a
two-byte filler'' or "something representing a four-byte string''
because the normal C data types are not the same size on all
architectures. To show the data size of the various C types, the
<em class="application">datasize</em> program has been included in the
sample files provided on the O'Reilly FTP site, in the directory
<em class="filename">misc-progs</em>. This is a sample run of the program
on a PC (the last four types shown are introduced in the next
section):
</p>


<blockquote><pre class="code">morgana% misc-progs/datasize
arch   Size:  char  shor   int  long   ptr long-long  u8 u16 u32 u64
i686            1     2     4     4     4     8        1   2   4   8
</pre></blockquote>


<p>The program can be used to show that <tt class="literal">long</tt> integers
and pointers feature a different size on 64-bit platforms, as
demonstrated by running the program on different Linux computers:
</p>


<blockquote><pre class="code">arch   Size:  char  shor   int  long   ptr long-long  u8 u16 u32 u64
i386            1     2     4     4     4     8        1   2   4   8
alpha           1     2     4     8     8     8        1   2   4   8
armv4l          1     2     4     4     4     8        1   2   4   8
ia64            1     2     4     8     8     8        1   2   4   8
m68k            1     2     4     4     4     8        1   2   4   8
mips            1     2     4     4     4     8        1   2   4   8
ppc             1     2     4     4     4     8        1   2   4   8
sparc           1     2     4     4     4     8        1   2   4   8
sparc64         1     2     4     4     4     8        1   2   4   8
</pre></blockquote>


<p><a name="INDEX-2,481" />
It's interesting to note that the user space of
<em class="emphasis">Linux-sparc64</em> runs 32-bit code, so pointers are
32 bits wide in user space, even though they are 64 bits wide in kernel
space. This can be verified by loading the
<em class="application">kdatasize</em> module (available in the
directory <em class="filename">misc-modules</em> within the sample
files). The module reports size information at load time using
<em class="emphasis">printk</em> and returns an error (so there's no need
to unload it):
</p>


<blockquote><pre class="code">kernel: arch   Size:  char short int long  ptr long-long u8 u16 u32 u64
kernel: sparc64         1    2    4    8    8     8       1   2   4   8
</pre></blockquote>


<p><a name="INDEX-2,482" />
<a name="INDEX-2,483" />
<a name="INDEX-2,484" />
<a name="INDEX-2,485" />
Although you must be careful when mixing different data types,
sometimes there are good reasons to do so. One such situation is for
memory addresses, which are special as far as the kernel is concerned.
Although conceptually addresses are pointers, memory administration is
better accomplished by using an unsigned integer type; the kernel
treats physical memory like a huge array, and a memory address is just
an index into the array. Furthermore, a pointer is easily
dereferenced; when dealing directly with memory addresses you almost
never want to dereference them in this manner. Using an integer type
prevents this dereferencing, thus avoiding bugs. Therefore, addresses
in the kernel are <tt class="literal">unsigned long</tt>, exploiting the
fact that pointers and <tt class="literal">long</tt> integers are always the
same size, at least on all the platforms currently supported by Linux.
</p>



<p><a name="INDEX-2,486" />
<a name="INDEX-2,487" />
The C99 standard defines the <tt class="literal">intptr_t</tt> and
<tt class="literal">uintptr_t</tt> types for an integer variable which
can hold a pointer value. These types are almost unused in the 2.4
kernel, but it would not be surprising to see them show up more often
as a result of future development work.

</p>



<h2 class="sect1"><a  name="t2">Assigning an Explicit Size to Data Items</a></h2>


<p><a name="INDEX-2,488" />
<a name="INDEX-2,489" />
<a name="INDEX-2,490" />
Sometimes kernel code requires data items of a specific size, either
to match predefined binary structures[39] or to align data within
structures by inserting "filler'' fields (but please refer to "Data Alignment" later in this chapter for information about
alignment issues).
</p>
<blockquote class="footnote"><a name="FOOTNOTE-39" />
<p>[39]This happens when
reading partition tables, when executing a binary file, or when
decoding a network packet.</p>
</blockquote>


<p><a name="INDEX-2,491" />
<a name="INDEX-2,492" />
<a name="INDEX-2,493" />
<a name="INDEX-2,494" />
The kernel offers the following data types to use whenever you need to
know the size of your data. All the types are declared in
<tt class="literal">&lt;asm/types.h&gt;</tt>, which in turn is included by
<tt class="literal">&lt;linux/types.h&gt;</tt>:
</p>


<blockquote><pre class="code">
u8;   /* unsigned byte (8 bits) */
u16;  /* unsigned word (16 bits) */
u32;  /* unsigned 32-bit value */
u64;  /* unsigned 64-bit value */
</pre></blockquote>


<p><a name="INDEX-2,495" />
<a name="INDEX-2,496" />
These data types are accessible only from kernel code (i.e.,
<tt class="literal">__KERNEL__</tt> must be defined before
including <tt class="literal">&lt;linux/types.h&gt;</tt>). The
corresponding signed types exist, but are rarely needed; just replace
<tt class="literal">u</tt> with <tt class="literal">s</tt> in the name if you need
them.
</p>



<p><a name="INDEX-2,497" />
If a user-space program needs to use these types, it can prefix the
names with a double underscore: <tt class="literal">__u8</tt> and
the other types are defined independent of
<tt class="literal">__KERNEL__</tt>. If, for example, a
driver needs to exchange binary structures with a program running in
user space by means of <em class="emphasis">ioctl</em>, the header files
should declare 32-bit fields in the structures as
<tt class="literal">__u32</tt>.
</p>



<p><a name="INDEX-2,498" />
It's important to remember that these types are Linux specific, and
using them hinders porting software to other Unix flavors. Systems
with recent compilers will support the C99-standard types, such as
<tt class="literal">uint8_t</tt> and <tt class="literal">uint32_t</tt>; when
possible, those types should be used in favor of the Linux-specific
variety. If your code must work with 2.0 kernels, however, use of
these types will not be possible (since only older compilers work with
2.0).
</p>



<p><a name="INDEX-2,499" />
You might also note that sometimes the kernel uses conventional types,
such as <tt class="literal">unsigned int</tt>, for items whose dimension is
architecture independent. This is usually done for backward
compatibility. When <tt class="literal">u32</tt> and friends were introduced
in version 1.1.67, the developers couldn't change existing data
structures to the new types because the compiler issues a warning when
there is a type mismatch <?troff .ne 10?>between the structure field and the value
being assigned to it.[40] Linus didn't
expect the OS he wrote for his own use to become multiplatform; as a
result, old structures are sometimes loosely typed.

</p>
<blockquote class="footnote"><a name="FOOTNOTE-40" />
<p>[40]As a matter of fact, the compiler
signals type inconsistencies even if the two types are just different
names for the same object, like <tt class="literal">unsigned long</tt> and
<tt class="literal">u32</tt> on the PC.</p>
</blockquote>



<h2 class="sect1"><a name="t3">Interface-Specific Types</a></h2>





<p><a name="INDEX-2,500" />
<a name="INDEX-2,501" />
<a name="INDEX-2,502" />
<a name="INDEX-2,503" />
Most of the commonly used data types in the kernel have their own
<tt class="literal">typedef</tt> statements, thus preventing any portability
problems. For example, a process identifier (pid) is usually
<tt class="literal">pid_t</tt> instead of <tt class="literal">int</tt>. Using
<tt class="literal">pid_t</tt> masks any possible difference in the actual
data typing. We use the expression
<em class="emphasis">interface-specific</em> to refer to a type defined by a
library in order to provide an interface to a specific data structure.
</p>



<p>Even when no interface-specific type is defined, it's always important
to use the proper data type in a way consistent with the rest of the
kernel. A jiffy count, for instance, is always <tt class="literal">unsigned
long</tt>, independent of its actual size, so the
<tt class="literal">unsigned long</tt> type should always be used when
working with jiffies. In this section we concentrate on use of
"<tt class="literal">_t</tt>'' types.
</p>



<p>The complete list of <tt class="literal">_t</tt> types appears in
<tt class="literal">&lt;linux/types.h&gt;</tt>, but the list is rarely
useful. When you need a specific type, you'll find it in the
prototype of the functions you need to call or in the data structures
you use.
</p>



<p><a name="INDEX-2,504" />
Whenever your driver uses functions that require such "custom'' types
and you don't follow the convention, the compiler issues a warning; if
you use the <em class="emphasis">-Wall</em> compiler flag and are careful
to remove all the warnings, you can feel confident that your code is
portable.
</p>



<p><a name="INDEX-2,505" />
The main problem with <tt class="literal">_t</tt> data items is that when
you need to print them, it's not always easy to choose the right
<em class="emphasis">printk</em> or <em class="emphasis">printf</em> format, and
warnings you resolve on one architecture reappear on another. For
example, how would you print a <tt class="literal">size_t</tt>, which is
<tt class="literal">unsigned long</tt> on some platforms and
<tt class="literal">unsigned int</tt> on some others?



</p>



<p><a name="INDEX-2,506" />
Whenever you need to print some interface-specific data, the best way
to do it is by casting the value to the biggest possible type (usually
<tt class="literal">long</tt> or <tt class="literal">unsigned long</tt>) and then
printing it through the corresponding format. This kind of tweaking
won't generate errors or warnings because the format matches the type,
and you won't lose data bits because the cast is either a null
operation or an extension of the item to a bigger data type.
</p>



<p>In practice, the data items we're talking about aren't usually meant
to be printed, so the issue applies only to debugging messages. Most
often, the code needs only to store and compare the interface-specific
types, in addition to passing them as arguments to library or kernel
functions.
</p>



<p>Although <tt class="literal">_t</tt> types are the correct solution for most
situations, sometimes the right type doesn't exist. This happens for
some old interfaces that haven't yet been cleaned up.
</p>



<p><a name="INDEX-2,507" />
<a name="INDEX-2,508" />
<a name="INDEX-2,509" />
The one ambiguous point we've found in the kernel headers is data
typing for I/O functions, which is loosely defined (see the section
"Platform Dependencies" in Chapter 8, "Hardware Management"). The loose
typing is mainly there for historical reasons, but it can create
problems when writing code. For example, one can get into trouble by
swapping the arguments to









functions like <em class="emphasis">outb</em>; if there were a
<tt class="literal">port_t</tt> type, the compiler would find this type of
error.


</p>




<h2 class="sect1"><a name="t4">Other Portability Issues</a></h2>


<p><a name="INDEX-2,510" />
In addition to data typing, there are a few other software issues to
keep in mind when writing a driver if you want it to be portable
across Linux platforms.
</p>



<p>A general rule is to be suspicious of explicit constant values.
Usually the code has been parameterized using preprocessor macros.
This section lists the most important portability problems. Whenever
you encounter other values that have been parameterized, you'll be
able to find hints in the header files and in the device drivers
distributed with the official kernel.





</p>



<h3 class="sect2">Time Intervals</h3>


<p><a name="INDEX-2,511" />
<a name="INDEX-2,512" />
<a name="INDEX-2,513" />
<a name="INDEX-2,514" />
When dealing with time intervals, don't assume that there are 100
jiffies per second. Although this is currently true for Linux-x86,
not every Linux platform runs at 100 Hz (as of 2.4 you find values
ranging from 20 to 1200, although 20 is only used in the IA-64
simulator). The assumption can be false even for the x86 if you play
with the <tt class="literal">HZ</tt> value (as some people do), and nobody
knows what will happen in future kernels. Whenever you calculate time
intervals using jiffies, scale your times using <tt class="literal">HZ</tt>
(the number of timer interrupts per second). For example, to check
against a timeout of half a second, compare the elapsed time against
<tt class="literal">HZ/2</tt>. More generally, the number of jiffies
corresponding to <tt class="literal">msec</tt> milliseconds is always
<tt class="literal">msec*HZ/1000</tt>. This detail had to be fixed in many
network drivers when porting them to the Alpha; some of them didn't
work on that platform because they assumed <tt class="literal">HZ</tt> to be 100.

</p>



<h3 class="sect2">Page Size</h3>

<?troff .wcon_off?>

<p><a name="INDEX-2,515" />
<a name="INDEX-2,516" />
<a name="INDEX-2,517" />
<a name="INDEX-2,518" />
<a name="INDEX-2,519" />
<a name="INDEX-2,520" />
When playing games with memory, remember that a memory page is
<tt class="literal">PAGE_SIZE</tt> bytes, not 4 KB. Assuming that the page
size is 4 KB and hard-coding the value is a common error among PC
programmers -- instead, supported platforms show page sizes from 4
KB to 64 KB, and sometimes they differ between different
implementations of the same platform. The relevant macros are
<tt class="literal">PAGE_SIZE</tt> and <tt class="literal">PAGE_SHIFT</tt>. The
latter contains the number of bits to shift an address to get its page
number. The number currently is 12 or greater, for 4 KB and bigger
pages. The macros are defined in
<tt class="literal">&lt;asm/page.h&gt;</tt>; user-space programs can use
<em class="emphasis">getpagesize</em> if they ever need the information.
</p>



<p><a name="INDEX-2,521" />
Let's look at a nontrivial situation. If a driver needs 16 KB for
temporary data, it shouldn't specify an <tt class="literal">order</tt> of
<tt class="literal">2</tt> to <em class="emphasis">get_free_pages</em>. You need
a portable solution.  Using an array of <tt class="literal">#ifdef</tt>
conditionals may work, but it only accounts for platforms you care to
list and would break on other architectures, such as one that might be
supported in the future. We suggest that you use this code instead:
</p>


<blockquote><pre class="code">
int order = (14 - PAGE_SHIFT &gt; 0) ? 14 - PAGE_SHIFT : 0;
buf = get_free_pages(GFP_KERNEL, order);
</pre></blockquote>


<p>The solution depends on the knowledge that 16 KB is
<tt class="literal">1&lt;&lt;14</tt>. The quotient of two numbers is the
difference of their logarithms (orders), and both
<tt class="literal">14</tt> and <tt class="literal">PAGE_SHIFT</tt> are orders.
The value of <tt class="literal">order</tt> is calculated at compile time,
and the implementation shown is a safe way to allocate memory for any
power of two, independent of <tt class="literal">PAGE_SIZE</tt>.


</p>



<h3 class="sect2">Byte Order</h3>


<p><a name="INDEX-2,522" />
<a name="INDEX-2,523" />
<a name="INDEX-2,524" />
Be careful not to make assumptions about byte ordering. Whereas the
PC stores multibyte values low-byte first (little end first, thus
little-endian), most high-level platforms work the other way
(big-endian). Modern processors can operate in either mode, but most
of them prefer to work in big-endian mode; support for little-endian
memory access has been added to interoperate with PC data and Linux
usually prefers to run in the native processor mode. Whenever
possible, your code should be written such that it does not care about
byte ordering in the data it manipulates. However, sometimes a driver
needs to build an integer number out of single bytes or do the
opposite.
</p>



<p><a name="INDEX-2,525" />
<a name="INDEX-2,526" />
<a name="INDEX-2,527" />
<a name="INDEX-2,528" />
You'll need to deal with endianness when you fill in network packet
headers, for example, or when you are dealing with a peripheral that
operates in a specific byte ordering mode. In that case, the code
should include <tt class="literal">&lt;asm/byteorder.h&gt;</tt> and should
check whether <tt class="literal">__BIG_ENDIAN</tt> or
<tt class="literal">__LITTLE_ENDIAN</tt> is defined by the header.
</p>



<p><a name="INDEX-2,529" />
<a name="INDEX-2,530" />
You could code a bunch of <tt class="literal">#ifdef __LITTLE_ENDIAN</tt> conditionals, but there is a better
way. The Linux kernel defines a set of macros that handle conversions
between the processor's byte ordering and that of the data you need to
store or load in a specific byte order. For example:
</p>


<blockquote><pre class="code">
u32 __cpu_to_le32 (u32);
u32 __le32_to_cpu (u32);
</pre></blockquote>


<p>These two macros convert a value from whatever the CPU uses to
an unsigned, little-endian, 32-bit quantity and back. They work
whether your CPU is big-endian or little-endian, and, for that matter,
whether it is a 32-bit processor or not. They return their argument
unchanged in cases where there is no work to be done. Use of these
macros makes it easy to write portable code without having to use a
lot of conditional compilation constructs.
</p>



<p>There are dozens of similar routines; you can see the full list in
<tt class="literal">&lt;linux/byteorder/big_endian.h&gt;</tt> and
<tt class="literal">&lt;linux/byteorder/little_endian.h&gt;</tt>. After a
while, the pattern is not hard to follow.
<em class="emphasis">__be64_to_cpu</em> converts an unsigned,
big-endian, 64-bit value to the internal CPU representation.
<em class="emphasis">__le16_to_cpus</em>, instead, handles signed,
little-endian, 16-bit quantities. When dealing with pointers, you can
also use functions like <em class="emphasis">__cpu_to_le32p</em>,
which take a pointer to the value to be converted rather than the
value itself. See the include file for the rest.
</p>



<p>Not all Linux versions defined all the macros that deal with byte
ordering. In particular, the <em class="filename">linux/byteorder</em>directory appeared in version 2.1.72 to make order in the various
<tt class="literal">&lt;asm/byteorder.h&gt;</tt> files and remove duplicate
definitions. If you use our <em class="emphasis">sysdep.h</em>, you'll be
able to use all of the macros available in Linux 2.4 when compiling
code for 2.0 or 2.2.
</p>


<a name="judasalign" />
<h3 class="sect2">Data Alignment</h3>


<p><a name="INDEX-2,531" />
<a name="INDEX-2,532" />
<a name="INDEX-2,533" />
<a name="INDEX-2,534" />
<a name="INDEX-2,535" />
<a name="INDEX-2,536" />
<a name="INDEX-2,537" />
The last problem worth considering when writing portable code is how
to access unaligned data -- for example, how to read a four-byte
value stored at an address that isn't a multiple of four bytes. PC
users often access unaligned data items, but few architectures permit
it. Most modern architectures generate an exception every time the
program tries unaligned data transfers; data transfer is handled by
the exception handler, with a great performance penalty. If you need
to access unaligned data, you should use the following macros:
</p>


<blockquote><pre class="code">
#include &lt;asm/unaligned.h&gt;
get_unaligned(ptr);
put_unaligned(val, ptr);
</pre></blockquote>


<p>These macros are typeless and work for every data item, whether it's
one, two, four, or eight bytes long. They are defined with any kernel
version.
</p>



<p><a name="INDEX-2,538" />
Another issue related to alignment is portability of data structures
across platforms. The same data structure (as defined in the
C-language source file) can be compiled differently on different
platforms. The compiler arranges structure fields to be aligned
according to conventions that differ from platform to platform. At
least in theory, the compiler can even reorder structure fields in
order to optimize memory usage.[41]
</p>
<blockquote class="footnote"><a name="FOOTNOTE-41" />
<p>[41]Field reordering
doesn't happen in currently supported architectures because it could
break interoperability with existing code, but a new architecture may
define field reordering rules for structures with holes due to
alignment restrictions.</p>
</blockquote>


<p><a name="INDEX-2,539" />
In order to write data structures for data items that can be moved
across architectures, you should always enforce natural alignment of
the data items in addition to standardizing on a specific
endianness. <em class="emphasis">Natural alignment</em> means storing data
items at an address that is a multiple of their size (for instance,
8-byte items go in an address multiple of 8). To enforce natural
alignment while preventing the compiler from moving fields around, you
should use filler fields that avoid leaving holes in the data
structure.
</p>



<p><a name="INDEX-2,540" />
<a name="INDEX-2,541" />
<a name="INDEX-2,542" />
To show how alignment is enforced by the compiler, the
<em class="application">dataalign</em> program is distributed in the
<em class="filename">misc-progs</em> directory of the sample code, and an
equivalent <em class="application">kdataalign</em> module is part of
<em class="filename">misc-modules</em>. This is the output of the program
on several platforms and the output of the module on the SPARC64:
</p>


<blockquote><pre class="code">arch  Align:  char  short  int  long   ptr long-long  u8 u16 u32 u64
i386            1     2     4     4     4     4        1   2   4   4
i686            1     2     4     4     4     4        1   2   4   4
alpha           1     2     4     8     8     8        1   2   4   8
armv4l          1     2     4     4     4     4        1   2   4   4
ia64            1     2     4     8     8     8        1   2   4   8
mips            1     2     4     4     4     8        1   2   4   8
ppc             1     2     4     4     4     8        1   2   4   8
sparc           1     2     4     4     4     8        1   2   4   8
sparc64         1     2     4     4     4     8        1   2   4   8

kernel: arch  Align: char short int long  ptr long-long u8 u16 u32 u64
kernel: sparc64        1    2    4    8    8     8       1   2   4   8
</pre></blockquote>


<p>It's interesting to note that not all platforms align 64-bit values on
64-bit boundaries, so you'll need filler fields to enforce alignment
and ensure portability.

</p>




<h2 class="sect1"><a name="t5">Linked Lists</a></h2>


<p><a name="INDEX-2,543" />
<a name="INDEX-2,544" />
<a name="INDEX-2,545" />
Operating system kernels, like many other programs, often need to
maintain lists of data structures. The Linux kernel has, at times,
been host to several linked list implementations at the same time. To
reduce the amount of duplicated code, the kernel developers have
created a standard implementation of circular, doubly-linked lists;
others needing to manipulate lists are encouraged to use this
facility, introduced in version 2.1.45 of the kernel.
</p>



<p><a name="INDEX-2,546" />
<a name="INDEX-2,547" />
<a name="INDEX-2,548" />
To use the list mechanism, your driver must include the file
<tt class="literal">&lt;linux/list.h&gt;</tt>. This file defines a simple
structure of type <tt class="literal">list_head</tt>:
</p>


<blockquote><pre class="code">
struct list_head {
    struct list_head *next, *prev;
};
</pre></blockquote>


<p>Linked lists used in real code are almost invariably made up of some
type of structure, each one describing one entry in the list. To use
the Linux list facility in your code, you need only embed a
<tt class="literal">list_head</tt> inside the structures that make up the
list. If your driver maintains a list of things to do, say, its
declaration would look something like this:
</p>


<blockquote><pre class="code">
struct todo_struct {
    struct list_head list;
    int priority; /* driver specific */
    /* ... add other driver-specific fields */
};
</pre></blockquote>


<p><a name="INDEX-2,549" />
The head of the list must be a standalone <tt class="literal">list_head</tt>
structure. List heads must be initialized prior to use with the
<tt class="literal">INIT_LIST_HEAD</tt> macro. A "things to do'' list head
could be declared and initialized with:
</p>


<blockquote><pre class="code">
struct list_head todo_list;

INIT_LIST_HEAD(&amp;todo_list);
</pre></blockquote>


<p>Alternatively, lists can be initialized at compile time as follows:
</p>


<blockquote><pre class="code">
LIST_HEAD(todo_list);
</pre></blockquote>


<p>Several functions are defined in
<tt class="literal">&lt;linux/list.h&gt;</tt> that work with lists:
</p>


<dl>
<dt><b><tt class="literal">list_add(struct list_head *new, struct list_head *head);</tt></b></dt><dd>
<p><a name="INDEX-2,550" />
This function adds the <tt class="literal">new</tt> entry immediately after
the list head -- normally at the beginning of the list. It can
thus be used to build stacks. Note, however, that the
<tt class="literal">head</tt> need not be the nominal head of the list; if
you pass a <tt class="literal">list_head</tt> structure that happens to be
in the middle of the list somewhere, the new entry will go immediately
after it. Since Linux lists are circular, the head of the list is not
generally different from any other entry.
</p>

</dd>
<p>


<dt><b><tt class="literal">list_add_tail(struct list_head *new, struct list_head *head);</tt>
</b></dt><dd>
<p><a name="INDEX-2,551" />
Add a new entry just before the given list head -- at the end of the
list, in other words. <em class="emphasis">list_add_tail</em> can thus be
used to build first-in first-out queues.
</p>

</dd>
<p>


<dt><b><tt class="literal">list_del(struct list_head *entry);</tt></b></dt><dd>
<p><a name="INDEX-2,552" />
The given entry is removed from the list.
</p>

</dd>
<p>


<dt><b><tt class="literal">list_empty(struct list_head *head);</tt></b></dt><dd>
<p><a name="INDEX-2,553" />
Returns a nonzero value if the given list is empty.
</p>

</dd>
<p>


<dt><b><tt class="literal">list_splice(struct list_head *list, struct list_head *head);</tt></b></dt><dd>
<p><a name="INDEX-2,554" />
This function joins two lists by inserting <tt class="literal">list</tt>
immediately after <tt class="literal">head</tt>.
</p>

</dd>
<p>

</dl>


<p><a name="INDEX-2,555" />
The <tt class="literal">list_head</tt> structures are good for implementing
a list of like structures, but the invoking program is usually more
interested in the larger structures that make up the list as a whole.
A macro, <em class="emphasis">list_entry</em>, is provided that <?troff .ne 10?>will map a
<tt class="literal">list_head</tt> structure pointer back into a pointer to
the structure that contains it. It is invoked as follows:
</p>


<blockquote><pre class="code">
list_entry(struct list_head *ptr, type_of_struct, field_name);
</pre></blockquote>


<p>where <tt class="literal">ptr</tt> is a pointer to the <tt class="literal">struct
list_head</tt> being used, <tt class="literal">type_of_struct</tt> is
the type of the structure containing the <tt class="literal">ptr</tt>, and
<tt class="literal">field_name</tt> is the name of the list field within the
structure. In our <tt class="literal">todo_struct</tt> structure from
before, the list field is called simply <tt class="literal">list</tt>. Thus,
we would turn a list entry into its containing structure with a line
like this:
</p>


<blockquote><pre class="code">
struct todo_struct *todo_ptr =
    list_entry(listptr, struct todo_struct, list);
</pre></blockquote>


<p>The <em class="emphasis">list_entry</em> macro takes a little getting used
to, but is not that hard to use.
</p>



<p><a name="INDEX-2,556" />
The traversal of linked lists is easy: one need only follow the
<tt class="literal">prev</tt> and <tt class="literal">next</tt> pointers. As an
example, suppose we want to keep the list of
<tt class="literal">todo_struct</tt> items sorted in descending priority
order. A function to add a new entry would look something like this:
</p>


<blockquote><pre class="code">
void todo_add_entry(struct todo_struct *new)
{
    struct list_head *ptr;
    struct todo_struct *entry;

    for (ptr = todo_list.next; ptr != &amp;todo_list; ptr = ptr-&gt;next) {
        entry = list_entry(ptr, struct todo_struct, list);
        if (entry-&gt;priority &lt; new-&gt;priority) {
            list_add_tail(&amp;new-&gt;list, ptr);
            return;
        }
    }
    list_add_tail(&amp;new-&gt;list, &amp;todo_struct)
}
</pre></blockquote>


<p>The <tt class="literal">&lt;linux/list.h&gt;</tt> file also defines a macro
<em class="emphasis">list_for_each</em> that expands to the
<tt class="literal">for</tt> loop used in this code. As you may suspect, you
must be careful when modifying the list while traversing it.
</p>



<p>Figure 10-1 shows how the simple <tt class="literal">struct
list_head</tt> is used to maintain a list of data structures.
</p>

<center>
<a name="figlist" /><img src="figs/ldr2_1001.gif" alt="Figure 10-1" />
<h4 class="objtitle">Figure 10-1. The list_head data structure</h4>
</center>


<p><a name="INDEX-2,557" />
<a name="INDEX-2,558" />
<a name="INDEX-2,559" />
<a name="INDEX-2,560" />
<a name="INDEX-2,561" />
<a name="INDEX-2,562" />
<a name="INDEX-2,563" />
Although not all features exported by the <em class="filename">list.h</em>as it appears in Linux 2.4 are available with older kernels, our
<em class="filename">sysdep.h</em> fills the gap by declaring all macros
and functions for use in older kernels.


</p>




<h2 class="sect1"><a name="t6">Quick Reference</a></h2>


<p>The following symbols were introduced in this chapter.
</p>


<dl>
<dt><b><tt class="literal">#include &lt;linux/types.h&gt;</tt></b></dt><dt><b><tt class="literal">typedef u8;</tt></b></dt><dt><b><tt class="literal">typedef u16;</tt></b></dt><dt><b><tt class="literal">typedef u32;</tt></b></dt><dt><b><tt class="literal">typedef u64;</tt></b></dt><dd>
<p><a name="INDEX-2,564" />
<a name="INDEX-2,565" />
<a name="INDEX-2,566" />
These types are guaranteed to be 8-, 16-, 32-, and 64-bit unsigned
integer values. The equivalent signed types exist as well. In user
space, you can refer to the types as <tt class="literal">__u8</tt>,
<tt class="literal">__u16</tt>, and so forth.
</p>

</dd>
<p>


<dt><b><tt class="literal">#include &lt;asm/page.h&gt;</tt></b></dt><dt><b><tt class="literal">PAGE_SIZE</tt></b></dt><dt><b><tt class="literal">PAGE_SHIFT</tt></b></dt><dd>
<p><a name="INDEX-2,567" />
<a name="INDEX-2,568" />
<a name="INDEX-2,569" />
<a name="INDEX-2,570" />
These symbols define the number of bytes per page for the current
architecture and the number of bits in the page offset (12 for 4-KB
pages and 13 for 8-KB pages).
</p>

</dd>
<p>


<dt><b><tt class="literal">#include &lt;asm/byteorder.h&gt;</tt></b></dt><dt><b><tt class="literal">__LITTLE_ENDIAN</tt></b></dt><dt><b><tt class="literal">__BIG_ENDIAN</tt></b></dt><dd>
<p><a name="INDEX-2,571" />
<a name="INDEX-2,572" />
<a name="INDEX-2,573" />
<a name="INDEX-2,574" />
Only one of the two symbols is defined, depending on the architecture.
</p>

</dd>
<p>


<dt><b><tt class="literal">#include &lt;asm/byteorder.h&gt;</tt></b></dt><dt><b><tt class="literal">u32 __cpu_to_le32 (u32);</tt></b></dt><dt><b><tt class="literal">u32 __le32_to_cpu (u32);</tt></b></dt><dt><b></b></dt><dd>
<p><a name="INDEX-2,575" />
<a name="INDEX-2,576" />
Functions for converting between known byte orders and that of the
processor. There are more than 60 such functions; see the various
files in <em class="filename">include/linux/byteorder/</em> for a full list
and the ways in which they are defined.
</p>

</dd>
<p>


<dt><b><tt class="literal">#include &lt;asm/unaligned.h&gt;</tt></b></dt><dt><b><tt class="literal">get_unaligned(ptr);</tt></b></dt><dt><b><tt class="literal">put_unaligned(val, ptr);</tt></b></dt><dd>
<p><a name="INDEX-2,577" />
<a name="INDEX-2,578" />
<a name="INDEX-2,579" />
<a name="INDEX-2,580" />
Some architectures need to protect unaligned data access using these
macros. The macros expand to normal pointer dereferencing for
architectures that permit you to access unaligned data.
</p>

</dd>
<p>


<dt><b><tt class="literal">#include &lt;linux/list.h&gt;</tt></b></dt><dt><b><tt class="literal">list_add(struct list_head *new, struct list_head *head);</tt></b></dt><dt><b><tt class="literal">list_add_tail(struct list_head *new, struct list_head *head);</tt></b></dt><dt><b><tt class="literal">list_del(struct list_head *entry);</tt></b></dt><dt><b><tt class="literal">list_empty(struct list_head *head);</tt></b></dt><dt><b><tt class="literal">list_entry(entry, type, member);</tt></b></dt><dt><b><tt class="literal">list_splice(struct list_head *list, struct list_head *head);</tt></b></dt><dd>
<p><a name="INDEX-2,581" />
<a name="INDEX-2,582" />
<a name="INDEX-2,583" />
<a name="INDEX-2,584" />
<a name="INDEX-2,585" />
<a name="INDEX-2,586" />
<a name="INDEX-2,587" />
<a name="INDEX-2,588" />
Functions for manipulating circular, doubly linked lists.
</p>

</dd>
<p>












</dl>

</blockquote>


<!-- End of sample chapter -->
<center>
<hr noshade size=1>
<TABLE WIDTH="515" BORDER="0" CELLSPACING="0" CELLPADDING="0">

<TR>
<TD ALIGN="LEFT" VALIGN="TOP" WIDTH="172">
<A HREF="ch09.html">
<IMG SRC="figs/txtpreva.gif" ALT="Previous" BORDER="0"></a>
</td>

<TD ALIGN="CENTER" VALIGN="TOP" WIDTH="171">
<A HREF="inx.html">
<IMG SRC="figs/index.gif" ALT="Index" BORDER="0"></a>
</td>

<TD ALIGN="RIGHT" VALIGN="TOP" WIDTH="172">
<A HREF="ch11.html">
<IMG SRC="figs/txtnexta.gif" ALT="Next" BORDER="0"></a>
</td>
</tr>

</table>
<hr noshade size=1>
</center>


<p><b>Back to: <a href="index.html">Table of Contents</a></b>
<p><b>Back to: <a href="http://www.oreilly.com/catalog/linuxdrive2/">Linux Device Drivers, 2nd Edition</a></b>

<!-- O'Reilly Footer Begins Here -->

<CENTER>
<HR SIZE="1" NOSHADE>
<FONT SIZE="1" FACE="Verdana, Arial, Helvetica">
<A HREF="http://www.oreilly.com/">
<B>oreilly.com Home</B></A> <B> | </B>
<A HREF="http://www.oreilly.com/sales/bookstores">
<B>O'Reilly Bookstores</B></A> <B> | </B>
<A HREF="http://www.oreilly.com/order_new/">
<B>How to Order</B></A> <B> | </B>
<A HREF="http://www.oreilly.com/oreilly/contact.html">
<B>O'Reilly Contacts<BR></B></A>
<A HREF="http://www.oreilly.com/international/">
<B>International</B></A> <B> | </B>
<A HREF="http://www.oreilly.com/oreilly/about.html">
<B>About O'Reilly</B></A> <B> | </B>
<A HREF="http://www.oreilly.com/affiliates.html">
<B>Affiliated Companies</B></A> <B> | </B>
<a href="http://www.oreilly.com/privacy_policy.html">
<b>Privacy Policy</b></a><p>
<EM>&copy; 2001, O'Reilly &amp; Associates, Inc.</EM>
</FONT>
</CENTER>

<!-- O'Reilly Footer Ends Here -->

</BODY>
</html>
