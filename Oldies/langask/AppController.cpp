#include "StdAfx.h"
#include "AppController.h"

CAppController::CAppController(void)
{
    iModel = new CAppModel( this );
}

CAppController::~CAppController(void)
{
    delete iModel;
    iModel = NULL;
}

int CAppController::Initialize( MAppController* aView )
{
    if ( !aView )
    {
        return -1;
    }
    iView = aView;
    int retTemp;
    retTemp = ReadRegistry();
    if ( retTemp < 0 )
    {
        return retTemp;
    }
    retTemp = ShowCategories();
    if ( retTemp < 0 )
    {
        return retTemp;
    }
    retTemp = iModel->InitializeLangBase();
    if ( retTemp < 0 )
    {
        return retTemp;
    }
    retTemp = ShowVariants();
    if ( retTemp < 0 )
    {
        return retTemp;
    }
    retTemp = ShowRandomization();
    if ( retTemp < 0 )
    {
        return retTemp;
    }
    return 0;
}

int CAppController::ReadRegistry()
{
    HKEY Key;
    LONG lResult;
    LPTSTR subKeyPtr;
    CString subKey = _T("Software\\LangAsk\\Settings");
    subKeyPtr = subKey.GetBuffer();
    lResult = RegOpenKeyEx(HKEY_CURRENT_USER, subKeyPtr, 0, KEY_QUERY_VALUE, &Key);
    subKey.ReleaseBuffer();
    if (lResult != ERROR_SUCCESS)
    {
        return -1;
    }
    // Get category
    CString category = _T("Category");
    LPTSTR categoryPtr = category.GetBuffer();
    LPTSTR categoryVal = FetchRegistryStringValue(Key, categoryPtr);
    category.ReleaseBuffer();
    iCategory = categoryVal;
    delete [] categoryVal;
    // Get first variant
    CString firstVar = _T("FirstVariant");
    LPTSTR firstVarPtr = firstVar.GetBuffer();
    LPTSTR firstVarVal = FetchRegistryStringValue(Key, firstVarPtr);
    firstVar.ReleaseBuffer();
    iFirstVar = firstVarVal;
    delete [] firstVarVal;
    // Get second variant
    CString secondVar = _T("SecondVariant");
    LPTSTR secondVarPtr = secondVar.GetBuffer();
    LPTSTR secondVarVal = FetchRegistryStringValue(Key, secondVarPtr);
    secondVar.ReleaseBuffer();
    iSecondVar = secondVarVal;
    delete [] secondVarVal;
    // Get randomization
    CString randomize = _T("Randomize");
    LPTSTR randomizePtr = randomize.GetBuffer();
    LPTSTR randomizeVal = FetchRegistryStringValue(Key, randomizePtr);
    randomize.ReleaseBuffer();
    iRandomize = randomizeVal;
    delete [] randomizeVal;
    RegCloseKey(Key);
    return 0;
}

LPTSTR CAppController::FetchRegistryStringValue(HKEY szKey, LPTSTR szValue)
{
    LONG lResult;
    DWORD dwType;
    DWORD dwDataLen;
    LPBYTE RetCode;
    dwType = REG_SZ;
    lResult = RegQueryValueEx(szKey, szValue, NULL, &dwType, NULL, &dwDataLen);
    if (lResult != ERROR_SUCCESS)
    {
        return NULL;
    }
    RetCode = new BYTE[dwDataLen];
    if (!RetCode)
    {
        return NULL;
    }
    lResult = RegQueryValueEx(szKey, szValue, NULL, &dwType, RetCode, &dwDataLen);
    if (lResult != ERROR_SUCCESS)
    {
        return NULL;
    }
    return (LPTSTR)RetCode;
}

int CAppController::ShowCategories()
{
    TCHAR fileName[MAX_PATH];
    CString finddef(_T("\\data\\*"));
    GetModuleFileName(AfxGetInstanceHandle(), fileName, MAX_PATH);
    CString progDir(fileName);
    progDir = progDir.Left(progDir.ReverseFind('\\')) + finddef;
    // Find files
    CString firstFile;
    WIN32_FIND_DATA findFileData;
    HANDLE hFind = INVALID_HANDLE_VALUE;
    LPTSTR dirSpecPtr = progDir.GetBuffer();
    hFind = FindFirstFile(dirSpecPtr, &findFileData);
    progDir.ReleaseBuffer();
    if ( hFind == INVALID_HANDLE_VALUE )
    {
        FindClose(hFind);
        return -1;
    }
    if (!(findFileData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY))
    {
        FindClose(hFind);
        return -1;
    }
    int retTemp;
    int nTries = 0;
    int nAdded = 0;
    while (FindNextFile(hFind,&findFileData) != 0)
    {
        if (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            if (nTries == 0)
            {
                // Skip ".."
                nTries = 1;
                continue;
            }
            if (firstFile == "")
            {
                firstFile = findFileData.cFileName;
            }
            retTemp = iView->AddCategoryString( findFileData.cFileName );
            if ( retTemp == 0 )
            {
                nAdded++;
            }
        }
    }
    if ( nAdded <= 0 )
    {
        FindClose( hFind );
        return -1;
    }
    LPTSTR selection = iCategory.GetBuffer();
    retTemp = iView->SetCategorySelection( selection );
    iCategory.ReleaseBuffer();
    if ( retTemp < 0 )
    {
        iCategory = firstFile;
    }
    FindClose( hFind );
    return 0;
}

int CAppController::ShowVariants()
{
    CString selection;
    iView->GetCategorySelection( selection );
    selection += _T("\\dict_???.txt");
    // Get program's directory
    TCHAR fileName[MAX_PATH];
    GetModuleFileName( AfxGetInstanceHandle(), fileName, MAX_PATH );
    CString progDir( fileName );
    progDir = progDir.Left(progDir.ReverseFind('\\')) + _T("\\Data\\") + selection;
    // Find files
    WIN32_FIND_DATA findFileData;
    HANDLE hFind = INVALID_HANDLE_VALUE;
    LPTSTR dirSpecPtr = progDir.GetBuffer();
    hFind = FindFirstFile( dirSpecPtr, &findFileData );
    progDir.ReleaseBuffer();
    if (hFind == INVALID_HANDLE_VALUE)
    {
        FindClose(hFind);
        return -1;
    }
    CString newStr = iModel->GetLangFromFilename( findFileData.cFileName );
    iView->AddVariant1String( newStr );
    iView->AddVariant2String( newStr );
    while (FindNextFile(hFind,&findFileData) != 0)
    {
        newStr = iModel->GetLangFromFilename(findFileData.cFileName);
        iView->AddVariant1String( newStr );
        iView->AddVariant2String( newStr );
    }
    FindClose(hFind);
    if (iView->GetVariant1Count() < 2)
    {
        return -1;
    }
    // Set indices
    int foundIndex1 = GetVariant1IndexForIsoForm( iFirstVar );
    int foundIndex2 = GetVariant2IndexForIsoForm( iSecondVar );
    if ( foundIndex1<0 || foundIndex2<0 )
    {
        SetVariantTexts( 0, 1 );
        foundIndex1 = 0;
        foundIndex2 = 1;
    }
    iView->SetCurrentVariant1Index( foundIndex1 );
    iView->SetCurrentVariant2Index( foundIndex2 );
    return 0;
}

int CAppController::ShowRandomization()
{
    CString trueString = _T("true");
    CString falseString = _T("false");
    if ( iRandomize!=trueString && iRandomize!=falseString )
    {
        return -1;
    }
    if ( iRandomize == trueString )
    {
        iView->InitializeRandomizationState();
    }
    return 0;
}

int CAppController::GetVariant1IndexForIsoForm( CString& aIsoForm )
{
    LPTSTR variantPtr = aIsoForm.GetBuffer();
    char* searchStr = iModel->ConvertWideCharToMultiByte( variantPtr );
    aIsoForm.ReleaseBuffer();
    char* foundStr = iModel->GetLanguageByIsoForm( searchStr );
    delete [] searchStr;
    if ( !foundStr )
    {
        return -1;
    }
    LPWSTR converted = iModel->ConvertMultiByteToWideChar( foundStr );
    delete [] foundStr;
    int foundIndex = iView->GetVariant1Index( converted );
    delete [] converted;
    return foundIndex;
}

int CAppController::GetVariant2IndexForIsoForm( CString& aIsoForm )
{
    LPTSTR variantPtr = aIsoForm.GetBuffer();
    char* searchStr = iModel->ConvertWideCharToMultiByte( variantPtr );
    aIsoForm.ReleaseBuffer();
    char* foundStr = iModel->GetLanguageByIsoForm( searchStr );
    delete [] searchStr;
    if ( !foundStr )
    {
        return -1;
    }
    LPWSTR converted = iModel->ConvertMultiByteToWideChar( foundStr );
    delete [] foundStr;
    int foundIndex = iView->GetVariant2Index( converted );
    delete [] converted;
    return foundIndex;
}

int CAppController::SetVariantTexts( int aFirstIndex, int aSecondIndex )
{
    // Set first
    char* isoForm1 = iModel->GetIsoFormByIndex( aFirstIndex );
    LPWSTR isoForm1W = iModel->ConvertMultiByteToWideChar( isoForm1 );
    delete [] isoForm1;
    iFirstVar = isoForm1W;
    delete [] isoForm1W;
    // Set second
    char* isoForm2 = iModel->GetIsoFormByIndex( aSecondIndex );
    LPWSTR isoForm2W = iModel->ConvertMultiByteToWideChar( isoForm2 );
    delete [] isoForm2;
    iSecondVar = isoForm2W;
    delete [] isoForm2W;
    return 0;
}

// From View

void CAppController::NotifyCategoryChanged()
{
    iModel->ChangeCategory();
}

void CAppController::NotifyFirstVariantChanged()
{
    iModel->ChangeFirstVariant();
}

void CAppController::NotifySecondVariantChanged()
{
    iModel->ChangeSecondVariant();
}

void CAppController::NotifyRandomizeChanged()
{
    CString trueString(_T("true"));
    CString falseString(_T("false"));
    if ( iRandomize == trueString )
    {
        iRandomize = falseString;
        iView->SetRandomizationState( true );
    }
    else
    {
        iRandomize = trueString;
        iView->SetRandomizationState( false );
    }
}

void CAppController::NotifyEnterAnswerPressed()
{
    iModel->EnterAnswer();
}
    
// From Model

void CAppController::NotifyNewDatabaseReady()
{
}

void CAppController::NotifyRightAnswer()
{
}

void CAppController::NotifyWrongAnswer()
{
}
