#pragma once

class CDatabase
{
public:
    CDatabase(void);
    ~CDatabase(void);
    void SetFirstFilename(char* aFilename);
    void SetSecondFilename(char* aFilename);
    int Initialize();
    int GetVariant(char*& aVariant, int& aVariantLen, int& aVariantNum, bool aGetFirst);
    int GiveAnswer(char* aVariant, char*& aCorrect, int& aCorrectLen, bool aGiveSecond);
    int GetTotalPairs();
    int IncCurrentPos();
    int Restart();
private:
    int GenerateTables();
    char* CopyString(char* aSrcString, int* aDstLength);
    void SeekToStart();
    int ReadOneLine();
private:
    FILE* iFirstFileP;
    FILE* iSecondFileP;
    char* iFirstFilename;
    char* iSecondFilename;
    char* iFirstVariant;
    int iFirstVarSize;
    char* iSecondVariant;
    int iSecondVarSize;
    int* iSkiptable;
    char* iTypetable;
    int iCurrentPos;
    int iTotalPairs;
};
