#include "StdAfx.h"
#include "AppModel.h"

CAppModel::CAppModel( MAppModel* aCallback ) :
iCallback( aCallback ),
iLangbase( NULL ),
iBaseTotal( 0 )
{
}

CAppModel::~CAppModel(void)
{
    Uninitialize();
}

void CAppModel::Uninitialize()
{
    delete iLangbase;
    iLangbase = NULL;
}

int CAppModel::InitializeLangBase()
{
    CStringA progDir;
    TCHAR fileName[MAX_PATH];
    CString basename(_T("\\data\\iso639-2.txt"));
    GetModuleFileName(AfxGetInstanceHandle(), fileName, MAX_PATH);
    progDir = fileName;
    progDir = progDir.Left(progDir.ReverseFind('\\'));
    progDir += basename;
    // Initialize langbase
    iLangbase = new CLangbase;
    LPSTR charPtr = progDir.GetBuffer();
    iLangbase->SetFilename(charPtr);
    progDir.ReleaseBuffer();
    int retVal = iLangbase->Initialize();
    if (retVal < 0)
    {
        return retVal;
    }
    iBaseTotal = iLangbase->GetTotalPairs();
    return 0;
}

CString CAppModel::GetLangFromFilename(LPTSTR aFilename)
{
    CString returnStr;
    CString isoForm(aFilename);
    isoForm = isoForm.Mid(5, 3);
    LPTSTR isoFormPtr = isoForm.GetBuffer();
    char* searchStr = ConvertWideCharToMultiByte(isoFormPtr);
    isoForm.ReleaseBuffer();
    char* foundStr = iLangbase->GetLanguageByIsoForm(searchStr);
    delete [] searchStr;
    if (foundStr)
    {
        LPWSTR converted = ConvertMultiByteToWideChar(foundStr);
        returnStr = converted;
        delete [] converted;
        delete [] foundStr;
    }
    return returnStr;
}

LPWSTR CAppModel::ConvertMultiByteToWideChar(char* aMultiByte)
{
    int strLen = MultiByteToWideChar(CP_UTF8, 0, aMultiByte, -1, NULL, 0);
    wchar_t* result = new wchar_t[strLen];
    MultiByteToWideChar(CP_UTF8, 0, aMultiByte, -1, result, strLen);
    return result;
}

char* CAppModel::ConvertWideCharToMultiByte(LPWSTR aWideChar)
{
    int strLen = WideCharToMultiByte( CP_UTF8, 0, aWideChar, -1, NULL, 0, 0, 0 );
    char* result = new char[ strLen ];
    WideCharToMultiByte( CP_UTF8, 0, aWideChar, -1, result, strLen, 0, 0 );
    return result;
}

char* CAppModel::GetIsoFormByIndex( int aIndex )
{
    return iLangbase->GetIsoFormByIndex( aIndex );
}

char* CAppModel::GetLanguageByIsoForm( char* aIsoForm )
{
    return iLangbase->GetLanguageByIsoForm( aIsoForm );
}

void CAppModel::ChangeCategory()
{
}

void CAppModel::ChangeFirstVariant()
{
}

void CAppModel::ChangeSecondVariant()
{
}

void CAppModel::EnterAnswer()
{
}
