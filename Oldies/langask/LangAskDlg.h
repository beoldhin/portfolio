// LangAskDlg.h : header file
//

#pragma once

#include "AppController.h"


// CLangAskDlg dialog
class CLangAskDlg : public CDialog,
                    public MAppController
{
// Construction
public:
	CLangAskDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_LANGASK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
// From MAppController
    int AddCategoryString( LPTSTR aString );
    int SetCategorySelection( LPTSTR aString );
    int GetCategorySelection( CString& aString );
    int AddVariant1String( CString& aString );
    int AddVariant2String( CString& aString );
    int GetVariant1Count();
    int GetVariant1Index( LPWSTR aString );
    int GetVariant2Index( LPWSTR aString );
    void SetCurrentVariant1Index( int aIndex );
    void SetCurrentVariant2Index( int aIndex );
    void InitializeRandomizationState();
    void SetRandomizationState( bool aState );
public:
	afx_msg void OnCbnSelendokComboboxCategory();
public:
	afx_msg void OnCbnSelendokComboboxVariant1();
public:
	afx_msg void OnCbnSelendokComboboxVariant2();
public:
	afx_msg void OnBnClickedCheckRandomize();
public:
	afx_msg void OnBnClickedButtonEnteranswer();
public:
	afx_msg void OnBnClickedButtonEdit();
public:
	afx_msg void OnBnClickedButtonRestart();
private:
    CAppController* iController;
    afx_msg void OnBnClickedOk();
};
