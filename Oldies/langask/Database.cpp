#include "StdAfx.h"
#include ".\database.h"

CDatabase::CDatabase(void) :
iFirstFileP(NULL),
iSecondFileP(NULL),
iFirstVariant(NULL),
iFirstVarSize(80),
iSecondVariant(NULL),
iSecondVarSize(80),
iSkiptable(NULL),
iTypetable(NULL),
iCurrentPos(0),
iTotalPairs(0)
{
}

CDatabase::~CDatabase(void)
{
    if (iFirstFileP)
    {
        fclose(iFirstFileP);
        iFirstFileP = NULL;
    }
    if (iSecondFileP)
    {
        fclose(iSecondFileP);
        iSecondFileP = NULL;
    }
    if (iFirstFilename)
    {
        delete [] iFirstFilename;
        iFirstFilename = NULL;
    }
    if (iSecondFilename)
    {
        delete [] iSecondFilename;
        iSecondFilename = NULL;
    }
    if (iFirstVariant)
    {
        delete [] iFirstVariant;
        iFirstVariant = NULL;
    }
    if (iSecondVariant)
    {
        delete [] iSecondVariant;
        iSecondVariant = NULL;
    }
    if (iSkiptable)
    {
        delete [] iSkiptable;
        iSkiptable = NULL;
    }
    if (iTypetable)
    {
        delete [] iTypetable;
        iTypetable = NULL;
    }
}

void CDatabase::SetFirstFilename(char* aFilename)
{
    iFirstFilename = CopyString(aFilename, NULL);
}

void CDatabase::SetSecondFilename(char* aFilename)
{
    iSecondFilename = CopyString(aFilename, NULL);
}

int CDatabase::Initialize()
{
    if (!iFirstFilename || !iSecondFilename)
    {
        return -1;
    }
    iFirstFileP = fopen(iFirstFilename, "rb");
    if (!iFirstFileP)
        {
        return errno;
        }
    iSecondFileP = fopen(iSecondFilename, "rb");
    if (!iSecondFileP)
    {
        return errno;
    }
    iFirstVariant = new char[iFirstVarSize];
    iSecondVariant = new char[iSecondVarSize];
    // Get number of pairs
    int numOfPairs = 0;
    SeekToStart();
    while (ReadOneLine() >= 0)
        {
        numOfPairs++;
        }
    if (numOfPairs <= 0)
        {
        return -1;
        }
    iTotalPairs = numOfPairs;
    // Create type table
    iTypetable = new char[numOfPairs];
    // Create skip table
    iSkiptable = new int[numOfPairs];
    GenerateTables();
    SeekToStart();
    return 0;
}

int CDatabase::GenerateTables()
{
    int i;
    int j;
    time_t nowTime;
    time(&nowTime);
	srand((unsigned int)nowTime);
    for (i=0; i<iTotalPairs; i++)
    {
        iTypetable[i] = rand() % 2;
    }
    int randnum;
    memset(iSkiptable, 0x00, sizeof(int)*iTotalPairs);
    for (i=iTotalPairs; i>0; i--)
    {
        randnum = (rand() % i) + 1;
        for (j=0;; j++)
        {
            if (iSkiptable[j] == 0)
            {
                randnum--;
                if (randnum == 0)
                {
                    iSkiptable[j] = i;
                    break;
                }
            }
        }
    }
    return 0;
}

int CDatabase::GetVariant(char*& aVariant, int& aVariantLen, int& aVariantNum, bool aGetFirst)
{
    int i;
    int retVal;
    int numOfTries = iSkiptable[iCurrentPos];
    SeekToStart();
    for (i=0; i<numOfTries; i++)
    {
        retVal = ReadOneLine();
        if (retVal < 0)
        {
            return retVal;
        }
    }
    if (iTypetable[iCurrentPos]==0 || aGetFirst)
    {
        aVariant = CopyString(iFirstVariant, &aVariantLen);
    }
    else
    {
        aVariant = CopyString(iSecondVariant, &aVariantLen);
    }
    aVariantNum = numOfTries;
    SeekToStart();
    return 0;
}

int CDatabase::GiveAnswer(char* aVariant, char*& aCorrect, int& aCorrectLen, bool aGiveSecond)
{
    int i;
    int retVal;
    int numOfTries = iSkiptable[iCurrentPos];
    SeekToStart();
    for (i=0; i<numOfTries; i++)
    {
        retVal = ReadOneLine();
        if (retVal < 0)
        {
            return retVal;
        }
    }
    if (iTypetable[iCurrentPos]==0 || aGiveSecond)
    {
        retVal = strcmp(iSecondVariant, aVariant);
        if (retVal != 0)
        {
            aCorrect = CopyString(iSecondVariant, &aCorrectLen);
        }
    }
    else
    {
        retVal = strcmp(iFirstVariant, aVariant);
        if (retVal != 0)
        {
            aCorrect = CopyString(iFirstVariant, &aCorrectLen);
        }
    }
    SeekToStart();
    return retVal;
}

int CDatabase::GetTotalPairs()
{
    return iTotalPairs;
}

int CDatabase::IncCurrentPos()
{
    if (iCurrentPos >= iTotalPairs-1)
        {
        return -1;
        }
    iCurrentPos++;
    return 0;
}

int CDatabase::Restart()
{
    iCurrentPos = 0;
    GenerateTables();
    return 0;
}

char* CDatabase::CopyString(char* aSrcString, int* aDstLength)
{
    char* aDstString;
    size_t strLength = strlen(aSrcString) + 1;
    aDstString = new char[strLength];
    memcpy(aDstString, aSrcString, strLength);
    if (aDstLength)
    {
        *aDstLength = (int)strLength;
    }
    return aDstString;
}

void CDatabase::SeekToStart()
{
    fseek(iFirstFileP, 0L, SEEK_SET);
    fseek(iSecondFileP, 0L, SEEK_SET);
}

int CDatabase::ReadOneLine()
{
    int i;
    char* retTemp;
    bool stop = false;
    while (!stop)
    {
        retTemp = fgets(iFirstVariant, iFirstVarSize, iFirstFileP);
        if (retTemp == NULL)
        {
            return -1;
        }
        for (i=0; i<iFirstVarSize; i++)
        {
            if (iFirstVariant[i]==0x0D || iFirstVariant[i]==0x0A)
            {
                iFirstVariant[i] = '\0';
                break;
            }
        }
        if (strlen(iFirstVariant) > 0)
        {
            stop = true;
        }
    }
    stop = false;
    while (!stop)
    {
        retTemp = fgets(iSecondVariant, iSecondVarSize, iSecondFileP);
        if (retTemp == NULL)
        {
            return -1;
        }
        for (i=0; i<iSecondVarSize; i++)
        {
            if (iSecondVariant[i]==0x0D || iSecondVariant[i]==0x0A)
            {
                iSecondVariant[i] = '\0';
                break;
            }
        }
        if (strlen(iSecondVariant) > 0)
        {
            stop = true;
        }
    }
    return 0;
}
