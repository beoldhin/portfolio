﻿#pragma once

class CLangbase
{
public:
    CLangbase();
    ~CLangbase();
    void SetFilename(char* aFilename);
    int Initialize();
    int ReadNext();
    void GetCurrentIsoForm(char*& aIsoForm, int& aIsoFormLength);
    void GetCurrentLanguage(char*& aLanguage, int& aLanguageLength);
    int GetIndexByIsoForm(char* aIsoForm);
    char* GetIsoFormByIndex(int aIndex);
    char* GetLanguageByIndex(int aIndex);
    char* GetLanguageByIsoForm(char* aIsoForm);
    int GetTotalPairs();
private:
    char* CopyString(char* aSrcString, int* aDstLength);
    int ReadTwoLines();
private:
    FILE* iFileP;
    char* iFilename;
    char* iIsoForm;
    char* iLanguage;
    int iIsoFormLength;
    int iLanguageLength;
    int iTotalPairs;
};
