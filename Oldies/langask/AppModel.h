#pragma once

#include "Langbase.h"

class MAppModel
{
public:
    virtual void NotifyNewDatabaseReady() = 0;
    virtual void NotifyRightAnswer() = 0;
    virtual void NotifyWrongAnswer() = 0;
};

class CAppModel
{
public:
    CAppModel( MAppModel* aCallback );
    ~CAppModel( void );
    void Uninitialize();
    int InitializeLangBase();
    CString GetLangFromFilename( LPTSTR aFilename );
    LPWSTR ConvertMultiByteToWideChar( char* aMultiByte );
    char* ConvertWideCharToMultiByte( LPWSTR aWideChar );
// Langbase.h
    char* GetIsoFormByIndex( int aIndex );
    char* GetLanguageByIsoForm( char* aIsoForm );
// Explicit notifications
    void ChangeCategory();
    void ChangeFirstVariant();
    void ChangeSecondVariant();
    void EnterAnswer();
private:
    MAppModel* iCallback;
    CLangbase* iLangbase;
    int iBaseTotal;
};
