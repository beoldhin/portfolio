﻿#include "StdAfx.h"
#include ".\langbase.h"

CLangbase::CLangbase() :
iFileP(NULL),
iFilename(NULL),
iIsoForm(NULL),
iLanguage(NULL),
iIsoFormLength(80),
iLanguageLength(80),
iTotalPairs(0)
{
}

CLangbase::~CLangbase()
{
    if (iFileP)
    {
        fclose(iFileP);
        iFileP = NULL;
    }
    if (iFilename)
    {
        delete [] iFilename;
        iFilename = NULL;
    }
    if (iIsoForm)
    {
        delete [] iIsoForm;
        iIsoForm = NULL;
    }
    if (iLanguage)
    {
        delete [] iLanguage;
        iLanguage = NULL;
    }
}

void CLangbase::SetFilename(char* aFilename)
{
    iFilename = CopyString(aFilename, NULL);
}

int CLangbase::Initialize()
{
    if (!iFilename)
    {
        return -1;
    }
    iFileP = fopen(iFilename, "rb");
    if (!iFileP)
        {
        return -1;
        }
    iIsoForm = new char[iIsoFormLength];
    iLanguage = new char[iLanguageLength];
    // Get number of entries
    int numOfPairs = 0;
    while (ReadTwoLines() >= 0)
        {
        numOfPairs++;
        }
    if (numOfPairs <= 0)
        {
        return -1;
        }
    iTotalPairs = numOfPairs;
    fseek(iFileP, 0L, SEEK_SET);
    return 0;
}

int CLangbase::ReadNext()
{
    int retTemp = ReadTwoLines();
    if (retTemp < 0)
    {
        return -1;
    }
    return 0;
}

void CLangbase::GetCurrentIsoForm(char*& aIsoForm, int& aIsoFormLength)
{
    aIsoForm = CopyString(iIsoForm, &aIsoFormLength);
}

void CLangbase::GetCurrentLanguage(char*& aLanguage, int& aLanguageLength)
{
    aLanguage = CopyString(iLanguage, &aLanguageLength);
}

int CLangbase::GetIndexByIsoForm(char* aIsoForm)
{
    int i = 0;
    fseek(iFileP, 0L, SEEK_SET);
    while (ReadTwoLines() >= 0)
    {
        if (strcmp(iIsoForm,aIsoForm) == 0)
        {
            return i;
        }
        i++;
    }
    return -1;
}

char* CLangbase::GetIsoFormByIndex(int aIndex)
{
    if (aIndex > iTotalPairs-1)
    {
        return 0;
    }
    int i;
    fseek(iFileP, 0L, SEEK_SET);
    for (i=0; i<=aIndex; i++)
    {
        if (ReadTwoLines() < 0)
        {
            return NULL;
        }
    }
    char* retString = CopyString(iIsoForm, NULL);
    return retString;
}

char* CLangbase::GetLanguageByIndex(int aIndex)
{
    if (aIndex > iTotalPairs-1)
    {
        return 0;
    }
    int i;
    fseek(iFileP, 0L, SEEK_SET);
    for (i=0; i<=aIndex; i++)
    {
        if (ReadTwoLines() < 0)
        {
            return NULL;
        }
    }
    char* retString = CopyString(iLanguage, NULL);
    return retString;
}

char* CLangbase::GetLanguageByIsoForm(char* aIsoForm)
{
    fseek(iFileP, 0L, SEEK_SET);
    while (ReadTwoLines() >= 0)
    {
        if (strcmp(iIsoForm,aIsoForm) == 0)
        {
        return CopyString(iLanguage, NULL);
        }
    }
    return NULL;
}

int CLangbase::GetTotalPairs()
{
    return iTotalPairs;
}

char* CLangbase::CopyString(char* aSrcString, int* aDstLength)
{
    char* aDstString;
    size_t strLength = strlen(aSrcString) + 1;
    aDstString = new char[strLength];
    memcpy(aDstString, aSrcString, strLength);
    if (aDstLength)
    {
        *aDstLength = (int)strLength;
    }
    return aDstString;
}

int CLangbase::ReadTwoLines()
{
    int i;
    char* retTemp;
    retTemp = fgets(iIsoForm, iIsoFormLength, iFileP);
    if (retTemp == NULL)
        {
        return -1;
        }
    for (i=0; i<iIsoFormLength; i++)
    {
        if (iIsoForm[i]==0x0D || iIsoForm[i]==0x0A)
        {
            iIsoForm[i] = '\0';
            break;
        }
    }
    retTemp = fgets(iLanguage, iLanguageLength, iFileP);
    if (retTemp == NULL)
    {
        return -1;
    }
    for (i=0; i<iLanguageLength; i++)
    {
        if (iLanguage[i]==0x0D || iLanguage[i]==0x0A)
        {
            iLanguage[i] = '\0';
            break;
        }
    }
    return 0;
}
