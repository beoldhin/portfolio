// LangAskDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LangAsk.h"
#include "LangAskDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CLangAskDlg dialog




CLangAskDlg::CLangAskDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLangAskDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLangAskDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CLangAskDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_CBN_SELENDOK(IDC_COMBOBOX_CATEGORY, &CLangAskDlg::OnCbnSelendokComboboxCategory)
	ON_CBN_SELENDOK(IDC_COMBOBOX_VARIANT1, &CLangAskDlg::OnCbnSelendokComboboxVariant1)
	ON_CBN_SELENDOK(IDC_COMBOBOX_VARIANT2, &CLangAskDlg::OnCbnSelendokComboboxVariant2)
	ON_BN_CLICKED(IDC_CHECK_RANDOMIZE, &CLangAskDlg::OnBnClickedCheckRandomize)
	ON_BN_CLICKED(IDC_BUTTON_ENTERANSWER, &CLangAskDlg::OnBnClickedButtonEnteranswer)
	ON_BN_CLICKED(IDC_BUTTON_EDIT, &CLangAskDlg::OnBnClickedButtonEdit)
	ON_BN_CLICKED(IDC_BUTTON_RESTART, &CLangAskDlg::OnBnClickedButtonRestart)
    ON_BN_CLICKED(IDOK, &CLangAskDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CLangAskDlg message handlers

BOOL CLangAskDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Create controller for this application
	iController = new CAppController;
    iController->Initialize( this );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CLangAskDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CLangAskDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CLangAskDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

int CLangAskDlg::AddCategoryString( LPTSTR aString )
{
    CComboBox *categoryBox = (CComboBox*)GetDlgItem(IDC_COMBOBOX_CATEGORY);
    categoryBox->AddString( aString );
    return 0;
}

int CLangAskDlg::SetCategorySelection( LPTSTR aString )
{
    int retVal = 0;
    CComboBox *categoryBox = (CComboBox*)GetDlgItem( IDC_COMBOBOX_CATEGORY );
    int foundIndex = categoryBox->FindStringExact( 0, aString );
    if ( foundIndex == CB_ERR )
    {
        foundIndex = 0;
        retVal = -1;
    }
    categoryBox->SetCurSel( foundIndex );
    return retVal;
}

int CLangAskDlg::GetCategorySelection( CString& aString )
{
    GetDlgItemText(IDC_COMBOBOX_CATEGORY, aString);
    return 0;
}

int CLangAskDlg::AddVariant1String( CString& aString )
{
    CComboBox *variantBox1 = (CComboBox*)GetDlgItem(IDC_COMBOBOX_VARIANT1);
    variantBox1->AddString( aString );
    return 0;
}

int CLangAskDlg::AddVariant2String( CString& aString )
{
    CComboBox *variantBox2 = (CComboBox*)GetDlgItem(IDC_COMBOBOX_VARIANT2);
    variantBox2->AddString( aString );
    return 0;
}

int CLangAskDlg::GetVariant1Count()
{
    CComboBox *variantBox1 = (CComboBox*)GetDlgItem(IDC_COMBOBOX_VARIANT1);
    return variantBox1->GetCount();
}

int CLangAskDlg::GetVariant1Index( LPWSTR aString )
{
    CComboBox *variantBox1 = (CComboBox*)GetDlgItem(IDC_COMBOBOX_VARIANT1);
    int foundIndex = variantBox1->FindStringExact( 0, aString );
    return foundIndex;
}

int CLangAskDlg::GetVariant2Index( LPWSTR aString )
{
    CComboBox *variantBox2 = (CComboBox*)GetDlgItem(IDC_COMBOBOX_VARIANT2);
    int foundIndex = variantBox2->FindStringExact( 0, aString );
    return foundIndex;
}

void CLangAskDlg::SetCurrentVariant1Index( int aIndex )
{
    CComboBox *variantBox1 = (CComboBox*)GetDlgItem(IDC_COMBOBOX_VARIANT1);
    variantBox1->SetCurSel( aIndex );
}

void CLangAskDlg::SetCurrentVariant2Index( int aIndex )
{
    CComboBox *variantBox2 = (CComboBox*)GetDlgItem(IDC_COMBOBOX_VARIANT2);
    variantBox2->SetCurSel( aIndex );
}

void CLangAskDlg::InitializeRandomizationState()
{
    CheckDlgButton( IDC_CHECK_RANDOMIZE, BST_CHECKED );
    GetDlgItem(IDC_BUTTON_SWITCH)->EnableWindow( false );
}

void CLangAskDlg::SetRandomizationState( bool aState )
{
    GetDlgItem(IDC_BUTTON_SWITCH)->EnableWindow( aState );
}

void CLangAskDlg::OnCbnSelendokComboboxCategory()
{
    iController->NotifyCategoryChanged();
}

void CLangAskDlg::OnCbnSelendokComboboxVariant1()
{
    iController->NotifyFirstVariantChanged();
}

void CLangAskDlg::OnCbnSelendokComboboxVariant2()
{
    iController->NotifySecondVariantChanged();
}

void CLangAskDlg::OnBnClickedCheckRandomize()
{
    iController->NotifyRandomizeChanged();
}

void CLangAskDlg::OnBnClickedButtonEnteranswer()
{
    iController->NotifyEnterAnswerPressed();
}

void CLangAskDlg::OnBnClickedButtonEdit()
{
	// TODO: Add your control notification handler code here
}

void CLangAskDlg::OnBnClickedButtonRestart()
{
	// TODO: Add your control notification handler code here
}

void CLangAskDlg::OnBnClickedOk()
{
    // TODO: Add your control notification handler code here
    delete iController;
    iController = NULL;
    OnOK();
}
