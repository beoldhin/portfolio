#include <stdio.h>
#include <stdlib.h>

int readtwolines(void);
void askquestions(void);

int pairs;
FILE* file;
int* skiptable;
char lang1[80+1];
char lang2[80+1];
char lang1exp[3+1];
char lang2exp[3+1];

int main(int argc, char* argv[])
{
    int i;
    int j;
    int ret;
    int randnum;
    time_t nowtime;
    if (argc != 2)
    {
        fprintf(stderr, "Required parameter missing (filename)\n");
        return -1;
    }
    file = fopen(argv[1], "rb");
    if (!file)
    {
        fprintf(stderr, "File not found!\n");
        return -1;
    }
    ret = readtwolines();
    if (ret != 0)
    {
        printf("Read error, exit\n");
        fclose(file);
        return -1;
    }
    strncpy(lang1exp, lang1, sizeof(lang1exp));
    strncpy(lang2exp, lang2, sizeof(lang2exp));
    printf("%s/%s\n", lang1exp, lang2exp);
	/* Count pairs */
	for (pairs=0;;pairs++)
	{
	    ret = readtwolines();
	    if (ret != 0)
        {
            break;
        }
	}
	printf("Found %d pairs of data\n", pairs);
	printf("Creating skip table... ");
	time(&nowtime);
	srand(nowtime);
	skiptable = (int*)malloc(sizeof(int) * pairs);
	memset(skiptable, 0x00, sizeof(int)*pairs);
	for (i=pairs; i>0; i--)
	{
	    randnum = (rand() % i) + 1;
	    for (j=0;; j++)
	    {
	        if (skiptable[j] == 0)
            {
                randnum--;
                if (randnum == 0)
                {
                    skiptable[j] = i;
                    break;
                }
	        }
	    }
	}
	printf("created.\n");
	askquestions();
	free(skiptable);
    fclose(file);
}

int readtwolines(void)
{
    int i;
    char* ret;
    ret = fgets(lang1, sizeof(lang1), file);
    if (ret == NULL)
        {
        return -1;
        }
    for (i=0; i<sizeof(lang1); i++)
    {
        if (lang1[i] == '\n')
        {
            lang1[i] = '\0';
        }
    }
    ret = fgets(lang2, sizeof(lang2), file);
    if (ret == NULL)
        {
        return -1;
        }
    for (i=0; i<sizeof(lang2); i++)
    {
        if (lang2[i] == '\n')
        {
            lang2[i] = '\0';
        }
    }
    return 0;
}

void askquestions(void)
{
    int i;
    int j;
    int ncorrect;
    char* cmpstr;
    char readline[80+1];
    for (i=0,ncorrect=0; i<pairs; i++)
    {
        fseek(file, 0, SEEK_SET);
        for (j=0; j<skiptable[i]+1; j++)
        {
            readtwolines();
        }
        printf("Question %d/%d (%d)\n", i+1, pairs, skiptable[i]);
        if ((rand()%2) == 0)
        {
            printf("Foreign variant (%s) is   : \"%s\"\n", lang1exp, lang1);
            printf("Enter local variant (%s)  : ", lang2exp);
            fgets(readline, sizeof(readline), stdin);
            cmpstr = lang2;
        }
        else
        {
            printf("Local variant (%s) is     : \"%s\"\n", lang2exp, lang2);
            printf("Enter foreign variant (%s): ", lang1exp);
            fgets(readline, sizeof(readline), stdin);
            cmpstr = lang1;
        }
        for (j=0; j<sizeof(readline); j++)
        {
            if (readline[j] == '\n')
            {
                readline[j] = '\0';
            }
        }
        if (strcmp(cmpstr,readline) == 0)
        {
            printf("=> CORRECT\n");
            ncorrect++;
        }
        else
        {
            printf("=> WRONG (%s)\n", cmpstr);
        }
        puts("");
    }
    printf("Correct/wrong = %d/%d\n", ncorrect, pairs-ncorrect);
}
