#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include <sys/fcntl.h>
#include <asm/unistd.h>
#include <sys/stat.h>
#include <stdio.h>


#include <sys/mman.h>


#define PAGE_SIZE (4096UL)
#define PAGE_MASK (~(PAGE_SIZE-1))


#define serialize() asm volatile("lock ; addl $0,(%esp)")


#define rdtsc() ({ unsigned long a, d; asm volatile("rdtsc":"=a" (a), "=d" (d)); a; })


static int unused = 0;


#define NR (100000)


int main()
{
        int i;
        unsigned long overhead = ~0UL, empty = 0;
        void * address = (void *)(PAGE_MASK & (unsigned long)main);


        mprotect(address, PAGE_SIZE, PROT_READ | PROT_WRITE | PROT_EXEC);


        overhead = ~0UL;
        for (i = 0; i < NR; i++) {
                unsigned long cycles = rdtsc();
                serialize();
                serialize();
                cycles = rdtsc() - cycles;
                if (cycles < overhead)
                        overhead = cycles;
        }
        printf("empty overhead=%ld cycles\n", overhead);
        empty = overhead;


        overhead = ~0UL;
        for (i = 0; i < NR; i++) {
                unsigned long dummy;
                unsigned long cycles = rdtsc();
                serialize();
                asm volatile("movl %1,%0":"=r" (dummy):"m" (unused));
                serialize();
                cycles = rdtsc() - cycles;
                if (cycles < overhead)
                        overhead = cycles;
        }
        printf("load overhead=%ld cycles\n", overhead-empty);


        overhead = ~0UL;
        for (i = 0; i < NR; i++) {
                unsigned long dummy;
                unsigned long cycles = rdtsc();
                serialize();
                asm volatile("1:\tmovl 1b,%0":"=r" (dummy));
                serialize();
                cycles = rdtsc() - cycles;
                if (cycles < overhead)
                        overhead = cycles;
        }
        printf("I$ load overhead=%ld cycles\n", overhead-empty);


        asm volatile("jmp 1f\n.align 128\n99:\t.long 0\n1:");
        overhead = ~0UL;
        for (i = 0; i < NR; i++) {
                unsigned long dummy;
                unsigned long cycles;
                cycles = rdtsc();
                serialize();
                asm volatile("movl 99b,%0":"=r" (dummy));
                serialize();
                cycles = rdtsc() - cycles;
                if (cycles < overhead)
                        overhead = cycles;
        }
        printf("I$ load overhead=%ld cycles\n", overhead-empty);


        asm volatile("jmp 1f\n99:\t.long 0\n1:");
        overhead = ~0UL;
        for (i = 0; i < NR; i++) {
                unsigned long dummy;
                unsigned long cycles;
                cycles = rdtsc();
                serialize();
                asm volatile("1:\tmovl %0,99b":"=r" (dummy));
                serialize();
                cycles = rdtsc() - cycles;
                if (cycles < overhead)
                        overhead = cycles;
        }
        printf("I$ store overhead=%ld cycles\n", overhead-empty);
        return 0;
}
