/* FileXOR version 1.0                                                */
/* This program tries to garble an original file with a garble file   */
/* by using a simple and fast XOR operation. The original file is     */
/* "orgfile", the garble file is "xorfile", and the output file is    */
/* "outfile". The following must be taken care of:                    */
/* 1. If xorfile is smaller than orgfile, garbling is done in blocks. */
/* 2. If orgfile is smaller than xorfile, only partial garbling is    */
/*    performed.                                                      */

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../algo/randgen/mtcokus.c"

int isdigitstr(const char*);
long tellfilesize(const char*);
int applyfxor(void);
void initrandfunc(void);
int readxorfile(void);
void xorbuffer(long);

const char usagemsg[] = "FileXOR version 1.0\n"\
                        "Usage: fxor [seed|i1|i2|i3] orgfile outfile.\n"\
                        "Filenames must contain letters.";
char outfname[] = "outfile.xor";
char* orgfname = NULL;
char* xorfname = NULL;
char* orgfdata = NULL;
char* xorfdata = NULL;
long orgfsize;
long xorfsize;
int seed = -1;
int i1 = -1;
int i2 = -1;
int i3 = -1;

int main(int argc, char* argv[]) {
    /* Mersenne Twister's period is 2^19937-1. We need to calculate   */
    /* 2147483647th root of 2^19937-1) to get the number of nested    */
    /* loops. Using only 3 nested loops with ints, for christ's sake. */
    int i;
    int num;
    char* arg;
    for (i=1; i<argc; i++) {
        arg = argv[i];
        if (!isdigitstr(arg)) {
            if (orgfname == NULL) {
                orgfname = arg;
            } else if (xorfname == NULL) {
                xorfname = arg;
            }
        } else {
            num = atoi(arg);
            if (seed < 0) {
                seed = num;
            } else if (i1 < 0) {
                i1 = num;
            } else if (i2 < 0) {
                i2 = num;
            } else if (i3 < 0) {
                i3 = num;
            }
        }
    }
    if ((orgfname==NULL) || (xorfname==NULL)) {
        fprintf(stderr, "Missing filename(s).\n");
        puts(usagemsg);
        return -1;
    }
    orgfsize = tellfilesize(orgfname);
    xorfsize = tellfilesize(xorfname);
    if ((orgfsize==-1) || (xorfsize==-1)) {
        fprintf(stderr, "Missing file(s).\n");
        puts(usagemsg);
        return -1;
    }
    if (orgfsize == 0) {
        fprintf(stderr, "Nothing to do.\n");
        return -1;
    }
    if (xorfsize == 0) {
        fprintf(stderr, "XOR file size is 0.\n");
        return -1;
    }
    applyfxor();
    return 0;
}

int isdigitstr(const char* str) {
    while (*str) {
        if (!isdigit(*str))
            return 0;
        str++;
    }
    return 1;
}

long tellfilesize(const char* fname) {
    long fpos;
    FILE* filep;
    filep = fopen(fname, "rb");
    if (filep == NULL) {
        return -1;
    }
    fseek(filep, 0, SEEK_END);
    fpos = ftell(filep);
    fclose(filep);
    return fpos;
}

int applyfxor(void) {
    long c;
    long wholes;
    long remains;
    FILE* rfp;
    FILE* wfp;
    if (readxorfile()) {
        fprintf(stderr, "XOR file read failed.\n");
        return -1;
    }
    initrandfunc();
    wholes = orgfsize / xorfsize;
    remains = orgfsize % xorfsize;
    rfp = fopen(orgfname, "rb");
    wfp = fopen(outfname, "wb");
    if (wholes > 0) {
        orgfdata = (char*)malloc(xorfsize*sizeof(char));
        c = 0;
        do {
            fread(orgfdata, sizeof(char), xorfsize, rfp);
            xorbuffer(xorfsize);
            fwrite(orgfdata, sizeof(char), xorfsize, wfp);
            c++;
        } while (c < wholes);
        free(orgfdata);
    }
    if (remains > 0) {
        orgfdata = (char*)malloc(remains*sizeof(char));
        fread(orgfdata, sizeof(char), remains, rfp);
        xorbuffer(remains);
        fwrite(orgfdata, sizeof(char), remains, wfp);
    }
    if (orgfdata != NULL)
        free(orgfdata);
    if (xorfdata != NULL)
        free(xorfdata);
    fclose(rfp);
    fclose(wfp);
    return 0;
}

void initrandfunc(void) {
    int i, j, k;
    seedMT(seed);
    for (k=0; k<i3; k++) {
        for (j=0; j<i2; j++) {
            for (i=0; i<i1; i++) {
                randomMT();
            }
        }
    }
}

int readxorfile(void) {
    FILE* filep;
    size_t readb;
    filep = fopen(xorfname, "rb");
    xorfdata = (char*)malloc(xorfsize*sizeof(char));
    readb = fread(xorfdata, sizeof(char), xorfsize, filep);
    fclose(filep);
    if (xorfsize != readb)
        return -1;
    return 0;
}

void xorbuffer(long bufsize) {
    long c;
    unsigned long randn;
    char xorn1, xorn2, xorn3, xorn4;
    for (c=0; c<bufsize; c++) {
        randn = randomMT();
        xorn1 = (randn>>24) & 0xFF;
        xorn2 = (randn>>16) & 0xFF;
        xorn3 = (randn>>8) & 0xFF;
        xorn4 = randn & 0xFF;
        orgfdata[c] ^= xorfdata[c] ^ xorn1 ^ xorn2 ^ xorn3 ^ xorn4;
    }
}
