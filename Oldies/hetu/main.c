#include <stdio.h>
#include <stdlib.h>

void show_hetu(void);
void create_puncts(void);
void create_leapmonth(void);

int day;
int month;
int year;
char* punct;
char daylist[12] = {31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
char hetulist[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C',
                   'D','E','F','H','J','K','L','M','N','P','R','S','T',
                   'U','V','W','X','Y'};

int main(void) {
    day = 1;
    month = 1;
    year = 1950;
    create_puncts();
    create_leapmonth();
    while (year <= 1950) {
        
        show_hetu();
        
        if (day < daylist[month-1]) {
            day++;
        } else {
            day = 1;
            if (month < 12) {
                month++;
            } else {
                month = 1;
                year++;
                create_leapmonth();
            }
        }
    }
    return 0;
}

void create_puncts(void) {
    /* Punctuations are the following: */
    /* '+': year 1800                  */
    /* '-': year 1900                  */
    /* 'A': year 2000                  */
    /* 'B': year 2100                  */
    /* 'C': year 2200                  */
    /* 'D': year 2300                  */
    /* 'E': year 2400                  */
    /* 'F': year 2500                  */
    /* 'G': year 2600                  */
    /* 'H': year 2700                  */
    /* 'I': year 2800                  */
    /* 'J': year 2900                  */
    /* 'K': year 3000                  */
    /* 'L': year 3100                  */
    /* 'M': year 3200                  */
    /* 'N': year 3300                  */
    /* 'O': year 3400                  */
    /* 'P': year 3500                  */
    /* 'Q': year 3600                  */
    /* 'R': year 3700                  */
    /* 'S': year 3800                  */
    /* 'T': year 3900                  */
    /* 'U': year 4000                  */
    /* 'V': year 4100                  */
    /* 'W': year 4200                  */
    /* 'X': year 4300                  */
    /* 'Y': year 4400                  */
    /* 'Z': year 4500                  */
    /* Creating 45 entries             */
    int i;
    char ch;
    punct = (char*)malloc(45);
    for (i=0; i<17; i++) {
        punct[i] = -1;
    }
    punct[17] = '+';
    punct[18] = '-';
    ch = 'A';
    for (i=19; i<45; i++) {
        punct[i] = ch;
        ch++;
    }
}

void show_hetu(void) {
    int chk;
    int hetu_int;
    char hetu_char[((2+2+2)+1+3+1)+1];
    for (chk=0; chk<1000; chk++) {
        hetu_int = day * 10000000;
        hetu_int += month * 100000;
        hetu_int += (year%100) * 1000;
        hetu_int += chk;
        sprintf(hetu_char, "%02d%02d%02d%c%03d%c", day, month, year%100,
                punct[(year/100)-1], chk, hetulist[hetu_int%31]);
        puts(hetu_char);
    }
}

void create_leapmonth(void) {
    /* For long range accuracy, a formula suggested by the Vatican
       librarian Aloysius Giglio was adopted. It said that every fourth
       year is a leap year except for century years that are not
       divisible by 400. Thus 1700, 1800 and 1900 would not be leap
       years, but 2000 would be a leap year since 2000 is divisible by
       400. */
    if ((year%400==0) || ((year%4==0)&&(year%100!=0))) {
        daylist[1] = 29;
    } else {
        daylist[1] = 28;
    }
}
