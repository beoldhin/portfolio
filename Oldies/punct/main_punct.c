#include <stdio.h>
#include <ctype.h>


int main(void) {
    int ch;
    int putenter = 0;
    while ((ch=getchar()) != EOF) {
        if (isalpha(ch)) {
            if (putenter) {
                putchar('\n');
                putenter = 0;
            }
            putchar(ch);
        } else {
            putenter = 1;
        }
    }
    return 0;
}
