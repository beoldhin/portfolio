#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(int argc, char* argv[]) {
    int ch;
    int def = 'y';
    if ((argc<2) || (argc>3)) {
        fprintf(stderr, "Usage: ask [-n] \"question\"\n");
        return -1;
    }
    if (argc == 3) {
        if (strcmp(argv[1],"-n") != 0) {
            fprintf(stderr, "Unknown switch.\n");
            return -1;
        } else {
            def = 'n';
        }
        printf(argv[2]);
    } else {
        printf(argv[1]);
    }
    ch = tolower(getchar());
    if (ch == def)
        return 0;
    return -1;
}
