
#! /bin/bash

# name of administrator

USER=olli

# locate of current directory

OLDDIR=`pwd`

# filename and location for SDL

ENDING=.tar.bz2
SOURCE=/home/$USER/inst/

ALSADF=alsa-driver-0.5.10b
ALSALF=alsa-lib-0.5.10b
ALSAUF=alsa-utils-0.5.10
IRCIIF=ircii-4.4
SSLF=openssl-0.9.6
MPG123F=mpg123-0.59r
MUTTF=mutt-1.2.5i
PGPF=pgp-6.5.1i-beta2
PNGCF=pngcrush-1.5.3
PORTSF=portsentry-1.0
SDLF=SDL-1.1.8
SSH1F=ssh-1.2.31
SSH2F=ssh-2.4.0
STLF=stl-3.3

ALSAD=$ALSADF$ENDING
ALSAL=$ALSALF$ENDING
ALSAU=$ALSAUF$ENDING
IRCII=$IRCIIF$ENDING
SSL=$SSLF$ENDING
MPG123=$MPG123F$ENDING
MUTT=$MUTTF$ENDING
PGP=$PGPF$ENDING
PNGC=$PNGCF$ENDING
PORTS=$PORTSF$ENDING
SDL=$SDLF$ENDING
SSH1=$SSH1F$ENDING
SSH2=$SSH2F$ENDING
STL=$STLF$ENDING

INSTALSA=0
INSTIRCII=0
INSTMPG123=0
INSTMUTT=0
INSTPGP=0
INSTPNGC=0
INSTPORTS=0
INSTSDL=0
INSTSSH=0
INSTSTL=0

# check if everything is OK

if [ `whoami` != "root" ]; then
    echo "You are not root."
    exit -1
fi

# removing unnecessary packages

apt-get --purge remove bzip2
apt-get --purge remove exim
apt-get --purge remove fbset
apt-get --purge remove xfs
apt-get --purge remove xdm
apt-get --purge remove ssh
apt-get --purge remove xterm
apt-get --purge remove wmaker
rm /etc/rcS.d/S41portmap

# installing global and updating

apt-get install ppp
apt-get install pppconfig
apt-get install ntpdate
apt-get install gettext
apt-get install minicom
apt-get install pump
apt-get install gperf
apt-get install flex
apt-get install byacc
apt-get install wget
apt-get install sudo
apt-get install bin86
apt-get install screen
apt-get install xemacs-21
apt-get install libncurses5-dev
apt-get install libwrap0-dev
cp /floppy/etc/apt/sources.list /etc/apt/
apt-get update
apt-get upgrade
apt-get dist-upgrade

# install source files from the Internet

if $OLDDIR/ask "Do you want to install ALSA? [y/n] "; then
    INSTALSA=1
fi
if $OLDDIR/ask "Do you want to install ircII? [y/n] "; then
    INSTIRCII=1
fi
if $OLDDIR/ask "Do you want to install MPG123? [y/n] "; then
    INSTMPG123=1
fi
if $OLDDIR/ask "Do you want to install MUTT? [y/n] "; then
    INSTMUTT=1
fi
if $OLDDIR/ask "Do you want to install PGP? [y/n] "; then
    INSTPGP=1
fi
if $OLDDIR/ask "Do you want to install Pngcrush? [y/n] "; then
    INSTPNGC=1
fi
if $OLDDIR/ask "Do you want to install PortSentry? [y/n] "; then
    INSTPORTS=1
fi
if $OLDDIR/ask "Do you want to install SDL? [y/n] "; then
    INSTSDL=1
fi
if $OLDDIR/ask "Do you want to install SSH? [y/n] "; then
    INSTSSH=1
fi
if $OLDDIR/ask "Do you want to install STL? [y/n] "; then
    INSTSTL=1
fi
if [ $INSTALSA -eq 1 ]; then
    cd /usr/local/src/
    if ! test -e $ALSADF; then
	tar Ixvf $SOURCE$ALSAD
	cd $ALSADF
	./configure --with-oss=yes --with-isapnp=yes
	make
	make install
	./snddevices
    fi
    cd /usr/local/src/
    if ! test -e $ALSALF; then
	tar Ixvf $SOURCE$ALSAL
	cd $ALSALF
	./configure
	make
	make install
    fi
    cd /usr/local/src/
    if ! test -e $ALSAUF; then
	tar Ixvf $SOURCE$ALSAU
	cd $ALSAUF
	./configure
	make
	make install
    fi
fi
if [ $INSTIRCII -eq 1 ]; then
    cd /usr/local/src/
    if ! test -e $IRCIIF; then
	tar Ixvf $SOURCE$IRCII
	cd $IRCIIF
	./configure
	make
	make install
    fi
fi
if [ $INSTMPG123 -eq 1 ]; then
    cd /usr/local/src/
    if ! test -e $MPG123F; then
	tar Ixvf $SOURCE$MPG123
	cd $MPG123F
	make linux
	make install
    fi
fi
if [ $INSTMUTT -eq 1 ]; then
    apt-get install qmail
    cd /usr/local/src/
    if ! test -e $SSLF; then
	tar Ixvf $SOURCE$SSL
	cd $SSLF
	./config
	make
	make test
	make install
    fi
    cd /usr/local/src/
    if ! test -e $MUTTF; then
	tar Ixvf $SOURCE$MUTT
	cd $MUTTF
	./configure --enable-imap --with-ssl=/usr/local/ssl/
	make
	make install
    fi
fi
if [ $INSTPGP -eq 1 ]; then
    cd /usr/local/src/
    if ! test -e $PGPF; then
	tar Ixvf $SOURCE$PGP
	cd $PGPF
	./build.sh
	cd clients/pgp/cmdline/
	make install
	sudo -u $USER mkdir /home/$USER/.pgp
	sudo -u $USER touch /home/$USER/.pgp/pgp.cfg
	sudo -u $USER touch /home/$USER/.pgp/pubring.pkr
	sudo -u $USER touch /home/$USER/.pgp/secring.skr
    fi
fi
if [ $INSTPNGC -eq 1 ]; then
    cd /usr/local/src/
    if ! test -e $PNGCF; then
	tar Ixvf $SOURCE$PNGC
	cd $PNGCF
	make -f Makefile.gcc
    fi
fi
if [ $INSTPORTS -eq 1 ]; then
    cd /usr/local/src/
    if ! test -e $PORTSF; then
	tar Ixvf $SOURCE$PORTS
	cd $PORTSF
	cp /floppy/usr/local/src/portsentry-1.0/* .
	make linux
	cp /floppy/etc/init.d/portsentry /etc/init.d/
	cd /etc/rcS.d/
	ln -s ../init.d/portsentry S95portsentry
    fi
fi
if [ $INSTSDL -eq 1 ]; then
    cd /usr/local/src/
    if ! test -e $SDLF; then
	tar Ixvf $SOURCE$SDL
	cd $SDLF
	./configure
	make
	make install
    fi
fi
if [ $INSTSSH -eq 1 ]; then
    cd /usr/local/src/
    if ! test -e $SSH1F; then
	tar Ixvf $SOURCE$SSH1
	cd $SSH1F
	./configure --with-libwrap
	make
	make install
	sudo -u $USER ssh-keygen1
	cp /floppy/etc/sshd_config /etc/
    fi
    cd /usr/local/src/
    if ! test -e $SSH2F; then
	tar Ixvf $SOURCE$SSH2
	cd $SSH2F
	./configure --with-libwrap
	make
	make install
	sudo -u $USER ssh-pubkeymgr
	cp /floppy/etc/ssh2/* /etc/ssh2
    fi
    cp /floppy/etc/services /etc/
    cp /floppy/etc/hosts.allow /etc/
    cp /floppy/etc/hosts.deny /etc/
    cp /floppy/etc/inetd.conf /etc/
fi
if [ $INSTSTL -eq 1 ]; then
    cd /usr/include/g++-3/
    tar Ixvf $SOURCE$STL
    mv $STLF/* .
fi

# ipchains firewalling

ipchains -A input -p icmp -s 0/0 8 -j DENY -i eth0
ipchains -A output -p icmp -s 0/0 0 -j DENY -i eth0
ipchains-save > /etc/ipchains.rules
cp /floppy/etc/init.d/packetfilter /etc/init.d/
cd /etc/rcS.d/
ln -s ../init.d/packetfilter S38packetfilter

# new pam configuration

cp /floppy/etc/pam.d/* /etc/pam.d/
cp /floppy/etc/login.defs /etc/
addgroup wheel

# miscellaneous

cp /floppy/$USER/c.elc /home/$USER/
sudo -u $USER cp /floppy/$USER/.* /home/$USER/
cp /floppy/root/.* /root/
cd /dev/
ln -s ttyS0 mouse

# pam and other access for admin

usermod -G audio,cdrom,floppy,wheel $USER

# config for gateway (elrond). illegal icmp requests-echoes are now
# rejected from outside (eth1), while accepting inside

cp /etc/network/interfaces /etc/network/interfaces.old
cp /floppy/etc/network/* /etc/network/
ipchains -D input 1
ipchains -D output 1
ipchains -A input -p icmp -s 0/0 8 -j DENY -i eth1
ipchains -P forward DENY
ipchains -A forward -s gandalf -d 0/0 -j MASQ -i eth1
ipchains -A output -p icmp -s 0/0 0 -j DENY -i eth1
ipchains-save > /etc/ipchains.rules

# install new rtl8139 drivers

cp /floppy/usr/src/linux/drivers/net/* /usr/src/linux/drivers/net/

cd $OLDDIR
exit 0
