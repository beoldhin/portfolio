/* ANSI-C code produced by gperf version 2.7.1 (19981006 egcs) */
/* Command-line: gperf -LANSI-C -Ktag -HLibTags_hash -NLibTags_in_word_set -C -E -j 1 -k 1,$ -t -o LibTags.gperf  */
#include <string.h>
struct tags {
    char* tag;
    int token;
};
/* maximum key range = 44, duplicates = 0 */

#ifdef __GNUC__
__inline
#endif
static unsigned int
LibTags_hash (register const char *str, register unsigned int len)
{
  static const unsigned char asso_values[] =
    {
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 16, 19,  2,
      10, 15,  6, 22, 22, 10, 45, 45,  0, 20,
      25,  1,  0, 45,  5,  0,  0,  1,  0, 14,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45, 45, 45, 45, 45,
      45, 45, 45, 45, 45, 45
    };
  return len + asso_values[(unsigned char)str[len - 1]] + asso_values[(unsigned char)str[0]];
}

#ifdef __GNUC__
__inline
#endif
const struct tags *
LibTags_in_word_set (register const char *str, register unsigned int len)
{
  enum
    {
      TOTAL_KEYWORDS = 36,
      MIN_WORD_LENGTH = 1,
      MAX_WORD_LENGTH = 9,
      MIN_HASH_VALUE = 1,
      MAX_HASH_VALUE = 44
    };

  static const struct tags wordlist[] =
    {
      {""},
      {"p",         0x20},
      {""},
      {"u",         0x3D},
      {"prev",      0x32},
      {"small",     0x38},
      {"select",    0x37},
      {"tr",        0x1E},
      {"onevent",   0x33},
      {"optgroup",  0x34},
      {"timer",     0x3C},
      {"setvar",    0x3E},
      {"td",        0x1D},
      {"do",        0x28},
      {"fieldset",  0x2A},
      {"input",     0x2F},
      {"card",      0x27},
      {"wml",       0x3F},
      {"pre",       0x1B},
      {"postfield", 0x21},
      {"table",     0x1F},
      {"i",         0x2D},
      {"access",    0x23},
      {"template",  0x3B},
      {""},
      {"go",        0x2B},
      {"br",        0x26},
      {"anchor",    0x22},
      {"strong",    0x39},
      {"noop",      0x31},
      {""}, {""},
      {"option",    0x35},
      {"a",         0x1C},
      {"refresh",   0x36},
      {"img",       0x2E},
      {"head",      0x2C},
      {"em",        0x29},
      {""},
      {"b",         0x24},
      {"meta",      0x30},
      {""}, {""}, {""},
      {"big",       0x25}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = LibTags_hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = wordlist[key].tag;

          if (*str == *s && !strcmp (str + 1, s + 1))
            return &wordlist[key];
        }
    }
  return 0;
}
