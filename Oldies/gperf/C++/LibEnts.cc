/* C++ code produced by gperf version 2.7.1 (19981006 egcs) */
/* Command-line: gperf -LC++ -Kent -ZLibEnts -C -E -j 1 -k 1 -t -o LibEnts.gperf  */
#include <string.h>
struct ents {
    char* ent;
    int token;
};
/* maximum key range = 7, duplicates = 0 */

class LibEnts
{
private:
  static inline unsigned int hash (const char *str, unsigned int len);
public:
  static const struct ents *in_word_set (const char *str, unsigned int len);
};

inline unsigned int
LibEnts::hash (register const char *str, register unsigned int len)
{
  static const unsigned char asso_values[] =
    {
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 0, 9, 9,
      9, 9, 9, 4, 9, 9, 9, 9, 0, 9,
      3, 9, 9, 1, 9, 5, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9, 9, 9, 9, 9,
      9, 9, 9, 9, 9, 9
    };
  return len + asso_values[(unsigned char)str[0]];
}

const struct ents *
LibEnts::in_word_set (register const char *str, register unsigned int len)
{
  enum
    {
      TOTAL_KEYWORDS = 7,
      MIN_WORD_LENGTH = 2,
      MAX_WORD_LENGTH = 4,
      MIN_HASH_VALUE = 2,
      MAX_HASH_VALUE = 8
    };

  static const struct ents wordlist[] =
    {
      {""}, {""},
      {"lt",   0x3C},
      {"amp",  0x26},
      {"apos", 0x27},
      {"quot", 0x22},
      {"gt",   0x3E},
      {"nbsp", 0xA0},
      {"shy",  0xAD}
    };

  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register const char *s = wordlist[key].ent;

          if (*str == *s && !strcmp (str + 1, s + 1))
            return &wordlist[key];
        }
    }
  return 0;
}
