#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/event.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BACKLOG 20

int get_event(int kq, struct kevent* ke);
int add_to_event_queue(int kq, int fd);
int remove_from_event_queue(int kq, int fd);
int create_listen_socket(char* addr, int port);

int main(int argc, char* argv[])
{
    int kq;
    int newfd;
    int nbytes;
    int listener;
    char buf[256];
    struct kevent ke;
    socklen_t socklen;
    struct sockaddr_in remoteaddr;
    if (argc != 3) {
        fprintf(stderr, "Invalid number of arguments.\n");
        fprintf(stderr, "Usage: %s <addr> <port>\n", argv[0]);
        return -1;
    }
    listener = create_listen_socket(argv[1], atoi(argv[2]));
    kq = kqueue();
    if (kq == -1) {
        perror("kqueue");
        return -1;
    }
    newfd = add_to_event_queue(kq, listener);
    if (newfd == -1)
        return -1;
    while (1) {
        newfd = get_event(kq, &ke);
        if (newfd == -1)
            return -1;
        if (newfd == 0)
            continue;
        if ((signed)ke.ident == listener) {  // new client waiting
            socklen = sizeof(remoteaddr);
            newfd = accept(listener, (struct sockaddr*)&remoteaddr, &socklen);
            if (newfd == -1) {
                perror("accept");
                return -1;
            }
            // add new socket to event list - begin
            printf("New connection from %s on socket %d\n",
                inet_ntoa(remoteaddr.sin_addr), newfd);
            newfd = add_to_event_queue(kq, newfd);
            if (newfd == -1)
                return -1;
            // add new socket to event list - end
        } else {  // data from client
            nbytes = read(ke.ident, buf, sizeof(buf)-1);
            if (nbytes > 0) {
                buf[nbytes] = '\0';
                if (strcmp(buf,"end\r\n") != 0) {
                    write(ke.ident, buf, nbytes);
                } else {
                    write(ke.ident, "The End\r\n", 9);
                    printf("socket %d explicitly hung up\n", ke.ident);
                    // remove socket from event list - begin
                    newfd = remove_from_event_queue(kq, ke.ident);
                    if (newfd == -1)
                        return -1;
                    close(ke.ident);
                    // remove socket from event list - end
                }
            } else {
                if (nbytes < 0) {
                    perror("read");
                    return -1;
                }
                printf("socket %d implicitly hung up\n", ke.ident);
                // remove socket from event list - begin
                newfd = remove_from_event_queue(kq, ke.ident);
                if (newfd == -1)
                    return -1;
                close(ke.ident);
                // remove socket from event list - end
            }
        }
    }
    close(listener);
    return 0;
}

int get_event(int kq, struct kevent* ke)
{
    int newfd;
    memset(ke, 0x00, sizeof(struct kevent));
    newfd = kevent(kq, NULL, 0, ke, 1, NULL);
    if (newfd == -1) {
        perror("kevent");
        return -1;
    }
    return newfd;
}

int add_to_event_queue(int kq, int fd)
{
    int newfd;
    struct kevent ke;
    EV_SET(&ke, fd, EVFILT_READ, EV_ADD, 0, BACKLOG, NULL);
    newfd = kevent(kq, &ke, 1, NULL, 0, NULL);
    if (newfd == -1) {
        perror("kevent");
        return -1;
    }
    return newfd;
}

int remove_from_event_queue(int kq, int fd)
{
    int newfd;
    struct kevent ke;
    EV_SET(&ke, fd, EVFILT_READ, EV_DELETE, 0, 0, NULL);
    newfd = kevent(kq, &ke, 1, 0, 0, NULL);
    if (newfd == -1) {
        perror("kevent");
        return -1;
    }
    return newfd;
}

int create_listen_socket(char* addr, int port)
{
    int ret;
    int yes;
    int listener;
    struct hostent* he;
    struct sockaddr_in myaddr;
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener == -1) {
        perror("socket");
        return -1;
    }
    yes = 1;
    ret = setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
    if (ret == -1) {
        perror("setsockopt");
        return -1;
    }
    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(port);
    he = gethostbyname(addr);
    if (!he) {
        perror("gethostbyname");
        return -1;
    }
    myaddr.sin_addr = *((struct in_addr*)he->h_addr);
    memset(&(myaddr.sin_zero), '\0', sizeof(myaddr.sin_zero));
    ret = bind(listener, (struct sockaddr*)&myaddr,sizeof(myaddr));
    if (ret == -1) {
        perror("bind");
        return -1;
    }
    ret = listen(listener, BACKLOG);
    if (ret == -1) {
        perror("listen");
        return -1;
    }
    return listener;
}
