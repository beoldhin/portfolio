/* Exit codes:                          */
/* 0: Normal input - 1..9 entered       */
/* 1: Right password when END initiated */
/* 2: Wrong password when END initiated */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char rightpass[] = "49513";


int main(int argc, char* argv[]) {
    FILE* fp;
    char passdata[] = "XXXXX";
    char outfile[] = "/home/olli/.lircpass";
    if (argc != 2) {
        fprintf(stderr, "Invalid number of parameters.\n");
        return -1;
    }
    fp = fopen(outfile, "rb");
    if (fp) {
        fread(passdata, sizeof(char), 5, fp);
        fclose(fp);
        if (tolower(argv[1][0]) == 'e') {
            if (!strcmp(passdata,rightpass)) {
                return 1;
            } else {
                return 2;
            }
        }
    }
    if (tolower(argv[1][0]) != 'e') {
        passdata[0] = passdata[1];
        passdata[1] = passdata[2];
        passdata[2] = passdata[3];
        passdata[3] = passdata[4];
        passdata[4] = argv[1][0];
        fp = fopen(outfile, "wb");
        fwrite(passdata, sizeof(char), 5, fp);
        fclose(fp);
    }
    return 0;
}
