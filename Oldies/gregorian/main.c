#include <stdio.h>
#include <stdlib.h>

int calculate_dow(int, int, int);
int calculate_wkn(int, int, int);
int calculate_idx(int);

const char days[7][10] = {"Sunday",
                         "Monday",
                         "Tuesday",
                         "Wednesday",
                         "Thursday",
                         "Friday",
                         "Saturday"};

/* The following variables are global for speed purposes - begin */

/* The line below is used when converting from Sunday..Saturday    */
/* format to Monday..Sunday format. It eliminates the use of the   */
/* "dow--; if (dow<0) dow=6;" sequence. Table conversion is faster */
/* especially on high end (32 bit) machines, but about the same on */
/* older machines.                                                 */
const int dowtbl[7] = {6, 0, 1, 2, 3, 4, 5};

/* A cumulative table of months was created to eliminate the need   */
/* to count days. The month table below was generated from this:    */
/* int mtbl[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; */
/* The first two numbers are 0 because this eliminates the need for */
/* decrementing and comparing a variable when finding the days to   */
/* the previous month. The first 0 exist because days start from 1, */
/* not from 0. The seconds 0 exist because cumulative value is      */
/* required to the *previous* month. This increases the speed of    */
/* the algorithm by about 60 percent (worst case).                  */
const int mtbl[14] = {0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};

/* A year may have 53 weeks. The week 53 "belogs to" the next year    */
/* and is actually the first week. The definition below is used when  */
/* converting the week 53 to week 1. It eliminates the use of the     */
/* "if (wkn==53) wkn=1;" sequence. The first number is 0 because week */
/* numbers start from 1, not from 0 (no need to decrease 1 to make an */
/* index).                                                            */
const int wkntbl[54] = { 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11,
                        12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
                        24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
                        36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
                        48, 49, 50, 51, 52, 1};

/* The following variables are global for speed purposes - end */


int main(int argc, char* argv[]) {
    int dow;
    int idx;
    int day;
    int wkn;
    int year;
    int month;
    if (argc != 4) {
        printf("Invalid number of parameters.\n");
        return -1;
    }
    day = atoi(argv[1]);
    month = atoi(argv[2]);
    year = atoi(argv[3]);
    dow = calculate_dow(day, month, year);
    printf("DOW: %s\n", days[dow]);
    wkn = calculate_wkn(day, month, year);
    printf("WKN: %d\n", wkn);
    idx = calculate_idx(dow);
    printf("Idx: %d\n", idx);
    return 0;
}

/* Calculate Day of Week from date */
int calculate_dow(int day, int month, int year) {
    int d;    /* This is max. 366     */
    int s;    /* This is max. INT_MAX */
    int dow;  /* This is max. 6       */
    int ym1;  /* This is max. INT_MAX */
    
    /* Calculate number of days in previous (full) years */
    
    ym1 = year - 1;                            /* 1 */
    s = ym1 + (ym1/4) - (ym1/100) + (ym1/400); /* 1 */
    
    /* Calculate number of days in this year */
    
    d = mtbl[month] + day;
    if (month > 2) {
        if ((year%4) != 0) {          /* 2 */
        } else if ((year%400) == 0) { /* 2 */
            d++;                      /* 2 */
        } else if ((year%100) == 0) { /* 2 */
        } else {                      /* 2 */
            d++;                      /* 2 */
        }                             /* 2 */
    }
    
    dow = (s+d) % 7;
    
    /* Return value of 0 is Sunday, 6 is Saturday */
    
    return dow;
}

/* Calculate week number from date */
int calculate_wkn(int day, int month, int year) {
    int d;    /* This is max. 366     */
    int f;    /* This is max. 366     */
    int r;    /* This is max. 7       */
    int s;    /* This is max. INT_MAX */
    int dow;  /* This is max. 6       */
    int wkn;  /* This is max. 52      */
    int ym1;  /* This is max. INT_MAX */
    
    /* Calculate Day of Month from the beginning of the year.      */
    /* This could be achieved by "dow = calculate_dow(1, 1, year)" */
    /* but for speed purposes we are using a modified Day of Week  */
    /* code that does not have the code for calculating non-full   */
    /* years (just adding 1 to the day counter).                   */
    
    ym1 = year - 1;                            /* 1 */
    s = ym1 + (ym1/4) - (ym1/100) + (ym1/400); /* 1 */
    dow = (s+1) % 7;
    
    /* Convert the week number to Monday..Sunday format */
    /* Sunday..Saturday format is used in United States */
    
    dow = dowtbl[dow];
    
    /* Count the days from the start of the year while increasing  */
    /* week counter after each full week. For better performance,  */
    /* replace this by directly calculating (not looping) the week */
    /* number.                                                     */
    
    /* cwk = dow;                               */
    /* wkn = 1;                                 */
    /* cday = 1;                                */
    /* cmonth = 1;                              */
    /* mtbl[1] = get_days_of_february(year);    */
    /* while ((cday!=day) || (cmonth!=month)) { */
    /*    if (cday < mtbl[cmonth-1]) {          */
    /*        cday++;                           */
    /*    } else {                              */
    /*        cday = 1;                         */
    /*          cmonth++;                       */
    /*    }                                     */
    /*    cwk++;                                */
    /*    if (cwk == 7) {                       */
    /*        cwk = 0;                          */
    /*        wkn++;                            */
    /*    }                                     */
    /* }                                        */
    
    /* Direct calculation was achieved and the above code commented */
    /* The code below divides full months out from the day counter, */
    /* adding the remaining (possibly non-full) days possibly       */
    /* starting from the previous year. Variable "r" represents the */
    /* remaining years and variable "d" is the number of days from  */
    /* the start of the year.                                       */
    
    /* On the worst case (31. December) the code below is about 21  */
    /* times faster than the above commented day counting code.     */
    
    d = mtbl[month] + day;
    if (month > 2) {
        if ((year%4) != 0) {          /* 2 */
        } else if ((year%400) == 0) { /* 2 */
            d++;                      /* 2 */
        } else if ((year%100) == 0) { /* 2 */
        } else {                      /* 2 */
            d++;                      /* 2 */
        }                             /* 2 */
    }
    
    r = 7 - dow;
    f = d;
    d -= r;
    wkn = d / 7;
    if (f >= r) {
        wkn++;
    }
    if ((d%7) != 0) {
        wkn++;
    }
    
    /* Return value is week number */
    
    wkn = wkntbl[wkn];
    
    return wkn;
}

int calculate_idx(int dow) {
    int idx;
    dow = dowtbl[dow];
    idx = -dow + 1;
    return idx;
}

/* 1: Owner Asian Online (http://www.asiaonline.net.hk/~tfwong/dowexam.htm)        */
/* 2: Owner MITRE (http://www.mitre.org/research/cots/LEAPCALC.html)               */
