BEGIN {
}

{
    if ($3=="implicit" && $4=="declaration") {
        printf("#define %s\n", substr(toupper($7),2,length($7)-2));
    } else if ($2=="undefined" && $3=="reference") {
        printf("#define %s\n", substr(toupper($5),2,length($5)-2));
    }
}

END {
}
