/* 1. Destination file: copy from SOF to end of C comment (not including it) */
/* 2. Source file: copy from ["N:"-8] to end of C comment (not including it) */
/* 3. Destination file: copy from end of C comment (including it) to EOF     */
/* Needed search routines: end of C comment and "N:"                         */
/* Todo:                                                                     */
/* - Error if illegal characters before start of C comment                   */
/* - Error if "N:" not found inside a C comment block ("N:" finder should    */
/*   detech also C comment ends)                                             */
/* - Error if "-8" or "-2" points past the buffer                            */
/* - Error if history area points past the buffer                            */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FAILTYPE_WARNING 0
#define FAILTYPE_ERROR 1

typedef struct pointers {
    unsigned char* startp;
    unsigned char* endp;
} POINTERS;

size_t add_change_history(POINTERS*);
void convert_buffer_to_unix(POINTERS*);
void read_file(const char*, POINTERS*, int);
void find_twochars(POINTERS*, unsigned char, unsigned char);
size_t find_comment_length(const char*, POINTERS*, int);
size_t find_history_length(const char*, POINTERS*, int);


int main(int argc, char* argv[]) {
    int i;
    FILE* filep;
    size_t writen;
    size_t L1, L2, L3;
    POINTERS src, dst;
    POINTERS srchist;
    unsigned char* res;
    if (argc < 3) {
        fprintf(stderr, "Error: Too few parameters.\n");
        fprintf(stderr, "Usage: histcopy src dst1 [dst2] [dst3] [...]\n");
        return -1;
    }
    read_file(argv[1], &src, FAILTYPE_ERROR);
    if (src.startp == NULL)
        return -1;
    convert_buffer_to_unix(&src);
    srchist.startp = src.startp;
    srchist.endp = src.endp;
    L2 = find_history_length(argv[1], &srchist, FAILTYPE_ERROR);
    if (L2 == 0) {
        free(src.startp);
        return -1;
    }
    L2 += add_change_history(&srchist);
    for (i=2; i<argc; i++) {
        read_file(argv[i], &dst, FAILTYPE_WARNING);
        if (dst.startp == NULL)
            continue;
        L1 = find_comment_length(argv[1], &dst, FAILTYPE_WARNING);
        if (L1 == 0) {
            free(dst.startp);
            continue;
        }
        L3 = (dst.endp-dst.startp) - L1;
        
        /* write the final file - begin */
        res = (unsigned char*)malloc((L1+L2+L3)*sizeof(unsigned char));
        memcpy(res, dst.startp, L1);
        memcpy(res+L1, srchist.startp, L2);
        memcpy(res+L1+L2, dst.startp+L1, L3);
        filep = fopen(argv[i], "wb");
        if (filep == NULL) {
            fprintf(stderr, "Warning: File open failed for %s.\n", argv[i]);
            free(dst.startp);
            free(res);
            continue;
        }
        writen = fwrite(res, sizeof(unsigned char), L1+L2+L3, filep);
        if (L1+L2+L3 != writen) {
            fprintf(stderr, "Warning: File write failed for %s.\n", argv[i]);
            free(dst.startp);
            free(res);
            continue;
        }
        fclose(filep);
        free(res);
        /* write the final file - end */
        
        free(dst.startp);
    }
    free(src.startp);
    return 0;
}

void read_file(const char* fname, POINTERS* p, int failtype) {
    FILE* filep;
    size_t readn;
    unsigned long fpos;
    unsigned char* memp;
    filep = fopen(fname, "rb");
    if (filep == NULL) {
        if (failtype == FAILTYPE_ERROR)
            fprintf(stderr, "Error:");
        else
            fprintf(stderr, "Warning:");
        fprintf(stderr, " File open failed for %s\n", fname);
        p->startp = NULL;
        return;
    }
    fseek(filep, 0, SEEK_END);
    fpos = ftell(filep);
    fseek(filep, 0, SEEK_SET);
    memp = (unsigned char*)malloc(fpos*sizeof(unsigned char));
    readn = fread(memp, sizeof(unsigned char), (size_t)fpos, filep);
    if (fpos != readn) {
        if (failtype == FAILTYPE_ERROR)
            fprintf(stderr, "Error:");
        else
            fprintf(stderr, "Warning:");
        fprintf(stderr, " File read failed for %s\n", fname);
        p->startp = NULL;
        return;
    }
    p->startp = memp;
    p->endp = memp + fpos;
    fclose(filep);
}

size_t add_change_history(POINTERS* p) {
    size_t strsize;
    char historytext[] = "   Change history:   ";
    historytext[0] = 0x0A;
    historytext[1] = 0x0A;
    historytext[2] = 0x0A;
    historytext[18] = 0x0A;
    historytext[19] = 0x0A;
    historytext[20] = 0x0A;
    strsize = strlen(historytext);
    memcpy(p->startp-strsize, historytext, strsize);
    p->startp -= strsize;
    return strsize;
}

void find_twochars(POINTERS* p, unsigned char previous, unsigned char current) {
    unsigned char prevc = '\0';
    while (p->startp < p->endp) {
        if (*(p->startp) == current) {
            if (prevc == previous) {
                (p->startp)++;
                return;
            }
        }
        prevc = *(p->startp);
        (p->startp)++;
    }
    p->startp = NULL;
}

void convert_buffer_to_unix(POINTERS* p) {
    unsigned char* oldbuf;
    unsigned char* newbuf;
    unsigned char* oldcount;
    unsigned char* newcount;
    oldbuf = p->startp;
    newbuf = (unsigned char*)malloc(p->endp - p->startp);
    oldcount = oldbuf;
    newcount = newbuf;
    while (oldcount < p->endp) {
        if (*oldcount != 0x0D) {
            *newcount = *oldcount;
            newcount++;
        }
        oldcount++;
    }
    p->startp = newbuf;
    p->endp = newcount;
    free(oldbuf);
}

size_t find_comment_length(const char* fname, POINTERS* p, int failtype) {
    POINTERS newp;
    newp.startp = p->startp;
    newp.endp = p->endp;
    find_twochars(&newp, '/', '*');
    if (newp.startp == NULL) {
        if (failtype == FAILTYPE_ERROR)
            fprintf(stderr, "Error:");
        else
            fprintf(stderr, "Warning:");
        fprintf(stderr, " Can't find comment start for %s.\n", fname);
        return 0;
    }
    find_twochars(&newp, '*', '/');
    if (newp.startp == NULL) {
        if (failtype == FAILTYPE_ERROR)
            fprintf(stderr, "Error:");
        else
            fprintf(stderr, "Warning:");
        fprintf(stderr, " Can't find comment end for %s.\n", fname);
        return 0;
    }
    return newp.startp - p->startp - 2;
}

size_t find_history_length(const char* fname, POINTERS* p, int failtype) {
    size_t length;
    POINTERS hisp;
    find_twochars(p, '/', '*');
    if (p->startp == NULL) {
        if (failtype == FAILTYPE_ERROR)
            fprintf(stderr, "Error:");
        else
            fprintf(stderr, "Warning:");
        fprintf(stderr, " Can't find comment start for %s.\n", fname);
        return 0;
    }
    find_twochars(p, 'N', ':');
    if (p->startp == NULL) {
        if (failtype == FAILTYPE_ERROR)
            fprintf(stderr, "Error:");
        else
            fprintf(stderr, "Warning:");
        fprintf(stderr, " Can't find version start for %s.\n", fname);
        return 0;
    }
    hisp.startp = p->startp - 8;
    find_twochars(p, '*', '/');
    if (p->startp == NULL) {
        if (failtype == FAILTYPE_ERROR)
            fprintf(stderr, "Error:");
        else
            fprintf(stderr, "Warning:");
        fprintf(stderr, " Can't find comment end for %s.\n", fname);
        return 0;
    }
    hisp.endp = p->startp - 2;
    length = hisp.endp - hisp.startp;
    p->startp = hisp.startp;
    p->endp = hisp.endp;
    return hisp.endp - hisp.startp;
}
