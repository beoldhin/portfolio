#include <windows.h>
#include <stdio.h>
#include <iostream.h>
#include <conio.h>
#include <fstream.h>

#define GlobalLockFM(hmSource, pTarget, pType)\
{\
	pTarget = (pType *)GlobalLock(hmSource);\
}
#define GlobalInsertFM(hmSource, pTarget, uFlags, iSize, pType)\
{\
	hmSource = GlobalAlloc(uFlags, iSize);\
	GlobalLockFM(hmSource, pTarget, pType);\
}
#define GlobalReAllocFM(hmSource, pTarget, uFlags, iSize, pType)\
{\
	GlobalUnlock(hmSource);\
	hmSource = GlobalReAlloc(hmSource, iSize, uFlags);\
	GlobalLockFM(hmSource, pTarget, pType);\
}
#define GlobalRemoveFM(hMemory)\
{\
	GlobalUnlock(hMemory);\
	GlobalFree(hMemory);\
}
#define RemoveMemFileFM(hMemory, hFile)\
{\
	GlobalRemoveFM(hmConfig);\
	CloseHandle(hFile);\
}


void Add_Fibonacci(int *, int *, int *, int, char *, char *, char *);

int iI, iJ, iK, iX;
int iCharTotal, iLimit, iFib1Size, iFib2Size, iTempSize;
HGLOBAL hmFib1, hmFib2, hmTemp;
char *pszFib1, *pszFib2, *pszTemp;


int main(void)
{
	cout << "Inc limit: ";
	cin >> iLimit;
	
	if (iLimit > 7)
		iCharTotal = iLimit / 4;
	else
		if (iLimit < 7)
			iCharTotal = 1;
		else
			iCharTotal = 2;
	
	cout << "Alloc " << (iCharTotal*3)/1024 << "kb:\n";
	GlobalInsertFM(hmFib1, pszFib1, GHND, iCharTotal, char);
	if (hmFib1 == NULL)
	{
		cout << "Memory allocation error.\n";
		return 1;
	}
	GlobalInsertFM(hmFib2, pszFib2, GHND, iCharTotal, char);
	if (hmFib2 == NULL)
	{
		cout << "Memory allocation error.\n";
		GlobalRemoveFM(hmFib1);
		return 2;
	}
	GlobalInsertFM(hmTemp, pszTemp, GHND, iCharTotal, char);
	if (hmTemp == NULL)
	{
		cout << "Memory allocation error.\n";
		GlobalRemoveFM(hmFib1);
		GlobalRemoveFM(hmFib2);
		return 3;
	}
	
	iFib1Size = 1;
	iFib2Size = 1;
	pszFib1[iCharTotal-1] = 0;
	pszFib2[iCharTotal-1] = 1;
	
	iK = 1;
	while (iK < iLimit)
	{
		Add_Fibonacci(&iFib1Size, &iFib2Size, &iTempSize, iCharTotal, pszFib1, pszFib2, pszTemp);
		
		printf("%d / %d\r", iFib2Size, iK);

		for (iI=iCharTotal-1,iJ=0; iJ<iFib2Size; iI--,iJ++)
			pszFib1[iI] = pszFib2[iI];
		iFib1Size = iFib2Size;
		
		for (iI=iCharTotal-1,iJ=0; iJ<iTempSize; iI--,iJ++)
			pszFib2[iI] = pszTemp[iI];
		iFib2Size = iTempSize;
		
		iK++;
	}
	
	ofstream ofile("fib.txt");
	ofile << iFib2Size << " / " << iK << ":\n";
	for (iI=iCharTotal-iFib2Size,iJ=0; iI<iCharTotal; iI++,iJ++)
	{
		if (iJ == 78)
		{
			ofile << "\n";
			iJ = 0;
		}
		iX = pszFib2[iI];
		ofile << iX;
	}
	ofile.close();	
	
	GlobalRemoveFM(hmFib1);
	GlobalRemoveFM(hmFib2);
	GlobalRemoveFM(hmTemp);
	return 0;
}

void Add_Fibonacci(int *iFib1Size, int *iFib2Size, int *iTempSize, int iTLength, char *pszFib1, char *pszFib2, char *pszTemp)
{
	// iTempSize:n kooksi laitetaan suuremman arvo
	int iI, iJ, iK, iX, iY, iMem;
	*iTempSize = *iFib2Size;
	for (iI=iTLength-1,iJ=*iFib1Size,iMem=0; iJ>0; iI--,iJ--)
	{
		iX = pszFib1[iI] + pszFib2[iI] + iMem;
		if (iX > 9)
		{
			pszTemp[iI] = iX - 10;
			iMem = 1;
		}
		else
		{
			pszTemp[iI] = iX;
			iMem = 0;
		}
	}
	for (iJ=*iFib2Size-*iFib1Size; iJ>0; iI--,iJ--)
	{
		iX = pszFib2[iI] + iMem;
		if (iX > 9)
		{
			pszTemp[iI] = iX - 10;
			iMem = 1;
		}
		else
		{
			pszTemp[iI] = iX;
			iMem = 0;
		}
	}
	if (iMem == 1)
	{
		(*iTempSize)++;
		pszTemp[iI] = 1;
	}
}