
#include <stdio.h>
#include <unistd.h>
#include "CFrontEnd.h"
#include "iomuxer.h"

#define CR 0x0D
#define LF 0x0A
#define RBUFLEN (510+2+1)  // 510 data + 2 newline + 1 zero

int CFrontEnd::StartService(char *aAddress, int aPort)
{
	iomuxer_setcallback(this);
	iomuxer_startservice(aAddress, aPort, RBUFLEN);
	for (;;) sleep(1000);  // block forever;
}

void CFrontEnd::MSyncIOMUXNewConnection(int aFd)
{
	printf("New connection on socket %d\n", aFd);
}

int CFrontEnd::MSyncIOMUXPartialData(unsigned char *aMessage, ssize_t aBuflen)
{
	if (aBuflen >= 2) {
		if ((aMessage[aBuflen-2]==CR) && (aMessage[aBuflen-1]==LF)) {
			return 1;
		}
	}
	return 0;
}

void CFrontEnd::MSyncIOMUXNewData(int aFd, unsigned char *aMessage, ssize_t aBuflen)
{
	aMessage[aBuflen] = '\0';
	if (strcmp((char *)aMessage,"end\r\n") == 0) {
		printf("Explicit hup on socket %d\n", aFd);
		iomuxer_explicithup(aFd);
	} else {
		iomuxer_sendmessage(aFd, aMessage, aBuflen);
		printf("Sent back %d bytes\n", aBuflen);
	}
}

void CFrontEnd::MSyncIOMUXImplicitHup(int aFd)
{
	printf("Implicit hup on socket %d\n", aFd);
}

void CFrontEnd::MSyncIOMUXFdChanged(int oldfd, int newfd)
{
}

