
#include <stdio.h>
#include <stdlib.h>
#include "CFrontEnd.h"

int main(int argc, char *argv[])
{
	CFrontEnd frontend;
	if (argc != 3) {
		fprintf(stderr, "Invalid number of arguments.\n");
		fprintf(stderr, "Usage: %s <addr> <port>\n", argv[0]);
		return -1;
	}
	frontend.StartService(argv[1], atoi(argv[2]));
	return 0;
}
