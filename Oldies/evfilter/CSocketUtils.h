
#ifndef _CSOCKETUTILS_
#define _CSOCKETUTILS_

#include <sys/types.h>
#include <sys/socket.h>

class CSocketUtils
{
public:
	static int CreateListenSocket(char *aAddr, int aPort);
	static int CreateConnectSocket(char *aAddr, int aPort);
private:
	static int CreateSocket(char *aAddr, int aPort, struct sockaddr_in *aAddrDef);
};

#endif  // _CSOCKETUTILS_
