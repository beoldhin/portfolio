
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "CSocketUtils.h"

#define QUEUESIZE 20

int CSocketUtils::CreateListenSocket(char *aAddr, int aPort)
{
	int ret;
	int listener;
	struct sockaddr_in addrdef;
	listener = CreateSocket(aAddr, aPort, &addrdef);
	if (listener == -1)
		return -1;
	ret = listen(listener, QUEUESIZE);
	if (ret == -1) {
		perror("listen");
		return -1;
	}
	return listener;
}

int CSocketUtils::CreateConnectSocket(char *aAddr, int aPort)
{
	int ret;
	int connector;
	struct sockaddr_in addrdef;
	connector = CreateSocket(aAddr, aPort, &addrdef);
	if (connector == -1)
		return -1;
	ret = connect(connector, (struct sockaddr *)&addrdef, sizeof(addrdef));
	if (ret == -1) {
		perror("connect");
		return -1;
	}
	return connector;
}

int CSocketUtils::CreateSocket(char *aAddr, int aPort, struct sockaddr_in *aAddrDef)
{
	int ret;
	int reuse = 1;
	int newsocket;
	struct hostent *he;
	he = gethostbyname(aAddr);
	if (!he) {
		perror("gethostbyname");
		return -1;
	}
	newsocket = socket(AF_INET, SOCK_STREAM, 0);
	if (newsocket == -1) {
		perror("socket");
		return -1;
	}
	ret = setsockopt(newsocket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int));
	if (ret == -1) {
		perror("setsockopt");
		return -1;
	}
	aAddrDef->sin_family = AF_INET;
	aAddrDef->sin_port = htons(aPort);
	aAddrDef->sin_addr = *((struct in_addr *)he->h_addr);
	memset(&(aAddrDef->sin_zero), 0x00, sizeof(aAddrDef->sin_zero));
	ret = bind(newsocket, (struct sockaddr *)aAddrDef, sizeof(*aAddrDef));
	if (ret == -1) {
		perror("bind");
		return -1;
	}
	return newsocket;
}
