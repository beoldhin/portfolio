
#ifndef _MSYNCIOMUX_
#define _MSYNCIOMUX_

class MSyncIOMUX
{
public:
	virtual void MSyncIOMUXNewConnection(int aFd) { };
	virtual int MSyncIOMUXPartialData(unsigned char *aMessage, ssize_t aBuflen) { return 0; };
	virtual void MSyncIOMUXNewData(int aFd, unsigned char *aMessage, ssize_t aBuflen) { };
	virtual void MSyncIOMUXImplicitHup(int aFd) { };
	virtual void MSyncIOMUXFdChanged(int oldfd, int newfd) { };
};

#endif  // _MSYNCIOMUX_
