
/*
	TODO:
	+ Add code to connect to new servers
	- Add handling for EINTR case (ignore signal)
*/

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <event.h>
#include <list.h>
#include <sys/param.h>
#include "iomuxer.h"
#include "CSocketUtils.h"

#define HOSTNAMELEN (MAXHOSTNAMELEN-1)

#define ADD_READER_EVENT(event) {\
	pthread_mutex_lock(&iRQMutex);\
	ret = event_add(event, NULL);\
	pthread_mutex_unlock(&iRQMutex);\
	if (ret == -1) {\
		perror("event_add");\
		exit(-1);\
	}\
}

struct THandlerData
{
	char address[HOSTNAMELEN+1];
	int port;
};

struct TInQueueEntity
{
	unsigned char *message;
	short bytesgot;
	int fd;
};

struct TConnectionEntity
{
	struct event rev;
	int fd;
};

static void *iomuxer_handleevents(void *arg);
static void iomuxer_handlenewconnection(const int listenfd, short event, void *arg);
static void iomuxer_handlereads(const int newfd, short event, void *arg);
static void iomuxer_addreader(int fd, void (*fn)(int, short, void*));
static int iomuxer_explicithup_knowniq(int fd, list<TInQueueEntity>::iterator pos);
static void iomuxer_removefrominqueue(int fd);
static void iomuxer_removefrominqueue_knowniq(list<TInQueueEntity>::iterator iqpos);

static list <TConnectionEntity> iConnQueue;
static list <TInQueueEntity> iInQueue;
static pthread_mutex_t iIQMutex;  // mutex for iInQueue
static pthread_mutex_t iRQMutex;  // mutex for rev in iConnQueue
static pthread_mutex_t iCQMutex;  // mutex for iConnQueue itself
static THandlerData iHandlerData;
static pthread_t iRWThread;
static MSyncIOMUX *iParent;
static ssize_t iRBuflen;

// public functions -- begin

void iomuxer_setcallback(MSyncIOMUX *parent)
{
	iParent = parent;
}

int iomuxer_startservice(char *address, int port, ssize_t rbuflen)
{
	int ret;
	iRBuflen = rbuflen;
	// create mutexes
	ret = pthread_mutex_init(&iIQMutex, NULL);
	if (ret != 0) {
		perror("pthread_mutex_init");
		return -1;
	}
	ret = pthread_mutex_init(&iRQMutex, NULL);
	if (ret != 0) {
		perror("pthread_mutex_init");
		return -1;
	}
	ret = pthread_mutex_init(&iCQMutex, NULL);
	if (ret != 0) {
		perror("pthread_mutex_init");
		return -1;
	}
	// start threads
	puts("Creating libevent watcher thread...");
	strncpy(iHandlerData.address, address, HOSTNAMELEN);
	iHandlerData.port = port;
	ret = pthread_create(&iRWThread, NULL, iomuxer_handleevents, (void *)&iHandlerData);
	if (ret != 0) {
		perror("pthread_create");
		return -1;
	}
	sleep(1);
	puts("Creation OK");
	return 0;
}

int iomuxer_sendmessage(int fd, unsigned char *message, ssize_t buflen)
{
	ssize_t nbytes;
	while (1) {
		nbytes = write(fd, message, buflen);
		if (nbytes == -1) {
			switch (errno) {
				case EBADF:
					return -1;
				case EPIPE:
					return -1;
				case EFBIG:
					return -1;
				case EINVAL:
					return -1;
				case ENOSPC:
					return -1;
				case EDQUOT:
					return -1;
				case EIO:
					return -1;
				case EAGAIN:
					continue;
				case EFAULT:
					return -1;
				default:  // should never happen
					perror("write");
					return -1;
			}
		} else {
			break;
		}
	}
	return 0;
}

int iomuxer_addconnector(char *address, int port)
{
	int connector;
	connector = CSocketUtils::CreateConnectSocket(address, port);
	if (connector == -1)
		return -1;
	iomuxer_addreader(connector, iomuxer_handlereads);
	return 0;
}

int iomuxer_addlistener(char *address, int port)
{
	int listener;
	listener = CSocketUtils::CreateListenSocket(address, port);
	if (listener == -1)
		return -1;
	iomuxer_addreader(listener, iomuxer_handlenewconnection);
	return 0;
}

int iomuxer_explicithup(int fd)
{
	int ret;
	list<TConnectionEntity>::iterator a;
	list<TConnectionEntity>::iterator b;
	pthread_mutex_lock(&iCQMutex);
	for (a=iConnQueue.begin(),b=iConnQueue.end(); a!=b; a++) {
		if ((*a).fd == fd) {
			break;
		}
	}
	pthread_mutex_unlock(&iCQMutex);
	if (a != b) {
		pthread_mutex_lock(&iRQMutex);
		ret = event_del(&(*a).rev);
		pthread_mutex_unlock(&iRQMutex);
		if (ret == -1) {
			perror("event_del");
			return -1;
		}
		pthread_mutex_lock(&iCQMutex);
		iConnQueue.erase(a);
		pthread_mutex_unlock(&iCQMutex);
		iomuxer_removefrominqueue(fd);
		close((*a).fd);
	}
	return 0;
}

// public functions -- end

// private functions -- start

static void *iomuxer_handleevents(void *arg)
{
	THandlerData *paramdata = static_cast<THandlerData *>(arg);
	event_init();
	iomuxer_addlistener(paramdata->address, paramdata->port);
	event_dispatch();  // should block if not error
	pthread_exit(NULL);
}

static void iomuxer_handlenewconnection(const int listenfd, short event, void *arg)
{
	int ret;
	int flags;
	int newfd;
	socklen_t socklen;
	struct sockaddr_in remoteaddr;
	struct event *ev = static_cast<struct event *>(arg);
	event_add(ev, NULL);  // rescheduled
	newfd = accept(listenfd, (struct sockaddr*)&remoteaddr, &socklen);
	if (newfd == -1) {
		perror("accept");
		exit(-1);
	}
	// set new fd to non-blocking -- begin
	flags = fcntl(newfd, F_GETFL);
	if (flags == -1) {
		perror("fcntl");
		exit(-1);
	}
	flags |= O_NONBLOCK;
	ret = fcntl(newfd, F_SETFL, flags);
	if (ret == -1) {
		perror("fcntl");
		exit(-1);
	}
	// set new fd to non-blocking -- end
	iParent->MSyncIOMUXNewConnection(newfd);  // note: newfd NOT ready for reading
	iomuxer_addreader(newfd, iomuxer_handlereads);
}

static void iomuxer_handlereads(const int fd, short event, void *arg)
{
	int ret;
	ssize_t nbytes;
	ssize_t bytesgot;
	ssize_t newlength;
	TInQueueEntity inqent;
	unsigned char *message;
	list<TInQueueEntity>::iterator a;
	list<TInQueueEntity>::iterator b;
	// there should be some stuff in newfd, let's read
	pthread_mutex_lock(&iIQMutex);
	for (a=iInQueue.begin(),b=iInQueue.end(); a!=b; a++) {
		if ((*a).fd == fd) {
			break;
		}
	}
	if (a == b) {
		inqent.message = new unsigned char[iRBuflen];
		inqent.bytesgot = 0;
		inqent.fd = fd;
		iInQueue.push_back(inqent);
		a = iInQueue.end(); a--;
	}
	pthread_mutex_unlock(&iIQMutex);
	// a is now set to new entity
	nbytes = read(fd, inqent.message, iRBuflen-1);
	if (nbytes == -1) {
		switch (errno) {
			case EBADF:  // should not happen
				iomuxer_removefrominqueue_knowniq(a);
				return;
			case EFAULT:  // why this happens when we receive ^C from telnet???
				iomuxer_removefrominqueue_knowniq(a);
				return;
			case EIO:
				iomuxer_removefrominqueue_knowniq(a);
				return;
			case EINTR:  // should not happen
				iomuxer_removefrominqueue_knowniq(a);
				return;
			case EINVAL:  // should not happen
				iomuxer_removefrominqueue_knowniq(a);
				break;
			case EAGAIN:  // can happen because we use non-blocking sockets
				return;
			default:  // should never happen
				perror("read");
				iomuxer_removefrominqueue_knowniq(a);
				return;
		}
	}
	if (nbytes == 0) {  // implicit HUP (end of file)
		ret = iomuxer_explicithup_knowniq(fd, a);
		if (ret == -1) {
			exit(-1);
		}
		iParent->MSyncIOMUXImplicitHup(fd);
		return;
	}
	bytesgot = (*a).bytesgot;
	if (bytesgot+nbytes > iRBuflen-1) {  // would overflow
		nbytes = (iRBuflen-1) - bytesgot;
	}
	message = (*a).message;
	memcpy(message+bytesgot, inqent.message, nbytes);
	(*a).bytesgot += nbytes;
	newlength = bytesgot + nbytes;
	if (iParent->MSyncIOMUXPartialData(message, newlength)) {
		iParent->MSyncIOMUXNewData(fd, message, newlength);
		// The call is below because the callback above can call explicit HUP
		// Explicit HUP removes entity from iInQueue so "a" above can't be used
		iomuxer_removefrominqueue(fd);
	} else {
		if (newlength == iRBuflen-1) {
			// buffer full but no finalizer -> invalid message, remove
			iomuxer_removefrominqueue_knowniq(a);
		}
	}
}

static void iomuxer_addreader(int fd, void (*fn)(int, short, void*))
{
	int ret;
	struct event *evp;
	TConnectionEntity newconn;
	list<TConnectionEntity>::iterator a;
	pthread_mutex_lock(&iCQMutex);
	iConnQueue.push_back(newconn);
	a = iConnQueue.end(); a--;
	pthread_mutex_unlock(&iCQMutex);
	(*a).fd = fd;
	evp = &(*a).rev;
	event_set(evp, fd, EV_READ | EV_PERSIST, fn, evp);
	ADD_READER_EVENT(evp);
}

static int iomuxer_explicithup_knowniq(int fd, list<TInQueueEntity>::iterator iqpos)
{
	int ret;
	list<TConnectionEntity>::iterator a;
	list<TConnectionEntity>::iterator b;
	pthread_mutex_lock(&iCQMutex);
	for (a=iConnQueue.begin(),b=iConnQueue.end(); a!=b; a++) {
		if ((*a).fd == fd) {
			break;
		}
	}
	pthread_mutex_unlock(&iCQMutex);
	if (a != b) {
		pthread_mutex_lock(&iRQMutex);
		ret = event_del(&(*a).rev);
		pthread_mutex_unlock(&iRQMutex);
		if (ret == -1) {
			perror("event_del");
			return -1;
		}
		pthread_mutex_lock(&iCQMutex);
		iConnQueue.erase(a);
		pthread_mutex_unlock(&iCQMutex);
		iomuxer_removefrominqueue_knowniq(iqpos);
		close((*a).fd);
	}
	return 0;
}

static void iomuxer_removefrominqueue(int fd)
{
	list<TInQueueEntity>::iterator a;
	list<TInQueueEntity>::iterator b;
	pthread_mutex_lock(&iIQMutex);
	for (a=iInQueue.begin(),b=iInQueue.end(); a!=b; a++) {
		if ((*a).fd == fd) {
			delete [] (*a).message;
			iInQueue.erase(a);
			break;
		}
	}
	pthread_mutex_unlock(&iIQMutex);
}

static void iomuxer_removefrominqueue_knowniq(list<TInQueueEntity>::iterator iqpos)
{
	pthread_mutex_lock(&iIQMutex);
	delete [] (*iqpos).message;
	iInQueue.erase(iqpos);
	pthread_mutex_unlock(&iIQMutex);
}

// private functions -- end
