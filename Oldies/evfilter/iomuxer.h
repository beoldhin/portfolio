
#ifndef _IOMUXER_
#define _IOMUXER_

#include "MSyncIOMUX.h"

void iomuxer_setcallback(MSyncIOMUX *parent);
int iomuxer_startservice(char *address, int port, ssize_t rbuflen);
int iomuxer_sendmessage(int fd, unsigned char *message, ssize_t buflen);
int iomuxer_addconnector(char *address, int port);
int iomuxer_addlistener(char *address, int port);
int iomuxer_explicithup(int fd);

#endif  // _IOMUXER_
