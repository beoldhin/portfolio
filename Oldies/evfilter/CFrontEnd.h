
#ifndef _CFRONTEND_
#define _CFRONTEND_

#include "MSyncIOMUX.h"

class CFrontEnd : public MSyncIOMUX
{
public:
	int StartService(char *aAddress, int aPort);
	void MSyncIOMUXNewConnection(int aFd);
	int MSyncIOMUXPartialData(unsigned char *aMessage, int aBuflen);
	void MSyncIOMUXNewData(int aFd, unsigned char *aMessage, int aBuflen);
	void MSyncIOMUXImplicitHup(int aFd);
	void MSyncIOMUXFdChanged(int oldfd, int newfd);
};

#endif  // _CFRONTEND_
