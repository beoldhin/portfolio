#include <windows.h>
#include <stdio.h>

void DirectoryLister(void);


int iI;
BOOL bReturned = FALSE;
WIN32_FIND_DATA stFindData;
HANDLE hFindFirst;

char szTest[MAX_PATH+1];


void main(void)
{
	DirectoryLister();
}

void DirectoryLister(void)
{
	int iCounter;
	
	if (bReturned == FALSE)
	{
		iCounter = 1;
		
		printf("%d, UP\n", iCounter);
		
		hFindFirst = FindFirstFile("*.*", &stFindData);
		if (hFindFirst == INVALID_HANDLE_VALUE)
		{
			bReturned = TRUE;
			SetCurrentDirectory("..");
			printf("Failed 1.\n");
			return;
		}
		if ((strcmp(stFindData.cFileName,".")==0) || ((stFindData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)!=FILE_ATTRIBUTE_DIRECTORY))
		{
			do
			{
				if (FindNextFile(hFindFirst,&stFindData) == 0)
				{
					bReturned = TRUE;
					SetCurrentDirectory("..");
					printf("Failed 2.\n");
					return;
				}
			}
			while ((strcmp(stFindData.cFileName,"..")==0) || ((stFindData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)!=FILE_ATTRIBUTE_DIRECTORY));
		}
		iCounter++;
		bReturned = FALSE;
		SetCurrentDirectory(stFindData.cFileName);
		DirectoryLister();
	}
	else
	{
		printf("%d, DOWN\n", iCounter);
	
		hFindFirst = FindFirstFile("*.*", &stFindData);
		if (hFindFirst == INVALID_HANDLE_VALUE)
		{
			bReturned = TRUE;
			SetCurrentDirectory("..");
			printf("Failed 3.\n");
			return;
		}
		for (iI=0; iI<iCounter; iI++)
		{
			do
			{
				if (FindNextFile(hFindFirst,&stFindData) == 0)
				{
					bReturned = TRUE;
					SetCurrentDirectory("..");
					printf("Failed 4.\n");
					return;
				}
			}
			while ((strcmp(stFindData.cFileName,"..")==0) || ((stFindData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)!=FILE_ATTRIBUTE_DIRECTORY));
		}
		iCounter++;
		bReturned = FALSE;
		SetCurrentDirectory(stFindData.cFileName);
		DirectoryLister();
	}
}