/* argv[1] must be in lowercase */
#include <stdio.h>
#include <ctype.h>
#include <string.h>

int readch;

int main(int argc, char* argv[]) {
    int i;
    size_t strsize;
    if (argc != 2) {
        fprintf(stderr, "Error: Invalid number of parameters.\n");
        return -1;
    }
    
    while (1) {
        readch = tolower(getchar());
        strsize = strlen(argv[1]);
        for (i=0; i<strsize; i++) {
            if (argv[1][i] == readch) {
                return i + 1;
            }
        }
    }
    
    return 0;
}
